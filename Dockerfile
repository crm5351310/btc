FROM node:16.18.0
ADD . /home/backends/bchqs
WORKDIR /home/backends/bchqs

RUN npm i
RUN npm run build
CMD npm run start:prod