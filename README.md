# Dự án bộ chỉ huy quân sự - version 2 - backend


## 1. Cấu hình và chạy server

1.1.  Truy cập thư mục làm việc

`cd /home/btc/bchqs/backend`

1.2.  Thực hiện pull code mới nhất từ gitlab
- Bước 1: Tạo token (nếu đã có thì bỏ qua bước này)
Truy cập profile để sinh token [tại đây](https://gitlab.omzcloud.vn/-/user_settings/personal_access_tokens)
Chọn `add new token` và chọn các quyền tương ứng

- Bước 2: Tại thư mục làm việc, thực hiện lệnh
`git pull origin dev`
Nhập username + token (ở bước trên)

1.3. Build các service cho dự án

Đảm bảo server đã cài `docker`
Tại thư mục root (có chứa file `docker-compose.yml` đã cấu hình)
 
- Build database

`docker compose build mariadb`

`docker compose up -d mariadb`

Tài khoản DB được sinh ra sẵn
`root` `123456`
`bchqs` `12345678`  

Thực hiện phân quyền hoặc tạo mới các user cho phù hợp

- Build redis

`docker compose build redis`

`docker compose up -d redis`

  Truy cập redis (tại thư mục root của dự án)
  
  `docker compose exec -it redis redis-cli`

 `redis cli -h <host> -p <port>`

(nếu cấu hình authen cho redis  => cần cấu hình thêm vào source code)

- Build server

Thêm file `.env` vào thư mục root (nếu chưa có)

`docker compose build gtcc`

`docker compose up -d gtcc`

- Xem log nếu có lỗi xảy ra

`docker compose logs -f` hoặc `docker compose logs -f gtcc`

