import { Routes } from '@nestjs/core/router/interfaces';
import { categoryRouter } from '../module/category/category.module';
import { systemRouter } from '../module/system/system.module';
import { CitizenManagementModule } from '../module/citizen-management/citizen-management.module';
import { citizenProcessRootRoute } from '../module/citizen-process-management/citizen-process-management.module';
import { MilitaryRecruitmentModule } from 'src/module/militaty-recruitment/military-recruitment.module';
import { ReportModule } from '../module/report/report.module';

export const routerConfig: Routes = [
  categoryRouter,
  systemRouter,
  CitizenManagementModule.route,
  citizenProcessRootRoute,
  MilitaryRecruitmentModule.route,
  ReportModule.route,
];
