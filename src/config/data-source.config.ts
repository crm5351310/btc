import { config } from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions } from 'typeorm-extension';

import CommonHelper from '../common/utils/sys.util';

config();
// meta data for entities of system
const entitiesPath: string = CommonHelper.pathConfig('module');

export const mariaOption: DataSourceOptions & SeederOptions = {
  charset: 'utf8mb4_bin',
  type: 'mariadb',
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT),
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  timezone: 'Z',
  entities: [entitiesPath],
  synchronize: false,
  logging: ['error'],
  seeds: ['src/database/seeds/**/main.seed{.ts,.js}'],
  factories: ['src/database/factories/**/main.seed{.ts,.js}'],
  migrations: ['dist/database/migrations/**/*{.ts,.js}'],
  logger: 'debug',
};

export const mongoOption: DataSourceOptions = {
  type: 'mongodb',
  database: process.env.DATABASE2_NAME,
  url: process.env.DATABASE2_SRING,
  entities: [],
  synchronize: true,
};

export const dataSource = new DataSource(mariaOption);
