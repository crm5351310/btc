import {
  BadRequestException,
  INestApplication,
  NestApplicationOptions,
  ValidationPipe,
  ValidationPipeOptions,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import basicAuth from 'express-basic-auth';
import 'reflect-metadata';
import { AppModule } from '../app.module';
import { HttpExceptionFilter } from '../common/filter/http-exception.filter';
import { consola } from 'consola';
interface Init {
  port: string;
  prePort: number;
  enviroment: string;
  user: string;
  password: string;
  host: string;
}

class Main {
  private flag: number = 0;
  private config: NestApplicationOptions = {
    cors: true,
    logger: ['error', 'warn', 'verbose', 'debug', 'fatal'],
  };
  private validationConfig: ValidationPipeOptions = {
    disableErrorMessages: false,
    whitelist: true,
    transform: true,
    transformOptions: { enableImplicitConversion: true },
    forbidNonWhitelisted: false,
    exceptionFactory: (errors) => {
      return new BadRequestException(
        errors.map((error) => {
          const errorType =
            error.constraints != null
              ? Object.keys(error.constraints)[Object.keys(error.constraints).length - 1]
              : 'failed';
          return `${error.property}.${errorType}`;
        }),
      );
    },
  };
  private SWAGGER_ENVS: string[] = ['development', 'dev', 'staging'];
  private configSwaggerB = {
    swaggerOptions: {
      displayOperationId: true,
      persistAuthorization: true,
    },
    customSiteTitle: 'Bộ chỉ huy quân sự',
    customCss: '.swagger-ui .topbar {display: none; }',
  };
  private configSwaggerA = new DocumentBuilder()
    .setTitle('Phần mềm bộ chỉ huy quân sự')
    .setDescription('Tài liệu api cho phần mềm bộ chỉ huy quân sự - version 3.0')
    .setVersion('')
    .addBearerAuth({ in: 'header', type: 'http' }, 'Token')
    .addSecurityRequirements('Token')
    .build();

  pipe = async (app: INestApplication): Promise<void> => {
    try {
      app.useGlobalPipes(new ValidationPipe(this.validationConfig));
      consola.success(' Pipe');
    } catch (error) {
      consola.log(error);
      consola.fail(' Pipe');
      this.flag++;
    }
  };

  swagger = async (app: INestApplication, init: Init): Promise<void> => {
    try {
      if (this.SWAGGER_ENVS.includes(String(init.enviroment))) {
        app.use(
          ['/api'],
          basicAuth({
            challenge: true,
            users: {
              [init.user]: init.password,
            },
          }),
        );
        const document = SwaggerModule.createDocument(app, this.configSwaggerA);
        SwaggerModule.setup('api', app, document, this.configSwaggerB);
      }
      consola.success(' Swagger');
    } catch (error) {
      consola.log(error);
      consola.fail(' Swagger');
      this.flag++;
    }
  };

  onInit = (app: INestApplication): Init => {
    const configService = app.get(ConfigService);

    return {
      port: String(configService.get('PORT' as never)),
      prePort: 3100,
      enviroment: String(configService.get('NODE_ENV' as never)),
      user: String(configService.get('SWAGGER_USER' as never)),
      password: String(configService.get('SWAGGER_PASSWORD' as never)),
      host: String(configService.get('HOST' as never)),
    };
  };

  run = async (): Promise<void> => {
    const app = await NestFactory.create(AppModule, this.config);
    app.useGlobalFilters(new HttpExceptionFilter());
    const init = this.onInit(app);

    // console.clear();
    consola.start(' Booting server');

    await this.pipe(app);
    await this.swagger(app, init);
    await app.listen(init.port || init.prePort, '0.0.0.0');

    if (!this.flag) {
      consola.box('Server is running');
      consola.info(` Api: ${init.host}:${init.port || init.prePort}`);
      consola.info(` Swagger: ${init.host}:${init.port || init.prePort}/api\n\n`);
    } else {
      consola.error(' Config server failed');
    }
  };
}

export default Main;
