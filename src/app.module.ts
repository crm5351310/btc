import { CacheModule } from '@nestjs/cache-manager';
import {
  ClassSerializerInterceptor,
  Logger,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, RouterModule } from '@nestjs/core';

import * as redisStore from 'cache-manager-redis-store';

import { CategoryModule } from './module/category/category.module';
import { SystemModule } from './module/system/system.module';
import { HttpExceptionFilter } from './common/filter/http-exception.filter';
import { TypeOrmExceptionFilter } from './common/filter/typeorm-exception.filter';
import { AuthGuard } from './common/guard/auth.guard';
import { SysMiddleware } from './common/middleware/sys.middleware';
import { TimingMiddleware } from './common/middleware/time.middleware';
import { CitizenManagementModule } from './module/citizen-management/citizen-management.module';
import { CitizenProcessManagementModule } from './module/citizen-process-management/citizen-process-management.module';
import { MilitaryRecruitmentModule } from './module/militaty-recruitment/military-recruitment.module';
import { routerConfig } from './router/init.router';
import { mariaOption, mongoOption } from './config/data-source.config';
import { ShareModule } from "./module/shared/shared.module";
import { ReportModule } from './module/report/report.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env'],
    }),
    TypeOrmModule.forRootAsync({
      useFactory: async () => mariaOption,
    }),
    TypeOrmModule.forRootAsync({
      useFactory: async () => mongoOption,
    }),
    CacheModule.registerAsync({
      isGlobal: true,
      useFactory: async () => ({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        store: redisStore,
      }),
    }),
    RouterModule.register(routerConfig),
    SystemModule,
    CategoryModule,
    CitizenManagementModule,
    MilitaryRecruitmentModule,
    SystemModule,
    JwtModule,
    CitizenProcessManagementModule,
    ReportModule,
    ShareModule,
    ReportModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: TypeOrmExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimingMiddleware,
    },
    {
      provide: Logger,
      useValue: new Logger('', {
        timestamp: false,
      }),
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(SysMiddleware)
      // Define the routes you want this middleware to be applied to
      .forRoutes('/*');
    consumer.apply(TimingMiddleware).forRoutes('*');
  }
}
