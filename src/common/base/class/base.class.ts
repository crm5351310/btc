import { HttpStatus, Logger } from '@nestjs/common';
import { SubjectMessage } from '../../message/subject.message';
import { BaseMessage } from '../../message/base.message';
import { ContentMessage } from '../../message/content.message';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import ResponseHelper from '../../utils/reponse.util';
import { Transform } from 'class-transformer';

export class BaseService {
  protected name: string = '';
  protected subject: SubjectMessage = SubjectMessage.INIT;
  protected action: BaseMessage = BaseMessage.CREATE;
  private readonly logger = new Logger(this.name);
  async catch(func: () => void, name: string) {
    try {
      return func();
    } catch (error) {
      this.logger.debug(this[name].name);
      this.logger.error(error);
    }
  }

  response(data: any, failed: boolean = false) {
    const result = new Response(
      failed
        ? HttpStatus.INTERNAL_SERVER_ERROR
        : this.action === BaseMessage.CREATE
          ? HttpStatus.CREATED
          : HttpStatus.OK,
      data,
      this.subject,
      this.action,
      failed ? ContentMessage.INTERNAL_SERVER_ERROR : ContentMessage.SUCCESSFULLY,
    ).get();
    return result;
  }

  reponseCustom(content: ContentMessage, field: string | null) {
    const result = new Response(HttpStatus.BAD_REQUEST, null, this.subject, field, content).get();
    return result;
  }
}

export class Response {
  status: number;
  data: any;
  subject: SubjectMessage;
  field: string | null;

  content: ContentMessage | undefined;
  constructor(
    status: number,
    data: any,
    subject: SubjectMessage,
    field: string | null,
    content?: ContentMessage,
  ) {
    this.status = status;
    this.subject = subject;
    this.field = field;
    this.content = content;
    this.data = data;
  }
  get() {
    return {
      message: [this.field + '.' + this.content],
      data: this.data,
      statusCode: this.status,
      toast: true,
    };
  }
}

export class PathDto {
  @ApiProperty({
    description: 'Id',
  })
  @IsNotEmpty({
    message: ResponseHelper.response(SubjectMessage.PATH_VARIABLE, ContentMessage.REQUIRED),
  })
  @IsNumber(
    {},
    {
      message: ResponseHelper.response(SubjectMessage.PATH_VARIABLE, ContentMessage.INVALID),
    },
  )
  id: number;
}

export class YearDto {
  @ApiProperty({
    description: 'Năm',
    type: 'number',
    default: 1,
    required: false,
  })
  @IsNumber({})
  @IsOptional()
  year?: number;
}

export class PathArrayDto {
  @ApiProperty({
    description: 'Id',
    type: [Number],
    name: 'id',
    required: true,
  })
  @IsNotEmpty({
    message: ResponseHelper.response(SubjectMessage.PATH_VARIABLE, ContentMessage.REQUIRED),
  })
  id: string;
}

export class AdministrativeLevelDto {
  @ApiProperty({
    description: 'Cấp hành chính',
    type: [Number],
    name: 'administrativeLevel',
    required: true,
  })
  @IsNotEmpty({
    message: ResponseHelper.response(SubjectMessage.ADMINISTRATIVE_LEVEL, ContentMessage.REQUIRED),
  })
  @Transform(({ value }) => {
    if (typeof value == 'string') {
      value = [value];
    }
    return value;
  })
  administrativeLevel: number[];
}

export class RelationTypeBase {
  @ApiProperty({
    description: 'Id',
    name: 'id',
    required: true,
    default: 1,
  })
  id: number;
}

export class RelationTypeKeyBase {
  @ApiProperty({
    description: 'Key',
    name: 'key',
    required: true,
    default: 1,
  })
  key: number;
}

export class PaginationDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset?: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit?: number;
}
