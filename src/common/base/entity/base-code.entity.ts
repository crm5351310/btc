import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from './base.entity';
import { IsString, MaxLength } from 'class-validator';
import { Column } from 'typeorm';

export abstract class BaseCodeEntity extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  // validate
  @IsString()
  @MaxLength(25)
  // entity
  @Column('varchar', {
    length: 25,
    nullable: false,
  })
  code: string;
}
