import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export abstract class BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Id',
    minimum: 1,
  })
  // validate
  @IsNotEmpty()
  @IsNumber({})
  @ApiProperty({
    description: 'Id',
  })
  // entity
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: 'created_at', nullable: true })
  createdAt?: Date;

  @Column('varchar', { name: 'created_by', length: 255, nullable: true })
  createdBy?: string;

  @UpdateDateColumn({ name: 'updated_at', nullable: true })
  updatedAt?: Date;

  @Column('varchar', { name: 'updated_by', length: 255, nullable: true })
  updatedBy?: string;

  @DeleteDateColumn({ name: 'deleted_at', nullable: true })
  deletedAt?: Date;
}
