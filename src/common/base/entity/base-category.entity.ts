import { ApiProperty } from '@nestjs/swagger';
import { BaseCodeEntity } from './base-code.entity';
import { IsString, MaxLength } from 'class-validator';
import { Column } from 'typeorm';

export abstract class BaseCategoryEntity extends BaseCodeEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validate
  @IsString()
  @MaxLength(100)
  // entity
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  // nếu có giá trị true thì bản ghi ko được cập nhật, xóa
  @Column('boolean', { default: false })
  isFreeze?: boolean;
}
