import { BaseCategoryEntity } from '../entity/base-category.entity';
import { BaseService } from './base-service';
import { CustomBadRequestException } from '../../exception/bad.exception';
import { ContentMessage } from '../../message/content.message';

export abstract class BaseCategoryService<T extends BaseCategoryEntity> extends BaseService<T> {
  async checkNotExist(id: number, className: string): Promise<T> {
    const result = await this.repository.findOne({
      where: { id: id as any },
    });

    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, className, true);
    }

    if (result.isFreeze) {
      throw new CustomBadRequestException(className, ContentMessage.IS_FREEZE, true);
    }

    return result;
  }
}
