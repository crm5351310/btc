import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../response';

export interface BaseCRUDService<T> {
  create(dto: any): Promise<ResponseCreated<T>>;

  findAll(dto: any): Promise<ResponseFindAll<T>>;

  findOne(id: number): Promise<ResponseFindOne<T | null>>;

  update(id: number, dto: any): Promise<ResponseUpdate>;

  remove(id: number[]): Promise<ResponseDelete<T>>;
}
