import { ResponseFindAll, ResponseFindOne } from 'src/common/response';
import { Repository } from 'typeorm';
import { BaseCRUDService } from './base-crud.service';
import { BaseEntity } from '../entity/base.entity';
import { ContentMessage } from "../../message/content.message";
import { CustomBadRequestException } from "../../exception/bad.exception";

export abstract class BaseSimpleService<T extends BaseEntity>
  implements Pick<BaseCRUDService<T>, 'findAll' | 'findOne'>
{
  constructor(protected repository: Repository<T>) {}

  abstract findAll(dto: any): Promise<ResponseFindAll<T>>;

  abstract findOne(id: number): Promise<ResponseFindOne<T>>;

  /**
   * @param id là id của đối tượng
   * @param className tên class của đối tượng
   */
  async checkNotExist(id: number, className: string): Promise<T> {
    const result = await this.repository.findOne({
      where: { id: id as any },
    });

    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, className, true);
    }

    return result;
  }
}
