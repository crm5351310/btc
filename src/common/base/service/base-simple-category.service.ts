import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { BaseCategoryEntity } from '../entity/base-category.entity';
import { BaseSimpleService } from './base-simple.service';

export abstract class BaseSimpleCategoryService<
  T extends BaseCategoryEntity,
> extends BaseSimpleService<T> {
  /**
   * @param id là id của đối tượng
   * @param className tên class của đối tượng
   */
  async checkNotExist(id: number, className: string): Promise<T> {
    const result = await this.repository.findOne({
      where: { id: id as any },
    });

    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, className, true);
    }

    if (result.isFreeze) {
      throw new CustomBadRequestException(className, ContentMessage.IS_FREEZE, true);
    }

    return result;
  }
}
