import { DeepPartial, FindOptionsWhere, Not } from 'typeorm';
import { ResponseCreated, ResponseUpdate, ResponseDelete } from '../../response';
import { BaseEntity } from '../entity/base.entity';
import { BaseCRUDService } from './base-crud.service';
import { ContentMessage } from '../../message/content.message';
import { CustomBadRequestExceptionCustom } from '../../exception/bad.exception';
import { BaseSimpleService } from './base-simple.service';

export abstract class BaseService<T extends BaseEntity>
  extends BaseSimpleService<T>
  implements BaseCRUDService<T>
{
  abstract create(dto: any): Promise<ResponseCreated<T>>;

  abstract update(id: number, dto: any): Promise<ResponseUpdate>;

  abstract remove(id: number[]): Promise<ResponseDelete<T>>;

  /**
   * @param properties truyền list tên thuộc tính muốn check trùng
   * @param dto truyền vào dto
   * @param id truyền id của đối tượng đó nếu muốn bỏ qua check chính nó
   */
  protected async checkExist(
    properties: (keyof DeepPartial<T>)[],
    dto: DeepPartial<T>,
    id?: number,
  ): Promise<void> {
    const errors: string[] = [];

    await Promise.all(
      properties.map(async (property) => {
        if (!dto[property]) return;
        const isExist = await this.repository.exist({
          where: {
            id: id ? Not(id) : undefined,
            [property]: dto[property],
          } as FindOptionsWhere<T>,
        });
        if (isExist) {
          errors.push(`${property.toString()}.${ContentMessage.EXIST}`);
        }
      }),
    );

    if (errors.length > 0) {
      throw new CustomBadRequestExceptionCustom(errors);
    }
  }
}
