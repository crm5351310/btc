import XLSX from 'xlsx';
import { StringHelper } from '../../utils/string.util';
import { ValidateUtil } from '../../utils/validate.util';

export class Import {
  private file: Express.Multer.File;
  private code: string[];
  private data: object[];
  private headerCode: object;

  setFile(file: Express.Multer.File): void {
    this.file = file;
  }

  setData(codeRow: number, headerRow: number, slice: number): object {
    if (!this.file) throw new Error('Set file error: File not set');
    const workBook: XLSX.WorkBook = XLSX.read(this.file.buffer, {
      type: 'buffer',
      cellDates: true,
      cellNF: false,
    });
    const sheetName = workBook?.SheetNames[0];
    const sheet: XLSX.WorkSheet = workBook.Sheets[sheetName];
    const code: string[] = Object.values(XLSX.utils.sheet_to_json(sheet)[codeRow] as string[]);
    const header: string[] = Object.values(XLSX.utils.sheet_to_json(sheet)[headerRow] as string[]);
    if (code.includes('error')) code.pop();
    const headerCode: object = code.reduce((acc, key, index) => {
      return { ...acc, [key]: header[index] };
    }, {});
    const data: object[] = XLSX.utils
      .sheet_to_json(sheet, { header: code })
      .slice(slice) as object[];
    this.code = code;
    this.data = data;
    this.headerCode = headerCode;
    return {
      code: code,
      data: data,
      headerCode: headerCode,
    };
  }

  getData(): object[] {
    return StringHelper.spaceObject(this.data);
  }

  getHeaderCode(): object {
    return this.headerCode;
  }

  getCode(): string[] {
    return this.code;
  }

  getFile(): Express.Multer.File {
    return this.file;
  }
}

interface Validate {
  col: number;
  validation: string[];
}
export class FormControl {
  private readonly code: string[];
  private data: object[];
  private readonly headerCode: object;
  constructor(code: string[], headerCode: object, data: object[]) {
    this.code = code;
    this.headerCode = headerCode;
    this.data = data;
  }
  validationDataType(validations: Validate[]): boolean {
    const initArr = this.code.map((item: string) => {
      return {
        key: item,
      };
    });
    validations.forEach((item: Validate) => {
      initArr[item.col - 1]['validation'] = item.validation;
    });

    const validation = new ValidateUtil(this.data, this.headerCode, initArr);

    const val = validation.getData();
    this.data = val.data;
    return !val.errorCount;
  }

  getData(): object[] {
    return this.data;
  }
}
