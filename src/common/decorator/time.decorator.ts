// timing.decorator.ts
import { SetMetadata } from '@nestjs/common';

export const Timing = () => SetMetadata('timing', true);
