import { Controller as NestController, SetMetadata } from '@nestjs/common';

export const CustomController = (prefix: string) => {
  return (target: any) => {
    const root = prefix.split('\\');
    const src = root.findIndex((item: string) => item === 'module') + 1;
    prefix = root.slice(src).join('/');
    SetMetadata('prefix', prefix)(target);
    NestController(prefix)(target);
  };
};
