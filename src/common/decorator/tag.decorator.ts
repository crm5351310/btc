// api-tag.decorator.ts
import { applyDecorators, SetMetadata } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../enum/tag.enum';
import SysHelper from '../utils/sys.util';

export const CustomTag = (tag: string) => {
  tag = TagEnum[SysHelper.prefix(tag)];
  return applyDecorators(SetMetadata('tag', tag), ApiTags(tag));
};
