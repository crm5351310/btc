export interface BaseInterfaceService {
  condition?: string;
  property?: string;
  isProp: boolean;
}
