interface ValidateUtilType {
  key: string;
  validation?: string[];
}
export class ValidateFunc {
  static readonly isNumber: string = 'isNumber';
  static readonly isInt: string = 'isNumber';
  static readonly isRequired: string = 'isRequired';
  static readonly max25: string = 'max25';
  static readonly max50: string = 'max50';
  static readonly max100: string = 'max100';
  static readonly max1000: string = 'max1000';
  static readonly max5000: string = 'max5000';
  static readonly isDate: string = 'isDate';
}
export class ValidateUtil {
  data: object[];
  validate: ValidateUtilType[];
  headerCode: object;
  constructor(data: object[], headerCode: object, validate: ValidateUtilType[]) {
    this.data = data;
    this.validate = validate;
    this.headerCode = headerCode;
  }

  getData() {
    let errorCount = 0;
    console.log(this.headerCode)
    this.data.forEach((item: object, index: number) => {
      this.validate.forEach((val: ValidateUtilType) => {
        if (val.validation && val.validation.length) {
          val.validation.forEach((valChild: string) => {
            const validationResult = this[valChild](item[val.key]);
            if (validationResult.error) {
              const errorMessage =
                this.headerCode[val.key].replace(/\*/g, '') + ' ' + validationResult.message + '\n';
              if (this.data[index]['error'] === undefined) {
                this.data[index]['error'] = '';
              }
              this.data[index]['error'] += errorMessage;
              errorCount++;
            }
          });
        }
      });
      if (this.data[index]['error']) {
        this.data[index]['error'] = this.data[index]['error'].trim();
      }
    });
    return {
      data: this.data,
      errorCount: errorCount,
    };
  }

  isNumber(value: string) {
    let error = false;
    const numberPattern = /^-?\d*\.?\d+$/;
    if (!numberPattern.test(value)) {
      error = true;
    }
    return {
      error: error,
      message: 'not a number',
    };
  }

  isInt(value: string) {
    let error = false;
    const integerPattern = /^-?\d+$/;
    if (!integerPattern.test(value)) {
      error = true;
    }
    return {
      error: error,
      message: 'not an integer',
    };
  }

  isRequired(value: string) {
    let error = false;
    if (value === null || value === undefined) {
      error = true;
    }
    return {
      error: error,
      message: 'bắt buộc',
    };
  }

  isDate(value: string) {
    let error = false;
    if (!value) return { error: error, message: '' };
    const dateFormatPattern =
      /^(0?[1-9]|1\d|2[0-8])\/(0?[1-9]|1[0-2])\/((19|20)\d{2})$|^(29)\/(0?2)\/((19|20)(04|08|[2468][048]|[13579][26]))$|^([12]\d|30)\/(0?[13-9]|1[0-2])\/((19|20)\d{2})$|^([12]\d|31)\/(0?[13578]|1[02])\/((19|20)\d{2})$|^([12]\d|30)\/(0?2)\/((19|20)\d{2})$|^([12]\d|29)\/(0?2)\/(((19|20)(04|08|[2468][048]|[13579][26]))|2000)$/;
    if (!dateFormatPattern.test(value) || !this.isValidDateString(value)) {
      error = true;
    }
    return {
      error: error,
      message: 'không hợp lệ',
    };
  }

  private isValidDateString(dateString: string) {
    const parts = dateString.split('/');
    if (parts.length !== 3) return false;
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10);
    const year = parseInt(parts[2], 10);
    return this.isValidDate(year, month, day);
  }

  private isValidDate(year: number, month: number, day: number) {
    const date = new Date(year, month - 1, day);
    return date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day;
  }

  max100(value: string) {
    const temp = this.nullable(value);
    let error = false;
    if (temp.length > 100) {
      error = true;
    }
    return {
      error: error,
      message: 'không vượt quá 100 ký tự',
    };
  }

  max1000(value: string) {
    const temp = this.nullable(value);
    let error = false;
    if (temp.length > 1000) {
      error = true;
    }
    return {
      error: error,
      message: 'không vượt quá 1000 ký tự',
    };
  }

  max5000(value: string) {
    const temp = this.nullable(value);
    let error = false;
    if (temp.length > 5000) {
      error = true;
    }
    return {
      error: error,
      message: 'không vượt quá 5000 ký tự',
    };
  }

  max50(value: string) {
    const temp = this.nullable(value);
    let error = false;
    if (temp.length > 50) {
      error = true;
    }
    return {
      error: error,
      message: 'không vượt quá 50 ký tự',
    };
  }

  max25(value: string) {
    const temp = this.nullable(value);
    let error = false;
    if (temp.length > 25) {
      error = true;
    }
    return {
      error: error,
      message: 'không vượt quá 25 ký tự',
    };
  }

  nullable(value: string | null | undefined) {
    if (value === null || value === undefined) {
      return '';
    }
    return value;
  }
}
