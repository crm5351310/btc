import * as ExcelJS from 'exceljs';
import { border } from '../constant/report.constant';
import { Readable as ReadableStream } from 'node:stream';

export interface JsonParam {
  sheet: string;
  initWidth: number;
  initHeight: number;
  headerHeight: number;
  fill?: object[];
  dropdown?: Dropdown[];
}

export interface Dropdown {
  col: number;
  data: string;
}

export class ReportExportUtil {
  static common = async (json: JsonParam[], file: Buffer): Promise<Buffer> => {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(file);
    json.forEach((item: JsonParam) => {
      const sheet = workbook.getWorksheet(item.sheet);
      if (sheet) {
        if (!item.fill?.length) item.fill = [{ stt: 1 }, { stt: 2 }, { stt: 3 }];

        let height = item.initHeight;
        const header: string[] = [];
        for (let i = 0; i < item.initWidth; i++) {
          header.push(String(sheet.getRow(item.initHeight - 1).getCell(i + 1).value || ''));
        }

        if (item.dropdown?.length) {
          item.dropdown.forEach((itemDropdown) => {
            for (let i = item.initHeight; i <= item.initHeight + 1000; i++) {
              sheet.getRow(i).getCell(itemDropdown.col).dataValidation = {
                type: 'list',
                allowBlank: true,
                formulae: [itemDropdown.data],
                showInputMessage: true,
                promptTitle: 'Danh sách',
                prompt: 'Chọn hoặc nhập đúng dữ liệu đúng trong droplist',
                showErrorMessage: true,
                errorTitle: 'Dữ liệu sai',
                error: 'Chọn hoặc nhập lại dữ liệu đúng trong droplist',
              };
            }
          });
        }

        if (item.fill != undefined) {
          item.fill.forEach((item: never) => {
            header.forEach((itemHeader: string, indexHeader: number) => {
              sheet.getRow(height).getCell(indexHeader + 1).border = border;
              if (item[itemHeader]) {
                sheet.getRow(height).getCell(indexHeader + 1).value = item[itemHeader];
              }
            });
            height++;
          });
        }
      }
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return await workbook.xlsx.writeBuffer();
  };

  static header = (fileName: string) => {
    return {
      'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'Content-Disposition': `attachment; filename*=UTF-8''${encodeURIComponent(fileName)}`,
    };
  };
}
export class ImportUtil {
  static common = async (json: JsonParam[], file: Buffer, sheetNumber: number): Promise<Buffer> => {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(file);

    const item = json[sheetNumber];
    let height = item.initHeight + 1;
    const sheet = workbook.getWorksheet(item.sheet);

    if (sheet) {
      const header: string[] = [];
      for (let i = 0; i < item.initWidth; i++) {
        header.push(String(sheet.getRow(item.initHeight - 1).getCell(i + 1).value || ''));
      }

      if (item.fill != undefined) {
        item.fill.forEach((itemFill: never) => {
          header.forEach((itemHeader: string, indexHeader: number) => {
            sheet.getRow(height).getCell(indexHeader + 1).border = border;
            if (itemFill[itemHeader] || itemFill[itemHeader] === 0) {
              sheet.getRow(height).getCell(indexHeader + 1).value = itemFill[itemHeader];
            }
          });
          height++;
        });
      }
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return await workbook.xlsx.writeBuffer();
  };
}