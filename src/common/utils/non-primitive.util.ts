type NestedObject = { [key: string]: any };
export class NonPrimitiveUtil {
  static getAllKeys<T extends object>(enumObject: T): (keyof T)[] {
    return Object.keys(enumObject) as (keyof T)[];
  }

  static getObjectLengthAndDepth(arr: NestedObject[]): {
    length: number;
    depth: number;
  } {
    let maxLength = 0;
    let maxDepth = 0;

    function traverseObject(obj: NestedObject, currentDepth: number): void {
      if (currentDepth > 2) {
        return; // Stop traversing deeper if depth exceeds 2
      }

      for (const key in obj) {
        if (key === 'brave' && Array.isArray(obj[key])) {
          for (const childObj of obj[key]) {
            traverseObject(childObj, currentDepth + 1);
          }
        } else if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
          traverseObject(obj[key], currentDepth + 1);
        }
      }
      maxLength = Math.max(maxLength, Object.keys(obj).length);
      maxDepth = Math.max(maxDepth, currentDepth);
    }

    for (const obj of arr) {
      traverseObject(obj, 1);
    }

    return { length: maxLength, depth: maxDepth };
  }
}
