import { join } from 'path';

export default class SysHelper {
  static pathConfig = (path: string): string => {
    return join(__dirname, '..', '..', path, '/**/*.entity{.ts,.js}');
  };

  static prefix = (path: string): any => {
    const root = path.split('\\');
    return root[root.length - 1];
  };
}
