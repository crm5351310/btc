import * as fs from 'fs';
import { join } from 'path';

export class StringHelper {
  static space = (letter: string) => {
    const withoutExtraSpaces = letter.replace(/[^\S\n]+/g, ' ');
    return withoutExtraSpaces.trim();
  };
  static spaceObject = (obj: any) => {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (typeof obj[key] === 'string') {
          obj[key] = StringHelper.space(obj[key]);
          obj[key] = obj[key].trim();
        } else if (typeof obj[key] === 'object') {
          StringHelper.spaceObject(obj[key]);
        }
      }
    }
    return obj;
  };

  static dateConvert = (date: any, symbol?: any, check?: boolean) => {
    let data: any = null;
    if (date) {
      date = new Date(date);
      data =
        (check ? (symbol ? symbol : '"') : '') +
        date.getFullYear().toString() +
        '-' +
        (date.getMonth() < 10
          ? '0' + (date.getMonth() + 1).toString()
          : (date.getMonth() + 1).toString()) +
        '-' +
        (date.getDate() < 10 ? '0' + (date.getDate() + 1).toString() : date.getDate().toString()) +
        (check ? (symbol ? symbol : '"') : '');
    }

    return data;
  };

  static raw(fileName: string, path: string) {
    const filePath = join(__dirname, '..', '..', 'database', 'sql', path, fileName + '.sql');
    return fs
      .readFileSync(filePath)
      .toString()
      .replace(/(\r\n|\n|\r)/gm, ' ')
      .replace(/\s+/g, ' ');
  }

  static randomTimestamp() {
    const startDate = new Date(2020, 0, 1).getTime();
    const endDate = new Date(2024 + 1, 0, 1).getTime() - 1;
    return new Date(startDate + Math.random() * (endDate - startDate)).getTime();
  }

  static randomTimestampInit() {
    const startDate = new Date(2000, 0, 1).getTime();
    const endDate = new Date(2011 + 1, 0, 1).getTime() - 1;
    return new Date(startDate + Math.random() * (endDate - startDate)).getTime();
  }
}
