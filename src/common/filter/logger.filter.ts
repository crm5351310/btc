import { ConsoleLogger } from '@nestjs/common';

export class CustomLogger extends ConsoleLogger {
  log(message: any, context?: string) {
    // Check if the message is a SQL error and ignore it
    if (typeof message === 'string' && message.includes('QueryFailedError')) {
      return; // Ignore SQL error messages
    }
    super.log(message, context);
  }
}
