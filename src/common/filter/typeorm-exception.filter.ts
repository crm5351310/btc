import { Catch, ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { QueryFailedError } from 'typeorm';
import { ContentMessage } from '../message/content.message';
import { pascalCase } from 'change-case-all';

@Catch(QueryFailedError)
export class TypeOrmExceptionFilter implements ExceptionFilter {
  catch(exception: QueryFailedError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    const driverError = exception.driverError as DriverError;

    if (driverError.code === 'ER_NO_REFERENCED_ROW_2') {
      const fieldError = driverError.sqlMessage.split('REFERENCES')[1].trim().split('`')[1];
      const changeCase = pascalCase(fieldError);
      return response.status(400).json({
        message: [`${changeCase}.${ContentMessage.NOT_FOUND}`],
        error: 'Validate failed',
        statusCode: 400,
        toast: true,
      });
    }

    response.status(500).json({
      message: ['die.server'],
      error: exception.message,
      statusCode: 500,
      toast: true,
    });
  }
}

interface DriverError {
  code: string;
  errno: number;
  sqlState: string;
  sqlMessage: string;
  sql: string;
}
