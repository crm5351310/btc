// custom.middleware.ts

import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { StringHelper } from '../utils/string.util';

@Injectable()
export class SysMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    // Your middleware logic goes here
    req.body = StringHelper.spaceObject(req.body);
    next();
  }
}
