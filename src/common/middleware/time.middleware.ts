// timing.middleware.ts
import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
@Injectable()
export class TimingMiddleware implements NestMiddleware {
  constructor(private readonly logger: Logger) {}
  use(req: Request, res: Response, next: any) {
    if (process.env.DEBUG && process.env.DEBUG === 'on') {
      const start = process.hrtime();
      res.on('finish', () => {
        const elapsed = process.hrtime(start);
        const endpointArr = req.path.split('/');
        const endpoint = endpointArr[endpointArr.length - 1];
        this.logger.debug(
          ` ${req.method} ${endpoint} completed in ${elapsed[0]}s ${elapsed[1] / 1000000}ms`,
        );
      });
    }
    next();
  }
}
