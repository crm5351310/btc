export enum ContentMessage {
  // status
  SUCCESSFULLY = 'successfully',
  FAILURE = 'failure',
  INTERNAL_SERVER_ERROR = 'internalServerError',

  // validate
  INVALID = 'invalid',
  REQUIRED = 'required',
  NOT_FOUND = 'notFound',
  EXIST = 'existed',
  IN_USED = 'inUsed',
  IS_FREEZE = 'isFreeze',

  INVALIDED = 'invalided',
}

export const contentMaxLength = (num: number) => {
  return 'maxLength' + num;
};
