export enum BaseMessage {
  CREATE = 'create',
  READ = 'read',
  UPDATE = 'update',
  DELETE = 'delete',

  LOGIN = 'login',
  LOGOUT = 'logout',
  REFRESH = 'refresh',
}
