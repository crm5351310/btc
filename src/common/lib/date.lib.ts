import * as _ from 'lodash';

/**
 * Type: yyyy + symbol + mm + symbol + dd
 */
export class DateLib {
  private datePattern = /^\d{4}([-/])(0[1-9]|1[0-2])\1(0[1-9]|[1-2][0-9]|3[0-1])$/;
  private formatPattern = /^[;:><?\/\\'"-.!@#$^&*]+$/;
  private date: string | Date = '';
  private symbol: string = '/';

  /**
   * Set date
   */
  setDate = (date: string): void => {
    if (!this.pattern(date)) throw new Error('Set date error: Not type date');
    this.date = this.symbols(date, this.symbol);
  };

  setDateTime = (date: Date): void => {
    if (!_.isDate(date)) throw new Error('Set date error: Not type date');
    this.date = date;
  };

  /**
   * Set symbol for date
   */
  setFormatBy = (symbolFormat: string): void => {
    if (typeof this.date !== 'string') throw new Error('Set format by error: date is date string');
    if (!this.date) throw new Error('Set format by error: date is not provide');
    if (symbolFormat.length != 1) throw new Error('Set format by error: 1 length of symbol');
    if (!this.formatPattern.test(symbolFormat))
      throw new Error('Set format by error: Not a symbol');
    this.date = this.symbols(this.date, symbolFormat);
    this.symbol = symbolFormat;
  };

  /**
   * Get current date
   */
  getDate = (): string | Date => {
    return this.date;
  };

  /**
   * 1 is firstDate > secondeDate
   * -1 is firstDate < secondeDate
   * 0 is firstDate = secondeDate
   */
  compareTo = (date: string): number => {
    if (typeof this.date !== 'string') throw new Error('Set format by error: date is date string');
    if (!this.pattern(date)) throw new Error('Set date error: Not type date');
    return this.compare(new Date(this.date), new Date(this.symbols(date, this.symbol)));
  };

  compareToDateTime = (date: Date): number => {
    if (typeof this.date === 'string') throw new Error('Set format by error: date is date string');
    return this.compare(new Date(this.date), new Date(date));
  };

  /**
   * Add day, month, year, minute to date
   */
  addDay = (days: number): void => {
    this.adding(days, TypeDate.DAY);
  };

  addMonth = (months: number): void => {
    this.adding(months, TypeDate.MONTH);
  };

  addYear = (years: number): void => {
    this.adding(years, TypeDate.YEAR);
  };

  addMinute = (minutes: number): void => {
    if (typeof this.date === 'string') throw new Error('Set format by error: date is date string');
    this.adding(minutes, TypeDate.MINUTE);
  };

  private compare = (firstDate: Date, secondDate: Date): number => {
    switch (true) {
      case firstDate > secondDate:
        return 1;
      case firstDate < secondDate:
        return -1;
      case _.isEqual(firstDate, secondDate):
        return 0;
    }
    return 5;
  };
  private pattern = (date: string): boolean => {
    return this.datePattern.test(date);
  };

  private symbols = (date: string, symbol: string): string => {
    return date.split(/\W+/).join(symbol);
  };

  private format = (date: Date, symbol: string): string => {
    const day = date.getDate() > 10 ? date.getDate() : '0' + date.getDate();
    const month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
    return date.getFullYear() + symbol + month + symbol + day;
  };
  private adding = (days: number, type: string): void => {
    const day: Date = new Date(this.date);
    switch (true) {
      case type === TypeDate.DAY:
        day.setDate(day.getDate() + days);
        break;
      case type === TypeDate.MONTH:
        day.setMonth(day.getMonth() + days);
        break;
      case type === TypeDate.YEAR:
        day.setFullYear(day.getFullYear() + days);
        break;
      case type === TypeDate.MINUTE:
        day.setMinutes(day.getMinutes() + days);
        break;
    }

    if (typeof this.date === 'string') {
      const dayString: string = this.format(day, this.symbol);
      this.date = this.symbols(dayString, this.symbol);
    } else {
      this.date = day;
    }
  };
}

// eslint-disable-next-line no-unused-vars
enum TypeDate {
  // eslint-disable-next-line no-unused-vars
  DAY = 'day',
  // eslint-disable-next-line no-unused-vars
  MONTH = 'month',
  // eslint-disable-next-line no-unused-vars
  YEAR = 'year',
  // eslint-disable-next-line no-unused-vars
  MINUTE = 'minute',
}
