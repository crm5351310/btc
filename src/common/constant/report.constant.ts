import * as ExcelJS from 'exceljs';

export const center: Partial<ExcelJS.Alignment> = {
  vertical: 'middle',
  horizontal: 'center',
  wrapText: true,
};

export const left: Partial<ExcelJS.Alignment> = {
  vertical: 'middle',
  horizontal: 'left',
  wrapText: true,
};

export const right: Partial<ExcelJS.Alignment> = {
  vertical: 'middle',
  horizontal: 'right',
  wrapText: true,
};

export const largeBold: Partial<ExcelJS.Font> = {
  name: 'Times New Roman',
  size: 14,
  bold: true,
};

export const mediumBold: Partial<ExcelJS.Font> = {
  name: 'Times New Roman',
  size: 13,
  bold: true,
};

export const smallBold: Partial<ExcelJS.Font> = {
  name: 'Times New Roman',
  size: 12,
  bold: true,
};

export const large: Partial<ExcelJS.Font> = {
  name: 'Times New Roman',
  size: 14,
};

export const medium: Partial<ExcelJS.Font> = {
  name: 'Times New Roman',
  size: 13,
};

export const small: Partial<ExcelJS.Font> = {
  name: 'Times New Roman',
  size: 12,
};

export const border: Partial<ExcelJS.Borders> = {
  top: { style: 'thin' },
  left: { style: 'thin' },
  bottom: { style: 'thin' },
  right: { style: 'thin' },
};

export const D6DCE4: ExcelJS.FillPattern = {
  type: 'pattern',
  pattern: 'solid',
  fgColor: { argb: 'D6DCE4' },
  bgColor: { argb: 'D6DCE4' },
};
