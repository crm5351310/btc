export const GeoLocationConst = {
  NOI_DIA: 'Nội địa',
  BIEN_GIOI_VIET_TRUNG: 'Biên giới Việt - Trung',
  BIEN_GIOI_VIET_LAO: 'Biên giới Việt - Lào',
  BIEN_GIOI_VIET_CAMPUCHIA: 'Biên giới Việt - Campuchia',
  VEN_BIEN: 'Ven biển',
  DAO: 'Đảo',
};
