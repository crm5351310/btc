import { HttpException, HttpStatus } from '@nestjs/common';

export class CustomBadRequestException extends HttpException {
  constructor(content: string, field: string, toast: boolean) {
    super(
      {
        message: [field + '.' + content],
        error: 'Validate failed',
        statusCode: HttpStatus.BAD_REQUEST,
        toast: toast,
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class CustomBadRequestExceptionCustom extends HttpException {
  constructor(message: string[]) {
    super(
      {
        message: message,
        error: 'Validate failed',
        statusCode: HttpStatus.BAD_REQUEST,
        toast: false,
      },
      HttpStatus.BAD_REQUEST,
    );
  }

  static message(content: string, field: string) {
    return field + '.' + content;
  }
}
