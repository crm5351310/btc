import { BaseEntity } from "../base/entity/base.entity";
import { ContentMessage } from "../message/content.message";
import { CustomBadRequestException } from "./bad.exception";

export class InvalidException<T extends BaseEntity> extends CustomBadRequestException {
  constructor(property: keyof T) {
    super(
      ContentMessage.INVALID,
      property.toString(),
      false,
    );
  }
}