import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';
import { BaseMessage } from "../message/base.message";

export class ResponseCreated<T> extends ResponseCustom {
  constructor(public readonly data: T) {
    super(BaseMessage.CREATE, HttpStatus.CREATED);
  }
}
