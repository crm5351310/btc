import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';
import { BaseMessage } from "../message/base.message";

export class ResponseFindOne<T> extends ResponseCustom {
  constructor(public data: T | null) {
    super(BaseMessage.READ, HttpStatus.OK);
  }
}
