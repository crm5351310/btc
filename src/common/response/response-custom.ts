import { HttpStatus } from '@nestjs/common';

export abstract class ResponseCustom {
  constructor(message: string, statusCode: HttpStatus) {
    this.message = message;
    this.statusCode = statusCode;
    this.toast = true;
  }

  readonly message: string;
  readonly statusCode: HttpStatus;
  readonly toast: boolean;
}
