import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';
import { BaseMessage } from "../message/base.message";

export class ResponseDelete<T> extends ResponseCustom {
  constructor(results: T[], ids: number[], rows?: T[]) {
    super(BaseMessage.DELETE, HttpStatus.OK);
    this.data = {
      affected: (rows ? rows.length + ids.length - results.length : results.length),
    };
  }

  data: any;
}
