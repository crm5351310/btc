import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';
import { Pagination } from '../utils/pagination.util';
import { BaseMessage } from "../message/base.message";

export class ResponseFindAll<T> extends ResponseCustom {
  constructor(data: T[], total: number) {
    super(BaseMessage.READ, HttpStatus.OK);
    this.data = new Pagination<T>(data, total);
  }

  data: Pagination<T>;
}
