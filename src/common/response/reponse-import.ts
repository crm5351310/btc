import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';

export class ReponseImport<T> extends ResponseCustom {
  constructor(public readonly data: T) {
    super('import.successfully', HttpStatus.CREATED);
  }
}
