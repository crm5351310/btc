import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';

export class ReponseExport<T> extends ResponseCustom {
  constructor(public readonly data: T) {
    super('export.successfully', HttpStatus.OK);
  }
}
