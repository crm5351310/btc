import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';

export class ResponseSync<T> extends ResponseCustom {
  constructor(public readonly data: T) {
    super('sync.successfully', HttpStatus.OK);
  }
}
