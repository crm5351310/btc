import { HttpStatus } from '@nestjs/common';
import { ResponseCustom } from './response-custom';
import { BaseMessage } from "../message/base.message";

export class ResponseUpdate extends ResponseCustom {
  constructor(public data: any) {
    super(BaseMessage.UPDATE, HttpStatus.OK);
  }
}
