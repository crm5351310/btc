export enum ReportEnum {
  M_2 = '" (m"&CHAR(178)&")"',
  M = '" (m)"',
}

export enum ReportSheetEnum {}

export enum WidthEnum {
  STT = 7,
  VERY_SHORT = 10,
  SHORT = 15,
  MEDIUM = 20,
  LONG = 25,
  VERY_LONG = 30,
}

export enum AlignmentEnum {
  LEFT = 'left',
  CENTER = 'center',
  RIGHT = 'right',
}

export enum ErrorEnum {
  error = 'Thông báo lỗi',
}

export enum ReportDatatype {
  number = '0',
  date = 'MM/DD/YYYY',
  string = '@',
}
