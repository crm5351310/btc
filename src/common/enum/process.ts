export enum ProcessEnum{
  NVQS_DK = 'NVQS_DK',
  NVQS_MDK = 'NVQS_MDK',
  NVQS_THDK = 'NVQS_THDK',
  NVQS_CDK = 'NVQS_CDK',
  NVQS_TDK = 'NVQS_TDK',
}