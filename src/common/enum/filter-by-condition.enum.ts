export enum FilterByConditionEnum {
  DQTV = 'dqtv',
  DBDVH1 = 'dbdvh1',
  DBDVH2 = 'dbdvh2',
  SQDB = 'sqdb',
}
