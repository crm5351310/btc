import { ValidationOptions, registerDecorator } from 'class-validator';

export function IsIdentityNumber(validationOptions?: ValidationOptions) {
  return function (object: object, propertyName: string) {
    registerDecorator({
      name: 'isIdentityNumber',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any) {
          return value && typeof value === 'string' && !Number.isNaN(+value);
        },
      },
    });
  };
}
