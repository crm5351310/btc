import { ValidationOptions, registerDecorator } from 'class-validator';

export function IsObjectRelation(validationOptions?: ValidationOptions) {
  return function (object: object, propertyName: string) {
    registerDecorator({
      name: 'isObjectRelation',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any) {
          return value && value.id && typeof value.id === 'number' && !Number.isNaN(value.id);
        },
      },
    });
  };
}
