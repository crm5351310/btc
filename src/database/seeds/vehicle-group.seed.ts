import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { VehicleGroup } from '../../module/category/vehicle-root/vehicle-group/vehicle-group.entity';

export default class VehicleGroupSeed implements Seeder {
  public vehicleGroup = {
    DB: {
      id: 1,
      code: 'db',
      name: 'PTVT cơ giới đường bộ và chuyên dùng đường bộ',
    },
    DT: {
      id: 2,
      code: 'dt',
      name: 'PTVT cơ giới đường thủy và chuyên dùng đường thủy',
    },
    XDCT: {
      id: 3,
      code: 'xdct',
      name: 'PTXD cầu đường xây dựng công trình',
    },
    XDHH: {
      id: 4,
      code: 'xdhh',
      name: 'PT xếp dỡ hàng hóa',
    },
    KHAC: {
      id: 5,
      code: 'khac',
      name: 'Phương tiện khác',
    },
  };
  public async run(dataSource: DataSource): Promise<any> {
    const vehicleRepository = dataSource.getRepository(VehicleGroup);
    await vehicleRepository.save([
      this.vehicleGroup.DB,
      this.vehicleGroup.DT,
      this.vehicleGroup.KHAC,
      this.vehicleGroup.XDHH,
      this.vehicleGroup.XDCT,
    ]);
  }
}
