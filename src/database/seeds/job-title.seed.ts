import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { JobTitle } from '../../module/category/job-title/job-title.entity';
export const jobTitleData = {
  TRO_LY: {
    id: 1,
    code: 'ts',
    name: 'Trợ lý',
  },
  THO: {
    id: 2,
    code: 'tho',
    name: 'Thợ',
  },
  TIEU_DOI_TRUONG: {
    id: 3,
    code: 'tieudt',
    name: 'Tiểu đội trưởng',
  },
  CHIEN_SI: {
    id: 4,
    code: 'cs',
    name: 'Chiến sĩ',
  },
  KHAU_DOI_TRUONG: {
    id: 5,
    code: 'kdt',
    name: 'Khẩu đội trưởng',
  },
  PHO_BI_THU_DOAN: {
    id: 6,
    code: 'pbtd',
    name: 'Phó bí thư đoàn',
  },
  BI_THU_DOAN: {
    id: 7,
    code: 'btd',
    name: 'Bí thư đoàn',
  },
  CHI_HUY_TRUONG: {
    id: 8,
    code: 'cht',
    name: 'Chỉ huy trưởng',
  },
  CHINH_TRI_VIEN: {
    id: 9,
    code: 'ctv',
    name: 'Chính trị viên',
  },
  CHINH_TRI_VIEN_PHO: {
    id: 10,
    code: 'ctvp',
    name: 'Chính trị viên phó',
  },
  CHI_HUY_PHO: {
    id: 11,
    code: 'chp',
    name: 'Chỉ huy phó',
  },
  TRUNG_DOI_TRUONG: {
    id: 12,
    code: 'trungdt',
    name: 'Trung đội trưởng',
  },
  PHO_TRUNG_DOI_TRUONG: {
    id: 13,
    code: 'ptdt',
    name: 'Phó trung đội trưởng',
  },
};
export default class JobTitleSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(JobTitle);
    await reporitory.save([
      jobTitleData.TRO_LY,
      jobTitleData.THO,
      jobTitleData.TIEU_DOI_TRUONG,
      jobTitleData.CHIEN_SI,
      jobTitleData.KHAU_DOI_TRUONG,
      jobTitleData.PHO_BI_THU_DOAN,
      jobTitleData.BI_THU_DOAN,
      jobTitleData.CHI_HUY_TRUONG,
      jobTitleData.CHINH_TRI_VIEN,
      jobTitleData.CHINH_TRI_VIEN_PHO,
      jobTitleData.CHI_HUY_PHO,
      jobTitleData.TRUNG_DOI_TRUONG,
      jobTitleData.PHO_TRUNG_DOI_TRUONG,
    ]);
  }
}
