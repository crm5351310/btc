import { GeneralEducationLevel } from '../../module/category/general-education-level';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class GeneralEducationLevelSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const generalEducationLevelRepository = dataSource.getRepository(GeneralEducationLevel);
    await generalEducationLevelRepository.save([
      KHONG_BIET_CHU,
      LOP_1,
      LOP_2,
      LOP_3,
      LOP_4,
      LOP_5,
      LOP_6,
      LOP_7,
      LOP_8,
      LOP_9,
      LOP_10,
      LOP_11,
      LOP_12,
    ]);
  }
}

export const KHONG_BIET_CHU = {
  id: 1,
  code: 'KHONG_BIET_CHU',
  name: 'Không biết chữ',
  description: '',
};
export const LOP_1 = {
  id: 2,
  code: 'LOP_1',
  name: 'lớp 1',
  description: '',
};
export const LOP_2 = {
  id: 3,
  code: 'LOP_2',
  name: 'lớp 2',
  description: '',
};
export const LOP_3 = {
  id: 4,
  code: 'LOP_3',
  name: 'lớp 3',
  description: '',
};
export const LOP_4 = {
  id: 5,
  code: 'LOP_4',
  name: 'lớp 4',
  description: '',
};
export const LOP_5 = {
  id: 6,
  code: 'LOP_5',
  name: 'lớp 5',
  description: '',
};
export const LOP_6 = {
  id: 7,
  code: 'LOP_6',
  name: 'lớp 6',
  description: '',
};
export const LOP_7 = {
  id: 8,
  code: 'LOP_7',
  name: 'lớp 7',
  description: '',
};
export const LOP_8 = {
  id: 9,
  code: 'LOP_8',
  name: 'lớp 8',
  description: '',
};
export const LOP_9 = {
  id: 10,
  code: 'LOP_9',
  name: 'lớp 9',
  description: '',
};
export const LOP_10 = {
  id: 11,
  code: 'LOP_10',
  name: 'lớp 10',
  description: '',
};
export const LOP_11 = {
  id: 12,
  code: 'LOP_11',
  name: 'lớp 11',
  description: '',
};
export const LOP_12 = {
  id: 13,
  code: 'LOP_12',
  name: 'lớp 12',
  description: '',
};