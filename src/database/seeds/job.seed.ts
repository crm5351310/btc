import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { Job } from '../../module/category/job/job.entity';
export default class JobSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(Job);
    await reporitory.save([
      {
        id: 1,
        code: 'hs',
        name: 'Học sinh',
      },
      {
        id: 2,
        code: 'cn',
        name: 'Công nhân',
      },
      {
        id: 3,
        code: 'ks',
        name: 'Kỹ sư',
      },
      {
        id: 4,
        code: 'lr',
        name: 'Làm ruộng',
      },
      {
        id: 5,
        code: 'gv',
        name: 'Giáo viên',
      },
      {
        id: 6,
        code: 'nvvp',
        name: 'Nhân viên văn phòng',
      },
      {
        id: 7,
        code: 'cc',
        name: 'Công chức',
      },
    ]);
  }
}
