import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { OrganizationGroup } from '../../module/category/organization-root/organization-group/organization-group.entity';

export default class OrganizationGroupSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const organizationGroupRepository = dataSource.getRepository(OrganizationGroup);
    await organizationGroupRepository.save([
      {
        id: 1,
        code: 'co_quan_to_chuc',
        name: 'Cơ quan tổ chức',
      },
      {
        id: 2,
        code: 'doanh_nghiep',
        name: 'Doanh nghiệp',
      },
    ]);
  }
}
