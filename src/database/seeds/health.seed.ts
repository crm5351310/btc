import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { Health } from '../../module/category/health/health.entity';

export default class HealthSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(Health);
    await reporitory.save([
      {
        id: 1,
        code: 'I',
        name: 'Loại I',
      },
      {
        id: 2,
        code: 'II',
        name: 'Loại II',
      },
      {
        id: 3,
        code: 'III',
        name: 'Loại III',
      },
      {
        id: 4,
        code: 'IV',
        name: 'Loại IV',
      },
      {
        id: 5,
        code: 'V',
        name: 'Loại V',
      },
    ]);
  }
}
