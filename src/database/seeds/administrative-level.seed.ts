import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { AdministrativeLevel } from '../../module/category/administrative/administrative-level/administrative-level.entity';

export default class AdministrativeLevelSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const administrativeLevel = await dataSource.getRepository(AdministrativeLevel);
    await administrativeLevel.save([
      TINH_LEVEL,
      HUYEN_LEVEL,
      XA_LEVEL,
    ]);
  }
}

export const TINH_LEVEL = {
  id: 1,
  name: "Tỉnh",
  code: "TINH",
};

export const HUYEN_LEVEL = {
  id: 2,
  name: "Huyện",
  code: "HUYEN",
};

export const XA_LEVEL = {
  id: 3,
  name: "Xã",
  code: "XA",
};