import { Gender } from '../../module/citizen-management/category/gender/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
export const genderData = {
  MALE: { id: 1, code: 'MALE', name: 'Nam' },
  FEMALE: { id: 2, code: 'FEMALE', name: 'Nữ' },
};
export default class GenderSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(Gender);
    await reporitory.save([genderData.MALE, genderData.FEMALE]);
  }
}
