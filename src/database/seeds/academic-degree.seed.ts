import { AcademicDegree } from '../../module/category/academic-degree/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class AcademicDegreeSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(AcademicDegree);
    await reporitory.save([
      { id: 1, code: 'TU_TAI', name: 'Tú tài' },
      { id: 2, code: 'CU_NHAN', name: 'Cử nhân' },
      { id: 3, code: 'KY_SU', name: 'Kỹ sư' },
      { id: 4, code: 'THAC_SI', name: 'Thạc sĩ' },
      { id: 5, code: 'TIEN_SI', name: 'Tiến sĩ' },
    ]);
  }
}
