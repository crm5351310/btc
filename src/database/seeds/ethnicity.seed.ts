import { Ethnicity } from '../../module/category/ethnicity/ethnicity.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export const ethnicityData = {
  KINH: { id: 1, code: 'KINH', name: 'Kinh' },
};
export default class EthnicitySeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(Ethnicity);
    await reporitory.save([
      { id: 1, code: 'KINH', name: 'Kinh' },
      { id: 2, code: 'TAY', name: 'Tày' },
      { id: 3, code: 'THAI', name: 'Thái' },
      { id: 4, code: 'HOA', name: 'Hoa' },
      { id: 5, code: 'KHO_ME', name: 'Khơ-me' },
      { id: 6, code: 'MUONG', name: 'Mường' },
      { id: 7, code: 'NUNG', name: 'Nùng' },
      { id: 8, code: 'HMONG', name: 'HMông' },
      { id: 9, code: 'DAO', name: 'Dao' },
      { id: 10, code: 'GIA_RAI', name: 'Gia-rai' },
      { id: 11, code: 'NGAI', name: 'Ngái' },
      { id: 12, code: 'E_DE', name: 'Ê-đê' },
      { id: 13, code: 'BA_NA', name: 'Ba na' },
      { id: 14, code: 'XO_DANG', name: 'Xơ-Đăng' },
      { id: 15, code: 'SAN_CHAY', name: 'Sán Chay' },
      { id: 16, code: 'CO_HO', name: 'Cơ-ho' },
      { id: 17, code: 'CHAM', name: 'Chăm' },
      { id: 18, code: 'SAN_DIU', name: 'Sán Dìu' },
      { id: 19, code: 'HRE', name: 'Hrê' },
      { id: 20, code: 'MONG', name: 'Mnông' },
      { id: 21, code: 'RA_GLAI', name: 'Ra-glai' },
      { id: 22, code: 'XTIENG', name: 'Xtiêng' },
      { id: 23, code: 'BRU_VAN_KIEU', name: 'Bru-Vân Kiều' },
      { id: 24, code: 'THO', name: 'Thổ' },
      { id: 25, code: 'GIAY', name: 'Giáy' },
      { id: 26, code: 'CO_TU', name: 'Cơ-tu' },
      { id: 27, code: 'GIE_TRIENG', name: 'Gié Triêng' },
      { id: 28, code: 'MA', name: 'Mạ' },
      { id: 29, code: 'KHO_MU', name: 'Khơ-mú' },
      { id: 30, code: 'CO', name: 'Co' },
      { id: 31, code: 'TA_OI', name: 'Tà-ôi' },
      { id: 32, code: 'CHO_RO', name: 'Chơ-ro' },
      { id: 33, code: 'KHANG', name: 'Kháng' },
      { id: 34, code: 'XINH_MUN', name: 'Xinh-mun' },
      { id: 35, code: 'HA_NHI', name: 'Hà Nhì' },
      { id: 36, code: 'CHU_RU', name: 'Chu ru' },
      { id: 37, code: 'LAO', name: 'Lào' },
      { id: 38, code: 'LA_CHI', name: 'La Chí' },
      { id: 39, code: 'LA_HA', name: 'La Ha' },
      { id: 40, code: 'PHU_LA', name: 'Phù Lá' },
      { id: 41, code: 'LA_HU', name: 'La Hủ' },
      { id: 42, code: 'LU', name: 'Lự' },
      { id: 43, code: 'LO_LO', name: 'Lô Lô' },
      { id: 44, code: 'CHUT', name: 'Chứt' },
      { id: 45, code: 'MANG', name: 'Mảng' },
      { id: 46, code: 'PA_THEN', name: 'Pà Thẻn' },
      { id: 47, code: 'CO_LAO', name: 'Co Lao' },
      { id: 48, code: 'CONG', name: 'Cống' },
      { id: 49, code: 'BO_Y', name: 'Bố Y' },
      { id: 50, code: 'SI_LA', name: 'Si La' },
      { id: 51, code: 'PU_PEO', name: 'Pu Péo' },
      { id: 52, code: 'BRAU', name: 'Brâu' },
      { id: 53, code: 'O_DU', name: 'Ơ Đu' },
      { id: 54, code: 'RO_MAM', name: 'Rơ măm' },
    ]);
  }
}
