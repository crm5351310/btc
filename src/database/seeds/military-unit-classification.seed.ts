import { MilitaryUnitClassification } from '../../module/category/military-unit-root/military-unit-classification/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { boCongAn, boQuocPhong } from './ministry.seed';

export default class MilitaryUnitClassificationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(MilitaryUnitClassification);
    await reporitory.save([
      {
        id: 1,
        code: 'CONG_AN_NHAN_DAN',
        name: 'Đơn vị công an nhân dân',
        ministry: boCongAn,
      },
      {
        id: 2,
        code: 'QUAN_KHU',
        name: 'Đơn vị thuộc quân khu',
        ministry: boQuocPhong,
      },
      { id: 3, code: 'TINH', name: 'Đơn vị thuộc tỉnh', ministry: boQuocPhong },
      {
        id: 4,
        code: 'HUYEN',
        name: 'Đơn vị thuộc huyện',
        ministry: boQuocPhong,
      },
    ]);
  }
}
