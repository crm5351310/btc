import { FamilyRelation } from '../../module/category/family-relation/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class FamilyRelationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(FamilyRelation);
    await reporitory.save([
      { id: 1, code: 'ME', name: 'Mẹ', isFreeze: true },
      { id: 2, code: 'CHA', name: 'Cha', isFreeze: true },
      { id: 3, code: 'VO', name: 'Vợ' },
      { id: 4, code: 'CHONG', name: 'Chồng' },
      { id: 5, code: 'ANH', name: 'Anh' },
      { id: 6, code: 'CHI', name: 'Chị' },
      { id: 7, code: 'EM', name: 'Em' },
      { id: 8, code: 'CON_RUOT', name: 'Con ruột' },
      { id: 9, code: 'CON_NUOI', name: 'Con nuôi' },
      { id: 10, code: 'ONG_NOI', name: 'Ông nội' },
      { id: 11, code: 'BA_NOI', name: 'Bà nội' },
      { id: 12, code: 'ONG_NGOAI', name: 'Ông ngoại' },
      { id: 13, code: 'BA_NGOAI', name: 'Bà ngoại' },
    ]);
  }
}
