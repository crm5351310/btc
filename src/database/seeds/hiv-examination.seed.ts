import { HivExamination } from '../../module/citizen-process-management/category/hiv-examination/entities/hiv-examination.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class HivExaminationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(HivExamination);
    await repository.save([
      {
        id: 1,
        code: 'AT',
        name: 'Âm tính',
      },
      {
        id: 2,
        code: 'DT',
        name: 'Dương tính',
      },
      {
        id: 3,
        code: 'KXD',
        name: 'Không xác định',
      },
    ]);
  }
}
