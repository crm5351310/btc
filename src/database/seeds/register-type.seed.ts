import { RegisterType } from '../../module/citizen-process-management/category/register-type/entities/register-type.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class RegisterTypeSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(RegisterType);
    await repository.save([
      {
        id: 1,
        code: 'DKC',
        name: 'Đăng ký chung',
      },
      {
        id: 2,
        code: 'DKR',
        name: 'Đăng ký riêng',
      },
    ]);
  }
}
