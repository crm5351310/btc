import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { AdministrativeTitle } from '../../module/category/administrative/administrative-title/administrative-title.entity';
import { TINH_LEVEL } from './administrative-level.seed';

export default class AdministrativeTitleSeeder implements Seeder {
  public administrativeTitle = {
    TINH: {
      id: 1,
      name: TINH_LEVEL.name,
      Code: TINH_LEVEL.code,
      administrativeLevel: {
        id: 1,
      },
    },
  };
  public async run(dataSource: DataSource): Promise<any> {
    const administrativeTitle = await dataSource.getRepository(AdministrativeTitle);
    await administrativeTitle.save([this.administrativeTitle.TINH]);
  }
}
