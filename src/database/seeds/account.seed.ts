import * as bcrypt from 'bcrypt';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { Account } from '../../module/system/account-root/account/account.entity';
import { AccountPassword } from '../../module/system/account-root/account-password/account-password.entity';

export default class AccountSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const accountRepository = dataSource.getRepository(Account);
    const accountPasswordRepository = dataSource.getRepository(AccountPassword);
    await accountRepository.save([
      {
        id: 1,
        username: 'admin@bitecco.vn',
        fullname: 'admin',
        phoneNumber: '123456789',
        role: {
          id: 1,
        },
        department: {
          id: 1,
        },
      },
    ]);
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash('12345678', salt);
    await accountPasswordRepository.save({
      id: 1,
      account: {
        id: 1,
      },
      password: hash,
    });
  }
}
