import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { AdministrativeUnit } from '../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { AdministrativeTitle } from '../../module/category/administrative/administrative-title/administrative-title.entity';
import { GeoLocation } from '../../module/category/administrative/geo-location';

export default class AdministrativeUnitSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const CT = dataSource.getRepository(AdministrativeUnit);
    const ct = new AdministrativeUnit();
    ct.id = 1;
    ct.name = 'Hậu giang';
    ct.code = '93';
    ct.isSecurity = false;
    ct.isFocalArea = false;

    const title = new AdministrativeTitle();
    title.id = 1;
    const geo = new GeoLocation();
    geo.id = 1;
    ct.administrativeTitle = title;
    ct.geolocation = geo;

    const root = await CT.findOne({
      where: {
        id: 1
      }
    })
    if(!root){
      await CT.save(ct);
    }

    const huyenViThanh = new AdministrativeUnit();
    huyenViThanh.id = 2;
    huyenViThanh.name = 'Thành phố Vị Thanh';
    huyenViThanh.code = '930';
    huyenViThanh.isSecurity = false;
    huyenViThanh.isFocalArea = false;
    huyenViThanh.parent = ct;

    const huyenNgaBay = new AdministrativeUnit();
    huyenNgaBay.id = 3;
    huyenNgaBay.name = 'Thành phố Ngã Bảy';
    huyenNgaBay.code = '931';
    huyenNgaBay.isSecurity = false;
    huyenNgaBay.isFocalArea = false;
    huyenNgaBay.parent = ct;

    const huyenChauThanhA = new AdministrativeUnit();
    huyenChauThanhA.id = 4;
    huyenChauThanhA.name = 'Huyện Châu Thành A';
    huyenChauThanhA.code = '932';
    huyenChauThanhA.isSecurity = false;
    huyenChauThanhA.isFocalArea = false;
    huyenChauThanhA.parent = ct;

    const huyenChauThanh = new AdministrativeUnit();
    huyenChauThanh.id = 5;
    huyenChauThanh.name = 'Huyện Châu Thành';
    huyenChauThanh.code = '933';
    huyenChauThanh.isSecurity = false;
    huyenChauThanh.isFocalArea = false;
    huyenChauThanh.parent = ct;

    const huyenPhungHiep = new AdministrativeUnit();
    huyenPhungHiep.id = 6;
    huyenPhungHiep.name = 'Huyện Phụng Hiệp';
    huyenPhungHiep.code = '934';
    huyenPhungHiep.isSecurity = false;
    huyenPhungHiep.isFocalArea = false;
    huyenPhungHiep.parent = ct;

    const huyenViThuy = new AdministrativeUnit();
    huyenViThuy.id = 7;
    huyenViThuy.name = 'Huyện Vị Thuỷ';
    huyenViThuy.code = '935';
    huyenViThuy.isSecurity = false;
    huyenViThuy.isFocalArea = false;
    huyenViThuy.parent = ct;

    const huyenLongMy = new AdministrativeUnit();
    huyenLongMy.id = 8;
    huyenLongMy.name = 'Huyện Long Mỹ';
    huyenLongMy.code = '936';
    huyenLongMy.isSecurity = false;
    huyenLongMy.isFocalArea = false;
    huyenLongMy.parent = ct;

    const thiXaLongMy = new AdministrativeUnit();
    thiXaLongMy.id = 9;
    thiXaLongMy.name = 'Thị xã Long Mỹ';
    thiXaLongMy.code = '937';
    thiXaLongMy.isSecurity = false;
    thiXaLongMy.isFocalArea = false;
    thiXaLongMy.parent = ct;

    await CT.save([
      huyenViThanh,
      huyenNgaBay,
      huyenChauThanhA,
      huyenChauThanh,
      huyenPhungHiep,
      huyenViThuy,
      huyenLongMy,
      thiXaLongMy,
    ]);

    const phuongI = new AdministrativeUnit();
    phuongI.id = 10;
    phuongI.name = 'Phường I';
    phuongI.code = '31318';
    phuongI.isSecurity = false;
    phuongI.isFocalArea = false;
    phuongI.parent = huyenViThanh;

    const phuongIII = new AdministrativeUnit();
    phuongIII.id = 11;
    phuongIII.name = 'Phường III';
    phuongIII.code = '31321';
    phuongIII.isSecurity = false;
    phuongIII.isFocalArea = false;
    phuongIII.parent = huyenViThanh;

    const phuongIV = new AdministrativeUnit();
    phuongIV.id = 12;
    phuongIV.name = 'Phường IV';
    phuongIV.code = '31324';
    phuongIV.isSecurity = false;
    phuongIV.isFocalArea = false;
    phuongIV.parent = huyenViThanh;

    const phuongV = new AdministrativeUnit();
    phuongV.id = 13;
    phuongV.name = 'Phường V';
    phuongV.code = '31327';
    phuongV.isSecurity = false;
    phuongV.isFocalArea = false;
    phuongV.parent = huyenViThanh;

    const phuongVII = new AdministrativeUnit();
    phuongVII.id = 14;
    phuongVII.name = 'Phường VII';
    phuongVII.code = '31330';
    phuongVII.isSecurity = false;
    phuongVII.isFocalArea = false;
    phuongVII.parent = huyenViThanh;

    const xaViTan = new AdministrativeUnit();
    xaViTan.id = 15;
    xaViTan.name = 'Xã Vị Tân';
    xaViTan.code = '31333';
    xaViTan.isSecurity = false;
    xaViTan.isFocalArea = false;
    xaViTan.parent = huyenViThanh;

    const xaHoaLuu = new AdministrativeUnit();
    xaHoaLuu.id = 16;
    xaHoaLuu.name = 'Xã Hoả Lựu';
    xaHoaLuu.code = '31336';
    xaHoaLuu.isSecurity = false;
    xaHoaLuu.isFocalArea = false;
    xaHoaLuu.parent = huyenViThanh;

    const xaTanTien = new AdministrativeUnit();
    xaTanTien.id = 17;
    xaTanTien.name = 'Xã Tân Tiến';
    xaTanTien.code = '31338';
    xaTanTien.isSecurity = false;
    xaTanTien.isFocalArea = false;
    xaTanTien.parent = huyenViThanh;

    const xaHoaTien = new AdministrativeUnit();
    xaHoaTien.id = 18;
    xaHoaTien.name = 'Xã Hoả Tiến';
    xaHoaTien.code = '31339';
    xaHoaTien.isSecurity = false;
    xaHoaTien.isFocalArea = false;
    xaHoaTien.parent = huyenViThanh;

    await CT.save([
      phuongI,
      phuongIII,
      phuongIV,
      phuongV,
      phuongVII,
      xaViTan,
      xaHoaLuu,
      xaTanTien,
      xaHoaTien,
    ]);

    const phuongNgaBay = new AdministrativeUnit();
    phuongNgaBay.id = 19;
    phuongNgaBay.name = 'Phường Ngã Bảy';
    phuongNgaBay.code = '31340';
    phuongNgaBay.isSecurity = false;
    phuongNgaBay.isFocalArea = false;
    phuongNgaBay.parent = huyenNgaBay;

    const phuongLaiHieu = new AdministrativeUnit();
    phuongLaiHieu.id = 20;
    phuongLaiHieu.name = 'Phường Lái Hiếu';
    phuongLaiHieu.code = '31341';
    phuongLaiHieu.isSecurity = false;
    phuongLaiHieu.isFocalArea = false;
    phuongLaiHieu.parent = huyenNgaBay;

    const phuongHiepThanh = new AdministrativeUnit();
    phuongHiepThanh.id = 21;
    phuongHiepThanh.name = 'Phường Hiệp Thành';
    phuongHiepThanh.code = '31343';
    phuongHiepThanh.isSecurity = false;
    phuongHiepThanh.isFocalArea = false;
    phuongHiepThanh.parent = huyenNgaBay;

    const phuongHiepLoi = new AdministrativeUnit();
    phuongHiepLoi.id = 22;
    phuongHiepLoi.name = 'Phường Hiệp Lợi';
    phuongHiepLoi.code = '31344';
    phuongHiepLoi.isSecurity = false;
    phuongHiepLoi.isFocalArea = false;
    phuongHiepLoi.parent = huyenNgaBay;

    const xaDaiThanh = new AdministrativeUnit();
    xaDaiThanh.id = 23;
    xaDaiThanh.name = 'Xã Đại Thành';
    xaDaiThanh.code = '31411';
    xaDaiThanh.isSecurity = false;
    xaDaiThanh.isFocalArea = false;
    xaDaiThanh.parent = huyenNgaBay;

    const xaTanThanh = new AdministrativeUnit();
    xaTanThanh.id = 24;
    xaTanThanh.name = 'Xã Tân Thành';
    xaTanThanh.code = '31414';
    xaTanThanh.isSecurity = false;
    xaTanThanh.isFocalArea = false;
    xaTanThanh.parent = huyenNgaBay;

    await CT.save([
      phuongNgaBay,
      phuongLaiHieu,
      phuongHiepThanh,
      phuongHiepLoi,
      xaDaiThanh,
      xaTanThanh,
    ]);

    const thiTranThamNgan = new AdministrativeUnit();
    thiTranThamNgan.id = 25;
    thiTranThamNgan.name = 'Thị trấn Một Ngàn';
    thiTranThamNgan.code = '31342';
    thiTranThamNgan.isSecurity = false;
    thiTranThamNgan.isFocalArea = false;
    thiTranThamNgan.parent = huyenChauThanhA;

    const xaTanHoa = new AdministrativeUnit();
    xaTanHoa.id = 26;
    xaTanHoa.name = 'Xã Tân Hoà';
    xaTanHoa.code = '31345';
    xaTanHoa.isSecurity = false;
    xaTanHoa.isFocalArea = false;
    xaTanHoa.parent = huyenChauThanhA;

    const thiTranBayNgan = new AdministrativeUnit();
    thiTranBayNgan.id = 27;
    thiTranBayNgan.name = 'Thị trấn Bảy Ngàn';
    thiTranBayNgan.code = '31346';
    thiTranBayNgan.isSecurity = false;
    thiTranBayNgan.isFocalArea = false;
    thiTranBayNgan.parent = huyenChauThanhA;

    const xaTruongLongTay = new AdministrativeUnit();
    xaTruongLongTay.id = 28;
    xaTruongLongTay.name = 'Xã Trường Long Tây';
    xaTruongLongTay.code = '31348';
    xaTruongLongTay.isSecurity = false;
    xaTruongLongTay.isFocalArea = false;
    xaTruongLongTay.parent = huyenChauThanhA;

    const xaTruongLongA = new AdministrativeUnit();
    xaTruongLongA.id = 29;
    xaTruongLongA.name = 'Xã Trường Long A';
    xaTruongLongA.code = '31351';
    xaTruongLongA.isSecurity = false;
    xaTruongLongA.isFocalArea = false;
    xaTruongLongA.parent = huyenChauThanhA;

    const xaNhonNghiaA = new AdministrativeUnit();
    xaNhonNghiaA.id = 30;
    xaNhonNghiaA.name = 'Xã Nhơn Nghĩa A';
    xaNhonNghiaA.code = '31357';
    xaNhonNghiaA.isSecurity = false;
    xaNhonNghiaA.isFocalArea = false;
    xaNhonNghiaA.parent = huyenChauThanhA;

    const thiTranRachGoi = new AdministrativeUnit();
    thiTranRachGoi.id = 31;
    thiTranRachGoi.name = 'Thị trấn Rạch Gòi';
    thiTranRachGoi.code = '31359';
    thiTranRachGoi.isSecurity = false;
    thiTranRachGoi.isFocalArea = false;
    thiTranRachGoi.parent = huyenChauThanhA;

    const xaThanhXuan = new AdministrativeUnit();
    xaThanhXuan.id = 32;
    xaThanhXuan.name = 'Xã Thạnh Xuân';
    xaThanhXuan.code = '31360';
    xaThanhXuan.isSecurity = false;
    xaThanhXuan.isFocalArea = false;
    xaThanhXuan.parent = huyenChauThanhA;

    const thiTranCaiTac = new AdministrativeUnit();
    thiTranCaiTac.id = 33;
    thiTranCaiTac.name = 'Thị trấn Cái Tắc';
    thiTranCaiTac.code = '31362';
    thiTranCaiTac.isSecurity = false;
    thiTranCaiTac.isFocalArea = false;
    thiTranCaiTac.parent = huyenChauThanhA;

    const xaTanPhuThanh = new AdministrativeUnit();
    xaTanPhuThanh.id = 34;
    xaTanPhuThanh.name = 'Xã Tân Phú Thạnh';
    xaTanPhuThanh.code = '31363';
    xaTanPhuThanh.isSecurity = false;
    xaTanPhuThanh.isFocalArea = false;
    xaTanPhuThanh.parent = huyenChauThanhA;

    await CT.save([
      thiTranThamNgan,
      xaTanHoa,
      thiTranBayNgan,
      xaTruongLongTay,
      xaTruongLongA,
      xaNhonNghiaA,
      thiTranRachGoi,
      xaThanhXuan,
      thiTranCaiTac,
      xaTanPhuThanh,
    ]);

    const thiTranNgaSau = new AdministrativeUnit();
    thiTranNgaSau.id = 35;
    thiTranNgaSau.name = 'Thị Trấn Ngã Sáu';
    thiTranNgaSau.code = '31366';
    thiTranNgaSau.isSecurity = false;
    thiTranNgaSau.isFocalArea = false;
    thiTranNgaSau.parent = huyenChauThanh;

    const xaDongThanh = new AdministrativeUnit();
    xaDongThanh.id = 36;
    xaDongThanh.name = 'Xã Đông Thạnh';
    xaDongThanh.code = '31369';
    xaDongThanh.isSecurity = false;
    xaDongThanh.isFocalArea = false;
    xaDongThanh.parent = huyenChauThanh;

    const xaDongPhu = new AdministrativeUnit();
    xaDongPhu.id = 37;
    xaDongPhu.name = 'Xã Đông Phú';
    xaDongPhu.code = '31375';
    xaDongPhu.isSecurity = false;
    xaDongPhu.isFocalArea = false;
    xaDongPhu.parent = huyenChauThanh;

    const xaPhuHuu = new AdministrativeUnit();
    xaPhuHuu.id = 38;
    xaPhuHuu.name = 'Xã Phú Hữu';
    xaPhuHuu.code = '31378';
    xaPhuHuu.isSecurity = false;
    xaPhuHuu.isFocalArea = false;
    xaPhuHuu.parent = huyenChauThanh;

    const xaPhuTan = new AdministrativeUnit();
    xaPhuTan.id = 39;
    xaPhuTan.name = 'Xã Phú Tân';
    xaPhuTan.code = '31379';
    xaPhuTan.isSecurity = false;
    xaPhuTan.isFocalArea = false;
    xaPhuTan.parent = huyenChauThanh;

    const thiTranMaiDam = new AdministrativeUnit();
    thiTranMaiDam.id = 40;
    thiTranMaiDam.name = 'Thị trấn Mái Dầm';
    thiTranMaiDam.code = '31381';
    thiTranMaiDam.isSecurity = false;
    thiTranMaiDam.isFocalArea = false;
    thiTranMaiDam.parent = huyenChauThanh;

    const xaDongPhuoc = new AdministrativeUnit();
    xaDongPhuoc.id = 41;
    xaDongPhuoc.name = 'Xã Đông Phước';
    xaDongPhuoc.code = '31384';
    xaDongPhuoc.isSecurity = false;
    xaDongPhuoc.isFocalArea = false;
    xaDongPhuoc.parent = huyenChauThanh;

    const xaDongPhuocA = new AdministrativeUnit();
    xaDongPhuocA.id = 42;
    xaDongPhuocA.name = 'Xã Đông Phước A';
    xaDongPhuocA.code = '31387';
    xaDongPhuocA.isSecurity = false;
    xaDongPhuocA.isFocalArea = false;
    xaDongPhuocA.parent = huyenChauThanh;

    await CT.save([
      thiTranNgaSau,
      xaDongThanh,
      xaDongPhu,
      xaPhuHuu,
      xaPhuTan,
      thiTranMaiDam,
      xaDongPhuoc,
      xaDongPhuocA,
    ]);

    const thiTranKinhCung = new AdministrativeUnit();
    thiTranKinhCung.id = 43;
    thiTranKinhCung.name = 'Thị trấn Kinh Cùng';
    thiTranKinhCung.code = '31393';
    thiTranKinhCung.isSecurity = false;
    thiTranKinhCung.isFocalArea = false;
    thiTranKinhCung.parent = huyenPhungHiep;

    const thiTranCayDuong = new AdministrativeUnit();
    thiTranCayDuong.id = 44;
    thiTranCayDuong.name = 'Thị trấn Cây Dương';
    thiTranCayDuong.code = '31396';
    thiTranCayDuong.isSecurity = false;
    thiTranCayDuong.isFocalArea = false;
    thiTranCayDuong.parent = huyenPhungHiep;

    const xaTanBinh = new AdministrativeUnit();
    xaTanBinh.id = 45;
    xaTanBinh.name = 'Xã Tân Bình';
    xaTanBinh.code = '31399';
    xaTanBinh.isSecurity = false;
    xaTanBinh.isFocalArea = false;
    xaTanBinh.parent = huyenPhungHiep;

    const xaBinhThanh = new AdministrativeUnit();
    xaBinhThanh.id = 46;
    xaBinhThanh.name = 'Xã Bình Thành';
    xaBinhThanh.code = '31402';
    xaBinhThanh.isSecurity = false;
    xaBinhThanh.isFocalArea = false;
    xaBinhThanh.parent = huyenPhungHiep;

    const xaThanhHoa = new AdministrativeUnit();
    xaThanhHoa.id = 47;
    xaThanhHoa.name = 'Xã Thạnh Hòa';
    xaThanhHoa.code = '31405';
    xaThanhHoa.isSecurity = false;
    xaThanhHoa.isFocalArea = false;
    xaThanhHoa.parent = huyenPhungHiep;

    const xaLongThanh = new AdministrativeUnit();
    xaLongThanh.id = 48;
    xaLongThanh.name = 'Xã Long Thạnh';
    xaLongThanh.code = '31408';
    xaLongThanh.isSecurity = false;
    xaLongThanh.isFocalArea = false;
    xaLongThanh.parent = huyenPhungHiep;

    const xaPhungHiep = new AdministrativeUnit();
    xaPhungHiep.id = 49;
    xaPhungHiep.name = 'Xã Phụng Hiệp';
    xaPhungHiep.code = '31417';
    xaPhungHiep.isSecurity = false;
    xaPhungHiep.isFocalArea = false;
    xaPhungHiep.parent = huyenPhungHiep;

    const xaHoaMy = new AdministrativeUnit();
    xaHoaMy.id = 50;
    xaHoaMy.name = 'Xã Hòa Mỹ';
    xaHoaMy.code = '31420';
    xaHoaMy.isSecurity = false;
    xaHoaMy.isFocalArea = false;
    xaHoaMy.parent = huyenPhungHiep;

    const xaHoaAn = new AdministrativeUnit();
    xaHoaAn.id = 51;
    xaHoaAn.name = 'Xã Hòa An';
    xaHoaAn.code = '31423';
    xaHoaAn.isSecurity = false;
    xaHoaAn.isFocalArea = false;
    xaHoaAn.parent = huyenPhungHiep;

    const xaPhuongBinh = new AdministrativeUnit();
    xaPhuongBinh.id = 52;
    xaPhuongBinh.name = 'Xã Phương Bình';
    xaPhuongBinh.code = '31426';
    xaPhuongBinh.isSecurity = false;
    xaPhuongBinh.isFocalArea = false;
    xaPhuongBinh.parent = huyenPhungHiep;

    const xaHiepHung = new AdministrativeUnit();
    xaHiepHung.id = 53;
    xaHiepHung.name = 'Xã Hiệp Hưng';
    xaHiepHung.code = '31429';
    xaHiepHung.isSecurity = false;
    xaHiepHung.isFocalArea = false;
    xaHiepHung.parent = huyenPhungHiep;

    const xaTanPhuocHung = new AdministrativeUnit();
    xaTanPhuocHung.id = 54;
    xaTanPhuocHung.name = 'Xã Tân Phước Hưng';
    xaTanPhuocHung.code = '31432';
    xaTanPhuocHung.isSecurity = false;
    xaTanPhuocHung.isFocalArea = false;
    xaTanPhuocHung.parent = huyenPhungHiep;

    const thiTranBungTau = new AdministrativeUnit();
    thiTranBungTau.id = 55;
    thiTranBungTau.name = 'Thị trấn Búng Tàu';
    thiTranBungTau.code = '31433';
    thiTranBungTau.isSecurity = false;
    thiTranBungTau.isFocalArea = false;
    thiTranBungTau.parent = huyenPhungHiep;

    const xaPhuongPhu = new AdministrativeUnit();
    xaPhuongPhu.id = 56;
    xaPhuongPhu.name = 'Xã Phương Phú';
    xaPhuongPhu.code = '31435';
    xaPhuongPhu.isSecurity = false;
    xaPhuongPhu.isFocalArea = false;
    xaPhuongPhu.parent = huyenPhungHiep;

    const xaTanLong = new AdministrativeUnit();
    xaTanLong.id = 57;
    xaTanLong.name = 'Xã Tân Long';
    xaTanLong.code = '31438';
    xaTanLong.isSecurity = false;
    xaTanLong.isFocalArea = false;
    xaTanLong.parent = huyenPhungHiep;

    await CT.save([
      thiTranKinhCung,
      thiTranCayDuong,
      xaTanBinh,
      xaThanhHoa,
      xaLongThanh,
      xaPhungHiep,
      xaHoaMy,
      xaHoaAn,
      xaPhuongBinh,
      xaHiepHung,
      xaTanPhuocHung,
      thiTranBungTau,
      xaPhuongPhu,
      xaTanLong,
    ]);

    const thiTranNangMau = new AdministrativeUnit();
    thiTranNangMau.id = 58;
    thiTranNangMau.name = 'Thị trấn Nàng Mau';
    thiTranNangMau.code = '31441';
    thiTranNangMau.isSecurity = false;
    thiTranNangMau.isFocalArea = false;
    thiTranNangMau.parent = huyenViThuy;

    const xaViTrung = new AdministrativeUnit();
    xaViTrung.id = 59;
    xaViTrung.name = 'Xã Vị Trung';
    xaViTrung.code = '31444';
    xaViTrung.isSecurity = false;
    xaViTrung.isFocalArea = false;
    xaViTrung.parent = huyenViThuy;

    const xaViThuy = new AdministrativeUnit();
    xaViThuy.id = 60;
    xaViThuy.name = 'Xã Vị Thuỷ';
    xaViThuy.code = '31447';
    xaViThuy.isSecurity = false;
    xaViThuy.isFocalArea = false;
    xaViThuy.parent = huyenViThuy;

    const xaViNang = new AdministrativeUnit();
    xaViNang.id = 61;
    xaViNang.name = 'Xã Vị Thắng';
    xaViNang.code = '31450';
    xaViNang.isSecurity = false;
    xaViNang.isFocalArea = false;
    xaViNang.parent = huyenViThuy;

    const xaVinhThuanTay = new AdministrativeUnit();
    xaVinhThuanTay.id = 62;
    xaVinhThuanTay.name = 'Xã Vĩnh Thuận Tây';
    xaVinhThuanTay.code = '31453';
    xaVinhThuanTay.isSecurity = false;
    xaVinhThuanTay.isFocalArea = false;
    xaVinhThuanTay.parent = huyenViThuy;

    const xaVinhTrung = new AdministrativeUnit();
    xaVinhTrung.id = 63;
    xaVinhTrung.name = 'Xã Vĩnh Trung';
    xaVinhTrung.code = '31456';
    xaVinhTrung.isSecurity = false;
    xaVinhTrung.isFocalArea = false;
    xaVinhTrung.parent = huyenViThuy;

    const xaVinhTuong = new AdministrativeUnit();
    xaVinhTuong.id = 64;
    xaVinhTuong.name = 'Xã Vĩnh Tường';
    xaVinhTuong.code = '31459';
    xaVinhTuong.isSecurity = false;
    xaVinhTuong.isFocalArea = false;
    xaVinhTuong.parent = huyenViThuy;

    const xaViDong = new AdministrativeUnit();
    xaViDong.id = 65;
    xaViDong.name = 'Xã Vị Đông';
    xaViDong.code = '31462';
    xaViDong.isSecurity = false;
    xaViDong.isFocalArea = false;
    xaViDong.parent = huyenViThuy;

    const xaViThanh = new AdministrativeUnit();
    xaViThanh.id = 66;
    xaViThanh.name = 'Xã Vị Thanh';
    xaViThanh.code = '31465';
    xaViThanh.isSecurity = false;
    xaViThanh.isFocalArea = false;
    xaViThanh.parent = huyenViThuy;

    const xaViBinh = new AdministrativeUnit();
    xaViBinh.id = 67;
    xaViBinh.name = 'Xã Vị Bình';
    xaViBinh.code = '31468';
    xaViBinh.isSecurity = false;
    xaViBinh.isFocalArea = false;
    xaViBinh.parent = huyenViThuy;

    await CT.save([
      thiTranNangMau,
      xaViTrung,
      xaViThuy,
      xaViNang,
      xaVinhThuanTay,
      xaVinhTrung,
      xaViDong,
      xaViThanh,
      xaViBinh,
    ]);

    const xaThuanHung = new AdministrativeUnit();
    xaThuanHung.id = 68;
    xaThuanHung.name = 'Xã Thuận Hưng';
    xaThuanHung.code = '31483';
    xaThuanHung.isSecurity = false;
    xaThuanHung.isFocalArea = false;
    xaThuanHung.parent = huyenLongMy;

    const xaThuanHoa = new AdministrativeUnit();
    xaThuanHoa.id = 69;
    xaThuanHoa.name = 'Xã Thuận Hòa';
    xaThuanHoa.code = '31484';
    xaThuanHoa.isSecurity = false;
    xaThuanHoa.isFocalArea = false;
    xaThuanHoa.parent = huyenLongMy;

    const xaVinhThuanDong = new AdministrativeUnit();
    xaVinhThuanDong.id = 70;
    xaVinhThuanDong.name = 'Xã Vĩnh Thuận Đông';
    xaVinhThuanDong.code = '31486';
    xaVinhThuanDong.isSecurity = false;
    xaVinhThuanDong.isFocalArea = false;
    xaVinhThuanDong.parent = huyenLongMy;

    const thiTranVinhVien = new AdministrativeUnit();
    thiTranVinhVien.id = 71;
    thiTranVinhVien.name = 'Thị trấn Vĩnh Viễn';
    thiTranVinhVien.code = '31489';
    thiTranVinhVien.isSecurity = false;
    thiTranVinhVien.isFocalArea = false;
    thiTranVinhVien.parent = huyenLongMy;

    const xaVinhVienA = new AdministrativeUnit();
    xaVinhVienA.id = 72;
    xaVinhVienA.name = 'Xã Vĩnh Viễn A';
    xaVinhVienA.code = '31490';
    xaVinhVienA.isSecurity = false;
    xaVinhVienA.isFocalArea = false;
    xaVinhVienA.parent = huyenLongMy;

    const xaLuongTam = new AdministrativeUnit();
    xaLuongTam.id = 73;
    xaLuongTam.name = 'Xã Lương Tâm';
    xaLuongTam.code = '31492';
    xaLuongTam.isSecurity = false;
    xaLuongTam.isFocalArea = false;
    xaLuongTam.parent = huyenLongMy;

    const xaLuongNghia = new AdministrativeUnit();
    xaLuongNghia.id = 74;
    xaLuongNghia.name = 'Xã Lương Nghĩa';
    xaLuongNghia.code = '31493';
    xaLuongNghia.isSecurity = false;
    xaLuongNghia.isFocalArea = false;
    xaLuongNghia.parent = huyenLongMy;

    const xaXaPhien = new AdministrativeUnit();
    xaXaPhien.id = 75;
    xaXaPhien.name = 'Xã Xà Phiên';
    xaXaPhien.code = '31495';
    xaXaPhien.isSecurity = false;
    xaXaPhien.isFocalArea = false;
    xaXaPhien.parent = huyenLongMy;

    await CT.save([xaThuanHung, xaThuanHoa, xaVinhThuanDong, thiTranVinhVien, xaVinhVienA, xaLuongTam, xaLuongNghia, xaXaPhien]);

    const phuongThuanAn = new AdministrativeUnit();
    phuongThuanAn.id = 76;
    phuongThuanAn.name = 'Phường Thuận An';
    phuongThuanAn.code = '31471';
    phuongThuanAn.isSecurity = false;
    phuongThuanAn.isFocalArea = false;
    phuongThuanAn.parent = thiXaLongMy;

    const phuongTraLong = new AdministrativeUnit();
    phuongTraLong.id = 77;
    phuongTraLong.name = 'Phường Trà Lồng';
    phuongTraLong.code = '31472';
    phuongTraLong.isSecurity = false;
    phuongTraLong.isFocalArea = false;
    phuongTraLong.parent = thiXaLongMy;

    const phuongBinhThanh = new AdministrativeUnit();
    phuongBinhThanh.id = 78;
    phuongBinhThanh.name = 'Phường Bình Thạnh';
    phuongBinhThanh.code = '31473';
    phuongBinhThanh.isSecurity = false;
    phuongBinhThanh.isFocalArea = false;
    phuongBinhThanh.parent = thiXaLongMy;

    const xaLongBinh = new AdministrativeUnit();
    xaLongBinh.id = 79;
    xaLongBinh.name = 'Xã Long Bình';
    xaLongBinh.code = '31474';
    xaLongBinh.isSecurity = false;
    xaLongBinh.isFocalArea = false;
    xaLongBinh.parent = thiXaLongMy;

    const phuongVinhTuong = new AdministrativeUnit();
    phuongVinhTuong.id = 80;
    phuongVinhTuong.name = 'Phường Vĩnh Tường';
    phuongVinhTuong.code = '31475';
    phuongVinhTuong.isSecurity = false;
    phuongVinhTuong.isFocalArea = false;
    phuongVinhTuong.parent = thiXaLongMy;

    const xaLongTri = new AdministrativeUnit();
    xaLongTri.id = 81;
    xaLongTri.name = 'Xã Long Trị';
    xaLongTri.code = '31477';
    xaLongTri.isSecurity = false;
    xaLongTri.isFocalArea = false;
    xaLongTri.parent = thiXaLongMy;

    const xaLongTriA = new AdministrativeUnit();
    xaLongTriA.id = 82;
    xaLongTriA.name = 'Xã Long Trị A';
    xaLongTriA.code = '31478';
    xaLongTriA.isSecurity = false;
    xaLongTriA.isFocalArea = false;
    xaLongTriA.parent = thiXaLongMy;

    const xaLongPhu = new AdministrativeUnit();
    xaLongPhu.id = 83;
    xaLongPhu.name = 'Xã Long Phú';
    xaLongPhu.code = '31480';
    xaLongPhu.isSecurity = false;
    xaLongPhu.isFocalArea = false;
    xaLongPhu.parent = thiXaLongMy;

    const xaTanPhu = new AdministrativeUnit();
    xaTanPhu.id = 84;
    xaTanPhu.name = 'Xã Tân Phú';
    xaTanPhu.code = '31481';
    xaTanPhu.isSecurity = false;
    xaTanPhu.isFocalArea = false;
    xaTanPhu.parent = thiXaLongMy;

    await CT.save([phuongThuanAn, phuongTraLong, phuongBinhThanh, xaLongBinh, phuongVinhTuong, xaLongTri, xaLongTriA, xaLongPhu, xaTanPhu]);
  }
}
