import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { PermissionRole } from '../../module/system/permission-role/permission-role.entity';

export default class PermissionRoleSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(PermissionRole);
    let index = 1;
    const qtht: any[] = [];
    let ids: number[] = [];
    let total: number = 1;
    // QTHT
    for (let i = 1; i < 429; i++) {
      qtht.push({
        id: i,
        role: {
          id: 1,
        },
        permission: {
          id: i,
        },
        isCheck: 1,
      });
      index++;
    }

    // TBQL vs CVBQL
    total = index + 1;
    ids = [
      2, 114, 115, 116, 117, 118, 3, 4, 375, 376, 377, 378, 5, 139, 140, 141, 142, 6, 143, 144, 145,
      146, 7, 379, 380, 381, 8, 382, 383, 384, 9, 385, 386, 387, 10, 403, 404, 28, 29, 30, 417, 418,
      31, 394, 395, 396, 32, 419, 420, 33, 397, 398, 399, 34, 421, 422, 35, 423, 424, 36, 37, 175,
      176, 177, 178, 38, 400, 401, 402, 39, 134, 135, 136, 137, 138, 40, 41, 179, 180, 181, 182, 42,
      183, 184, 185, 186, 43, 44, 187, 188, 189, 190, 45, 191, 192, 193, 194, 46, 425, 426, 47, 407,
      408, 59, 60, 61, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241,
      242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260,
      261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279,
      280, 281, 282, 283, 284, 285, 286, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 81,
      82, 83, 92, 93, 94, 95, 96, 97, 98, 99, 100, 102, 303, 304, 305, 306, 307, 308, 309, 310, 335,
      336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 119, 120, 121, 122,
      123, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 124, 125, 126, 127, 128, 363,
      364, 365, 366,
    ];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 2,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    total = index + 1;
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 3,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // TBDQTV vs CVBDQTV
    total = index + 1;
    ids = [
      2, 114, 115, 116, 117, 118, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
      27, 147, 148, 149, 150, 411, 412, 413, 414, 388, 389, 390, 415, 416, 391, 392, 393, 427, 428,
      429, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168,
      169, 170, 171, 172, 173, 174, 405, 406, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297,
      298, 299, 300, 301, 302, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 92, 93,
      94, 95, 96, 97, 98, 99, 100, 102, 303, 304, 305, 306, 307, 308, 309, 310, 335, 336, 337, 338,
      339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 119, 120, 121, 122, 123, 351, 352,
      353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 124, 125, 126, 127, 128, 363, 364, 365, 366,
      59
    ];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 4,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    total = index + 1;
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 5,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // CBKT
    total = index + 1;
    ids = [
      53, 57, 219, 220, 221, 222, 59, 88, 89, 90, 91, 323, 324, 325, 326, 327, 328, 329, 330, 331,
      332, 333, 334,
    ];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 6,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // CBGDQP
    total = index + 1;
    ids = [
      48, 49, 50, 51, 52, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
      210, 59, 84, 85, 86, 87, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 101, 129,
      130, 131, 132, 133,
    ];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 7,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // CBVT
    total = index + 1;
    ids = [53, 58, 223, 224, 225, 226];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 8,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // NVTH
    total = index + 1;
    ids = [53, 54, 55, 56, 211, 212, 213, 214, 215, 216, 217, 218];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 9,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // Ban CHQSH
    total = index + 1;
    ids = [
      2, 114, 115, 116, 117, 118, 3, 4, 375, 376, 377, 378, 5, 139, 140, 141, 142, 6, 143, 144, 145,
      146, 7, 379, 380, 381, 8, 382, 383, 384, 9, 385, 386, 387, 59, 97, 351, 352, 353, 354,
    ];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 10,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }

    // Ban CHQSX
    total = index + 1;
    ids = [
      2, 114, 115, 116, 117, 118, 59, 97, 351, 352, 353, 354, 3, 4, 375, 376, 377, 378, 7, 379, 380,
      381,
    ];
    for (let i = total; i < ids.length + total; i++) {
      qtht.push({
        id: i,
        role: {
          id: 11,
        },
        permission: {
          id: ids[i - total],
        },
        isCheck: 1,
      });
      index++;
    }
    await reporitory.save([...qtht]);
  }
}
