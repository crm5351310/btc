import { BusinessType } from '../../module/citizen-management/category/business-type/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export const businessTypeData = {
  QUOC_PHONG: { id: 1, code: 'QUOC_PHONG', name: 'CQ-DN quốc phòng' },
  CO_QUAN: { id: 2, code: 'CO_QUAN', name: 'Cơ quan' },
  DOANH_NGHIEP: { id: 3, code: 'DOANH_NGHIEP', name: 'Doanh nghiệp' },
};
export default class BusinessTypeSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(BusinessType);
    await reporitory.save([
      businessTypeData.QUOC_PHONG,
      businessTypeData.CO_QUAN,
      businessTypeData.DOANH_NGHIEP,
    ]);
  }
}
