import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { Talent } from '../../module/category/talent/talent.entity';

export default class TalentSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const religionRepository = dataSource.getRepository(Talent);
    await religionRepository.save([
      {
        id: 1,
        code: 'HOI_HOA',
        name: 'Hội hoạ',
      },
      {
        id: 2,
        code: 'KE_CHUYEN',
        name: 'Kể chuyện',
      },
      {
        id: 3,
        code: 'GUITAR',
        name: 'Guitar',
      },
      {
        id: 4,
        code: 'CA_HAT',
        name: 'Ca hát',
      },
    ]);
  }
}
