import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { JobPosition } from '../../module/category/jop-position/entities';
export const jobPositionData = {
  CONG_AN_XA: {
    id: 1,
    code: 'CONG_AN_XA',
    name: 'Công an xã',
  },
};
export default class JobPositionSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(JobPosition);

    await reporitory.save([jobPositionData.CONG_AN_XA]);
  }
}
