import { PersonalClass } from '../../module/category/personal-class/personal-class.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export const personalClassData = {
  CONG_NHAN: {
    id: 1,
    code: 'CONG_NHAN',
    name: 'Công nhân',
  },
  NONG_DAN: {
    id: 2,
    code: 'NONG_DAN',
    name: 'Nông dân',
  },
  NHAN_VIEN: {
    id: 3,
    code: 'NHAN_VIEN',
    name: 'Nhân viên',
  },
  HOC_SINH: {
    id: 4,
    code: 'HOC_SINH',
    name: 'Học sinh',
  },
  SINH_VIEN: {
    id: 5,
    code: 'SINH_VIEN',
    name: 'Sinh viên',
  },
  CONG_CHUC: {
    id: 6,
    code: 'CONG_CHUC',
    name: 'Công chức',
  },
  VIEN_CHUC: {
    id: 7,
    code: 'VIEN_CHUC',
    name: 'Viên chức',
  },
};
export default class PersonalClassSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const personalClassRepository = dataSource.getRepository(PersonalClass);
    await personalClassRepository.save([
      personalClassData.CONG_NHAN,
      personalClassData.NONG_DAN,
      personalClassData.NHAN_VIEN,
      personalClassData.HOC_SINH,
      personalClassData.SINH_VIEN,
      personalClassData.CONG_CHUC,
      personalClassData.VIEN_CHUC,
    ]);
  }
}
