import { PartyMemberRanking } from '../../module/citizen-management/category/party-member-ranking/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class PartyMemberRankingSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(PartyMemberRanking);
    await reporitory.save([
      { id: 1, code: 'EXCELLENT', name: 'Hoàn thành xuất sắc' },
      { id: 2, code: 'GOOD', name: 'Hoàn thành tốt' },
      { id: 3, code: 'PASS', name: 'Hoàn thành' },
      { id: 4, code: 'NOT_PASS', name: 'Không hoàn thành' },
    ]);
  }
}
