import { CourseClassification } from '../../module/category/course-classification/entities/course-classification.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class CourseClassificationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(CourseClassification);
    await reporitory.save([
      { id: 1, code: 'TT-BQP', name: 'đào tạo theo thông tin 38/TT-BQP' },
      { id: 2, code: 'QUANDOI', name: 'đào tạo quân đội' },
      { id: 3, code: 'CONGAN', name: 'đào tạo công an' },
      { id: 4, code: 'TAICU', name: 'bồi dưỡng tái cử' },
    ]);
  }
}
