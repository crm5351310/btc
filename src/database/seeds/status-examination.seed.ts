import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { StatusExamination } from '../../module/citizen-process-management/category/status-examination/entities/status-examination.entity';

export default class StatusExaminationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(StatusExamination);
    await repository.save([
      {
        id: 1,
        code: 'VANG',
        name: 'Vắng',
      },
      {
        id: 2,
        code: 'CHONG',
        name: 'Chống',
      },
      {
        id: 3,
        code: 'TRON',
        name: 'Trốn',
      },
      {
        id: 4,
        code: 'DAT',
        name: 'Đạt',
      },
      {
        id: 5,
        code: 'KHONG_DAT',
        name: 'Không đạt',
      },
    ]);
  }
}
