import { ProcessGroup } from '../../module/citizen-process-management/category/process-group/entities/process-group.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class ProcessGroupSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(ProcessGroup);
    await repository.save([NVQS_PROCESS_GROUP, DQTV_PROCESS_GROUP, DBDV_PROCESS_GROUP]);
  }
}

export const NVQS_PROCESS_GROUP = {
  id: 1,
  code: 'NVQS',
  name: 'NVQS',
};

export const DBDV_PROCESS_GROUP = {
  id: 2,
  code: 'DBDV',
  name: 'DBDV',
};

export const DQTV_PROCESS_GROUP = {
  id: 3,
  code: 'DQTV',
  name: 'DQTV',
};
