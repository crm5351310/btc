import { Role } from '../../module/system/role/role.entity';
import { RoleCode } from '../../common/constant/role.constant';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class RoleSeeder implements Seeder {
  public roleData = {
    QUAN_TRI_HE_THONG: {
      id: 1,
      code: RoleCode.QUAN_TRI_HE_THONG,
      name: 'Quản trị hệ thống',
      isFreeze: true,
      description:
        'Quản lý danh sách tài khoản và phân quyền người sử dụng, cấu hình hệ thống, sao lưu dữ liệu.',
    },
    TRUONG_BAN_QUAN_LUC: {
      id: 2,
      code: RoleCode.TRUONG_BAN_QUAN_LUC,
      name: 'Trưởng Ban Quân Lực',
      isFreeze: true,
      description: 'Quản lý thông tin công dân, tuyển quân NVQS, công tác dự bị động viên',
    },
    CHUYEN_VIEN_BAN_QUAN_LUC: {
      id: 3,
      code: RoleCode.CHUYEN_VIEN_BAN_QUAN_LUC,
      name: 'Chuyên viên Ban Quân Lực',
      isFreeze: true,
      description:
        'Thực hiện quản lý thông tin công dân, tuyển quân NVQS, thực hiện công tác dự bị động viên',
    },
    TRUONG_BAN_DAN_QUAN_TU_VE: {
      id: 4,
      code: RoleCode.TRUONG_BAN_DAN_QUAN_TU_VE,
      name: 'Trưởng Ban Dân quân tự vệ',
      isFreeze: true,
      description:
        'Quản lý thông tin công dân, Quản lý công tác Dân quân tự vệ, Quốc phòng an ninh',
    },
    CHUYEN_VIEN_BAN_DAN_QUAN_TU_VE: {
      id: 5,
      code: RoleCode.CHUYEN_VIEN_BAN_DAN_QUAN_TU_VE,
      name: 'Chuyên viên Ban Dân quân tự vệ',
      isFreeze: true,
      description:
        'Thực hiện quản lý thông tin công dân, thực hiện công tác Dân quân tự vệ, Quốc phòng an ninh',
    },
    CAN_BO_KHEN_THUONG: {
      id: 6,
      code: RoleCode.CAN_BO_KHEN_THUONG,
      name: 'Cán bộ khen thưởng',
      isFreeze: true,
      description: 'Quản lý và thực hiện khen thưởng',
    },
    CAN_BO_GIAO_DUC_QUOC_PHONG: {
      id: 7,
      code: RoleCode.CAN_BO_GIAO_DUC_QUOC_PHONG,
      name: 'Cán bộ giáo dục quốc phòng',
      isFreeze: true,
      description: 'Quản lý và thực hiện giáo dục Quốc phòng an ninh',
    },
    CAN_BO_VAN_THU: {
      id: 8,
      code: RoleCode.CAN_BO_VAN_THU,
      name: 'Cán bộ văn thư',
      isFreeze: true,
      description: 'Quản lý và thực hiện quản lý văn bản',
    },
    NHAN_VIEN_TONG_HOP: {
      id: 9,
      code: RoleCode.NHAN_VIEN_TONG_HOP,
      name: 'Nhân viên tổng hợp',
      isFreeze: true,
      description: 'Quản lý và thực hiện công tác quản lý cán bộ và đào tạo cán bộ thuộc BCHQS',
    },
    BAN_CHI_HUY_QUAN_SU_HUYEN: {
      id: 10,
      code: RoleCode.BAN_CHI_HUY_QUAN_SU_HUYEN,
      name: 'Ban Chỉ huy quân sự cấp Huyện',
      isFreeze: true,
      description:
        'Thực hiện khởi tạo thông tin công dân, thực hiện công tác gọi và tuyển chọn công dân nhập ngũ, thực hiện cập nhật kết quả khám nghĩa vụ quân sự, thực hiện cập nhật thông tin công dân liên quan đến các quá trình Dự bị động viên, Tuyển quân NVQS của công dân thuộc Huyện',
    },
    BAN_CHI_HUY_QUAN_SU_XA: {
      id: 11,
      code: RoleCode.BAN_CHI_HUY_QUAN_SU_XA,
      name: 'Ban Chỉ huy quân sự cấp Xã',
      isFreeze: true,
      description:
        'Thực hiện khởi tạo thông tin công dân, thực hiện công tác gọi và tuyển chọn công dân nhập ngũ, thực hiện cập nhật kết quả khám nghĩa vụ quân sự, thực hiện cập nhật thông tin công dân liên quan đến các quá trình Dự bị động viên, Tuyển quân NVQS của công dân thuộc Xã',
    },
  };
  public async run(dataSource: DataSource): Promise<any> {
    const roleRepository = dataSource.getRepository(Role);
    await roleRepository.save([
      this.roleData.QUAN_TRI_HE_THONG,
      this.roleData.TRUONG_BAN_QUAN_LUC,
      this.roleData.CHUYEN_VIEN_BAN_QUAN_LUC,
      this.roleData.TRUONG_BAN_DAN_QUAN_TU_VE,
      this.roleData.CHUYEN_VIEN_BAN_DAN_QUAN_TU_VE,
      this.roleData.CAN_BO_KHEN_THUONG,
      this.roleData.CAN_BO_GIAO_DUC_QUOC_PHONG,
      this.roleData.CAN_BO_VAN_THU,
      this.roleData.NHAN_VIEN_TONG_HOP,
      this.roleData.BAN_CHI_HUY_QUAN_SU_HUYEN,
      this.roleData.BAN_CHI_HUY_QUAN_SU_XA,
    ]);
  }
}
