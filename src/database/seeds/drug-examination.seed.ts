import { DrugExamination } from '../../module/citizen-process-management/category/drug-examination/entities/drug-examination.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class DrugExaminationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(DrugExamination);
    await repository.save([
      {
        id: 1,
        code: 'AT',
        name: 'Âm tính',
      },
      {
        id: 2,
        code: 'DT',
        name: 'Dương tính',
      },
      {
        id: 3,
        code: 'KXD',
        name: 'Không xác định',
      },
    ]);
  }
}
