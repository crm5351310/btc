import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { CommuneTypeConst } from '../../common/constant/commune-type.constant';
import { CommuneType } from '../../module/category/administrative/administrative-unit-root/commune-root/commune-type/commune-type.entity';

export default class CommuneTypeSeeder implements Seeder {
  public communeType = {
    LOAI_1: {
      id: 1,
      name: CommuneTypeConst.LOAI_1,
      code: Object.keys(CommuneTypeConst)[0],
      isDefault: true,
    },
    LOAI_2: {
      id: 2,
      name: CommuneTypeConst.LOAI_2,
      code: Object.keys(CommuneTypeConst)[1],
      isDefault: true,
    },
    LOAI_3: {
      id: 3,
      name: CommuneTypeConst.LOAI_3,
      code: Object.keys(CommuneTypeConst)[2],
      isDefault: true,
    },
  };
  public async run(dataSource: DataSource): Promise<any> {
    const CT = dataSource.getRepository(CommuneType);
    await CT.save([this.communeType.LOAI_1, this.communeType.LOAI_2, this.communeType.LOAI_3]);
  }
}
