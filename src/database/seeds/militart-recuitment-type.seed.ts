import { MilitaryRecruitmentType } from '../../module/citizen-process-management/category/military-recruitment-type/entities/military-recruitment-type.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class MilitaryRecruitmentTypeSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(MilitaryRecruitmentType);
    await repository.save([
      {
        id: 1,
        code: 'NVQS',
        name: 'Nghĩa vụ quân sự',
      },
      {
        id: 2,
        code: 'NVCA',
        name: 'Nghĩa vụ công an',
      },
    ]);
  }
}
