import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { FamilyClass } from '../../module/category/family-class/family-class.entity';

export default class FamilyClassSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(FamilyClass);
    await reporitory.save([
      {
        id: 1,
        code: 'BAN_NONG',
        name: 'Bần nông',
      },
      {
        id: 2,
        code: 'CO_NONG',
        name: 'Cố nông',
      },
      {
        id: 3,
        code: 'THANG_NONG',
        name: 'Thặng nông',
      },
      {
        id: 4,
        code: 'CONG_CHUC',
        name: 'Công chức',
      },
      {
        id: 5,
        code: 'VIEN_CHUC',
        name: 'Viên chức',
      },
      {
        id: 6,
        code: 'TIEU_THUONG',
        name: 'Tiểu thương',
      },
      {
        id: 7,
        code: 'TIEU_CHU',
        name: 'TIểu chủ',
      },
      {
        id: 8,
        code: 'TU_SAN',
        name: 'Tư sản',
      },
      {
        id: 9,
        code: 'TIEU_TU_SAN',
        name: 'TIểu tư sản',
      },
    ]);
  }
}
