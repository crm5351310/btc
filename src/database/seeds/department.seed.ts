import { Department } from '../../module/system/department-root/department/department.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class DepartmentSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const departmentRepository = dataSource.getRepository(Department);
    await departmentRepository.save({
      id: 1,
      code: 'ROOT',
      name: 'BCHQS tỉnh Hậu Giang',
    });
  }
}
