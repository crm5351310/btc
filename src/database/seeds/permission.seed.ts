import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { Permission } from '../../module/system/permission/permission.entity';

export default class PermissionSeeder implements Seeder {
  public permission: any[] = [
    {
      id: 1,
      name: 'Root',
      code: 'root',
      url: '',
      isMenu: false,
    },
    {
      id: 2,
      name: 'Thông tin công dân',
      code: 'thong_tin_cong_dan',
      url: 'citizen-information',
      isMenu: true,
      parent: {
        id: 1,
      },
    },
    {
      id: 3,
      name: 'Tuyển quân NVQS',
      code: 'tuyen_quan_nvqs',
      url: '',
      isMenu: true,
      parent: {
        id: 1,
      },
    },
    {
      id: 4,
      name: 'Công dân sẵn sàng nhập ngũ',
      code: 'cong_dan_san_sang_nhap_ngu',
      url: 'military-service-management/ready-enlistment',
      isMenu: true,
      parent: {
        id: 3,
      },
    },
    {
      id: 5,
      name: 'Đợt tuyển quân',
      code: 'dot_tuyen_quan',
      url: 'military-service-management/military-recruitment-period',
      isMenu: true,
      parent: {
        id: 3,
      },
    },
    {
      id: 6,
      name: 'Chỉ tiêu quân',
      code: 'chi_tieu_quan',
      url: 'military-service-management/military-recruitment-target',
      parent: {
        id: 3,
      },
      isMenu: true,
    },
    {
      id: 7,
      name: 'Xét duyệt cấp xã',
      code: 'xet_duyet_cap_xa',
      url: 'military-service-management/military-pre-recruitment',
      parent: {
        id: 3,
      },
      isMenu: true,
    },
    {
      id: 8,
      name: 'Xét duyệt cấp huyện',
      code: 'xet_duyet_cap_huyen',
      url: 'military-service-management/military-recruitment',
      parent: {
        id: 3,
      },
      isMenu: true,
    },
    {
      id: 9,
      name: 'Danh sách công dân nhập ngũ',
      code: 'danh_sach_cong_dan_nhap_ngu',
      url: 'military-service-management/enlistment',
      parent: {
        id: 3,
      },
      isMenu: true,
    },
    {
      id: 10,
      name: 'Báo cáo tuyển quân NVQS',
      code: 'bao_cao_tuyen_quan_nvqs',
      url: 'military-service-management/military-recruitment-report',
      parent: {
        id: 3,
      },
      isMenu: true,
    },
    {
      id: 11,
      name: 'Dân quân tự vệ',
      code: 'dan_quan_tu_ve',
      url: '',
      parent: {
        id: 1,
      },
      isMenu: true,
    },
    {
      id: 12,
      name: 'Danh sách dân quân tự vệ',
      code: 'danh_sach_dan_quan_tu_ve',
      url: '',
      parent: {
        id: 11,
      },
      isMenu: true,
    },
    {
      id: 13,
      name: 'Kế hoạch giao chỉ tiêu luân phiên',
      code: 'ke_hoach_giao_chi_tieu_luan_phien',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 14,
      name: 'Nguồn DQTV',
      code: 'nguon_dqtv',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 15,
      name: 'Danh sách DQTV xây dựng hàng năm',
      code: 'danh_sach_dqtv_xd_hang_nam',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 16,
      name: 'Danh sách DQTV đã biên chế',
      code: 'danh_sach_dqtv_da_bien_che',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 17,
      name: 'Danh sách tự vệ',
      code: 'danh_sach_tu_ve',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 18,
      name: 'Tra cứu thời gian tham gia DQTV',
      code: 'tra_cuu_thoi_gian_DQTC',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 19,
      name: 'Danh sách công dân sắp quá tuổi DQTV',
      code: 'dan_quan_tu_ve',
      url: '',
      parent: {
        id: 12,
      },
      isMenu: true,
    },
    {
      id: 20,
      name: 'Công tác dân quân tự vệ',
      code: 'cong_tac_dan_quan_tu_ve',
      url: '',
      parent: {
        id: 11,
      },
      isMenu: true,
    },
    {
      id: 21,
      name: 'Huấn luyện',
      code: 'huan_luyen',
      url: '',
      parent: {
        id: 20,
      },
      isMenu: true,
    },
    {
      id: 22,
      name: 'Tập huấn',
      code: 'tap_huan',
      url: '',
      parent: {
        id: 20,
      },
      isMenu: true,
    },
    {
      id: 23,
      name: 'Bồi dưỡng',
      code: 'boi_duong',
      url: '',
      parent: {
        id: 20,
      },
      isMenu: true,
    },
    {
      id: 24,
      name: 'Đào tạo',
      code: 'dao_tao',
      url: '',
      parent: {
        id: 20,
      },
      isMenu: true,
    },
    {
      id: 25,
      name: 'Diễn tập',
      code: 'dien_tap',
      url: '',
      parent: {
        id: 20,
      },
      isMenu: true,
    },
    {
      id: 26,
      name: 'Hoạt động',
      code: 'hoat_dong',
      url: '',
      parent: {
        id: 20,
      },
      isMenu: true,
    },
    {
      id: 27,
      name: 'Báo cáo dân quân tự vệ',
      code: 'bao_cao_dan_quan_tu_ve',
      url: '',
      parent: {
        id: 11,
      },
      isMenu: true,
    },
    {
      id: 28,
      name: 'Dự bị động viên',
      code: 'du_bi_dong_vien',
      url: '',
      parent: {
        id: 1,
      },
      isMenu: true,
    },
    {
      id: 29,
      name: 'Đăng ký quân nhân dự bị',
      code: 'dang_ky_quan_nhan_du_bi',
      url: '',
      parent: {
        id: 28,
      },
      isMenu: true,
    },
    {
      id: 30,
      name: 'Công dân đủ điều kiện dự bị động viên hạng 2',
      code: 'ddk_dbdv_2',
      url: 'military-reserve-management/military-reserve-registration/eligible-2nd-military-reserve-list',
      parent: {
        id: 29,
      },
      isMenu: true,
    },
    {
      id: 31,
      name: 'Công dân đã đăng ký dự bị động viên hạng 2',
      code: 'dk_dbdv_2',
      url: 'military-reserve-management/military-reserve-registration/registered-2nd-military-reserve-list',
      parent: {
        id: 29,
      },
      isMenu: true,
    },
    {
      id: 32,
      name: 'Công dân đủ điều kiện dự bị động viên hạng 1',
      code: 'dk_dbdv_1',
      url: 'military-reserve-management/military-reserve-registration/eligible-1st-military-reserve-list',
      parent: {
        id: 29,
      },
      isMenu: true,
    },
    {
      id: 33,
      name: 'Công dân đã đăng ký dự bị động viên hạng 1',
      code: 'ddk_dbdv_1',
      url: 'military-reserve-management/military-reserve-registration/registered-1st-military-reserve-list',
      parent: {
        id: 29,
      },
      isMenu: true,
    },
    {
      id: 34,
      name: 'Công dân DBĐV đã qua huấn luyện tập trung',
      code: 'cd_dbdv_hltt',
      url: 'military-reserve-management/military-reserve-registration/trained-military-reserve-list',
      parent: {
        id: 29,
      },
      isMenu: true,
    },
    {
      id: 35,
      name: 'Sỹ quan dự bị',
      code: 'sqdb',
      url: 'military-reserve-management/military-reserve-registration/reserve-officer-list',
      parent: {
        id: 29,
      },
      isMenu: true,
    },
    {
      id: 36,
      name: 'Biên chế quân nhân dự bị',
      code: 'bcqndb',
      url: '',
      parent: {
        id: 28,
      },
      isMenu: true,
    },
    {
      id: 37,
      name: 'Kế hoạch biên chế quân nhân dự bị',
      code: 'khbcqndb',
      url: 'military-service-management/military-reserve-payroll/payroll-plan',
      parent: {
        id: 36,
      },
      isMenu: true,
    },
    {
      id: 38,
      name: 'Danh sách công dân đã biên chế DBĐV',
      code: 'dscddncdndv',
      url: 'military-service-management/military-reserve-payroll/payrolled-military-reserve-list',
      parent: {
        id: 36,
      },
      isMenu: true,
    },
    {
      id: 39,
      name: 'Theo dõi huấn luyện',
      code: 'tdhl',
      url: 'military-service-management/military-reserve-payroll/training-tracking',
      parent: {
        id: 36,
      },
      isMenu: true,
    },
    {
      id: 40,
      name: 'Đăng ký phương tiện kỹ thuật',
      code: 'dkptkt',
      url: '',
      parent: {
        id: 28,
      },
      isMenu: true,
    },
    {
      id: 41,
      name: 'Danh sách PTKT',
      code: 'dsptkt',
      url: 'military-service-management/vehicle-registration/vehicle-list',
      parent: {
        id: 40,
      },
      isMenu: true,
    },
    {
      id: 42,
      name: 'Theo dõi và kiểm tra PTKT',
      code: 'tdvktptkt',
      url: 'military-service-management/vehicle-registration/vehicle-inspection',
      parent: {
        id: 40,
      },
      isMenu: true,
    },
    {
      id: 43,
      name: 'Biên chế phương tiện kỹ thuật',
      code: 'bcptkt',
      url: '',
      parent: {
        id: 28,
      },
      isMenu: true,
    },
    {
      id: 44,
      name: 'Đợt kế hoạch',
      code: 'dkh',
      url: 'military-service-management/vehicle-reserve/planning-period',
      parent: {
        id: 43,
      },
      isMenu: true,
    },
    {
      id: 45,
      name: 'Kế hoạch biên chế PTKT',
      code: 'khbcptkt',
      url: 'military-service-management/vehicle-reserve/payroll-vehicle-plan',
      parent: {
        id: 43,
      },
      isMenu: true,
    },
    {
      id: 46,
      name: 'Kết quả biên chế PTKT',
      code: 'kqbcptkt',
      url: 'military-service-management/vehicle-reserve/payrolled-vehicle',
      parent: {
        id: 43,
      },
      isMenu: true,
    },
    {
      id: 47,
      name: 'Báo cáo dự bị động viên',
      code: 'bcdbdv',
      url: '',
      parent: {
        id: 28,
      },
      isMenu: true,
    },
    {
      id: 48,
      name: 'Giáo dục QPAN',
      code: 'gdqpan',
      url: '',
      parent: {
        id: 1,
      },
      isMenu: true,
    },
    {
      id: 49,
      name: 'Trường, đơn vị tham gia',
      code: 'tdvtg',
      url: '',
      parent: {
        id: 48,
      },
      isMenu: true,
    },
    {
      id: 50,
      name: 'Giáo dục quốc phòng an ninh',
      code: 'gdqpan',
      url: '',
      parent: {
        id: 48,
      },
      isMenu: true,
    },
    {
      id: 51,
      name: 'Giáo dục quốc phòng an ninh toàn dân',
      code: 'gdqpan',
      url: '',
      parent: {
        id: 48,
      },
      isMenu: true,
    },
    {
      id: 52,
      name: 'Giáo viên giáo dục quốc phòng',
      code: 'gvgdqp',
      url: '',
      parent: {
        id: 48,
      },
      isMenu: true,
    },
    {
      id: 53,
      name: 'Công tác cán bộ',
      code: 'ctcbbchqs',
      url: '',
      parent: {
        id: 1,
      },
      isMenu: true,
    },
    {
      id: 54,
      name: 'Quản lý và đào tạo cán bộ',
      code: 'qlcbvdtcb',
      url: '',
      parent: {
        id: 53,
      },
      isMenu: true,
    },
    {
      id: 55,
      name: 'Danh sách cán bộ tham gia',
      code: 'dscbtg',
      url: '',
      parent: {
        id: 54,
      },
      isMenu: true,
    },
    {
      id: 56,
      name: 'Đào tạo cán bộ',
      code: 'dtcb',
      url: '',
      parent: {
        id: 54,
      },
      isMenu: true,
    },
    {
      id: 57,
      name: 'Quản lý khen thưởng',
      code: 'qlkt',
      url: '',
      parent: {
        id: 53,
      },
      isMenu: true,
    },
    {
      id: 58,
      name: 'Quản lý văn bản',
      code: 'qlvb',
      url: '',
      parent: {
        id: 53,
      },
      isMenu: true,
    },
    {
      id: 59,
      name: 'Danh mục',
      code: 'dm',
      url: '',
      parent: {
        id: 1,
      },
      isMenu: true,
    },
    {
      id: 60,
      name: 'Thông tin công dân',
      code: 'ttcd',
      url: '',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 61,
      name: 'Cấp hành chính',
      code: 'chc',
      url: 'categories-management/citizen-information/administrative-level',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 62,
      name: 'Dân tộc',
      code: 'dt',
      url: 'categories-management/citizen-information/ethnicity',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 63,
      name: 'Sức khỏe',
      code: 'sk',
      url: 'categories-management/citizen-information/health',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 64,
      name: 'Thành phần bản thân',
      code: 'tpbt',
      url: 'categories-management/citizen-information/personal-class',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 65,
      name: 'Thành phần gia đình',
      code: 'tpgd',
      url: 'categories-management/citizen-information/family-class',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 66,
      name: 'Học vị',
      code: 'hv',
      url: 'categories-management/citizen-information/academic-degree',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 67,
      name: 'Năng khiếu',
      code: 'nk',
      url: 'categories-management/citizen-information/talent',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 68,
      name: 'Ngành nghề CMKT',
      code: 'nncmck',
      url: 'categories-management/citizen-information/job-specialization-group',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 69,
      name: 'Nghề nghiệp',
      code: 'nn',
      url: 'categories-management/citizen-information/job',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 70,
      name: 'Quan hệ',
      code: 'qh',
      url: 'categories-management/citizen-information/family-relation',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 71,
      name: 'Tôn giáo',
      code: 'tg',
      url: 'categories-management/citizen-information/religion',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 72,
      name: 'Trình độ học vấn',
      code: 'tdhv',
      url: 'categories-management/citizen-information/education-level',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 73,
      name: 'Trình độ văn hóa',
      code: 'tdvh',
      url: 'categories-management/citizen-information/general-education-level',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 74,
      name: 'Lý luận chính trị',
      code: 'llct',
      url: 'categories-management/citizen-information/political-theory',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 75,
      name: 'Chức vụ',
      code: 'cv',
      url: 'categories-management/citizen-information/military-position',
      parent: {
        id: 60,
      },
      isMenu: true,
    },
    {
      id: 76,
      name: 'Dân quân tự vệ',
      code: 'dqtv',
      url: '',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 77,
      name: 'Hoạt động DQTV',
      code: 'hddqtv',
      url: 'categories-management/militia-and-self-defence/militia-and-self-defence-activity',
      parent: {
        id: 76,
      },
      isMenu: true,
    },
    {
      id: 78,
      name: 'Đơn vị DQTV',
      code: 'dvdqtv',
      url: 'categories-management/militia-and-self-defence/militia-and-self-defence-unit',
      parent: {
        id: 76,
      },
      isMenu: true,
    },
    {
      id: 79,
      name: 'Lực lượng',
      code: 'll',
      url: 'categories-management/militia-and-self-defence/force',
      parent: {
        id: 76,
      },
      isMenu: true,
    },
    {
      id: 80,
      name: 'Loại DQTV',
      code: 'ldqtv',
      url: 'categories-management/militia-and-self-defence/militia-and-self-defence-type',
      parent: {
        id: 76,
      },
      isMenu: true,
    },
    {
      id: 81,
      name: 'Dự bị động viên',
      code: 'dbdv',
      url: '',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 82,
      name: 'Loại phương tiện',
      code: 'lpt',
      url: 'categories-management/reserve-mobilization-force/vehicle',
      parent: {
        id: 81,
      },
      isMenu: true,
    },
    {
      id: 83,
      name: 'Chuyên nghiệp quân sự',
      code: 'cnqs',
      url: 'categories-management/reserve-mobilization-force/military-job',
      parent: {
        id: 81,
      },
      isMenu: true,
    },
    {
      id: 84,
      name: 'Giáo dục QPAN',
      code: 'gdqpan',
      url: '',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 85,
      name: 'Đối tượng QPAN',
      code: 'dtqpan',
      url: 'categories-management/defense-security-education/defense-security-trainee',
      parent: {
        id: 84,
      },
      isMenu: true,
    },
    {
      id: 86,
      name: 'Phân loại lớp học',
      code: 'pllh',
      url: 'categories-management/defense-security-education/course-classification',
      parent: {
        id: 84,
      },
      isMenu: true,
    },
    {
      id: 87,
      name: 'Phân loại đối tượng bồi dưỡng kiến thức QPAN',
      code: 'pldtbdktqpan',
      url: 'categories-management/defense-security-education/defense-security-education-object',
      parent: {
        id: 84,
      },
      isMenu: true,
    },
    {
      id: 88,
      name: 'Khen thưởng',
      code: 'kt',
      url: '',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 89,
      name: 'Cấp khen thưởng',
      code: 'ckt',
      url: 'categories-management/award/award-level',
      parent: {
        id: 88,
      },
      isMenu: true,
    },
    {
      id: 90,
      name: 'Danh hiệu thi đua - Hình thức khen thưởng',
      code: 'dhtdhtkt',
      url: 'categories-management/award/emulation-title',
      parent: {
        id: 88,
      },
      isMenu: true,
    },
    {
      id: 91,
      name: 'Xếp loại - khen thưởng',
      code: 'xlkt',
      url: 'categories-management/award/rank-award',
      parent: {
        id: 88,
      },
      isMenu: true,
    },
    {
      id: 92,
      name: 'Lý do',
      code: 'ld',
      url: 'categories-management/reason',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 93,
      name: 'Phân loại trọng điểm',
      code: 'pltdvqpan',
      url: 'categories-management/focal-area-classification',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 94,
      name: 'Quân hàm',
      code: 'qh',
      url: 'categories-management/military-rank',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 95,
      name: 'Quy mô tổ chức đơn vị quân đội',
      code: 'qmtcdbqd',
      url: 'categories-management/organization-scale',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 96,
      name: 'Tổ chức xã hội, doanh nghiệp',
      code: 'dstcxhdn',
      url: 'categories-management/organization',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 97,
      name: 'Tỉnh huyện xã',
      code: 'thx',
      url: 'categories-management/administrative-unit',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 98,
      name: 'Kết quả đánh giá',
      code: 'kqdg',
      url: 'categories-management/result',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 99,
      name: 'Hình thức đào tạo',
      code: 'htdt',
      url: 'categories-management/training-method',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 100,
      name: 'Đơn vị quân đội',
      code: 'dvqd',
      url: 'categories-management/military-unit',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 101,
      name: 'Trường học',
      code: 'th',
      url: 'categories-management/school',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 102,
      name: 'Chức danh',
      code: 'cd',
      url: 'categories-management/job-title',
      parent: {
        id: 59,
      },
      isMenu: true,
    },
    {
      id: 103,
      name: 'Quản trị hệ thống',
      code: 'qtht',
      url: '',
      parent: {
        id: 1,
      },
      isMenu: true,
    },
    {
      id: 104,
      name: 'Đơn vị quản lý',
      code: 'qtht',
      url: 'system-management/department',
      parent: {
        id: 103,
      },
      isMenu: true,
    },
    {
      id: 105,
      name: 'Quyền sử dụng',
      code: 'qsd',
      url: 'system-management/role-permission',
      parent: {
        id: 103,
      },
      isMenu: true,
    },
    {
      id: 106,
      name: 'Quản lý tài khoản',
      code: 'qltk',
      url: 'system-management/account',
      parent: {
        id: 103,
      },
      isMenu: true,
    },
    {
      id: 107,
      name: 'Nhật ký hệ thống',
      code: 'nkht',
      url: 'system-management/history',
      parent: {
        id: 103,
      },
      isMenu: true,
    },
  ];

  public action = [
    {
      id: 105,
      name: 'Xem',
      code: 'view',
      url: '',
      isMenu: false,
      parent: {
        id: 2,
      },
    },
    {
      id: 105,
      name: 'Thêm',
      code: 'create',
      url: '',
      isMenu: false,
      parent: {
        id: 2,
      },
    },
    {
      id: 105,
      name: 'Sửa',
      code: 'update',
      url: '',
      isMenu: false,
      parent: {
        id: 2,
      },
    },
    {
      id: 105,
      name: 'Xoá',
      code: 'delete',
      url: '',
      isMenu: false,
      parent: {
        id: 2,
      },
    },
    {
      id: 105,
      name: 'Tải lên',
      code: 'import',
      url: '',
      isMenu: false,
      parent: {
        id: 2,
      },
    },
    {
      id: 105,
      name: 'Tải xuống',
      code: 'export',
      url: '',
      isMenu: false,
      parent: {
        id: 2,
      },
    },
  ];
  public async run(dataSource: DataSource): Promise<any> {
    const permissionRepository = dataSource.getRepository(Permission);

    // crudei
    const permissions_a = [104];

    // crudi
    const permissions_b = [2, 96, 100, 101];
    const action_b = [this.action[0], this.action[1], this.action[2], this.action[3], this.action[5]];

    // crude
    const permissions_c = [39];
    const action_c = [this.action[0], this.action[1], this.action[2], this.action[3], this.action[4]];

    // crud
    const permissions_d = [5, 6, 13, 21, 22, 23, 24, 25, 26, 37, 41, 42, 44, 45, 49, 50, 51, 52, 55, 56, 57, 58, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 77, 78, 79, 80, 82, 83, 85, 86, 87, 89, 90, 91, 92, 93, 94, 95, 97, 98, 99, 102, 105, 106];
    const action_d = [this.action[0], this.action[1], this.action[2], this.action[3]];

    // crui
    const permissions_e = [4];
    const action_e = [this.action[0], this.action[1], this.action[2], this.action[4]];

    // cru
    const permissions_f = [7, 8, 9];
    const action_f = [this.action[0], this.action[1], this.action[2]];

    // rue
    const permissions_g = [16, 18, 31, 33, 38];
    const action_g = [this.action[0], this.action[2], this.action[5]];

    // re
    const permissions_h = [10, 27, 47, 107];
    const action_h = [this.action[0], this.action[5]];

    // ru
    const permissions_i = [14, 15, 17, 30, 32, 34, 35, 46];
    const action_i = [this.action[0], this.action[2]];


    //out rule

    // rue
    const permissions_k = [19];
    const action_k = [this.action[0], this.action[2], this.action[5]];

    let index = this.permission.length + 1;

    index = this.dub(permissions_a, this.action, index);
    index = this.dub(permissions_b, action_b, index);
    index = this.dub(permissions_c, action_c, index);
    index = this.dub(permissions_d, action_d, index);
    index = this.dub(permissions_e, action_e, index);
    index = this.dub(permissions_f, action_f, index);
    index = this.dub(permissions_g, action_g, index);
    index = this.dub(permissions_h, action_h, index);
    index = this.dub(permissions_i, action_i, index);
    this.dub(permissions_k, action_k, index);

    for (const i in this.permission) {
      await permissionRepository.save(this.permission[i]);
    }
  }

  public dub(array: any[], actionArray: any[], index: number) {
    for (let i = 0; i < array.length; i++) {
      for (let j = 0; j < actionArray.length; j++) {
        this.permission.push({
          id: index,
          name: actionArray[j].name,
          code: actionArray[j].code,
          url: actionArray[j].url,
          isMenu: false,
          parent: {
            id: array[i],
          },
        });
        index++;
      }
    }
    return index;
  }
}
