import { GroupProcess } from '../../module/category/group-process/entities/group-process.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class GroupProcessSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(GroupProcess);
    await reporitory.save([
      { id: 1, code: 'DQTV', name: 'Dân quân tự vệ' },
      { id: 2, code: 'DBDV', name: 'Dự bị động viên' },
      { id: 3, code: 'NVQS', name: 'Nghĩa vụ quân sự' },
    ]);
  }
}
