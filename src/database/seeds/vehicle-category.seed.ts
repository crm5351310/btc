import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { VehicleCategory } from '../../module/category/vehicle-root/vehicle-category/vehicle-category.entity';

export default class VehicleCategorySeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const vehicleRepository = dataSource.getRepository(VehicleCategory);
    await vehicleRepository.save([
      {
        id: 1,
        code: 'bt',
        name: 'Xe bán tải',
        vehicleGroup: {
          id: 1,
        },
      },
      {
        id: 2,
        code: 'c',
        name: 'Xe con',
        vehicleGroup: {
          id: 1,
        },
      },
      {
        id: 3,
        code: 'vt',
        name: 'Xe vận tải',
        vehicleGroup: {
          id: 1,
        },
      },
      {
        id: 4,
        code: 'nl',
        name: 'Xe chở nhiên liệu',
        vehicleGroup: {
          id: 1,
        },
      },
      {
        id: 5,
        code: 'k',
        name: 'Xe khách',
        vehicleGroup: {
          id: 1,
        },
      },
      {
        id: 6,
        code: 'ct',
        name: 'Xe cứu thương',
        vehicleGroup: {
          id: 1,
        },
      },
      {
        id: 7,
        code: 't',
        name: 'Tàu',
        vehicleGroup: {
          id: 2,
        },
      },
      {
        id: 8,
        code: 'cn',
        name: 'Ca nô',
        vehicleGroup: {
          id: 2,
        },
      },
      {
        id: 9,
        code: 'cn',
        name: 'Xuồng máy các loại',
        vehicleGroup: {
          id: 2,
        },
      },
      {
        id: 10,
        code: 'tk',
        name: 'Tàu kéo, đẩy công từ 90CV trở lên',
        vehicleGroup: {
          id: 2,
        },
      },
      {
        id: 11,
        code: 'mx',
        name: 'Máy xúc, máy san gạt',
        vehicleGroup: {
          id: 3,
        },
      },
      {
        id: 12,
        code: 'otc',
        name: 'Xe ô tô cẩu',
        vehicleGroup: {
          id: 4,
        },
      },
      {
        id: 13,
        code: 'tt',
        name: 'Thông tin',
        vehicleGroup: {
          id: 5,
        },
      },
      {
        id: 14,
        code: 'vtyt',
        name: 'Vật tư y tế',
        vehicleGroup: {
          id: 5,
        },
      },
      {
        id: 15,
        code: 'xch',
        name: 'Xe cứu hoả',
        vehicleGroup: {
          id: 5,
        },
      },
    ]);
  }
}
