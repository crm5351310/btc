import { MilitaryStatus } from '../../module/citizen-process-management/category/military-status/entities/military-status.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class MilitaryStatusSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(MilitaryStatus);
    await repository.save([
      {
        id: 1,
        code: 'DUNG',
        name: 'Đúng',
      },
      {
        id: 2,
        code: 'GAN_DUNG',
        name: 'Gần đúng',
      },
      {
        id: 3,
        code: 'KHONG_DUNG',
        name: 'Không đúng',
      },
    ]);
  }
}
