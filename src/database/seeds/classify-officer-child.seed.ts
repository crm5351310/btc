import { ClassifyOfficerChild } from '../../module/citizen-management/category/classify-officer-child/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class ClassifyOfficerChildSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(ClassifyOfficerChild);
    await reporitory.save([
      { id: 1, code: 'TINH', name: 'Tỉnh' },
      { id: 2, code: 'HUYEN', name: 'Huyện' },
      { id: 3, code: 'XA', name: 'Xã' },
      { id: 4, code: 'QUAN_DOI', name: 'Quân đội' },
      { id: 5, code: 'CONG_AN', name: 'Công an' },
    ]);
  }
}
