import { Result } from '../../module/category/result/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class ResultSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(Result);
    await reporitory.save([
      { id: 1, code: 'XUAT_SAC', name: 'Kết quả xuất sắc' },
      { id: 2, code: 'GIOI', name: 'Giỏi' },
      { id: 3, code: 'KHA', name: 'Khá' },
      { id: 4, code: 'DAT', name: 'Đạt' },
      { id: 5, code: 'KHONG_DAT', name: 'Không đạt' },
      { id: 6, code: 'TRUNG_BINH', name: 'Trung bình' },
      { id: 7, code: 'YEU', name: 'Yếu' },
    ]);
  }
}
