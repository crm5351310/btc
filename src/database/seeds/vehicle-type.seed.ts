import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { VehicleType } from '../../module/category/vehicle-root/vehicle-type/vehicle-type.entity';

export default class VehicleTypeSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const vehicleRepository = dataSource.getRepository(VehicleType);
    await vehicleRepository.save([
      {
        id: 1,
        code: '305',
        name: 'Xe tải 3,5 tấn',
        vehicleCategory: {
          id: 3,
        },
      },
      {
        id: 2,
        code: '10',
        name: 'Xe tải 10 tấn',
        vehicleCategory: {
          id: 3,
        },
      },
      {
        id: 3,
        code: '29',
        name: 'Xe 29 chỗ',
        vehicleCategory: {
          id: 5,
        },
      },
      {
        id: 4,
        code: '45',
        name: 'Xe 45 chỗ',
        vehicleCategory: {
          id: 5,
        },
      },
    ]);
  }
}
