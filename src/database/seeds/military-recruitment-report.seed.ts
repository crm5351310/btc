import { MilitaryRecruitmentReport } from '../../module/militaty-recruitment/military-recruitment-report/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class MilitaryRecruitmentReportSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(MilitaryRecruitmentReport);
    await reporitory.save([
      { 
        id: 1,
        code: '01/GNN-2016',
        name: 'Danh sách công dân nam đủ 17 tuổi trong năm',
        url: '01-GNN-2016', 
      },
      {
        id: 2,
        code: '01B/GNN-2016',
        name: 'Báo cáo kết quả đăng ký nghĩa vụ quân sự cho công dân nam đủ 17 tuổi',
        url: '01B-GNN-2016',
      },
      { 
        id: 3, 
        code: '06/GNN-2016', 
        name: 'Báo cáo số lượng công dân nam trong lứa tuổi nhập ngũ',
        url: '06-GNN-2016',
      },
      {
        id: 4,
        code: '06A/GNN-2016',
        name: 'Số lượng công dân nam trong độ tuổi nhập ngũ có chuyên môn kỹ thuật nghiệp vụ đang công tác tại các cơ quan, tổ chức',
        url: '06A-GNN-2016',
      },
      {
        id: 5,
        code: '06B/GNN-2016',
        name: 'Số lượng công dân trong độ tuổi nhập ngũ đang học các cơ sở giáo dục nghề nghiệp chính quy có trình độ đào tạo cao đẳng, đại học sẽ ra trường năm sau',
        url: '06B-GNN-2016',
      },
      {
        id: 6,
        code: 'TQ_01',
        name: 'Báo cáo kết quả xét duyệt nghĩa vụ quân sự lần 1 công tác tuyển chọn và gọi công dân nhập ngũ',
        url: 'TQ-01',
      },
      {
        id: 7,
        code: 'TQ_02',
        name: 'Báo cáo kết quả sơ tuyển sức khỏe nghĩa vụ quân sự công tác tuyển chọn và gọi công dân nhập ngũ',
        url: 'TQ-02'
      },
      {
        id: 8,
        code: 'TQ_03',
        name: 'Báo cáo kết quả bình nghị lần 1 công tác tuyển chọn và gọi công dân nhập ngũ',
        url: 'TQ-03'
      },
      {
        id: 9,
        code: 'TQ_04',
        name: 'Báo cáo kết quả khám sức khỏe nghĩa vụ quân sự (Quân đội + Công an)',
        url: 'TQ-04'
      },
      { id: 10,
        code: 'TQ_05',
        name: 'Báo cáo kết quả khám sức khỏe nghĩa vụ quân sự (Quân đội)',
        url: 'TQ-05'
      },
      {
        id: 11,
        code: 'TQ_06',
        name: 'Báo cáo kết quả bình nghị lần 2 công tác tuyển chọn và gọi công dân nhập ngũ',
        url: 'TQ-06'
      },
      {
        id: 12,
        code: 'TQ_07',
        name: 'Báo cáo kết quả xét duyệt nghĩa vụ quân sự lần 2 công tác tuyển chọn và gọi công dân nhập ngũ',
        url: 'TQ-07'
      },
      {
        id: 13,
        code: 'TQ_08',
        name: 'Báo cáo kết quả chốt sổ phát lệnh gọi công dân nhập ngũ (Quân đội + Công an)',
        url: 'TQ-08'
      },
      { 
        id: 14,
        code: 'TQ_09', 
        name: 'Báo cáo nhanh kết quả giao quân',
        url: 'TQ-09'
      },
      { 
        id: 15, 
        code: '12/GNN-2016', 
        name: 'Báo cáo kết quả các bước gọi công dân nam nhập ngũ',
        url: '12-GNN-2016'
      },
      { 
        id: 16, 
        code: '12B/GNN-2016', 
        name: 'Báo cáo số lượng giao quân từng đơn vị',
        url: '12B-GNN-2016'
      },
      { 
        id: 17, 
        code: '13/GNN-2016', 
        name: 'Báo cáo chất lượng giao nhận quân' ,
        url: '13-GNN-2016'
      },
      {
        id: 18,
        code: '14/GNN-2016',
        name: 'Báo cáo số lượng công dân có trình độ chuyên môn kỹ thuật trước khi nhập ngũ',
        url: '14-GNN-2016'
      },
      {
        id: 19,
        code: '15A/QL',
        name: 'Danh sách chiến sĩ mới có trình độ đại học, cao đẳng, trung cấp nhập ngũ',
        url: '15A-QL'
      },
    ]);
  }
}
