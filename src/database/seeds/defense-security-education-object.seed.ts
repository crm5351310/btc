import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { DefenseSecurityEducationObject } from '../../module/category/defense-security-education-object/entities/defense-security-education-object.entity';

export default class DefenseSecurityEducationObjectSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(DefenseSecurityEducationObject);
    await reporitory.save([
      { id: 1, code: 'HOCSINH', name: 'Học sinh' },
      { id: 2, code: 'SINHVIEN', name: 'Sinh viên' },
      { id: 3, code: 'TTTTINH', name: 'trường trực thuộc tỉnh' },
      { id: 4, code: 'GDTD', name: 'giáo dục toàn dân' },
      {
        id: 5,
        code: 'BOIDUONG',
        name: 'bồi dưỡng theo đối tượng được bồi dưỡng',
      },
      { id: 6, code: 'TAICU', name: 'BD ĐT tái cử' },
    ]);
  }
}
