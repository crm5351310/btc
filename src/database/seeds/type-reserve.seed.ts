import { TypeReserve } from '../../module/category/military-job-root/type-reserve/entities/type-reserve.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
export default class TypeReserveSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(TypeReserve);
    await reporitory.save([
      {
        id: 1,
        code: 'quan_nhan_du_bi',
        name: 'Quân nhân dự bị',
      },
      {
        id: 2,
        code: 'si_quan_du_bi',
        name: 'Sĩ quan dự bị',
      },
    ]);
  }
}
