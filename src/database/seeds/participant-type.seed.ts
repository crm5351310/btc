import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { ParticipantType } from '../../module/citizen-process-management/category/participant-type/entities/participant-type.entity';

export default class ParticipantTypeSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(ParticipantType);
    await repository.save([
      {
        id: 1,
        code: 'DAN_QUAN',
        name: 'Dân quân',
      },
      {
        id: 2,
        code: 'TU_VE',
        name: 'Tự vệ',
      },
    ]);
  }
}
