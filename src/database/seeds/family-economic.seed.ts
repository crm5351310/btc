import { FamilyEconomic } from '../../module/citizen-management/category/family-economic/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class FamilyEconomicSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(FamilyEconomic);
    await reporitory.save([
      { id: 1, code: 'NGHEO', name: 'Nghèo' },
      { id: 2, code: 'CAN_NGHEO', name: 'Cận nghèo' },
    ]);
  }
}
