import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { SchoolType } from '../../module/category/school-root/school-type/entities/school-type.entity';

export default class SchoolTypeSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const religionRepository = dataSource.getRepository(SchoolType);
    await religionRepository.save([
      {
        id: 1,
        code: 'TH',
        name: 'Tiểu học',
      },
      {
        id: 2,
        code: 'THCS',
        name: 'Trung học cơ sở',
      },
      {
        id: 3,
        code: 'THPT',
        name: 'Trung học phổ thông',
      },
      {
        id: 4,
        code: 'TC',
        name: 'Trung cấp',
      },
      {
        id: 5,
        code: 'CD',
        name: 'Cao đẳng',
      },

      {
        id: 6,
        code: 'DH',
        name: 'Đại học',
      },
    ]);
  }
}
