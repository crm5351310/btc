import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { OrganizationType } from '../../module/category/organization-root/organization-type/organization-type.entity';

export default class OrganizationTypeSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const organizationTypeRepository = dataSource.getRepository(OrganizationType);
    await organizationTypeRepository.save([
      {
        id: 1,
        name: 'Thuộc Bộ, ngành TW',
        code: 'tbntw',
        organizationGroup: {
          id: 1,
        },
      },
      {
        id: 2,
        name: 'Thuộc cấp tỉnh',
        code: 'tct',
        organizationGroup: {
          id: 1,
        },
      },
      {
        id: 3,
        name: 'Thuộc cấp huyện',
        code: 'tch',
        organizationGroup: {
          id: 1,
        },
      },
      {
        id: 4,
        name: 'DN 100% vốn nhà nước',
        code: 'dnvnn',
        organizationGroup: {
          id: 2,
        },
      },
      {
        id: 5,
        name: 'DNNN nắm giữ cổ phần chi phối',
        code: 'tct',
        organizationGroup: {
          id: 2,
        },
      },
      {
        id: 6,
        name: 'DN tư nhân',
        code: 'dntn',
        organizationGroup: {
          id: 2,
        },
      },
      {
        id: 7,
        name: 'Doanh nghiệp FDI',
        code: 'fdi',
        organizationGroup: {
          id: 2,
        },
      },
      {
        id: 8,
        name: 'DN quân đội của quân khu, địa phương quản lý',
        code: 'qd1',
        organizationGroup: {
          id: 2,
        },
      },
      {
        id: 9,
        name: 'DN quân đội không thuộc quân khu địa phương',
        code: 'qd2',
        organizationGroup: {
          id: 2,
        },
      },
      {
        id: 10,
        name: 'Các loại hình doanh nghiệp khác',
        code: 'k',
        organizationGroup: {
          id: 2,
        },
      },
    ]);
  }
}
