import { Process } from '../../module/citizen-process-management/category/process/entities/process.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { NVQS_PROCESS_GROUP, DBDV_PROCESS_GROUP, DQTV_PROCESS_GROUP } from './process-group.seed';

export default class ProcessSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const repository = dataSource.getRepository(Process);
    await repository.save([
      PROCESS_INIT,
      NVQS_DK,
      NVQS_DKTN,
      NVQS_THDK,
      NVQS_CDK,
      NVQS_TDK,
      NVQS_MDK,
      NVQS_TH,
      NVQS_MIEN,
      NVQS_GST,
      NVQS_DST,
      NVQS_KDST,
      NVQS_CST,
      NVQS_TST,
      NVQS_VST,
      NVQS_DKT,
      NVQS_KDKT,
      NVQS_CKT,
      NVQS_TKT,
      NVQS_VKT,
      NVQS_GNNCT,
      NVQS_GNNDP,
      NVQS_NN,
      NVQS_VNN,
      NVQS_TNN,
      NVQS_CNN,
      NVQS_DN,
      NVQS_XN,
      NVQS_DC,
      DBDV_H1,
      DBDV_H2,
      DBDV_BCH1,
      DBDV_BCH2,
      DBDV_TNH1,
      DBDV_TNH2,
      DBDV_SQDB,
      DBDV_BCSQDB,
      DBDV_TNSQDB,
      PTKT_DK,
      PTKT_TDK,
      PTKT_DKL,
      PTKT_BC,
      PTKT_TBC,
      PTKT_TV,
      DQTV_DK,
      DQTV_DKTN,
      DQTV_XDHN,
      DQTV_BC,
      DQTV_LC,
      DQTV_TBC,
      DQTV_RKXDHN,
      DQTV_HT,
    ]);

    await repository.save([
      {
        ...PROCESS_INIT,
        nextProcesses: [NVQS_DKTN, NVQS_THDK, NVQS_DK, NVQS_CDK, NVQS_TDK],
      },
      {
        ...NVQS_DK,
        nextProcesses: [
          NVQS_DKTN,
          NVQS_TH,
          NVQS_MIEN,
          DBDV_H1,
          DBDV_H2,
          DBDV_SQDB,
          DQTV_DK,
          NVQS_GST,
        ],
      },
      { ...NVQS_DKTN, nextProcesses: [DBDV_H2, NVQS_GST] },
      { ...NVQS_THDK, nextProcesses: [NVQS_MDK] },
      { ...NVQS_CDK, nextProcesses: [NVQS_DK, NVQS_TDK] },
      { ...NVQS_TDK, nextProcesses: [NVQS_DK, NVQS_CDK] },
      { ...NVQS_MDK, nextProcesses: [] },
      { ...NVQS_TH, nextProcesses: [NVQS_DKTN, NVQS_DK] },
      { ...NVQS_MIEN, nextProcesses: [NVQS_DKTN] },
      { ...NVQS_GST, nextProcesses: [NVQS_DST, NVQS_KDST, NVQS_CST, NVQS_TST, NVQS_VST] },
      { ...NVQS_DST, nextProcesses: [NVQS_DKT, NVQS_KDKT, NVQS_CKT, NVQS_TKT, NVQS_VKT] },
      { ...NVQS_KDST, nextProcesses: [NVQS_DK] },
      { ...NVQS_CST, nextProcesses: [NVQS_DK] },
      { ...NVQS_TST, nextProcesses: [NVQS_DK] },
      { ...NVQS_VST, nextProcesses: [NVQS_DK] },
      { ...NVQS_DKT, nextProcesses: [NVQS_GNNCT, NVQS_GNNDP] },
      { ...NVQS_KDKT, nextProcesses: [NVQS_DK] },
      { ...NVQS_CKT, nextProcesses: [NVQS_DK] },
      { ...NVQS_TKT, nextProcesses: [NVQS_DK] },
      { ...NVQS_VKT, nextProcesses: [NVQS_DK] },
      {
        ...NVQS_GNNCT,
        nextProcesses: [NVQS_VNN, NVQS_NN, NVQS_CNN, NVQS_TNN],
      },
      {
        ...NVQS_GNNDP,
        nextProcesses: [NVQS_DK, NVQS_VNN, NVQS_NN, NVQS_CNN, NVQS_TNN],
      },
      { ...NVQS_NN, nextProcesses: [NVQS_DN, NVQS_DC, NVQS_XN] },
      { ...NVQS_VNN, nextProcesses: [NVQS_DK, NVQS_NN] },
      { ...NVQS_TNN, nextProcesses: [NVQS_NN] },
      { ...NVQS_CNN, nextProcesses: [NVQS_NN] },
      { ...NVQS_DN, nextProcesses: [] },
      { ...NVQS_XN, nextProcesses: [DBDV_H1, DBDV_H2] },
      { ...NVQS_DC, nextProcesses: [NVQS_DC, NVQS_DN] },
      { ...DBDV_H1, nextProcesses: [DBDV_BCH1] },
      { ...DBDV_H2, nextProcesses: [DBDV_H1, DBDV_BCH2] },
      { ...DBDV_BCH1, nextProcesses: [DBDV_TNH1, DBDV_SQDB] },
      { ...DBDV_BCH2, nextProcesses: [DBDV_TNH2, DBDV_SQDB] },
      { ...DBDV_TNH1, nextProcesses: [] },
      { ...DBDV_TNH2, nextProcesses: [] },
      { ...DBDV_SQDB, nextProcesses: [DBDV_BCSQDB] },
      { ...DBDV_BCSQDB, nextProcesses: [DBDV_TNSQDB] },
      { ...DBDV_TNSQDB, nextProcesses: [] },
      { ...PTKT_DK, nextProcesses: [] },
      { ...PTKT_TDK, nextProcesses: [] },
      { ...PTKT_DKL, nextProcesses: [] },
      { ...PTKT_BC, nextProcesses: [] },
      { ...PTKT_TBC, nextProcesses: [] },
      { ...PTKT_TV, nextProcesses: [] },
      { ...DQTV_DK, nextProcesses: [DQTV_DKTN, DQTV_XDHN] },
      { ...DQTV_DKTN, nextProcesses: [DQTV_XDHN] },
      { ...DQTV_XDHN, nextProcesses: [DQTV_BC, DBDV_H1] },
      { ...DQTV_BC, nextProcesses: [DQTV_RKXDHN, DQTV_LC, DQTV_HT] },
      { ...DQTV_LC, nextProcesses: [DQTV_HT] },
      { ...DQTV_TBC, nextProcesses: [DQTV_XDHN] },
      { ...DQTV_RKXDHN, nextProcesses: [DQTV_TBC] },
      { ...DQTV_HT, nextProcesses: [DQTV_TBC] },
    ]);
  }
}
const modulePath = {
  nvqs: 'nvqs-process',
  dbdv: 'dbdv-process',
  dqtv: 'dqtv-process',
};
export const PROCESS_INIT = {
  id: 1,
  name: 'process_init',
  code: 'PROCESS_INIT',
};
export const NVQS_DK = {
  id: 2,
  name: 'Đăng ký nghĩa vụ quân sự lần đầu',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_DK',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-dk-process',
};
export const NVQS_DKTN = {
  id: 3,
  name: 'Đăng ký tự nguyện tham gia nghĩa vụ quân sự',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_DKTN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-dktn-process',
};
export const NVQS_THDK = {
  id: 4,
  name: 'Tạm hoãn đăng ký',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_THDK',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-thdk-process',
};
export const NVQS_CDK = {
  id: 5,
  name: 'Chống đăng ký',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_CDK',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-cdk-process',
};
export const NVQS_TDK = {
  id: 6,
  name: 'Trốn đăng ký',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_TDK',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-tdk-process',
};
export const NVQS_MDK = {
  id: 7,
  name: 'Miễn thực hiện NVQS (Miễn đăng ký)',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_MDK',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-mdk-process',
};
export const NVQS_TH = {
  id: 8,
  name: 'Tạm hoãn NVQS',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_TH',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-th-process',
};
export const NVQS_MIEN = {
  id: 9,
  name: 'Miễn gọi nhập ngũ',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_MIEN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-mien-process',
};
export const NVQS_GST = {
  id: 10,
  name: 'Được gọi sơ tuyển sức khỏe cấp xã',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_GST',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-gst-process',
};
export const NVQS_DST = {
  id: 11,
  name: 'Đạt sơ tuyển chuyển lên khám cấp huyện',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_DST',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-dst-process',
};
export const NVQS_KDST = {
  id: 12,
  name: 'Không đạt sơ tuyển',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_KDST',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-kdst-process',
};
export const NVQS_CST = {
  id: 13,
  name: 'Chống sơ tuyển',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_CST',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-cst-process',
};
export const NVQS_TST = {
  id: 14,
  name: 'Trốn sơ tuyển',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_TST',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-tst-process',
};
export const NVQS_VST = {
  id: 15,
  name: 'Vắng khám sơ tuyển',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_VST',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-vst-process',
};
export const NVQS_DKT = {
  id: 16,
  name: 'Đạt khám NVQS (chờ gọi nhập ngũ)',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_DKT',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-dkt-process',
};
export const NVQS_KDKT = {
  id: 17,
  name: 'Không đạt khám NVQS',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_KDKT',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-kdkt-process',
};
export const NVQS_CKT = {
  id: 18,
  name: 'Chống khám NVQS',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_CKT',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-ckt-process',
};
export const NVQS_TKT = {
  id: 19,
  name: 'Trốn khám NVQS',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_TKT',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-tkt-process',
};
export const NVQS_VKT = {
  id: 20,
  name: 'Vắng khám NVQS',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_VKT',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-vkt-process',
};
export const NVQS_GNNCT = {
  id: 21,
  name: 'Gọi nhập ngũ (chính thức)',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_GNNCT',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-gnnct-process',
};
export const NVQS_GNNDP = {
  id: 22,
  name: 'Gọi nhập ngũ (dự phòng)',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_GNNDP',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-gnndp-process',
};
export const NVQS_NN = {
  id: 23,
  name: 'Nhập ngũ',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_NN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-nn-process',
};
export const NVQS_VNN = {
  id: 24,
  name: 'Vắng nhập ngũ có lý do',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_VNN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-vnn-process',
};
export const NVQS_TNN = {
  id: 25,
  name: 'Trốn nhập ngũ',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_TNN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-tnn-process',
};
export const NVQS_CNN = {
  id: 26,
  name: 'Chống nhập ngũ',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_CNN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-cnn-process',
};
export const NVQS_DN = {
  id: 27,
  name: 'Đảo ngũ',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_DN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-dn-process',
};
export const NVQS_XN = {
  id: 28,
  name: 'Xuất ngũ',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_XN',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-xn-process',
};
export const NVQS_DC = {
  id: 29,
  name: 'Di chuyển',
  processGroup: NVQS_PROCESS_GROUP,
  code: 'NVQS_DC',
  modulePath: modulePath.nvqs,
  processPath: 'nvqs-dc-process',
};
export const DBDV_H1 = {
  id: 30,
  name: 'Đăng kí dự bị động viên hạng 1',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_H1',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-dkh1-process',
};
export const DBDV_H2 = {
  id: 31,
  name: 'Đăng kí dự bị động viên hạng 2',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_H2',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-dkh2-process',
};
export const DBDV_BCH1 = {
  id: 32,
  name: 'Biên chế dự bị động viên hạng 1',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_BCH1',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-bch1-process',
};
export const DBDV_BCH2 = {
  id: 33,
  name: 'Biên chế dự bị động viên hạng 2',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_BCH2',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-bch2-process',
};
export const DBDV_TNH1 = {
  id: 34,
  name: 'Thôi ngạch dự bị hạng 1',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_TNH1',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-tnh1-process',
};
export const DBDV_TNH2 = {
  id: 35,
  name: 'Thôi ngạch dự bị hạng 2',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_TNH2',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-tnh2-process',
};
export const DBDV_SQDB = {
  id: 36,
  name: 'Sĩ quan dự bị',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_SQDB',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-sqdb-process',
};
export const DBDV_BCSQDB = {
  id: 37,
  name: 'Sĩ quan dự bị biên chế',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_BCSQDB',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-bcsqdb-process',
};
export const DBDV_TNSQDB = {
  id: 38,
  name: 'Thôi ngạch dự bị Sĩ quan',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'DBDV_TNSQDB',
  modulePath: modulePath.dbdv,
  processPath: 'dbdv-tnsqdb-process',
};
export const PTKT_DK = {
  id: 39,
  name: 'Đăng ký PTKT',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'PTKT_DK',
};
export const PTKT_TDK = {
  id: 40,
  name: 'Thôi đăng ký PTKT',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'PTKT_TDK',
};
export const PTKT_DKL = {
  id: 41,
  name: 'Đăng ký lại PTKT',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'PTKT_DKL',
};
export const PTKT_BC = {
  id: 42,
  name: 'Biên chế PTKT',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'PTKT_BC',
};
export const PTKT_TBC = {
  id: 43,
  name: 'Thôi biên chế PTKT',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'PTKT_TBC',
};
export const PTKT_TV = {
  id: 44,
  name: 'Tạm vắng',
  processGroup: DBDV_PROCESS_GROUP,
  code: 'TVPTKT_TV',
};
export const DQTV_DK = {
  id: 45,
  name: 'Đăng ký tham gia DQTV',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_DK',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-dk-process',
};
export const DQTV_DKTN = {
  id: 46,
  name: 'Đăng ký tự nguyện tham gia DQTV',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_DKTN',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-dktn-process',
};
export const DQTV_XDHN = {
  id: 47,
  name: 'Dân quân tự vệ xây dựng hàng năm',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_XDHN',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-xdhn-process',
};
export const DQTV_BC = {
  id: 48,
  name: 'Biên chế dân quân tự vệ',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_BC',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-bc-process',
};
export const DQTV_LC = {
  id: 49,
  name: 'Luân chuyển dân quân tự vệ',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_LC',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-lc-process',
};
export const DQTV_TBC = {
  id: 50,
  name: 'Thôi biên chế DQTV',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_TBC',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-tbc-process',
};
export const DQTV_RKXDHN = {
  id: 51,
  name: 'Ra khỏi dân quân tự vệ xây dựng hàng năm',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_RKXDHN',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-rkxdhn-process',
};
export const DQTV_HT = {
  id: 52,
  name: 'Hoàn thành nghĩa vụ dân quân tự vệ',
  processGroup: DQTV_PROCESS_GROUP,
  code: 'DQTV_HT',
  modulePath: modulePath.dqtv,
  processPath: 'dqtv-ht-process',
};
