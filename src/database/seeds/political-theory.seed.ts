import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { PoliticalTheory } from '../../module/category/political-theory/political-theory.entity';

export default class PoliticalTheorySeeder implements Seeder {
  public politicalTheoryData = {
    SO_CAP: {
      id: 1,
      code: 'SO_CAP',
      name: 'Sơ cấp',
      description: '',
    },
    TRUNG_CAP: {
      id: 2,
      code: 'TRUNG_CAP',
      name: 'Trung cấp',
      description: '',
    },
    CAO_CAP: {
      id: 3,
      code: 'CAO_CAP',
      name: 'Cao cấp',
      description: '',
    },
  };
  public async run(dataSource: DataSource): Promise<any> {
    const politicalTheoryData = dataSource.getRepository(PoliticalTheory);
    await politicalTheoryData.save([
      this.politicalTheoryData.SO_CAP,
      this.politicalTheoryData.TRUNG_CAP,
      this.politicalTheoryData.CAO_CAP,
    ]);
  }
}
