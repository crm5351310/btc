import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { DefenseSecurityTrainee } from '../../module/category/defense-security-trainee/entities/defense-security-trainee.entity';

export default class DefenseSecurityTraineeSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(DefenseSecurityTrainee);
    await reporitory.save([
      { id: 1, code: 'DOITUONG1', name: 'Đối tượng 1' },
      { id: 2, code: 'DOITUONG2', name: 'Đối tượng 2' },
      { id: 3, code: 'DOITUONG3', name: 'Đối tượng 3' },
      { id: 4, code: 'DOITUONG4', name: 'Đối tượng 4' },
      { id: 5, code: 'CHUCSAC', name: 'Chức sắc' },
      { id: 6, code: 'CHUCVIEC', name: 'Chức việc' },
      { id: 7, code: 'TRUONGDONGHO', name: 'Trưởng dòng họ' },
      { id: 8, code: 'KHAC', name: 'Đối tượng khác' },
    ]);
  }
}
