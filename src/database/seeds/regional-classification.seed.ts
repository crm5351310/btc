import { RegionalClassification } from '../../module/system/department-root/regional-classification/regional-classification.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class RegionalClassificationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const regionalClassificationRepository = dataSource.getRepository(RegionalClassification);
    await regionalClassificationRepository.save([
      {
        id: 1,
        code: 'NONG_THON',
        name: 'Nông thôn',
      },
      {
        id: 2,
        code: 'THANH_THI',
        name: 'Thành thị',
      },
    ]);
  }
}
