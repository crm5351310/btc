import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { EducationLevel } from '../../module/category/education-level/education-level.entity';
export default class EducationLevelSeeder implements Seeder {

  public async run(dataSource: DataSource): Promise<any> {
    const educationLevelRepository = dataSource.getRepository(EducationLevel);
    await educationLevelRepository.save([
      SO_CAP_NGHE,
      TRUNG_CAP_NGHE,
      CAO_DANG,
      DAI_HOC,
      SAU_DAI_HOC,
      KHONG,
    ]);
  }
}

export const SO_CAP_NGHE = {
  id: 1,
  code: 'SO_CAP_NGHE',
  name: 'Sơ cấp nghề',
  description: '',
};
export const TRUNG_CAP_NGHE = {
  id: 2,
  code: 'TRUNG_CAP_NGHE',
  name: 'Trung cấp nghề',
  description: '',
};
export const CAO_DANG = {
  id: 3,
  code: 'CAO_DANG',
  name: 'Cao đẳng',
  description: '',
};
export const DAI_HOC = {
  id: 4,
  code: 'DAI_HOC',
  name: 'Đại học',
  description: '',
};
export const SAU_DAI_HOC = {
  id: 5,
  code: 'SAU_DAI_HOC',
  name: 'Sau đại học',
  description: '',
};
export const KHONG = {
  id: 6,
  code: 'KHONG',
  name: 'Không',
  description: '',
};