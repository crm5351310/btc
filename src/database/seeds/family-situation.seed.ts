import { FamilySituation } from '../../module/citizen-management/category/family-situation/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class FamilySituationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(FamilySituation);
    await reporitory.save([
      { id: 1, code: 'CON_LIET_SY', name: 'Con liệt sỹ' },
      { id: 2, code: 'CON_THUONG_BINH', name: 'Con thương binh' },
      { id: 3, code: 'CON_BENH_BINH', name: 'Con bệnh binh' },
      { id: 4, code: 'CON_NGOAI_GIA_THU', name: 'Con ngoài giá thú' },
    ]);
  }
}
