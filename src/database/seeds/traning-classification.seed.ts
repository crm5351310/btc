import { TrainingClassification } from '../../module/category/military-job-root/training-classification/entities/training-classification.entity';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
export default class TrainingClassificationSeed implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(TrainingClassification);
    await reporitory.save([
      {
        id: 1,
        code: 'so_cap',
        name: 'Sơ cấp',
      },
      {
        id: 2,
        code: 'trung_cap',
        name: 'Trung cấp',
      },
      {
        id: 3,
        code: 'cao_cap',
        name: 'Cao cấp',
      },
    ]);
  }
}
