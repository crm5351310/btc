import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { TrainingMethod } from '../../module/category/training-method/entities';

export default class TrainingMethodSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(TrainingMethod);
    await reporitory.save([
      { id: 1, code: 'KET_HOP', name: 'Đào tạo kết hợp các hình thức khác' },
      { id: 2, code: 'TAI_DON_VI', name: 'Đào tạo tại đơn vị' },
      { id: 3, code: 'TRUYEN_HINH', name: 'Chuyên mục truyền hình' },
    ]);
  }
}
