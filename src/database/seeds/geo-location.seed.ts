import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { GeoLocationConst } from '../../common/constant/geo-location.constant';
import { GeoLocation } from '../../module/category/administrative/geo-location/geo-location.entity';

export default class GeoLocationSeeder implements Seeder {
  public geoData = {
    NOI_DIA: {
      id: 1,
      name: GeoLocationConst.NOI_DIA,
      code: Object.keys(GeoLocationConst)[0],
      isDefault: true,
    },
    BG_VT: {
      id: 2,
      name: GeoLocationConst.BIEN_GIOI_VIET_TRUNG,
      code: Object.keys(GeoLocationConst)[1],
      isDefault: true,
    },
    BG_VL: {
      id: 3,
      name: GeoLocationConst.BIEN_GIOI_VIET_LAO,
      code: Object.keys(GeoLocationConst)[2],
      isDefault: true,
    },
    BG_VC: {
      id: 4,
      name: GeoLocationConst.BIEN_GIOI_VIET_CAMPUCHIA,
      code: Object.keys(GeoLocationConst)[3],
      isDefault: true,
    },
    VEN_BIEN: {
      id: 5,
      name: GeoLocationConst.VEN_BIEN,
      code: Object.keys(GeoLocationConst)[4],
      isDefault: true,
    },
    DAO: {
      id: 6,
      name: GeoLocationConst.DAO,
      code: Object.keys(GeoLocationConst)[5],
      isDefault: true,
    },
  };
  public async run(dataSource: DataSource): Promise<any> {
    const geoLocation = dataSource.getRepository(GeoLocation);
    await geoLocation.save([
      this.geoData.NOI_DIA,
      this.geoData.BG_VT,
      this.geoData.BG_VL,
      this.geoData.BG_VC,
      this.geoData.VEN_BIEN,
      this.geoData.DAO,
    ]);
  }
}
