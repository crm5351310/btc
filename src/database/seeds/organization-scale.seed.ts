import { OrganizationScale } from '../../module/category/organization-scale/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class OrganizationScaleSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(OrganizationScale);
    await reporitory.save([
      { id: 1, code: 'TO', name: 'Tổ' },
      { id: 2, code: 'TIEU_DOI', name: 'Tiểu đội' },
      { id: 3, code: 'TRUNG_DOI', name: 'Trung đội' },
      { id: 4, code: 'DAI_DOI', name: 'Đại đội' },
      { id: 5, code: 'TIEU_DOAN', name: 'Tiểu đoàn' },
      { id: 6, code: 'TRUNG_DOAN', name: 'Trung đoàn' },
      { id: 7, code: 'SU_DOAN', name: 'Sư đoàn' },
    ]);
  }
}
