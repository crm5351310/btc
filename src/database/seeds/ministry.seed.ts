import { Ministry } from '../../module/category/military-unit-root/ministry/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class MinistrySeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(Ministry);
    await reporitory.save([boCongAn, boQuocPhong]);
  }
}

export const boCongAn = {
  id: 1,
  code: 'BO_CONG_AN',
  name: 'Bộ công an',
} as Ministry;

export const boQuocPhong = {
  id: 2,
  code: 'BO_QUOC_PHONG',
  name: 'Bộ quốc phòng',
} as Ministry;
