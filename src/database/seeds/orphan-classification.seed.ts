import { OrphanClassification } from '../../module/citizen-management/category/orphan-classification/entities';
import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';

export default class OrphanClassificationSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const reporitory = dataSource.getRepository(OrphanClassification);
    await reporitory.save([
      { id: 1, code: 'FATHER_MOTHER', name: 'Cha và mẹ' },
      { id: 2, code: 'FATHER', name: 'Cha' },
      { id: 3, code: 'MOTHER', name: 'Mẹ' },
    ]);
  }
}
