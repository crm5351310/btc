import { DataSource } from 'typeorm';
import { Seeder } from 'typeorm-extension';
import { Religion } from '../../module/category/religion/religion.entity';

export default class ReligionSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<any> {
    const religionRepository = dataSource.getRepository(Religion);
    await religionRepository.save([
      {
        id: 1,
        code: 'thien_chua_giao',
        name: 'Thiên chúa giáo',
      },
      {
        id: 2,
        code: 'phat_giao',
        name: 'Phật giáo',
      },
      {
        id: 3,
        code: 'tin_lanh',
        name: 'Tin lành',
      },
      {
        id: 4,
        code: 'cong_giao',
        name: 'Công giáo',
      },
      {
        id: 5,
        code: 'hoi_giao',
        name: 'Hồi giáo',
      },
    ]);
  }
}
