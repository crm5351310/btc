import { MigrationInterface, QueryRunner } from 'typeorm';
import { StringHelper } from '../../../common/utils/string.util';

export class Migration implements MigrationInterface {
  name: string = `init${StringHelper.randomTimestampInit()}`;

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'init%\';\n');

    //procedure
    await queryRunner.query('DROP PROCEDURE IF EXISTS citizen_import_school');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_education_level');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_ethicity');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_family_class');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_family_economic');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_gender');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_general_education_level');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_orphan_classification');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_personal_class');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_religion');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_academic_degree');
    await queryRunner.query('DROP PROCEDURE IF EXISTS party_member_ranking');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_family_situation');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_classify_officer_child');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_business_type');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_job');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_job_position');
    await queryRunner.query('DROP PROCEDURE IF EXISTS citizen_import_district');
    await queryRunner.query('DROP PROCEDURE IF EXISTS citizen_import_commune');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_school');
    await queryRunner.query('DROP PROCEDURE IF EXISTS citizen_import_major');
    await queryRunner.query('DROP PROCEDURE IF EXISTS droplist_job_specialization_group');
    await queryRunner.query('DROP PROCEDURE IF EXISTS citizen_import_job_specialization');
    await queryRunner.query('DROP PROCEDURE IF EXISTS citizen_import_reason');
    await queryRunner.query('DROP PROCEDURE IF EXISTS proc_citizens_at_least_seventeen');

    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'citizen-import-school%\';\n');
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-education-level%\';\n',
    );
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-ethicity%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-family-class%\';\n');
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-family-economic%\';\n',
    );
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-gender%\';\n');
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-general-education-level%\';\n',
    );
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-orphan-classification%\';\n',
    );
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-personal-class%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-religion%\';\n');
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-academic-degree%\';\n',
    );
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-party-member-ranking%\';\n',
    );
    await queryRunner.query(
      'DELETE FROM migrations WHERE name LIKE \'droplist-family-situation%\';\n',
    );
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'classify-officer-child%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-business-type%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-job%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-job-position%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'citizen-import-district%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'citizen-import-commune%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-school%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'citizen-import-major%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'droplist-job-specialization-group%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'citizen-import-job-specialization%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'citizen-import-reason%\';\n');
    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'proc-citizens-at-least-seventeen%\';\n');
    //function
    await queryRunner.query('DROP TABLE IF EXISTS roman');

    await queryRunner.query('DELETE FROM migrations WHERE name LIKE \'roman%\';\n');
    
  }

  public async down(): Promise<void> {}
}
