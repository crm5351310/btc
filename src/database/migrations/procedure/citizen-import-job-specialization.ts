import { MigrationInterface, QueryRunner } from 'typeorm';
import { StringHelper } from '../../../common/utils/string.util';
export class Migration implements MigrationInterface {
  name: string = `citizen-import-job-specialization${StringHelper.randomTimestamp()}`;
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(StringHelper.raw('citizen-import-job-specialization', 'procedure'));
  }

  public async down(): Promise<void> {}
}
