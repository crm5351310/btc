import { MigrationInterface, QueryRunner } from 'typeorm';
import { StringHelper } from '../../../common/utils/string.util';

export class Migration implements MigrationInterface {
  name: string = `droplist-family-situation${StringHelper.randomTimestamp()}`;

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(StringHelper.raw('droplist-family-situation', 'procedure'));
  }

  public async down(): Promise<void> {}
}
