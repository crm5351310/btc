import { MigrationInterface, QueryRunner } from 'typeorm';
import { StringHelper } from '../../../common/utils/string.util';

export class Migration implements MigrationInterface {
  name: string = `proc-citizens-at-least-seventeen${StringHelper.randomTimestamp()}`;

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(StringHelper.raw('proc-citizens-at-least-seventeen', 'procedure'));
  }

  public async down(): Promise<void> {}
}
