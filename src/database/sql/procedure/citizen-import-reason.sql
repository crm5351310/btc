CREATE PROCEDURE IF NOT EXISTS citizen_import_reason(
    IN `code` VARCHAR(100)
)
BEGIN
SELECT
    A.name as value
FROM
    reason A
        INNER JOIN process B ON
            A.processId = B.id
WHERE B.code = code;

END
