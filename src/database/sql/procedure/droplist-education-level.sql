CREATE PROCEDURE IF NOT EXISTS droplist_education_level()
BEGIN
SELECT
    name AS value
FROM
    education_level;

END
