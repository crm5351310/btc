CREATE PROCEDURE IF NOT EXISTS droplist_business_type()
BEGIN
SELECT
    name AS value
FROM
    business_type;

END
