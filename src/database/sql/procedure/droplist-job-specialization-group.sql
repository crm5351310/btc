CREATE PROCEDURE IF NOT EXISTS droplist_job_specialization_group()
BEGIN
SELECT
    id,
    name
FROM
    job_specialization_group;

END
