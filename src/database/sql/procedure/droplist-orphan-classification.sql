CREATE PROCEDURE IF NOT EXISTS droplist_orphan_classification()
BEGIN
SELECT
    name AS value
FROM
    orphan_classification;

END
