CREATE PROCEDURE IF NOT EXISTS citizen_import_major(
    IN `_id_school` INT
)
BEGIN
SELECT
    B.name,
    A.schoolId AS parentId
FROM
    school_majors_major A
        INNER JOIN major B ON
            A.majorId = B.id
        WHERE A.schoolId = _id_school;

END
