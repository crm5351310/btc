CREATE PROCEDURE IF NOT EXISTS citizen_import_school()
BEGIN
SELECT
    DENSE_RANK() OVER(
ORDER BY
	A.id) AS stt,
        A.name AS school,
    C.name AS major
FROM
    school A
        LEFT JOIN school_majors_major B ON
            B.schoolId = A.id
        LEFT JOIN major C ON
            C.id = B.majorId ;

END
