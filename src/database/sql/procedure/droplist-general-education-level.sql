CREATE PROCEDURE IF NOT EXISTS droplist_general_education_level()
BEGIN
SELECT
    name AS value
FROM
    general_education_level;

END
