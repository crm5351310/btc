CREATE PROCEDURE IF NOT EXISTS citizen_import_commune(
    IN `_id_district` INT
)
BEGIN
SELECT
    name
FROM
    administrative_unit
WHERE parentId = _id_district;

END
