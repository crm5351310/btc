CREATE PROCEDURE IF NOT EXISTS `proc_citizens_at_least_seventeen`(
    IN `administrativeUnitId` INT,
    IN `toDate` DATE
)
BEGIN
		SELECT 
		(@row_number:=@row_number + 1) AS stt,
		CONCAT(UPPER(A.fullName), '\n', IFNULL(UPPER(A.otherName), ''), '\n', DATE_FORMAT(A.dateOfBirth, '%d/%m/%Y')) AS name,
		B.name AS generalEducationLevelName,
		REPLACE(
				CONCAT(
						IFNULL(A.residenceDetail, ''),
						IFNULL(IF(A.residenceDetail IS NULL OR A.originPlace IS NULL, '', ','), ''),
						IFNULL(A.originPlace, ''), '\n',
						IFNULL(A.address, ''), '\n',
						IFNULL(A.workPlace, '')
				),
				',', ' - '
		) AS address,
		CONCAT(
				IFNULL(C.name, ''), '\n',
				IFNULL(D.name, ''), '\n',
				CONCAT(IFNULL(E.name, ''), IFNULL(IF(E.name IS NULL OR M.name IS NULL, '', ','), ''), IFNULL(M.name, ''))
		) AS classAndEthnicity,
		CONCAT(F.name, '\n', IFNULL(IFNULL(K.name, G.name), ''), '\n', IFNULL(I.name, '')) AS job_data,
		GROUP_CONCAT(
				IF((N.code = 'ME' OR N.code = 'CHA'), CONCAT(L.name, ',', IFNULL(L.dateOfBirth, ''), IFNULL(IF(L.dateOfBirth IS NULL,'',','), ''), N.name), '') 
				ORDER BY L.id
				SEPARATOR '\n'
		) AS family_member
		FROM 
				citizen A
		INNER JOIN 
				general_education_level B ON A.generalEducationLevelId = B.id AND B.deleted_at IS NULL
		LEFT JOIN 
				family_class C ON A.familyClassId = C.id AND C.deleted_at IS NULL
		LEFT JOIN 
				personal_class D ON D.id = A.personalClassId AND D.deleted_at IS NULL
		LEFT JOIN 
				ethnicity E ON E.id = A.ethnicityId AND E.deleted_at IS NULL
		INNER JOIN 
				education_level F ON F.id = A.educationLevelId AND F.deleted_at IS NULL
		LEFT JOIN 
				major G ON G.id = A.majorId AND G.deleted_at IS NULL
		INNER JOIN 
				administrative_unit H ON H.id = A.administrativeUnitId AND H.deleted_at IS NULL
		LEFT JOIN 
				job I ON I.id = A.jobId AND I.deleted_at IS NULL
		LEFT JOIN 
				job_specialization K ON K.id = A.jobSpecializationId AND K.deleted_at IS NULL
		LEFT JOIN 
				citizen_family_member L ON L.citizenId = A.id AND L.deleted_at IS NULL
		LEFT JOIN 
				family_relation N ON N.id = L.familyRelationId AND N.deleted_at IS NULL
		LEFT JOIN 
				religion M  ON M.id = A.religionId AND M.deleted_at IS NULL
		INNER JOIN 
				gender O ON O.id = A.genderId AND M.deleted_at IS NULL
		INNER JOIN 
				administrative_title R ON R.id = H.administrativeTitleId AND R.deleted_at IS NULL
		INNER JOIN 
				administrative_level S ON S.id = R.administrativeLevelId AND S.deleted_at IS NULL
		CROSS JOIN 
				(SELECT @row_number := 0) AS dummy
		WHERE 
				A.administrativeUnitId = administrativeUnitId
				AND S.code = 'xa'
				AND YEAR(toDate) - YEAR(A.dateOfBirth) = 17
				AND O.code = 'MALE'
		GROUP BY 
				A.id;

        
		SELECT P.name, Q.name AS parentName
		FROM administrative_unit AS P
		INNER JOIN administrative_unit AS Q ON P.parentId = Q.id
		WHERE P.parentId = Q.id 
		AND P.id = administrativeUnitId
		GROUP BY P.id;
END