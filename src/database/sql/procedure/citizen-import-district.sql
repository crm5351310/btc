CREATE PROCEDURE IF NOT EXISTS citizen_import_district()
BEGIN
SELECT
    id,
    name
FROM
    administrative_unit
WHERE parentId = 1;

END
