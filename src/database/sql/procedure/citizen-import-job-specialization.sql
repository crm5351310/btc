CREATE PROCEDURE IF NOT EXISTS citizen_import_job_specialization(
    IN `_id_job_specialization` INT
)
BEGIN
SELECT
    name
FROM
    job_specialization
WHERE jobSpecializationGroupId = _id_job_specialization;

END
