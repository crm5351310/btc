CREATE PROCEDURE IF NOT EXISTS droplist_academic_degree()
BEGIN
SELECT
    name AS value
FROM
    academic_degree;

END
