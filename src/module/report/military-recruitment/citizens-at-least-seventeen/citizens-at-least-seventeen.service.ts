import { Injectable } from '@nestjs/common';
import { ImportService } from 'src/module/shared/import/import.service';
import { QueryReportCitizensAtLeastSeventeenDto } from './dto/queryReportCitizensAtLeastSeventeen.dto';
import citizenAtLeastSeventeenModel, { JsonParam } from './model/citizens-at-least-seventeen.model'

@Injectable()
export class CitizensAtLeastSeventeenService extends ImportService{
  async export(dto : QueryReportCitizensAtLeastSeventeenDto) {
    const data = await this.getDataMultiple('proc_citizens_at_least_seventeen',[dto.administrativeUnitId,dto.year])
    const files = await this.minioService.findOneFile(
      '1.01GNN-2016 BCDanhSachCDNamDu17TuoiTrongNam',
      'report/BaoCaoTuyenQuan',
    );
    data[0].push({
      name : 'TỔNG : ' + data[0].length,
      isBold : true
    })
    let jsonExport : JsonParam[] = [{
      sheetName : 'Bia',
      replaces: [
        {
          text: '_commune',
          cell: 'H25',
          value: data[1].length > 0 ? data[1][0].name : '',
        },
        {
          text: '_district',
          cell: 'H26',
          value: data[1].length > 0 ? data[1][0].parentName : '',
        },
        {
          text: '_province',
          cell: 'H27',
          value: 'Hậu Giang',
        },
        {
          text: "_commune",
          cell: "B4",
          value: data[1].length > 0 ? data[1][0].name : '',
        },
        {
          text: "_year",
          cell: "E5",
          value: dto.year?.getFullYear(),
        },
      ],
      table: {
        rowCode: 1,
        rowStart: 13,
      },
    },
    {
      sheetName : 'DanhSachCongDan',
      replaces: [
        {
          text: "_commune",
          cell: "B4",
          value: data[1].length > 0 ? data[1][0].name : '',
        },
        {
          text: "_year",
          cell: "E5",
          value: dto.year?.getFullYear(),
        },
      ],
      table: {
        rowCode: 1,
        rowStart: 13,
      },
      data: data[0]
    }
  ];
    return await citizenAtLeastSeventeenModel(jsonExport,files)
  }
}
