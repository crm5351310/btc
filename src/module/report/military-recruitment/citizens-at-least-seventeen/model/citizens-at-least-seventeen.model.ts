import * as ExcelJS from 'exceljs';
import { Readable as ReadableStream } from 'node:stream';

export interface JsonParam {
  replaces: replaces[],
  sheetName: string,
  table: {
    rowCode: number,
    rowStart: number
  },
  data?: any[]
}
export interface replaces {
  text: string,
  cell: string,
  value: string
}
const citizenAtLeastSeventeenModel = async (data: JsonParam[], file: ReadableStream): Promise<Buffer> => {
  const workbook = new ExcelJS.Workbook();
  await workbook.xlsx.read(file);
  data.forEach(async (item) => {
    var ws = workbook.getWorksheet(item.sheetName);
    item.replaces.forEach((e) => {
      if (ws) {
        ws.getCell(e.cell).value = ws.getCell(e.cell).value ? ws.getCell(e.cell).value?.toString().replaceAll(`${e.text}`, e.value) : '';
      }
    });
    if (item.data) {
      // Thiết lập nội dung báo cáo
      if (item.data.length > 0 && item.table.rowCode) {
        if (item.data.length > 3) { // Insert thêm dòng
          ws?.duplicateRow(item.table.rowStart + 1, item.data.length - 3, true);

        } else if (item.data.length < 3) { // xóa dòng
          ws?.spliceRows(item.table.rowStart, 3 - item.data.length);
        }

        // Fill nội dung
        let row,
          c,
          code,
          rowCode = ws?.getRow(item.table.rowCode);
        var i = 0;

        item.data.forEach((e) => {
          row = ws?.getRow(item.table.rowStart + i);
          c = 1;
          code = rowCode?.getCell(c).value;
          let defaultRowHeight = row?.height
          do {

            if (e[code]) {
              const cellContentHeight: number = estimateCellHeight(e[code]?.toString(), defaultRowHeight);
              if (row.height < cellContentHeight) {
                row.height = cellContentHeight
              }
            }
            if (e.isBold) {
              row.getCell(c).value = e[code];
              row.getCell(c).font = {
                name: "Times New Roman",
                size: 12,
                bold: true,
              }
              row.getCell(c).alignment = {
                vertical: "middle",
                horizontal: "left",
                wrapText: true,
              }
            } else {
              row.getCell(c).value = e[code];
              row.getCell(c).alignment = {
                vertical: "middle",
                horizontal: "left",
                wrapText: true,
              }
              if (code === 'generalEducationLevelName' || code === 'stt') {
                row.getCell(c).value = e[code];
                row.getCell(c).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                  wrapText: true,
                }
              }
            }
            c++;
            code = rowCode?.getCell(c).value;
          } while (code);
          i++;
        });

      }
      if (item.table.rowCode) {
        if (ws)
          ws.getRow(item.table.rowCode).hidden = true;
      }
    }
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return await workbook.xlsx.writeBuffer();
};

const estimateCellHeight = (cellValue: string, defaultHeight: number): number => {
  const lines = cellValue.split('\n').length;
  let estimatedHeight: number = 0
  if (lines > 1) {
    estimatedHeight = (lines + 1) * defaultHeight;
  } else {
    estimatedHeight = lines * defaultHeight;
  }
  return estimatedHeight;
}

export default citizenAtLeastSeventeenModel;
