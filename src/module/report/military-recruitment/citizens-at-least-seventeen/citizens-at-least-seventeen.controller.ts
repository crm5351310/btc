import { Controller, Get, Query, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { TagEnum } from 'src/common/enum/tag.enum';
import { ReportExportUtil } from 'src/common/utils/report.util';
import { CitizensAtLeastSeventeenService } from './citizens-at-least-seventeen.service';
import { QueryReportCitizensAtLeastSeventeenDto } from './dto/queryReportCitizensAtLeastSeventeen.dto';

@Controller()
@ApiTags(TagEnum.REPORT_CITIZENS_AT_LEAST_SEVENTEEN)

export class CitizensAtLeastSeventeenController {
  constructor(private readonly citizensAtLeastSeventeenService: CitizensAtLeastSeventeenService) {}
  @Get()
  async export(@Query() queryReportCitizensAtLeastSeventeenDto: QueryReportCitizensAtLeastSeventeenDto,@Res() res: Response ) {
    const result = await this.citizensAtLeastSeventeenService.export(queryReportCitizensAtLeastSeventeenDto);
    res.status(200).set(ReportExportUtil.header('01GNN-2016 BCDanhSachCDNamDu17TuoiTrongNam')).send(result);
  }
}
