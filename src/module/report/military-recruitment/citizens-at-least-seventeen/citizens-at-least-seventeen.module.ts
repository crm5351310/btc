import { Module } from '@nestjs/common';
import { CitizensAtLeastSeventeenService } from './citizens-at-least-seventeen.service';
import { CitizensAtLeastSeventeenController } from './citizens-at-least-seventeen.controller';
import { RouteTree } from '@nestjs/core';
import { MinioModule } from 'src/module/shared/minio/minio.module';

@Module({
  imports : [
    MinioModule
  ],
  controllers: [CitizensAtLeastSeventeenController],
  providers: [CitizensAtLeastSeventeenService],
})
export class CitizensAtLeastSeventeenModule {
  static readonly route : RouteTree = {
    path : 'citizens-at-least-seventeen',
    module : CitizensAtLeastSeventeenModule,
  }
}
