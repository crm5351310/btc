import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional } from 'class-validator';

export class QueryReportCitizensAtLeastSeventeenDto {
  @ApiProperty({
    description: 'Id xã',
    required: false,
  })
  @IsOptional()
  administrativeUnitId: number;

  @ApiProperty({
    description: 'Năm',
    required: false,
  })
  @IsOptional()
  @IsDate()
  year?: Date;
}
