import { Module } from '@nestjs/common';
import { NvqsDkMaleCitizenService } from './nvqs-dk-male-citizen.service';
import { NvqsDkMaleCitizenController } from './nvqs-dk-male-citizen.controller';
import { RouteTree } from '@nestjs/core';
import { MinioModule } from '../../../shared/minio/minio.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { Citizen } from '../../../citizen-management/citizen/entities';
import { GeneralEducationLevel } from '../../../category';
import { Reason } from '../../../category/reason/entities';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AdministrativeUnit,
      Citizen,
      GeneralEducationLevel,
      Reason,
    ]),
    MinioModule,
  ],
  controllers: [NvqsDkMaleCitizenController],
  providers: [NvqsDkMaleCitizenService],
})
export class NvqsDkMaleCitizenModule {
  static readonly route: RouteTree = {
    path: 'nvqs-dk-male-citizen',
    module: NvqsDkMaleCitizenModule,
  }
}
