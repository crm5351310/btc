import { Injectable } from '@nestjs/common';
import { MinioService } from '../../../shared/minio/minio.service';
import { Workbook } from 'exceljs';
import { InjectRepository } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { LessThanOrEqual, Repository } from 'typeorm';
import { ExportNvqsDkMaleCitizenDto } from './dto/export-nvqs-dk-male-citizen.dto';
import { NvqsDkMaleCitizen } from './entities';
import { Citizen } from '../../../citizen-management/citizen/entities';
import { SO_CAP_NGHE, TRUNG_CAP_NGHE } from '../../../../database/seeds/education-level.seed';
import { NVQS_CDK, NVQS_DK, NVQS_MIEN, NVQS_TDK, NVQS_THDK } from '../../../../database/seeds/process.seed';
import { GeneralEducationLevel } from '../../../category';
import { Reason } from '../../../category/reason/entities';
import { upperCaseFirst } from 'change-case-all';
import { NVQSTHDKProcess } from '../../../citizen-process-management/citizen-process/nvqs-process/nvqs-thdk-process/entities/nvqs-thdk-process.entity';
import { border, center, mediumBold, small } from '../../../../common/constant/report.constant';

@Injectable()
export class NvqsDkMaleCitizenService {
  constructor(
    @InjectRepository(AdministrativeUnit)
    private administrativeUnitRepository: Repository<AdministrativeUnit>,
    @InjectRepository(Citizen)
    private citizenRepository: Repository<Citizen>,
    @InjectRepository(GeneralEducationLevel)
    private generalEducationLevelRepository: Repository<GeneralEducationLevel>,
    @InjectRepository(Reason)
    private reasonRepository: Repository<Reason>,
    private minioService: MinioService,
  ) {

  }

  async export(query: ExportNvqsDkMaleCitizenDto) {
    let file = await this.minioService.findOneFile(
      "2.01B GNN-2016 KetQuaDangKiNVQSChoNamThanhNien17Tuoi",
      "report/BaoCaoTuyenQuan"
    );

    let wb = new Workbook();
    await wb.xlsx.read(file);

    let ws = wb.getWorksheet("01bGNN");

    if (!ws) return;

    const queryDistrict = await this.administrativeUnitRepository.findOne({
      where: {
        id: query.administrativeUnitId
      },
      relations: {
        parent: true,
      }
    });

    let provinceCell = ws.getCell("A2");
    provinceCell.value = provinceCell.value?.toString().replace("_tinh", queryDistrict?.parent.name.toString() ?? "");
    provinceCell.value = provinceCell.value?.toString().replace("_huyen", queryDistrict?.name.toString() ?? "");

    ws.getCell("L5").value = ws.getCell("L5").value?.toString().replaceAll("_nam", query.year.toString());
    ws.getCell("Y2").value = ws.getCell("Y2").value?.toString().replace("_nam", query.year.toString());
    ws.getCell("B13").value = ws.getCell("B13").value?.toString().replace("_nam", query.year.toString());


    const codeRow = ws.getRow(11);
    const formulaRow = ws.getRow(12);
    const header1Row = ws.getRow(13);
    const header2Row = ws.getRow(14);

    // Lấy danh sách lý do theo quá trình tạm hoãn đăng ký
    const reasons = await this.reasonRepository.findBy({
      process: {
        code: NVQS_THDK.code,
      }
    });

    // Lấy danh sách trình độ văn hóa
    const generalEducationLevels = await this.generalEducationLevelRepository.find();

    // lấy danh sách các xã theo id huyện
    let administrativeUnits = await this.administrativeUnitRepository.find({
      where: {
        parent: {
          id: query.administrativeUnitId,
        },
      },
    });

    // Tạo danh sách các dòng cần fill vào excel
    // Mỗi một instance new NvqsDkMaleCitizen() là một dòng trên excel
    let nvqsDkMaleCitizens = new Map<number, NvqsDkMaleCitizen>(
      administrativeUnits.map(e => [e.id, new NvqsDkMaleCitizen(e)])
    );

    // lấy dân số của các xã
    administrativeUnits = await this.administrativeUnitRepository.find({
      where: {
        parent: {
          id: query.administrativeUnitId,
        },
        populationHistory: {
          toDate: LessThanOrEqual(`${query.year}-12-31`),
        }
      },
      order: {
        populationHistory: {
          toDate: "DESC",
        }
      }
    });

    administrativeUnits.forEach(e => {
      let nvqsDkMaleCitizen = nvqsDkMaleCitizens.get(e.id);
      if (e.populationHistory[0] && nvqsDkMaleCitizen) {
        nvqsDkMaleCitizen.danSo = e.populationHistory[0].populationMale + e.populationHistory[0].populationFemale;
      }
    });

    // Lấy danh sách công dân tuổi 17
    let m17YearAgo = new Date();
    m17YearAgo.setFullYear(m17YearAgo.getFullYear() - 17);
    let citizens = await this.citizenRepository.find({
      where: {
        administrativeUnit: {
          parent: {
            id: query.administrativeUnitId,
          },
        },
        citizenProcesses: {
          fromDate: LessThanOrEqual(new Date(`${query.year}-12-31`))
        },
        dateOfBirth: LessThanOrEqual(m17YearAgo),
      },
      relations: {
        citizenProcesses: {
          process: true,
        },
        educationLevel: true,
        generalEducationLevel: true,
        administrativeUnit: true,
      },
      order: {
        citizenProcesses: {
          id: "DESC",
        }
      }
    });

    citizens.forEach(citizen => {
      let nvqsDkMaleCitizen = nvqsDkMaleCitizens.get(citizen.administrativeUnit.id)!;
      switch (citizen.educationLevel.code) {
        case SO_CAP_NGHE.code:
          if (nvqsDkMaleCitizen.dangHocOTruong_soCapNghe) {
            nvqsDkMaleCitizen.dangHocOTruong_soCapNghe++;
          } else {
            nvqsDkMaleCitizen.dangHocOTruong_soCapNghe = 1;
          }
          break;
        case TRUNG_CAP_NGHE.code:
          if (nvqsDkMaleCitizen.dangHocOTruong_trungCapNghe) {
            nvqsDkMaleCitizen.dangHocOTruong_trungCapNghe++;
          } else {
            nvqsDkMaleCitizen.dangHocOTruong_trungCapNghe = 1;
          }
          break;
      }

      switch (citizen.citizenProcesses[0].process.code) {
        case NVQS_MIEN.code:
          if (nvqsDkMaleCitizen.khongThuocDienDK_mienDangKy) {
            nvqsDkMaleCitizen.khongThuocDienDK_mienDangKy++;
          } else {
            nvqsDkMaleCitizen.khongThuocDienDK_mienDangKy = 1;
          }
          break;
        case NVQS_DK.code:
          if (nvqsDkMaleCitizen.thuocDienDangKy_daDangKy) {
            nvqsDkMaleCitizen.thuocDienDangKy_daDangKy++;
          } else {
            nvqsDkMaleCitizen.thuocDienDangKy_daDangKy = 1;
          }
          break;
        case NVQS_TDK.code:
          if (nvqsDkMaleCitizen.thuocDienDangKy_tron) {
            nvqsDkMaleCitizen.thuocDienDangKy_tron++;
          } else {
            nvqsDkMaleCitizen.thuocDienDangKy_tron = 1;
          }
          break;
        case NVQS_CDK.code:
          if (nvqsDkMaleCitizen.thuocDienDangKy_chong) {
            nvqsDkMaleCitizen.thuocDienDangKy_chong++;
          } else {
            nvqsDkMaleCitizen.thuocDienDangKy_chong = 1;
          }
          break;
        case NVQS_THDK.code:
          if (nvqsDkMaleCitizen[`khongThuocDienDK_${(citizen.citizenProcesses[0] as NVQSTHDKProcess)}`]) {
            nvqsDkMaleCitizen[`khongThuocDienDK_${(citizen.citizenProcesses[0] as NVQSTHDKProcess)}`]++;
          } else {
            nvqsDkMaleCitizen[`khongThuocDienDK_${(citizen.citizenProcesses[0] as NVQSTHDKProcess)}`] = 1;
          }
      }
      if (nvqsDkMaleCitizen[`giaoDucPhoThong_${citizen.generalEducationLevel.code}`]) {
        nvqsDkMaleCitizen[`giaoDucPhoThong_${citizen.generalEducationLevel.code}`]++;
      } else {
        nvqsDkMaleCitizen[`giaoDucPhoThong_${citizen.generalEducationLevel.code}`] = 1;
      }
    });

    let codeRowIndex = 6;

    // Thêm công thức tính tổng số nam đủ 17 tuổi trong năm
    formulaRow.getCell(codeRowIndex - 3).value = {
      formula: `${formulaRow.getCell(codeRowIndex - 1).address} + ${formulaRow.getCell(codeRowIndex + reasons.length + 1).address}`
    };
    // Thêm công thức tính tổng không thuộc diện đăng ký
    formulaRow.getCell(codeRowIndex - 1).value = {
      formula: `SUM(${formulaRow.getCell(codeRowIndex).address}:${formulaRow.getCell(codeRowIndex + reasons.length).address})`
    };

    // Thêm header không thuộc diện đăng ký và công thức tính tổng
    // codeRowIndex đang có giá trị 6
    for (const reason of reasons) {
      codeRow.getCell(codeRowIndex).value = `khongThuocDienDK_${reason.code}`;
      header2Row.getCell(codeRowIndex).value = reason.name;
      codeRowIndex++;
    }

    codeRow.getCell(codeRowIndex).value = "khongThuocDienDK_mienDangKy";
    header2Row.getCell(codeRowIndex).value = "Miễn đăng ký NVQS";
    ws.mergeCells(header1Row.getCell(codeRowIndex - reasons.length - 1).address, header1Row.getCell(codeRowIndex).address);
    codeRowIndex++;

    // Thêm header thuộc diện đăng ký và công thức tính tổng
    formulaRow.getCell(codeRowIndex).value = {
      formula: `SUM(${formulaRow.getCell(codeRowIndex + 1).address}:${formulaRow.getCell(codeRowIndex + 3).address})`
    };
    header1Row.getCell(codeRowIndex).value = "THUỘC DIỆN ĐĂNG KÝ"

    codeRow.getCell(codeRowIndex).value = "thuocDienDangKy_tong";
    header2Row.getCell(codeRowIndex).value = "+";
    codeRowIndex++;

    codeRow.getCell(codeRowIndex).value = "thuocDienDangKy_daDangKy";
    header2Row.getCell(codeRowIndex).value = "Đã đăng ký";
    codeRowIndex++;

    codeRow.getCell(codeRowIndex).value = "thuocDienDangKy_tron";
    header2Row.getCell(codeRowIndex).value = "Trốn";
    codeRowIndex++;

    codeRow.getCell(codeRowIndex).value = "thuocDienDangKy_chong";
    header2Row.getCell(codeRowIndex).value = "Chống";

    ws.mergeCells(header1Row.getCell(codeRowIndex - 3).address, header1Row.getCell(codeRowIndex).address);
    codeRowIndex++;

    // Thêm header Trình độ giáo dục phổ thông và công thức tính tổng
    formulaRow.getCell(codeRowIndex).value = {
      formula: `SUM(${formulaRow.getCell(codeRowIndex + 1).address}:${formulaRow.getCell(codeRowIndex + generalEducationLevels.length).address})`
    };
    header1Row.getCell(codeRowIndex).value = "TRÌNH ĐỘ GIÁO DỤC PHỔ THÔNG";

    codeRow.getCell(codeRowIndex).value = "giaoDucPhoThong_tong";
    header2Row.getCell(codeRowIndex).value = "+";
    codeRowIndex++;

    for (const generalEducationLevel of generalEducationLevels) {
      codeRow.getCell(codeRowIndex).value = `giaoDucPhoThong_${generalEducationLevel.code}`;
      header2Row.getCell(codeRowIndex).value = upperCaseFirst(generalEducationLevel.name);
      codeRowIndex++;
    }
    ws.mergeCells(header1Row.getCell(codeRowIndex - generalEducationLevels.length - 1).address, header1Row.getCell(codeRowIndex - 1).address);

    // Thêm header Đang học ở trường
    header1Row.getCell(codeRowIndex).value = "ĐANG HỌC Ở TRƯỜNG"

    codeRow.getCell(codeRowIndex).value = "dangHocOTruong_soCapNghe";
    header2Row.getCell(codeRowIndex).value = "Sơ cấp nghề";
    codeRowIndex++;

    codeRow.getCell(codeRowIndex).value = "dangHocOTruong_trungCapNghe";
    header2Row.getCell(codeRowIndex).value = "Trung cấp nghề";
    ws.mergeCells(header1Row.getCell(codeRowIndex - 1).address, header1Row.getCell(codeRowIndex).address);
    codeRowIndex++;

    // set key for columns
    codeRow.eachCell((cell, colNumber) => {
      ws!.getColumn(colNumber).key = cell.value?.toString();
    });

    let dataRowIndex = 15;
    let rowSum = ws.getRow(dataRowIndex);
    codeRow.eachCell(codeCell => {
      if (codeCell.value == "administrativeUnitName") return;
      rowSum.getCell(codeCell.col).value = {
        formula: `SUM(${ws!.getCell(dataRowIndex + 1, codeCell.col).address}:${ws!.getCell(dataRowIndex + nvqsDkMaleCitizens.size, codeCell.col).address})`
      }
    });
    dataRowIndex++;

    nvqsDkMaleCitizens.forEach(nvqsDkMaleCitizen => {
      let row = ws!.getRow(dataRowIndex);
      codeRow.eachCell(codeCell => {
        let cell = row.getCell(codeCell.col);
        if (formulaRow.getCell(codeCell.col).value) {
          cell.value = { sharedFormula: formulaRow.getCell(codeCell.col).address }
        } else {
          cell.value = nvqsDkMaleCitizen[codeCell.value as string];
        }
      });
      dataRowIndex++;
    });

    // draw border
    for (let i = 13; i < dataRowIndex; i++) {
      for (let j = 1; j < codeRowIndex; j++) {
        let cell = ws.getCell(i, j);
        cell.border = border;
        cell.font = small;
        cell.alignment = center;
      }
    }

    header1Row.font = mediumBold;

    return wb.xlsx.writeBuffer();
  }
}
