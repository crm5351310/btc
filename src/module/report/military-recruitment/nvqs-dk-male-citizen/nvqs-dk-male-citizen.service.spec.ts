import { Test, TestingModule } from '@nestjs/testing';
import { NvqsDkMaleCitizenService } from './nvqs-dk-male-citizen.service';

describe('NvqsDkMaleCitizenService', () => {
  let service: NvqsDkMaleCitizenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NvqsDkMaleCitizenService],
    }).compile();

    service = module.get<NvqsDkMaleCitizenService>(NvqsDkMaleCitizenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
