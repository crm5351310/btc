import { ApiProperty } from "@nestjs/swagger";
import { IsNumber } from "class-validator";

export class ExportNvqsDkMaleCitizenDto {
  @ApiProperty({
    description: "Id huyện",
    example: 4,
  })
  @IsNumber()
  administrativeUnitId: number;

  @ApiProperty({
    description: "Năm",
    example: 2024,
  })
  @IsNumber()
  year: number;
}