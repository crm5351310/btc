import { Controller, Get, Query, Res } from '@nestjs/common';
import { NvqsDkMaleCitizenService } from './nvqs-dk-male-citizen.service';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { ReportExportUtil } from '../../../../common/utils/report.util';
import { ExportNvqsDkMaleCitizenDto } from './dto/export-nvqs-dk-male-citizen.dto';

@Controller()
@ApiTags("KẾT QUẢ ĐĂNG KÝ NGHĨA VỤ QUÂN SỰ CHO CÔNG DÂN NAM ĐỦ 17 TUỔI TRONG NĂM")
export class NvqsDkMaleCitizenController {
  constructor(private readonly nvqsDkMaleCitizenService: NvqsDkMaleCitizenService) { }

  @Get()
  async export(
    @Query() query: ExportNvqsDkMaleCitizenDto,
    @Res() res: Response
  ) {
    let result = await this.nvqsDkMaleCitizenService.export(query);
    res.status(200).set(ReportExportUtil.header('KẾT QUẢ ĐĂNG KÝ NGHĨA VỤ QUÂN SỰ CHO CÔNG DÂN NAM ĐỦ 17 TUỔI TRONG NĂM')).send(result);
  }
}
