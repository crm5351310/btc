import { AdministrativeUnit } from "../../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity";

export class NvqsDkMaleCitizen {
  administrativeUnitName: string;
  danSo?: number;
  khongThuocDienDK_mienDangKy?: number;
  thuocDienDangKy_daDangKy?: number;
  thuocDienDangKy_tron?: number;
  thuocDienDangKy_chong?: number;
  // khongBietChu?: number;
  // lop1?: number;
  // lop2?: number;
  // lop3?: number;
  // lop4?: number;
  // lop5?: number;
  // lop6?: number;
  // lop7?: number;
  // lop8?: number;
  // lop9?: number;
  // lop10?: number;
  // lop11?: number;
  // lop12?: number;
  dangHocOTruong_soCapNghe?: number;
  dangHocOTruong_trungCapNghe?: number;

  [index: string]: any;

  constructor(administrativeUnit: AdministrativeUnit) {
    this.administrativeUnitName = administrativeUnit.name;
  }
}
