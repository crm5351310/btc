import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { CitizensAtLeastSeventeenModule } from './citizens-at-least-seventeen/citizens-at-least-seventeen.module';
import { NvqsDkMaleCitizenModule } from './nvqs-dk-male-citizen/nvqs-dk-male-citizen.module';

@Module({
  imports: [CitizensAtLeastSeventeenModule, NvqsDkMaleCitizenModule]
})
export class MilitaryRecruitmentModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment',
    module: MilitaryRecruitmentModule,
    children: [
      CitizensAtLeastSeventeenModule.route,
      NvqsDkMaleCitizenModule.route,
    ]
  }
}
