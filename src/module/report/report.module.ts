import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { MilitaryRecruitmentModule } from './military-recruitment/military-recruitment.module';

@Module({
  imports: [
    MilitaryRecruitmentModule,
  ]
})
export class ReportModule {
  static readonly route: RouteTree = {
    path: 'report',
    module: ReportModule,
    children: [
      MilitaryRecruitmentModule.route
    ]
  }
}
