import { Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { MinioService } from '../minio/minio.service';

@Injectable()
export class ImportService {
  protected optional = [{ value: 'Có' }, { value: 'Không' }];
  constructor(
    // eslint-disable-next-line no-unused-vars
    protected readonly entityManager: EntityManager,
    // eslint-disable-next-line no-unused-vars
    protected readonly minioService: MinioService,
  ) {}

  dropdownString(table: string, data: object[]): string {
    return table + '!$A$3:$A$' + (data.length + 3);
  }

  dropdownStringV2(table: string, data: object[]): string {
    return `'${table}'` + `!$A$1:$${this.getColumnLetter(data.length + 1)}$1`;
  }

  getColumnLetter(columnNumber: number): string {
    let columnLetter = '';
    let tempNumber = columnNumber;

    while (tempNumber > 0) {
      const remainder = (tempNumber - 1) % 26;
      columnLetter = String.fromCharCode(65 + remainder) + columnLetter;
      tempNumber = Math.floor((tempNumber - remainder) / 26);
    }

    return columnLetter;
  }

  createDropdown(col: number, sheet: string, data: object[]) {
    return {
      col: col,
      data: this.dropdownString(sheet, data),
    };
  }

  async getData(proc: string, data?: string[] | number[]) {
    let list: string[] | string = [];
    let procedure = `CALL ${proc}()`;
    if (data) {
      for (let i = 0; i < data.length; i++) {
        list.push('?');
      }
      list = list.join(',');
      procedure = `CALL ${proc}(${list})`;
      return (await this.entityManager.query(procedure, data))[0] as object[];
    }
    return (await this.entityManager.query(procedure))[0] as object[];
  }

  getDataV3 = async (proc: string, data?: string[]) => await Promise.resolve(this.getData(proc, data))

  getDataV4 = async (item: object) => {return Promise.resolve(item)}

  refill = (col: number, result: object[], sheet: string, nameCol: string) => {
    return {
      col: col,
      range: this.dropdownStringV2(sheet, result),
      ref: result,
      nameCol: nameCol,
      sheet: sheet
    }
  }
  async getDataMultiple(proc: string, data?: any[]): Promise<any[]> {
    let list: string[] | string = [];
    let procedure = `CALL ${proc}()`;
    if (data) {
      for (let i = 0; i < data.length; i++) {
        list.push('?');
      }
      list = list.join(',');
      procedure = `CALL ${proc}(${list})`;
      return (await this.entityManager.query(procedure, data)) as any[];
    }
    return (await this.entityManager.query(procedure)) as any[];
  }
}
