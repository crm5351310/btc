import { Module } from '@nestjs/common';
import { MinioService } from '../minio/minio.service';

@Module({
  providers: [MinioService],
})
export class ImportModule {}
