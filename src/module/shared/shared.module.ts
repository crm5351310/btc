import { Module } from '@nestjs/common';
import { MinioModule } from './minio/minio.module';
import { ImportModule } from "./import/import.module";
@Module({
  imports: [MinioModule, ImportModule],
})
export class ShareModule {}
