import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import {
  MilitaryRankTypeModule,
  militaryRankTypeRouter,
} from './military-rank-type/military-rank-type.module';
import { MilitaryRankModule, militaryRankRouter } from './military-rank/military-rank.module';

@Module({
  imports: [MilitaryRankModule, MilitaryRankTypeModule],
})
export class MilitaryRankRootModule {}
export const militaryRankRootRouter: RouteTree = {
  path: 'military-rank-root',
  module: MilitaryRankRootModule,
  children: [militaryRankTypeRouter, militaryRankRouter],
};
