import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../common/base/class/base.class';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { MilitaryRankType } from '../military-rank-type/military-rank-type.entity';

@Entity()
export class MilitaryRank extends BaseCategoryEntity {
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  @ApiProperty({
    description: 'Loại quân hàm',
    type: RelationTypeBase,
  })
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(MilitaryRankType, ['id']))
  @ManyToOne(() => MilitaryRankType, (militaryRankType) => militaryRankType.militaryRanks)
  militaryRankType: MilitaryRankType;
}
