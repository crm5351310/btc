import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../../common/base/service/base-category.service';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
  ResponseDelete,
} from '../../../../common/response';
import { In, Repository } from 'typeorm';
import { CreateMilitaryRankDto } from './dto/create-military-rank.dto';
import { QueryMilitaryRankDto } from './dto/query-military-rank.dto';
import { UpdateMilitaryRankDto } from './dto/update-military-rank.dto';
import { MilitaryRank } from './military-rank.entity';
@Injectable()
export class MilitaryRankService extends BaseCategoryService<MilitaryRank> {
  constructor(
    @InjectRepository(MilitaryRank)
    responsitory: Repository<MilitaryRank>,
  ) {
    super(responsitory);
  }
  async create(dto: CreateMilitaryRankDto): Promise<ResponseCreated<MilitaryRank>> {
    await this.checkExist(['name', 'code'], dto);

    const militaryRank = await this.repository.save(dto);
    return new ResponseCreated(militaryRank);
  }
  async findAll(dto: QueryMilitaryRankDto): Promise<ResponseFindAll<MilitaryRank>> {
    const [results, total] = await this.repository.findAndCount({
      relations: {
        militaryRankType: true,
      },
      where: {
        militaryRankType: {
          id: dto.militaryRankTypeId ?? undefined,
        },
      },
      skip: dto.limit && dto.offset,
      take: dto.limit,
    });
    return new ResponseFindAll(results, total);
  }
  async findOne(id: number): Promise<ResponseFindOne<MilitaryRank>> {
    const result = await this.checkNotExist(id, MilitaryRank.name);
    return new ResponseFindOne(result);
  }
  async update(id: number, dto: UpdateMilitaryRankDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitaryRank.name);
    await this.checkExist(['name', 'code'], dto, id);
    const result = await this.repository.save({ id, ...dto });

    return new ResponseUpdate(result);
  }
  async remove(ids: number[]): Promise<ResponseDelete<MilitaryRank>> {
    const results = await this.repository.find({
      relations: {
        militaryRankType: true,
      },
      where: {
        id: In(ids),
      },
    });
    if (results.length === 0) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, MilitaryRank.name, true);
    }
    await this.repository.softRemove(results);
    return new ResponseDelete<MilitaryRank>(results, ids);
  }
}
