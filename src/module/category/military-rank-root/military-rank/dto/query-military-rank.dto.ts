import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { BaseQueryDto } from '../../../../../common/base/dto/base-query.dto';

export class QueryMilitaryRankDto extends BaseQueryDto {
  @ApiProperty({
    name: 'militaryRankTypeId',
    required: false,
    description: 'ID của loại quân hàm',
  })
  @IsOptional()
  militaryRankTypeId?: number;
}
