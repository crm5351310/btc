import { OmitType } from '@nestjs/swagger';
import { MilitaryRank } from '../military-rank.entity';

export class CreateMilitaryRankDto extends OmitType(MilitaryRank, ['id' as const]) {}
