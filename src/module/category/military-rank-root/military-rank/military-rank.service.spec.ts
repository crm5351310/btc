import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
  ResponseDelete,
} from '../../../../common/response';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { CreateMilitaryRankDto } from './dto/create-military-rank.dto';
import { UpdateMilitaryRankDto } from './dto/update-military-rank.dto';
import { MilitaryRank } from './military-rank.entity';
import { MilitaryRankService } from './military-rank.service';

describe('MilitaryRankService', () => {
  let service: MilitaryRankService;
  let repository: Repository<MilitaryRank>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryRankService,
        {
          provide: getRepositoryToken(MilitaryRank),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<MilitaryRankService>(MilitaryRankService);
    repository = module.get(getRepositoryToken(MilitaryRank));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryRank.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryRank>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryRank;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryRank, militaryRank.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitaryRank>) => {
        return militaryRank.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitaryRank>).code ||
            e.name == (options.where as FindOptionsWhere<MilitaryRank>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryRank) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryRank.length,
        };
      }
    });
  });

  const militaryRank = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      militaryRankType: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      militaryRankType: {
        id: 1,
      },
    },
  ] as MilitaryRank[];

  describe('MilitaryRankService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        militaryRankType: {
          id: 1,
        },
      } as CreateMilitaryRankDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
        militaryRankType: {
          id: 1,
        },
      } as CreateMilitaryRankDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('MilitaryRankService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryRank.length);
    });
  });

  describe('MilitaryRankService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('MilitaryRankService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitaryRankDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitaryRankDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('MilitaryRankService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
