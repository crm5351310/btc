import { Module } from '@nestjs/common';
import { MilitaryRankService } from './military-rank.service';
import { MilitaryRankController } from './military-rank.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRank } from './military-rank.entity';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryRank])],
  controllers: [MilitaryRankController],
  providers: [MilitaryRankService],
  exports: [MilitaryRankService],
})
export class MilitaryRankModule {}
export const militaryRankRouter: RouteTree = {
  path: 'military-rank',
  module: MilitaryRankModule,
  children: [],
};
