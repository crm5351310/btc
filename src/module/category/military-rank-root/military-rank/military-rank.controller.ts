import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseArrayPipe,
  Query,
} from '@nestjs/common';
import { MilitaryRankService } from './military-rank.service';
import { CreateMilitaryRankDto } from './dto/create-military-rank.dto';
import { UpdateMilitaryRankDto } from './dto/update-military-rank.dto';
import { QueryMilitaryRankDto } from './dto/query-military-rank.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.MILITARY_RANK)
export class MilitaryRankController {
  constructor(private readonly militaryRankService: MilitaryRankService) {}

  @Post()
  create(@Body() createMilitaryRankDto: CreateMilitaryRankDto) {
    return this.militaryRankService.create(createMilitaryRankDto);
  }

  @Get()
  findAll(@Query() queryMilitaryRankDto: QueryMilitaryRankDto) {
    return this.militaryRankService.findAll(queryMilitaryRankDto);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.militaryRankService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMilitaryRankDto: UpdateMilitaryRankDto) {
    return this.militaryRankService.update(+id, updateMilitaryRankDto);
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryRankService.remove(ids);
  }
}
