import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, IsOptional } from 'class-validator';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { MilitaryRank } from '../military-rank/military-rank.entity';

@Entity()
export class MilitaryRankType extends BaseCategoryEntity {
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  @OneToMany(() => MilitaryRank, (militaryRank) => militaryRank.militaryRankType)
  militaryRanks: MilitaryRank[];
}
