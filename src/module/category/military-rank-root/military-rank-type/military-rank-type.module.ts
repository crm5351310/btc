import { Module } from '@nestjs/common';
import { MilitaryRankTypeService } from './military-rank-type.service';
import { MilitaryRankTypeController } from './military-rank-type.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRankType } from './military-rank-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryRankType])],
  controllers: [MilitaryRankTypeController],
  providers: [MilitaryRankTypeService],
})
export class MilitaryRankTypeModule {}
export const militaryRankTypeRouter: RouteTree = {
  path: 'military-rank-type',
  module: MilitaryRankTypeModule,
  children: [],
};
