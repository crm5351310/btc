import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseArrayPipe,
  Query,
} from '@nestjs/common';
import { MilitaryRankTypeService } from './military-rank-type.service';
import { CreateMilitaryRankTypeDto } from './dto/create-military-rank-type.dto';
import { UpdateMilitaryRankTypeDto } from './dto/update-military-rank-type.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { QueryMilitaryRankTypeDto } from './dto/query-military-rank-type.dto';

@Controller()
@ApiTags(TagEnum.MILITARY_RANK_TYPE)
export class MilitaryRankTypeController {
  constructor(private readonly militaryRankTypeService: MilitaryRankTypeService) {}

  @Post()
  create(@Body() createMilitaryRankTypeDto: CreateMilitaryRankTypeDto) {
    return this.militaryRankTypeService.create(createMilitaryRankTypeDto);
  }

  @Get()
  findAll(@Query() queryMilitaryRankTypeDto: QueryMilitaryRankTypeDto) {
    return this.militaryRankTypeService.findAll(queryMilitaryRankTypeDto);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.militaryRankTypeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateMilitaryRankTypeDto: UpdateMilitaryRankTypeDto) {
    return this.militaryRankTypeService.update(+id, updateMilitaryRankTypeDto);
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryRankTypeService.remove(ids);
  }
}
