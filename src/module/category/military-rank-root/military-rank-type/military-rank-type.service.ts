import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../../common/base/service/base-category.service';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
  ResponseDelete,
} from '../../../../common/response';
import { In, Repository } from 'typeorm';
import { CreateMilitaryRankTypeDto } from './dto/create-military-rank-type.dto';
import { QueryMilitaryRankTypeDto } from './dto/query-military-rank-type.dto';
import { UpdateMilitaryRankTypeDto } from './dto/update-military-rank-type.dto';
import { MilitaryRankType } from './military-rank-type.entity';

@Injectable()
export class MilitaryRankTypeService extends BaseCategoryService<MilitaryRankType> {
  constructor(
    @InjectRepository(MilitaryRankType)
    repository: Repository<MilitaryRankType>,
  ) {
    super(repository);
  }
  async create(dto: CreateMilitaryRankTypeDto): Promise<ResponseCreated<MilitaryRankType>> {
    await this.checkExist(['name', 'code'], dto);

    const militaryRankType = await this.repository.save(dto);

    return new ResponseCreated(militaryRankType);
  }
  async findAll(dto: QueryMilitaryRankTypeDto): Promise<ResponseFindAll<MilitaryRankType>> {
    const [results, total] = await this.repository.findAndCount({
      skip: dto.limit && dto.offset,
      take: dto.limit,
    });
    return new ResponseFindAll(results, total);
  }
  async findOne(id: number): Promise<ResponseFindOne<MilitaryRankType>> {
    const result = await this.checkNotExist(id, MilitaryRankType.name);
    return new ResponseFindOne(result);
  }
  async update(id: number, dto: UpdateMilitaryRankTypeDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitaryRankType.name);
    await this.checkExist(['name', 'code'], dto, id);

    const result = await this.repository.save({ id, ...dto });
    return new ResponseUpdate(result);
  }
  async remove(ids: number[]): Promise<ResponseDelete<MilitaryRankType>> {
    const results = await this.repository.find({
      relations: {
        militaryRanks: true,
      },
      where: {
        id: In(ids),
      },
    });
    const rows = results.reduce((pre: MilitaryRankType[], cur: MilitaryRankType) => {
      if (!cur.militaryRanks || cur.militaryRanks?.length === 0) {
        pre.push(cur);
      }
      return pre;
    }, []);
    if (rows.length === 0) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, MilitaryRankType.name, true);
    }
    await this.repository.softRemove(rows);
    return new ResponseDelete<MilitaryRankType>(results, ids, rows);
  }
}
