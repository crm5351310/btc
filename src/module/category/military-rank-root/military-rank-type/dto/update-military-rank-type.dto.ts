import { PartialType } from '@nestjs/swagger';
import { CreateMilitaryRankTypeDto } from './create-military-rank-type.dto';

export class UpdateMilitaryRankTypeDto extends PartialType(CreateMilitaryRankTypeDto) {}
