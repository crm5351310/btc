import { OmitType } from '@nestjs/swagger';
import { MilitaryRankType } from '../military-rank-type.entity';

export class CreateMilitaryRankTypeDto extends OmitType(MilitaryRankType, ['id' as const]) {}
