import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
  ResponseDelete,
} from '../../../../common/response';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { CreateMilitaryRankTypeDto } from './dto/create-military-rank-type.dto';
import { UpdateMilitaryRankTypeDto } from './dto/update-military-rank-type.dto';
import { MilitaryRankType } from './military-rank-type.entity';
import { MilitaryRankTypeService } from './military-rank-type.service';

describe('MilitaryRankTypeService', () => {
  let service: MilitaryRankTypeService;
  let repository: Repository<MilitaryRankType>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryRankTypeService,
        {
          provide: getRepositoryToken(MilitaryRankType),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<MilitaryRankTypeService>(MilitaryRankTypeService);
    repository = module.get(getRepositoryToken(MilitaryRankType));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryRankType.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryRankType>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryRankType;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryRankType, militaryRankType.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitaryRankType>) => {
        return militaryRankType.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitaryRankType>).code ||
            e.name == (options.where as FindOptionsWhere<MilitaryRankType>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryRankType) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryRankType.length,
        };
      }
    });
  });

  const militaryRankType = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as MilitaryRankType[];

  describe('MilitaryRankTypeService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateMilitaryRankTypeDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateMilitaryRankTypeDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('MilitaryRankTypeService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryRankType.length);
    });
  });

  describe('MilitaryRankTypeService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('MilitaryRankTypeService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitaryRankTypeDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitaryRankTypeDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('MilitaryRankTypeService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
