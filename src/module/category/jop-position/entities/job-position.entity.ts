import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { Column, Entity, ManyToMany } from 'typeorm';
import { PathDto } from '../../../../common/base/class/base.class';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { GroupProcess } from '../../group-process/entities/group-process.entity';

@Entity()
export class JobPosition extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validator
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;

  // swagger
  @ApiProperty({
    description: 'Nhóm quá trình',
    type: [PathDto],
  })
  // validator
  @IsDefined()
  @ValidateNested()
  // orm
  @ManyToMany(() => GroupProcess, (item) => item.jobPositions)
  groupProcesses: GroupProcess[];
}
