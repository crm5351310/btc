import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { GroupProcess } from '../group-process/entities/group-process.entity';
import { GroupProcessService } from '../group-process/group-process.service';
import { CreateJobPositionDto } from './dto/create-job-position.dto';
import { UpdateJobPositionDto } from './dto/update-job-position.dto';
import { JobPosition } from './entities';
import { JobPositionService } from './job-position.service';

//npm run test:run src/module/category/jop-position

describe('JobPositionService', () => {
  let service: JobPositionService;
  let repository: Repository<JobPosition>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JobPositionService,
        {
          provide: getRepositoryToken(JobPosition),
          useValue: createStubInstance(Repository),
        },
        GroupProcessService,
        {
          provide: getRepositoryToken(GroupProcess),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<JobPositionService>(JobPositionService);
    repository = module.get(getRepositoryToken(JobPosition));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        jobPositions.find(
          (value) => value.id === (options.where as FindOptionsWhere<JobPosition>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return jobPositions;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [jobPositions, jobPositions.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<JobPosition>) => {
        return jobPositions.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<JobPosition>).code ||
            e.name == (options.where as FindOptionsWhere<JobPosition>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: JobPosition) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: jobPositions.length,
        };
      }
    });
  });

  const jobPositions = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as JobPosition[];

  describe('JobPositionService => create', () => {
    it('Tạo mới chức vụ thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateJobPositionDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateJobPositionDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('JobPositionService => findAll', () => {
    it('Tìm tất cả chức vụ', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(jobPositions.length);
    });
  });

  describe('JobPositionService = > findOne', () => {
    it('Tìm thành công một chức vụ', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một chức vụ không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('JobPositionService = > update', () => {
    it('Cập nhật chức vụ thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateJobPositionDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateJobPositionDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('JobPositionService', () => {
    it('Xóa chức vụ  thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
