import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateJobPositionDto } from './dto/create-job-position.dto';
import { QueryJobPositionDto } from './dto/query-job-position.dto';
import { UpdateJobPositionDto } from './dto/update-job-position.dto';
import { JobPositionService } from './job-position.service';

@Controller()
@ApiTags(TagEnum.JOB_POSITION)
export class JobPositionController {
  constructor(private readonly jobPositionService: JobPositionService) {}

  @Post()
  create(@Body() createJobPositionDto: CreateJobPositionDto) {
    return this.jobPositionService.create(createJobPositionDto);
  }

  @Get()
  findAll(@Query() query: QueryJobPositionDto) {
    return this.jobPositionService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.jobPositionService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateJobPositionDto: UpdateJobPositionDto) {
    return this.jobPositionService.update(+id, updateJobPositionDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.jobPositionService.remove(ids);
  }
}
