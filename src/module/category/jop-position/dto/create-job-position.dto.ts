import { OmitType } from '@nestjs/swagger';
import { JobPosition } from '../entities/job-position.entity';

export class CreateJobPositionDto extends OmitType(JobPosition, ['id']) {}
