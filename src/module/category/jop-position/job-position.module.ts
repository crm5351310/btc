import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobPosition } from './entities';
import { JobPositionController } from './job-position.controller';
import { JobPositionService } from './job-position.service';
import { GroupProcess } from '../group-process/entities/group-process.entity';

@Module({
  imports: [TypeOrmModule.forFeature([JobPosition, GroupProcess])],
  controllers: [JobPositionController],
  providers: [JobPositionService],
  exports: [JobPositionService],
})
export class JobPositionModule {
  static readonly route: RouteTree = {
    path: 'job-position',
    module: JobPositionModule,
  };
}
