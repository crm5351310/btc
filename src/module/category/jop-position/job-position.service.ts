import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { GroupProcess } from '../group-process/entities/group-process.entity';
import { CreateJobPositionDto } from './dto/create-job-position.dto';
import { QueryJobPositionDto } from './dto/query-job-position.dto';
import { UpdateJobPositionDto } from './dto/update-job-position.dto';
import { JobPosition } from './entities';

@Injectable()
export class JobPositionService extends BaseCategoryService<JobPosition> {
  constructor(
    @InjectRepository(JobPosition)
    repository: Repository<JobPosition>,

    @InjectRepository(GroupProcess)
    private readonly groupProcessRepository: Repository<GroupProcess>,
  ) {
    super(repository);
  }

  async create(createJobPositionDto: CreateJobPositionDto) {
    await this.checkExist(['name', 'code'], createJobPositionDto);

    const jobPosition: JobPosition = await this.repository.save(createJobPositionDto);

    return new ResponseCreated(jobPosition);
  }

  async findAll(query: QueryJobPositionDto): Promise<ResponseFindAll<JobPosition>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
      relations: {
        groupProcesses: true,
      },
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<JobPosition>> {
    await this.checkNotExist(id, JobPosition.name);

    const result = await this.repository.findOne({
      where: { id },
      relations: {
        groupProcesses: true,
      },
    });

    return new ResponseFindOne(result);
  }

  async update(id: number, updateJobPositionDto: UpdateJobPositionDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, JobPosition.name);
    await this.checkExist(['name', 'code'], updateJobPositionDto, id);

    const result = await this.repository.save({
      id,
      ...updateJobPositionDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<JobPosition>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<JobPosition>(results, ids);
  }
}
