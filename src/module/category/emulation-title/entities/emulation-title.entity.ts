import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { Type } from 'class-transformer';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { AwardLevel } from '../../award-level/award-level.entity';
import { RankAward } from '../../rank-award/rank-award.entity';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';

@Entity()
export class EmulationTitle extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Cấp khen thưởng',
    type: () => AwardLevel,
  })
  // validate
  @IsObjectRelation()
  // entity
  @ManyToOne(() => AwardLevel, (item) => item.emulationTitle)
  awardLevel: AwardLevel;

  //swagger
  @ApiProperty({
    description: 'Xếp loại khen thưởng',
    type: () => RankAward,
  })
  // validate
  @IsObjectRelation()
  // entity
  @ManyToOne(() => RankAward, (item) => item.emulationTitle)
  rankAward: RankAward;

  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
    required: false,
  })
  // validate
  @IsOptional()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;
}
