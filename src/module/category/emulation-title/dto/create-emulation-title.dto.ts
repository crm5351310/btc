import { OmitType } from '@nestjs/swagger';
import { EmulationTitle } from '../entities/emulation-title.entity';

export class CreateEmulationTitleDto extends OmitType(EmulationTitle, ['id'] as const) {}
