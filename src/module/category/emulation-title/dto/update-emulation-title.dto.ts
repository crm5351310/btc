import { PartialType } from '@nestjs/swagger';
import { CreateEmulationTitleDto } from './create-emulation-title.dto';

export class UpdateEmulationTitleDto extends PartialType(CreateEmulationTitleDto) {}
