import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from 'src/common/response';
import { EmulationTitleService } from './emulation-title.service';
import { EmulationTitle } from './entities/emulation-title.entity';
import { CreateEmulationTitleDto } from './dto/create-emulation-title.dto';
import { UpdateEmulationTitleDto } from './dto/update-emulation-title.dto';
import { AwardLevelService } from '../award-level/award-level.service';
import { RankAwardService } from '../rank-award/rank-award.service';

describe('EmulationTitleService', () => {
  let service: EmulationTitleService;
  let repository: Repository<EmulationTitle>;

  let rankAwardService: RankAwardService;
  let awardLevelService: AwardLevelService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmulationTitleService,
        {
          provide: getRepositoryToken(EmulationTitle),
          useValue: createStubInstance(Repository),
        },
        {
          provide: RankAwardService,
          useValue: createStubInstance(RankAwardService),
        },
        {
          provide: AwardLevelService,
          useValue: createStubInstance(AwardLevelService),
        },
      ],
    }).compile();

    service = module.get<EmulationTitleService>(EmulationTitleService);
    repository = module.get(getRepositoryToken(EmulationTitle));
    rankAwardService = module.get(RankAwardService);
    awardLevelService = module.get(AwardLevelService);

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        awards.find(
          (value) => value.id === (options.where as FindOptionsWhere<EmulationTitle>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return awards;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [awards, awards.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<EmulationTitle>) => {
        return awards.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<EmulationTitle>).code ||
            e.name == (options.where as FindOptionsWhere<EmulationTitle>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: EmulationTitle) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: awards.length,
        };
      }
    });
  });

  const awards = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      awardLevel: {
        id: 1,
        code: 'code1',
        name: 'name1',
        description: 'mo ta 1',
      },
      rankAward: {
        id: 1,
        code: 'code1',
        name: 'name1',
        description: 'mo ta 1',
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      awardLevel: {
        id: 1,
        code: 'code1',
        name: 'name1',
        description: 'mo ta 1',
      },
      rankAward: {
        id: 1,
        code: 'code1',
        name: 'name1',
        description: 'mo ta 1',
      },
    },
    {
      id: 3,
      code: 'code3',
      name: 'name3',
      description: 'mo ta 3',
      awardLevel: {
        id: 1,
        code: 'code1',
        name: 'name1',
        description: 'mo ta 1',
      },
      rankAward: {
        id: 1,
        code: 'code1',
        name: 'name1',
        description: 'mo ta 1',
      },
    },
  ] as EmulationTitle[];

  describe('AwardLevelService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        awardLevel: {
          id: 2,
        },
        rankAward: {
          id: 2,
        },
      } as CreateEmulationTitleDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateEmulationTitleDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('AwardLevelService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(awards.length);
    });
  });

  describe('AwardLevelService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 5;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('AwardLevelService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateEmulationTitleDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateEmulationTitleDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });

    it('Cập nhật không thành công do bản ghi không tồn tại', async () => {
      const id = 6;
      const payload = {
        code: 'code21231',
        name: 'name2312',
      } as UpdateEmulationTitleDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('AwardLevelService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3, 4, 5]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });

    it('Xóa không thành công do bản ghi không tồn tại', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      expect(service.remove([5])).rejects.toThrowError;
    });
  });
});
