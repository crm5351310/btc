import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { AwardLevel } from '../award-level/award-level.entity';
import { RankAward } from '../rank-award/rank-award.entity';
import { EmulationTitle } from './entities/emulation-title.entity';
import { CreateEmulationTitleDto } from './dto/create-emulation-title.dto';
import { UpdateEmulationTitleDto } from './dto/update-emulation-title.dto';
import { QueryEmulationTitle } from './dto/query-emulation-title.dto';
import { RankAwardService } from '../rank-award/rank-award.service';
import { AwardLevelService } from '../award-level/award-level.service';
import { CustomBadRequestException } from "../../../common/exception/bad.exception";
import { ContentMessage } from "../../../common/message/content.message";
import { FieldMessage } from "../../../common/message/field.message";

@Injectable()
export class EmulationTitleService extends BaseCategoryService<EmulationTitle> {
  constructor(
    protected readonly rankAwardService: RankAwardService,

    protected readonly awardLevelService: AwardLevelService,

    @InjectRepository(EmulationTitle)
    repository: Repository<EmulationTitle>,
  ) {
    super(repository);
  }

  async create(createDto: CreateEmulationTitleDto): Promise<ResponseCreated<EmulationTitle>> {
    await this.checkExist(['code', 'name'], createDto);

    const emlationTitle: EmulationTitle = await this.repository.save(createDto);

    return new ResponseCreated(emlationTitle);
  }

  async update(id: number, dto: UpdateEmulationTitleDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, EmulationTitle.name);
    await this.checkExist(['name', 'code'], dto, id);
    const res = await this.repository.save({ id, ...dto });
    return new ResponseUpdate(res);
  }

  async remove(id: number[]): Promise<ResponseDelete<EmulationTitle>> {
    const res = await this.repository.find({
      where: {
        id: In(id),
      },
    });
    await this.repository.remove(res);
    return new ResponseDelete<EmulationTitle>(res, id);
  }

  async findAll(dto: QueryEmulationTitle): Promise<ResponseFindAll<EmulationTitle>> {
    const [results, total] = await this.repository.findAndCount({
      skip: dto.limit && dto.offset,
      take: dto.limit,
      relations: {
        rankAward: true,
        awardLevel: true,
      },
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<EmulationTitle>> {
    const inUses = await this.repository.findOne({
      where: {
        id: id,
      },
      relations: {
        rankAward: true,
        awardLevel: true,
      }
    });

    if (!inUses)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.EMULATION_TITLE,
        true,
      );

    return new ResponseFindOne(inUses);
  }
}
