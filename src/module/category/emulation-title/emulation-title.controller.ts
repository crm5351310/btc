import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { TagEnum } from 'src/common/enum/tag.enum';
import { ApiTags } from '@nestjs/swagger';
import { EmulationTitleService } from './emulation-title.service';
import { CreateEmulationTitleDto } from './dto/create-emulation-title.dto';
import { QueryEmulationTitle } from './dto/query-emulation-title.dto';
import { UpdateEmulationTitleDto } from './dto/update-emulation-title.dto';

@Controller()
@ApiTags(TagEnum.EMULATION_TITLE)
export class EmulationTitleConTroller {
  constructor(private readonly service: EmulationTitleService) { }

  @Post()
  create(@Body() createDto: CreateEmulationTitleDto) {
      return this.service.create(createDto);
  }

  @Get()
  findAll(@Query() query: QueryEmulationTitle) {
      return this.service.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
      return this.service.findOne(+id);
  }

  @Patch(':id')
  update(
      @Param('id') id: string,
      @Body() updateEmulationTitleDto: UpdateEmulationTitleDto,
  ) {
      return this.service.update(+id, updateEmulationTitleDto);
  }

  @Delete(':ids')
  remove(
      @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
      ids: number[],
  ) {
      return this.service.remove(ids);
  }
}
