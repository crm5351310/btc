import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmulationTitleService } from './emulation-title.service';
import { EmulationTitle } from './entities/emulation-title.entity';
import { EmulationTitleConTroller } from './emulation-title.controller';
import { AwardLevelModule } from '../award-level/award-level.module';
import { RankAwardModule } from '../rank-award/rank-award.module';

@Module({
  imports: [TypeOrmModule.forFeature([EmulationTitle]), AwardLevelModule, RankAwardModule],
  controllers: [EmulationTitleConTroller],
  providers: [EmulationTitleService],
  exports: [EmulationTitleService],
})
export class EmulationTitleModule {
  static readonly route: RouteTree = {
    path: 'emulation-title',
    module: EmulationTitleModule,
  };
}
