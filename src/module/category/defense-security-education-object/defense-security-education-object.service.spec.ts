import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { DefenseSecurityEducationObjectService } from './defense-security-education-object.service';
import { CreateDefenseSecurityEducationObjectDto } from './dto/create-defense-security-education-object.dto';
import { UpdateDefenseSecurityEducationObjectDto } from './dto/update-defense-security-education-object.dto';
import { DefenseSecurityEducationObject } from './entities/defense-security-education-object.entity';

describe('DefenseSecurityEducationObjectService', () => {
  let service: DefenseSecurityEducationObjectService;
  let repository: Repository<DefenseSecurityEducationObject>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefenseSecurityEducationObjectService,
        {
          provide: getRepositoryToken(DefenseSecurityEducationObject),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<DefenseSecurityEducationObjectService>(
      DefenseSecurityEducationObjectService,
    );
    repository = module.get(getRepositoryToken(DefenseSecurityEducationObject));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        defenseSecurityEducationObjects.find(
          (value) =>
            value.id === (options.where as FindOptionsWhere<DefenseSecurityEducationObject>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return defenseSecurityEducationObjects;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [defenseSecurityEducationObjects, defenseSecurityEducationObjects.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<DefenseSecurityEducationObject>) => {
        return defenseSecurityEducationObjects.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<DefenseSecurityEducationObject>).code ||
            e.name == (options.where as FindOptionsWhere<DefenseSecurityEducationObject>).name,
        );
      });

    jest
      .spyOn(repository, 'save')
      .mockImplementation(async (entity: DefenseSecurityEducationObject) => {
        if (entity.id) {
          return entity;
        } else {
          return {
            ...entity,
            id: defenseSecurityEducationObjects.length,
          };
        }
      });
  });

  const defenseSecurityEducationObjects = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as DefenseSecurityEducationObject[];

  describe('DefenseSecurityEducationObjectService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateDefenseSecurityEducationObjectDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateDefenseSecurityEducationObjectDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('DefenseSecurityEducationObjectService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(defenseSecurityEducationObjects.length);
    });
  });

  describe('DefenseSecurityEducationObjectService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('DefenseSecurityEducationObjectService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateDefenseSecurityEducationObjectDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateDefenseSecurityEducationObjectDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('DefenseSecurityEducationObjectService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
