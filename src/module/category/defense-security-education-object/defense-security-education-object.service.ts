import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { DefenseSecurityEducationObject } from './entities/defense-security-education-object.entity';
import { CreateDefenseSecurityEducationObjectDto } from './dto/create-defense-security-education-object.dto';
import { QueryDefenseSecurityEducationObjectDto } from './dto/query-defense-security-education-object.dto';
import { UpdateDefenseSecurityEducationObjectDto } from './dto/update-defense-security-education-object.dto';


@Injectable()
export class DefenseSecurityEducationObjectService extends BaseCategoryService<DefenseSecurityEducationObject> {
  constructor(
    @InjectRepository(DefenseSecurityEducationObject)
    repository: Repository<DefenseSecurityEducationObject>,
  ) {
    super(repository);
  }

  async create(
    createDefenseSecurityEducationObjectDto: CreateDefenseSecurityEducationObjectDto,
  ): Promise<ResponseCreated<DefenseSecurityEducationObject>> {
    await this.checkExist(['name', 'code'], createDefenseSecurityEducationObjectDto);

    const defenseSecurityEducationObject: DefenseSecurityEducationObject =
      await this.repository.save(createDefenseSecurityEducationObjectDto);

    return new ResponseCreated(defenseSecurityEducationObject);
  }

  async findAll(
    query: QueryDefenseSecurityEducationObjectDto,
  ): Promise<ResponseFindAll<DefenseSecurityEducationObject>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<DefenseSecurityEducationObject>> {
    const result = await this.checkNotExist(id, DefenseSecurityEducationObject.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateDefenseSecurityEducationObjectDto: UpdateDefenseSecurityEducationObjectDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, DefenseSecurityEducationObject.name);
    await this.checkExist(
      ['name', 'code'],
      updateDefenseSecurityEducationObjectDto,
      id,
    );
    const result = await this.repository.save({
      id,
      ...updateDefenseSecurityEducationObjectDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<DefenseSecurityEducationObject>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<DefenseSecurityEducationObject>(results, ids);
  }
}
