import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DefenseSecurityEducationObjectController } from './defense-security-education-object.controller';
import { DefenseSecurityEducationObjectService } from './defense-security-education-object.service';
import { DefenseSecurityEducationObject } from './entities/defense-security-education-object.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DefenseSecurityEducationObject])],
  controllers: [DefenseSecurityEducationObjectController],
  providers: [DefenseSecurityEducationObjectService],
  exports: [DefenseSecurityEducationObjectService],
})
export class DefenseSecurityEducationObjectModule {
  static readonly route: RouteTree = {
    path: 'defense-security-education-object',
    module: DefenseSecurityEducationObjectModule,
  };
}
