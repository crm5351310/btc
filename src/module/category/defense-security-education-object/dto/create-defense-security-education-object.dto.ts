import { OmitType } from '@nestjs/swagger';
import { DefenseSecurityEducationObject } from '../entities/defense-security-education-object.entity';

export class CreateDefenseSecurityEducationObjectDto extends OmitType(
  DefenseSecurityEducationObject,
  ['id'] as const,
) {}
