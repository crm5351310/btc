import { PartialType } from '@nestjs/swagger';
import { CreateDefenseSecurityEducationObjectDto } from './create-defense-security-education-object.dto';

export class UpdateDefenseSecurityEducationObjectDto extends PartialType(
  CreateDefenseSecurityEducationObjectDto,
) {}
