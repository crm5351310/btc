import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { DefenseSecurityEducationObjectService } from './defense-security-education-object.service';
import { CreateDefenseSecurityEducationObjectDto } from './dto/create-defense-security-education-object.dto';
import { QueryDefenseSecurityEducationObjectDto } from './dto/query-defense-security-education-object.dto';
import { UpdateDefenseSecurityEducationObjectDto } from './dto/update-defense-security-education-object.dto';

@Controller()
@ApiTags(TagEnum.DEFENSE_SECURITY_EDUCATION_OBJECT)
export class DefenseSecurityEducationObjectController {
  constructor(
    private readonly defenseSecurityEducationObjectService: DefenseSecurityEducationObjectService,
  ) {}

  @Post()
  create(
    @Body()
    createDefenseSecurityEducationObjectDto: CreateDefenseSecurityEducationObjectDto,
  ) {
    return this.defenseSecurityEducationObjectService.create(
      createDefenseSecurityEducationObjectDto,
    );
  }

  @Get()
  findAll(@Query() query: QueryDefenseSecurityEducationObjectDto) {
    return this.defenseSecurityEducationObjectService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.defenseSecurityEducationObjectService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body()
    updateDefenseSecurityEducationObjectDto: UpdateDefenseSecurityEducationObjectDto,
  ) {
    return this.defenseSecurityEducationObjectService.update(
      +id,
      updateDefenseSecurityEducationObjectDto,
    );
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.defenseSecurityEducationObjectService.remove(ids);
  }
}
