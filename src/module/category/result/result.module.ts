import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Result } from './entities';
import { ResultController } from './result.controller';
import { ResultService } from './result.service';

@Module({
  imports: [TypeOrmModule.forFeature([Result])],
  controllers: [ResultController],
  providers: [ResultService],
})
export class ResultModule {
  static readonly route: RouteTree = {
    path: 'result',
    module: ResultModule,
  };
}
