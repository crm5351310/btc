import { BaseQueryDto } from '../../../../common/base/dto/base-query.dto';

export class QueryResultDto extends BaseQueryDto {}
