import { OmitType } from '@nestjs/swagger';
import { Result } from '../entities/result.entity';

export class CreateResultDto extends OmitType(Result, ['id'] as const) {}
