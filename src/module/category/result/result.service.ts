import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { CreateResultDto, QueryResultDto, UpdateResultDto } from './dto';
import { Result } from './entities';

@Injectable()
export class ResultService extends BaseCategoryService<Result> {
  constructor(
    @InjectRepository(Result)
    repository: Repository<Result>,
  ) {
    super(repository);
  }

  async create(createResultDto: CreateResultDto) {
    await this.checkExist(['name', 'code'], createResultDto);

    const result: Result = await this.repository.save(createResultDto);

    return new ResponseCreated(result);
  }

  async findAll(query: QueryResultDto): Promise<ResponseFindAll<Result>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Result>> {
    const result = await this.checkNotExist(id, Result.name);

    return new ResponseFindOne(result);
  }

  async update(id: number, updateResultDto: UpdateResultDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Result.name);
    await this.checkExist(['name', 'code'], updateResultDto, id);

    const result = await this.repository.save({
      id,
      ...updateResultDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Result>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<Result>(results, ids);
  }
}
