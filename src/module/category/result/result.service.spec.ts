import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { ResultService } from './result.service';
import { Result } from './entities/result.entity';
import { CreateResultDto } from './dto/create-result.dto';
import { UpdateResultDto } from './dto/update-result.dto';

describe('ResultService', () => {
  let service: ResultService;
  let repository: Repository<Result>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ResultService,
        {
          provide: getRepositoryToken(Result),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<ResultService>(ResultService);
    repository = module.get(getRepositoryToken(Result));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        results.find((value) => value.id === (options.where as FindOptionsWhere<Result>).id) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return results;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [results, results.length];
    });

    jest.spyOn(repository, 'exist').mockImplementation(async (options: FindManyOptions<Result>) => {
      return results.some(
        (e) =>
          e.code == (options.where as FindOptionsWhere<Result>).code ||
          e.name == (options.where as FindOptionsWhere<Result>).name,
      );
    });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: Result) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: results.length,
        };
      }
    });
  });

  const results = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as Result[];

  describe('ResultService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateResultDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('ResultService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(results.length);
    });
  });

  describe('ResultService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('ResultService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateResultDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateResultDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('ResultService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
