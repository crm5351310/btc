import { Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { ParamJobDto } from './dto/param-job.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Job } from './job.entity';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { UpdateAcademicDegreeDto } from '../academic-degree/dto';

@Injectable()
export class JobService extends BaseCategoryService<Job> {
  constructor(
    @InjectRepository(Job)
    repository: Repository<Job>,
  ) {
    super(repository);
  }
  async create(createJobDto: CreateJobDto) {
    await this.checkExist(['name', 'code'], createJobDto);

    const academicDegree: Job = await this.repository.save(createJobDto);

    return new ResponseCreated(academicDegree);
  }

  async findAll(param: ParamJobDto): Promise<ResponseFindAll<Job>> {
    const [results, total] = await this.repository.findAndCount({
      skip: param.limit && param.offset,
      take: param.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Job>> {
    const result = await this.checkNotExist(id, Job.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateAcademicDegreeDto: UpdateAcademicDegreeDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Job.name);
    await this.checkExist(['name', 'code'], updateAcademicDegreeDto, id);
    const result = await this.repository.save({
      id,
      ...updateAcademicDegreeDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Job>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<Job>(results, ids);
  }
}
