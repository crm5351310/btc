// // npm run test:run src/module/category/job
// import { CacheModule } from '@nestjs/cache-manager';
// import { Test, TestingModule } from '@nestjs/testing';
// import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
// import * as redisStore from 'cache-manager-redis-store';
// import { Repository } from 'typeorm';
// import { dataSourceOptions } from '../../../../config/data-source.config';
// import {
//   PathArrayDto,
//   PathDto,
// } from '../../../../common/base/class/base.class';
// import { JobService } from '../job.service';
// import { Job } from '../job.entity';
// import { JobModule } from '../job.module';
//
// describe('JobTitleService', () => {
//   let module: TestingModule;
//   let jobService: JobService;
//   let jobRepository: Repository<Job>;
//
//   const JOB_REPOSITORY_TOKEN = getRepositoryToken(Job);
//
//   beforeAll(async () => {
//     module = await Test.createTestingModule({
//       imports: [
//         JobModule,
//         TypeOrmModule.forRootAsync({
//           useFactory: () => dataSourceOptions,
//         }),
//         CacheModule.register({
//           isGlobal: true,
//           host: process.env.REDIS_HOST,
//           port: process.env.REDIS_PORT,
//           store: redisStore,
//         }),
//       ],
//     }).compile();
//
//     jobRepository = module.get(JOB_REPOSITORY_TOKEN);
//     jobService = module.get<JobService>(JobService);
//   });
//
//   afterEach(() => {
//     jest.resetAllMocks();
//   });
//
//   describe('JobService => create', () => {
//     const payload = {
//       name: 'tenten',
//       code: 'te2',
//     } as Job;
//
//     it('Tạo mới nghề nghiệp thành công', async () => {
//       jest.spyOn(jobRepository, 'findOne').mockResolvedValue(null);
//       jest
//         .spyOn(jobRepository, 'save')
//         .mockResolvedValue({ ...payload } as Job);
//
//       const result = await jobService.create(payload);
//
//       expect(jobRepository.save).toHaveBeenCalled();
//
//       expect(result.statusCode).toBe(201);
//       expect(result.data).not.toBeNull();
//       expect(result.data).toMatchObject(payload);
//     });
//   });
//
//   describe('JobService => findOne', () => {
//     it('Tìm một nghề nghiệp thành công', async () => {
//       const data = new Job();
//       data.id = 1;
//       data.name = 'h';
//       data.code = '3';
//       jest.spyOn(jobRepository, 'findOne').mockResolvedValue(data);
//
//       const result = await jobService.findOne({ id: 1 } as PathDto);
//
//       expect(jobRepository.findOne).toHaveBeenCalled();
//       expect(result.statusCode).toBe(200);
//       expect(result.data).not.toBeNull();
//       expect(result.data).toMatchObject(data);
//     });
//
//     it('Không thể tìm một nghề nghiệp nếu ID không tồn tại', async () => {
//       jest.spyOn(jobRepository, 'findOne').mockResolvedValue(null);
//       await expect(jobService.findOne({ id: 1 })).rejects.toThrowError();
//     });
//   });
//
//   describe('JobService => findAll', () => {
//     it('Tìm danh sách nghề nghiệp thành công', async () => {
//       const mockData = [
//         { id: 1, name: 'user1' },
//         { id: 2, name: 'user2' },
//         { id: 3, name: 'user2' },
//       ] as Job[];
//       jest
//         .spyOn(jobRepository, 'findAndCount')
//         .mockResolvedValue([mockData, mockData.length]);
//
//       const result = await jobService.findAll({} as any);
//
//       expect(jobRepository.findAndCount).toHaveBeenCalled();
//       expect(result.statusCode).toBe(200);
//       expect(result.data.result).toMatchObject(mockData);
//       expect(result.data.result.length).toBe(mockData.length);
//     });
//   });
//   //
//   describe('JobService => remove', () => {
//     it('Xoá một nghề nghiệp thành công', async () => {
//       const mockData = { id: 1 } as Job;
//       jest.spyOn(jobRepository, 'find').mockResolvedValue([mockData]);
//       jest.spyOn(jobRepository, 'softRemove').mockResolvedValue(mockData);
//       const result = await jobService.remove({ id: '1' } as PathArrayDto);
//
//       expect(jobRepository.find).toHaveBeenCalled();
//       expect(result.statusCode).toBe(200);
//       expect(result.data).not.toBeNull();
//     });
//
//     it('Không thể xoá một nghề nghiệp nếu ID không tồn tại', async () => {
//       jest.spyOn(jobRepository, 'find').mockResolvedValue([]);
//       await expect(jobService.remove({ id: '1' })).rejects.toThrowError();
//     });
//   });
// });
