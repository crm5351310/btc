import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { JobService } from './job.service';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { PathArrayDto, PathDto } from '../../../common/base/class/base.class';
import { ParamJobDto } from './dto/param-job.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.JOB)
export class JobController {
  constructor(private readonly jobService: JobService) {}

  @Post()
  create(@Body() createJobDto: CreateJobDto) {
    return this.jobService.create(createJobDto);
  }

  @Get()
  findAll(@Query() param: ParamJobDto) {
    return this.jobService.findAll(param);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.jobService.findOne(id);
  }

  @Patch(':id')
  update(@Param() id: number, @Body() updateJobDto: UpdateJobDto) {
    return this.jobService.update(id, updateJobDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.jobService.remove(ids);
  }
}
