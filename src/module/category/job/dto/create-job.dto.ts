import { OmitType } from '@nestjs/swagger';
import { Job } from '../job.entity';

export class CreateJobDto extends OmitType(Job, ['id'] as const) {}
