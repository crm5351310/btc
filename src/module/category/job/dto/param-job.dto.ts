import { IntersectionType } from '@nestjs/swagger';
import { PaginationDto } from '../../../../common/base/class/base.class';

export class ParamJobDto extends IntersectionType(PaginationDto) {}
