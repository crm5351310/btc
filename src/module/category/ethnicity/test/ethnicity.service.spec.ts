import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { EthnicityService } from '../ethnicity.service';
import { Ethnicity } from '../ethnicity.entity';
import { EthnicityModule } from '../ethnicity.module';
import {
  CreateEthnicityDto,
  FindManyEthnicityParamDto,
  UpdateEthnicityDto,
} from '../ethnicity.dto';

// npm run test:run src/module/category/ethnicity
describe('EthnicityService', () => {
  let ethnicityService: EthnicityService;
  let ethnicityRepository: Repository<Ethnicity>;
  let module: TestingModule;

  const ETHNICITY_REPOSITORY_TOKEN = getRepositoryToken(Ethnicity);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        EthnicityModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    ethnicityService = module.get<EthnicityService>(EthnicityService);
    ethnicityRepository = module.get(ETHNICITY_REPOSITORY_TOKEN);
  });

  describe('EthnicityService => create', () => {
    const payload = {
      code: 'code',
      name: 'name',
      description: 'Mô tả',
    } as CreateEthnicityDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Ethnicity>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Ethnicity>;

    it('Tạo mới dân tộc thành công', async () => {
      jest.spyOn(ethnicityRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(ethnicityRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Ethnicity);

      const result = await ethnicityService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới dân tộc không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(ethnicityRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as Ethnicity);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as Ethnicity);
        return Promise.resolve(null);
      });

      await expect(ethnicityService.create(payload)).rejects.toThrowError();
    });
  });

  describe('EthnicityService => findOne', () => {
    it('Tìm một dân tộc thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'dân tộc số 1',
        description: 'Mô tả',
      } as Ethnicity;
      jest.spyOn(ethnicityRepository, 'findOne').mockResolvedValue(mockData);

      const result = await ethnicityService.findOne({ id: 1 });

      expect(ethnicityRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });
    it('Không thể tìm một đơn vị quản lý nếu ID không tồn tại', async () => {
      jest.spyOn(ethnicityRepository, 'findOne').mockResolvedValue(null);

      await expect(ethnicityService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('EthnicityService => findAll', () => {
    it('Tìm danh sách dân tộc thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'dân tộc số 1',
        description: 'Mô tả',
      } as Ethnicity;
      jest.spyOn(ethnicityRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await ethnicityService.findAll({} as FindManyEthnicityParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('EthnicityService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'dân tộc số 1',
      description: 'Mô tả',
    } as UpdateEthnicityDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Ethnicity>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Ethnicity>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<Ethnicity>;

    it('Cập nhật dân tộc thành công', async () => {
      jest.spyOn(ethnicityRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as Ethnicity);
        return Promise.resolve(null);
      });
      jest.spyOn(ethnicityRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Ethnicity);

      const result = await ethnicityService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật dân tộc không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(ethnicityRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as Ethnicity);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as Ethnicity);
        return Promise.resolve(null);
      });
      jest.spyOn(ethnicityRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Ethnicity);

      await expect(ethnicityService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật dân tộc không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(ethnicityRepository, 'findOne').mockResolvedValue({ id: 1 } as Ethnicity);
      jest.spyOn(ethnicityRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Ethnicity);

      await expect(ethnicityService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('EthnicityService => remove', () => {
    it('Xoá dân tộc thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'dân tộc số 1',
        description: 'Mô tả',
      } as Ethnicity;
      jest.spyOn(ethnicityRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(ethnicityRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await ethnicityService.remove({ id: '1' });

      expect(ethnicityRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá dân tộc không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(ethnicityRepository, 'find').mockResolvedValue([]);
      await expect(ethnicityService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
