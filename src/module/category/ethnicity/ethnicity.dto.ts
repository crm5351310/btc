import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { Ethnicity } from './ethnicity.entity';

export class CreateEthnicityDto extends OmitType(Ethnicity, [
  'id',
  'createdAt',
  'updatedAt',
  'createdBy',
  'updatedBy',
] as const) {}

export class UpdateEthnicityDto extends PartialType(CreateEthnicityDto) {}

export class FindManyEthnicityParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
