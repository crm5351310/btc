import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateEthnicityDto, FindManyEthnicityParamDto, UpdateEthnicityDto } from './ethnicity.dto';
import { EthnicityService } from './ethnicity.service';

@Controller()
@ApiTags(TagEnum.ETHNICITY)
export class EthnicityController {
  constructor(private readonly ethnicityService: EthnicityService) {}

  @Post()
  create(@Body() createEthnicityDto: CreateEthnicityDto) {
    return this.ethnicityService.create(createEthnicityDto);
  }

  @Get()
  findAll(@Query() param: FindManyEthnicityParamDto) {
    return this.ethnicityService.findAll(param);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.ethnicityService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateEthnicityDto: UpdateEthnicityDto) {
    return this.ethnicityService.update(id, updateEthnicityDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.ethnicityService.remove(ids);
  }
}
