import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { CreateEthnicityDto, FindManyEthnicityParamDto, UpdateEthnicityDto } from './ethnicity.dto';
import { Ethnicity } from './ethnicity.entity';
import { BaseCategoryService } from "../../../common/base/service/base-category.service";

@Injectable()
export class EthnicityService extends BaseCategoryService<Ethnicity> {
  constructor(
    @InjectRepository(Ethnicity)
    repository: Repository<Ethnicity>,
  ) {
    super(repository);
  }
  async create(createEthnicityDto: CreateEthnicityDto) {
    await this.checkExist(['code', 'name'], createEthnicityDto);
    const Ethnicity: Ethnicity = await this.repository.save(createEthnicityDto);

    return new ResponseCreated(Ethnicity);
  }

  async findAll(param: FindManyEthnicityParamDto) {
    const [results, total] = await this.repository.findAndCount({
      skip: param.limit && param.offset,
      take: param.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Ethnicity>> {
    const result = await this.checkNotExist(id, Ethnicity.name);

    return new ResponseFindOne(result);
  }

  async update(id: number, updateEthnicityDto: UpdateEthnicityDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Ethnicity.name);
    await this.checkExist(['name', 'code'], updateEthnicityDto, id);
    const result = await this.repository.save({
      id,
      ...updateEthnicityDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Ethnicity>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
