import { Module } from '@nestjs/common';
import { EthnicityService } from './ethnicity.service';
import { EthnicityController } from './ethnicity.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ethnicity } from './ethnicity.entity';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([Ethnicity])],
  controllers: [EthnicityController],
  providers: [EthnicityService],
})
export class EthnicityModule {}
export const ethnicityRouter: RouteTree = {
  path: 'ethnicity',
  module: EthnicityModule,
  children: [],
};
