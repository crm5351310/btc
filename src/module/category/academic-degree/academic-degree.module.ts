import { Module } from '@nestjs/common';
import { AcademicDegreeService } from './academic-degree.service';
import { AcademicDegreeController } from './academic-degree.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AcademicDegree } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([AcademicDegree])],
  controllers: [AcademicDegreeController],
  providers: [AcademicDegreeService],
  exports: [AcademicDegreeService],
})
export class AcademicDegreeModule {
  static readonly route: RouteTree = {
    path: 'academic-degree',
    module: AcademicDegreeModule,
  };
}
