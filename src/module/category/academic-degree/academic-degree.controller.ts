import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseArrayPipe,
  Query,
} from '@nestjs/common';
import { AcademicDegreeService } from './academic-degree.service';

import { CreateAcademicDegreeDto, UpdateAcademicDegreeDto, QueryAcademicDegreeDto } from './dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.ACADEMIC_DEGREE)
export class AcademicDegreeController {
  constructor(private readonly academicDegreeService: AcademicDegreeService) {}

  @Post()
  create(@Body() createAcademicDegreeDto: CreateAcademicDegreeDto) {
    return this.academicDegreeService.create(createAcademicDegreeDto);
  }

  @Get()
  findAll(@Query() query: QueryAcademicDegreeDto) {
    return this.academicDegreeService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.academicDegreeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAcademicDegreeDto: UpdateAcademicDegreeDto) {
    return this.academicDegreeService.update(+id, updateAcademicDegreeDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.academicDegreeService.remove(ids);
  }
}
