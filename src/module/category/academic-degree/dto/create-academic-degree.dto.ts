import { OmitType } from '@nestjs/swagger';
import { AcademicDegree } from '../entities/academic-degree.entity';

export class CreateAcademicDegreeDto extends OmitType(AcademicDegree, ['id'] as const) {}
