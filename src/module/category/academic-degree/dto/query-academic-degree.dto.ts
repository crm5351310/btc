import { BaseQueryDto } from 'src/common/base/dto/base-query.dto';

export class QueryAcademicDegreeDto extends BaseQueryDto {}
