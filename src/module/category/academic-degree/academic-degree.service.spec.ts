import { Test, TestingModule } from '@nestjs/testing';
import { AcademicDegreeService } from './academic-degree.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AcademicDegree } from './entities';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { CreateAcademicDegreeDto, UpdateAcademicDegreeDto } from './dto';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';

describe('AcademicDegreeService', () => {
  let service: AcademicDegreeService;
  let repository: Repository<AcademicDegree>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AcademicDegreeService,
        {
          provide: getRepositoryToken(AcademicDegree),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<AcademicDegreeService>(AcademicDegreeService);
    repository = module.get(getRepositoryToken(AcademicDegree));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        academicDegrees.find(
          (value) => value.id === (options.where as FindOptionsWhere<AcademicDegree>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return academicDegrees;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [academicDegrees, academicDegrees.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<AcademicDegree>) => {
        return academicDegrees.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<AcademicDegree>).code ||
            e.name == (options.where as FindOptionsWhere<AcademicDegree>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: AcademicDegree) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: academicDegrees.length,
        };
      }
    });
  });

  const academicDegrees = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as AcademicDegree[];

  describe('AcademicDegreeService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateAcademicDegreeDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateAcademicDegreeDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('AcademicDegreeService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(academicDegrees.length);
    });
  });

  describe('AcademicDegreeService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('AcademicDegreeService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateAcademicDegreeDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateAcademicDegreeDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('AcademicDegreeService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
