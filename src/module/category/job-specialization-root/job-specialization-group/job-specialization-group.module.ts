import { Module } from '@nestjs/common';
import { JobSpecializationGroupService } from './job-specialization-group.service';
import { JobSpecializationGroupController } from './job-specialization-group.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobSpecializationGroup } from './entities/job-specialization-group.entity';

@Module({
  imports: [TypeOrmModule.forFeature([JobSpecializationGroup])],
  controllers: [JobSpecializationGroupController],
  providers: [JobSpecializationGroupService],
  exports: [JobSpecializationGroupService],
})
export class JobSpecializationGroupModule {
  static readonly route: RouteTree = {
    path: 'job-specialization-group',
    module: JobSpecializationGroupModule,
    children: [],
  };
}
