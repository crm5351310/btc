import { OmitType } from '@nestjs/swagger';
import { JobSpecializationGroup } from '../entities/job-specialization-group.entity';

export class CreateJobSpecializationGroupDto extends OmitType(JobSpecializationGroup, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
  'deletedAt',
] as const) {}
