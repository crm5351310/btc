import { PartialType } from '@nestjs/swagger';
import { CreateJobSpecializationGroupDto } from './create-job-specialization-group.dto';

export class UpdateJobSpecializationGroupDto extends PartialType(CreateJobSpecializationGroupDto) {}
