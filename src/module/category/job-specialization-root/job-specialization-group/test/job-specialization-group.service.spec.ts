import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../../config/data-source.config';
import { JobSpecializationGroupService } from '../job-specialization-group.service';
import { JobSpecializationGroup } from '../entities/job-specialization-group.entity';
import { JobSpecializationGroupModule } from '../job-specialization-group.module';
import { CreateJobSpecializationGroupDto } from '../dto/create-job-specialization-group.dto';
import { FindManyJobSpecializationGroupParamDto } from '../dto/find-many-job-specialization-group-param.dto';
import { UpdateJobSpecializationGroupDto } from '../dto/update-job-specialization-group.dto';

// npm run test:run src/module/category/job-specialization-root/job-specialization-group
describe('JobSpecializationService', () => {
  let jobSpecializationService: JobSpecializationGroupService;
  let JobSpecializationRepository: Repository<JobSpecializationGroup>;

  const JOB_SPECIALIZATION_REPOSITORY_TOKEN = getRepositoryToken(JobSpecializationGroup);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JobSpecializationGroupModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    jobSpecializationService = module.get<JobSpecializationGroupService>(
      JobSpecializationGroupService,
    );
    JobSpecializationRepository = module.get(JOB_SPECIALIZATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(jobSpecializationService).toBeDefined();
    expect(JobSpecializationRepository).toBeDefined();
  });

  describe('JobSpecializationService => create', () => {
    const payload = {
      code: 'code',
      name: 'name',
      description: 'Mô tả',
    } as CreateJobSpecializationGroupDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<JobSpecializationGroup>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<JobSpecializationGroup>;

    it('Tạo mới nhóm ngành CMKT thành công', async () => {
      jest.spyOn(JobSpecializationRepository, 'findOne').mockResolvedValue(null);
      jest
        .spyOn(JobSpecializationRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as JobSpecializationGroup);

      const result = await jobSpecializationService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới nhóm ngành CMKT không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(JobSpecializationRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery))
          return Promise.resolve({ id: 1 } as JobSpecializationGroup);
        if (_.isEqual(query, nameQuery))
          return Promise.resolve({ id: 1 } as JobSpecializationGroup);
        return Promise.resolve(null);
      });

      await expect(jobSpecializationService.create(payload)).rejects.toThrowError();
    });
  });

  describe('JobSpecializationService => findOne', () => {
    it('Tìm một nhóm ngành CMKT thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'nhóm ngành CMKT số 1',
        description: 'Mô tả',
      } as JobSpecializationGroup;
      jest.spyOn(JobSpecializationRepository, 'findOne').mockResolvedValue(mockData);

      const result = await jobSpecializationService.findOne({ id: 1 });

      expect(JobSpecializationRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một nhóm ngành CMKT nếu ID không tồn tại', async () => {
      jest.spyOn(JobSpecializationRepository, 'findOne').mockResolvedValue(null);
      await expect(jobSpecializationService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('JobSpecializationService => findAll', () => {
    it('Tìm danh sách nhóm ngành CMKT thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'nhóm ngành CMKT số 1',
        description: 'Mô tả',
      } as JobSpecializationGroup;
      jest.spyOn(JobSpecializationRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await jobSpecializationService.findAll(
        {} as FindManyJobSpecializationGroupParamDto,
      );

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('JobSpecializationService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'nhóm ngành CMKT số 1',
      description: 'Mô tả',
    } as UpdateJobSpecializationGroupDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<JobSpecializationGroup>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<JobSpecializationGroup>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<JobSpecializationGroup>;

    it('Cập nhật nhóm ngành CMKT thành công', async () => {
      jest.spyOn(JobSpecializationRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as JobSpecializationGroup);
        return Promise.resolve(null);
      });
      jest.spyOn(JobSpecializationRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as JobSpecializationGroup);

      const result = await jobSpecializationService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật nhóm ngành CMKT không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(JobSpecializationRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery))
          return Promise.resolve({ id: 2 } as JobSpecializationGroup);
        if (_.isEqual(query, nameQuery))
          return Promise.resolve({ id: 2 } as JobSpecializationGroup);
        return Promise.resolve(null);
      });
      jest.spyOn(JobSpecializationRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as JobSpecializationGroup);

      await expect(jobSpecializationService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật nhóm ngành CMKT không thành công nếu name hoặc code đã tồn tại', async () => {
      jest
        .spyOn(JobSpecializationRepository, 'findOne')
        .mockResolvedValue({ id: 1 } as JobSpecializationGroup);
      jest.spyOn(JobSpecializationRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as JobSpecializationGroup);

      await expect(jobSpecializationService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('JobSpecializationService => remove', () => {
    it('Xoá nhóm ngành CMKT thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'nhóm ngành CMKT số 1',
        description: 'Mô tả',
      } as JobSpecializationGroup;
      jest.spyOn(JobSpecializationRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(JobSpecializationRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await jobSpecializationService.remove({ id: '1' });

      expect(JobSpecializationRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá nhóm ngành CMKT không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(JobSpecializationRepository, 'find').mockResolvedValue([]);
      await expect(jobSpecializationService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
