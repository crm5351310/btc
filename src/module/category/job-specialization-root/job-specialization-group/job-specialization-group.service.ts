import { Injectable } from '@nestjs/common';
import { CreateJobSpecializationGroupDto } from './dto/create-job-specialization-group.dto';
import { UpdateJobSpecializationGroupDto } from './dto/update-job-specialization-group.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { JobSpecializationGroup } from './entities/job-specialization-group.entity';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { In, Not, Repository } from 'typeorm';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { Pagination } from '../../../../common/utils/pagination.util';
import { FindManyJobSpecializationGroupParamDto } from './dto/find-many-job-specialization-group-param.dto';

@Injectable()
export class JobSpecializationGroupService extends BaseService {
  constructor(
    @InjectRepository(JobSpecializationGroup)
    private readonly jobSpecializationGroupRepository: Repository<JobSpecializationGroup>,
  ) {
    super();
    this.name = JobSpecializationGroupService.name;
    this.subject = SubjectMessage.JOB_SPECIALIZATION_GROUP;
  }
  async create(createJobSpecializationGroupDto: CreateJobSpecializationGroupDto) {
    this.action = BaseMessage.CREATE;
    const checkExistCodeEthnicity = await this.jobSpecializationGroupRepository.findOne({
      where: {
        code: createJobSpecializationGroupDto.code,
      },
    });
    if (checkExistCodeEthnicity)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    const checkExistNameEthnicity = await this.jobSpecializationGroupRepository.findOne({
      where: {
        name: createJobSpecializationGroupDto.name,
      },
    });
    if (checkExistNameEthnicity)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    const result = await this.jobSpecializationGroupRepository.save(
      createJobSpecializationGroupDto,
    );
    return this.response(result);
  }

  async findAll(param: FindManyJobSpecializationGroupParamDto) {
    this.action = BaseMessage.READ;

    const result = await this.jobSpecializationGroupRepository.findAndCount();
    return this.response(new Pagination<JobSpecializationGroup>(result[0], result[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.jobSpecializationGroupRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION_GROUP,
        true,
      );
    return this.response(result);
  }

  async update(path: PathDto, updateJobSpecializationGroupDto: UpdateJobSpecializationGroupDto) {
    this.action = BaseMessage.UPDATE;
    const checkExistJobSpecializationGroup = await this.jobSpecializationGroupRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistJobSpecializationGroup)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION_GROUP,
        true,
      );
    if (updateJobSpecializationGroupDto.code) {
      const checkExistCode = await this.jobSpecializationGroupRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateJobSpecializationGroupDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateJobSpecializationGroupDto.name) {
      const checkExistName = await this.jobSpecializationGroupRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateJobSpecializationGroupDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const data = await this.jobSpecializationGroupRepository.save({
      id: path.id,
      ...updateJobSpecializationGroupDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistJobSpecializationGroup = await this.jobSpecializationGroupRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistJobSpecializationGroup.length !== pathArray.length)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION_GROUP,
        true,
      );
    await this.jobSpecializationGroupRepository.softRemove(checkExistJobSpecializationGroup);
    return this.response(checkExistJobSpecializationGroup);
  }
}
