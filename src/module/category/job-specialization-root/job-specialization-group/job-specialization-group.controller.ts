import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { JobSpecializationGroupService } from './job-specialization-group.service';
import { CreateJobSpecializationGroupDto } from './dto/create-job-specialization-group.dto';
import { UpdateJobSpecializationGroupDto } from './dto/update-job-specialization-group.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { FindManyJobSpecializationGroupParamDto } from './dto/find-many-job-specialization-group-param.dto';

@Controller()
@ApiTags(TagEnum.JOB_SPECIALIZATION_GROUP)
export class JobSpecializationGroupController {
  constructor(private readonly jobSpecializationGroupService: JobSpecializationGroupService) {}

  @Post()
  create(@Body() createJobSpecializationGroupDto: CreateJobSpecializationGroupDto) {
    return this.jobSpecializationGroupService.create(createJobSpecializationGroupDto);
  }

  @Get()
  findAll(@Query() param: FindManyJobSpecializationGroupParamDto) {
    return this.jobSpecializationGroupService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.jobSpecializationGroupService.findOne(path);
  }

  @Patch(':id')
  update(
    @Param() path: PathDto,
    @Body() updateJobSpecializationGroupDto: UpdateJobSpecializationGroupDto,
  ) {
    return this.jobSpecializationGroupService.update(path, updateJobSpecializationGroupDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.jobSpecializationGroupService.remove(path);
  }
}
