import { Module } from '@nestjs/common';
import { JobSpecializationService } from './job-specialization.service';
import { JobSpecializationController } from './job-specialization.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobSpecialization } from './entities/job-specialization.entity';
import { JobSpecializationGroup } from '../job-specialization-group/entities/job-specialization-group.entity';
import { JobSpecializationGroupModule } from '../job-specialization-group/job-specialization-group.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([JobSpecialization, JobSpecializationGroup]),
    JobSpecializationGroupModule,
  ],
  controllers: [JobSpecializationController],
  providers: [JobSpecializationService],
  exports: [JobSpecializationService],
})
export class JobSpecializationModule {
  static readonly route: RouteTree = {
    path: 'job-specialization',
    module: JobSpecializationModule,
    children: [],
  };
}
