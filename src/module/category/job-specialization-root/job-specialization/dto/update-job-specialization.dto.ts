import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateJobSpecializationDto } from './create-job-specialization.dto';

export class UpdateJobSpecializationDto extends OmitType(PartialType(CreateJobSpecializationDto), [
  'jobSpecializationGroup',
]) {}
