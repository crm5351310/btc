import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class FindManyJobSpecializationParamDto {
  @ApiProperty({
    name: 'specializationGroupId',
    required: false,
    description: 'ID của nhóm ngành nghề',
  })
  @IsOptional()
  @IsNumber()
  specializationGroupId: number;
}
