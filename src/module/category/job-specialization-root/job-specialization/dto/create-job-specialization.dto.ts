import { OmitType } from '@nestjs/swagger';
import { JobSpecialization } from '../entities/job-specialization.entity';

export class CreateJobSpecializationDto extends OmitType(JobSpecialization, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
  'deletedAt',
] as const) {}
