import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { JobSpecializationGroup } from '../../job-specialization-group/entities/job-specialization-group.entity';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';

@Entity()
export class JobSpecialization extends BaseEntity {
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  description: string;

  @ApiProperty({
    description: '123',
    type: RelationTypeBase,
  })
  @ValidateNested()
  @IsNotEmpty()
  @Type(() => PickType(JobSpecializationGroup, ['id']))
  @ManyToOne(() => JobSpecializationGroup, (item) => item.jobSpecializations)
  @JoinColumn()
  jobSpecializationGroup: JobSpecializationGroup;
}
