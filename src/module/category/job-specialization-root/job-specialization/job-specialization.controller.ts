import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { CreateJobSpecializationDto } from './dto/create-job-specialization.dto';
import { FindManyJobSpecializationParamDto } from './dto/find-many-job-specialization.dto';
import { UpdateJobSpecializationDto } from './dto/update-job-specialization.dto';
import { JobSpecializationService } from './job-specialization.service';

@Controller()
@ApiTags(TagEnum.JOB_SPECIALIZATION)
export class JobSpecializationController {
  constructor(private readonly jobSpecializationService: JobSpecializationService) {}

  @Post()
  create(@Body() createJobSpecializationDto: CreateJobSpecializationDto) {
    return this.jobSpecializationService.create(createJobSpecializationDto);
  }

  @Get()
  findAll(@Query() param: FindManyJobSpecializationParamDto) {
    return this.jobSpecializationService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.jobSpecializationService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateJobSpecializationDto: UpdateJobSpecializationDto) {
    return this.jobSpecializationService.update(path, updateJobSpecializationDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.jobSpecializationService.remove(path);
  }
}
