import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pagination } from 'src/common/utils/pagination.util';
import { In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { CreateJobSpecializationDto } from './dto/create-job-specialization.dto';
import { FindManyJobSpecializationParamDto } from './dto/find-many-job-specialization.dto';
import { UpdateJobSpecializationDto } from './dto/update-job-specialization.dto';
import { JobSpecialization } from './entities/job-specialization.entity';
import { JobSpecializationGroupService } from '../job-specialization-group/job-specialization-group.service';

@Injectable()
export class JobSpecializationService extends BaseService {
  constructor(
    @InjectRepository(JobSpecialization)
    private readonly jobSpecializationRepository: Repository<JobSpecialization>,
    private readonly jobSpecializationGroupService: JobSpecializationGroupService,
  ) {
    super();
    this.name = JobSpecializationService.name;
    this.subject = SubjectMessage.JOB_SPECIALIZATION;
  }

  async create(createJobSpecializationDto: CreateJobSpecializationDto) {
    this.action = BaseMessage.CREATE;
    const checkExistCode = await this.jobSpecializationRepository.findOne({
      where: {
        code: createJobSpecializationDto.code,
      },
    });
    if (checkExistCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    const checkExistName = await this.jobSpecializationRepository.findOne({
      where: {
        name: createJobSpecializationDto.name,
      },
    });
    if (checkExistName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    await this.jobSpecializationGroupService.findOne(
      createJobSpecializationDto.jobSpecializationGroup,
    );

    const result = await this.jobSpecializationRepository.save(createJobSpecializationDto);
    return this.response(result);
  }

  async findAll(param: FindManyJobSpecializationParamDto) {
    this.action = BaseMessage.READ;

    const result = await this.jobSpecializationRepository.findAndCount({
      where: {
        jobSpecializationGroup: {
          id: param.specializationGroupId || undefined,
        },
      },
      relations: {
        jobSpecializationGroup: true,
      },
    });
    return this.response(new Pagination<JobSpecialization>(result[0], result[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.jobSpecializationRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION,
        true,
      );
    return this.response(result);
  }

  async update(path: PathDto, updateJobSpecializationDto: UpdateJobSpecializationDto) {
    this.action = BaseMessage.UPDATE;
    const checkExistJobSpecialization = await this.jobSpecializationRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistJobSpecialization)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION,
        true,
      );
    if (updateJobSpecializationDto.code) {
      const checkExistCode = await this.jobSpecializationRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateJobSpecializationDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateJobSpecializationDto.name) {
      const checkExistName = await this.jobSpecializationRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateJobSpecializationDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const data = await this.jobSpecializationRepository.save({
      id: path.id,
      ...updateJobSpecializationDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistJobSpecialization = await this.jobSpecializationRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistJobSpecialization.length !== pathArray.length)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION,
        true,
      );
    await this.jobSpecializationRepository.softRemove(checkExistJobSpecialization);
    return this.response(checkExistJobSpecialization);
  }
}
