import { Test, TestingModule } from '@nestjs/testing';
import { JobSpecializationController } from '../job-specialization.controller';
import { JobSpecializationService } from '../job-specialization.service';

describe('JobSpecializationController', () => {
  let controller: JobSpecializationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JobSpecializationController],
      providers: [JobSpecializationService],
    }).compile();

    controller = module.get<JobSpecializationController>(JobSpecializationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
