import { Test, TestingModule } from '@nestjs/testing';
import { JobSpecializationService } from '../job-specialization.service';

describe('JobSpecializationService', () => {
  let service: JobSpecializationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JobSpecializationService],
    }).compile();

    service = module.get<JobSpecializationService>(JobSpecializationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
