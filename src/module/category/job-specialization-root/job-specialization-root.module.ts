import { Module } from '@nestjs/common';
import { JobSpecializationGroupModule } from './job-specialization-group/job-specialization-group.module';
import { JobSpecializationModule } from './job-specialization/job-specialization.module';
import { RouteTree } from '@nestjs/core';

@Module({
  controllers: [],
  providers: [],
  imports: [JobSpecializationModule, JobSpecializationGroupModule],
})
export class JobSpecializationRootModule {
  static readonly route: RouteTree = {
    path: 'job-specialization-root',
    module: JobSpecializationRootModule,
    children: [JobSpecializationModule.route, JobSpecializationGroupModule.route],
  };
}
