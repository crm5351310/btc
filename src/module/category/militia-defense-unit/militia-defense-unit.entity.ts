import { ApiProperty, PickType } from '@nestjs/swagger';
import { BaseCategoryEntity } from '../../../common/base/entity/base-category.entity';
import { Column, Entity, ManyToOne, TableInheritance } from 'typeorm';
import { IsDefined, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { OrganizationScale } from '../organization-scale/entities';
import { Type } from 'class-transformer';
import { RelationTypeBase } from '../../../common/base/class/base.class';

@Entity()
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
export abstract class MilitiaDefenseUnit extends BaseCategoryEntity {
  @Column('varchar')
  type: string;

  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validator
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;

  // swagger
  @ApiProperty({
    description: 'Quy mô tổ chức',
    type: RelationTypeBase,
  })
  // validator
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(OrganizationScale, ['id']))
  // orm
  @ManyToOne(() => OrganizationScale, (organizationScale) => organizationScale.militiaDefenseUnits)
  organizationScale: OrganizationScale;
}
