import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { MilitiaUnitModule } from './militia-unit/militia-unit.module';
import { DefenseUnitModule } from './defense-unit/defense-unit.module';

@Module({
  imports: [MilitiaUnitModule, DefenseUnitModule],
})
export class MilitiaDefenseUnitModule {
  static readonly route: RouteTree = {
    path: 'militia-defense-unit',
    module: MilitiaDefenseUnitModule,
    children: [MilitiaUnitModule.route, DefenseUnitModule.route],
  };
}
