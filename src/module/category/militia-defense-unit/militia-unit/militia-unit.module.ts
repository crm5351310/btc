import { Module } from '@nestjs/common';
import { MilitiaUnitService } from './militia-unit.service';
import { MilitiaUnitController } from './militia-unit.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitiaUnit } from './entities';
import { OrganizationScaleModule } from '../../organization-scale/organization-scale.module';
import { AdministrativeUnitModule } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MilitiaUnit]),
    OrganizationScaleModule,
    AdministrativeUnitModule,
  ],
  controllers: [MilitiaUnitController],
  providers: [MilitiaUnitService],
  exports: [MilitiaUnitService],
})
export class MilitiaUnitModule {
  static readonly route: RouteTree = {
    path: 'militia-unit',
    module: MilitiaUnitModule,
  };
}
