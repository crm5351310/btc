import { Injectable } from '@nestjs/common';
import { CreateMilitiaUnitDto } from './dto/create-militia-unit.dto';
import { UpdateMilitiaUnitDto } from './dto/update-militia-unit.dto';
import { QueryMilitiaUnitDto } from './dto/query-militia-unit.dto';
import { BaseCategoryService } from '../../../../common/base/service/base-category.service';
import { MilitiaUnit } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../../common/response';
import { OrganizationScaleService } from '../../organization-scale/organization-scale.service';
import { AdministrativeUnitService } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.service';

@Injectable()
export class MilitiaUnitService extends BaseCategoryService<MilitiaUnit> {
  constructor(
    @InjectRepository(MilitiaUnit)
    repository: Repository<MilitiaUnit>,
    private organizationScaleService: OrganizationScaleService,
    private administrativeUnitService: AdministrativeUnitService,
  ) {
    super(repository);
  }
  async create(createMilitiaUnitDto: CreateMilitiaUnitDto): Promise<ResponseCreated<MilitiaUnit>> {
    await this.checkExist(['name', 'code'], createMilitiaUnitDto);

    // kiểm tra bản ghi quy mô tổ chức có tồn tại không
    await this.organizationScaleService.findOne(createMilitiaUnitDto.organizationScale.id);

    // kiểm tra bản ghi đơn vị hành chính có tồn tại không
    await this.administrativeUnitService.findOne(createMilitiaUnitDto.administrativeUnit);

    const militiaUnit: MilitiaUnit = await this.repository.save(createMilitiaUnitDto);

    return new ResponseCreated(militiaUnit);
  }

  async findAll(query: QueryMilitiaUnitDto): Promise<ResponseFindAll<MilitiaUnit>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
      relations: {
        administrativeUnit: {
          administrativeTitle: {
            administrativeLevel: true,
          }
        },
        organizationScale: true,
      },
    });

    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<MilitiaUnit>> {
    await this.checkNotExist(id, MilitiaUnit.name);

    const result = await this.repository.findOne({
      where: { id },
      relations: {
        administrativeUnit: {
          administrativeTitle: {
            administrativeLevel: true,
          },
          parent: true,
        },
        organizationScale: true,
      },
    });

    return new ResponseFindOne(result);
  }

  async update(id: number, updateMilitiaUnitDto: UpdateMilitiaUnitDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitiaUnit.name);
    await this.checkExist(['name', 'code'], updateMilitiaUnitDto, id);

    // kiểm tra bản ghi quy mô tổ chức có tồn tại không
    if (updateMilitiaUnitDto.organizationScale) {
      await this.organizationScaleService.findOne(updateMilitiaUnitDto.organizationScale.id);
    }

    // kiểm tra bản ghi đơn vị hành chính có tồn tại không
    if (updateMilitiaUnitDto.administrativeUnit) {
      await this.administrativeUnitService.findOne(updateMilitiaUnitDto.administrativeUnit);
    }

    const result = await this.repository.save({
      id,
      ...updateMilitiaUnitDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<MilitiaUnit>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<MilitiaUnit>(results, ids);
  }
}
