import { Test, TestingModule } from '@nestjs/testing';
import { MilitiaUnitService } from './militia-unit.service';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { MilitiaUnit } from './entities';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CreateMilitiaUnitDto } from './dto/create-militia-unit.dto';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../../common/response';
import { UpdateMilitiaUnitDto } from './dto/update-militia-unit.dto';
import { createStubInstance } from 'sinon';
import { OrganizationScaleService } from '../../organization-scale/organization-scale.service';
import { AdministrativeUnitService } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.service';

describe('MilitiaUnitService', () => {
  let service: MilitiaUnitService;
  let repository: Repository<MilitiaUnit>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitiaUnitService,
        {
          provide: getRepositoryToken(MilitiaUnit),
          useValue: createStubInstance(Repository),
        },
        {
          provide: OrganizationScaleService,
          useValue: createStubInstance(OrganizationScaleService),
        },
        {
          provide: AdministrativeUnitService,
          useValue: createStubInstance(AdministrativeUnitService),
        },
      ],
    }).compile();

    service = module.get<MilitiaUnitService>(MilitiaUnitService);
    repository = module.get(getRepositoryToken(MilitiaUnit));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militiaUnits.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitiaUnit>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militiaUnits;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militiaUnits, militiaUnits.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitiaUnit>) => {
        return militiaUnits.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitiaUnit>).code ||
            e.name == (options.where as FindOptionsWhere<MilitiaUnit>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitiaUnit) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militiaUnits.length,
        };
      }
    });
  });

  const militiaUnits = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      organizationScale: {
        id: 1,
      },
      administrativeUnit: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      organizationScale: {
        id: 2,
      },
      administrativeUnit: {
        id: 2,
      },
    },
  ] as MilitiaUnit[];

  describe('MilitiaUnitService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        organizationScale: {
          id: 1,
        },
        administrativeUnit: {
          id: 1,
        },
      } as CreateMilitiaUnitDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code1',
        name: 'tên',
        description: 'Mô tả',
        organizationScale: {
          id: 1,
        },
        administrativeUnit: {
          id: 1,
        },
      } as CreateMilitiaUnitDto;

      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('MilitiaUnitService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militiaUnits.length);
    });
  });

  describe('MilitiaUnitService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('MilitiaUnitService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitiaUnitDto;
      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name2',
      } as UpdateMilitiaUnitDto;
      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('MilitiaUnitService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
