import { PartialType } from '@nestjs/swagger';
import { CreateMilitiaUnitDto } from './create-militia-unit.dto';

export class UpdateMilitiaUnitDto extends PartialType(CreateMilitiaUnitDto) {}
