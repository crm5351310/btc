import { OmitType } from '@nestjs/swagger';
import { MilitiaUnit } from '../entities';

export class CreateMilitiaUnitDto extends OmitType(MilitiaUnit, ['id']) {}
