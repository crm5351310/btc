import { ChildEntity, Column, ManyToOne } from 'typeorm';
import { MilitiaDefenseUnit } from '../../militia-defense-unit.entity';
import { AdministrativeUnit } from '../../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { IsBoolean, IsDefined, IsOptional, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';

@ChildEntity()
export class MilitiaUnit extends MilitiaDefenseUnit {
  // swagger
  @ApiProperty({
    description: 'Đơn vị hành chính',
    type: RelationTypeBase,
  })
  // validator
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  // orm
  @ManyToOne(() => AdministrativeUnit, (administrativeUnit) => administrativeUnit.militiaUnits)
  administrativeUnit: AdministrativeUnit;

  // swagger
  @ApiProperty({
    description: 'Có cấp ủy',
    default: false,
  })
  // validator
  @IsOptional()
  @IsBoolean()
  //orm
  @Column('boolean', { default: false })
  hasCommittee: boolean;

  // swagger
  @ApiProperty({
    description: 'Là chi đoàn quân sự',
    default: false,
  })
  // validator
  @IsOptional()
  @IsBoolean()
  //orm
  @Column('boolean', { default: false })
  isMilitaryBranch: boolean;
}
