import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { MilitiaUnitService } from './militia-unit.service';
import { CreateMilitiaUnitDto } from './dto/create-militia-unit.dto';
import { UpdateMilitiaUnitDto } from './dto/update-militia-unit.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { QueryMilitiaUnitDto } from './dto/query-militia-unit.dto';

@Controller()
@ApiTags(TagEnum.MILITIA_UNIT)
export class MilitiaUnitController {
  constructor(private readonly militiaUnitService: MilitiaUnitService) {}

  @Post()
  create(@Body() createMilitiaUnitDto: CreateMilitiaUnitDto) {
    return this.militiaUnitService.create(createMilitiaUnitDto);
  }

  @Get()
  findAll(@Query() query: QueryMilitiaUnitDto) {
    return this.militiaUnitService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.militiaUnitService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMilitiaUnitDto: UpdateMilitiaUnitDto) {
    return this.militiaUnitService.update(+id, updateMilitiaUnitDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militiaUnitService.remove(ids);
  }
}
