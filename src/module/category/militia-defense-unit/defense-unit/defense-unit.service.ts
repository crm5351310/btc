import { Injectable } from '@nestjs/common';
import { CreateDefenseUnitDto } from './dto/create-defense-unit.dto';
import { UpdateDefenseUnitDto } from './dto/update-defense-unit.dto';
import { BaseCategoryService } from '../../../../common/base/service/base-category.service';
import { DefenseUnit } from './entities';
import { In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../../common/response';
import { QueryDefenseUnitDto } from './dto/query-defense-unit.dto';
import { OrganizationService } from '../../organization-root/organization/organization.service';
import { OrganizationScaleService } from '../../organization-scale/organization-scale.service';

@Injectable()
export class DefenseUnitService extends BaseCategoryService<DefenseUnit> {
  constructor(
    @InjectRepository(DefenseUnit)
    repository: Repository<DefenseUnit>,
    private organizationScaleService: OrganizationScaleService,
    private organizationService: OrganizationService,
  ) {
    super(repository);
  }

  async create(createDefenseUnitDto: CreateDefenseUnitDto): Promise<ResponseCreated<DefenseUnit>> {
    await this.checkExist(['name', 'code'], createDefenseUnitDto);

    // kiểm tra bản ghi quy mô tổ chức có tồn tại không
    await this.organizationScaleService.findOne(createDefenseUnitDto.organizationScale.id);

    // kiểm tra bản ghi tổ chức doanh nghiệp có tồn tại không
    await this.organizationService.findOne(createDefenseUnitDto.organization);

    const defenseUnit: DefenseUnit = await this.repository.save(createDefenseUnitDto);

    return new ResponseCreated(defenseUnit);
  }

  async findAll(query: QueryDefenseUnitDto): Promise<ResponseFindAll<DefenseUnit>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
      relations: {
        organization: true,
        organizationScale: true,
      },
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<DefenseUnit>> {
    await this.checkNotExist(id, DefenseUnit.name);

    const result = await this.repository.findOne({
      where: { id },
      relations: {
        organization: true,
        organizationScale: true,
      },
    });

    return new ResponseFindOne(result);
  }

  async update(id: number, updateDefenseUnitDto: UpdateDefenseUnitDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, DefenseUnit.name);
    await this.checkExist(['name', 'code'], updateDefenseUnitDto, id);

    // kiểm tra bản ghi quy mô tổ chức có tồn tại không
    if (updateDefenseUnitDto.organizationScale) {
      await this.organizationScaleService.findOne(updateDefenseUnitDto.organizationScale.id);
    }

    // kiểm tra bản ghi tổ chức doanh nghiệp có tồn tại không
    if (updateDefenseUnitDto.organization) {
      await this.organizationService.findOne(updateDefenseUnitDto.organization);
    }

    const result = await this.repository.save({
      id,
      ...updateDefenseUnitDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<DefenseUnit>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<DefenseUnit>(results, ids);
  }
}
