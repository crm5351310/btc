import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { DefenseUnitService } from './defense-unit.service';
import { CreateDefenseUnitDto } from './dto/create-defense-unit.dto';
import { UpdateDefenseUnitDto } from './dto/update-defense-unit.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { QueryDefenseUnitDto } from './dto/query-defense-unit.dto';

@Controller()
@ApiTags(TagEnum.DEFENSE_UNIT)
export class DefenseUnitController {
  constructor(private readonly defenseUnitService: DefenseUnitService) { }

  @Post()
  create(@Body() createDefenseUnitDto: CreateDefenseUnitDto) {
    return this.defenseUnitService.create(createDefenseUnitDto);
  }

  @Get()
  findAll(@Query() query: QueryDefenseUnitDto) {
    return this.defenseUnitService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.defenseUnitService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDefenseUnitDto: UpdateDefenseUnitDto) {
    return this.defenseUnitService.update(+id, updateDefenseUnitDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.defenseUnitService.remove(ids);
  }
}
