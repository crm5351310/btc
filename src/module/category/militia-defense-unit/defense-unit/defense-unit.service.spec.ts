import { Test, TestingModule } from '@nestjs/testing';
import { DefenseUnitService } from './defense-unit.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { DefenseUnit } from './entities';
import { CreateDefenseUnitDto } from './dto/create-defense-unit.dto';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../../common/response';
import { OrganizationScaleService } from '../../organization-scale/organization-scale.service';
import { OrganizationService } from '../../organization-root/organization/organization.service';
import { UpdateDefenseUnitDto } from './dto/update-defense-unit.dto';
import { createStubInstance } from 'sinon';

describe('DefenseUnitService', () => {
  let service: DefenseUnitService;
  let repository: Repository<DefenseUnit>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefenseUnitService,
        {
          provide: getRepositoryToken(DefenseUnit),
          useValue: createStubInstance(Repository),
        },
        {
          provide: OrganizationService,
          useValue: createStubInstance(OrganizationService),
        },
        {
          provide: OrganizationScaleService,
          useValue: createStubInstance(OrganizationScaleService),
        },
      ],
    }).compile();

    service = module.get<DefenseUnitService>(DefenseUnitService);
    repository = module.get(getRepositoryToken(DefenseUnit));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        defenseUnits.find(
          (value) => value.id === (options.where as FindOptionsWhere<DefenseUnit>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return defenseUnits;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [defenseUnits, defenseUnits.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<DefenseUnit>) => {
        return defenseUnits.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<DefenseUnit>).code ||
            e.name == (options.where as FindOptionsWhere<DefenseUnit>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: DefenseUnit) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: defenseUnits.length,
        };
      }
    });
  });

  const defenseUnits = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      organizationScale: {
        id: 1,
      },
      organization: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      organizationScale: {
        id: 2,
      },
      organization: {
        id: 2,
      },
    },
  ] as DefenseUnit[];

  describe('MilitiaUnitService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        organizationScale: {
          id: 1,
        },
        organization: {
          id: 1,
        },
      } as CreateDefenseUnitDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code2',
        name: 'tên232',
        description: 'Mô tả',
        organizationScale: {
          id: 1,
        },
        organization: {
          id: 1,
        },
      } as CreateDefenseUnitDto;

      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('MilitiaUnitService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(defenseUnits.length);
    });
  });

  describe('MilitiaUnitService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('MilitiaUnitService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateDefenseUnitDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name2',
      } as UpdateDefenseUnitDto;
      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('MilitiaUnitService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
