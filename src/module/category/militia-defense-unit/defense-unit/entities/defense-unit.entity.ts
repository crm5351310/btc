import { Organization } from '../../../../category/organization-root/organization/organization.entity';
import { MilitiaDefenseUnit } from '../../militia-defense-unit.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsDefined, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ChildEntity, ManyToOne, OneToMany } from 'typeorm';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';

@ChildEntity()
export class DefenseUnit extends MilitiaDefenseUnit {
  // swagger
  @ApiProperty({
    description: 'Tổ chức xã hội - doanh nghiệp',
    type: RelationTypeBase,
  })
  // validator
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(Organization, ['id']))
  // orm
  @ManyToOne(() => Organization, (organization) => organization.defenseUnits)
  // @ManyToOne(() => Organization)
  organization: Organization;

  // @OneToMany(() => CitizenProcess, (process) => process.defenseUnit)
  // citizenProcesses: CitizenProcess[];
}
