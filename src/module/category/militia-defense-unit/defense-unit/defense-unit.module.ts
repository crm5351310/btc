import { Module } from '@nestjs/common';
import { DefenseUnitService } from './defense-unit.service';
import { DefenseUnitController } from './defense-unit.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DefenseUnit } from './entities';
import { OrganizationModule } from '../../organization-root/organization/organization.module';
import { OrganizationScaleModule } from '../../organization-scale/organization-scale.module';

@Module({
  imports: [TypeOrmModule.forFeature([DefenseUnit]), OrganizationModule, OrganizationScaleModule],
  controllers: [DefenseUnitController],
  providers: [DefenseUnitService],
  exports: [DefenseUnitService],
})
export class DefenseUnitModule {
  static readonly route: RouteTree = {
    path: 'defense-unit',
    module: DefenseUnitModule,
  };
}
