import { PartialType } from '@nestjs/swagger';
import { CreateDefenseUnitDto } from './create-defense-unit.dto';

export class UpdateDefenseUnitDto extends PartialType(CreateDefenseUnitDto) {}
