import { OmitType } from '@nestjs/swagger';
import { DefenseUnit } from '../entities';

export class CreateDefenseUnitDto extends OmitType(DefenseUnit, ['id']) {}
