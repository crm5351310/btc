import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { VehicleTypeService } from './vehicle-type.service';
import { CreateVehicleTypeDto } from './dto/create-vehicle-type.dto';
import { UpdateVehicleTypeDto } from './dto/update-vehicle-type.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.VEHICLE_TYPE)
export class VehicleTypeController {
  constructor(private readonly vehicleTypeService: VehicleTypeService) {}

  @Post()
  create(@Body() createVehicleTypeDto: CreateVehicleTypeDto) {
    return this.vehicleTypeService.create(createVehicleTypeDto);
  }

  @Get()
  findAll(@Query() param: PathDto) {
    return this.vehicleTypeService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.vehicleTypeService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateVehicleTypeDto: UpdateVehicleTypeDto) {
    return this.vehicleTypeService.update(path, updateVehicleTypeDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.vehicleTypeService.remove(ids);
  }
}
