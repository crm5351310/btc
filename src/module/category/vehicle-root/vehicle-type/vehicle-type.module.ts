import { Module } from '@nestjs/common';
import { VehicleTypeService } from './vehicle-type.service';
import { VehicleTypeController } from './vehicle-type.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VehicleType } from './vehicle-type.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { VehicleCategory } from '../vehicle-category/vehicle-category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([VehicleType, VehicleCategory])],
  controllers: [VehicleTypeController],
  providers: [VehicleTypeService],
})
export class VehicleTypeModule {}
export const vehicleTypeRouter: RouteTree = {
  path: 'vehicle-type',
  module: VehicleTypeModule,
  children: [],
};
