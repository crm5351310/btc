import { Injectable } from '@nestjs/common';
import { CreateVehicleTypeDto } from './dto/create-vehicle-type.dto';
import { UpdateVehicleTypeDto } from './dto/update-vehicle-type.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { VehicleType } from './vehicle-type.entity';
import { BaseMessage } from '../../../../common/message/base.message';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { VehicleCategory } from '../vehicle-category/vehicle-category.entity';

@Injectable()
export class VehicleTypeService extends BaseService {
  constructor(
    @InjectRepository(VehicleType)
    private readonly vehicleTypeRepository: Repository<VehicleType>,

    @InjectRepository(VehicleCategory)
    private readonly vehicleCategoryRepository: Repository<VehicleCategory>,
  ) {
    super();
    this.name = VehicleTypeService.name;
    this.subject = SubjectMessage.VEHICLE_TYPE;
  }
  async create(createVehicleTypeDto: CreateVehicleTypeDto) {
    this.action = BaseMessage.CREATE;

    const vehicleCategory = await this.vehicleCategoryRepository.findOne({
      where: {
        id: createVehicleTypeDto.vehicleCategory.id,
      },
    });
    if (!vehicleCategory) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.VEHICAL_CATEGORY,
        false,
      );
    }

    const nameDublicate = await this.vehicleTypeRepository.findOne({
      where: {
        name: createVehicleTypeDto.name,
        vehicleCategory: {
          id: createVehicleTypeDto.vehicleCategory.id,
        },
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.vehicleTypeRepository.findOne({
      where: {
        code: createVehicleTypeDto.code,
        vehicleCategory: {
          id: createVehicleTypeDto.vehicleCategory.id,
        },
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }
    return this.response(await this.vehicleTypeRepository.save(createVehicleTypeDto));
  }

  async findAll(param: PathDto) {
    this.action = BaseMessage.READ;
    const vehicalType = await this.vehicleTypeRepository.find({
      where: {
        vehicleCategory: {
          id: param.id,
        },
      },
    });
    return this.response(vehicalType);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.vehicleTypeRepository.findOne({
      where: path,
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    return this.response(result);
  }

  async update(path: PathDto, updateVehicleTypeDto: UpdateVehicleTypeDto) {
    this.action = BaseMessage.UPDATE;
    const vehicalType = await this.vehicleTypeRepository.findOne({
      where: path,
    });
    if (!vehicalType) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }

    if (updateVehicleTypeDto.vehicleCategory) {
      const vehicleGroup = await this.vehicleCategoryRepository.findOne({
        where: {
          id: updateVehicleTypeDto.vehicleCategory.id,
        },
      });
      if (!vehicleGroup) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.VEHICAL_GROUP,
          false,
        );
      }
    }
    const nameDublicate = await this.vehicleTypeRepository.findOne({
      where: {
        name: updateVehicleTypeDto.name,
        id: Not(path.id),
        vehicleCategory: {
          id: updateVehicleTypeDto.vehicleCategory!.id,
        },
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.vehicleTypeRepository.findOne({
      where: {
        code: updateVehicleTypeDto.code,
        id: Not(path.id),
        vehicleCategory: {
          id: updateVehicleTypeDto.vehicleCategory!.id,
        },
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    return this.response(
      await this.vehicleTypeRepository.save({
        ...updateVehicleTypeDto,
        id: path.id,
      }),
    );
  }

  async remove(ids: number[]) {
    this.action = BaseMessage.DELETE;
    const vehicalType = await this.vehicleTypeRepository.find({
      where: {
        id: In(ids),
      },
    });
    if (!vehicalType.length) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, this.subject, true);
    }
    await this.vehicleTypeRepository.softRemove(vehicalType);
    return this.response({
      affected: ids.length - vehicalType.length + vehicalType.length,
    });
  }
}
