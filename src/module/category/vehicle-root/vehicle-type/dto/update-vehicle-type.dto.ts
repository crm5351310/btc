import { CreateVehicleTypeDto } from './create-vehicle-type.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateVehicleTypeDto extends PartialType(CreateVehicleTypeDto) {}
