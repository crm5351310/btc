import { OmitType } from '@nestjs/swagger';
import { VehicleType } from '../vehicle-type.entity';

export class CreateVehicleTypeDto extends OmitType(VehicleType, ['id'] as const) {}
