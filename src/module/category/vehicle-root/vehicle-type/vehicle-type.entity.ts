import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../common/base/class/base.class';
import { Type } from 'class-transformer';
import { VehicleGroup } from '../vehicle-group/vehicle-group.entity';
import { VehicleCategory } from '../vehicle-category/vehicle-category.entity';

@Entity()
export class VehicleType extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  // entity
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  // entity
  @Column('varchar', {
    length: 25,
    nullable: false,
  })
  code: string;

  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  // entity
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  // swagger
  @ApiProperty({
    description: 'Loại phương tiện',
    type: RelationTypeBase,
  })
  // validate
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(VehicleCategory, ['id']))
  // entity
  @ManyToOne(() => VehicleCategory, (item) => item.vehicleTypes)
  @JoinColumn()
  vehicleCategory: VehicleCategory;
}
