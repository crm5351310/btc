import { Injectable } from '@nestjs/common';
import { CreateVehicleGroupDto } from './dto/create-vehicle-group.dto';
import { UpdateVehicleGroupDto } from './dto/update-vehicle-group.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from '../../job/job.entity';
import { In, Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { VehicleGroup } from './vehicle-group.entity';
import { BaseMessage } from '../../../../common/message/base.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';

@Injectable()
export class VehicleGroupService extends BaseService {
  constructor(
    @InjectRepository(VehicleGroup)
    private readonly vehicleGroupRepository: Repository<VehicleGroup>,
  ) {
    super();
    this.name = VehicleGroupService.name;
    this.subject = SubjectMessage.VEHICLE_GROUP;
  }
  async create(createVehicleGroupDto: CreateVehicleGroupDto) {
    this.action = BaseMessage.CREATE;

    const nameDublicate = await this.vehicleGroupRepository.findOne({
      where: {
        name: createVehicleGroupDto.name,
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.vehicleGroupRepository.findOne({
      where: {
        code: createVehicleGroupDto.code,
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }
    return this.response(await this.vehicleGroupRepository.save(createVehicleGroupDto));
  }

  async findAll() {
    this.action = BaseMessage.READ;
    const vehicalGroup = await this.vehicleGroupRepository.find();
    return this.response(vehicalGroup);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.vehicleGroupRepository.findOne({
      where: path,
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    return this.response(result);
  }

  async update(path: PathDto, updateVehicleGroupDto: UpdateVehicleGroupDto) {
    this.action = BaseMessage.UPDATE;
    const religion = await this.vehicleGroupRepository.findOne({
      where: path,
    });
    if (!religion) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    const nameDublicate = await this.vehicleGroupRepository.findOne({
      where: {
        name: updateVehicleGroupDto.name,
        id: Not(path.id),
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.vehicleGroupRepository.findOne({
      where: {
        code: updateVehicleGroupDto.code,
        id: Not(path.id),
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    return this.response(
      await this.vehicleGroupRepository.save({
        ...updateVehicleGroupDto,
        id: path.id,
      }),
    );
  }

  async remove(ids: number[]) {
    this.action = BaseMessage.DELETE;
    const vehicleArr: any = [];
    const vehicalGroups = await this.vehicleGroupRepository.find({
      relations: {
        vehicleCategories: true,
      },
      where: {
        id: In(ids),
      },
    });
    if (!vehicalGroups.length) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, this.subject, true);
    }
    for (let i = 0; i < vehicalGroups.length; i++) {
      if (!vehicalGroups[i].vehicleCategories?.length) {
        vehicleArr.push(vehicalGroups[i]);
      }
    }
    await this.vehicleGroupRepository.softRemove(vehicalGroups);
    return this.response({
      affected: ids.length - vehicalGroups.length + vehicalGroups.length,
    });
  }
}
