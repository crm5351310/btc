import { OmitType } from '@nestjs/swagger';
import { VehicleGroup } from '../vehicle-group.entity';

export class CreateVehicleGroupDto extends OmitType(VehicleGroup, ['id'] as const) {}
