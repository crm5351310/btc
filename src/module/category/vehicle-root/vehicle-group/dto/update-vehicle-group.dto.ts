import { CreateVehicleGroupDto } from './create-vehicle-group.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateVehicleGroupDto extends PartialType(CreateVehicleGroupDto) {}
