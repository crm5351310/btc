import { Controller, Get, Post, Body, Patch, Param, Delete, ParseArrayPipe } from '@nestjs/common';
import { VehicleGroupService } from './vehicle-group.service';
import { CreateVehicleGroupDto } from './dto/create-vehicle-group.dto';
import { UpdateVehicleGroupDto } from './dto/update-vehicle-group.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.VEHICLE_GROUP)
export class VehicleGroupController {
  constructor(private readonly vehicleGroupService: VehicleGroupService) {}

  @Post()
  create(@Body() createVehicleGroupDto: CreateVehicleGroupDto) {
    return this.vehicleGroupService.create(createVehicleGroupDto);
  }

  @Get()
  findAll() {
    return this.vehicleGroupService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.vehicleGroupService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateVehicleGroupDto: UpdateVehicleGroupDto) {
    return this.vehicleGroupService.update(path, updateVehicleGroupDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.vehicleGroupService.remove(ids);
  }
}
