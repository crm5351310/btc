// npm run test:run src/module/category/vehicle-root/vehicle-group/vehicle-group.service.spec.ts
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { createStubInstance } from 'sinon';
import { PathDto } from '../../../../common/base/class/base.class';
import { VehicleGroupService } from './vehicle-group.service';
import { VehicleGroup } from './vehicle-group.entity';

describe('VehicleGroupService', () => {
  const payload = [
    {
      id: 1,
      name: 'ten',
      code: 'ten',
    },
  ] as VehicleGroup[];

  let vehicleGroupService: VehicleGroupService;
  let vehicleGroupRepository: Repository<VehicleGroup>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VehicleGroupService,
        {
          provide: getRepositoryToken(VehicleGroup),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    vehicleGroupService = module.get<VehicleGroupService>(VehicleGroupService);
    vehicleGroupRepository = module.get(getRepositoryToken(VehicleGroup));

    jest.spyOn(vehicleGroupRepository, 'findOne').mockImplementation(async (options) => {
      return (
        payload.find(
          (value) => value.id === (options.where as FindOptionsWhere<VehicleGroup>).id,
        ) ?? null
      );
    });

    jest.spyOn(vehicleGroupRepository, 'find').mockImplementation(async () => {
      return payload;
    });

    jest.spyOn(vehicleGroupRepository, 'findAndCount').mockImplementation(async () => {
      return [payload, payload.length];
    });

    jest
      .spyOn(vehicleGroupRepository, 'exist')
      .mockImplementation(async (options: FindManyOptions<VehicleGroup>) => {
        return payload.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<VehicleGroup>).code ||
            e.name == (options.where as FindOptionsWhere<VehicleGroup>).name,
        );
      });

    jest.spyOn(vehicleGroupRepository, 'save').mockImplementation(async (entity: VehicleGroup) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: payload.length,
        };
      }
    });
  });

  describe('VehicleGroup => create', () => {
    it('Create success', async () => {
      const result = await vehicleGroupService.create(payload[0]);

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });
  });

  describe('VehicleGroup => findOne', () => {
    it('Find success', async () => {
      const result = await vehicleGroupService.findOne({ id: 1 } as PathDto);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Find failed', async () => {
      await expect(vehicleGroupService.findOne({ id: 2 })).rejects.toThrowError();
    });
  });

  describe('VehicleGroup => update', () => {
    it('Update success', async () => {
      const result = await vehicleGroupService.update({ id: 1 }, payload[0]);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });
  });

  describe('VehicleGroupService => findAll', () => {
    it('Find all success', async () => {
      const result = await vehicleGroupService.findAll();

      expect(result.statusCode).toBe(200);
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('VehicleGroupService => remove', () => {
    it('Remove success', async () => {
      const result = await vehicleGroupService.remove([1, 2, 3]);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Remove failed', async () => {
      // logic for throwing error in here
      //
    });
  });
});
