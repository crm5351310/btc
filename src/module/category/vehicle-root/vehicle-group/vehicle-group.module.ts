import { Module } from '@nestjs/common';
import { VehicleGroupService } from './vehicle-group.service';
import { VehicleGroupController } from './vehicle-group.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VehicleGroup } from './vehicle-group.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([VehicleGroup])],
  controllers: [VehicleGroupController],
  providers: [VehicleGroupService],
})
export class VehicleGroupModule {}
export const vehicleGroupRouter: RouteTree = {
  path: 'vehicle-group',
  module: VehicleGroupModule,
  children: [],
};
