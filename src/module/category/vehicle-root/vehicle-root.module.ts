import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { VehicleGroupModule, vehicleGroupRouter } from './vehicle-group/vehicle-group.module';
import {
  VehicleCategoryModule,
  vehicleCategoryRouter,
} from './vehicle-category/vehicle-category.module';
import { VehicleTypeModule, vehicleTypeRouter } from './vehicle-type/vehicle-type.module';
@Module({
  controllers: [],
  providers: [],
  imports: [VehicleGroupModule, VehicleCategoryModule, VehicleTypeModule],
})
export class VehicleRootModule {}
export const vehicleRootRouter: RouteTree = {
  path: 'vehicle-root',
  module: VehicleRootModule,
  children: [vehicleGroupRouter, vehicleTypeRouter, vehicleCategoryRouter],
};
