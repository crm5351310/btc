import { OmitType } from '@nestjs/swagger';
import { VehicleCategory } from '../vehicle-category.entity';

export class CreateVehicleCategoryDto extends OmitType(VehicleCategory, ['id'] as const) {}
