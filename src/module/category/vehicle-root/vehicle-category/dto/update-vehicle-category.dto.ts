import { CreateVehicleCategoryDto } from './create-vehicle-category.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateVehicleCategoryDto extends PartialType(CreateVehicleCategoryDto) {}
