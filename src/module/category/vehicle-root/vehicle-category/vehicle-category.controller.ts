import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { VehicleCategoryService } from './vehicle-category.service';
import { CreateVehicleCategoryDto } from './dto/create-vehicle-category.dto';
import { UpdateVehicleCategoryDto } from './dto/update-vehicle-category.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.VEHICLE_CATEGORY)
export class VehicleCategoryController {
  constructor(private readonly vehicleCategoryService: VehicleCategoryService) {}

  @Post()
  create(@Body() createVehicleCategoryDto: CreateVehicleCategoryDto) {
    return this.vehicleCategoryService.create(createVehicleCategoryDto);
  }

  @Get()
  findAll(@Query() param: PathDto) {
    return this.vehicleCategoryService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.vehicleCategoryService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateVehicleCategoryDto: UpdateVehicleCategoryDto) {
    return this.vehicleCategoryService.update(path, updateVehicleCategoryDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.vehicleCategoryService.remove(ids);
  }
}
