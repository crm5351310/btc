// npm run test:run src/module/category/vehicle-root/vehicle-category/vehicle-category.service.spec.ts
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { createStubInstance } from 'sinon';
import { PathDto } from '../../../../common/base/class/base.class';
import { VehicleCategory } from './vehicle-category.entity';
import { VehicleCategoryService } from './vehicle-category.service';
import { VehicleGroup } from '../vehicle-group/vehicle-group.entity';
import { VehicleGroupService } from '../vehicle-group/vehicle-group.service';

describe('VehicleCategoryService', () => {
  const payload = [
    {
      id: 1,
      name: 'ten',
      code: 'ten',
      vehicleGroup: {
        id: 1,
      },
    },
  ] as VehicleCategory[];

  const payloadGroup = [
    {
      id: 1,
      name: 'ten',
      code: 'ten',
    },
  ] as VehicleGroup[];

  let vehicleCategoryService: VehicleCategoryService;
  let vehicleGroupService: VehicleGroupService;
  let vehicleCategoryRepository: Repository<VehicleCategory>;
  let vehicleGroupRepository: Repository<VehicleGroup>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VehicleCategoryService,
        VehicleGroupService,
        {
          provide: getRepositoryToken(VehicleCategory),
          useValue: createStubInstance(Repository),
        },
        {
          provide: getRepositoryToken(VehicleGroup),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    vehicleCategoryService = module.get<VehicleCategoryService>(VehicleCategoryService);
    vehicleGroupService = module.get<VehicleGroupService>(VehicleGroupService);
    vehicleCategoryRepository = module.get(getRepositoryToken(VehicleCategory));
    vehicleGroupRepository = module.get(getRepositoryToken(VehicleGroup));

    jest.spyOn(vehicleCategoryRepository, 'findOne').mockImplementation(async (options) => {
      return (
        payload.find(
          (value) => value.id === (options.where as FindOptionsWhere<VehicleCategory>).id,
        ) ?? null
      );
    });

    jest.spyOn(vehicleGroupRepository, 'findOne').mockImplementation(async (options) => {
      return (
        payloadGroup.find(
          (value) => value.id === (options.where as FindOptionsWhere<VehicleGroup>).id,
        ) ?? null
      );
    });

    jest.spyOn(vehicleCategoryRepository, 'find').mockImplementation(async () => {
      return payload;
    });

    jest.spyOn(vehicleCategoryRepository, 'findAndCount').mockImplementation(async () => {
      return [payload, payload.length];
    });

    jest
      .spyOn(vehicleCategoryRepository, 'exist')
      .mockImplementation(async (options: FindManyOptions<VehicleCategory>) => {
        return payload.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<VehicleCategory>).code ||
            e.name == (options.where as FindOptionsWhere<VehicleCategory>).name,
        );
      });

    jest
      .spyOn(vehicleCategoryRepository, 'save')
      .mockImplementation(async (entity: VehicleCategory) => {
        if (entity.id) {
          return entity;
        } else {
          return {
            ...entity,
            id: payload.length,
          };
        }
      });
  });

  describe('VehicleCategory => create', () => {
    it('Create success', async () => {
      const result = await vehicleCategoryService.create(payload[0]);

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Create failed', async () => {
      const dataFailed = JSON.parse(JSON.stringify(payload));
      delete dataFailed[0].vehicleGroup;
      await expect(vehicleCategoryService.create(dataFailed[0])).rejects.toThrowError();
    });
  });

  describe('VehicleCategory => findOne', () => {
    it('Find success', async () => {
      const result = await vehicleCategoryService.findOne({ id: 1 } as PathDto);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Find failed', async () => {
      await expect(vehicleCategoryService.findOne({ id: 2 })).rejects.toThrowError();
    });
  });

  describe('VehicleCategory => update', () => {
    it('Update success', async () => {
      const result = await vehicleCategoryService.update({ id: 1 }, payload[0]);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Update failed', async () => {
      const dataFailed = JSON.parse(JSON.stringify(payload));
      delete dataFailed[0].vehicleGroup;
      await expect(vehicleCategoryService.update({ id: 1 }, dataFailed[0])).rejects.toThrowError();
    });
  });

  describe('VehicleCategoryService => findAll', () => {
    it('Find all success', async () => {
      const result = await vehicleCategoryService.findAll({ id: 1 });

      expect(result.statusCode).toBe(200);
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('VehicleCategoryService => remove', () => {
    it('Remove success', async () => {
      const result = await vehicleCategoryService.remove([1, 2, 3]);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Remove failed', async () => {
      // logic for throwing error in here
      //
    });
  });
});
