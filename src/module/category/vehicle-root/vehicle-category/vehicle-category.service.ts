import { Injectable } from '@nestjs/common';
import { CreateVehicleCategoryDto } from './dto/create-vehicle-category.dto';
import { UpdateVehicleCategoryDto } from './dto/update-vehicle-category.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { BaseMessage } from '../../../../common/message/base.message';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { VehicleCategory } from './vehicle-category.entity';
import { VehicleGroupService } from '../vehicle-group/vehicle-group.service';
import { VehicleGroup } from '../vehicle-group/vehicle-group.entity';

@Injectable()
export class VehicleCategoryService extends BaseService {
  constructor(
    @InjectRepository(VehicleCategory)
    private readonly vehicleCategoryRepository: Repository<VehicleCategory>,
    @InjectRepository(VehicleGroup)
    private readonly vehicleGroupRepository: Repository<VehicleGroup>,
  ) {
    super();
    this.name = VehicleCategoryService.name;
    this.subject = SubjectMessage.VEHICLE_CATEGORY;
  }
  async create(createVehicleCategoryDto: CreateVehicleCategoryDto) {
    this.action = BaseMessage.CREATE;

    const vehicleGroup = await this.vehicleGroupRepository.findOne({
      where: {
        id: createVehicleCategoryDto.vehicleGroup.id,
      },
    });
    if (!vehicleGroup) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.VEHICAL_GROUP,
        false,
      );
    }

    const nameDublicate = await this.vehicleCategoryRepository.findOne({
      where: {
        name: createVehicleCategoryDto.name,
        vehicleGroup: {
          id: createVehicleCategoryDto.vehicleGroup.id,
        },
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.vehicleCategoryRepository.findOne({
      where: {
        code: createVehicleCategoryDto.code,
        vehicleGroup: {
          id: createVehicleCategoryDto.vehicleGroup.id,
        },
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }
    return this.response(await this.vehicleCategoryRepository.save(createVehicleCategoryDto));
  }

  async findAll(param: PathDto) {
    this.action = BaseMessage.READ;
    const vehicalCategory = await this.vehicleCategoryRepository.find({
      where: {
        vehicleGroup: {
          id: param.id,
        },
      },
    });
    return this.response(vehicalCategory);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.vehicleCategoryRepository.findOne({
      where: path,
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    return this.response(result);
  }

  async update(path: PathDto, updateVehicleCategoryDto: UpdateVehicleCategoryDto) {
    this.action = BaseMessage.UPDATE;
    const vehicleCategory = await this.vehicleCategoryRepository.findOne({
      where: path,
    });
    if (!vehicleCategory) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }

    if (updateVehicleCategoryDto.vehicleGroup) {
      const vehicleGroup = await this.vehicleGroupRepository.findOne({
        where: {
          id: updateVehicleCategoryDto.vehicleGroup.id,
        },
      });
      if (!vehicleGroup) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.VEHICAL_GROUP,
          false,
        );
      }
    }
    const nameDublicate = await this.vehicleCategoryRepository.findOne({
      where: {
        name: updateVehicleCategoryDto.name,
        id: Not(path.id),
        vehicleGroup: {
          id: updateVehicleCategoryDto.vehicleGroup!.id,
        },
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.vehicleCategoryRepository.findOne({
      where: {
        code: updateVehicleCategoryDto.code,
        id: Not(path.id),
        vehicleGroup: {
          id: updateVehicleCategoryDto.vehicleGroup!.id,
        },
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    return this.response(
      await this.vehicleCategoryRepository.save({
        ...updateVehicleCategoryDto,
        id: path.id,
      }),
    );
  }

  async remove(ids: number[]) {
    this.action = BaseMessage.DELETE;
    const vehicleArr: any = [];
    const vehicalCategory = await this.vehicleCategoryRepository.find({
      relations: {
        vehicleTypes: true,
      },
      where: {
        id: In(ids),
      },
    });

    if (!vehicalCategory.length) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, this.subject, true);
    }

    for (let i = 0; i < vehicalCategory.length; i++) {
      if (!vehicalCategory[i].vehicleTypes?.length) {
        vehicleArr.push(vehicalCategory[i]);
      }
    }
    await this.vehicleCategoryRepository.softRemove(vehicleArr);
    return this.response({
      affected: ids.length - vehicalCategory.length + vehicalCategory.length,
    });
  }
}
