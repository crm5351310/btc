import { Module } from '@nestjs/common';
import { VehicleCategoryService } from './vehicle-category.service';
import { VehicleCategoryController } from './vehicle-category.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VehicleCategory } from './vehicle-category.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { VehicleGroup } from '../vehicle-group/vehicle-group.entity';

@Module({
  imports: [TypeOrmModule.forFeature([VehicleCategory, VehicleGroup])],
  controllers: [VehicleCategoryController],
  providers: [VehicleCategoryService],
})
export class VehicleCategoryModule {}
export const vehicleCategoryRouter: RouteTree = {
  path: 'vehicle-category',
  module: VehicleCategoryModule,
  children: [],
};
