import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { DefenseSecurityTrainee } from './entities/defense-security-trainee.entity';
import { CreateDefenseSecurityTraineeDto } from './dto/create-defense-security-trainee.dto';
import { QueryDefenseSecurityTraineeDto } from './dto/query-defense-security-trainee.dto';
import { UpdateDefenseSecurityTraineeDto } from './dto/update-defense-security-trainee.dto';

@Injectable()
export class DefenseSecurityTraineeService extends BaseCategoryService<DefenseSecurityTrainee> {
  constructor(
    @InjectRepository(DefenseSecurityTrainee)
    repository: Repository<DefenseSecurityTrainee>,
  ) {
    super(repository);
  }
  async create(
    createDefenseSecurityTraineeDto: CreateDefenseSecurityTraineeDto,
  ): Promise<ResponseCreated<DefenseSecurityTrainee>> {
    await this.checkExist(['name', 'code'], createDefenseSecurityTraineeDto);
    const defensesecuritytrainee: DefenseSecurityTrainee = await this.repository.save(
      createDefenseSecurityTraineeDto,
    );

    return new ResponseCreated(defensesecuritytrainee);
  }

  async findAll(
    query: QueryDefenseSecurityTraineeDto,
  ): Promise<ResponseFindAll<DefenseSecurityTrainee>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<DefenseSecurityTrainee>> {
    const result = await this.checkNotExist(id, DefenseSecurityTrainee.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateDefenseSecurityTraineeDto: UpdateDefenseSecurityTraineeDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, DefenseSecurityTrainee.name);
    await this.checkExist(['name', 'code'], updateDefenseSecurityTraineeDto, id);
    const result = await this.repository.save({
      id,
      ...updateDefenseSecurityTraineeDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<DefenseSecurityTrainee>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
