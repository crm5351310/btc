import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DefenseSecurityTraineeController } from './defense-security-trainee.controller';
import { DefenseSecurityTraineeService } from './defense-security-trainee.service';
import { DefenseSecurityTrainee } from './entities/defense-security-trainee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DefenseSecurityTrainee])],
  controllers: [DefenseSecurityTraineeController],
  providers: [DefenseSecurityTraineeService],
})
export class DefenseSecurityTraineeModule {
  static readonly route: RouteTree = {
    path: 'defense-security-trainee',
    module: DefenseSecurityTraineeModule,
  };
}
