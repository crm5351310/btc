import { PartialType } from '@nestjs/swagger';
import { CreateDefenseSecurityTraineeDto } from './create-defense-security-trainee.dto';

export class UpdateDefenseSecurityTraineeDto extends PartialType(CreateDefenseSecurityTraineeDto) {}
