import { OmitType } from '@nestjs/swagger';
import { DefenseSecurityTrainee } from '../entities/defense-security-trainee.entity';

export class CreateDefenseSecurityTraineeDto extends OmitType(DefenseSecurityTrainee, ['id']) {}
