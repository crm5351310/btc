import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { DefenseSecurityTraineeService } from './defense-security-trainee.service';
import { CreateDefenseSecurityTraineeDto } from './dto/create-defense-security-trainee.dto';
import { QueryDefenseSecurityTraineeDto } from './dto/query-defense-security-trainee.dto';
import { UpdateDefenseSecurityTraineeDto } from './dto/update-defense-security-trainee.dto';

@Controller()
@ApiTags(TagEnum.DEFENSE_SECURITY_TRAINEE)
export class DefenseSecurityTraineeController {
  constructor(private readonly defensesecuritytraineeService: DefenseSecurityTraineeService) {}

  @Post()
  create(@Body() createDefenseSecurityTraineeDto: CreateDefenseSecurityTraineeDto) {
    return this.defensesecuritytraineeService.create(createDefenseSecurityTraineeDto);
  }

  @Get()
  findAll(@Query() query: QueryDefenseSecurityTraineeDto) {
    return this.defensesecuritytraineeService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.defensesecuritytraineeService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDefenseSecurityTraineeDto: UpdateDefenseSecurityTraineeDto,
  ) {
    return this.defensesecuritytraineeService.update(+id, updateDefenseSecurityTraineeDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.defensesecuritytraineeService.remove(ids);
  }
}
