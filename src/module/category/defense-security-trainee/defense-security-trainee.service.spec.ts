import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { DefenseSecurityTraineeService } from './defense-security-trainee.service';
import { DefenseSecurityTrainee } from './entities/defense-security-trainee.entity';
import { CreateDefenseSecurityTraineeDto } from './dto/create-defense-security-trainee.dto';
import { UpdateDefenseSecurityTraineeDto } from './dto/update-defense-security-trainee.dto';

describe('DefenseSecurityTraineeService', () => {
  let service: DefenseSecurityTraineeService;
  let repository: Repository<DefenseSecurityTrainee>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefenseSecurityTraineeService,
        {
          provide: getRepositoryToken(DefenseSecurityTrainee),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<DefenseSecurityTraineeService>(DefenseSecurityTraineeService);
    repository = module.get(getRepositoryToken(DefenseSecurityTrainee));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        defensesecuritytrainees.find(
          (value) => value.id === (options.where as FindOptionsWhere<DefenseSecurityTrainee>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return defensesecuritytrainees;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [defensesecuritytrainees, defensesecuritytrainees.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<DefenseSecurityTrainee>) => {
        return defensesecuritytrainees.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<DefenseSecurityTrainee>).code ||
            e.name == (options.where as FindOptionsWhere<DefenseSecurityTrainee>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: DefenseSecurityTrainee) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: defensesecuritytrainees.length,
        };
      }
    });
  });

  const defensesecuritytrainees = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as DefenseSecurityTrainee[];

  describe('DefenseSecurityTraineeService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateDefenseSecurityTraineeDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('DefenseSecurityTraineeService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(defensesecuritytrainees.length);
    });
  });

  describe('DefenseSecurityTraineeService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('DefenseSecurityTraineeService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateDefenseSecurityTraineeDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateDefenseSecurityTraineeDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('DefenseSecurityTraineeService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
