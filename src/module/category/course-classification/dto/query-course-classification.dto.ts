import { BaseQueryDto } from 'src/common/base/dto/base-query.dto';

export class QueryCourseClassificationDto extends BaseQueryDto {}
