import { PartialType } from '@nestjs/swagger';
import { CreateCourseClassificationDto } from './create-course-classification.dto';

export class UpdateCourseClassificationDto extends PartialType(CreateCourseClassificationDto) {}
