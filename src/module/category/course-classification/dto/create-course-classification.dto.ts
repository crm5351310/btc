import { OmitType } from '@nestjs/swagger';
import { CourseClassification } from '../entities/course-classification.entity';

export class CreateCourseClassificationDto extends OmitType(CourseClassification, ['id']) {}
