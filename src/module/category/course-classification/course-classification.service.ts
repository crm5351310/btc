import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { CreateCourseClassificationDto } from './dto/create-course-classification.dto';
import { QueryCourseClassificationDto } from './dto/query-course-classification.dto';
import { UpdateCourseClassificationDto } from './dto/update-course-classification.dto';
import { CourseClassification } from './entities/course-classification.entity';

@Injectable()
export class CourseClassificationService extends BaseCategoryService<CourseClassification> {
  constructor(
    @InjectRepository(CourseClassification)
    repository: Repository<CourseClassification>,
  ) {
    super(repository);
  }
  async create(
    createCourseClassificationDto: CreateCourseClassificationDto,
  ): Promise<ResponseCreated<CourseClassification>> {
    await this.checkExist(['name', 'code'], createCourseClassificationDto);
    const courseClassification: CourseClassification = await this.repository.save(
      createCourseClassificationDto,
    );

    return new ResponseCreated(courseClassification);
  }

  async findAll(
    query: QueryCourseClassificationDto,
  ): Promise<ResponseFindAll<CourseClassification>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<CourseClassification>> {
    const result = await this.checkNotExist(id, CourseClassification.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateCourseClassificationDto: UpdateCourseClassificationDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, CourseClassification.name);
    await this.checkExist(['name', 'code'], updateCourseClassificationDto, id);
    const result = await this.repository.save({
      id,
      ...updateCourseClassificationDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<CourseClassification>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
