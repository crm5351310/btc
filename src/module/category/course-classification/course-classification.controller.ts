import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { CourseClassificationService } from './course-classification.service';
import { CreateCourseClassificationDto } from './dto/create-course-classification.dto';
import { QueryCourseClassificationDto } from './dto/query-course-classification.dto';
import { UpdateCourseClassificationDto } from './dto/update-course-classification.dto';

@Controller()
@ApiTags(TagEnum.COURSE_CLASSIFICATION)
export class CourseClassificationController {
  constructor(private readonly courseClassificationService: CourseClassificationService) {}

  @Post()
  create(@Body() createCourseClassificationDto: CreateCourseClassificationDto) {
    return this.courseClassificationService.create(createCourseClassificationDto);
  }

  @Get()
  findAll(@Query() query: QueryCourseClassificationDto) {
    return this.courseClassificationService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.courseClassificationService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCourseClassificationDto: UpdateCourseClassificationDto,
  ) {
    return this.courseClassificationService.update(+id, updateCourseClassificationDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.courseClassificationService.remove(ids);
  }
}
