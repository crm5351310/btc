import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseClassificationController } from './course-classification.controller';
import { CourseClassificationService } from './course-classification.service';
import { CourseClassification } from './entities/course-classification.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CourseClassification])],
  controllers: [CourseClassificationController],
  providers: [CourseClassificationService],
})
export class CourseClassificationModule {
  static readonly route: RouteTree = {
    path: 'course-classification',
    module: CourseClassificationModule,
  };
}
