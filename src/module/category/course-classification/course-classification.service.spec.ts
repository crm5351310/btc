import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { CourseClassificationService } from './course-classification.service';
import { CreateCourseClassificationDto } from './dto/create-course-classification.dto';
import { UpdateCourseClassificationDto } from './dto/update-course-classification.dto';
import { CourseClassification } from './entities/course-classification.entity';

describe('CourseClassificationService', () => {
  let service: CourseClassificationService;
  let repository: Repository<CourseClassification>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CourseClassificationService,
        {
          provide: getRepositoryToken(CourseClassification),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<CourseClassificationService>(CourseClassificationService);
    repository = module.get(getRepositoryToken(CourseClassification));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        courseClassifications.find(
          (value) => value.id === (options.where as FindOptionsWhere<CourseClassification>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return courseClassifications;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [courseClassifications, courseClassifications.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<CourseClassification>) => {
        return courseClassifications.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<CourseClassification>).code ||
            e.name == (options.where as FindOptionsWhere<CourseClassification>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: CourseClassification) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: courseClassifications.length,
        };
      }
    });
  });

  const courseClassifications = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as CourseClassification[];

  describe('CourseClassificationService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateCourseClassificationDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('CourseClassificationService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(courseClassifications.length);
    });
  });

  describe('CourseClassificationService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('CourseClassificationService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateCourseClassificationDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateCourseClassificationDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('CourseClassificationService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
