import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MaxLength } from 'class-validator';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class CourseClassification extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;
}
