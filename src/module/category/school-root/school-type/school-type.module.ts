import { Module } from '@nestjs/common';
import { SchoolTypeService } from './school-type.service';
import { SchoolTypeController } from './school-type.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SchoolType } from './entities/school-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SchoolType])],
  controllers: [SchoolTypeController],
  providers: [SchoolTypeService],
  exports: [SchoolTypeService],
})
export class SchoolTypeModule {
  static readonly route: RouteTree = {
    path: 'school-type',
    module: SchoolTypeModule,
    children: [],
  };
}
