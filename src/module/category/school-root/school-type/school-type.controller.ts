import { Controller, Get, Param } from '@nestjs/common';
import { SchoolTypeService } from './school-type.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathDto } from 'src/common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.SCHOOL_TYPE)
export class SchoolTypeController {
  constructor(private readonly schoolTypeService: SchoolTypeService) {}

  @Get()
  findAll() {
    return this.schoolTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.schoolTypeService.findOne(path);
  }
}
