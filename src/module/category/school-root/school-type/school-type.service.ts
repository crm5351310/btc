import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SchoolType } from './entities/school-type.entity';
import { Repository } from 'typeorm';
import { BaseService, PathDto } from '../../../../common/base/class/base.class';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { BaseMessage } from '../../../../common/message/base.message';
import { Pagination } from '../../../../common/utils/pagination.util';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';

@Injectable()
export class SchoolTypeService extends BaseService {
  constructor(
    @InjectRepository(SchoolType)
    private readonly schoolTypeRepository: Repository<SchoolType>,
  ) {
    super();
    this.name = SchoolTypeService.name;
    this.subject = SubjectMessage.SCHOOL_TYPE;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.schoolTypeRepository.findAndCount();
    return this.response(new Pagination<SchoolType>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.schoolTypeRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.SCHOOL_TYPE, true);
    return this.response(result);
  }
}
