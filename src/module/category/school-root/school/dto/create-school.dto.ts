import { OmitType } from '@nestjs/swagger';
import { School } from '../entities/school.entity';

export class CreateSchoolDto extends OmitType(School, [
  'id',
  'createdAt',
  'updatedAt',
  'createdBy',
  'updatedBy',
] as const) {}
