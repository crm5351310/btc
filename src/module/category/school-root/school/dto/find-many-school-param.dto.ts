import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class FindManySchoolParamDto {
  @ApiProperty({
    name: 'schoolTypeId',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  schoolTypeId: number;
}
