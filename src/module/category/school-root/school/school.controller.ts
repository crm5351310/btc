import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { FindManySchoolParamDto } from './dto/find-many-school-param.dto';
import { CreateSchoolDto } from './dto/create-school.dto';
import { SchoolService } from './school.service';
import { UpdateSchoolDto } from './dto/update-school.dto';

@Controller()
@ApiTags(TagEnum.SCHOOL)
export class SchoolController {
  constructor(private readonly schoolService: SchoolService) {}

  @Post()
  create(@Body() createSchoolDto: CreateSchoolDto) {
    return this.schoolService.create(createSchoolDto);
  }

  @Get()
  findAll(@Query() param: FindManySchoolParamDto) {
    return this.schoolService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.schoolService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateSchoolDto: UpdateSchoolDto) {
    return this.schoolService.update(path, updateSchoolDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.schoolService.remove(path);
  }
}
