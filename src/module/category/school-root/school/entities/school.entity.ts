import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { AdministrativeUnit } from '../../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne } from 'typeorm';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Major } from '../../major/entities/major.entity';
import { SchoolType } from '../../school-type/entities/school-type.entity';

@Entity()
export class School extends BaseEntity {
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  description: string;

  @ApiProperty({
    description: 'ID loại trường học',
    type: RelationTypeBase,
  })
  @ValidateNested()
  @Type(() => PickType(SchoolType, ['id']))
  @ManyToOne(() => SchoolType, (type) => type.schools)
  @JoinColumn()
  schoolType: SchoolType;

  @ApiProperty({
    description: 'ID ngành học',
    type: [RelationTypeBase],
  })
  @ValidateNested()
  @Type(() => PickType(Major, ['id']))
  @ManyToMany(() => Major, (major) => major.schools)
  @JoinTable()
  majors: Major[];

  @ApiProperty({
    description: 'ID xã (nếu tạo mới trường thuộc tỉnh hậu giang)',
    type: RelationTypeBase,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  @ManyToOne(() => AdministrativeUnit, (unit) => unit.schools)
  @JoinColumn()
  administrativeUnit: AdministrativeUnit | null;

  @ApiProperty({
    description: 'Địa chỉ (nếu trường học không thuộc tỉnh hậu giang)',
    default: null,
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  address: string | null;
}
