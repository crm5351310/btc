import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnitModule } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.module';
import { MajorModule } from '../major/major.module';
import { SchoolTypeModule } from '../school-type/school-type.module';
import { School } from './entities/school.entity';
import { SchoolController } from './school.controller';
import { SchoolService } from './school.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([School]),
    MajorModule,
    AdministrativeUnitModule,
    SchoolTypeModule,
  ],
  controllers: [SchoolController],
  providers: [SchoolService],
  exports: [SchoolService],
})
export class SchoolModule {
  static readonly route: RouteTree = {
    path: 'school',
    module: SchoolModule,
    children: [],
  };
}
