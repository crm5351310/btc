import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { ResponseCreated } from '../../../../common/response';
import { SchoolType } from '../school-type/entities/school-type.entity';
import { SchoolTypeService } from '../school-type/school-type.service';
import { CreateSchoolDto } from './dto/create-school.dto';
import { School } from './entities/school.entity';
import { SchoolService } from './school.service';
import { PathDto } from 'src/common/base/class/base.class';
import { MajorService } from '../major/major.service';
import { AdministrativeUnitService } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { Major } from '../major/entities/major.entity';
import { AdministrativeUnit } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';

// npm run test:run src/module/category/school-root/school
describe('SchoolService', () => {
  let service: SchoolService;
  let schoolTypeService: SchoolTypeService;
  let administrativeUnitService: AdministrativeUnitService;
  let majorService: MajorService;
  let repository: Repository<School>;
  let schoolTypeRepository: Repository<SchoolType>;
  let administrativeUnitRepository: Repository<AdministrativeUnit>;
  let majorRepository: Repository<Major>;

  const schools = [
    {
      id: 1,
      code: 'MA 1',
      name: 'Trường học thuộc tỉnh',
      administrativeUnit: {
        id: 1,
        name: 'Xã 1',
      },
      description: 'Mô tả',
    },
    {
      id: 2,
      code: 'MA 2',
      name: 'Trường học không thuộc tỉnh',
      administrativeUnit: null,
      address: 'Hà Nội',
      description: 'Mô tả',
    },
  ] as School[];

  const schoolType = [
    {
      id: 1,
      code: 'MA 1',
      name: 'Trường học thuộc tỉnh',
      description: 'Mô tả',
    },
  ] as SchoolType[];
  
  const administrativeUnit = [
    {
      id: 1,
      code: 'MA 1',
      name: 'Trường học thuộc tỉnh',
    },
  ] as AdministrativeUnit[];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SchoolService,
        SchoolTypeService,
        MajorService,
        AdministrativeUnitService,
        {
          provide: getRepositoryToken(School),
          useValue: createStubInstance(Repository),
        },
        {
          provide: getRepositoryToken(SchoolType),
          useValue: createStubInstance(Repository),
        },
        {
          provide: getRepositoryToken(Major),
          useValue: createStubInstance(Repository),
        },
        {
          provide: getRepositoryToken(AdministrativeUnit),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<SchoolService>(SchoolService);
    repository = module.get(getRepositoryToken(School));
    majorService = module.get<MajorService>(MajorService);
    majorRepository = module.get(getRepositoryToken(Major));
    schoolTypeService = module.get<SchoolTypeService>(SchoolTypeService);
    schoolTypeRepository = module.get(getRepositoryToken(SchoolType));
    administrativeUnitService = module.get<AdministrativeUnitService>(AdministrativeUnitService);
    administrativeUnitRepository = module.get(getRepositoryToken(AdministrativeUnit));


    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        schools.find(
          (value) => value.id === (options.where as FindOptionsWhere<School>).id,
        ) ?? null
      );
    });

    jest.spyOn(schoolTypeRepository, 'findOne').mockImplementation(async (options) => {
      return (
        schoolType.find(
          (value) => value.id === (options.where as FindOptionsWhere<SchoolType>).id,
        ) ?? null
      );
    });
    jest.spyOn(administrativeUnitRepository, 'findOne').mockImplementation(async (options) => {
      return (
        administrativeUnit.find(
          (value) => value.id === (options.where as FindOptionsWhere<AdministrativeUnit>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return schools;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [schools, schools.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<School>) => {
        return schools.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<School>).code ||
            e.name == (options.where as FindOptionsWhere<School>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: School) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: schools.length,
        };
      }
    });
  });



  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('School => create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        schoolType:{
          id: 1
        },
        administrativeUnit: {
          id: 1,
        },
      } as CreateSchoolDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('School => findOne', () => {
    it('Find success', async () => {
      const result = await service.findOne({ id: 1 } as PathDto);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(schools[0]);
    });

    it('Find failed', async () => {
      await expect(service.findOne({ id: 3 })).rejects.toThrowError();
    });
  });
  describe('School => findAll', () => {
    it('Find all success', async () => {
      const result = await service.findAll({ schoolTypeId: 1 });

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(schools);
    });
  });

  describe('School => update', () => {
    it('Update success', async () => {
      const result = await service.update({ id: 1 }, schools[0]);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(schools[0]);
    });

    it('Update failed', async () => {
      const dataFailed = JSON.parse(JSON.stringify(schools));
      await expect(service.update({ id: 3 }, dataFailed[0])).rejects.toThrowError();
    });
  });


});
