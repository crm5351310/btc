import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Pagination } from '../../../../common/utils/pagination.util';
import { AdministrativeUnitService } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { MajorService } from '../major/major.service';
import { SchoolTypeService } from '../school-type/school-type.service';
import { CreateSchoolDto } from './dto/create-school.dto';
import { UpdateSchoolDto } from './dto/update-school.dto';
import { School } from './entities/school.entity';
import { FindManySchoolParamDto } from './dto/find-many-school-param.dto';

@Injectable()
export class SchoolService extends BaseService {
  constructor(
    @InjectRepository(School)
    private readonly SchoolRepository: Repository<School>,
    private readonly schoolTypeService: SchoolTypeService,
    private readonly majorService: MajorService,
    private readonly administrativeUniteService: AdministrativeUnitService,
  ) {
    super();
    this.name = SchoolService.name;
    this.subject = SubjectMessage.SCHOOL;
  }

  async create(createSchoolDto: CreateSchoolDto) {
    this.action = BaseMessage.CREATE;

    // kiểm tra người dùng chỉ truyền lên 1 trong 2 biến address hoặc administrativeUnit
    const { address, administrativeUnit } = createSchoolDto;
    if (address && administrativeUnit)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.DTO, false);
    const checkExistCode = await this.SchoolRepository.findOne({
      where: {
        code: createSchoolDto.code,
      },
    });
    if (checkExistCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);

    const checkExistName = await this.SchoolRepository.findOne({
      where: {
        name: createSchoolDto.name,
      },
    });
    if (checkExistName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    await this.schoolTypeService.findOne(createSchoolDto.schoolType);

    await this.majorService.findAllOrFailByIds(createSchoolDto.majors.map((major) => major.id));

    if (createSchoolDto.administrativeUnit)
      await this.administrativeUniteService.findOne({
        id: createSchoolDto.administrativeUnit.id,
      });

    const result = await this.SchoolRepository.save(createSchoolDto);

    return this.response(result);
  }

  async findAll(param: FindManySchoolParamDto) {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.SchoolRepository.findAndCount({
      where: {
        schoolType: {
          id: param.schoolTypeId,
        },
      },
      relations: {
        majors: true,
        schoolType: true,
        administrativeUnit: true,
      },
    });
    return this.response(new Pagination<School>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.SchoolRepository.findOne({
      where: {
        id: path.id,
      },
      relations: {
        majors: true,
        schoolType: true,
        administrativeUnit: {
          parent: true,
        },
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.SCHOOL, true);
    return this.response(result);
  }

  async update(path: PathDto, updateSchoolDto: UpdateSchoolDto) {
    this.action = BaseMessage.UPDATE;

    // kiểm tra người dùng chỉ truyền lên 1 trong 2 biến address hoặc administrativeUnit
    const { address, administrativeUnit } = updateSchoolDto;
    if (address && administrativeUnit)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.DTO, false);
    if (address) updateSchoolDto.administrativeUnit = null;
    if (administrativeUnit) updateSchoolDto.address = null;

    const checkExistSchool = await this.SchoolRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistSchool)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.SCHOOL, true);
    if (updateSchoolDto.code) {
      const checkExistCode = await this.SchoolRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateSchoolDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateSchoolDto.name) {
      const checkExistName = await this.SchoolRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateSchoolDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    if (updateSchoolDto.schoolType)
      await this.schoolTypeService.findOne(updateSchoolDto.schoolType);

    if (updateSchoolDto.majors)
      await this.majorService.findAllOrFailByIds(updateSchoolDto.majors.map((major) => major.id));

    if (updateSchoolDto.administrativeUnit)
      await this.administrativeUniteService.findOne(updateSchoolDto.administrativeUnit);

    const data = await this.SchoolRepository.save({
      id: path.id,
      ...updateSchoolDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistSchool = await this.SchoolRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistSchool.length === 0)
      throw new CustomBadRequestException(ContentMessage.FAILURE, FieldMessage.SCHOOL, true);
    await this.SchoolRepository.softRemove(checkExistSchool);

    return this.response({
      affected: pathArray.length,
    });
  }
}
