import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { SchoolTypeModule } from './school-type/school-type.module';
import { MajorModule } from './major/major.module';
import { SchoolModule } from './school/school.module';

@Module({
  imports: [SchoolModule, SchoolTypeModule, MajorModule],
})
export class SchoolRootModule {
  static readonly route: RouteTree = {
    path: 'school-root',
    module: SchoolRootModule,
    children: [SchoolModule.route, SchoolTypeModule.route, MajorModule.route],
  };
}
