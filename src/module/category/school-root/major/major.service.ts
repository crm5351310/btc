import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Pagination } from '../../../../common/utils/pagination.util';
import { CreateMajorDto } from './dto/create-major.dto';
import { UpdateMajorDto } from './dto/update-major.dto';
import { Major } from './entities/major.entity';
import { FindManyMajorParamDto } from './dto/find-many-major-param.dto';

@Injectable()
export class MajorService extends BaseService {
  constructor(
    @InjectRepository(Major)
    private readonly majorRepository: Repository<Major>,
  ) {
    super();
    this.name = MajorService.name;
    this.subject = SubjectMessage.ETHNICITY;
  }

  async create(createMajorDto: CreateMajorDto) {
    this.action = BaseMessage.CREATE;
    const checkExistCode = await this.majorRepository.findOne({
      where: {
        code: createMajorDto.code,
      },
    });
    if (checkExistCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    const checkExistName = await this.majorRepository.findOne({
      where: {
        name: createMajorDto.name,
      },
    });
    if (checkExistName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    const result = await this.majorRepository.save(createMajorDto);
    return this.response(result);
  }

  async findAll(param: FindManyMajorParamDto) {
    this.action = BaseMessage.READ;

    const [result, total] = await this.majorRepository.findAndCount({
      where: {
        schools: {
          id: param.schoolId,
        },
      },
    });
    return this.response(new Pagination<Major>(result, total));
  }

  async findAllOrFailByIds(ids: number[]) {
    const result = await this.majorRepository.find({
      where: {
        id: In(ids),
      },
    });
    if (result.length !== ids.length)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.MAJOR, true);
    return result;
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.majorRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.MAJOR, true);
    return this.response(result);
  }

  async update(path: PathDto, updateMajorDto: UpdateMajorDto) {
    this.action = BaseMessage.UPDATE;
    const checkExistMajor = await this.majorRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistMajor)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.MAJOR, true);
    if (updateMajorDto.code) {
      const checkExistCode = await this.majorRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateMajorDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateMajorDto.name) {
      const checkExistName = await this.majorRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateMajorDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const data = await this.majorRepository.save({
      id: path.id,
      ...updateMajorDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistMajor = await this.majorRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistMajor.length === 0)
      throw new CustomBadRequestException(ContentMessage.FAILURE, FieldMessage.MAJOR, true);
    await this.majorRepository.softRemove(checkExistMajor);
    return this.response({
      affected: pathArray.length,
    });
  }
}
