import { OmitType } from '@nestjs/swagger';
import { Major } from '../entities/major.entity';

export class CreateMajorDto extends OmitType(Major, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
  'deletedAt',
]) {}
