import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class FindManyMajorParamDto {
  @ApiProperty({
    name: 'schoolId',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  schoolId: number;
}
