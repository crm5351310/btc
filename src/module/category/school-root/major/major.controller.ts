import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { CreateMajorDto } from './dto/create-major.dto';
import { UpdateMajorDto } from './dto/update-major.dto';
import { MajorService } from './major.service';
import { FindManyMajorParamDto } from './dto/find-many-major-param.dto';

@Controller()
@ApiTags(TagEnum.MAJOR)
export class MajorController {
  constructor(private readonly majorService: MajorService) {}

  @Post()
  create(@Body() createMajorDto: CreateMajorDto) {
    return this.majorService.create(createMajorDto);
  }

  @Get()
  findAll(@Query() param: FindManyMajorParamDto) {
    return this.majorService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.majorService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateMajorDto: UpdateMajorDto) {
    return this.majorService.update(path, updateMajorDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.majorService.remove(path);
  }
}
