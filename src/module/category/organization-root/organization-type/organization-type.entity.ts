import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { OrganizationGroup } from '../organization-group/organization-group.entity';
import { Organization } from '../organization/organization.entity';

@Entity()
export class OrganizationType extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  // entity
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  // entity
  @Column('varchar', {
    length: 25,
    nullable: false,
  })
  code: string;

  @ManyToOne(() => OrganizationGroup, (item) => item.organizationTypes)
  @JoinColumn()
  organizationGroup: OrganizationGroup;

  @OneToMany(() => Organization, (item) => item.organizationType)
  organizations: Organization[];
}
