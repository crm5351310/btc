import { Module } from '@nestjs/common';
import { OrganizationTypeController } from './organization-type.controller';
import { OrganizationTypeService } from './organization-type.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganizationType } from './organization-type.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([OrganizationType])],
  controllers: [OrganizationTypeController],
  providers: [OrganizationTypeService],
})
export class OrganizationTypeModule {}
export const organizationTypeRouter: RouteTree = {
  path: 'organization-type',
  module: OrganizationTypeModule,
  children: [],
};
