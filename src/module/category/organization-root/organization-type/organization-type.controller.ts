import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { OrganizationTypeService } from './organization-type.service';
import { PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.ORGANIZATION_TYPE)
export class OrganizationTypeController {
  constructor(private readonly organizationTypeService: OrganizationTypeService) {}

  @Get()
  findAll(@Query() param: PathDto) {
    return this.organizationTypeService.findAll(param);
  }
}
