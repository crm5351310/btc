import { Injectable } from '@nestjs/common';
import { BaseService, PathDto } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { BaseMessage } from '../../../../common/message/base.message';
import { OrganizationType } from './organization-type.entity';

@Injectable()
export class OrganizationTypeService extends BaseService {
  constructor(
    @InjectRepository(OrganizationType)
    private readonly organizationTypeRepository: Repository<OrganizationType>,
  ) {
    super();
    this.name = OrganizationTypeService.name;
    this.subject = SubjectMessage.ORGANIZATION_TYPE;
  }
  async findAll(param: PathDto) {
    this.action = BaseMessage.READ;
    return this.response(
      await this.organizationTypeRepository.find({
        where: {
          organizationGroup: {
            id: param.id,
          },
        },
      }),
    );
  }
}
