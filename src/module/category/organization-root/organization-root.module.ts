import { Module } from '@nestjs/common';
import {
  OrganizationGroupModule,
  organizationGroupRouter,
} from './organization-group/organization-group.module';
import {
  OrganizationTypeModule,
  organizationTypeRouter,
} from './organization-type/organization-type.module';
import { OrganizationModule, organizationRouter } from './organization/organization.module';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [OrganizationGroupModule, OrganizationTypeModule, OrganizationModule],
})
export class OrganizationRootModule {}
export const organizationRootRouter: RouteTree = {
  path: 'organization-root',
  module: OrganizationRootModule,
  children: [organizationRouter, organizationGroupRouter, organizationTypeRouter],
};
