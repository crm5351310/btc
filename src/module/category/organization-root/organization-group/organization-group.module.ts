import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganizationGroup } from './organization-group.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { OrganizationGroupController } from './organization-group.controller';
import { OrganizationGroupService } from './organization-group.service';

@Module({
  imports: [TypeOrmModule.forFeature([OrganizationGroup])],
  controllers: [OrganizationGroupController],
  providers: [OrganizationGroupService],
})
export class OrganizationGroupModule {}
export const organizationGroupRouter: RouteTree = {
  path: 'organization-group',
  module: OrganizationGroupModule,
  children: [],
};
