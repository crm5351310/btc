import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { OrganizationGroupService } from './organization-group.service';

@Controller()
@ApiTags(TagEnum.ORGANIZATION_GROUP)
export class OrganizationGroupController {
  constructor(private readonly organizationGroupService: OrganizationGroupService) {}

  @Get()
  findAll() {
    return this.organizationGroupService.findAll();
  }
}
