import { Injectable } from '@nestjs/common';
import { BaseService } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { OrganizationGroup } from './organization-group.entity';
import { BaseMessage } from '../../../../common/message/base.message';

@Injectable()
export class OrganizationGroupService extends BaseService {
  constructor(
    @InjectRepository(OrganizationGroup)
    private readonly organizationGroupepository: Repository<OrganizationGroup>,
  ) {
    super();
    this.name = OrganizationGroupService.name;
    this.subject = SubjectMessage.ORGANIZATION_GROUP;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    return this.response(await this.organizationGroupepository.find());
  }
}
