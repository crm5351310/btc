import { Module } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { OrganizationController } from './organization.controller';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organization } from './organization.entity';
import { OrganizationType } from '../organization-type/organization-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Organization, OrganizationType])],
  controllers: [OrganizationController],
  providers: [OrganizationService],
  exports: [OrganizationService],
})
export class OrganizationModule {}
export const organizationRouter: RouteTree = {
  path: 'organization',
  module: OrganizationModule,
  children: [],
};
