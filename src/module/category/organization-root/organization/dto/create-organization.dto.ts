import { OmitType } from '@nestjs/swagger';
import { Organization } from '../organization.entity';

export class CreateOrganizationDto extends OmitType(Organization, ['id'] as const) {}
