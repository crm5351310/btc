import { Injectable } from '@nestjs/common';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Organization } from './organization.entity';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import { BaseMessage } from '../../../../common/message/base.message';
import { OrganizationType } from '../organization-type/organization-type.entity';
import { FieldMessage } from '../../../../common/message/field.message';

@Injectable()
export class OrganizationService extends BaseService {
  constructor(
    @InjectRepository(Organization)
    private readonly organizationRepository: Repository<Organization>,

    @InjectRepository(OrganizationType)
    private readonly organizationTypeRepository: Repository<OrganizationType>,
  ) {
    super();
    this.name = OrganizationService.name;
    this.subject = SubjectMessage.ORGANIZATION;
  }
  async create(createOrganizationDto: CreateOrganizationDto) {
    this.action = BaseMessage.CREATE;
    const organizationType = await this.organizationTypeRepository.findOne({
      where: {
        id: createOrganizationDto.organizationType.id,
      },
    });
    if (!organizationType) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ORGANIZATION_TYPE,
        true,
      );
    }
    return this.response(await this.organizationRepository.save(createOrganizationDto));
  }

  async findAll() {
    this.action = BaseMessage.READ;
    const data = await this.organizationRepository.findAndCount({
      relations: {
        organizationType: {
          organizationGroup: true,
        },
        administrativeUnit: {
          parent: {
            parent: true,
          },
        },
      },
    });
    return this.response({
      result: data[0],
      total: data[1],
    });
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const organization = await this.organizationRepository.findOne({
      relations: {
        organizationType: {
          organizationGroup: true,
        },
        administrativeUnit: {
          parent: {
            parent: true,
          },
        },
      },
      where: path,
    });
    if (!organization) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }

    if (!organization.administrativeUnit.parent) {
      organization['administrativeLevel'] = {
        id: 1,
      };
    }
    if (organization.administrativeUnit.parent && !organization.administrativeUnit.parent.parent) {
      organization['administrativeLevel'] = {
        id: 2,
      };
    }
    if (organization.administrativeUnit.parent && organization.administrativeUnit.parent.parent) {
      organization['administrativeLevel'] = {
        id: 3,
      };
    }
    return this.response(organization);
  }

  async update(path: PathDto, updateOrganizationDto: UpdateOrganizationDto) {
    this.action = BaseMessage.UPDATE;

    const organization = await this.organizationRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!organization) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }

    const organizationType = await this.organizationTypeRepository.findOne({
      where: {
        id: updateOrganizationDto.organizationType.id,
      },
    });
    if (!organizationType) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ORGANIZATION_TYPE,
        false,
      );
    }

    return this.response(
      await this.organizationRepository.save({
        ...updateOrganizationDto,
        id: path.id,
      }),
    );
  }

  async remove(ids: number[]) {
    this.action = BaseMessage.DELETE;
    const organization = await this.organizationRepository.find({
      where: {
        id: In(ids),
      },
    });

    if (!organization.length) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, this.subject, true);
    }
    await this.organizationRepository.softRemove(organization);

    // Logic for throwing error delete in relation here

    //

    return this.response({
      affected: ids.length,
    });
  }
}
