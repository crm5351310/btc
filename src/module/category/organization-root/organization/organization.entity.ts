import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength, ValidateNested } from 'class-validator';
import { OrganizationType } from '../organization-type/organization-type.entity';
import { RelationTypeBase } from '../../../../common/base/class/base.class';
import { Type } from 'class-transformer';
import { AdministrativeUnit } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { DefenseUnit } from '../../militia-defense-unit/defense-unit/entities/defense-unit.entity';

@Entity()
export class Organization extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  // entity
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  // entity
  @Column('varchar', {
    length: 25,
    nullable: false,
  })
  code: string;

  // swagger
  @ApiProperty({
    description: 'Loại tổ chức',
    type: RelationTypeBase,
  })
  // validate
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(OrganizationType, ['id']))
  @ManyToOne(() => OrganizationType, (item) => item.organizations)
  @JoinColumn()
  organizationType: OrganizationType;

  // swagger
  @ApiProperty({
    description: 'Là tổ chức đảng',
    default: false,
    type: 'boolean',
  })
  // validate
  @IsNotEmpty()
  // entity
  @Column('boolean', {
    nullable: false,
  })
  isPartyOrganization: boolean;

  @ApiProperty({
    description: 'Đủ điều kiện thành lập đội tự vệ',
    default: false,
    type: 'boolean',
  })
  // validate
  @IsNotEmpty()
  // entity
  @Column('boolean', {
    nullable: false,
  })
  isSelfDefenseTeam: boolean;

  @ApiProperty({
    description: 'Địa phận hành chính',
    type: RelationTypeBase,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  @ManyToOne(() => AdministrativeUnit, (item) => item.organizations)
  @JoinColumn()
  administrativeUnit: AdministrativeUnit;

  @OneToMany(() => DefenseUnit, (defenseUnit) => defenseUnit.organization)
  defenseUnits: DefenseUnit[];
}
