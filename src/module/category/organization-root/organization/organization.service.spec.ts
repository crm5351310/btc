// npm run test:run src/module/category/organization-root/organization/organization.service.spec.ts
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { OrganizationService } from './organization.service';
import { OrganizationType } from '../organization-type/organization-type.entity';
import { Organization } from './organization.entity';
import { createStubInstance } from 'sinon';
import { OrganizationTypeService } from '../organization-type/organization-type.service';
import { PathDto } from '../../../../common/base/class/base.class';

describe('OrganizationService', () => {
  const payload = [
    {
      id: 1,
      name: 'ten',
      code: 'ten',
      organizationType: {
        id: 1,
      },
      isSelfDefenseTeam: true,
      isPartyOrganization: true,
      administrativeUnit: {
        id: 1,
      },
    },
  ] as Organization[];

  const payloadType = [
    {
      id: 1,
      name: 'ten',
      code: 'ten',
      organizationGroup: {
        id: 1,
      },
    },
  ] as OrganizationType[];

  let organizationService: OrganizationService;
  let organizationTypeService: OrganizationTypeService;
  let organizationRepository: Repository<Organization>;
  let organizationTypeRepository: Repository<OrganizationType>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrganizationService,
        OrganizationTypeService,
        {
          provide: getRepositoryToken(Organization),
          useValue: createStubInstance(Repository),
        },
        {
          provide: getRepositoryToken(OrganizationType),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    organizationService = module.get<OrganizationService>(OrganizationService);
    organizationTypeService = module.get<OrganizationTypeService>(OrganizationTypeService);
    organizationRepository = module.get(getRepositoryToken(Organization));
    organizationTypeRepository = module.get(getRepositoryToken(OrganizationType));

    jest.spyOn(organizationRepository, 'findOne').mockImplementation(async (options) => {
      return (
        payload.find(
          (value) => value.id === (options.where as FindOptionsWhere<Organization>).id,
        ) ?? null
      );
    });

    jest.spyOn(organizationTypeRepository, 'findOne').mockImplementation(async (options) => {
      return (
        payloadType.find(
          (value) => value.id === (options.where as FindOptionsWhere<OrganizationType>).id,
        ) ?? null
      );
    });

    jest.spyOn(organizationRepository, 'find').mockImplementation(async () => {
      return payload;
    });

    jest.spyOn(organizationRepository, 'findAndCount').mockImplementation(async () => {
      return [payload, payload.length];
    });

    jest
      .spyOn(organizationRepository, 'exist')
      .mockImplementation(async (options: FindManyOptions<Organization>) => {
        return payload.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<Organization>).code ||
            e.name == (options.where as FindOptionsWhere<Organization>).name,
        );
      });

    jest.spyOn(organizationRepository, 'save').mockImplementation(async (entity: Organization) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: payload.length,
        };
      }
    });
  });

  describe('Organization => create', () => {
    it('Create success', async () => {
      const result = await organizationService.create(payload[0]);

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Create failed', async () => {
      const dataFailed = JSON.parse(JSON.stringify(payload));
      delete dataFailed[0].organizationType;
      await expect(organizationService.create(dataFailed[0])).rejects.toThrowError();
    });
  });

  describe('Organization => findOne', () => {
    it('Find success', async () => {
      const result = await organizationService.findOne({ id: 1 } as PathDto);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Find failed', async () => {
      await expect(organizationService.findOne({ id: 2 })).rejects.toThrowError();
    });
  });

  describe('Organization => update', () => {
    it('Update success', async () => {
      const result = await organizationService.update({ id: 1 }, payload[0]);
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload[0]);
    });

    it('Update failed', async () => {
      const dataFailed = JSON.parse(JSON.stringify(payload));
      delete dataFailed[0].organizationType;
      await expect(organizationService.update({ id: 1 }, dataFailed[0])).rejects.toThrowError();
    });
  });

  describe('OrganizationService => findAll', () => {
    it('Find all success', async () => {
      const result = await organizationService.findAll();

      expect(result.statusCode).toBe(200);
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('OrganizationService => remove', () => {
    it('Remove success', async () => {
      const result = await organizationService.remove([1, 2, 3]);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Remove failed', async () => {
      // logic for throwing error in here
      //
    });
  });
});
