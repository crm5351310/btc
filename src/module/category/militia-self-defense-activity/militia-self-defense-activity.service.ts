import { Injectable } from '@nestjs/common';
import { CreateMilitiaSelfDefenseActivityDto } from './dto/create-militia-self-defense-activity.dto';
import { UpdateMilitiaSelfDefenseActivityDto } from './dto/update-militia-self-defense-activity.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Religion } from '../religion/religion.entity';
import { In, Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../common/message/subject.message';
import { MilitiaSelfDefenseActivity } from './militia-self-defense-activity.entity';
import { BaseMessage } from '../../../common/message/base.message';
import { FieldMessage } from '../../../common/message/field.message';
import { BaseInterfaceService } from '../../../common/interface/base.interface';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { Pagination } from '../../../common/utils/pagination.util';
import { ParamMilitiaSelfDefenseActivityDto } from './dto/param-militia-self-defense-activity.dto';

@Injectable()
export class MilitiaSelfDefenseActivityService extends BaseService {
  constructor(
    @InjectRepository(MilitiaSelfDefenseActivity)
    private readonly militiaSelfDenfenseActivityRepository: Repository<MilitiaSelfDefenseActivity>,
  ) {
    super();
    this.name = MilitiaSelfDefenseActivityService.name;
    this.subject = SubjectMessage.MILITIA_SELF_DEFENSE_ACTIVITY;
  }
  async create(createMilitiaSelfDefenseActivityDto: CreateMilitiaSelfDefenseActivityDto) {
    this.action = BaseMessage.CREATE;
    await this.dubField(FieldMessage.NAME, createMilitiaSelfDefenseActivityDto.name);
    await this.dubField(FieldMessage.CODE, createMilitiaSelfDefenseActivityDto.code);

    return this.response(
      await this.militiaSelfDenfenseActivityRepository.save(createMilitiaSelfDefenseActivityDto),
    );
  }

  async findAll(param: ParamMilitiaSelfDefenseActivityDto) {
    this.action = BaseMessage.READ;
    const militiaSelfDefenseActivity =
      await this.militiaSelfDenfenseActivityRepository.findAndCount({
        skip: param.limit && param.offset,
        take: param.limit,
        order: {
          code: 'ASC',
        },
      });
    return this.response(
      new Pagination<MilitiaSelfDefenseActivity>(
        militiaSelfDefenseActivity[0],
        militiaSelfDefenseActivity[1],
      ),
    );
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.militiaSelfDenfenseActivityRepository.findOne({
      where: path,
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    return this.response(result);
  }

  async update(
    path: PathDto,
    updateMilitiaSelfDefenseActivityDto: UpdateMilitiaSelfDefenseActivityDto,
  ) {
    this.action = BaseMessage.UPDATE;
    const militiaSelfDefenseActivity = await this.militiaSelfDenfenseActivityRepository.findOne({
      where: path,
    });
    if (!militiaSelfDefenseActivity) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    const nameDublicate = await this.militiaSelfDenfenseActivityRepository.findOne({
      where: {
        name: updateMilitiaSelfDefenseActivityDto.name,
        id: Not(path.id),
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.militiaSelfDenfenseActivityRepository.findOne({
      where: {
        code: updateMilitiaSelfDefenseActivityDto.code,
        id: Not(path.id),
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    return this.response(
      await this.militiaSelfDenfenseActivityRepository.save({
        ...updateMilitiaSelfDefenseActivityDto,
        id: path.id,
      }),
    );
  }

  async remove(path: PathArrayDto) {
    const partArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const militiaSelfDefenseActivity = await this.militiaSelfDenfenseActivityRepository.find({
      where: {
        id: In(partArr),
      },
    });
    if (militiaSelfDefenseActivity.length != partArr.length) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    await this.militiaSelfDenfenseActivityRepository.softRemove(militiaSelfDefenseActivity);
    return this.response(path);
  }

  async dubField(
    key: string,
    value: string | number,
    condition?: BaseInterfaceService,
  ): Promise<void> {
    const dub = this.militiaSelfDenfenseActivityRepository
      .createQueryBuilder('A')
      .where(`A.${key} = :value`, { value });

    if (condition) {
      let subQuery = `A.id = ${value}`;
      if (condition.isProp) {
        subQuery = subQuery + ` AND A.${condition.property} ${condition.condition} `;
      }
      dub.where(subQuery);
    }

    const result = await dub.getOne();
    if (key == 'id' && !result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    if (key != 'id' && result) {
      throw new CustomBadRequestException(
        ContentMessage.EXIST,
        FieldMessage[key.toUpperCase()],
        false,
      );
    }
  }
}
