import { Module } from '@nestjs/common';
import { MilitiaSelfDefenseActivityService } from './militia-self-defense-activity.service';
import { MilitiaSelfDefenseActivityController } from './militia-self-defense-activity.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitiaSelfDefenseActivity } from './militia-self-defense-activity.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([MilitiaSelfDefenseActivity])],
  controllers: [MilitiaSelfDefenseActivityController],
  providers: [MilitiaSelfDefenseActivityService],
})
export class MilitiaSelfDefenseActivityModule {}
export const militiaSelfDefenseActivityRouter: RouteTree = {
  path: 'militia-and-self-defense-activity',
  module: MilitiaSelfDefenseActivityModule,
  children: [],
};
