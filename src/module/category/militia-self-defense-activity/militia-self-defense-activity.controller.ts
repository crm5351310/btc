import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { MilitiaSelfDefenseActivityService } from './militia-self-defense-activity.service';
import { CreateMilitiaSelfDefenseActivityDto } from './dto/create-militia-self-defense-activity.dto';
import { UpdateMilitiaSelfDefenseActivityDto } from './dto/update-militia-self-defense-activity.dto';
import { PathArrayDto, PathDto } from '../../../common/base/class/base.class';
import { ParamMilitiaSelfDefenseActivityDto } from './dto/param-militia-self-defense-activity.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.MILITIA_SELF_DEFENSE_ACTIVITY)
export class MilitiaSelfDefenseActivityController {
  constructor(
    private readonly militiaSelfDefenseActivityService: MilitiaSelfDefenseActivityService,
  ) {}

  @Post()
  create(
    @Body()
    createMilitiaSelfDefenseActivityDto: CreateMilitiaSelfDefenseActivityDto,
  ) {
    return this.militiaSelfDefenseActivityService.create(createMilitiaSelfDefenseActivityDto);
  }

  @Get()
  findAll(@Query() param: ParamMilitiaSelfDefenseActivityDto) {
    return this.militiaSelfDefenseActivityService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.militiaSelfDefenseActivityService.findOne(path);
  }

  @Patch(':id')
  update(
    @Param() path: PathDto,
    @Body()
    updateMilitiaSelfDefenseActivityDto: UpdateMilitiaSelfDefenseActivityDto,
  ) {
    return this.militiaSelfDefenseActivityService.update(path, updateMilitiaSelfDefenseActivityDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.militiaSelfDefenseActivityService.remove(path);
  }
}
