import { CreateMilitiaSelfDefenseActivityDto } from './create-militia-self-defense-activity.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateMilitiaSelfDefenseActivityDto extends PartialType(
  CreateMilitiaSelfDefenseActivityDto,
) {}
