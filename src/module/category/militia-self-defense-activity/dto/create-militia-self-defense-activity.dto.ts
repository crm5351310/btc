import { OmitType } from '@nestjs/swagger';
import { MilitiaSelfDefenseActivity } from '../militia-self-defense-activity.entity';

export class CreateMilitiaSelfDefenseActivityDto extends OmitType(MilitiaSelfDefenseActivity, [
  'id',
] as const) {}
