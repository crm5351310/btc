// npm run test:run src/module/category/militia-self-defense-activity
import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import { Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { MilitiaSelfDefenseActivityService } from '../militia-self-defense-activity.service';
import { MilitiaSelfDefenseActivity } from '../militia-self-defense-activity.entity';
import { MilitiaSelfDefenseActivityModule } from '../militia-self-defense-activity.module';

describe('JobTitleService', () => {
  let module: TestingModule;
  let militiaSelfDefenseActivityService: MilitiaSelfDefenseActivityService;
  let militiaSelfDefenseActivityRepository: Repository<MilitiaSelfDefenseActivity>;

  const MILITIA_SELF_DEFENSE_REPOSITORY_TOKEN = getRepositoryToken(MilitiaSelfDefenseActivity);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        MilitiaSelfDefenseActivityModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    militiaSelfDefenseActivityRepository = module.get(MILITIA_SELF_DEFENSE_REPOSITORY_TOKEN);
    militiaSelfDefenseActivityService = module.get<MilitiaSelfDefenseActivityService>(
      MilitiaSelfDefenseActivityService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('MilitiaSelfDefenseActivityService => create', () => {
    const payload = {
      name: 'tenten',
      code: 'te2',
    } as MilitiaSelfDefenseActivity;

    it('Tạo mới hoạt động dân quân tự vệ thành công', async () => {
      jest.spyOn(militiaSelfDefenseActivityRepository, 'findOne').mockResolvedValue(null);
      jest
        .spyOn(militiaSelfDefenseActivityRepository, 'save')
        .mockResolvedValue({ ...payload } as MilitiaSelfDefenseActivity);

      const result = await militiaSelfDefenseActivityService.create(payload);

      expect(militiaSelfDefenseActivityRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('MilitiaSelfDefenseActivityService => findOne', () => {
    it('Tìm một hoạt động dân quân tự vệ thành công', async () => {
      const data = new MilitiaSelfDefenseActivity();
      data.id = 1;
      data.name = 'h';
      data.code = '3';
      jest.spyOn(militiaSelfDefenseActivityRepository, 'findOne').mockResolvedValue(data);

      const result = await militiaSelfDefenseActivityService.findOne({
        id: 1,
      } as PathDto);

      expect(militiaSelfDefenseActivityRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(data);
    });

    it('Không thể tìm một hoạt động dân quân tự vệ nếu ID không tồn tại', async () => {
      jest.spyOn(militiaSelfDefenseActivityRepository, 'findOne').mockResolvedValue(null);
      await expect(militiaSelfDefenseActivityService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('MilitiaSelfDefenseActivityService => findAll', () => {
    it('Tìm danh sách hoạt động dân quân tự vệ thành công', async () => {
      const mockData = [
        { id: 1, name: 'user1' },
        { id: 2, name: 'user2' },
        { id: 3, name: 'user2' },
      ] as MilitiaSelfDefenseActivity[];
      jest
        .spyOn(militiaSelfDefenseActivityRepository, 'findAndCount')
        .mockResolvedValue([mockData, mockData.length]);

      const result = await militiaSelfDefenseActivityService.findAll({} as any);

      expect(militiaSelfDefenseActivityRepository.findAndCount).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('MilitiaSelfDefenseActivityService => remove', () => {
    it('Xoá một hoạt động dân quân tự vệ thành công', async () => {
      const mockData = { id: 1 } as MilitiaSelfDefenseActivity;
      jest.spyOn(militiaSelfDefenseActivityRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(militiaSelfDefenseActivityRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await militiaSelfDefenseActivityService.remove({
        id: '1',
      } as PathArrayDto);

      expect(militiaSelfDefenseActivityRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một hoạt động dân quân tự vệ nếu ID không tồn tại', async () => {
      jest.spyOn(militiaSelfDefenseActivityRepository, 'find').mockResolvedValue([]);
      await expect(militiaSelfDefenseActivityService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
