import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from '../../../common/base/class/base.class';
import { TagEnum } from '../../../common/enum/tag.enum';
import {
  CreateGeneralEducationLevelDto,
  FindManyGeneralEducationLevelParamDto,
  UpdateGeneralEducationLeveDto,
} from './general-education-level.dto';
import { GeneralEducationLevelService } from './general-education-level.service';

@Controller()
@ApiTags(TagEnum.GENERAL_EDUCATION_LEVEL)
export class GeneralEducationLevelController {
  constructor(private readonly generalEducationLevelService: GeneralEducationLevelService) {}

  @Get('')
  async findAll(@Query() query: FindManyGeneralEducationLevelParamDto) {
    const result = await this.generalEducationLevelService.findAll(query);
    return result;
  }

  @Get(':id')
  async findOne(@Param() path: PathDto) {
    const result = await this.generalEducationLevelService.findOne(path.id);
    return result;
  }

  @Post('')
  async create(@Body() payload: CreateGeneralEducationLevelDto) {
    const result = await this.generalEducationLevelService.create(payload);
    return result;
  }

  @Patch(':id')
  async update(@Param() path: PathDto, @Body() payload: UpdateGeneralEducationLeveDto) {
    const result = await this.generalEducationLevelService.update(path.id, payload);
    return result;
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.generalEducationLevelService.remove(ids);
  }
}
