import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { GeneralEducationLevel } from './general-education-level.entity';

export class CreateGeneralEducationLevelDto extends OmitType(GeneralEducationLevel, [
  'id' as const,
]) {}

export class UpdateGeneralEducationLeveDto extends PartialType(CreateGeneralEducationLevelDto) {}

export class FindManyGeneralEducationLevelParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset?: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit?: number;
}
