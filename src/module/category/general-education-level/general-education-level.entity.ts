import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../common/base/entity/base.entity';

@Entity()
export class GeneralEducationLevel extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'code',
    type: 'string',
    default: 'code 1',
    required: true,
  })
  // validate
  @IsNotEmpty()
  // entity
  @Column('varchar', {
    nullable: false,
    length: 25,
  })
  code: string;

  // swagger
  @ApiProperty({
    description: 'name',
    type: 'string',
    default: 'trình độ văn hóa phổ thông 1',
    required: true,
  })
  // validate
  @IsNotEmpty()
  // entity
  @Column('varchar', {
    nullable: false,
    length: 100,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mô tả trình độ văn hóa phổ thông',
    default: 'Mô tả trình độ văn hóa phổ thông',
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;
}
