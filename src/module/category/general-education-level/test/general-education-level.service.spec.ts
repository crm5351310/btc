import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { dataSourceOptions } from '../../../../config/data-source.config';
import {
  CreateGeneralEducationLevelDto,
  FindManyGeneralEducationLevelParamDto,
  UpdateGeneralEducationLeveDto,
} from '../general-education-level.dto';
import { GeneralEducationLevel } from '../general-education-level.entity';
import { GeneralEducationLevelModule } from '../general-education-level.module';
import { GeneralEducationLevelService } from '../general-education-level.service';

//  npm run test:run src/module/category/general-education-level

describe('GeneralEducationLevelService', () => {
  let generalEducationLevelService: GeneralEducationLevelService;
  let module: TestingModule;
  let generalEducationLevelRepository: Repository<GeneralEducationLevel>;

  const GENERAL_EDUCATION_LEVEL_REPOSITORY_TOKEN = getRepositoryToken(GeneralEducationLevel);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        GeneralEducationLevelModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
      ],
    }).compile();

    generalEducationLevelRepository = module.get(GENERAL_EDUCATION_LEVEL_REPOSITORY_TOKEN);
    generalEducationLevelService = module.get<GeneralEducationLevelService>(
      GeneralEducationLevelService,
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('GeneralEducationLevelService => create', () => {
    const payload = {
      code: 'code',
      name: 'tên',
      description: 'Mô tả',
    } as CreateGeneralEducationLevelDto;

    it('Tạo mới trình độ văn hóa thành công', async () => {
      jest
        .spyOn(generalEducationLevelRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as GeneralEducationLevel);

      const result = await generalEducationLevelService.create(payload);

      expect(generalEducationLevelRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới trình độ văn hóa không thành công nếu tên tồn tại', async () => {
      jest.spyOn(generalEducationLevelService, 'create').mockImplementation(async () => {
        throw new CustomBadRequestException('EXIST', 'NAME', false);
      });

      await expect(generalEducationLevelService.create(payload)).rejects.toThrowError();
      expect(generalEducationLevelRepository.save).not.toHaveBeenCalled();
    });

    it('Tạo mới trình độ văn hóa không thành công nếu code tồn tại', async () => {
      jest.spyOn(generalEducationLevelService, 'create').mockImplementation(async () => {
        throw new CustomBadRequestException('EXIST', 'CODE', false);
      });

      await expect(generalEducationLevelService.create(payload)).rejects.toThrowError();
      expect(generalEducationLevelRepository.save).not.toHaveBeenCalled();
    });
  });

  describe('GeneralEducationLevelService => findOne', () => {
    it('Tìm một trình độ văn hóa thành công', async () => {
      const mockData = {
        id: 1,
        code: 'code',
        name: 'name',
      } as GeneralEducationLevel;
      jest.spyOn(generalEducationLevelRepository, 'findOne').mockResolvedValue(mockData);

      const result = await generalEducationLevelService.findOne({
        id: 1,
      } as PathDto);

      expect(generalEducationLevelRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một trình độ văn hóa nếu ID không tồn tại', async () => {
      jest.spyOn(generalEducationLevelRepository, 'findOne').mockResolvedValue(null);
      await expect(
        generalEducationLevelService.findOne({ id: 1 } as PathDto),
      ).rejects.toThrowError();
    });
  });

  describe('GeneralEducationLevelService => findAll', () => {
    it('Tìm danh trình độ văn hóa thành công', async () => {
      const mockData = [
        { id: 1, code: 'code 1', name: 'name 1' },
        { id: 2, code: 'code 2', name: 'name 2' },
        { id: 3, code: 'code 3', name: 'name 3' },
      ] as GeneralEducationLevel[];
      const query: FindManyGeneralEducationLevelParamDto = {
        offset: 0,
        limit: 10,
      };
      jest.spyOn(generalEducationLevelRepository, 'find').mockResolvedValue(mockData);
      jest.spyOn(generalEducationLevelRepository, 'count').mockResolvedValue(3);

      const result = await generalEducationLevelService.findAll(query);

      expect(generalEducationLevelRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('GeneralEducationLevelService => update', () => {
    it('Cập nhật trường code trình độ văn hóa thành công', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        code: 'newCode',
      } as UpdateGeneralEducationLeveDto;

      jest.spyOn(generalEducationLevelRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as GeneralEducationLevel,
      );

      jest
        .spyOn(generalEducationLevelRepository, 'findOne')
        .mockImplementationOnce(async () => null);

      jest
        .spyOn(generalEducationLevelRepository, 'findOne')
        .mockImplementationOnce(async () => null);

      jest.spyOn(generalEducationLevelRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as GeneralEducationLevel);

      const result = await generalEducationLevelService.update(path, updatePayload);

      expect(result.statusCode).toBe(200);
      expect(result.data).toEqual(expect.objectContaining(updatePayload));
      expect(generalEducationLevelRepository.save).toHaveBeenCalledWith({
        id: path.id,
        ...updatePayload,
      });
    });
    it('Cập nhật trường name trình độ văn hóa thành công', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        name: 'newName',
      } as UpdateGeneralEducationLeveDto;

      jest.spyOn(generalEducationLevelRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as GeneralEducationLevel,
      );

      jest
        .spyOn(generalEducationLevelRepository, 'findOne')
        .mockImplementationOnce(async () => null);

      jest
        .spyOn(generalEducationLevelRepository, 'findOne')
        .mockImplementationOnce(async () => null);

      jest.spyOn(generalEducationLevelRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as GeneralEducationLevel);

      const result = await generalEducationLevelService.update(path, updatePayload);

      expect(result.statusCode).toBe(200);
      expect(result.data).toEqual(expect.objectContaining(updatePayload));
      expect(generalEducationLevelRepository.save).toHaveBeenCalledWith({
        id: path.id,
        ...updatePayload,
      });
    });

    it('Cập nhật trình độ văn hóa không thành công vì trùng code', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        code: 'newCode',
      } as UpdateGeneralEducationLeveDto;

      jest.spyOn(generalEducationLevelRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as GeneralEducationLevel,
      );

      jest.spyOn(generalEducationLevelRepository, 'findOne').mockImplementationOnce(async () => {
        throw new CustomBadRequestException('EXIST', 'CODE', false);
      });

      jest
        .spyOn(generalEducationLevelRepository, 'findOne')
        .mockImplementationOnce(async () => null);

      jest.spyOn(generalEducationLevelRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as GeneralEducationLevel);

      await expect(generalEducationLevelService.update(path, updatePayload)).rejects.toThrowError();
      expect(generalEducationLevelRepository.save).not.toHaveBeenCalled();
    });

    it('Cập nhật trình độ văn hóa không thành công vì trùng name', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        name: 'newName',
      } as UpdateGeneralEducationLeveDto;

      jest.spyOn(generalEducationLevelRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as GeneralEducationLevel,
      );

      jest.spyOn(generalEducationLevelRepository, 'findOne').mockImplementationOnce(async () => {
        throw new CustomBadRequestException('EXIST', 'NAME', false);
      });
      jest
        .spyOn(generalEducationLevelRepository, 'findOne')
        .mockImplementationOnce(async () => null);

      jest.spyOn(generalEducationLevelRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as GeneralEducationLevel);

      await expect(generalEducationLevelService.update(path, updatePayload)).rejects.toThrowError();
      expect(generalEducationLevelRepository.save).not.toHaveBeenCalled();
    });
  });

  describe('GeneralEducationLevelService => remove', () => {
    it('Xoá một trình độ văn hóa thành công', async () => {
      const mockData = { id: 1 } as GeneralEducationLevel;
      jest.spyOn(generalEducationLevelRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(generalEducationLevelRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await generalEducationLevelService.remove({
        id: '1',
      } as PathArrayDto);

      expect(generalEducationLevelRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một trình độ văn hóa nếu ID không tồn tại', async () => {
      jest.spyOn(generalEducationLevelRepository, 'find').mockResolvedValue([]);
      await expect(generalEducationLevelService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
