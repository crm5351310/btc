import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  CreateGeneralEducationLevelDto,
  FindManyGeneralEducationLevelParamDto,
  UpdateGeneralEducationLeveDto,
} from './general-education-level.dto';
import { GeneralEducationLevel } from './general-education-level.entity';

@Injectable()
export class GeneralEducationLevelService extends BaseCategoryService<GeneralEducationLevel> {
  constructor(
    @InjectRepository(GeneralEducationLevel)
    repository: Repository<GeneralEducationLevel>,
  ) {
    super(repository);
  }

  async findAll(
    query: FindManyGeneralEducationLevelParamDto,
  ): Promise<ResponseFindAll<GeneralEducationLevel>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<GeneralEducationLevel>> {
    const result = await this.checkNotExist(id, GeneralEducationLevel.name);

    return new ResponseFindOne(result);
  }

  async create(
    createGeneralEducationLevelDto: CreateGeneralEducationLevelDto,
  ): Promise<ResponseCreated<GeneralEducationLevel>> {
    await this.checkExist(['name', 'code'], createGeneralEducationLevelDto);

    const politicalTheory: GeneralEducationLevel = await this.repository.save(
      createGeneralEducationLevelDto,
    );

    return new ResponseCreated(politicalTheory);
  }

  async update(
    id: number,
    updateGeneralEducationLeveDto: UpdateGeneralEducationLeveDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, GeneralEducationLevel.name);
    await this.checkExist(['name', 'code'], updateGeneralEducationLeveDto, id);
    const result = await this.repository.save({
      id,
      ...updateGeneralEducationLeveDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<GeneralEducationLevel>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<GeneralEducationLevel>(results, ids);
  }
}
