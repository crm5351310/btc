import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GeneralEducationLevel } from './general-education-level.entity';
import { GeneralEducationLevelService } from './general-education-level.service';
import { GeneralEducationLevelController } from './general-education-level.controller';

@Module({
  imports: [TypeOrmModule.forFeature([GeneralEducationLevel])],
  controllers: [GeneralEducationLevelController],
  providers: [GeneralEducationLevelService],
  exports: [GeneralEducationLevelService],
})
export class GeneralEducationLevelModule {}
export const generalEducationLevelRouter: RouteTree = {
  path: 'general-education-level',
  module: GeneralEducationLevelModule,
  children: [],
};
