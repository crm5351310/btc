import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import {
  CreateFamilyClassDto,
  FindManyFamilyClassParamDto,
  UpdateFamilyClassDto,
} from '../family-class.dto';
import { FamilyClass } from '../family-class.entity';
import { FamilyClassModule } from '../family-class.module';
import { FamilyClassService } from '../family-class.service';

// npm run test:run src/module/category/family-class
describe('FamilyClassService', () => {
  let familyClassService: FamilyClassService;
  let familyClassRepository: Repository<FamilyClass>;

  const FAMILY_CLASS_REPOSITORY_TOKEN = getRepositoryToken(FamilyClass);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        FamilyClassModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    familyClassService = module.get<FamilyClassService>(FamilyClassService);
    familyClassRepository = module.get(FAMILY_CLASS_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(familyClassService).toBeDefined();
    expect(familyClassRepository).toBeDefined();
  });

  describe('FamilyClassService => create', () => {
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'thành phần gia đình số 1',
      description: 'Mô tả',
    } as CreateFamilyClassDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<FamilyClass>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<FamilyClass>;

    it('Tạo mới thành phần gia đình thành công', async () => {
      jest.spyOn(familyClassRepository, 'findOne').mockResolvedValue(null);
      jest
        .spyOn(familyClassRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as FamilyClass);

      const result = await familyClassService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới thành phần gia đình không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(familyClassRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as FamilyClass);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as FamilyClass);
        return Promise.resolve(null);
      });

      await expect(familyClassService.create(payload)).rejects.toThrowError();
    });
  });

  describe('FamilyClassService => findOne', () => {
    it('Tìm một thành phần gia đình thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'thành phần gia đình số 1',
        description: 'Mô tả',
      } as FamilyClass;
      jest.spyOn(familyClassRepository, 'findOne').mockResolvedValue(mockData);

      const result = await familyClassService.findOne({ id: 1 });

      expect(familyClassRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một thành phần gia đình nếu ID không tồn tại', async () => {
      jest.spyOn(familyClassRepository, 'findOne').mockResolvedValue(null);
      await expect(familyClassService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('FamilyClassService => findAll', () => {
    it('Tìm danh sách thành phần gia đình thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'thành phần gia đình số 1',
        description: 'Mô tả',
      } as FamilyClass;
      jest.spyOn(familyClassRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await familyClassService.findAll({} as FindManyFamilyClassParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('FamilyClassService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'thành phần gia đình số 1',
      description: 'Mô tả',
    } as UpdateFamilyClassDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<FamilyClass>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<FamilyClass>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<FamilyClass>;

    it('Cập nhật thành phần gia đình thành công', async () => {
      jest.spyOn(familyClassRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as FamilyClass);
        return Promise.resolve(null);
      });
      jest.spyOn(familyClassRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as FamilyClass);

      const result = await familyClassService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật thành phần gia đình không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(familyClassRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as FamilyClass);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as FamilyClass);
        return Promise.resolve(null);
      });
      jest.spyOn(familyClassRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as FamilyClass);

      await expect(familyClassService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật thành phần gia đình không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(familyClassRepository, 'findOne').mockResolvedValue({ id: 1 } as FamilyClass);
      jest.spyOn(familyClassRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as FamilyClass);

      await expect(familyClassService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('FamilyClassService => remove', () => {
    it('Xoá thành phần gia đình thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'thành phần gia đình số 1',
        description: 'Mô tả',
      } as FamilyClass;
      jest.spyOn(familyClassRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(familyClassRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await familyClassService.remove({ id: '1' });

      expect(familyClassRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá thành phần gia đình không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(familyClassRepository, 'find').mockResolvedValue([]);
      await expect(familyClassService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
