import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import {
  CreateFamilyClassDto,
  FindManyFamilyClassParamDto,
  UpdateFamilyClassDto,
} from './family-class.dto';
import { FamilyClassService } from './family-class.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.FAMILY_CLASS)
export class FamilyClassController {
  constructor(private readonly familyClassService: FamilyClassService) {}

  @Post()
  create(@Body() createFamilyClassDto: CreateFamilyClassDto) {
    return this.familyClassService.create(createFamilyClassDto);
  }

  @Get()
  findAll(@Query() param: FindManyFamilyClassParamDto) {
    return this.familyClassService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.familyClassService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateFamilyClassDto: UpdateFamilyClassDto) {
    return this.familyClassService.update(path, updateFamilyClassDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.familyClassService.remove(path);
  }
}
