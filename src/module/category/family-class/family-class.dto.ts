import { FamilyClass } from './family-class.entity';
import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class CreateFamilyClassDto extends OmitType(FamilyClass, [
  'id',
  'createdAt',
  'updatedAt',
  'createdBy',
  'updatedBy',
] as const) {}

export class UpdateFamilyClassDto extends PartialType(CreateFamilyClassDto) {}

export class FindManyFamilyClassParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
