import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FamilyClassController } from './family-class.controller';
import { FamilyClass } from './family-class.entity';
import { FamilyClassService } from './family-class.service';

@Module({
  imports: [TypeOrmModule.forFeature([FamilyClass])],
  controllers: [FamilyClassController],
  providers: [FamilyClassService],
  exports: [FamilyClassService],
})
export class FamilyClassModule {}
export const familyClassRouter: RouteTree = {
  path: 'family-class',
  module: FamilyClassModule,
  children: [],
};
