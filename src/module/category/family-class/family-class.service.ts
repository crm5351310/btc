import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../common/base/class/base.class';
import { BaseMessage } from '../../../common/message/base.message';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import { SubjectMessage } from '../../../common/message/subject.message';
import { Pagination } from '../../../common/utils/pagination.util';
import { StringHelper } from '../../../common/utils/string.util';
import {
  CreateFamilyClassDto,
  FindManyFamilyClassParamDto,
  UpdateFamilyClassDto,
} from './family-class.dto';
import { FamilyClass } from './family-class.entity';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';

@Injectable()
export class FamilyClassService extends BaseService {
  constructor(
    @InjectRepository(FamilyClass)
    private readonly familyClassRepository: Repository<FamilyClass>,
  ) {
    super();
    this.name = FamilyClassService.name;
    this.subject = SubjectMessage.FAMILY_CLASS;
  }
  async create(createFamilyClassDto: CreateFamilyClassDto) {
    this.action = BaseMessage.CREATE;
    createFamilyClassDto = StringHelper.spaceObject(createFamilyClassDto);
    const checkExistCodeFamilyClass = await this.familyClassRepository.findOne({
      where: {
        code: createFamilyClassDto.code,
      },
    });
    if (checkExistCodeFamilyClass)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    const checkExistNameFamilyClass = await this.familyClassRepository.findOne({
      where: {
        name: createFamilyClassDto.name,
      },
    });
    if (checkExistNameFamilyClass)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    const result = await this.familyClassRepository.save(createFamilyClassDto);
    return this.response(result);
  }

  async findAll(param: FindManyFamilyClassParamDto) {
    this.action = BaseMessage.READ;

    const result = await this.familyClassRepository.findAndCount({
      skip: param.offset || undefined,
      take: param.limit || undefined,
    });
    return this.response(new Pagination<FamilyClass>(result[0], result[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.familyClassRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.FAMILY_CLASS,
        true,
      );
    return this.response(result);
  }

  async update(path: PathDto, updateFamilyClassDto: UpdateFamilyClassDto) {
    this.action = BaseMessage.UPDATE;
    const checkExistFamilyClass = await this.familyClassRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistFamilyClass)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.FAMILY_CLASS,
        true,
      );
    if (updateFamilyClassDto.code) {
      const checkExistCode = await this.familyClassRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateFamilyClassDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateFamilyClassDto.name) {
      const checkExistName = await this.familyClassRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateFamilyClassDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const data = await this.familyClassRepository.save({
      id: path.id,
      ...updateFamilyClassDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistFamilyClass = await this.familyClassRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistFamilyClass.length !== pathArray.length)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.FAMILY_CLASS,
        true,
      );
    await this.familyClassRepository.softRemove(checkExistFamilyClass);
    return this.response(checkExistFamilyClass);
  }
}
