import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { AdministrativeModule, administrativeRouter } from './administrative/administrative.module';
import {
  EducationLevelModule,
  educationLevelRouter,
} from './education-level/education-level.module';
import { EthnicityModule, ethnicityRouter } from './ethnicity/ethnicity.module';
import { FamilyClassModule, familyClassRouter } from './family-class/family-class.module';
import {
  GeneralEducationLevelModule,
  generalEducationLevelRouter,
} from './general-education-level/general-education-level.module';
import { HealthModule, healthRouter } from './health/health.module';
import { ReligionModule, religionRouter } from './religion/religion.module';
import { PersonalClassModule, personalClassRouter } from './personal-class/personal-class.module';
import {
  PoliticalTheoryModule,
  politicalTheoryRouter,
} from './political-theory/political-theory.module';
import {
  MilitiaSelfDefenseActivityModule,
  militiaSelfDefenseActivityRouter,
} from './militia-self-defense-activity/militia-self-defense-activity.module';
import { JobModule, jobRouter } from './job/job.module';
import { VehicleRootModule, vehicleRootRouter } from './vehicle-root/vehicle-root.module';
import { AcademicDegreeModule } from './academic-degree/academic-degree.module';
import { TalentModule, talentRoute } from './talent/talent.module';
import {
  OrganizationRootModule,
  organizationRootRouter,
} from './organization-root/organization-root.module';
import { FamilyRelationModule } from './family-relation/family-relation.module';
import { MilitiaAndSelfDefenseTypeModule } from './militia-and-self-defence-type/militia-and-self-defence-type.module';
import { JobTitleModule, jobTitleRouter } from './job-title/job-title.module';
import { OrganizationScaleModule } from './organization-scale/organization-scale.module';
import { TrainingMethodModule } from './training-method/training-method.module';
import { MilitiaDefenseUnitModule } from './militia-defense-unit/militia-defense-unit.module';
import { ResultModule } from './result/result.module';
import { GroupProcessModule } from './group-process/group-process.module';
import { JobPositionModule } from './jop-position/job-position.module';
import {
  MilitaryRankRootModule,
  militaryRankRootRouter,
} from './military-rank-root/military-rank-root.module';
import { JobSpecializationRootModule } from './job-specialization-root/job-specialization-root.module';
import { ReasonModule } from './reason/reason.module';
import { ForceRootModule } from './force-root/force-root.module';
import { SchoolRootModule } from './school-root/school-root.module';
import {
  MilitaryJobRootModule,
  militaryJobRootRouter,
} from './military-job-root/military-job-root.module';
import { MilitaryUnitRootModule } from './military-unit-root/military-unit-root.module';
import { DefenseSecurityTraineeModule } from './defense-security-trainee/defense-security-trainee.module';
import { AwardLevelModule } from 'src/module/category/award-level/award-level.module';
import { CourseClassificationModule } from './course-classification/course-classification.module';
import { RankAwardModule } from './rank-award/rank-award.module';
import { DefenseSecurityEducationObjectModule } from './defense-security-education-object/defense-security-education-object.module';
import { EmulationTitleModule } from './emulation-title/emulation-title.module';

@Module({
  imports: [
    AdministrativeModule,
    PersonalClassModule,
    FamilyClassModule,
    EducationLevelModule,
    EthnicityModule,
    GeneralEducationLevelModule,
    HealthModule,
    ReligionModule,
    PoliticalTheoryModule,
    MilitiaSelfDefenseActivityModule,
    JobModule,
    AcademicDegreeModule,
    VehicleRootModule,
    TalentModule,
    FamilyRelationModule,
    OrganizationRootModule,
    MilitiaAndSelfDefenseTypeModule,
    JobTitleModule,
    OrganizationScaleModule,
    TrainingMethodModule,
    MilitiaDefenseUnitModule,
    ResultModule,
    GroupProcessModule,
    JobPositionModule,
    MilitaryRankRootModule,
    JobSpecializationRootModule,
    ReasonModule,
    ForceRootModule,
    MilitaryUnitRootModule,
    SchoolRootModule,
    MilitaryJobRootModule,
    DefenseSecurityTraineeModule,
    AwardLevelModule,
    CourseClassificationModule,
    RankAwardModule,
    DefenseSecurityEducationObjectModule,
    EmulationTitleModule,
  ],
  controllers: [],
  providers: [],
})
export class CategoryModule {}
export const categoryRouter: RouteTree = {
  path: 'category',
  module: CategoryModule,
  children: [
    administrativeRouter,
    personalClassRouter,
    familyClassRouter,
    educationLevelRouter,
    ethnicityRouter,
    generalEducationLevelRouter,
    healthRouter,
    politicalTheoryRouter,
    religionRouter,
    militiaSelfDefenseActivityRouter,
    jobRouter,
    vehicleRootRouter,
    AcademicDegreeModule.route,
    talentRoute,
    organizationRootRouter,
    FamilyRelationModule.route,
    MilitiaAndSelfDefenseTypeModule.route,
    jobTitleRouter,
    OrganizationScaleModule.route,
    TrainingMethodModule.route,
    MilitiaDefenseUnitModule.route,
    ResultModule.route,
    GroupProcessModule.route,
    JobPositionModule.route,
    militaryRankRootRouter,
    JobSpecializationRootModule.route,
    ReasonModule.route,
    ForceRootModule.route,
    MilitaryUnitRootModule.route,
    SchoolRootModule.route,
    militaryJobRootRouter,
    DefenseSecurityTraineeModule.route,
    AwardLevelModule.route,
    CourseClassificationModule.route,
    RankAwardModule.route,
    DefenseSecurityEducationObjectModule.route,
    EmulationTitleModule.route,
  ],
};
