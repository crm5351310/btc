import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { FamilyRelationService } from './family-relation.service';
import { CreateFamilyRelationDto, QueryFamilyRelation, UpdateFamilyRelationDto } from './dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.FAMILY_RELATION)
export class FamilyRelationController {
  constructor(private readonly familyRelationService: FamilyRelationService) {}

  @Post()
  create(@Body() createFamilyRelationDto: CreateFamilyRelationDto) {
    return this.familyRelationService.create(createFamilyRelationDto);
  }

  @Get()
  findAll(@Query() query: QueryFamilyRelation) {
    return this.familyRelationService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.familyRelationService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFamilyRelationDto: UpdateFamilyRelationDto) {
    return this.familyRelationService.update(+id, updateFamilyRelationDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.familyRelationService.remove(ids);
  }
}
