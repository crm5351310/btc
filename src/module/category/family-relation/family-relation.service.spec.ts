import { Test, TestingModule } from '@nestjs/testing';
import { FamilyRelationService } from './family-relation.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { FamilyRelation } from './entities';
import { CreateFamilyRelationDto, UpdateFamilyRelationDto } from './dto';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';

describe('FamilyRelationService', () => {
  let service: FamilyRelationService;
  let repository: Repository<FamilyRelation>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FamilyRelationService,
        {
          provide: getRepositoryToken(FamilyRelation),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<FamilyRelationService>(FamilyRelationService);
    repository = module.get(getRepositoryToken(FamilyRelation));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        familyRelations.find(
          (value) => value.id === (options.where as FindOptionsWhere<FamilyRelation>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return familyRelations;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [familyRelations, familyRelations.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<FamilyRelation>) => {
        return familyRelations.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<FamilyRelation>).code ||
            e.name == (options.where as FindOptionsWhere<FamilyRelation>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: FamilyRelation) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: familyRelations.length,
        };
      }
    });
  });

  const familyRelations = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as FamilyRelation[];

  describe('FamilyRelationService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateFamilyRelationDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateFamilyRelationDto;

      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('FamilyRelationService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(familyRelations.length);
    });
  });

  describe('FamilyRelationService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('FamilyRelationService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateFamilyRelationDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name11',
      } as UpdateFamilyRelationDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('FamilyRelationService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
