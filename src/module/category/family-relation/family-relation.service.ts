import { Injectable } from '@nestjs/common';
import { CreateFamilyRelationDto } from './dto/create-family-relation.dto';
import { UpdateFamilyRelationDto } from './dto/update-family-relation.dto';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import { FamilyRelation } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { QueryFamilyRelation } from './dto';

@Injectable()
export class FamilyRelationService extends BaseCategoryService<FamilyRelation> {
  constructor(
    @InjectRepository(FamilyRelation)
    private readonly familyRelationRepository: Repository<FamilyRelation>,
  ) {
    super(familyRelationRepository);
  }
  async create(
    createFamilyRelationDto: CreateFamilyRelationDto,
  ): Promise<ResponseCreated<FamilyRelation>> {
    await this.checkExist(['name', 'code'], createFamilyRelationDto);

    const familyRelation: FamilyRelation =
      await this.familyRelationRepository.save(createFamilyRelationDto);

    return new ResponseCreated(familyRelation);
  }

  async findAll(query: QueryFamilyRelation): Promise<ResponseFindAll<FamilyRelation>> {
    const [results, total] = await this.familyRelationRepository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<FamilyRelation>> {
    const result = await this.checkNotExist(id, FamilyRelation.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateFamilyRelationDto: UpdateFamilyRelationDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, FamilyRelation.name);
    await this.checkExist(['name', 'code'], updateFamilyRelationDto, id);
    const result = await this.familyRelationRepository.save({
      id,
      ...updateFamilyRelationDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<FamilyRelation>> {
    const results = await this.familyRelationRepository.find({
      where: {
        id: In(ids),
        isFreeze: false,
      },
    });

    await this.familyRelationRepository.softRemove(results);
    return new ResponseDelete<FamilyRelation>(results, ids);
  }
}
