import { Module } from '@nestjs/common';
import { FamilyRelationService } from './family-relation.service';
import { FamilyRelationController } from './family-relation.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FamilyRelation } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([FamilyRelation])],
  controllers: [FamilyRelationController],
  providers: [FamilyRelationService],
})
export class FamilyRelationModule {
  static readonly route: RouteTree = {
    path: 'family-relation',
    module: FamilyRelationModule,
  };
}
