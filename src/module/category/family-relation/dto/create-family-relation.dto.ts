import { OmitType } from '@nestjs/swagger';
import { FamilyRelation } from '../entities/family-relation.entity';

export class CreateFamilyRelationDto extends OmitType(FamilyRelation, ['id'] as const) {}
