import { PartialType } from '@nestjs/swagger';
import { CreateFamilyRelationDto } from './create-family-relation.dto';

export class UpdateFamilyRelationDto extends PartialType(CreateFamilyRelationDto) {}
