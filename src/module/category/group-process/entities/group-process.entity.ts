import { PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { Entity, JoinTable, ManyToMany } from 'typeorm';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { JobPosition } from '../../jop-position/entities/job-position.entity';

@Entity()
export class GroupProcess extends BaseCategoryEntity {
  // validator
  @IsDefined()
  @ValidateNested()
  // orm
  @ManyToMany(() => JobPosition, (item) => item.groupProcesses)
  @JoinTable()
  jobPositions: JobPosition[];
}
