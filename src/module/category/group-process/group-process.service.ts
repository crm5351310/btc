import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import { ResponseFindAll } from '../../../common/response';
import { GroupProcess } from './entities/group-process.entity';

@Injectable()
export class GroupProcessService {
  constructor(
    @InjectRepository(GroupProcess)
    private readonly repository: Repository<GroupProcess>,
  ) {}

  async findAll(): Promise<ResponseFindAll<GroupProcess>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }
}
