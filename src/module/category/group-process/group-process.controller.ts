import { Controller, Get, Param } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { GroupProcessService } from './group-process.service';

@Controller()
@ApiTags(TagEnum.GROUP_PROCESS)
export class GroupProcessController {
  constructor(private readonly resultService: GroupProcessService) {}

  @Get()
  findAll() {
    return this.resultService.findAll();
  }
}
