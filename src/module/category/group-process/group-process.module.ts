import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupProcess } from './entities/group-process.entity';
import { GroupProcessController } from './group-process.controller';
import { GroupProcessService } from './group-process.service';

@Module({
  imports: [TypeOrmModule.forFeature([GroupProcess])],
  controllers: [GroupProcessController],
  providers: [GroupProcessService],
})
export class GroupProcessModule {
  static readonly route: RouteTree = {
    path: 'group-process',
    module: GroupProcessModule,
  };
}
