import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateTrainingMethodDto, QueryTrainingMethodDto, UpdateTrainingMehodDto } from './dto';
import { TrainingMethodService } from './training-method.service';

@Controller()
@ApiTags(TagEnum.TRAINING_METHOD)
export class TrainingMethodController {
  constructor(private readonly trainingMethodService: TrainingMethodService) {}

  @Post()
  create(@Body() createTrainingMethodDto: CreateTrainingMethodDto) {
    return this.trainingMethodService.create(createTrainingMethodDto);
  }

  @Get()
  findAll(@Query() query: QueryTrainingMethodDto) {
    return this.trainingMethodService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.trainingMethodService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTrainingMethodDto: UpdateTrainingMehodDto) {
    return this.trainingMethodService.update(+id, updateTrainingMethodDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.trainingMethodService.remove(ids);
  }
}
