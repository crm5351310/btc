import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { TrainingMethodService } from './training-method.service';
import { TrainingMethod } from './entities/training-method.entity';
import { CreateTrainingMethodDto } from './dto/create-training-method.dto';
import { UpdateTrainingMehodDto } from './dto/update-training-method.dto';

describe('TrainingMethodService', () => {
  let service: TrainingMethodService;
  let repository: Repository<TrainingMethod>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TrainingMethodService,
        {
          provide: getRepositoryToken(TrainingMethod),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<TrainingMethodService>(TrainingMethodService);
    repository = module.get(getRepositoryToken(TrainingMethod));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        trainingmethods.find(
          (value) => value.id === (options.where as FindOptionsWhere<TrainingMethod>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return trainingmethods;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [trainingmethods, trainingmethods.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<TrainingMethod>) => {
        return trainingmethods.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<TrainingMethod>).code ||
            e.name == (options.where as FindOptionsWhere<TrainingMethod>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: TrainingMethod) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: trainingmethods.length,
        };
      }
    });
  });

  const trainingmethods = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as TrainingMethod[];

  describe('TrainingMethodService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateTrainingMethodDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('TrainingMethodService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(trainingmethods.length);
    });
  });

  describe('TrainingMethodService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('TrainingMethodService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateTrainingMehodDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateTrainingMehodDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('TrainingMethodService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
