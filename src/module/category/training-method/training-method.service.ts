import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { CreateTrainingMethodDto, QueryTrainingMethodDto, UpdateTrainingMehodDto } from './dto';
import { TrainingMethod } from './entities';

@Injectable()
export class TrainingMethodService extends BaseCategoryService<TrainingMethod> {
  constructor(
    @InjectRepository(TrainingMethod)
    repository: Repository<TrainingMethod>,
  ) {
    super(repository);
  }

  async create(createTrainingMethodDto: CreateTrainingMethodDto) {
    await this.checkExist(['name', 'code'], createTrainingMethodDto);

    const trainingMethod: TrainingMethod = await this.repository.save(createTrainingMethodDto);

    return new ResponseCreated(trainingMethod);
  }

  async findAll(query: QueryTrainingMethodDto): Promise<ResponseFindAll<TrainingMethod>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<TrainingMethod>> {
    const result = await this.checkNotExist(id, TrainingMethod.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateTrainingMethodDto: UpdateTrainingMehodDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, TrainingMethod.name);
    await this.checkExist(['name', 'code'], updateTrainingMethodDto, id);
    const result = await this.repository.save({
      id,
      ...updateTrainingMethodDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<TrainingMethod>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<TrainingMethod>(results, ids);
  }
}
