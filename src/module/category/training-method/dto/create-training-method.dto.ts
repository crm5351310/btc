import { OmitType } from '@nestjs/swagger';
import { TrainingMethod } from '../entities/training-method.entity';

export class CreateTrainingMethodDto extends OmitType(TrainingMethod, ['id'] as const) {}
