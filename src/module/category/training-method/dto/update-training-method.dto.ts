import { PartialType } from '@nestjs/swagger';
import { CreateTrainingMethodDto } from './create-training-method.dto';

export class UpdateTrainingMehodDto extends PartialType(CreateTrainingMethodDto) {}
