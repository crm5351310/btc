import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TrainingMethod } from './entities';
import { TrainingMethodController } from './training-method.controller';
import { TrainingMethodService } from './training-method.service';

@Module({
  imports: [TypeOrmModule.forFeature([TrainingMethod])],
  controllers: [TrainingMethodController],
  providers: [TrainingMethodService],
})
export class TrainingMethodModule {
  static readonly route: RouteTree = {
    path: 'training-method',
    module: TrainingMethodModule,
  };
}
