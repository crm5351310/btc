import { ApiProperty } from '@nestjs/swagger/dist/decorators/api-property.decorator';
import { IsOptional, IsString, MaxLength } from 'class-validator';
import { BaseCategoryEntity } from '../../../common/base/entity/base-category.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { EmulationTitle } from '../emulation-title/entities/emulation-title.entity';

@Entity()
export class AwardLevel extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;

  @OneToMany(() => EmulationTitle, (item) => item.awardLevel)
  emulationTitle: EmulationTitle[];
}
