import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AwardLevel } from './award-level.entity';
import { AwardLevelConTroller } from './award-level.controller';
import { AwardLevelService } from './award-level.service';

@Module({
  imports: [TypeOrmModule.forFeature([AwardLevel])],
  controllers: [AwardLevelConTroller],
  providers: [AwardLevelService],
  exports: [AwardLevelService],
})
export class AwardLevelModule {
  static readonly route: RouteTree = {
    path: 'award-level',
    module: AwardLevelModule,
  };
}
