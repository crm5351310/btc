import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { AwardLevel } from './award-level.entity';
import { BaseQueryDto } from 'src/common/base/dto/base-query.dto';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateAwardLevelDto extends OmitType(AwardLevel, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
]) {}

export class UpdateAwardLevelDto extends PartialType(CreateAwardLevelDto) {}

export class FindManyAwardLevelDto extends BaseQueryDto {}
