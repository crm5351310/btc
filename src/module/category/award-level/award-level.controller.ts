import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { AwardLevelService } from './award-level.service';
import { CreateAwardLevelDto, FindManyAwardLevelDto, UpdateAwardLevelDto } from './award-level.dto';
import { FindManyAccountParamDto } from 'src/module/system';
import { TagEnum } from 'src/common/enum/tag.enum';
import { ApiTags } from '@nestjs/swagger';

@Controller()
@ApiTags(TagEnum.AWARD_LEVEL)
export class AwardLevelConTroller {
  constructor(private readonly awardLevelService: AwardLevelService) {}

  @Post()
  create(@Body() createDto: CreateAwardLevelDto) {
    return this.awardLevelService.create(createDto);
  }

  @Get()
  findAll(@Query() query: FindManyAwardLevelDto) {
    return this.awardLevelService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.awardLevelService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAcademicDegreeDto: UpdateAwardLevelDto) {
    return this.awardLevelService.update(+id, updateAcademicDegreeDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.awardLevelService.remove(ids);
  }
}
