import {
  ResponseCreated,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
} from 'src/common/response';
import { AwardLevel } from './award-level.entity';
import { BaseCategoryService } from 'src/common/base/service/base-category.service';
import { Injectable } from '@nestjs/common';
import {
  CreateAwardLevelDto,
  FindManyAwardLevelDto,
  UpdateAwardLevelDto,
} from './award-level.dto';
import { In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AwardLevelService extends BaseCategoryService<AwardLevel> {
  constructor(
    @InjectRepository(AwardLevel)
    repository: Repository<AwardLevel>,
  ) {
    super(repository);
  }

  async create(createDto: CreateAwardLevelDto): Promise<ResponseCreated<AwardLevel>> {
    await this.checkExist(['code', 'name'], createDto);

    const rankAward: AwardLevel = await this.repository.save(createDto);

    return new ResponseCreated(rankAward);
  }

  async update(id: number, dto: UpdateAwardLevelDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, AwardLevel.name);
    await this.checkExist(['name', 'code'], dto, id);
    const res = await this.repository.save({ id, ...dto });
    return new ResponseUpdate(res);
  }

  async remove(ids: number[]): Promise<ResponseDelete<AwardLevel>> {
    const inUses = await this.repository.find({
      where: {
        id: In(ids),
      },
      relations: {
        emulationTitle: true,
      },
    });
    
    await this.repository.softRemove(inUses);
    return new ResponseDelete<AwardLevel>(inUses, ids);
  }

  async findAll(dto: FindManyAwardLevelDto): Promise<ResponseFindAll<AwardLevel>> {
    const [results, total] = await this.repository.findAndCount({
      skip: dto.limit && dto.offset,
      take: dto.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<AwardLevel>> {
    const res = await this.checkNotExist(id, AwardLevel.name);

    return new ResponseFindOne(res);
  }
}
