import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import { StringHelper } from '../../../common/utils/string.util';
import {
  CreateEducationLevelDto,
  FindManyEducationLevelParamDto,
  UpdateEducationLevelDto,
} from './education-level.dto';
import { EducationLevel } from './education-level.entity';

@Injectable()
export class EducationLevelService extends BaseCategoryService<EducationLevel> {
  constructor(
    @InjectRepository(EducationLevel)
    private readonly educationLevelRepository: Repository<EducationLevel>,
  ) {
    super(educationLevelRepository);
  }

  async findAll(query: FindManyEducationLevelParamDto) {
    const [results, total] = await this.educationLevelRepository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number) {
    const result = await this.checkNotExist(id, EducationLevel.name);

    return new ResponseFindOne(result);
  }

  async create(payload: CreateEducationLevelDto) {
    payload = StringHelper.spaceObject(payload);
    await this.checkExist(['code', 'name'], payload);
    const result = await this.educationLevelRepository.save(payload);
    return new ResponseCreated(result);
  }

  async update(id: number, payload: UpdateEducationLevelDto) {
    await this.checkNotExist(id, EducationLevel.name);
    await this.checkExist(['name', 'code'], payload, id);

    const result = await this.educationLevelRepository.save({
      id,
      ...payload,
    });
    return new ResponseUpdate(result);
  }

  async remove(ids: number[]) {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });

    await this.educationLevelRepository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
