import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import {
  CreateEducationLevelDto,
  FindManyEducationLevelParamDto,
  UpdateEducationLevelDto,
} from './education-level.dto';
import { EducationLevelService } from './education-level.services';

@Controller()
@ApiTags(TagEnum.EDUCATION_LEVEL)
export class EducationLevelController {
  constructor(private readonly educationLevelService: EducationLevelService) {}

  @Get('')
  async findAll(@Query() query: FindManyEducationLevelParamDto) {
    const result = await this.educationLevelService.findAll(query);
    return result;
  }

  @Get(':id')
  async findOne(@Param('id') id: number) {
    const result = await this.educationLevelService.findOne(id);
    return result;
  }

  @Post('')
  async create(@Body() payload: CreateEducationLevelDto) {
    const result = await this.educationLevelService.create(payload);
    return result;
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() payload: UpdateEducationLevelDto) {
    const result = await this.educationLevelService.update(id, payload);
    return result;
  }

  @Delete(':ids')
  async remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    const result = await this.educationLevelService.remove(ids);
    return result;
  }
}
