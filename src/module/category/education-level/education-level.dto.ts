import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { EducationLevel } from './education-level.entity';

export class CreateEducationLevelDto extends OmitType(EducationLevel, ['id' as const]) {}

export class UpdateEducationLevelDto extends PartialType(CreateEducationLevelDto) {}

export class FindManyEducationLevelParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset?: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit?: number;
}
