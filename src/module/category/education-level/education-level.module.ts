import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EducationLevel } from './education-level.entity';
import { EducationLevelController } from './education-level.controller';
import { EducationLevelService } from './education-level.services';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([EducationLevel])],
  controllers: [EducationLevelController],
  providers: [EducationLevelService],
  exports: [EducationLevelService],
})
export class EducationLevelModule {}
export const educationLevelRouter: RouteTree = {
  path: 'education-level',
  module: EducationLevelModule,
  children: [],
};
