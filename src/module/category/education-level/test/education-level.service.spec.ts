import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { dataSourceOptions } from '../../../../config/data-source.config';
import {
  CreateEducationLevelDto,
  FindManyEducationLevelParamDto,
  UpdateEducationLevelDto,
} from '../education-level.dto';
import { EducationLevel } from '../education-level.entity';
import { EducationLevelModule } from '../education-level.module';
import { EducationLevelService } from '../education-level.services';

//  npm run test:run src/module/category/education-level

describe('EducationLevelService', () => {
  let educationLevelService: EducationLevelService;
  let module: TestingModule;
  let educationLevelRepository: Repository<EducationLevel>;

  const EDUCATION_LEVEL_REPOSITORY_TOKEN = getRepositoryToken(EducationLevel);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        EducationLevelModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
      ],
    }).compile();

    educationLevelRepository = module.get(EDUCATION_LEVEL_REPOSITORY_TOKEN);
    educationLevelService = module.get<EducationLevelService>(EducationLevelService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('EducationLevelService => create', () => {
    const payload = {
      code: 'code',
      name: 'tên',
      description: 'Mô tả',
    } as CreateEducationLevelDto;

    it('Tạo mới trình độ học vấn thành công', async () => {
      jest
        .spyOn(educationLevelRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as EducationLevel);

      const result = await educationLevelService.create(payload);

      expect(educationLevelRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới trình độ học vấn không thành công nếu tên tồn tại', async () => {
      jest.spyOn(educationLevelService, 'create').mockImplementation(async () => {
        throw new CustomBadRequestException('EXIST', 'NAME', false);
      });

      await expect(educationLevelService.create(payload)).rejects.toThrowError();
      expect(educationLevelRepository.save).not.toHaveBeenCalled();
    });

    it('Tạo mới trình độ học vấn không thành công nếu code tồn tại', async () => {
      jest.spyOn(educationLevelService, 'create').mockImplementation(async () => {
        throw new CustomBadRequestException('EXIST', 'CODE', false);
      });

      await expect(educationLevelService.create(payload)).rejects.toThrowError();
      expect(educationLevelRepository.save).not.toHaveBeenCalled();
    });
  });

  describe('EducationLevelService => findOne', () => {
    it('Tìm một trình độ văn hóa thành công', async () => {
      const mockData = {
        id: 1,
        code: 'code',
        name: 'name',
      } as EducationLevel;
      jest.spyOn(educationLevelRepository, 'findOne').mockResolvedValue(mockData);

      const result = await educationLevelService.findOne({ id: 1 } as PathDto);

      expect(educationLevelRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một trình độ học vấn nếu ID không tồn tại', async () => {
      jest.spyOn(educationLevelRepository, 'findOne').mockResolvedValue(null);
      await expect(educationLevelService.findOne({ id: 1 } as PathDto)).rejects.toThrowError();
    });
  });

  describe('EducationLevelService => findAll', () => {
    it('Tìm danh trình độ học vấn thành công', async () => {
      const mockData = [
        { id: 1, code: 'code 1', name: 'name 1' },
        { id: 2, code: 'code 2', name: 'name 2' },
        { id: 3, code: 'code 3', name: 'name 3' },
      ] as EducationLevel[];
      const query: FindManyEducationLevelParamDto = {
        offset: 0,
        limit: 10,
      };
      jest.spyOn(educationLevelRepository, 'find').mockResolvedValue(mockData);
      jest.spyOn(educationLevelRepository, 'count').mockResolvedValue(3);

      const result = await educationLevelService.findAll(query);

      expect(educationLevelRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('EducationLevelService => update', () => {
    it('Cập nhật trường code trình độ học vấn thành công', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        code: 'newCode',
      } as UpdateEducationLevelDto;

      jest.spyOn(educationLevelRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as EducationLevel,
      );

      jest.spyOn(educationLevelRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(educationLevelRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(educationLevelRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as EducationLevel);

      const result = await educationLevelService.update(path, updatePayload);

      expect(result.statusCode).toBe(200);
      expect(result.data).toEqual(expect.objectContaining(updatePayload));
      expect(educationLevelRepository.save).toHaveBeenCalledWith({
        id: path.id,
        ...updatePayload,
      });
    });
    it('Cập nhật trường name trình độ học vấn thành công', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        name: 'newName',
      } as UpdateEducationLevelDto;

      jest.spyOn(educationLevelRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as EducationLevel,
      );

      jest.spyOn(educationLevelRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(educationLevelRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(educationLevelRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as EducationLevel);

      const result = await educationLevelService.update(path, updatePayload);

      expect(result.statusCode).toBe(200);
      expect(result.data).toEqual(expect.objectContaining(updatePayload));
      expect(educationLevelRepository.save).toHaveBeenCalledWith({
        id: path.id,
        ...updatePayload,
      });
    });
  });

  describe('EducationLevelService => remove', () => {
    it('Xoá một trình độ học vấn thành công', async () => {
      const mockData = { id: 1 } as EducationLevel;
      jest.spyOn(educationLevelRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(educationLevelRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await educationLevelService.remove({
        id: '1',
      } as PathArrayDto);

      expect(educationLevelRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một chức danh nếu ID không tồn tại', async () => {
      jest.spyOn(educationLevelRepository, 'find').mockResolvedValue([]);
      await expect(educationLevelService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
