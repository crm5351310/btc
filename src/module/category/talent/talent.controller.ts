import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateTalentDto, FindManyTalentParamDto, UpdateTalentDto } from './talent.dto';
import { TalentService } from './talent.service';

@Controller()
@ApiTags(TagEnum.TALENT)
export class TalentController {
  constructor(private readonly talentService: TalentService) {}

  @Post()
  create(@Body() createTalentDto: CreateTalentDto) {
    return this.talentService.create(createTalentDto);
  }

  @Get()
  findAll(@Query() param: FindManyTalentParamDto) {
    return this.talentService.findAll(param);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.talentService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateTalentDto: UpdateTalentDto) {
    return this.talentService.update(id, updateTalentDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.talentService.remove(ids);
  }
}
