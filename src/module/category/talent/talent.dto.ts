import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { Talent } from './talent.entity';
import { IsOptional, IsNumber } from 'class-validator';

export class CreateTalentDto extends OmitType(Talent, [
  'id',
  'createdAt',
  'deletedAt',
  'updatedAt',
  'createdBy',
  'updatedBy',
] as const) {}

export class UpdateTalentDto extends PartialType(CreateTalentDto) {}

export class FindManyTalentParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
