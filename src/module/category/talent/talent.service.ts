import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from 'src/common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from 'src/common/response';
import { In, Repository } from 'typeorm';
import { CreateTalentDto, FindManyTalentParamDto, UpdateTalentDto } from './talent.dto';
import { Talent } from './talent.entity';

@Injectable()
export class TalentService extends BaseCategoryService<Talent> {
  constructor(
    @InjectRepository(Talent)
    repository: Repository<Talent>,
  ) {
    super(repository);
  }
  async create(createTalentDto: CreateTalentDto) {
    await this.checkExist(['code', 'name'], createTalentDto);
    const talent: Talent = await this.repository.save(createTalentDto);

    return new ResponseCreated(talent);
  }

  async findAll(param: FindManyTalentParamDto) {
    const [results, total] = await this.repository.findAndCount({
      skip: param.limit && param.offset,
      take: param.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Talent>> {
    const result = await this.checkNotExist(id, Talent.name);

    return new ResponseFindOne(result);
  }

  async update(id: number, updateTalentDto: UpdateTalentDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Talent.name);
    await this.checkExist(['name', 'code'], updateTalentDto, id);
    const result = await this.repository.save({
      id,
      ...updateTalentDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Talent>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
