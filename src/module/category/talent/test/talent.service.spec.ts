import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { TalentService } from '../talent.service';
import { Talent } from '../talent.entity';
import { TalentModule } from '../talent.module';
import { CreateTalentDto, FindManyTalentParamDto, UpdateTalentDto } from '../talent.dto';

// npm run test:run src/module/category/talent
describe('TalentService', () => {
  let talentService: TalentService;
  let talentRepository: Repository<Talent>;

  const TALENT_REPOSITORY_TOKEN = getRepositoryToken(Talent);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TalentModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    talentService = module.get<TalentService>(TalentService);
    talentRepository = module.get(TALENT_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(talentService).toBeDefined();
    expect(talentRepository).toBeDefined();
  });

  describe('TalentService => create', () => {
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'năng khiếu số 1',
      description: 'Mô tả',
    } as CreateTalentDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Talent>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Talent>;

    it('Tạo mới năng khiếu thành công', async () => {
      jest.spyOn(talentRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(talentRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Talent);

      const result = await talentService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới năng khiếu không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(talentRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as Talent);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as Talent);
        return Promise.resolve(null);
      });

      await expect(talentService.create(payload)).rejects.toThrowError();
    });
  });

  describe('TalentService => findOne', () => {
    it('Tìm một năng khiếu thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'năng khiếu số 1',
        description: 'Mô tả',
      } as Talent;
      jest.spyOn(talentRepository, 'findOne').mockResolvedValue(mockData);

      const result = await talentService.findOne({ id: 1 });

      expect(talentRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một năng khiếu nếu ID không tồn tại', async () => {
      jest.spyOn(talentRepository, 'findOne').mockResolvedValue(null);
      await expect(talentService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('TalentService => findAll', () => {
    it('Tìm danh sách năng khiếu thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'năng khiếu số 1',
        description: 'Mô tả',
      } as Talent;
      jest.spyOn(talentRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await talentService.findAll({} as FindManyTalentParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('TalentService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'năng khiếu số 1',
      description: 'Mô tả',
    } as UpdateTalentDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Talent>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Talent>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<Talent>;

    it('Cập nhật năng khiếu thành công', async () => {
      jest.spyOn(talentRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as Talent);
        return Promise.resolve(null);
      });
      jest.spyOn(talentRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Talent);

      const result = await talentService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật năng khiếu không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(talentRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as Talent);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as Talent);
        return Promise.resolve(null);
      });
      jest.spyOn(talentRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Talent);

      await expect(talentService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật năng khiếu không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(talentRepository, 'findOne').mockResolvedValue({ id: 1 } as Talent);
      jest.spyOn(talentRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Talent);

      await expect(talentService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('TalentService => remove', () => {
    it('Xoá năng khiếu thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'năng khiếu số 1',
        description: 'Mô tả',
      } as Talent;
      jest.spyOn(talentRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(talentRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await talentService.remove({ id: '1' });

      expect(talentRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá năng khiếu không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(talentRepository, 'find').mockResolvedValue([]);
      await expect(talentService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
