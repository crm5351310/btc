import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import {
  CreatePersonalClassDto,
  FindManyPersonalClassParamDto,
  UpdatePersonalClassDto,
} from '../personal-class.dto';
import { PersonalClass } from '../personal-class.entity';
import { PersonalClassModule } from '../personal-class.module';
import { PersonalClassService } from '../personal-class.service';

// npm run test:run src/module/category/personal-class
describe('PersonalClassService', () => {
  let personalClassService: PersonalClassService;
  let personalClassRepository: Repository<PersonalClass>;

  const PERSONAL_CLASS_REPOSITORY_TOKEN = getRepositoryToken(PersonalClass);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PersonalClassModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    personalClassService = module.get<PersonalClassService>(PersonalClassService);
    personalClassRepository = module.get(PERSONAL_CLASS_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(personalClassService).toBeDefined();
    expect(personalClassRepository).toBeDefined();
  });

  describe('PersonalClassService => create', () => {
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'thành phần bản thân số 1',
      description: 'Mô tả',
    } as CreatePersonalClassDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<PersonalClass>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<PersonalClass>;

    it('Tạo mới thành phần bản thân thành công', async () => {
      jest.spyOn(personalClassRepository, 'findOne').mockResolvedValue(null);
      jest
        .spyOn(personalClassRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as PersonalClass);

      const result = await personalClassService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới thành phần bản thân không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(personalClassRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as PersonalClass);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as PersonalClass);
        return Promise.resolve(null);
      });

      await expect(personalClassService.create(payload)).rejects.toThrowError();
    });
  });

  describe('PersonalClassService => findOne', () => {
    it('Tìm một thành phần bản thân thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'thành phần bản thân số 1',
        description: 'Mô tả',
      } as PersonalClass;
      jest.spyOn(personalClassRepository, 'findOne').mockResolvedValue(mockData);

      const result = await personalClassService.findOne({ id: 1 });

      expect(personalClassRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một thành phần bản thân nếu ID không tồn tại', async () => {
      jest.spyOn(personalClassRepository, 'findOne').mockResolvedValue(null);
      await expect(personalClassService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('PersonalClassService => findAll', () => {
    it('Tìm danh sách thành phần bản thân thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'thành phần bản thân số 1',
        description: 'Mô tả',
      } as PersonalClass;
      jest.spyOn(personalClassRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await personalClassService.findAll({} as FindManyPersonalClassParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('PersonalClassService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'thành phần bản thân số 1',
      description: 'Mô tả',
    } as UpdatePersonalClassDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<PersonalClass>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<PersonalClass>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<PersonalClass>;

    it('Cập nhật thành phần bản thân thành công', async () => {
      jest.spyOn(personalClassRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as PersonalClass);
        return Promise.resolve(null);
      });
      jest.spyOn(personalClassRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as PersonalClass);

      const result = await personalClassService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật thành phần bản thân không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(personalClassRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as PersonalClass);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as PersonalClass);
        return Promise.resolve(null);
      });
      jest.spyOn(personalClassRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as PersonalClass);

      await expect(personalClassService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật thành phần bản thân không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(personalClassRepository, 'findOne').mockResolvedValue({ id: 1 } as PersonalClass);
      jest.spyOn(personalClassRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as PersonalClass);

      await expect(personalClassService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('PersonalClassService => remove', () => {
    it('Xoá thành phần bản thân thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'thành phần bản thân số 1',
        description: 'Mô tả',
      } as PersonalClass;
      jest.spyOn(personalClassRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(personalClassRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await personalClassService.remove({ id: '1' });

      expect(personalClassRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá thành phần bản thân không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(personalClassRepository, 'find').mockResolvedValue([]);
      await expect(personalClassService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
