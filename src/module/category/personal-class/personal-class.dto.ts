import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { PersonalClass } from './personal-class.entity';
import { IsNumber, IsOptional } from 'class-validator';

export class CreatePersonalClassDto extends OmitType(PersonalClass, ['id'] as const) {}

export class UpdatePersonalClassDto extends PartialType(CreatePersonalClassDto) {}

export class FindManyPersonalClassParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
