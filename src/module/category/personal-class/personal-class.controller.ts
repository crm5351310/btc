import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from '../../../common/base/class/base.class';
import { TagEnum } from '../../../common/enum/tag.enum';
import {
  CreatePersonalClassDto,
  FindManyPersonalClassParamDto,
  UpdatePersonalClassDto,
} from './personal-class.dto';
import { PersonalClassService } from './personal-class.service';

@Controller()
@ApiTags(TagEnum.PERSONAL_CLASS)
export class PersonalClassController {
  constructor(private readonly personalClassService: PersonalClassService) {}

  @Post()
  create(@Body() createPersonalClassDto: CreatePersonalClassDto) {
    return this.personalClassService.create(createPersonalClassDto);
  }

  @Get()
  findAll(@Query() param: FindManyPersonalClassParamDto) {
    return this.personalClassService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.personalClassService.findOne(path.id);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updatePersonalClassDto: UpdatePersonalClassDto) {
    return this.personalClassService.update(path.id, updatePersonalClassDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.personalClassService.remove(ids);
  }
}
