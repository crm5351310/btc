import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonalClassController } from './personal-class.controller';
import { PersonalClass } from './personal-class.entity';
import { PersonalClassService } from './personal-class.service';

@Module({
  imports: [TypeOrmModule.forFeature([PersonalClass])],
  controllers: [PersonalClassController],
  providers: [PersonalClassService],
  exports: [PersonalClassService],
})
export class PersonalClassModule {}
export const personalClassRouter: RouteTree = {
  path: 'personal-class',
  module: PersonalClassModule,
  children: [],
};
