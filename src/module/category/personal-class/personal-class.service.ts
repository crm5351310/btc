import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import {
  CreatePersonalClassDto,
  FindManyPersonalClassParamDto,
  UpdatePersonalClassDto,
} from './personal-class.dto';
import { PersonalClass } from './personal-class.entity';

@Injectable()
export class PersonalClassService extends BaseCategoryService<PersonalClass> {
  constructor(
    @InjectRepository(PersonalClass)
    repository: Repository<PersonalClass>,
  ) {
    super(repository);
  }
  async create(
    createPersonalClassDto: CreatePersonalClassDto,
  ): Promise<ResponseCreated<PersonalClass>> {
    await this.checkExist(['name', 'code'], createPersonalClassDto);

    const religion: PersonalClass = await this.repository.save(createPersonalClassDto);

    return new ResponseCreated(religion);
  }

  async findAll(param: FindManyPersonalClassParamDto): Promise<ResponseFindAll<PersonalClass>> {
    const [results, total] = await this.repository.findAndCount({
      skip: param.limit && param.offset,
      take: param.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<PersonalClass>> {
    const result = await this.checkNotExist(id, PersonalClass.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updatePersonalClassDto: UpdatePersonalClassDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, PersonalClass.name);
    await this.checkExist(['name', 'code'], updatePersonalClassDto, id);
    const result = await this.repository.save({
      id,
      ...updatePersonalClassDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<PersonalClass>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<PersonalClass>(results, ids);
  }
}
