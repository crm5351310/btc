import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { CreateHealthDto, FindManyHealthParamDto, UpdateHealthDto } from './health.dto';
import { Health } from './health.entity';
import { BaseCategoryService } from "../../../common/base/service/base-category.service";

@Injectable()
export class HealthService extends BaseCategoryService<Health> {
  constructor(
    @InjectRepository(Health)
    repository: Repository<Health>,
  ) {
    super(repository);
  }
  async create(createHealthDto: CreateHealthDto) {
    await this.checkExist(['code', 'name'], createHealthDto);
    const Health: Health = await this.repository.save(createHealthDto);

    return new ResponseCreated(Health);
  }

  async findAll(param: FindManyHealthParamDto) {
    const [results, total] = await this.repository.findAndCount({
      skip: param.limit && param.offset,
      take: param.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Health>> {
    const result = await this.checkNotExist(id, Health.name);

    return new ResponseFindOne(result);
  }

  async update(id: number, updateHealthDto: UpdateHealthDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Health.name);
    await this.checkExist(['name', 'code'], updateHealthDto, id);
    const result = await this.repository.save({
      id,
      ...updateHealthDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Health>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
