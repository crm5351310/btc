import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import { BaseEntity } from '../../../common/base/entity/base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class Health extends BaseEntity {
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  description: string;
}
