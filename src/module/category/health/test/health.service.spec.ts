import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { CreateHealthDto, FindManyHealthParamDto, UpdateHealthDto } from '../health.dto';
import { Health } from '../health.entity';
import { HealthModule } from '../health.module';
import { HealthService } from '../health.service';

// npm run test:run src/module/category/health
describe('HealthService', () => {
  let healthService: HealthService;
  let healthRepository: Repository<Health>;

  const HEALTH_REPOSITORY_TOKEN = getRepositoryToken(Health);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HealthModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    healthService = module.get<HealthService>(HealthService);
    healthRepository = module.get(HEALTH_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(healthService).toBeDefined();
    expect(healthRepository).toBeDefined();
  });

  describe('HealthService => create', () => {
    const payload = {
      code: 'code',
      name: 'name',
      description: 'Mô tả',
    } as CreateHealthDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Health>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Health>;

    it('Tạo mới sức khoẻ thành công', async () => {
      jest.spyOn(healthRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(healthRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Health);

      const result = await healthService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới sức khoẻ không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(healthRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as Health);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as Health);
        return Promise.resolve(null);
      });

      await expect(healthService.create(payload)).rejects.toThrowError();
    });
  });

  describe('HealthService => findOne', () => {
    it('Tìm một sức khoẻ thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'sức khoẻ số 1',
        description: 'Mô tả',
      } as Health;
      jest.spyOn(healthRepository, 'findOne').mockResolvedValue(mockData);

      const result = await healthService.findOne({ id: 1 });

      expect(healthRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một sức khoẻ nếu ID không tồn tại', async () => {
      jest.spyOn(healthRepository, 'findOne').mockResolvedValue(null);
      await expect(healthService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('HealthService => findAll', () => {
    it('Tìm danh sách sức khoẻ thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'sức khoẻ số 1',
        description: 'Mô tả',
      } as Health;
      jest.spyOn(healthRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await healthService.findAll({} as FindManyHealthParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('HealthService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'sức khoẻ số 1',
      description: 'Mô tả',
    } as UpdateHealthDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Health>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Health>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<Health>;

    it('Cập nhật sức khoẻ thành công', async () => {
      jest.spyOn(healthRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as Health);
        return Promise.resolve(null);
      });
      jest.spyOn(healthRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Health);

      const result = await healthService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật sức khoẻ không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(healthRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as Health);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as Health);
        return Promise.resolve(null);
      });
      jest.spyOn(healthRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Health);

      await expect(healthService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật sức khoẻ không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(healthRepository, 'findOne').mockResolvedValue({ id: 1 } as Health);
      jest.spyOn(healthRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Health);

      await expect(healthService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('HealthService => remove', () => {
    it('Xoá sức khoẻ thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'sức khoẻ số 1',
        description: 'Mô tả',
      } as Health;
      jest.spyOn(healthRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(healthRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await healthService.remove({ id: '1' });

      expect(healthRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá sức khoẻ không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(healthRepository, 'find').mockResolvedValue([]);
      await expect(healthService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
