import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateHealthDto, FindManyHealthParamDto, UpdateHealthDto } from './health.dto';
import { HealthService } from './health.service';

@Controller()
@ApiTags(TagEnum.HEALTH)
export class HealthController {
  constructor(private readonly healthService: HealthService) {}

  @Post()
  create(@Body() createHealthDto: CreateHealthDto) {
    return this.healthService.create(createHealthDto);
  }

  @Get()
  findAll(@Query() param: FindManyHealthParamDto) {
    return this.healthService.findAll(param);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.healthService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateHealthDto: UpdateHealthDto) {
    return this.healthService.update(id, updateHealthDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.healthService.remove(ids);
  }
}
