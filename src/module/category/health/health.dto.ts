import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { Health } from './health.entity';

export class CreateHealthDto extends OmitType(Health, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
]) {}

export class UpdateHealthDto extends PartialType(CreateHealthDto) {}

export class FindManyHealthParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
