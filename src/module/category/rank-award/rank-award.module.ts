import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RankAward } from './rank-award.entity';
import { RankAwardController } from './rank-award.controller';
import { RankAwardService } from './rank-award.service';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([RankAward])],
  controllers: [RankAwardController],
  providers: [RankAwardService],
  exports: [RankAwardService],
})
export class RankAwardModule {
  static readonly route: RouteTree = {
    path: 'rank-award',
    module: RankAwardModule,
  };
}
