import { BaseCategoryService } from 'src/common/base/service/base-category.service';
import { RankAward } from './rank-award.entity';
import {
  ResponseCreated,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
} from 'src/common/response';
import { CreateRankAwardDto, FindManyRankAwardDto, UpdateRankAwardDto } from './rank-award.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';

@Injectable()
export class RankAwardService extends BaseCategoryService<RankAward> {
  constructor(
    @InjectRepository(RankAward)
    private readonly rankAwardRepository: Repository<RankAward>,
  ) {
    super(rankAwardRepository);
  }

  async create(dto: CreateRankAwardDto): Promise<ResponseCreated<RankAward>> {
    await this.checkExist(['name', 'code'], dto);

    const rankAward: RankAward = await this.rankAwardRepository.save(dto);

    return new ResponseCreated(rankAward);
  }

  async update(id: number, dto: UpdateRankAwardDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, RankAward.name);
    await this.checkExist(['name', 'code'], dto, id);
    const result = await this.rankAwardRepository.save({
      id,
      ...dto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<RankAward>> {
    const results = await this.rankAwardRepository.find({
      where: {
        id: In(ids),
      },
      relations: {
        emulationTitle: true,
      },
    });

    await this.rankAwardRepository.softRemove(results);
    return new ResponseDelete<RankAward>(results, ids);
  }

  async findAll(query: FindManyRankAwardDto): Promise<ResponseFindAll<RankAward>> {
    const [results, total] = await this.rankAwardRepository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<RankAward>> {
    const result = await this.checkNotExist(id, RankAward.name);

    return new ResponseFindOne(result);
  }
}
