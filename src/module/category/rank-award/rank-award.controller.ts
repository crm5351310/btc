import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { RankAwardService } from './rank-award.service';
import { CreateRankAwardDto, FindManyRankAwardDto, UpdateRankAwardDto } from './rank-award.dto';

@Controller()
@ApiTags(TagEnum.RANK_AWARD)
export class RankAwardController {
  constructor(private readonly rankAwardService: RankAwardService) {}

  @Post()
  create(@Body() createRankAwardDto: CreateRankAwardDto) {
    return this.rankAwardService.create(createRankAwardDto);
  }

  @Get()
  findAll(@Query() query: FindManyRankAwardDto) {
    return this.rankAwardService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.rankAwardService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRankAwardDto: UpdateRankAwardDto) {
    return this.rankAwardService.update(+id, updateRankAwardDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.rankAwardService.remove(ids);
  }
}
