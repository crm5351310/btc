import { OmitType, PartialType } from '@nestjs/swagger';
import { BaseQueryDto } from 'src/common/base/dto/base-query.dto';
import { RankAward } from './rank-award.entity';

export class CreateRankAwardDto extends OmitType(RankAward, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
]) {}

export class UpdateRankAwardDto extends PartialType(CreateRankAwardDto) {}

export class FindManyRankAwardDto extends BaseQueryDto {}
