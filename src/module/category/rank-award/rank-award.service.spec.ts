import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from 'src/common/response';
import { RankAwardService } from './rank-award.service';
import { RankAward } from './rank-award.entity';
import { CreateRankAwardDto, UpdateRankAwardDto } from './rank-award.dto';

describe('AwardLevelService', () => {
  let service: RankAwardService;
  let repository: Repository<RankAward>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RankAwardService,
        {
          provide: getRepositoryToken(RankAward),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<RankAwardService>(RankAwardService);
    repository = module.get(getRepositoryToken(RankAward));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        awards.find((value) => value.id === (options.where as FindOptionsWhere<RankAward>).id) ??
        null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return awards;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [awards, awards.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<RankAward>) => {
        return awards.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<RankAward>).code ||
            e.name == (options.where as FindOptionsWhere<RankAward>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: RankAward) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: awards.length,
        };
      }
    });
  });

  const awards = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
    {
      id: 3,
      code: 'code3',
      name: 'name3',
      description: 'mo ta 3',
    },
  ] as RankAward[];

  describe('AwardLevelService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateRankAwardDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
      } as CreateRankAwardDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('AwardLevelService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(awards.length);
    });
  });

  describe('AwardLevelService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 5;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('AwardLevelService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateRankAwardDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateRankAwardDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });

    it('Cập nhật không thành công do bản ghi không tồn tại', async () => {
      const id = 6;
      const payload = {
        code: 'code21231',
        name: 'name2312',
      } as UpdateRankAwardDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('AwardLevelService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3, 4, 5]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });

    it('Xóa không thành công do bản ghi không tồn tại', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      expect(service.remove([5])).rejects.toThrowError;
    });
  });
});
