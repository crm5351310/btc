import { Controller, Get, Param } from '@nestjs/common';
import { MinistryService } from './ministry.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.MINISTRY)
export class MinistryController {
  constructor(private readonly ministryService: MinistryService) {}

  @Get()
  findAll() {
    return this.ministryService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ministryService.findOne(+id);
  }
}
