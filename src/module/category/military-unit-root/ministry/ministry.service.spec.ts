import { Test, TestingModule } from '@nestjs/testing';
import { MinistryService } from './ministry.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Ministry } from './entities';
import { createStubInstance } from 'sinon';
import { Repository } from 'typeorm';

describe('MinistryService', () => {
  let service: MinistryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MinistryService,
        {
          provide: getRepositoryToken(Ministry),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<MinistryService>(MinistryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
