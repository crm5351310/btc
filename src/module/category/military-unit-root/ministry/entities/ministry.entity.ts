import { BaseCategoryEntity } from '../../../../../common/base/entity/base-category.entity';
import { Entity } from 'typeorm';

@Entity()
export class Ministry extends BaseCategoryEntity {}
