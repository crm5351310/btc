import { Module } from '@nestjs/common';
import { MinistryService } from './ministry.service';
import { MinistryController } from './ministry.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ministry } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([Ministry])],
  controllers: [MinistryController],
  providers: [MinistryService],
})
export class MinistryModule {
  static readonly route: RouteTree = {
    path: 'ministry',
    module: MinistryModule,
  };
}
