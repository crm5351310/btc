import { Injectable } from '@nestjs/common';
import { Ministry } from './entities';
import { ResponseFindAll, ResponseFindOne } from '../../../../common/response';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MinistryService {
  constructor(
    @InjectRepository(Ministry)
    private repository: Repository<Ministry>,
  ) {}

  async findAll(): Promise<ResponseFindAll<Ministry>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Ministry>> {
    const result = await this.repository.findOneBy({ id });
    return new ResponseFindOne(result);
  }
}
