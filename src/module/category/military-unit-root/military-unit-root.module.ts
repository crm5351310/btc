import { Module } from '@nestjs/common';
import { MinistryModule } from './ministry/ministry.module';
import { RouteTree } from '@nestjs/core';
import { MilitaryUnitClassificationModule } from './military-unit-classification/military-unit-classification.module';
import { MilitaryUnitModule } from './military-unit/military-unit.module';

@Module({
  imports: [MinistryModule, MilitaryUnitClassificationModule, MilitaryUnitModule],
})
export class MilitaryUnitRootModule {
  static readonly route: RouteTree = {
    path: 'military-unit-root',
    module: MilitaryUnitRootModule,
    children: [
      MinistryModule.route,
      MilitaryUnitClassificationModule.route,
      MilitaryUnitModule.route,
    ],
  };
}
