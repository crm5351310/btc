import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MilitaryUnitClassification } from './entities';
import { Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from '../../../../common/response';
import { QueryMilitaryUnitClassification } from './dto/query_military_unit_classification.dto';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';

@Injectable()
export class MilitaryUnitClassificationService {
  constructor(
    @InjectRepository(MilitaryUnitClassification)
    private repository: Repository<MilitaryUnitClassification>,
  ) {}

  async findAll(
    query: QueryMilitaryUnitClassification,
  ): Promise<ResponseFindAll<MilitaryUnitClassification>> {
    const [results, total] = await this.repository.findAndCount({
      where: {
        ministry: {
          id: query.ministryId ?? undefined,
        },
      },
      relations: {
        ministry: true,
      },
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<MilitaryUnitClassification>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        ministry: true,
      },
    });

    if (!result) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        MilitaryUnitClassification.name,
        true,
      );
    }

    return new ResponseFindOne(result);
  }
}
