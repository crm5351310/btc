import { Module } from '@nestjs/common';
import { MilitaryUnitClassificationService } from './military-unit-classification.service';
import { MilitaryUnitClassificationController } from './military-unit-classification.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryUnitClassification } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryUnitClassification])],
  controllers: [MilitaryUnitClassificationController],
  providers: [MilitaryUnitClassificationService],
  exports: [MilitaryUnitClassificationService],
})
export class MilitaryUnitClassificationModule {
  static readonly route: RouteTree = {
    path: 'military-unit-classification',
    module: MilitaryUnitClassificationModule,
  };
}
