import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class QueryMilitaryUnitClassification {
  @ApiProperty({
    description: 'ID của bộ',
    required: false,
  })
  @IsOptional()
  ministryId?: number;
}
