import { Test, TestingModule } from '@nestjs/testing';
import { MilitaryUnitClassificationService } from './military-unit-classification.service';
import { MilitaryUnitClassification } from './entities';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { Repository } from 'typeorm';

describe('MilitaryUnitClassificationService', () => {
  let service: MilitaryUnitClassificationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryUnitClassificationService,
        {
          provide: getRepositoryToken(MilitaryUnitClassification),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<MilitaryUnitClassificationService>(MilitaryUnitClassificationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
