import { Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseCategoryEntity } from '../../../../../common/base/entity/base-category.entity';
import { Ministry } from '../../ministry/entities';
import { MilitaryUnit } from '../../military-unit/entities';

@Entity()
export class MilitaryUnitClassification extends BaseCategoryEntity {
  @ManyToOne(() => Ministry)
  ministry: Ministry;

  @OneToMany(() => MilitaryUnit, (militaryUnit) => militaryUnit.militaryUnitClassification)
  militaryUnit: MilitaryUnit[];
}
