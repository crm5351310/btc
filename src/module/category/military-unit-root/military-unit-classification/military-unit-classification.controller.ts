import { Controller, Get, Param, Query } from '@nestjs/common';
import { MilitaryUnitClassificationService } from './military-unit-classification.service';
import { QueryMilitaryUnitClassification } from './dto/query_military_unit_classification.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.MILITARY_UNIT_CLASSIFICATION)
export class MilitaryUnitClassificationController {
  constructor(
    private readonly militaryUnitClassificationService: MilitaryUnitClassificationService,
  ) {}

  @Get()
  findAll(@Query() query: QueryMilitaryUnitClassification) {
    return this.militaryUnitClassificationService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.militaryUnitClassificationService.findOne(+id);
  }
}
