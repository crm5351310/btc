import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { MilitaryUnitService } from './military-unit.service';
import { CreateMilitaryUnitDto } from './dto/create-military-unit.dto';
import { UpdateMilitaryUnitDto } from './dto/update-military-unit.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { QueryMilitaryUnitDto } from './dto/query-military-unit.dto';

@Controller()
@ApiTags(TagEnum.MILITARY_UNIT)
export class MilitaryUnitController {
  constructor(private readonly militaryUnitService: MilitaryUnitService) {}

  @Post()
  create(@Body() createMilitaryUnitDto: CreateMilitaryUnitDto) {
    return this.militaryUnitService.create(createMilitaryUnitDto);
  }

  @Get()
  findAll(@Query() query: QueryMilitaryUnitDto) {
    return this.militaryUnitService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.militaryUnitService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMilitaryUnitDto: UpdateMilitaryUnitDto) {
    return this.militaryUnitService.update(+id, updateMilitaryUnitDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryUnitService.remove(ids);
  }
}
