import { Test, TestingModule } from '@nestjs/testing';
import { MilitaryUnitService } from './military-unit.service';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { MilitaryUnit } from './entities';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { OrganizationScaleService } from '../../organization-scale/organization-scale.service';
import { AdministrativeUnitService } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { MilitaryUnitClassificationService } from '../military-unit-classification/military-unit-classification.service';
import { CreateMilitaryUnitDto } from './dto/create-military-unit.dto';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from 'src/common/response';
import { UpdateMilitaryUnitDto } from './dto/update-military-unit.dto';

describe('MilitaryUnitService', () => {
  let service: MilitaryUnitService;
  let repository: Repository<MilitaryUnit>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryUnitService,
        {
          provide: getRepositoryToken(MilitaryUnit),
          useValue: createStubInstance(Repository),
        },
        {
          provide: OrganizationScaleService,
          useValue: createStubInstance(OrganizationScaleService),
        },
        {
          provide: AdministrativeUnitService,
          useValue: createStubInstance(AdministrativeUnitService),
        },
        {
          provide: MilitaryUnitClassificationService,
          useValue: createStubInstance(MilitaryUnitClassificationService),
        },
      ],
    }).compile();

    service = module.get<MilitaryUnitService>(MilitaryUnitService);
    repository = module.get(getRepositoryToken(MilitaryUnit));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryUnits.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryUnit>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryUnits;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryUnits, militaryUnits.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitaryUnit>) => {
        return militaryUnits.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitaryUnit>).code ||
            e.name == (options.where as FindOptionsWhere<MilitaryUnit>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryUnit) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryUnits.length,
        };
      }
    });
  });

  const militaryUnits = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      militaryUnitClassification: {
        id: 1,
      },
      administrativeUnit: {
        id: 1,
      },
      organizationScale: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      militaryUnitClassification: {
        id: 2,
      },
      administrativeUnit: {
        id: 2,
      },
      organizationScale: {
        id: 2,
      },
    },
  ] as MilitaryUnit[];

  describe('create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        militaryUnitClassification: {
          id: 1,
        },
        administrativeUnit: {
          id: 1,
        },
        organizationScale: {
          id: 1,
        },
      } as CreateMilitaryUnitDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code',
        name: 'name1',
        description: 'Mô tả',
        militaryUnitClassification: {
          id: 1,
        },
        administrativeUnit: {
          id: 1,
        },
        organizationScale: {
          id: 1,
        },
      } as CreateMilitaryUnitDto;

      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryUnits.length);
    });
  });

  describe('findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitaryUnitDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitaryUnitDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('AcademicDegreeService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
