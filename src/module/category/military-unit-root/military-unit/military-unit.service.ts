import { Injectable } from '@nestjs/common';
import { CreateMilitaryUnitDto } from './dto/create-military-unit.dto';
import { UpdateMilitaryUnitDto } from './dto/update-military-unit.dto';
import { BaseCategoryService } from '../../../../common/base/service/base-category.service';
import { MilitaryUnit } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, In, MoreThan, Repository } from 'typeorm';
import { OrganizationScaleService } from '../../organization-scale/organization-scale.service';
import { AdministrativeUnitService } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { MilitaryUnitClassificationService } from '../military-unit-classification/military-unit-classification.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../../common/response';
import { QueryMilitaryUnitDto } from './dto/query-military-unit.dto';
import { QueryMilitaryRecruitTargetDto } from '../../../../module/militaty-recruitment/military-recruit-target-root/military-recruit-target/dto/query-military-recruit-target.dto';

@Injectable()
export class MilitaryUnitService extends BaseCategoryService<MilitaryUnit> {
  constructor(
    @InjectRepository(MilitaryUnit)
    repository: Repository<MilitaryUnit>,
    private organizationScaleService: OrganizationScaleService,
    private administrativeUnitService: AdministrativeUnitService,
    private militaryUnitClassificationService: MilitaryUnitClassificationService,
  ) {
    super(repository);
  }

  async create(
    createMilitaryUnitDto: CreateMilitaryUnitDto,
  ): Promise<ResponseCreated<MilitaryUnit>> {
    await this.checkExist(['name', 'code'], createMilitaryUnitDto);

    // kiểm tra bản ghi quy mô tổ chức có tồn tại không
    await this.organizationScaleService.findOne(createMilitaryUnitDto.organizationScale.id);

    // kiểm tra bản ghi đơn vị hành chính có tồn tại không
    await this.administrativeUnitService.findOne(createMilitaryUnitDto.administrativeUnit);

    // kiểm tra bản ghi phân cấp đơn vị có tồn tại không
    await this.militaryUnitClassificationService.findOne(
      createMilitaryUnitDto.militaryUnitClassification.id,
    );

    const militaryUnit: MilitaryUnit = await this.repository.save(createMilitaryUnitDto);

    return new ResponseCreated(militaryUnit);
  }

  async findAll(query: QueryMilitaryUnitDto): Promise<ResponseFindAll<MilitaryUnit>> {
    const [results, total] = await this.repository.findAndCount({
      where: {
        militaryUnitClassification: {
          id: query.militaryUnitClassificationId ?? undefined,
        },
      },
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<MilitaryUnit>> {
    await this.checkNotExist(id, MilitaryUnit.name);

    const result = await this.repository.findOne({
      where: { id },
      relations: {
        administrativeUnit: {
          administrativeTitle: {
            administrativeLevel: true,
          },
          parent: true,
        },
        organizationScale: true,
        militaryUnitClassification: true,
      },
    });

    return new ResponseFindOne(result);
  }

  async update(id: number, updateMilitaryUnitDto: UpdateMilitaryUnitDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitaryUnit.name);
    await this.checkExist(['name', 'code'], updateMilitaryUnitDto, id);

    // kiểm tra bản ghi quy mô tổ chức có tồn tại không
    if (updateMilitaryUnitDto.organizationScale) {
      await this.organizationScaleService.findOne(updateMilitaryUnitDto.organizationScale.id);
    }

    // kiểm tra bản ghi đơn vị hành chính có tồn tại không
    if (updateMilitaryUnitDto.administrativeUnit) {
      await this.administrativeUnitService.findOne(updateMilitaryUnitDto.administrativeUnit);
    }

    // kiểm tra bản ghi đơn vị hành chính có tồn tại không
    if (updateMilitaryUnitDto.militaryUnitClassification) {
      await this.militaryUnitClassificationService.findOne(
        updateMilitaryUnitDto.militaryUnitClassification.id,
      );
    }

    const result = await this.repository.save({
      id,
      ...updateMilitaryUnitDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<MilitaryUnit>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<MilitaryUnit>(results, ids);
  }

  async findAllMilitaryRecruitmentTarget(dto: QueryMilitaryRecruitTargetDto) {
    const [results, total] = await this.repository.findAndCount({
      relations: {
        militaryRecruitTarget: {
          administrativeUnit: true,
          militaryRecruitSubTarget: {
            administrativeUnit: true,
          },
        },
      },
      order: {
        militaryRecruitTarget: {
          id: 'DESC',
        },
      },
      where: {
        name: ILike(`%${dto.militaryUnitName ?? ''}%`),
        militaryRecruitTarget: {
          militaryRecruitmentPeriod: {
            id: dto.militaryRecruitmentPeriodId,
            year: dto.year,
          },
          administrativeUnit: {
            id: dto.administrativeUnitId,
            parent: {
              id: 1,
            },
          },
        },
      },
    });
    return new ResponseFindAll(results, total);
  }
}
