import { OmitType } from '@nestjs/swagger';
import { MilitaryUnit } from '../entities';

export class CreateMilitaryUnitDto extends OmitType(MilitaryUnit, ['id']) {}
