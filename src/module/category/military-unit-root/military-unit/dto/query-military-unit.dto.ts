import { ApiProperty } from '@nestjs/swagger';
import { BaseQueryDto } from '../../../../../common/base/dto/base-query.dto';
import { IsOptional } from 'class-validator';

export class QueryMilitaryUnitDto extends BaseQueryDto {
  @ApiProperty({
    description: 'Id phân cấp đơn vị',
    required: false,
  })
  @IsOptional()
  militaryUnitClassificationId?: number;
}
