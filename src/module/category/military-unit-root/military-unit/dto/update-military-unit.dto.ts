import { PartialType } from '@nestjs/swagger';
import { CreateMilitaryUnitDto } from './create-military-unit.dto';

export class UpdateMilitaryUnitDto extends PartialType(CreateMilitaryUnitDto) {}
