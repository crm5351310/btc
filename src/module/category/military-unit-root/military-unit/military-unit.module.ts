import { Module } from '@nestjs/common';
import { MilitaryUnitService } from './military-unit.service';
import { MilitaryUnitController } from './military-unit.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryUnit } from './entities';
import { MilitaryUnitClassificationModule } from '../military-unit-classification/military-unit-classification.module';
import { OrganizationScaleModule } from '../../organization-scale/organization-scale.module';
import { AdministrativeUnitModule } from '../../administrative/administrative-unit-root/administrative-unit/administrative-unit.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MilitaryUnit]),
    MilitaryUnitClassificationModule,
    OrganizationScaleModule,
    AdministrativeUnitModule,
  ],
  controllers: [MilitaryUnitController],
  providers: [MilitaryUnitService],
  exports: [MilitaryUnitService],
})
export class MilitaryUnitModule {
  static readonly route: RouteTree = {
    path: 'military-unit',
    module: MilitaryUnitModule,
  };
}
