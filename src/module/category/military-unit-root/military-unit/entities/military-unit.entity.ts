import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseCategoryEntity } from '../../../../../common/base/entity/base-category.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsDefined, IsOptional, MaxLength, ValidateNested } from 'class-validator';
import { OrganizationScale } from '../../../../../module/category/organization-scale/entities';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { Type } from 'class-transformer';
import { AdministrativeUnit } from '../../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { MilitaryUnitClassification } from '../../military-unit-classification/entities';
import { MilitaryRecruitTarget } from '../../../../../module/militaty-recruitment/military-recruit-target-root/military-recruit-target/entities/military-recruit-target.entity';

@Entity()
export class MilitaryUnit extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;

  // swagger
  @ApiProperty({
    description: 'Phân hiệu đơn vị',
    default: 'Phân hiệu 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  symbol?: string;

  //swagger
  @ApiProperty({
    description: 'Quy mô tổ chức đơn vị quân đội',
    type: RelationTypeBase,
  })
  // validator
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(OrganizationScale, ['id']))
  //orm
  @ManyToOne(() => OrganizationScale)
  organizationScale: OrganizationScale;

  //swagger
  @ApiProperty({
    description: 'Đơn vị hành chính trực thuộc',
    type: RelationTypeBase,
  })
  // validator
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  //orm
  @ManyToOne(() => AdministrativeUnit, (administrativeUnit) => administrativeUnit.militaryUnits)
  administrativeUnit: AdministrativeUnit;

  //swagger
  @ApiProperty({
    description: 'Phân cấp đơn vị quân đội',
    type: RelationTypeBase,
  })
  // validator
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(MilitaryUnitClassification, ['id']))
  //orm
  @ManyToOne(
    () => MilitaryUnitClassification,
    (militaryUnitClassification) => militaryUnitClassification.militaryUnit,
  )
  militaryUnitClassification: MilitaryUnitClassification;

  @OneToMany(
    () => MilitaryRecruitTarget,
    (militaryRecruitTarget) => militaryRecruitTarget.militaryUnit,
  )
  militaryRecruitTarget: MilitaryRecruitTarget[];
}
