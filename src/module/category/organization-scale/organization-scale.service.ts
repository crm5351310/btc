import { Injectable } from '@nestjs/common';
import { CreateOrganizationScaleDto } from './dto/create-organization-scale.dto';
import { UpdateOrganizationScaleDto } from './dto/update-organization-scale.dto';
import { QueryOrganizationScale } from './dto/query-organization-scale.dto';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import { In, Repository } from 'typeorm';
import { OrganizationScale } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';

@Injectable()
export class OrganizationScaleService extends BaseCategoryService<OrganizationScale> {
  constructor(
    @InjectRepository(OrganizationScale)
    repository: Repository<OrganizationScale>,
  ) {
    super(repository);
  }

  async create(
    createOrganizationScaleDto: CreateOrganizationScaleDto,
  ): Promise<ResponseCreated<OrganizationScale>> {
    await this.checkExist(['name', 'code'], createOrganizationScaleDto);

    const organizationScale: OrganizationScale = await this.repository.save(
      createOrganizationScaleDto,
    );

    return new ResponseCreated(organizationScale);
  }

  async findAll(query: QueryOrganizationScale): Promise<ResponseFindAll<OrganizationScale>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<OrganizationScale>> {
    const result = await this.checkNotExist(id, OrganizationScale.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateOrganizationScaleDto: UpdateOrganizationScaleDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, OrganizationScale.name);
    await this.checkExist(['name', 'code'], updateOrganizationScaleDto, id);
    const result = await this.repository.save({
      id,
      ...updateOrganizationScaleDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<OrganizationScale>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<OrganizationScale>(results, ids);
  }
}
