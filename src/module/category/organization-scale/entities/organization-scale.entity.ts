import { Column, Entity, OneToMany } from 'typeorm';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MaxLength } from 'class-validator';
import { MilitiaDefenseUnit } from '../../militia-defense-unit/militia-defense-unit.entity';
// import { MilitaryUnit } from "../../military-unit-root/military-unit/entities";

@Entity()
export class OrganizationScale extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;

  @OneToMany(() => MilitiaDefenseUnit, (militiaDefenseUnit) => militiaDefenseUnit.organizationScale)
  militiaDefenseUnits: MilitiaDefenseUnit[];

  // @OneToMany(() => MilitaryUnit, militaryUnit => militaryUnit.organizationScale)
  // militaryUnits: MilitaryUnit[];
}
