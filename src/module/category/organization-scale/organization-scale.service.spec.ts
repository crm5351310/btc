import { Test, TestingModule } from '@nestjs/testing';
import { OrganizationScaleService } from './organization-scale.service';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { OrganizationScale } from './entities';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { CreateOrganizationScaleDto } from './dto/create-organization-scale.dto';
import { UpdateOrganizationScaleDto } from './dto/update-organization-scale.dto';

describe('OrganizationScaleService', () => {
  let service: OrganizationScaleService;
  let repository: Repository<OrganizationScale>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrganizationScaleService,
        {
          provide: getRepositoryToken(OrganizationScale),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<OrganizationScaleService>(OrganizationScaleService);
    repository = module.get(getRepositoryToken(OrganizationScale));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        organizationScales.find(
          (value) => value.id === (options.where as FindOptionsWhere<OrganizationScale>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return organizationScales;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [organizationScales, organizationScales.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<OrganizationScale>) => {
        return organizationScales.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<OrganizationScale>).code ||
            e.name == (options.where as FindOptionsWhere<OrganizationScale>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: OrganizationScale) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: organizationScales.length,
        };
      }
    });
  });

  const organizationScales = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as OrganizationScale[];

  describe('OrganizationScaleService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateOrganizationScaleDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code1',
        name: 'tên',
        description: 'Mô tả',
      } as CreateOrganizationScaleDto;

      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('OrganizationScaleService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(organizationScales.length);
    });
  });

  describe('OrganizationScaleService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('OrganizationScaleService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateOrganizationScaleDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name2',
      } as UpdateOrganizationScaleDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('OrganizationScaleService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
