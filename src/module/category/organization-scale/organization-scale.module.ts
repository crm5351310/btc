import { Module } from '@nestjs/common';
import { OrganizationScaleService } from './organization-scale.service';
import { OrganizationScaleController } from './organization-scale.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganizationScale } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([OrganizationScale])],
  controllers: [OrganizationScaleController],
  providers: [OrganizationScaleService],
  exports: [OrganizationScaleService],
})
export class OrganizationScaleModule {
  static readonly route: RouteTree = {
    path: 'organization-scale',
    module: OrganizationScaleModule,
  };
}
