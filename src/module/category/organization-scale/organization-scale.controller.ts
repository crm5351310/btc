import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { OrganizationScaleService } from './organization-scale.service';
import { CreateOrganizationScaleDto } from './dto/create-organization-scale.dto';
import { UpdateOrganizationScaleDto } from './dto/update-organization-scale.dto';
import { QueryOrganizationScale } from './dto/query-organization-scale.dto';
import { TagEnum } from '../../../common/enum/tag.enum';
import { ApiTags } from '@nestjs/swagger';

@Controller()
@ApiTags(TagEnum.ORGANIZATION_SCALE)
export class OrganizationScaleController {
  constructor(private readonly organizationScaleService: OrganizationScaleService) {}

  @Post()
  create(@Body() createOrganizationScaleDto: CreateOrganizationScaleDto) {
    return this.organizationScaleService.create(createOrganizationScaleDto);
  }

  @Get()
  findAll(@Query() query: QueryOrganizationScale) {
    return this.organizationScaleService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.organizationScaleService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrganizationScaleDto: UpdateOrganizationScaleDto) {
    return this.organizationScaleService.update(+id, updateOrganizationScaleDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.organizationScaleService.remove(ids);
  }
}
