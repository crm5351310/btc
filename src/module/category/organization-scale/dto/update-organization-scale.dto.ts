import { PartialType } from '@nestjs/swagger';
import { CreateOrganizationScaleDto } from './create-organization-scale.dto';

export class UpdateOrganizationScaleDto extends PartialType(CreateOrganizationScaleDto) {}
