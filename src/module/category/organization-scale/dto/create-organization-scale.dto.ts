import { OmitType } from '@nestjs/swagger';
import { OrganizationScale } from '../entities';

export class CreateOrganizationScaleDto extends OmitType(OrganizationScale, ['id'] as const) {}
