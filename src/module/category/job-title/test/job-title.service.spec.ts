// npm run test:run src/module/category/job-title
import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import { Repository } from 'typeorm';
import { JobTitleService } from '../job-title.service';
import { JobTitle } from '../job-title.entity';
import { JobTitleModule } from '../job-title.module';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';

describe('JobTitleService', () => {
  let module: TestingModule;
  let jobTitleService: JobTitleService;
  let jobTitleRepository: Repository<JobTitle>;

  const JOB_TITLE_REPOSITORY_TOKEN = getRepositoryToken(JobTitle);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        JobTitleModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    jobTitleRepository = module.get(JOB_TITLE_REPOSITORY_TOKEN);
    jobTitleService = module.get<JobTitleService>(JobTitleService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('JobTitleService => create', () => {
    const payload = {
      name: 'tenten',
      code: 'te2',
    } as JobTitle;

    it('Tạo mới chức danh thành công', async () => {
      jest.spyOn(jobTitleRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(jobTitleRepository, 'save').mockResolvedValue({ ...payload } as JobTitle);

      const result = await jobTitleService.create(payload);

      expect(jobTitleRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('JobTitleService => findOne', () => {
    it('Tìm một chức danh thành công', async () => {
      const data = new JobTitle();
      data.id = 1;
      data.name = 'h';
      data.code = '3';
      jest.spyOn(jobTitleRepository, 'findOne').mockResolvedValue(data);

      const result = await jobTitleService.findOne({ id: 1 } as PathDto);

      expect(jobTitleRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(data);
    });

    it('Không thể tìm một chức danh nếu ID không tồn tại', async () => {
      jest.spyOn(jobTitleRepository, 'findOne').mockResolvedValue(null);
      await expect(jobTitleService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('JobTitleService => findAll', () => {
    it('Tìm danh sách chức danh thành công', async () => {
      const mockData = [
        { id: 1, name: 'user1' },
        { id: 2, name: 'user2' },
        { id: 3, name: 'user2' },
      ] as JobTitle[];
      jest.spyOn(jobTitleRepository, 'findAndCount').mockResolvedValue([mockData, mockData.length]);

      const result = await jobTitleService.findAll({} as any);

      expect(jobTitleRepository.findAndCount).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('JobTitleService => remove', () => {
    it('Xoá một chức danh thành công', async () => {
      const mockData = { id: 1 } as JobTitle;
      jest.spyOn(jobTitleRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(jobTitleRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await jobTitleService.remove({ id: '1' } as PathArrayDto);

      expect(jobTitleRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một chức danh nếu ID không tồn tại', async () => {
      jest.spyOn(jobTitleRepository, 'find').mockResolvedValue([]);
      await expect(jobTitleService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
