import { CreateJobTitleDto } from './create-job-title.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateJobTitleDto extends PartialType(CreateJobTitleDto) {}
