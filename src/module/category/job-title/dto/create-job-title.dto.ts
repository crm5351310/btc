import { OmitType } from '@nestjs/swagger';
import { JobTitle } from '../job-title.entity';

export class CreateJobTitleDto extends OmitType(JobTitle, ['id'] as const) {}
