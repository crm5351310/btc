import { Module } from '@nestjs/common';
import { JobTitleService } from './job-title.service';
import { JobTitleController } from './job-title.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobTitle } from './job-title.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([JobTitle])],
  controllers: [JobTitleController],
  providers: [JobTitleService],
  exports: [JobTitleService],
})
export class JobTitleModule {}
export const jobTitleRouter: RouteTree = {
  path: 'job-title',
  module: JobTitleModule,
  children: [],
};
