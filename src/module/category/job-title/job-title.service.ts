import { Injectable } from '@nestjs/common';
import { CreateJobTitleDto } from './dto/create-job-title.dto';
import { UpdateJobTitleDto } from './dto/update-job-title.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../common/message/subject.message';
import { JobTitle } from './job-title.entity';
import { BaseMessage } from '../../../common/message/base.message';
import { FieldMessage } from '../../../common/message/field.message';
import { BaseInterfaceService } from '../../../common/interface/base.interface';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { Pagination } from '../../../common/utils/pagination.util';
import { Job } from '../job/job.entity';
import { ParamJobTitleDto } from './dto/param-job-title.dto';

@Injectable()
export class JobTitleService extends BaseService {
  constructor(
    @InjectRepository(JobTitle)
    private readonly jobTitleRepository: Repository<JobTitle>,
  ) {
    super();
    this.name = JobTitleService.name;
    this.subject = SubjectMessage.JOB_TITLE;
  }
  async create(createJobTitleDto: CreateJobTitleDto) {
    this.action = BaseMessage.CREATE;
    await this.dubField(FieldMessage.NAME, createJobTitleDto.name);
    await this.dubField(FieldMessage.CODE, createJobTitleDto.code);

    return this.response(await this.jobTitleRepository.save(createJobTitleDto));
  }

  async findAll(param: ParamJobTitleDto) {
    this.action = BaseMessage.READ;
    const job = await this.jobTitleRepository.findAndCount({
      skip: param.limit && param.offset,
      take: param.limit,
      order: {
        code: 'ASC',
      },
    });
    return this.response(new Pagination<JobTitle>(job[0], job[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.jobTitleRepository.findOne({
      where: path,
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    return this.response(result);
  }

  async update(path: PathDto, updateJobTitleDto: UpdateJobTitleDto) {
    this.action = BaseMessage.UPDATE;
    const job = await this.jobTitleRepository.findOne({
      where: path,
    });
    if (!job) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    const nameDublicate = await this.jobTitleRepository.findOne({
      where: {
        name: updateJobTitleDto.name,
        id: Not(path.id),
      },
    });
    if (nameDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const codeDublicate = await this.jobTitleRepository.findOne({
      where: {
        code: updateJobTitleDto.code,
        id: Not(path.id),
      },
    });
    if (codeDublicate) {
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    return this.response(
      await this.jobTitleRepository.save({
        ...updateJobTitleDto,
        id: path.id,
      }),
    );
  }

  async remove(path: PathArrayDto) {
    const partArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const jobs = await this.jobTitleRepository.find({
      where: {
        id: In(partArr),
      },
    });
    if (jobs.length != partArr.length) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    await this.jobTitleRepository.softRemove(jobs);
    return this.response(path);
  }

  async dubField(
    key: string,
    value: string | number,
    condition?: BaseInterfaceService,
  ): Promise<void> {
    const dub = this.jobTitleRepository
      .createQueryBuilder('A')
      .where(`A.${key} = :value`, { value });

    if (condition) {
      let subQuery = `A.id = ${value}`;
      if (condition.isProp) {
        subQuery = subQuery + ` AND A.${condition.property} ${condition.condition} `;
      }
      dub.where(subQuery);
    }

    const result = await dub.getOne();
    if (key == 'id' && !result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    if (key != 'id' && result) {
      throw new CustomBadRequestException(
        ContentMessage.EXIST,
        FieldMessage[key.toUpperCase()],
        false,
      );
    }
  }
}
