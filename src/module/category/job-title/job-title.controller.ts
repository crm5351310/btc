import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { JobTitleService } from './job-title.service';
import { CreateJobTitleDto } from './dto/create-job-title.dto';
import { UpdateJobTitleDto } from './dto/update-job-title.dto';
import { PathArrayDto, PathDto } from '../../../common/base/class/base.class';
import { ParamJobTitleDto } from './dto/param-job-title.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.JOB_TITLE)
export class JobTitleController {
  constructor(private readonly jobTitleService: JobTitleService) {}

  @Post()
  create(@Body() createJobTitleDto: CreateJobTitleDto) {
    return this.jobTitleService.create(createJobTitleDto);
  }

  @Get()
  findAll(@Query() param: ParamJobTitleDto) {
    return this.jobTitleService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.jobTitleService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateJobTitleDto: UpdateJobTitleDto) {
    return this.jobTitleService.update(path, updateJobTitleDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.jobTitleService.remove(path);
  }
}
