import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { ReligionService } from './religion.service';
import { CreateReligionDto } from './dto/create-religion.dto';
import { UpdateReligionDto } from './dto/update-religion.dto';
import { PathDto } from '../../../common/base/class/base.class';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { ParamReligionDto } from './dto/param-religion.dto';

@Controller()
@ApiTags(TagEnum.RELIGION)
export class ReligionController {
  constructor(private readonly religionService: ReligionService) {}

  @Post()
  create(@Body() createReligionDto: CreateReligionDto) {
    return this.religionService.create(createReligionDto);
  }

  @Get()
  findAll(@Query() param: ParamReligionDto) {
    return this.religionService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.religionService.findOne(path.id);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateReligionDto: UpdateReligionDto) {
    return this.religionService.update(path.id, updateReligionDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.religionService.remove(ids);
  }
}
