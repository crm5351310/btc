import { Module } from '@nestjs/common';
import { ReligionService } from './religion.service';
import { ReligionController } from './religion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Religion } from './religion.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([Religion])],
  controllers: [ReligionController],
  providers: [ReligionService],
  exports: [ReligionService],
})
export class ReligionModule {}
export const religionRouter: RouteTree = {
  path: 'religion',
  module: ReligionModule,
  children: [],
};
