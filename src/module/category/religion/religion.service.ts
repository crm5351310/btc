import { Injectable } from '@nestjs/common';
import { CreateReligionDto } from './dto/create-religion.dto';
import { UpdateReligionDto } from './dto/update-religion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Religion } from './religion.entity';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { ParamReligionDto } from "./dto/param-religion.dto";

@Injectable()
export class ReligionService extends BaseCategoryService<Religion> {
  constructor(
    @InjectRepository(Religion)
    repository: Repository<Religion>,
  ) {
    super(repository);
  }
  async create(
    createReligionDto: CreateReligionDto,
  ): Promise<ResponseCreated<Religion>> {
    await this.checkExist(['name', 'code'], createReligionDto);

    const religion: Religion = await this.repository.save(createReligionDto);

    return new ResponseCreated(religion);
  }

  async findAll(query: ParamReligionDto): Promise<ResponseFindAll<Religion>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Religion>> {
    const result = await this.checkNotExist(id, Religion.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateReligionDto: UpdateReligionDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Religion.name);
    await this.checkExist(['name', 'code'], updateReligionDto, id);
    const result = await this.repository.save({
      id,
      ...updateReligionDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Religion>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<Religion>(results, ids);
  }
}
