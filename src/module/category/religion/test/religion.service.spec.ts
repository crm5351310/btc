// npm run test:run src/module/category/religion
import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import { Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { ReligionService } from '../religion.service';
import { Religion } from '../religion.entity';
import { ReligionModule } from '../religion.module';

describe('ReligionService', () => {
  let module: TestingModule;
  let religionService: ReligionService;
  let religionRepository: Repository<Religion>;

  const RELIGION_REPOSITORY_TOKEN = getRepositoryToken(Religion);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ReligionModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    religionRepository = module.get(RELIGION_REPOSITORY_TOKEN);
    religionService = module.get<ReligionService>(ReligionService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('ReligionService => create', () => {
    const payload = {
      name: 'tenten',
      code: 'te2',
    } as Religion;

    it('Tạo mới tôn giáo thành công', async () => {
      jest.spyOn(religionRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(religionRepository, 'save').mockResolvedValue({ ...payload } as Religion);

      const result = await religionService.create(payload);

      expect(religionRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('ReligionService => findOne', () => {
    it('Tìm một tôn giáo thành công', async () => {
      const data = new Religion();
      data.id = 1;
      data.name = 'h';
      data.code = '3';
      jest.spyOn(religionRepository, 'findOne').mockResolvedValue(data);

      const result = await religionService.findOne({ id: 1 } as PathDto);

      expect(religionRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(data);
    });

    it('Không thể tìm một tôn giáo nếu ID không tồn tại', async () => {
      jest.spyOn(religionRepository, 'findOne').mockResolvedValue(null);
      await expect(religionService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('ReligionService => findAll', () => {
    it('Tìm danh sách tôn giáo thành công', async () => {
      const mockData = [
        { id: 1, name: 'user1' },
        { id: 2, name: 'user2' },
        { id: 3, name: 'user2' },
      ] as Religion[];
      jest.spyOn(religionRepository, 'findAndCount').mockResolvedValue([mockData, mockData.length]);

      const result = await religionService.findAll({} as any);

      expect(religionRepository.findAndCount).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('ReligionService => remove', () => {
    it('Xoá một tôn giáo thành công', async () => {
      const mockData = { id: 1 } as Religion;
      jest.spyOn(religionRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(religionRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await religionService.remove({ id: '1' } as PathArrayDto);

      expect(religionRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một tôn giáo nếu ID không tồn tại', async () => {
      jest.spyOn(religionRepository, 'find').mockResolvedValue([]);
      await expect(religionService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
