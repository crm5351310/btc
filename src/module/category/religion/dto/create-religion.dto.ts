import { OmitType } from '@nestjs/swagger';
import { Religion } from '../religion.entity';

export class CreateReligionDto extends OmitType(Religion, ['id'] as const) {}
