import { CreateReligionDto } from './create-religion.dto';
import { PartialType } from '@nestjs/swagger';

export class UpdateReligionDto extends PartialType(CreateReligionDto) {}
