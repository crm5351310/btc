import { IntersectionType } from '@nestjs/swagger';
import { PaginationDto } from '../../../../common/base/class/base.class';

export class ParamReligionDto extends IntersectionType(PaginationDto) {}
