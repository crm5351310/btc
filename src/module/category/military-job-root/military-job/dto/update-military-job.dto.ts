import { PartialType } from '@nestjs/mapped-types';
import { CreateMilitaryJobDto } from './create-military-job.dto';

export class UpdateMilitaryJobDto extends PartialType(CreateMilitaryJobDto) {}
