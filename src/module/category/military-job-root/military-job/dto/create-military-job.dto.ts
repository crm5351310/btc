import { OmitType } from '@nestjs/swagger';
import { MilitaryJob } from '../entities/military-job.entity';

export class CreateMilitaryJobDto extends OmitType(MilitaryJob, ['id' as const]) {}
