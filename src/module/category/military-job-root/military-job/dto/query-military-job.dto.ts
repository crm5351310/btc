import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
export class QueryMilitaryJobDto {
  @ApiProperty({
    required: false,
    description: 'ID của nhóm ngành chuyên nghiệp quân sự',
  })
  @IsOptional()
  militaryJobGroupId?: number;
}
