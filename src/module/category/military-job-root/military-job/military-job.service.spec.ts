import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from 'src/common/response';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { MilitaryJobGroupService } from '../military-job-group/military-job-group.service';
import { MilitaryJobPositionService } from '../military-job-position/military-job-position.service';
import { TrainingClassificationService } from '../training-classification/training-classification.service';
import { CreateMilitaryJobDto } from './dto/create-military-job.dto';
import { UpdateMilitaryJobDto } from './dto/update-military-job.dto';
import { MilitaryJob } from './entities/military-job.entity';
import { MilitaryJobService } from './military-job.service';

describe('MilitaryJobService', () => {
  let service: MilitaryJobService;
  let repository: Repository<MilitaryJob>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryJobService,
        {
          provide: getRepositoryToken(MilitaryJob),
          useValue: createStubInstance(Repository),
        },
        {
          provide: MilitaryJobGroupService,
          useValue: createStubInstance(MilitaryJobGroupService),
        },
        {
          provide: TrainingClassificationService,
          useValue: createStubInstance(TrainingClassificationService),
        },
        {
          provide: MilitaryJobPositionService,
          useValue: createStubInstance(MilitaryJobPositionService),
        },
      ],
    }).compile();

    service = module.get<MilitaryJobService>(MilitaryJobService);
    repository = module.get(getRepositoryToken(MilitaryJob));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryJobGroup.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryJob>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryJobGroup;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryJobGroup, militaryJobGroup.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitaryJob>) => {
        return militaryJobGroup.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitaryJob>).code ||
            e.name == (options.where as FindOptionsWhere<MilitaryJob>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryJob) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryJobGroup.length,
        };
      }
    });
  });

  const militaryJobGroup = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      trainingClassification: {
        id: 1,
      },
      militaryJobGroup: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      trainingClassification: {
        id: 2,
      },
      militaryJobGroup: {
        id: 2,
      },
    },
  ] as MilitaryJob[];

  describe('create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        trainingClassification: {
          id: 1,
        },
        militaryJobGroup: {
          id: 1,
        },
      } as CreateMilitaryJobDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code2',
        name: 'name2',
        description: 'mo ta 2',
        trainingClassification: {
          id: 1,
        },
        militaryJobGroup: {
          id: 1,
        },
      } as CreateMilitaryJobDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({ militaryJobGroupId: 1 });
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryJobGroup.length);
    });
  });

  describe('findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 10;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitaryJobDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitaryJobDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
