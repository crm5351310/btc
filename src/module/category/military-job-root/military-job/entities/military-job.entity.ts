import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsNotEmpty, IsOptional, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { BaseCategoryEntity } from '../../../../../common/base/entity/base-category.entity';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { MilitaryJobGroup } from '../../military-job-group/entities/military-job-group.entity';
import { MilitaryJobPosition } from '../../military-job-position/entities/military-job-position.entity';
import { TrainingClassification } from '../../training-classification/entities/training-classification.entity';

@Entity()
export class MilitaryJob extends BaseCategoryEntity {
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  @ApiProperty({
    description: 'Phân loại đào tạo',
    type: RelationTypeBase,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => PickType(TrainingClassification, ['id']))
  @ManyToOne(
    () => TrainingClassification,
    (trainingClassification) => trainingClassification.militaryJob,
  )
  trainingClassification: TrainingClassification;

  @ApiProperty({
    description: 'Chuyên nghiệp quân sự - nhóm ngành',
    type: RelationTypeBase,
  })
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(MilitaryJobGroup, ['id']))
  @ManyToOne(() => MilitaryJobGroup, (militaryJobGroup) => militaryJobGroup.militaryJobs)
  militaryJobGroup: MilitaryJobGroup;

  @ApiProperty({
    description: 'Đã qua đào tạo',
    default: false,
    type: 'boolean',
  })
  @IsOptional()
  @Column('boolean', {
    nullable: false,
  })
  isTrained: boolean;

  @OneToMany(() => MilitaryJobPosition, (militaryJobPositions) => militaryJobPositions.militaryJob)
  militaryJobPositions: MilitaryJobPosition[];
}
