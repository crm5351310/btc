import { Module } from '@nestjs/common';
import { MilitaryJobService } from './military-job.service';
import { MilitaryJobController } from './military-job.controller';
import { MilitaryJob } from './entities/military-job.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryJob])],

  controllers: [MilitaryJobController],
  providers: [MilitaryJobService],
})
export class MilitaryJobModule {}
export const militaryJobRouter: RouteTree = {
  path: 'military-job',
  module: MilitaryJobModule,
};
