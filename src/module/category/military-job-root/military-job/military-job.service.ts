import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../../common/base/service/base-category.service';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import {
  ResponseCreated,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
} from '../../../../common/response';
import { DeepPartial, FindOptionsWhere, In, Not, Repository } from 'typeorm';
import { CreateMilitaryJobDto } from './dto/create-military-job.dto';
import { QueryMilitaryJobDto } from './dto/query-military-job.dto';
import { UpdateMilitaryJobDto } from './dto/update-military-job.dto';
import { MilitaryJob } from './entities/military-job.entity';

@Injectable()
export class MilitaryJobService extends BaseCategoryService<MilitaryJob> {
  constructor(
    @InjectRepository(MilitaryJob)
    responsitory: Repository<MilitaryJob>,
  ) {
    super(responsitory);
  }
  async create(dto: CreateMilitaryJobDto): Promise<ResponseCreated<MilitaryJob>> {
    await this.checkExist(['name', 'code'], dto);
    const militaryJob = await this.repository.save(dto);
    return new ResponseCreated(militaryJob);
  }
  async findAll(dto: QueryMilitaryJobDto): Promise<ResponseFindAll<MilitaryJob>> {
    const [results, total] = await this.repository.findAndCount({
      relations: {
        militaryJobGroup: true,
      },
      where: {
        militaryJobGroup: {
          id: dto.militaryJobGroupId,
        },
      },
    });
    return new ResponseFindAll(results, total);
  }
  async findOne(id: number): Promise<ResponseFindOne<MilitaryJob>> {
    const result = await this.repository.findOne({
      relations: {
        militaryJobGroup: true,
        militaryJobPositions: true,
        trainingClassification: true,
      },
      where: { id: id },
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, MilitaryJob.name, true);
    }

    return new ResponseFindOne(result);
  }
  async update(id: number, dto: UpdateMilitaryJobDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitaryJob.name);
    await this.checkExist(['name', 'code'], dto, id);
    const result = await this.repository.save({ id, ...dto });
    return new ResponseUpdate(result);
  }
  async remove(ids: number[]): Promise<ResponseDelete<MilitaryJob>> {
    const results = await this.repository.find({
      relations: {
        militaryJobPositions: true,
      },
      where: {
        id: In(ids),
      },
    });
    const rows = results.reduce((pre: MilitaryJob[], cur: MilitaryJob) => {
      if (!cur.militaryJobPositions || cur.militaryJobPositions?.length === 0) {
        pre.push(cur);
      }
      return pre;
    }, []);
    if (rows.length === 0) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, MilitaryJob.name, true);
    }
    await this.repository.softRemove(rows);
    return new ResponseDelete<MilitaryJob>(results, ids, rows);
  }
  protected override async checkExist(
    properties: (keyof MilitaryJob)[],
    dto: DeepPartial<MilitaryJob>,
    id?: number | undefined,
  ): Promise<void> {
    const errors: string[] = [];

    await Promise.all(
      properties.map(async (property) => {
        const isExist = await this.repository.exist({
          relations: {
            militaryJobGroup: true,
          },
          where: {
            militaryJobGroup: {
              id: dto.militaryJobGroup?.id,
            },
            id: id ? Not(id) : undefined,
            [property]: dto[property],
          } as FindOptionsWhere<MilitaryJob>,
        });
        if (isExist) {
          errors.push(`${property.toString()}.${ContentMessage.EXIST}`);
        }
      }),
    );

    if (errors.length > 0) {
      throw new CustomBadRequestExceptionCustom(errors);
    }
  }
}
