import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { MilitaryJobService } from './military-job.service';
import { CreateMilitaryJobDto } from './dto/create-military-job.dto';
import { UpdateMilitaryJobDto } from './dto/update-military-job.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { QueryMilitaryJobDto } from './dto/query-military-job.dto';
import { PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.MILITARY_JOB)
export class MilitaryJobController {
  constructor(private readonly militaryJobService: MilitaryJobService) {}

  @Post()
  create(@Body() createMilitaryJobDto: CreateMilitaryJobDto) {
    return this.militaryJobService.create(createMilitaryJobDto);
  }

  @Get()
  findAll(@Query() queryMilitaryJobDto: QueryMilitaryJobDto) {
    console.log(queryMilitaryJobDto);

    return this.militaryJobService.findAll(queryMilitaryJobDto);
  }

  @Get(':id')
  findOne(@Param() param: PathDto) {
    return this.militaryJobService.findOne(param.id);
  }

  @Patch(':id')
  update(@Param() param: PathDto, @Body() updateMilitaryJobDto: UpdateMilitaryJobDto) {
    return this.militaryJobService.update(param.id, updateMilitaryJobDto);
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryJobService.remove(ids);
  }
}
