import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { TypeReserveService } from './type-reserve.service';

@Controller()
@ApiTags(TagEnum.TYPE_RESERVE)
export class TypeReserveController {
  constructor(private readonly typeReserveService: TypeReserveService) {}
  @Get()
  findAll() {
    return this.typeReserveService.findAll();
  }
}
