import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '../../../../common/base/class/base.class';
import { ResponseFindAll } from '../../../../common/response';
import { Repository } from 'typeorm';
import { TypeReserve } from './entities/type-reserve.entity';

@Injectable()
export class TypeReserveService extends BaseService {
  constructor(
    @InjectRepository(TypeReserve)
    private readonly repository: Repository<TypeReserve>,
  ) {
    super();
  }
  async findAll(): Promise<ResponseFindAll<TypeReserve>> {
    const [result, total] = await this.repository.findAndCount();
    return new ResponseFindAll(result, total);
  }
}
