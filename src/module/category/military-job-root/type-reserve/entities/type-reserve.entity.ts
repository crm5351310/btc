import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { MilitaryJobGroup } from '../../military-job-group/entities/military-job-group.entity';

@Entity()
export class TypeReserve extends BaseEntity {
  @Column('varchar', { length: 25 })
  name: string;
  @Column('varchar', { length: 25 })
  code: string;

  @OneToMany(() => MilitaryJobGroup, (militaryJobGroup) => militaryJobGroup.typeReserve)
  militaryJobGroups: MilitaryJobGroup[];
}
