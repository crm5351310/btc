import { Module } from '@nestjs/common';
import { TypeReserveService } from './type-reserve.service';
import { TypeReserveController } from './type-reserve.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeReserve } from './entities/type-reserve.entity';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([TypeReserve])],
  controllers: [TypeReserveController],
  providers: [TypeReserveService],
})
export class TypeReserveModule {}

export const typeReserveRouter: RouteTree = {
  path: 'type-reserve',
  module: TypeReserveModule,
};
