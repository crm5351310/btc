import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from 'src/common/response';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { MilitaryJobService } from '../military-job/military-job.service';
import { CreateMilitaryJobPositionDto } from './dto/create-military-job-position.dto';
import { UpdateMilitaryJobPositionDto } from './dto/update-military-job-position.dto';
import { MilitaryJobPosition } from './entities/military-job-position.entity';
import { MilitaryJobPositionService } from './military-job-position.service';

describe('MilitaryJobPositionService', () => {
  let service: MilitaryJobPositionService;
  let repository: Repository<MilitaryJobPosition>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryJobPositionService,
        {
          provide: getRepositoryToken(MilitaryJobPosition),
          useValue: createStubInstance(Repository),
        },
        {
          provide: MilitaryJobService,
          useValue: createStubInstance(MilitaryJobService),
        },
      ],
    }).compile();

    service = module.get<MilitaryJobPositionService>(MilitaryJobPositionService);
    repository = module.get(getRepositoryToken(MilitaryJobPosition));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryJobGroup.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryJobPosition>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryJobGroup;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryJobGroup, militaryJobGroup.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitaryJobPosition>) => {
        return militaryJobGroup.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitaryJobPosition>).code ||
            e.name == (options.where as FindOptionsWhere<MilitaryJobPosition>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryJobPosition) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryJobGroup.length,
        };
      }
    });
  });

  const militaryJobGroup = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      militaryJob: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      militaryJob: {
        id: 2,
      },
    },
  ] as MilitaryJobPosition[];

  describe('create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        militaryJob: {
          id: 1,
        },
      } as CreateMilitaryJobPositionDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code2',
        name: 'name2',
        description: 'mo ta 2',
        militaryJob: {
          id: 1,
        },
      } as CreateMilitaryJobPositionDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({ militaryJobId: 1 });
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryJobGroup.length);
    });
  });

  describe('findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 10;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitaryJobPositionDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitaryJobPositionDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
