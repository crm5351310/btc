import { Module } from '@nestjs/common';
import { MilitaryJobPositionService } from './military-job-position.service';
import { MilitaryJobPositionController } from './military-job-position.controller';
import { MilitaryJobPosition } from './entities/military-job-position.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryJobPosition])],
  controllers: [MilitaryJobPositionController],
  providers: [MilitaryJobPositionService],
  exports: [MilitaryJobPositionService],
})
export class MilitaryJobPositionModule {}
export const militaryJobPositionRoute: RouteTree = {
  path: 'military-job-position',
  module: MilitaryJobPositionModule,
};
