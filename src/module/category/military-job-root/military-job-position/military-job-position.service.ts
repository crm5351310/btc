import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from 'src/common/base/service/base-category.service';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import {
  ResponseCreated,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
} from 'src/common/response';
import { Repository, In, DeepPartial, FindOptionsWhere, Not } from 'typeorm';
import { CreateMilitaryJobPositionDto } from './dto/create-military-job-position.dto';
import { QueryMilitaryJobPositionDto } from './dto/query-military-job-position.dto';
import { UpdateMilitaryJobPositionDto } from './dto/update-military-job-position.dto';
import { MilitaryJobPosition } from './entities/military-job-position.entity';

@Injectable()
export class MilitaryJobPositionService extends BaseCategoryService<MilitaryJobPosition> {
  constructor(
    @InjectRepository(MilitaryJobPosition)
    responsitory: Repository<MilitaryJobPosition>,
  ) {
    super(responsitory);
  }
  async create(dto: CreateMilitaryJobPositionDto): Promise<ResponseCreated<MilitaryJobPosition>> {
    await this.checkExist(['name', 'code'], dto);
    const militaryJob = await this.repository.save(dto);
    return new ResponseCreated(militaryJob);
  }
  async findAll(dto: QueryMilitaryJobPositionDto): Promise<ResponseFindAll<MilitaryJobPosition>> {
    const [results, total] = await this.repository.findAndCount({
      relations: {
        militaryJob: true,
      },
      where: {
        militaryJob: {
          id: dto.militaryJobId,
        },
      },
    });
    return new ResponseFindAll(results, total);
  }
  async findOne(id: number): Promise<ResponseFindOne<MilitaryJobPosition>> {
    const result = await this.repository.findOne({
      relations: {
        militaryJob: true,
      },
      where: { id: id },
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, MilitaryJobPosition.name, true);
    }

    return new ResponseFindOne(result);
  }
  async update(id: number, dto: UpdateMilitaryJobPositionDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitaryJobPosition.name);
    await this.checkExist(['name', 'code'], dto, id);
    const result = await this.repository.save({ id, ...dto });
    return new ResponseUpdate(result);
  }
  async remove(ids: number[]): Promise<ResponseDelete<MilitaryJobPosition>> {
    const results = await this.repository.find({
      relations: {
        militaryJob: true,
      },
      where: {
        id: In(ids),
      },
    });
    if (results.length === 0) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, MilitaryJobPosition.name, true);
    }
    await this.repository.softRemove(results);
    return new ResponseDelete<MilitaryJobPosition>(results, ids);
  }

  protected override async checkExist(
    properties: (keyof MilitaryJobPosition)[],
    dto: DeepPartial<MilitaryJobPosition>,
    id?: number | undefined,
  ): Promise<void> {
    const errors: string[] = [];

    await Promise.all(
      properties.map(async (property) => {
        const isExist = await this.repository.exist({
          relations: {
            militaryJob: true,
          },
          where: {
            militaryJob: {
              id: dto.militaryJob?.id,
            },
            id: id ? Not(id) : undefined,
            [property]: dto[property],
          } as FindOptionsWhere<MilitaryJobPosition>,
        });
        if (isExist) {
          errors.push(`${property.toString()}.${ContentMessage.EXIST}`);
        }
      }),
    );

    if (errors.length > 0) {
      throw new CustomBadRequestExceptionCustom(errors);
    }
  }
}
