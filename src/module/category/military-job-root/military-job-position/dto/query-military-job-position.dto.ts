import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
export class QueryMilitaryJobPositionDto {
  @ApiProperty({
    required: false,
    description: 'ID của ngành chuyên nghiệp quân sự',
  })
  @IsOptional()
  militaryJobId?: number;
}
