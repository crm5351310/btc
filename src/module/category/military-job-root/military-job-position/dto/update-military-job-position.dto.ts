import { PartialType } from '@nestjs/mapped-types';
import { CreateMilitaryJobPositionDto } from './create-military-job-position.dto';

export class UpdateMilitaryJobPositionDto extends PartialType(CreateMilitaryJobPositionDto) {}
