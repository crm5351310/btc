import { OmitType } from '@nestjs/swagger';
import { MilitaryJobPosition } from '../entities/military-job-position.entity';

export class CreateMilitaryJobPositionDto extends OmitType(MilitaryJobPosition, ['id' as const]) {}
