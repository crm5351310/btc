import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsOptional, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { BaseCategoryEntity } from '../../../../../common/base/entity/base-category.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { MilitaryJob } from '../../military-job/entities/military-job.entity';

@Entity()
export class MilitaryJobPosition extends BaseCategoryEntity {
  @ApiProperty({
    description: 'Chuyên nghiệp quân sự - ngành',
    type: RelationTypeBase,
  })
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(MilitaryJob, ['id']))
  @ManyToOne(() => MilitaryJob, (militaryJob) => militaryJob.militaryJobPositions)
  militaryJob: MilitaryJob;

  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;
}
