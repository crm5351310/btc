import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { MilitaryJobPositionService } from './military-job-position.service';
import { CreateMilitaryJobPositionDto } from './dto/create-military-job-position.dto';
import { UpdateMilitaryJobPositionDto } from './dto/update-military-job-position.dto';
import { QueryMilitaryJobPositionDto } from './dto/query-military-job-position.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.MILITARY_JOB_POSITION)
export class MilitaryJobPositionController {
  constructor(private readonly militaryJobPositionService: MilitaryJobPositionService) {}

  @Post()
  create(@Body() createMilitaryJobPositionDto: CreateMilitaryJobPositionDto) {
    return this.militaryJobPositionService.create(createMilitaryJobPositionDto);
  }

  @Get()
  findAll(@Query() queryMilitaryJobPositionDto: QueryMilitaryJobPositionDto) {
    return this.militaryJobPositionService.findAll(queryMilitaryJobPositionDto);
  }

  @Get(':id')
  findOne(@Param() param: PathDto) {
    return this.militaryJobPositionService.findOne(param.id);
  }

  @Patch(':id')
  update(
    @Param() param: PathDto,
    @Body() updateMilitaryJobPositionDto: UpdateMilitaryJobPositionDto,
  ) {
    return this.militaryJobPositionService.update(param.id, updateMilitaryJobPositionDto);
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryJobPositionService.remove(ids);
  }
}
