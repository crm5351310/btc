import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import {
  MilitaryJobGroupModule,
  militaryJobGroupRouter,
} from './military-job-group/military-job-group.module';
import { MilitaryJobModule, militaryJobRouter } from './military-job/military-job.module';
import {
  MilitaryJobPositionModule,
  militaryJobPositionRoute,
} from './military-job-position/military-job-position.module';
import {
  TrainingClassificationModule,
  trainingClassificationRoute,
} from './training-classification/training-classification.module';
import { TypeReserveModule, typeReserveRouter } from './type-reserve/type-reserve.module';

@Module({
  imports: [
    MilitaryJobGroupModule,
    MilitaryJobModule,
    MilitaryJobPositionModule,
    TrainingClassificationModule,
    TypeReserveModule,
  ],
})
export class MilitaryJobRootModule {}
export const militaryJobRootRouter: RouteTree = {
  path: 'military-job-root',
  module: MilitaryJobRootModule,
  children: [
    trainingClassificationRoute,
    typeReserveRouter,
    militaryJobGroupRouter,
    militaryJobPositionRoute,
    militaryJobRouter,
  ],
};
