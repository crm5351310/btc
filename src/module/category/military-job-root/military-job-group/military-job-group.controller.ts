import { Controller, Get, Post, Body, Patch, Param, Delete, ParseArrayPipe } from '@nestjs/common';
import { MilitaryJobGroupService } from './military-job-group.service';
import { CreateMilitaryJobGroupDto } from './dto/create-military-job-group.dto';
import { UpdateMilitaryJobGroupDto } from './dto/update-military-job-group.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathDto } from '../../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.MILITARY_JOB_GROUP)
export class MilitaryJobGroupController {
  constructor(private readonly militaryJobGroupService: MilitaryJobGroupService) {}

  @Post()
  create(@Body() createMilitaryJobGroupDto: CreateMilitaryJobGroupDto) {
    return this.militaryJobGroupService.create(createMilitaryJobGroupDto);
  }

  @Get()
  findAll() {
    return this.militaryJobGroupService.findAll();
  }

  @Get(':id')
  findOne(@Param() param: PathDto) {
    return this.militaryJobGroupService.findOne(param.id);
  }

  @Patch(':id')
  update(@Param() param: PathDto, @Body() updateMilitaryJobGroupDto: UpdateMilitaryJobGroupDto) {
    return this.militaryJobGroupService.update(param.id, updateMilitaryJobGroupDto);
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryJobGroupService.remove(ids);
  }
}
