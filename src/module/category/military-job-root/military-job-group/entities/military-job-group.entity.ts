import { BaseCategoryEntity } from '../.././../../../common/base/entity/base-category.entity';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsString, MaxLength, IsDefined, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { TypeReserve } from '../../type-reserve/entities/type-reserve.entity';
import { MilitaryJob } from '../../military-job/entities/military-job.entity';

@Entity()
export class MilitaryJobGroup extends BaseCategoryEntity {
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  @ApiProperty({
    description: 'Loại dự bị',
    type: RelationTypeBase,
  })
  @IsDefined()
  @ValidateNested()
  @Type(() => PickType(TypeReserve, ['id']))
  @ManyToOne(() => TypeReserve, (typeReserve) => typeReserve.militaryJobGroups)
  typeReserve: TypeReserve;

  @OneToMany(() => MilitaryJob, (militaryJob) => militaryJob.militaryJobGroup)
  militaryJobs: MilitaryJob[];
}
