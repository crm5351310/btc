import { Module } from '@nestjs/common';
import { MilitaryJobGroupService } from './military-job-group.service';
import { MilitaryJobGroupController } from './military-job-group.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryJobGroup } from './entities/military-job-group.entity';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryJobGroup])],
  controllers: [MilitaryJobGroupController],
  providers: [MilitaryJobGroupService],
})
export class MilitaryJobGroupModule {}
export const militaryJobGroupRouter: RouteTree = {
  path: 'military-job-group',
  module: MilitaryJobGroupModule,
  children: [],
};
