import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindOne,
} from '../../../../common/response';
import { Repository, FindOptionsWhere, FindManyOptions } from 'typeorm';
import { TypeReserveService } from '../type-reserve/type-reserve.service';
import { CreateMilitaryJobGroupDto } from './dto/create-military-job-group.dto';
import { UpdateMilitaryJobGroupDto } from './dto/update-military-job-group.dto';
import { MilitaryJobGroup } from './entities/military-job-group.entity';
import { MilitaryJobGroupService } from './military-job-group.service';

describe('MilitaryJobGroupService', () => {
  let service: MilitaryJobGroupService;
  let repository: Repository<MilitaryJobGroup>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryJobGroupService,
        {
          provide: getRepositoryToken(MilitaryJobGroup),
          useValue: createStubInstance(Repository),
        },
        {
          provide: TypeReserveService,
          useValue: createStubInstance(TypeReserveService),
        },
      ],
    }).compile();

    service = module.get<MilitaryJobGroupService>(MilitaryJobGroupService);
    repository = module.get(getRepositoryToken(MilitaryJobGroup));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryJobGroup.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryJobGroup>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryJobGroup;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryJobGroup, militaryJobGroup.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitaryJobGroup>) => {
        return militaryJobGroup.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitaryJobGroup>).code ||
            e.name == (options.where as FindOptionsWhere<MilitaryJobGroup>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryJobGroup) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryJobGroup.length,
        };
      }
    });
  });

  const militaryJobGroup = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
      typeReserve: {
        id: 1,
      },
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
      typeReserve: {
        id: 2,
      },
    },
  ] as MilitaryJobGroup[];

  describe('create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
        typeReserve: {
          id: 1,
        },
      } as CreateMilitaryJobGroupDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    it('Fail nếu trùng trường code hoặc name', async () => {
      const payload = {
        code: 'code2',
        name: 'name2',
        description: 'mo ta 2',
        typeReserve: {
          id: 2,
        },
      } as CreateMilitaryJobGroupDto;
      expect(service.create(payload)).rejects.toThrowError();
    });
  });

  describe('findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll();
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryJobGroup.length);
    });
  });

  describe('findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 10;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitaryJobGroupDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitaryJobGroupDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
