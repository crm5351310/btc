import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from 'src/common/base/service/base-category.service';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import {
  ResponseCreated,
  ResponseUpdate,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
} from 'src/common/response';
import { DeepPartial, FindOptionsWhere, In, Not, Repository } from 'typeorm';
import { MilitaryJob } from '../military-job/entities/military-job.entity';
import { CreateMilitaryJobGroupDto } from './dto/create-military-job-group.dto';
import { UpdateMilitaryJobGroupDto } from './dto/update-military-job-group.dto';
import { MilitaryJobGroup } from './entities/military-job-group.entity';

@Injectable()
export class MilitaryJobGroupService extends BaseCategoryService<MilitaryJobGroup> {
  constructor(
    @InjectRepository(MilitaryJobGroup)
    responsitory: Repository<MilitaryJobGroup>,
  ) {
    super(responsitory);
  }
  async create(dto: CreateMilitaryJobGroupDto): Promise<ResponseCreated<MilitaryJobGroup>> {
    await this.checkExist(['name', 'code'], dto);

    const result = await this.repository.save(dto);

    return new ResponseCreated(result);
  }
  async findAll(): Promise<ResponseFindAll<MilitaryJobGroup>> {
    const [results, total] = await this.repository.findAndCount({
      relations: {
        typeReserve: true,
      },
    });
    return new ResponseFindAll(results, total);
  }
  async findOne(id: number): Promise<ResponseFindOne<MilitaryJobGroup>> {
    const result = await this.repository.findOne({
      relations: {
        typeReserve: true,
      },
      where: { id: id as any },
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, MilitaryJobGroup.name, true);
    }

    return new ResponseFindOne(result);
  }
  async update(id: number, dto: UpdateMilitaryJobGroupDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitaryJobGroup.name);
    await this.checkExist(['name', 'code'], dto, id);
    const result = await this.repository.save({ id, ...dto });
    return new ResponseUpdate(result);
  }
  async remove(ids: number[]): Promise<ResponseDelete<MilitaryJobGroup>> {
    const results = await this.repository.find({
      relations: {
        typeReserve: true,
        militaryJobs: true,
      },
      where: {
        id: In(ids),
      },
    });
    const rows = results.reduce((pre: MilitaryJobGroup[], cur: MilitaryJobGroup) => {
      if (!cur.militaryJobs || cur.militaryJobs?.length === 0) {
        pre.push(cur);
      }
      return pre;
    }, []);
    if (rows.length === 0) {
      throw new CustomBadRequestException(ContentMessage.FAILURE, MilitaryJobGroup.name, true);
    }
    await this.repository.softRemove(rows);
    return new ResponseDelete<MilitaryJobGroup>(results, ids, rows);
  }
  protected override async checkExist(
    properties: (keyof MilitaryJobGroup)[],
    dto: DeepPartial<MilitaryJobGroup>,
    id?: number | undefined,
  ): Promise<void> {
    const errors: string[] = [];

    await Promise.all(
      properties.map(async (property) => {
        const isExist = await this.repository.exist({
          relations: {
            typeReserve: true,
          },
          where: {
            typeReserve: {
              id: dto.typeReserve?.id,
            },
            id: id ? Not(id) : undefined,
            [property]: dto[property],
          } as FindOptionsWhere<MilitaryJobGroup>,
        });
        if (isExist) {
          errors.push(`${property.toString()}.${ContentMessage.EXIST}`);
        }
      }),
    );

    if (errors.length > 0) {
      throw new CustomBadRequestExceptionCustom(errors);
    }
  }
}
