import { PartialType } from '@nestjs/mapped-types';
import { CreateMilitaryJobGroupDto } from './create-military-job-group.dto';

export class UpdateMilitaryJobGroupDto extends PartialType(CreateMilitaryJobGroupDto) {}
