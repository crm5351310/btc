import { OmitType } from '@nestjs/swagger';
import { MilitaryJobGroup } from '../entities/military-job-group.entity';

export class CreateMilitaryJobGroupDto extends OmitType(MilitaryJobGroup, ['id' as const]) {}
