import { Module } from '@nestjs/common';
import { TrainingClassificationService } from './training-classification.service';
import { TrainingClassificationController } from './training-classification.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TrainingClassification } from './entities/training-classification.entity';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([TrainingClassification])],
  controllers: [TrainingClassificationController],
  providers: [TrainingClassificationService],
})
export class TrainingClassificationModule {}
export const trainingClassificationRoute: RouteTree = {
  path: 'training-classification',
  module: TrainingClassificationModule,
};
