import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '../../../../common/base/class/base.class';
import { ResponseFindAll } from '../../../../common/response';
import { Repository } from 'typeorm';
import { TrainingClassification } from './entities/training-classification.entity';

@Injectable()
export class TrainingClassificationService extends BaseService {
  constructor(
    @InjectRepository(TrainingClassification)
    private readonly repository: Repository<TrainingClassification>,
  ) {
    super();
  }
  async findAll(): Promise<ResponseFindAll<TrainingClassification>> {
    const [result, total] = await this.repository.findAndCount();
    return new ResponseFindAll(result, total);
  }
}
