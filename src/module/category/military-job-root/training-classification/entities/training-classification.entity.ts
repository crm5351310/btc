import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { MilitaryJob } from '../../military-job/entities/military-job.entity';

@Entity()
export class TrainingClassification extends BaseEntity {
  @Column('varchar', { length: 25 })
  name: string;
  @Column('varchar', { length: 25 })
  code: string;

  @OneToMany(() => MilitaryJob, (militaryJob) => militaryJob.trainingClassification)
  militaryJob: MilitaryJob[];
}
