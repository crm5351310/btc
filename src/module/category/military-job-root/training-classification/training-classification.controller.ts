import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { TrainingClassificationService } from './training-classification.service';

@Controller()
@ApiTags(TagEnum.TRANING_CLASSIFICATION)
export class TrainingClassificationController {
  constructor(private readonly trainingClassificationService: TrainingClassificationService) {}
  @Get()
  findAll() {
    return this.trainingClassificationService.findAll();
  }
}
