import { Module } from '@nestjs/common';
import { ForceTypeModule } from './force-type/force-type.module';
import { ForceModule } from './force/force.module';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [ForceModule, ForceTypeModule],
})
export class ForceRootModule {
  static readonly route: RouteTree = {
    path: 'force-root',
    module: ForceRootModule,
    children: [ForceModule.route, ForceTypeModule.route],
  };
}
