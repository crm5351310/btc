import { Module } from '@nestjs/common';
import { ForceService } from './force.service';
import { ForceController } from './force.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Force } from './entities/force.entity';
import { ForceTypeModule } from '../force-type/force-type.module';

@Module({
  imports: [TypeOrmModule.forFeature([Force]), ForceTypeModule],
  controllers: [ForceController],
  providers: [ForceService],
  exports: [ForceService],
})
export class ForceModule {
  static readonly route: RouteTree = {
    path: 'force',
    module: ForceModule,
    children: [],
  };
}
