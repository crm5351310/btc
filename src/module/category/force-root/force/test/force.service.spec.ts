import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../../config/data-source.config';
import { ForceService } from '../force.service';
import { Force } from '../entities/force.entity';
import { ForceModule } from '../force.module';
import { CreateForceDto } from '../dto/create-force.dto';
import { FindManyForceParamDto } from '../dto/find-many-force-param.dto';
import { UpdateForceDto } from '../dto/update-force.dto';
import { ForceTypeService } from '../../force-type/force-type.service';

// npm run test:run src/module/category/force-root/force
describe('ForceService', () => {
  let forceService: ForceService;
  let forceRepository: Repository<Force>;
  let forceTypeService: jest.Mocked<ForceTypeService>;

  const FORCE_TYPE_REPOSITORY_TOKEN = getRepositoryToken(Force);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ForceModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    forceService = module.get<ForceService>(ForceService);
    forceRepository = module.get(FORCE_TYPE_REPOSITORY_TOKEN);

    forceTypeService = module.get(ForceTypeService);
  });

  it('should be defined', () => {
    expect(forceService).toBeDefined();
    expect(forceRepository).toBeDefined();
  });

  describe('ForceService => create', () => {
    const payload = {
      code: 'code',
      name: 'name',
      description: 'Mô tả',
      forceType: {
        id: 1,
      },
    } as CreateForceDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Force>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Force>;

    it('Tạo mới lực lượng thành công', async () => {
      jest.spyOn(forceRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(forceTypeService, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(forceRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Force);

      const result = await forceService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới lực lượng không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(forceRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as Force);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as Force);
        return Promise.resolve(null);
      });

      await expect(forceService.create(payload)).rejects.toThrowError();
    });
  });

  describe('ForceService => findOne', () => {
    it('Tìm một lực lượng thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'lực lượng số 1',
        description: 'Mô tả',
      } as Force;
      jest.spyOn(forceRepository, 'findOne').mockResolvedValue(mockData);

      const result = await forceService.findOne({ id: 1 });

      expect(forceRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một lực lượng nếu ID không tồn tại', async () => {
      jest.spyOn(forceRepository, 'findOne').mockResolvedValue(null);
      await expect(forceService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('ForceService => findAll', () => {
    it('Tìm danh sách lực lượng thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'lực lượng số 1',
        description: 'Mô tả',
      } as Force;
      jest.spyOn(forceRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await forceService.findAll({} as FindManyForceParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('ForceService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'lực lượng số 1',
      description: 'Mô tả',
    } as UpdateForceDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Force>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Force>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<Force>;

    it('Cập nhật lực lượng thành công', async () => {
      jest.spyOn(forceRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as Force);
        return Promise.resolve(null);
      });
      jest.spyOn(forceRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Force);

      const result = await forceService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật lực lượng không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(forceRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as Force);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as Force);
        return Promise.resolve(null);
      });
      jest.spyOn(forceRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Force);

      await expect(forceService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật lực lượng không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(forceRepository, 'findOne').mockResolvedValue({ id: 1 } as Force);
      jest.spyOn(forceRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as Force);

      await expect(forceService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('ForceService => remove', () => {
    it('Xoá lực lượng thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'lực lượng số 1',
        description: 'Mô tả',
      } as Force;
      jest.spyOn(forceRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(forceRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await forceService.remove({ id: '1' });

      expect(forceRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá lực lượng không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(forceRepository, 'find').mockResolvedValue([]);
      await expect(forceService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
