import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Pagination } from '../../../../common/utils/pagination.util';
import { ForceTypeService } from '../force-type/force-type.service';
import { CreateForceDto } from './dto/create-force.dto';
import { FindManyForceParamDto } from './dto/find-many-force-param.dto';
import { UpdateForceDto } from './dto/update-force.dto';
import { Force } from './entities/force.entity';

@Injectable()
export class ForceService extends BaseService {
  constructor(
    @InjectRepository(Force)
    private readonly forceRepository: Repository<Force>,
    private readonly forceTypeService: ForceTypeService,
  ) {
    super();
    this.name = ForceService.name;
    this.subject = SubjectMessage.FORCE;
  }

  async create(createForceDto: CreateForceDto) {
    this.action = BaseMessage.CREATE;
    const checkExistCode = await this.forceRepository.findOne({
      where: {
        code: createForceDto.code,
      },
    });
    if (checkExistCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    const checkExistName = await this.forceRepository.findOne({
      where: {
        name: createForceDto.name,
      },
    });
    if (checkExistName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    await this.forceTypeService.findOne(createForceDto.forceType);

    const result = await this.forceRepository.save(createForceDto);
    return this.response(result);
  }

  async findAll(param: FindManyForceParamDto) {
    this.action = BaseMessage.READ;

    const result = await this.forceRepository.findAndCount({
      where: {
        forceType: {
          id: param.forceTypeId,
        },
      },
    });
    return this.response(new Pagination<Force>(result[0], result[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.forceRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.FORCE, true);
    return this.response(result);
  }

  async update(path: PathDto, updateForceDto: UpdateForceDto) {
    this.action = BaseMessage.UPDATE;
    const checkExistJobSpecialization = await this.forceRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistJobSpecialization)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.JOB_SPECIALIZATION,
        true,
      );
    if (updateForceDto.code) {
      const checkExistCode = await this.forceRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateForceDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateForceDto.name) {
      const checkExistName = await this.forceRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateForceDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const data = await this.forceRepository.save({
      id: path.id,
      ...updateForceDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistForce = await this.forceRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistForce.length !== pathArray.length)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.FORCE_TYPE, true);
    await this.forceRepository.softRemove(checkExistForce);
    return this.response(checkExistForce);
  }
}
