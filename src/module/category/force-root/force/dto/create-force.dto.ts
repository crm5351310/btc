import { OmitType } from '@nestjs/swagger';
import { Force } from '../entities/force.entity';

export class CreateForceDto extends OmitType(Force, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
  'deletedAt',
] as const) {}
