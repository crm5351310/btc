import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateForceDto } from './create-force.dto';

export class UpdateForceDto extends OmitType(PartialType(CreateForceDto), ['forceType']) {}
