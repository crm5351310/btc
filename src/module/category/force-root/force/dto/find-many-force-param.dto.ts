import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class FindManyForceParamDto {
  @ApiProperty({
    name: 'forceTypeId',
    required: false,
    description: 'ID của loại lực lượng',
  })
  @IsOptional()
  @IsNumber()
  forceTypeId: number;
}
