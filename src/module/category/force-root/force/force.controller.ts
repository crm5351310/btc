import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { CreateForceDto } from './dto/create-force.dto';
import { FindManyForceParamDto } from './dto/find-many-force-param.dto';
import { UpdateForceDto } from './dto/update-force.dto';
import { ForceService } from './force.service';

@Controller()
@ApiTags(TagEnum.FORCE)
export class ForceController {
  constructor(private readonly jobSpecializationService: ForceService) {}

  @Post()
  create(@Body() createForceDto: CreateForceDto) {
    return this.jobSpecializationService.create(createForceDto);
  }

  @Get()
  findAll(@Query() param: FindManyForceParamDto) {
    return this.jobSpecializationService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.jobSpecializationService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() UpdateForceDto: UpdateForceDto) {
    return this.jobSpecializationService.update(path, UpdateForceDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.jobSpecializationService.remove(path);
  }
}
