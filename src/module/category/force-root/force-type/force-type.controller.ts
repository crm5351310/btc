import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { ForceTypeService } from './force-type.service';
import { CreateForceTypeDto } from './dto/create-force-type.dto';
import { UpdateForceTypeDto } from './dto/update-force-type.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { FindManyForceTypeParamDto } from './dto/find-many-force-type-param.dto';

@Controller()
@ApiTags(TagEnum.FORCE_TYPE)
export class ForceTypeController {
  constructor(private readonly ForceTypeService: ForceTypeService) {}

  @Post()
  create(@Body() createForceTypeDto: CreateForceTypeDto) {
    return this.ForceTypeService.create(createForceTypeDto);
  }

  @Get()
  findAll(@Query() param: FindManyForceTypeParamDto) {
    return this.ForceTypeService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.ForceTypeService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateForceTypeDto: UpdateForceTypeDto) {
    return this.ForceTypeService.update(path, updateForceTypeDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.ForceTypeService.remove(path);
  }
}
