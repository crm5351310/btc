import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class FindManyForceTypeParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
