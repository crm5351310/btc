import { OmitType } from '@nestjs/swagger';
import { ForceType } from '../entities/force-type.entity';

export class CreateForceTypeDto extends OmitType(ForceType, [
  'id',
  'createdAt',
  'createdBy',
  'updatedAt',
  'updatedBy',
  'deletedAt',
] as const) {}
