import { PartialType } from '@nestjs/swagger';
import { CreateForceTypeDto } from './create-force-type.dto';

export class UpdateForceTypeDto extends PartialType(CreateForceTypeDto) {}
