import { Module } from '@nestjs/common';
import { ForceTypeService } from './force-type.service';
import { ForceTypeController } from './force-type.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ForceType } from './entities/force-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ForceType])],
  controllers: [ForceTypeController],
  providers: [ForceTypeService],
  exports: [ForceTypeService],
})
export class ForceTypeModule {
  static readonly route: RouteTree = {
    path: 'force-type',
    module: ForceTypeModule,
    children: [],
  };
}
