import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Pagination } from '../../../../common/utils/pagination.util';
import { CreateForceTypeDto } from './dto/create-force-type.dto';
import { FindManyForceTypeParamDto } from './dto/find-many-force-type-param.dto';
import { UpdateForceTypeDto } from './dto/update-force-type.dto';
import { ForceType } from './entities/force-type.entity';

@Injectable()
export class ForceTypeService extends BaseService {
  constructor(
    @InjectRepository(ForceType)
    private readonly forceTypeRepository: Repository<ForceType>,
  ) {
    super();
    this.name = ForceTypeService.name;
    this.subject = SubjectMessage.FORCE_TYPE;
  }
  async create(createJobSpecializationGroupDto: CreateForceTypeDto) {
    this.action = BaseMessage.CREATE;
    const checkExistCodeEthnicity = await this.forceTypeRepository.findOne({
      where: {
        code: createJobSpecializationGroupDto.code,
      },
    });
    if (checkExistCodeEthnicity)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    const checkExistNameEthnicity = await this.forceTypeRepository.findOne({
      where: {
        name: createJobSpecializationGroupDto.name,
      },
    });
    if (checkExistNameEthnicity)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    const result = await this.forceTypeRepository.save(createJobSpecializationGroupDto);
    return this.response(result);
  }

  async findAll(param: FindManyForceTypeParamDto) {
    this.action = BaseMessage.READ;

    const result = await this.forceTypeRepository.findAndCount({
      skip: param.offset || undefined,
      take: param.limit || undefined,
    });
    return this.response(new Pagination<ForceType>(result[0], result[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.forceTypeRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.FORCE_TYPE, true);
    return this.response(result);
  }

  async update(path: PathDto, updateJobSpecializationGroupDto: UpdateForceTypeDto) {
    this.action = BaseMessage.UPDATE;
    const checkExistForceType = await this.forceTypeRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistForceType)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.FORCE_TYPE, true);
    if (updateJobSpecializationGroupDto.code) {
      const checkExistCode = await this.forceTypeRepository.findOne({
        where: {
          id: Not(path.id),
          code: updateJobSpecializationGroupDto.code,
        },
      });
      if (checkExistCode)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);
    }

    if (updateJobSpecializationGroupDto.name) {
      const checkExistName = await this.forceTypeRepository.findOne({
        where: {
          id: Not(path.id),
          name: updateJobSpecializationGroupDto.name,
        },
      });
      if (checkExistName)
        throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    }

    const data = await this.forceTypeRepository.save({
      id: path.id,
      ...updateJobSpecializationGroupDto,
    });
    return this.response(data);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');
    const checkExistForceType = await this.forceTypeRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistForceType.length !== pathArray.length)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.FORCE_TYPE, true);
    await this.forceTypeRepository.softRemove(checkExistForceType);
    return this.response(checkExistForceType);
  }
}
