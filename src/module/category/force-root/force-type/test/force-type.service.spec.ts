import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../../config/data-source.config';
import { ForceTypeModule } from '../force-type.module';
import { ForceType } from '../entities/force-type.entity';
import { ForceTypeService } from '../force-type.service';
import { CreateForceTypeDto } from '../dto/create-force-type.dto';
import { FindManyForceTypeParamDto } from '../dto/find-many-force-type-param.dto';
import { UpdateForceTypeDto } from '../dto/update-force-type.dto';

// npm run test:run src/module/category/force-root/force-type
describe('ForceTypeService', () => {
  let forceTypeService: ForceTypeService;
  let forceTypeRepository: Repository<ForceType>;

  const FORCE_TYPE_REPOSITORY_TOKEN = getRepositoryToken(ForceType);

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ForceTypeModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    forceTypeService = module.get<ForceTypeService>(ForceTypeService);
    forceTypeRepository = module.get(FORCE_TYPE_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(forceTypeService).toBeDefined();
    expect(forceTypeRepository).toBeDefined();
  });

  describe('ForceTypeService => create', () => {
    const payload = {
      code: 'code',
      name: 'name',
      description: 'Mô tả',
    } as CreateForceTypeDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<ForceType>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<ForceType>;

    it('Tạo mới loại lực lượng thành công', async () => {
      jest.spyOn(forceTypeRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(forceTypeRepository, 'save').mockResolvedValue({ id: 1, ...payload } as ForceType);

      const result = await forceTypeService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới loại lực lượng không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(forceTypeRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as ForceType);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as ForceType);
        return Promise.resolve(null);
      });

      await expect(forceTypeService.create(payload)).rejects.toThrowError();
    });
  });

  describe('ForceTypeService => findOne', () => {
    it('Tìm một loại lực lượng thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'loại lực lượng số 1',
        description: 'Mô tả',
      } as ForceType;
      jest.spyOn(forceTypeRepository, 'findOne').mockResolvedValue(mockData);

      const result = await forceTypeService.findOne({ id: 1 });

      expect(forceTypeRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một loại lực lượng nếu ID không tồn tại', async () => {
      jest.spyOn(forceTypeRepository, 'findOne').mockResolvedValue(null);
      await expect(forceTypeService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('ForceTypeService => findAll', () => {
    it('Tìm danh sách loại lực lượng thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'loại lực lượng số 1',
        description: 'Mô tả',
      } as ForceType;
      jest.spyOn(forceTypeRepository, 'findAndCount').mockResolvedValue([[mockData], 1]);

      const result = await forceTypeService.findAll({} as FindManyForceTypeParamDto);

      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('ForceTypeService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'loại lực lượng số 1',
      description: 'Mô tả',
    } as UpdateForceTypeDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<ForceType>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<ForceType>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<ForceType>;

    it('Cập nhật loại lực lượng thành công', async () => {
      jest.spyOn(forceTypeRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as ForceType);
        return Promise.resolve(null);
      });
      jest.spyOn(forceTypeRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as ForceType);

      const result = await forceTypeService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật loại lực lượng không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(forceTypeRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 2 } as ForceType);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 2 } as ForceType);
        return Promise.resolve(null);
      });
      jest.spyOn(forceTypeRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as ForceType);

      await expect(forceTypeService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật loại lực lượng không thành công nếu name hoặc code đã tồn tại', async () => {
      jest.spyOn(forceTypeRepository, 'findOne').mockResolvedValue({ id: 1 } as ForceType);
      jest.spyOn(forceTypeRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as ForceType);

      await expect(forceTypeService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('ForceTypeService => remove', () => {
    it('Xoá loại lực lượng thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'loại lực lượng số 1',
        description: 'Mô tả',
      } as ForceType;
      jest.spyOn(forceTypeRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(forceTypeRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await forceTypeService.remove({ id: '1' });

      expect(forceTypeRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá loại lực lượng không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(forceTypeRepository, 'find').mockResolvedValue([]);
      await expect(forceTypeService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
