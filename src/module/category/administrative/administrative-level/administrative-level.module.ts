import { Module } from '@nestjs/common';
import { AdministrativeLevelService } from './administrative-level.service';
import { AdministrativeLevelController } from './administrative-level.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeLevel } from './administrative-level.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([AdministrativeLevel])],
  controllers: [AdministrativeLevelController],
  providers: [AdministrativeLevelService],
})
export class AdministrativeLevelModule {}
export const administrativeLevelRouter: RouteTree = {
  path: 'administrative-level',
  module: AdministrativeLevelModule,
  children: [],
};
