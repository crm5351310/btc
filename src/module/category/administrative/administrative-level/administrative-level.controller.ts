import { Controller, Get } from '@nestjs/common';
import { AdministrativeLevelService } from './administrative-level.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.ADMINISTRATIVE_LEVEL)
export class AdministrativeLevelController {
  constructor(private readonly administrativeLevelService: AdministrativeLevelService) {}

  @Get()
  findAll() {
    return this.administrativeLevelService.findAll();
  }
}
