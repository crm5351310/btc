import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseMessage } from '../../../../common/message/base.message';
import { BaseService } from '../../../../common/base/class/base.class';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { AdministrativeLevel } from './administrative-level.entity';

@Injectable()
export class AdministrativeLevelService extends BaseService {
  constructor(
    @InjectRepository(AdministrativeLevel)
    private readonly administrativeLevelRepository: Repository<AdministrativeLevel>,
  ) {
    super();
    this.name = AdministrativeLevelService.name;
    this.subject = SubjectMessage.ADMINISTRATIVE_LEVEL;
  }
  async findAll() {
    this.action = BaseMessage.READ;

    const administrativeLevel = await this.administrativeLevelRepository.find();
    return this.response(administrativeLevel);
  }
}
