import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { AdministrativeTitle } from '../administrative-title/administrative-title.entity';

@Entity()
export class AdministrativeLevel extends BaseEntity {
  @Column('varchar', {
    length: 5,
    nullable: false,
  })
  name: string;

  @Column('varchar', {
    length: 5,
    nullable: false,
  })
  code: string;

  @OneToMany(() => AdministrativeTitle, (item) => item.administrativeLevel)
  administrativeTitle: AdministrativeTitle[];
}
