import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { AdministrativeUnit } from '../administrative-unit-root/administrative-unit/administrative-unit.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, Matches, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../common/base/class/base.class';
import { Type } from 'class-transformer';

@Entity()
export class PopulationHistory extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Đến ngày',
    type: 'string',
    default: '2024-01-12',
    required: true,
  })
  // validate
  @IsNotEmpty()
  @Matches(
    /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/,
  )
  // entity
  @Column('date', {
    nullable: true,
  })
  toDate: string;

  // swagger
  @ApiProperty({
    description: 'Dân số',
    type: 'number',
    default: 1,
    required: true,
  })
  // validate
  @IsNumber({})
  @IsNotEmpty()
  // entity
  @Column('int', {
    nullable: false,
  })
  populationMale: number;

  // swagger
  @ApiProperty({
    description: 'Dân số',
    type: 'number',
    default: 1,
    required: true,
  })
  // validate
  @IsNumber({})
  @IsNotEmpty()
  // entity
  @Column('int', {
    nullable: false,
  })
  populationFemale: number;

  // swagger
  @ApiProperty({
    description: 'Cấp hành chính',
    type: RelationTypeBase,
  })
  // validate
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  // entity
  @ManyToOne(() => AdministrativeUnit, (item) => item.populationHistory)
  @JoinColumn()
  administrativeUnit: AdministrativeUnit;
}
