import { Test, TestingModule } from '@nestjs/testing';
import { PopulationHistoryService } from '../population-history.service';

describe('PopulationHistoryService', () => {
  let service: PopulationHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PopulationHistoryService],
    }).compile();

    service = module.get<PopulationHistoryService>(PopulationHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
