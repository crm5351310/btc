import { Test, TestingModule } from '@nestjs/testing';
import { PopulationHistoryController } from '../population-history.controller';
import { PopulationHistoryService } from '../population-history.service';

describe('PopulationHistoryController', () => {
  let controller: PopulationHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PopulationHistoryController],
      providers: [PopulationHistoryService],
    }).compile();

    controller = module.get<PopulationHistoryController>(PopulationHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
