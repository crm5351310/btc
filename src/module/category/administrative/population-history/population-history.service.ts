import { Injectable } from '@nestjs/common';
import { BaseService, PathDto } from '../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Equal, In, Repository } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { PopulationHistory } from './population-history.entity';
import {
  CreatePopulationHistoryDto,
  ParamPopulationHistoryDto,
  UpdatePopulationHistoryDto,
} from './population-history.dto';
import { BaseMessage } from '../../../../common/message/base.message';
import { StringHelper } from '../../../../common/utils/string.util';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { AdministrativeUnit } from '../administrative-unit-root/administrative-unit/administrative-unit.entity';

@Injectable()
export class PopulationHistoryService extends BaseService {
  constructor(
    @InjectRepository(PopulationHistory)
    private readonly populationHistoryRepository: Repository<PopulationHistory>,

    @InjectRepository(AdministrativeUnit)
    private readonly unitRepository: Repository<AdministrativeUnit>,
  ) {
    super();
    this.name = PopulationHistoryService.name;
    this.subject = SubjectMessage.POPULATION_HISTORY;
  }
  async create(createPopulationHistoryDto: CreatePopulationHistoryDto) {
    this.action = BaseMessage.CREATE;
    createPopulationHistoryDto = StringHelper.spaceObject(createPopulationHistoryDto);

    const administrativeTitle = await this.populationHistoryRepository.save(
      createPopulationHistoryDto.populationHistory,
    );
    return this.response(administrativeTitle);
  }

  async findAll(param: ParamPopulationHistoryDto) {
    this.action = BaseMessage.READ;
    const query = this.populationHistoryRepository
      .createQueryBuilder('A')
      .leftJoin('A.administrativeUnit', 'B')
      .where('B.id = :unitId', { unitId: param.id });

    if (param.year) {
      query.andWhere('YEAR(A.toDate) = :year', { year: param.year });
    }

    query.orderBy('A.toDate', 'DESC');

    const population = await query.getMany();
    return this.response(population);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const population = await this.populationHistoryRepository.findOne({
      where: path,
    });
    if (!population)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.POPULAR_HISTORY,
        true,
      );
    return this.response(population);
  }

  async update(updatePopulationHistoryDto: UpdatePopulationHistoryDto) {
    this.action = BaseMessage.UPDATE;
    const administrativeTitle = await this.populationHistoryRepository.save(
      updatePopulationHistoryDto.populationHistory,
    );
    return this.response(administrativeTitle);
  }

  async remove(path: PathDto) {
    this.action = BaseMessage.DELETE;
    const populationHistory = await this.populationHistoryRepository.findOne({
      where: path,
    });

    if (!populationHistory)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.POPULAR_HISTORY,
        true,
      );

    const populationHistorys = await this.populationHistoryRepository.softRemove(populationHistory);
    return this.response(populationHistorys);
  }
}
