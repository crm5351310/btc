import { Module } from '@nestjs/common';
import { PopulationHistoryService } from './population-history.service';
import { PopulationHistoryController } from './population-history.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PopulationHistory } from './population-history.entity';
import { AdministrativeUnit } from '../administrative-unit-root/administrative-unit/administrative-unit.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { FocalAreaModule } from '../focal-area-classification-root/focal-area/focal-area.module';

@Module({
  imports: [TypeOrmModule.forFeature([PopulationHistory, AdministrativeUnit])],
  controllers: [PopulationHistoryController],
  providers: [PopulationHistoryService],
})
export class PopulationHistoryModule {}
export const populationHistoryRouter: RouteTree = {
  path: 'population-history',
  module: PopulationHistoryModule,
  children: [],
};
