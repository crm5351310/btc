import { ApiProperty, IntersectionType, OmitType, PartialType } from '@nestjs/swagger';
import { PathDto, YearDto } from '../../../../common/base/class/base.class';
import { IsNotEmpty, IsNumber, Matches, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { PrimaryGeneratedColumn } from 'typeorm';

class ChildPopulationHistoryDto {
  // swagger
  @ApiProperty({
    description: 'Id',
    minimum: 1,
  })
  // validate
  @IsNotEmpty()
  @IsNumber({})
  @ApiProperty({
    description: 'Id',
  })
  // entity
  @PrimaryGeneratedColumn()
  id: number;

  // swagger
  @ApiProperty({
    description: 'Đến ngày',
    type: 'string',
    default: '2024-01-12',
    required: true,
  })
  // validate
  @IsNotEmpty()
  @Matches(
    /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/,
  )
  // entity
  toDate: string;

  // swagger
  @ApiProperty({
    description: 'Dân số',
    type: 'number',
    default: 1,
    required: true,
  })
  // validate
  @IsNumber({})
  @IsNotEmpty()
  // entity
  populationMale: number;

  // swagger
  @ApiProperty({
    description: 'Dân số',
    type: 'number',
    default: 1,
    required: true,
  })
  // validate
  @IsNumber({})
  @IsNotEmpty()
  // entity
  populationFemale: number;

  @ApiProperty({
    type: PathDto,
  })
  @ValidateNested()
  @Type(() => PathDto)
  administrativeUnit: PathDto;
}
class ChildAblePopulationHistoryDto {
  // swagger
  @ApiProperty({
    description: 'Đến ngày',
    type: 'string',
    default: '2024-01-12',
    required: true,
  })
  // validate
  @IsNotEmpty()
  @Matches(
    /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/,
  )
  // entity
  toDate: string;

  // swagger
  @ApiProperty({
    description: 'Dân số',
    type: 'number',
    default: 1,
    required: true,
  })
  // validate
  @IsNumber({})
  @IsNotEmpty()
  // entity
  populationMale: number;

  // swagger
  @ApiProperty({
    description: 'Dân số',
    type: 'number',
    default: 1,
    required: true,
  })
  // validate
  @IsNumber({})
  @IsNotEmpty()
  // entity
  populationFemale: number;

  @ApiProperty({
    type: PathDto,
  })
  @ValidateNested()
  @Type(() => PathDto)
  administrativeUnit: PathDto;
}
export class UpdatePopulationHistoryDto {
  @ApiProperty({
    type: [ChildPopulationHistoryDto],
  })
  @ValidateNested()
  @Type(() => ChildPopulationHistoryDto)
  populationHistory: ChildPopulationHistoryDto;
}

export class CreatePopulationHistoryDto {
  @ApiProperty({
    type: [ChildAblePopulationHistoryDto],
  })
  @ValidateNested()
  @Type(() => ChildAblePopulationHistoryDto)
  populationHistory: ChildAblePopulationHistoryDto;
}

export class ParamPopulationHistoryDto extends IntersectionType(PathDto, YearDto) {}
