import { Get, Post, Body, Patch, Param, Delete, Query, Controller } from '@nestjs/common';
import { PopulationHistoryService } from './population-history.service';
import { PathDto } from '../../../../common/base/class/base.class';
import {
  CreatePopulationHistoryDto,
  ParamPopulationHistoryDto,
  UpdatePopulationHistoryDto,
} from './population-history.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.POPULATION_HISTORY)
export class PopulationHistoryController {
  constructor(private readonly populationHistoryService: PopulationHistoryService) {}

  @Post()
  create(@Body() createPopulationHistoryDto: CreatePopulationHistoryDto) {
    return this.populationHistoryService.create(createPopulationHistoryDto);
  }

  @Get()
  findAll(@Query() param: ParamPopulationHistoryDto) {
    return this.populationHistoryService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.populationHistoryService.findOne(path);
  }

  @Patch()
  update(@Body() updatePopulationHistoryDto: UpdatePopulationHistoryDto) {
    return this.populationHistoryService.update(updatePopulationHistoryDto);
  }

  @Delete(':id')
  remove(@Param() path: PathDto) {
    return this.populationHistoryService.remove(path);
  }
}
