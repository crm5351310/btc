import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import {
  AdministrativeUnitRootModule,
  administrativeUnitRootRouter,
} from './administrative-unit-root/administrative-unit-root.module';
import { AdministrativeLevelModule, administrativeLevelRouter } from './administrative-level';
import { AdministrativeTitleModule, administrativeTitleRouter } from './administrative-title';
import {
  FocalAreaClassificationRootModule,
  focalAreaClassificationRootRouter,
} from './focal-area-classification-root/focal-area-classification-root.module';
import { GeoLocationModule, geoLocationRouter } from './geo-location';
import {
  PopulationHistoryModule,
  populationHistoryRouter,
} from './population-history/population-history.module';

@Module({
  imports: [
    AdministrativeUnitRootModule,
    AdministrativeLevelModule,
    AdministrativeTitleModule,
    FocalAreaClassificationRootModule,
    GeoLocationModule,
    PopulationHistoryModule,
  ],
  controllers: [],
  providers: [],
})
export class AdministrativeModule {}
export const administrativeRouter: RouteTree = {
  path: 'administrative',
  children: [
    geoLocationRouter,
    administrativeLevelRouter,
    administrativeTitleRouter,
    administrativeUnitRootRouter,
    populationHistoryRouter,
    focalAreaClassificationRootRouter,
  ],
};
