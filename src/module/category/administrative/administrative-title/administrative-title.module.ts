import { Module } from '@nestjs/common';
import { AdministrativeTitleController } from './administrative-title.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeTitle } from './administrative-title.entity';
import { AdministrativeLevel } from '../administrative-level/administrative-level.entity';
import { AdministrativeTitleService } from './administrative-title.service';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([AdministrativeLevel, AdministrativeTitle])],
  controllers: [AdministrativeTitleController],
  providers: [AdministrativeTitleService],
})
export class AdministrativeTitleModule {}
export const administrativeTitleRouter: RouteTree = {
  path: 'administrative-title',
  module: AdministrativeTitleModule,
  children: [],
};
