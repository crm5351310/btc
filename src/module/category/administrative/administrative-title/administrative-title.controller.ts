import { Get, Post, Body, Patch, Param, Delete, Query, Controller } from '@nestjs/common';
import { AdministrativeTitleService } from './administrative-title.service';
import {
  CreateAdministrativeTitleDto,
  QueryAdministrativeLevelDto,
  UpdateAdministrativeTitleDto,
} from './administrative-title.dto';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.ADMINISTRATIVE_TITLE)
export class AdministrativeTitleController {
  constructor(private readonly titleService: AdministrativeTitleService) {}

  @Post()
  create(@Body() createTitleDto: CreateAdministrativeTitleDto) {
    return this.titleService.create(createTitleDto);
  }

  @Get()
  findAll(@Query() param: QueryAdministrativeLevelDto) {
    return this.titleService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.titleService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateTitleDto: UpdateAdministrativeTitleDto) {
    return this.titleService.update(path, updateTitleDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.titleService.remove(path);
  }
}
