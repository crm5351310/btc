import { Column, Entity, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { AdministrativeLevel } from '../administrative-level/administrative-level.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { AdministrativeUnit } from '../administrative-unit-root/administrative-unit/administrative-unit.entity';
import { RelationTypeBase } from '../../../../common/base/class/base.class';

@Entity()
export class AdministrativeTitle extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  // entity
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
    maxLength: 25,
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(25)
  // entity
  @Column('varchar', {
    length: 25,
    nullable: false,
  })
  code: string;

  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  // entity
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  // swagger
  @ApiProperty({
    description: 'Cấp hành chính',
    type: RelationTypeBase,
  })
  // validate
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(AdministrativeLevel, ['id']))
  // entity
  @ManyToOne(() => AdministrativeLevel, (item) => item.administrativeTitle)
  @JoinColumn()
  administrativeLevel?: AdministrativeLevel | null;

  @OneToMany(() => AdministrativeUnit, (item) => item.administrativeTitle)
  administrativeUnit: AdministrativeUnit[];
}
