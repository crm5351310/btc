import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In, Not } from 'typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { BaseMessage } from '../../../../common/message/base.message';
import { StringHelper } from '../../../../common/utils/string.util';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from '../../../../common/exception/bad.exception';
import { AdministrativeTitle } from './administrative-title.entity';
import { AdministrativeLevel } from '../administrative-level';
import {
  CreateAdministrativeTitleDto,
  QueryAdministrativeLevelDto,
  UpdateAdministrativeTitleDto,
} from './administrative-title.dto';
import { MilitaryRank } from '../../military-rank-root/military-rank/military-rank.entity';

@Injectable()
export class AdministrativeTitleService extends BaseService {
  constructor(
    @InjectRepository(AdministrativeTitle)
    private readonly administrativeTitleRepository: Repository<AdministrativeTitle>,
    @InjectRepository(AdministrativeLevel)
    private readonly administrativeLevelRepository: Repository<AdministrativeLevel>,
  ) {
    super();
    this.name = AdministrativeTitleService.name;
    this.subject = SubjectMessage.ADMINISTRATIVE_TITLE;
  }
  async create(createAdministrativeTitleDto: CreateAdministrativeTitleDto) {
    this.action = BaseMessage.CREATE;
    const message: string[] = [];
    let flag = 0;
    const administrativelevel = await this.administrativeLevelRepository.findOne({
      where: {
        id: createAdministrativeTitleDto.administrativeLevel?.id,
      },
    });
    if (!administrativelevel) {
      message.push(FieldMessage.ADMINISTRATIVE_LEVEL + '.' + ContentMessage.NOT_FOUND);
      flag++;
    }

    const administrativeTitleDublicateName = await this.administrativeTitleRepository.findOne({
      where: {
        name: createAdministrativeTitleDto.name,
        administrativeLevel: { id: administrativelevel?.id },
      },
    });
    if (administrativeTitleDublicateName) {
      message.push(FieldMessage.NAME + '.' + ContentMessage.EXIST);
      flag++;
    }

    const administrativeTitleDublicateCode = await this.administrativeTitleRepository.findOne({
      where: {
        code: createAdministrativeTitleDto.code,
        administrativeLevel: { id: administrativelevel?.id },
      },
    });
    if (administrativeTitleDublicateCode) {
      message.push(FieldMessage.CODE + '.' + ContentMessage.EXIST);
      flag++;
    }

    if (flag) {
      throw new CustomBadRequestExceptionCustom(message);
    }

    const administrativeTitle = await this.administrativeTitleRepository.save(
      createAdministrativeTitleDto,
    );
    return this.response(administrativeTitle);
  }

  async findAll(param: QueryAdministrativeLevelDto) {
    this.action = BaseMessage.READ;

    const administrativeLevel = await this.administrativeLevelRepository.findOne({
      where: {
        code: param.level,
      },
    });
    if (!administrativeLevel)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_LEVEL,
        true,
      );

    const administrativeTitles = await this.administrativeTitleRepository.find({
      where: {
        administrativeLevel: {
          code: param.level,
        },
      },
      order: {
        code: 'ASC',
      },
    });
    return this.response(administrativeTitles);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;

    const administrativeTitle = await this.administrativeTitleRepository.findOne({
      where: path,
    });
    if (!administrativeTitle)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_TITLE,
        true,
      );
    return this.response(administrativeTitle);
  }

  async update(path: PathDto, updateAdministrativeTitleDto: UpdateAdministrativeTitleDto) {
    this.action = BaseMessage.UPDATE;
    const message: string[] = [];
    let flag = 0;
    const administrativeLevel = await this.administrativeLevelRepository.findOne({
      where: {
        id: updateAdministrativeTitleDto.administrativeLevel?.id,
      },
    });
    if (!administrativeLevel) {
      message.push(FieldMessage.ADMINISTRATIVE_LEVEL + '.' + ContentMessage.NOT_FOUND);
      flag++;
    }

    const administrativeTitleExist = await this.administrativeTitleRepository.findOne({
      where: path,
    });
    if (!administrativeTitleExist) {
      message.push(FieldMessage.ADMINISTRATIVE_TITLE + '.' + ContentMessage.NOT_FOUND);
      flag++;
    }

    const administrativeTitleDublicateName = await this.administrativeTitleRepository.findOne({
      where: {
        name: updateAdministrativeTitleDto.name,
        administrativeLevel: { id: administrativeLevel?.id },
        id: Not(path.id),
      },
    });
    if (administrativeTitleDublicateName) {
      message.push(FieldMessage.NAME + '.' + ContentMessage.EXIST);
      flag++;
    }

    const administrativeTitleDublicateCode = await this.administrativeTitleRepository.findOne({
      where: {
        code: updateAdministrativeTitleDto.code,
        administrativeLevel: { id: administrativeLevel?.id },
        id: Not(path.id),
      },
    });
    if (administrativeTitleDublicateCode) {
      message.push(FieldMessage.CODE + '.' + ContentMessage.EXIST);
      flag++;
    }

    if (flag) {
      throw new CustomBadRequestExceptionCustom(message);
    }

    const administrativeTitle = await this.administrativeTitleRepository.save({
      id: path.id,
      ...updateAdministrativeTitleDto,
    });
    return this.response(administrativeTitle);
  }

  async remove(path: PathArrayDto) {
    const pathArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const deleteArr: AdministrativeTitle[] = [];
    const titleExist = await this.administrativeTitleRepository.find({
      relations: {
        administrativeUnit: true,
      },
      where: {
        id: In(pathArr),
      },
    });

    const affected = titleExist.reduce((pre: any, cur: AdministrativeTitle) => {
      if (cur.administrativeUnit.length) {
        pre += 1;
      } else {
        deleteArr.push(cur);
      }
      return pre;
    }, 0);


    if (!titleExist.length || !deleteArr.length) {
      throw new CustomBadRequestException(
        ContentMessage.FAILURE,
        FieldMessage.ADMINISTRATIVE_TITLE,
        true,
      );
    }


    await this.administrativeTitleRepository.softRemove(deleteArr);
    return this.response({ affected: pathArr.length - affected });
  }
}
