import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Transform } from 'class-transformer';
import { AdministrativeTitle } from './administrative-title.entity';
import { AdministrativeLevelEnum } from '../../../../common/enum/administrative-level.enum';

export class CreateAdministrativeTitleDto extends OmitType(AdministrativeTitle, ['id'] as const) {}

export class UpdateAdministrativeTitleDto extends PartialType(CreateAdministrativeTitleDto) {}

export class QueryAdministrativeLevelDto {
  @ApiProperty({
    description: 'Cấp hành chính',
    enum: AdministrativeLevelEnum,
    name: 'level',
    required: true,
    default: AdministrativeLevelEnum.TINH,
  })
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (typeof value == 'string') {
      value = [value];
    }
    return value;
  })
  level: AdministrativeLevelEnum;
}
