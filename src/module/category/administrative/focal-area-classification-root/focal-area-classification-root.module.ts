import { Module } from '@nestjs/common';
import {
  FocalAreaClassificationModule,
  focalAreaClassificationRouter,
} from './focal-area-classification/focal-area-classification.module';
import { FocalAreaModule, focalAreaRouter } from './focal-area/focal-area.module';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [FocalAreaClassificationModule, FocalAreaModule],
})
export class FocalAreaClassificationRootModule {}
export const focalAreaClassificationRootRouter: RouteTree = {
  path: 'focal-area-classification-root',
  module: FocalAreaClassificationRootModule,
  children: [focalAreaRouter, focalAreaClassificationRouter],
};
