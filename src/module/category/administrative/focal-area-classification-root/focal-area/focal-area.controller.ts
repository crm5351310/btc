import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { FocalAreaService } from './focal-area.service';
import { CreateFocalAreaDto } from './dto/create-focal-area.dto';
import { UpdateFocalAreaDto } from './dto/update-focal-area.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.FOCAL_AREA)
export class FocalAreaController {
  constructor(private readonly focalAreaService: FocalAreaService) {}

  @Post()
  create(@Body() createFocalAreaDto: CreateFocalAreaDto) {
    return this.focalAreaService.create(createFocalAreaDto);
  }

  @Get()
  findAll() {
    return this.focalAreaService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.focalAreaService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFocalAreaDto: UpdateFocalAreaDto) {
    return this.focalAreaService.update(+id, updateFocalAreaDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.focalAreaService.remove(+id);
  }
}
