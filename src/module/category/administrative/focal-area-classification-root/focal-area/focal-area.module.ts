import { Module } from '@nestjs/common';
import { FocalAreaService } from './focal-area.service';
import { FocalAreaController } from './focal-area.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FocalArea } from './focal-area.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { FocalAreaClassificationRootModule } from '../focal-area-classification-root.module';

@Module({
  imports: [TypeOrmModule.forFeature([FocalArea])],
  providers: [FocalAreaService],
})
export class FocalAreaModule {}
export const focalAreaRouter: RouteTree = {
  path: 'focal-area',
  module: FocalAreaModule,
  children: [],
};
