import { FocalAreaClassification } from '../focal-area-classification/focal-area-classification.entity';

import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { IsNotEmpty, Matches, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { AdministrativeUnit } from '../../administrative-unit-root/administrative-unit/administrative-unit.entity';

@Entity()
export class FocalArea extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Từ ngày',
    type: 'string',
    default: '2024-01-12',
    required: false,
  })
  // validate
  @Matches(
    /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/,
  )
  @IsNotEmpty()
  // entity
  @Column('date', {
    nullable: true,
  })
  startDate: string;

  // swagger
  @ApiProperty({
    description: 'Đến ngày',
    type: 'string',
    default: '2024-01-12',
    required: false,
  })
  // validate
  @Matches(
    /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/,
  )
  @IsNotEmpty()
  // entity
  @Column('date', {
    nullable: true,
  })
  endDate: string;

  // swagger
  @ApiProperty({
    description: 'Phân loại trọng điểm',
    type: RelationTypeBase,
  })
  // validate
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(FocalAreaClassification, ['id']))
  @ManyToOne(() => FocalAreaClassification, (item) => item.focalAreas)
  @JoinColumn()
  focalAreaClassification: FocalAreaClassification;

  @ManyToOne(() => AdministrativeUnit, (item) => item.focalAreas)
  @JoinColumn()
  administrativeUnit: AdministrativeUnit;
}
