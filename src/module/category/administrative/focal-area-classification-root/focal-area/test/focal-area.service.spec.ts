import { Test, TestingModule } from '@nestjs/testing';
import { FocalAreaService } from '../focal-area.service';

describe('FocalAreaService', () => {
  let service: FocalAreaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FocalAreaService],
    }).compile();

    service = module.get<FocalAreaService>(FocalAreaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
