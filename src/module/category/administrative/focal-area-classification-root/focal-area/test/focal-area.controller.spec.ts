import { Test, TestingModule } from '@nestjs/testing';
import { FocalAreaController } from '../focal-area.controller';
import { FocalAreaService } from '../focal-area.service';

describe('FocalAreaController', () => {
  let controller: FocalAreaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FocalAreaController],
      providers: [FocalAreaService],
    }).compile();

    controller = module.get<FocalAreaController>(FocalAreaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
