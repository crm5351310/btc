import { Injectable } from '@nestjs/common';
import { CreateFocalAreaDto } from './dto/create-focal-area.dto';
import { UpdateFocalAreaDto } from './dto/update-focal-area.dto';

@Injectable()
export class FocalAreaService {
  create(createFocalAreaDto: CreateFocalAreaDto) {
    return 'This action adds a new focalArea';
  }

  findAll() {
    return 'This action returns all focalArea';
  }

  findOne(id: number) {
    return `This action returns a #${id} focalArea`;
  }

  update(id: number, updateFocalAreaDto: UpdateFocalAreaDto) {
    return `This action updates a #${id} focalArea`;
  }

  remove(id: number) {
    return `This action removes a #${id} focalArea`;
  }
}
