import { PartialType } from '@nestjs/mapped-types';
import { CreateFocalAreaDto } from './create-focal-area.dto';

export class UpdateFocalAreaDto extends PartialType(CreateFocalAreaDto) {}
