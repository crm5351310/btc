import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  CreateFocalAreaClassificationDto,
  FindManyFocalAreaClassificationParamDto,
  UpdateFocalAreaClassificationDto,
} from './focal-area-classification.dto';
import { FocalAreaClassificationService } from './focal-area-classification.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.FOCAL_AREA_CLASSIFICATION)
export class FocalAreaClassificationController {
  constructor(private readonly focalAreaClassificationService: FocalAreaClassificationService) {}

  @Get('')
  async findAll(@Query() query: FindManyFocalAreaClassificationParamDto) {
    const result = await this.focalAreaClassificationService.findAll(query);
    return result;
  }

  @Get(':id')
  async findOne(@Param('id') id: number) {
    const result = await this.focalAreaClassificationService.findOne(id);
    return result;
  }

  @Post('')
  async create(@Body() payload: CreateFocalAreaClassificationDto) {
    const result = await this.focalAreaClassificationService.create(payload);
    return result;
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() payload: UpdateFocalAreaClassificationDto) {
    const result = await this.focalAreaClassificationService.update(id, payload);
    return result;
  }

  @Delete(':ids')
  async remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    const result = await this.focalAreaClassificationService.remove(ids);
    return result;
  }
}
