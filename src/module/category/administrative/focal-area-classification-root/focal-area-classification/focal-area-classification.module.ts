import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FocalAreaClassificationController } from './focal-area-classification.controller';
import { FocalAreaClassification } from './focal-area-classification.entity';
import { FocalAreaClassificationService } from './focal-area-classification.service';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([FocalAreaClassification])],
  controllers: [FocalAreaClassificationController],
  providers: [FocalAreaClassificationService],
})
export class FocalAreaClassificationModule {}
export const focalAreaClassificationRouter: RouteTree = {
  path: 'focal-area-classification',
  module: FocalAreaClassificationModule,
  children: [],
};
