import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { FocalAreaClassification } from './focal-area-classification.entity';

export class CreateFocalAreaClassificationDto extends OmitType(FocalAreaClassification, [
  'id' as const,
]) {}

export class UpdateFocalAreaClassificationDto extends PartialType(
  CreateFocalAreaClassificationDto,
) {}

export class FindManyFocalAreaClassificationParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;
}
