import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from 'src/common/base/service/base-category.service';
import { ResponseCreated, ResponseDelete, ResponseFindAll, ResponseFindOne, ResponseUpdate } from 'src/common/response';
import { In, Repository } from 'typeorm';
import { CustomBadRequestException } from '../../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../common/message/field.message';
import {
  CreateFocalAreaClassificationDto,
  FindManyFocalAreaClassificationParamDto,
  UpdateFocalAreaClassificationDto,
} from './focal-area-classification.dto';
import { FocalAreaClassification } from './focal-area-classification.entity';

@Injectable()
export class FocalAreaClassificationService extends BaseCategoryService<FocalAreaClassification> {
  constructor(
    @InjectRepository(FocalAreaClassification)
    repository: Repository<FocalAreaClassification>,
  ) {
    super(repository);
  }

  async findAll(query: FindManyFocalAreaClassificationParamDto) {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number) {
    const result = await this.checkNotExist(id, FocalAreaClassification.name);

    return new ResponseFindOne(result);
  }

  async create(payload: CreateFocalAreaClassificationDto) {
    await this.checkExist(['name', 'code'], payload);

    const data: FocalAreaClassification = await this.repository.save(payload);

    return new ResponseCreated(data);
  }

  async update(id: number, payload: UpdateFocalAreaClassificationDto) {
    await this.checkNotExist(id, FocalAreaClassification.name);
    await this.checkExist(['name', 'code'], payload, id);
    const result = await this.repository.save({
      id,
      ...payload,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]) {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
      relations: {
        focalAreas: true,
      },
    });
    

    // không thể xoá nếu có tỉnh huyện xã đang dùng phân loại trọng điểm
    for (const item of results) {
      if (item.focalAreas.length !== 0)
        throw new CustomBadRequestException(
          ContentMessage.IN_USED,
          FieldMessage.ADMINISTRATIVE_UNIT,
          true,
        );
    }

    await this.repository.softRemove(results);

    return new ResponseDelete<FocalAreaClassification>(results, ids);

  }
}
