import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { FocalArea } from '../focal-area/focal-area.entity';

@Entity()
export class FocalAreaClassification extends BaseEntity {
  @ApiProperty({
    description: 'Mã của phân loại trọng điểm',
    default: 'TRONG_DIEM',
    maxLength: 25,
  })
  @IsNotEmpty()
  @MaxLength(25)
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @ApiProperty({
    description: 'Tên của phân loại trọng điểm',
    default: 'Trọng điểm 1',
  })
  @IsNotEmpty()
  @MaxLength(100)
  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @ApiProperty({
    description: 'Mô tả phân loại trọng điểm',
    default: 'Mô tả về trọng điểm số 1',
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  description: string;

  @OneToMany(() => FocalArea, (item) => item.focalAreaClassification)
  focalAreas: FocalArea[];
}
