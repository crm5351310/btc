import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as _ from 'lodash';
import { FindOneOptions, Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../../../config/data-source.config';
import {
  CreateFocalAreaClassificationDto,
  FindManyFocalAreaClassificationParamDto,
} from '../focal-area-classification.dto';
import { FocalAreaClassification } from '../focal-area-classification.entity';
import { FocalAreaClassificationModule } from '../focal-area-classification.module';
import { FocalAreaClassificationService } from '../focal-area-classification.service';

// npm run test:run src/module/category/administrative/focal-area-classification-root/focal-area-classification
describe('FocalAreaClassificationService', () => {
  let focalAreaClassificationService: FocalAreaClassificationService;
  let focalAreaClassificationRepository: Repository<FocalAreaClassification>;
  let module: TestingModule;

  const FOCAL_AREA_CLASSIFICATION_REPOSITORY_TOKEN = getRepositoryToken(FocalAreaClassification);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        FocalAreaClassificationModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    focalAreaClassificationService = module.get<FocalAreaClassificationService>(
      FocalAreaClassificationService,
    );
    focalAreaClassificationRepository = module.get(FOCAL_AREA_CLASSIFICATION_REPOSITORY_TOKEN);
  });

  it('should be defined', () => {
    expect(focalAreaClassificationService).toBeDefined();
  });

  describe('FocalAreaClassificationService => create', () => {
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'Phân loại trọng điểm số 1',
      description: 'Mô tả',
    } as CreateFocalAreaClassificationDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<FocalAreaClassification>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<FocalAreaClassification>;

    it('Tạo mới phân loại trọng điểm thành công', async () => {
      jest.spyOn(focalAreaClassificationRepository, 'findOne').mockResolvedValue(null);
      jest
        .spyOn(focalAreaClassificationRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as FocalAreaClassification);

      const result = await focalAreaClassificationService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới phân loại trọng điểm không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(focalAreaClassificationRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery))
          return Promise.resolve({ id: 1 } as FocalAreaClassification);
        if (_.isEqual(query, nameQuery))
          return Promise.resolve({ id: 1 } as FocalAreaClassification);
        return Promise.resolve(null);
      });

      await expect(focalAreaClassificationService.create(payload)).rejects.toThrowError();
    });
  });

  describe('FocalAreaClassificationService => findOne', () => {
    it('Tìm một phân loại trọng điểm thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'Phân loại trọng điểm số 1',
        description: 'Mô tả',
      } as FocalAreaClassification;
      jest.spyOn(focalAreaClassificationRepository, 'findOne').mockResolvedValue(mockData);

      const result = await focalAreaClassificationService.findOne({ id: 1 });

      expect(focalAreaClassificationRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });
    it('Không thể tìm một đơn vị quản lý nếu ID không tồn tại', async () => {
      jest.spyOn(focalAreaClassificationRepository, 'findOne').mockResolvedValue(null);

      await expect(focalAreaClassificationService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('FocalAreaClassificationService => findAll', () => {
    it('Tìm danh sách phân loại trọng điểm thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'Phân loại trọng điểm số 1',
        description: 'Mô tả',
      } as FocalAreaClassification;
      jest.spyOn(focalAreaClassificationRepository, 'find').mockResolvedValue([mockData]);

      const result = await focalAreaClassificationService.findAll(
        {} as FindManyFocalAreaClassificationParamDto,
      );

      expect(focalAreaClassificationRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject([mockData]);
    });
  });

  describe('FocalAreaClassificationService => update', () => {
    const path = {
      id: 1,
    };
    const payload = {
      code: 'PHAN_LOAI_1',
      name: 'Phân loại trọng điểm số 1',
      description: 'Mô tả',
    } as CreateFocalAreaClassificationDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<FocalAreaClassification>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<FocalAreaClassification>;

    const idQuery = {
      where: {
        id: path.id,
      },
    } as FindOneOptions<FocalAreaClassification>;

    it('Cập nhật phân loại trọng điểm thành công', async () => {
      jest.spyOn(focalAreaClassificationRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, idQuery)) return Promise.resolve({ id: 1 } as FocalAreaClassification);
        return Promise.resolve(null);
      });
      jest.spyOn(focalAreaClassificationRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as FocalAreaClassification);

      const result = await focalAreaClassificationService.update(path, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật phân loại trọng điểm không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(focalAreaClassificationRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery))
          return Promise.resolve({ id: 2 } as FocalAreaClassification);
        if (_.isEqual(query, nameQuery))
          return Promise.resolve({ id: 2 } as FocalAreaClassification);
        return Promise.resolve(null);
      });
      jest.spyOn(focalAreaClassificationRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as FocalAreaClassification);

      await expect(focalAreaClassificationService.update(path, payload)).rejects.toThrowError();
    });

    it('Cập nhật phân loại trọng điểm không thành công nếu name hoặc code đã tồn tại', async () => {
      jest
        .spyOn(focalAreaClassificationRepository, 'findOne')
        .mockResolvedValue({ id: 1 } as FocalAreaClassification);
      jest.spyOn(focalAreaClassificationRepository, 'save').mockResolvedValue({
        id: path.id,
        ...payload,
      } as FocalAreaClassification);

      await expect(focalAreaClassificationService.update(path, payload)).rejects.toThrowError();
    });
  });

  describe('FocalAreaClassificationService => remove', () => {
    it('Xoá phân loại trọng điểm thành công', async () => {
      const mockData = {
        id: 1,
        code: 'PHAN_LOAI_1',
        name: 'Phân loại trọng điểm số 1',
        description: 'Mô tả',
        focalAreas: [] as any,
      } as FocalAreaClassification;
      jest.spyOn(focalAreaClassificationRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(focalAreaClassificationRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await focalAreaClassificationService.remove({ id: '1' });

      expect(focalAreaClassificationRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Xoá phân loại trọng điểm không thành công nếu ID không tồn tại', async () => {
      jest.spyOn(focalAreaClassificationRepository, 'find').mockResolvedValue([]);
      await expect(focalAreaClassificationService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
