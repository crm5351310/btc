import { Module } from '@nestjs/common';
import { DistrictService } from './district.service';
import { DistrictController } from './district.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../administrative-unit/administrative-unit.entity';
import { AdministrativeTitle } from '../../administrative-title';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { AdministrativeUnitModule } from '../administrative-unit/administrative-unit.module';

@Module({
  imports: [TypeOrmModule.forFeature([AdministrativeUnit, AdministrativeTitle])],
  controllers: [DistrictController],
  providers: [DistrictService],
  exports: [DistrictService],
})
export class DistrictModule {}
export const districtRouter: RouteTree = {
  path: 'district',
  module: DistrictModule,
  children: [],
};
