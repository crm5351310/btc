import { Get, Post, Body, Patch, Param, Delete, Controller } from '@nestjs/common';
import { DistrictService } from './district.service';
import { CreateAdministrativeDistrictDto, UpdateAdministrativeDistrictDto } from './district.dto';
import { PathArrayDto, PathDto } from '../../../../../common/base/class/base.class';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DISTRICT)
export class DistrictController {
  constructor(private readonly administrativeDistrictService: DistrictService) {}

  @Post()
  create(@Body() createAdministrativeDistrictDto: CreateAdministrativeDistrictDto) {
    return this.administrativeDistrictService.create(createAdministrativeDistrictDto);
  }

  @Get()
  findAll() {
    return this.administrativeDistrictService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.administrativeDistrictService.findOne(path);
  }

  @Patch(':id')
  update(
    @Param() path: PathDto,
    @Body() updateAdministrativeDistrictDto: UpdateAdministrativeDistrictDto,
  ) {
    return this.administrativeDistrictService.update(path, updateAdministrativeDistrictDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.administrativeDistrictService.remove(path);
  }
}
