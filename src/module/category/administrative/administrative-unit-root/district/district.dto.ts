import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { AdministrativeUnit } from '../administrative-unit/administrative-unit.entity';

export class CreateAdministrativeDistrictDto extends PickType(AdministrativeUnit, [
  'name',
  'code',
  'geolocation',
  'administrativeTitle',
] as const) {}

export class UpdateAdministrativeDistrictDto extends PartialType(
  PickType(AdministrativeUnit, [
    'id',
    'name',
    'code',
    'geolocation',
    'administrativeTitle',
  ] as const),
) {}
