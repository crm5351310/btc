import { Injectable } from '@nestjs/common';
import { CreateAdministrativeDistrictDto, UpdateAdministrativeDistrictDto } from './district.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { And, In, Not, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../../common/base/class/base.class';
import { SubjectMessage } from '../../../../../common/message/subject.message';
import { BaseMessage } from '../../../../../common/message/base.message';
import { StringHelper } from '../../../../../common/utils/string.util';
import { AdministrativeUnit } from '../administrative-unit/administrative-unit.entity';
import { CustomBadRequestException } from '../../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../common/message/field.message';
import { AdministrativeTitle } from '../../administrative-title';

@Injectable()
export class DistrictService extends BaseService {
  constructor(
    @InjectRepository(AdministrativeUnit)
    private readonly unitRepository: Repository<AdministrativeUnit>,

    @InjectRepository(AdministrativeTitle)
    private readonly titleRepository: Repository<AdministrativeTitle>,
  ) {
    super();
    this.name = DistrictService.name;
    this.subject = SubjectMessage.ADMINISTRATIVE_DISTRICT;
  }
  async create(createAdministrativeDistrictDto: CreateAdministrativeDistrictDto) {
    this.action = BaseMessage.CREATE;
    createAdministrativeDistrictDto = StringHelper.spaceObject(createAdministrativeDistrictDto);
    const titleExist = await this.titleRepository.findOne({
      where: {
        id: createAdministrativeDistrictDto.administrativeTitle?.id,
        administrativeLevel: {
          id: 2,
        },
      },
    });
    if (!titleExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_TITLE,
        true,
      );

    const districtDublicateName = await this.unitRepository.findOne({
      where: {
        name: createAdministrativeDistrictDto.name,
        administrativeTitle: {
          administrativeLevel: {
            id: 2,
          },
        },
      },
    });
    if (districtDublicateName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    const districDublicateCode = await this.unitRepository.findOne({
      where: {
        code: createAdministrativeDistrictDto.code,
        administrativeTitle: {
          administrativeLevel: {
            id: 2,
          },
        },
      },
    });
    if (districDublicateCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);

    const result = await this.unitRepository.save({
      ...createAdministrativeDistrictDto,
      isFocalArea: false,
      isSecurity: false,
      parent: {
        id: 1,
      },
    });
    return this.response(result);
  }

  async findAll() {
    this.action = BaseMessage.READ;
    const administrativeUnit = await this.unitRepository.find({
      order: {
        code: 'ASC',
      },
      where: {
        parent: {
          id: 1,
        },
      },
    });
    return this.response(administrativeUnit);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const administrativeUnit = await this.unitRepository.findOne({
      relations: {
        administrativeTitle: {
          administrativeLevel: true,
        },
        geolocation: true,
      },
      where: {
        id: path.id,
      },
    });
    if (!administrativeUnit)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_DISTRICT,
        true,
      );

    const count = await this.unitRepository
      .createQueryBuilder('A')
      .leftJoin('A.children', 'B')
      .leftJoin('B.populationHistory', 'C')
      .addSelect('sum(C.populationMale)', 'summaryMale')
      .addSelect('sum(C.populationFemale)', 'summaryFemale')
      .where('A.id = :id', { id: path.id })
      .getRawOne();

    return this.response({
      ...administrativeUnit,
      populationMale: count.summaryMale ? Number(count.summaryMale) : 0,
      populationFemale: count.summaryFemale ? Number(count.summaryFemale) : 0,
    });
  }

  async update(path: PathDto, updateAdministrativeDistrictDto: UpdateAdministrativeDistrictDto) {
    this.action = BaseMessage.UPDATE;
    updateAdministrativeDistrictDto = StringHelper.spaceObject(updateAdministrativeDistrictDto);
    const administrativeUnit = await this.unitRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!administrativeUnit)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_DISTRICT,
        true,
      );

    const titleExist = await this.titleRepository.findOne({
      where: {
        id: updateAdministrativeDistrictDto.administrativeTitle?.id,
        administrativeLevel: {
          id: 2,
        },
      },
    });
    if (!titleExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_TITLE,
        true,
      );

    const districtDublicateName = await this.unitRepository.findOne({
      where: {
        name: updateAdministrativeDistrictDto.name,
        parent: {
          id: 1,
        },
        id: Not(path.id),
      },
    });
    if (districtDublicateName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    const districDublicateCode = await this.unitRepository.findOne({
      where: {
        code: updateAdministrativeDistrictDto.code,
        parent: {
          id: 1,
        },
        id: Not(path.id),
      },
    });
    if (districDublicateCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);

    const result = await this.unitRepository.save({
      id: path.id,
      ...updateAdministrativeDistrictDto,
      isFocalArea: false,
      isSecurity: false,
      parent: {
        id: 1,
      },
    });
    return this.response(result);
  }

  async remove(path: PathArrayDto) {
    const pathArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const deleteArr: AdministrativeUnit[] = [];
    const units = await this.unitRepository.find({
      relations: {
        children: true,
        populationHistory: true,
      },
      where: {
        id: In(pathArr),
      },
    });

    const affected = units.reduce((pre: any, cur: AdministrativeUnit) => {
      if (cur.children.length) {
        pre += 1;
      } else {
        deleteArr.push(cur);
      }
      return pre;
    }, 0);

    if (!units.length || !deleteArr.length) {
      throw new CustomBadRequestException(
        ContentMessage.FAILURE,
        FieldMessage.ADMINISTRATIVE_DISTRICT,
        true,
      );
    }

    await this.unitRepository.softRemove(deleteArr);
    return this.response({ affected: pathArr.length - affected });
  }
}
