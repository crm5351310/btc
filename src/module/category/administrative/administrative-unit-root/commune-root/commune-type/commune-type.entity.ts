import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../../../common/base/entity/base.entity';
import { AdministrativeUnit } from '../../administrative-unit/administrative-unit.entity';

@Entity()
export class CommuneType extends BaseEntity {
  @Column('varchar', {
    length: 50,
    nullable: false,
  })
  name: string;

  @Column('varchar', {
    length: 50,
    nullable: false,
  })
  code: string;

  @Column('boolean', {
    nullable: false,
    default: false,
  })
  isDefault: boolean;

  @OneToMany(() => AdministrativeUnit, (item) => item.communeType)
  administrativeUnit: AdministrativeUnit[];
}
