import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import ResponseHelper from '../../../../../../common/utils/reponse.util';
import { SubjectMessage } from '../../../../../../common/message/subject.message';
import { contentMaxLength, ContentMessage } from '../../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../../common/message/field.message';

export class CreateCommuneTypeDto {
  @ApiProperty({
    description: 'Tên loại xã địa lý',
    default: 'Loại xã 1',
  })
  @IsNotEmpty({
    message: ResponseHelper.response(
      SubjectMessage.COMMUNE_TYPE,
      ContentMessage.REQUIRED,
      FieldMessage.NAME,
    ),
  })
  @IsString({
    message: ResponseHelper.response(
      SubjectMessage.COMMUNE_TYPE,
      ContentMessage.INVALID,
      FieldMessage.NAME,
    ),
  })
  @MaxLength(50, {
    message: ResponseHelper.response(
      SubjectMessage.COMMUNE_TYPE,
      contentMaxLength(50),
      FieldMessage.NAME,
    ),
  })
  name: string;

  @ApiProperty({
    description: 'Mã vị trí địa lý',
    default: 'Mã 1',
  })
  @IsNotEmpty({
    message: ResponseHelper.response(
      SubjectMessage.COMMUNE_TYPE,
      ContentMessage.REQUIRED,
      FieldMessage.CODE,
    ),
  })
  @IsString({
    message: ResponseHelper.response(
      SubjectMessage.COMMUNE_TYPE,
      ContentMessage.INVALID,
      FieldMessage.CODE,
    ),
  })
  @MaxLength(50, {
    message: ResponseHelper.response(
      SubjectMessage.COMMUNE_TYPE,
      contentMaxLength(50),
      FieldMessage.CODE,
    ),
  })
  code: string;
}

export class UpdateCommuneTypeDto extends PartialType(CreateCommuneTypeDto) {}
