import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CommuneTypeService } from './commune-type.service';
import { CreateCommuneTypeDto, UpdateCommuneTypeDto } from './commune-type.dto';
import { PathArrayDto, PathDto } from '../../../../../../common/base/class/base.class';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.COMMUNE_TYPE)
export class CommuneTypeController {
  constructor(private readonly communeTypeService: CommuneTypeService) {}

  @Post()
  create(@Body() createCommuneTypeDto: CreateCommuneTypeDto) {
    return this.communeTypeService.create(createCommuneTypeDto);
  }

  @Get()
  findAll() {
    return this.communeTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.communeTypeService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateCommuneTypeDto: UpdateCommuneTypeDto) {
    return this.communeTypeService.update(path, updateCommuneTypeDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.communeTypeService.remove(path);
  }
}
