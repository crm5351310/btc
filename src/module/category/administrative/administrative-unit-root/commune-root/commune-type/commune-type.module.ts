import { Module } from '@nestjs/common';
import { CommuneTypeService } from './commune-type.service';
import { CommuneTypeController } from './commune-type.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommuneType } from './commune-type.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { CommuneModule } from '../commune/commune.module';

@Module({
  imports: [TypeOrmModule.forFeature([CommuneType])],
  controllers: [CommuneTypeController],
  providers: [CommuneTypeService],
})
export class CommuneTypeModule {}
export const communeTypeRouter: RouteTree = {
  path: 'commune-type',
  module: CommuneTypeModule,
  children: [],
};
