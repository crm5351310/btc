import { Injectable } from '@nestjs/common';
import { CreateCommuneTypeDto, UpdateCommuneTypeDto } from './commune-type.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../../../common/base/class/base.class';
import { BaseMessage } from '../../../../../../common/message/base.message';
import { StringHelper } from '../../../../../../common/utils/string.util';
import { ContentMessage } from '../../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../../common/message/field.message';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../../../../common/message/subject.message';
import { CommuneType } from './commune-type.entity';

@Injectable()
export class CommuneTypeService extends BaseService {
  constructor(
    @InjectRepository(CommuneType)
    private readonly communeTypeRepository: Repository<CommuneType>,
  ) {
    super();
    this.name = CommuneTypeService.name;
    this.subject = SubjectMessage.COMMUNE_TYPE;
  }

  async create(createCommuneTypeDto: CreateCommuneTypeDto) {
    this.action = BaseMessage.CREATE;
    createCommuneTypeDto = StringHelper.spaceObject(createCommuneTypeDto);
    const geoLocationDublicateName = await this.communeTypeRepository.findOne({
      where: { name: createCommuneTypeDto.name },
    });
    if (geoLocationDublicateName)
      return this.reponseCustom(ContentMessage.EXIST, FieldMessage.NAME);

    const geoLocationDublicateCode = await this.communeTypeRepository.findOne({
      where: { code: createCommuneTypeDto.code },
    });
    if (geoLocationDublicateCode)
      return this.reponseCustom(ContentMessage.EXIST, FieldMessage.CODE);

    const geoLocations = await this.communeTypeRepository.save(createCommuneTypeDto);
    return this.response(geoLocations);
  }

  async findAll() {
    this.action = BaseMessage.READ;
    const communeTypes = await this.communeTypeRepository.find();
    return this.response(communeTypes);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const communeTypeExist = await this.communeTypeRepository.findOne({
      where: {
        id: +path.id,
      },
    });
    if (!communeTypeExist) return this.reponseCustom(ContentMessage.NOT_FOUND, null);
    return this.response(communeTypeExist);
  }

  async update(path: PathDto, updateCommuneTypeDto: UpdateCommuneTypeDto) {
    this.action = BaseMessage.UPDATE;
    updateCommuneTypeDto = StringHelper.spaceObject(updateCommuneTypeDto);
    const communeTypeExist = await this.communeTypeRepository.findOne({
      where: {
        id: path.id,
        isDefault: false,
      },
    });
    if (!communeTypeExist) return this.reponseCustom(ContentMessage.NOT_FOUND, null);

    const communeTypeDublicateName = await this.communeTypeRepository.findOne({
      where: { name: updateCommuneTypeDto.name, id: Not(path.id) },
    });
    if (communeTypeDublicateName)
      return this.reponseCustom(ContentMessage.EXIST, FieldMessage.NAME);

    const communeTypeDublicateCode = await this.communeTypeRepository.findOne({
      where: { code: updateCommuneTypeDto.code, id: Not(path.id) },
    });
    if (communeTypeDublicateCode)
      return this.reponseCustom(ContentMessage.EXIST, FieldMessage.CODE);

    const geoLocations = await this.communeTypeRepository.save({
      id: path.id,
      ...updateCommuneTypeDto,
    });
    return this.response(geoLocations);
  }

  async remove(path: PathArrayDto) {
    const pathArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const communeTypeExist = await this.communeTypeRepository.find({
      relations: {
        administrativeUnit: true,
      },
      where: {
        id: In(pathArr),
        isDefault: false,
      },
    });
    if (communeTypeExist.length != pathArr.length)
      return this.reponseCustom(ContentMessage.NOT_FOUND, null);
    for (let i = 0; i < communeTypeExist.length; i++) {
      if (communeTypeExist[i].administrativeUnit.length) {
        return this.reponseCustom(ContentMessage.IN_USED, null);
      }
    }

    const geoLocations = await this.communeTypeRepository.softRemove(communeTypeExist);
    return this.response(geoLocations);
  }
}
