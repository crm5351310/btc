import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { And, In, Not, Repository } from 'typeorm';
import { AdministrativeUnit } from '../../administrative-unit/administrative-unit.entity';
import { BaseService, PathArrayDto, PathDto } from '../../../../../../common/base/class/base.class';
import { SubjectMessage } from '../../../../../../common/message/subject.message';
import { CreateAdministrativeCommuneDto, UpdateAdministrativeCommuneDto } from './commune.dto';
import { BaseMessage } from '../../../../../../common/message/base.message';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from '../../../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../../common/message/field.message';
import { StringHelper } from '../../../../../../common/utils/string.util';
import { AdministrativeTitle } from '../../../administrative-title';
import { FocalAreaClassification } from '../../../focal-area-classification-root/focal-area-classification/focal-area-classification.entity';
import { CommuneType } from '../commune-type/commune-type.entity';
import { FocalArea } from '../../../focal-area-classification-root/focal-area/focal-area.entity';
import { GeoLocation } from '../../../geo-location';

@Injectable()
export class CommuneService extends BaseService {
  constructor(
    @InjectRepository(AdministrativeUnit)
    private readonly unitRepository: Repository<AdministrativeUnit>,
    @InjectRepository(AdministrativeTitle)
    private readonly titleRepository: Repository<AdministrativeTitle>,
    @InjectRepository(FocalAreaClassification)
    private readonly focalAreaClassificationRepository: Repository<FocalAreaClassification>,
    @InjectRepository(CommuneType)
    private readonly communeTypeRepository: Repository<CommuneType>,
    @InjectRepository(GeoLocation)
    private readonly geoLocationRepository: Repository<GeoLocation>,

    @InjectRepository(FocalArea)
    private readonly focalAreaRepository: Repository<FocalArea>,
  ) {
    super();
    this.name = CommuneService.name;
    this.subject = SubjectMessage.ADMINISTRATIVE_COMMUNE;
  }
  async create(createAdministrativeCommuneDto: CreateAdministrativeCommuneDto) {
    this.action = BaseMessage.CREATE;
    const message: string[] = [];
    let flag = 0;
    const dataA = {
      name: createAdministrativeCommuneDto.name,
      code: createAdministrativeCommuneDto.code,
      geolocation: createAdministrativeCommuneDto.geolocation,
      isSecurity: createAdministrativeCommuneDto.isSecurity,
      isFocalArea: createAdministrativeCommuneDto.isFocalArea,
      parent: createAdministrativeCommuneDto.parent,
    };
    const parentExist = await this.unitRepository.findOne({
      where: {
        id: createAdministrativeCommuneDto.parent.id,
        parent: {
          id: 1,
        },
      },
    });
    if (!parentExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_DISTRICT,
        true,
      );

    if (createAdministrativeCommuneDto.administrativeTitle?.id) {
      const titleExist = await this.titleRepository.findOne({
        where: {
          id: createAdministrativeCommuneDto.administrativeTitle.id,
          administrativeLevel: {
            id: 3,
          },
        },
      });
      if (!titleExist) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.ADMINISTRATIVE_TITLE,
          true,
        );
      } else {
        dataA['administrativeTitle'] = createAdministrativeCommuneDto.administrativeTitle;
      }
    }

    if (createAdministrativeCommuneDto.geolocation?.id) {
      const geoExist = await this.geoLocationRepository.findOne({
        where: {
          id: createAdministrativeCommuneDto.geolocation.id,
        },
      });
      if (!geoExist) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.GEO_LOCATION,
          true,
        );
      } else {
        dataA['geolocation'] = createAdministrativeCommuneDto.geolocation;
      }
    }

    if (
      createAdministrativeCommuneDto.isFocalArea &&
      createAdministrativeCommuneDto.focalAreaClassification
    ) {
      const focalAreaClassificationExist = await this.focalAreaClassificationRepository.findOne({
        where: {
          id: createAdministrativeCommuneDto.focalAreaClassification.id,
        },
      });
      if (!focalAreaClassificationExist)
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.FOCAL_AREA_CLASSIFICATION,
          true,
        );
    }

    if (createAdministrativeCommuneDto.communeType?.id) {
      const communeType = await this.communeTypeRepository.findOne({
        where: {
          id: createAdministrativeCommuneDto.communeType.id,
        },
      });
      if (!communeType) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.ADMINISTRATIVE_COMMUNE,
          true,
        );
      } else {
        dataA['communeType'] = createAdministrativeCommuneDto.communeType;
      }
    }

    const communeDublicateName = await this.unitRepository.findOne({
      where: {
        name: createAdministrativeCommuneDto.name,
        id: Not(1),
        parent: {
          id: In([createAdministrativeCommuneDto.parent.id]),
        },
      },
    });
    if (communeDublicateName) {
      message.push(FieldMessage.NAME + '.' + ContentMessage.EXIST);
      flag++;
    }

    const communeDublicateCode = await this.unitRepository.findOne({
      where: {
        code: createAdministrativeCommuneDto.code,
        id: Not(1),
        parent: {
          id: In([createAdministrativeCommuneDto.parent.id]),
        },
      },
    });
    if (communeDublicateCode) {
      message.push(FieldMessage.CODE + '.' + ContentMessage.EXIST);
      flag++;
    }

    if (flag) {
      throw new CustomBadRequestExceptionCustom(message);
    }

    const result = await this.unitRepository.save({
      ...dataA,
    });

    if (createAdministrativeCommuneDto.isFocalArea) {
      await this.focalAreaRepository.save({
        startDate: createAdministrativeCommuneDto.startDate,
        endDate: createAdministrativeCommuneDto.endDate,
        focalAreaClassification: createAdministrativeCommuneDto.focalAreaClassification,
        administrativeUnit: {
          id: result.id,
        },
      });
    }

    return this.response(result);
  }

  async findAll(param: PathDto) {
    this.action = BaseMessage.READ;
    const administrativeUnit = await this.unitRepository.find({
      relations: {
        parent: true,
      },
      order: {
        code: 'ASC',
      },
      where: {
        parent: {
          id: param.id,
        },
      },
    });
    return this.response(administrativeUnit);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const administrativeUnit = await this.unitRepository.findOne({
      relations: {
        geolocation: true,
        communeType: true,
        administrativeTitle: {
          administrativeLevel: true,
        },
      },
      where: {
        id: path.id,
      },
    });
    if (!administrativeUnit)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_COMMUNE,
        true,
      );

    const focal = await this.focalAreaClassificationRepository.findOne({
      relations: {
        focalAreas: true,
      },
      where: {
        focalAreas: {
          administrativeUnit: {
            id: path.id,
          },
        },
      },
    });

    const focalClassification: any = {
      ...administrativeUnit,
    };
    if (focal) {
      focalClassification['focalAreaClassification'] = {
        ...focal,
        startDate: focal.focalAreas[focal.focalAreas.length - 1].startDate,
        endDate: focal.focalAreas[focal.focalAreas.length - 1].endDate,
      };
      delete focalClassification.focalAreaClassification.focalAreas;
    } else {
      focalClassification['focalAreaClassification'] = null;
    }

    return this.response(focalClassification);
  }

  async update(path: PathDto, updateAdministrativeCommuneDto: UpdateAdministrativeCommuneDto) {
    this.action = BaseMessage.UPDATE;
    const message: string[] = [];
    let flag = 0;
    const dataA = {
      id: path.id,
      name: updateAdministrativeCommuneDto.name,
      code: updateAdministrativeCommuneDto.code,
      isSecurity: updateAdministrativeCommuneDto.isSecurity,
      isFocalArea: updateAdministrativeCommuneDto.isFocalArea,
      parent: updateAdministrativeCommuneDto.parent,
    };
    updateAdministrativeCommuneDto = StringHelper.spaceObject(updateAdministrativeCommuneDto);
    const parentExist = await this.unitRepository.findOne({
      where: {
        id: updateAdministrativeCommuneDto.parent?.id,
        parent: {
          id: 1,
        },
      },
    });
    if (!parentExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_DISTRICT,
        true,
      );

    if (updateAdministrativeCommuneDto.administrativeTitle?.id) {
      const titleExist = await this.titleRepository.findOne({
        where: {
          id: updateAdministrativeCommuneDto.administrativeTitle.id,
          administrativeLevel: {
            id: 3,
          },
        },
      });
      if (!titleExist) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.ADMINISTRATIVE_TITLE,
          true,
        );
      } else {
        dataA['administrativeTitle'] = updateAdministrativeCommuneDto.administrativeTitle;
      }
    }

    if (updateAdministrativeCommuneDto.geolocation?.id) {
      const geoExist = await this.geoLocationRepository.findOne({
        where: {
          id: updateAdministrativeCommuneDto.geolocation.id,
        },
      });
      if (!geoExist) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.GEO_LOCATION,
          true,
        );
      } else {
        dataA['geolocation'] = updateAdministrativeCommuneDto.geolocation;
      }
    }

    if (updateAdministrativeCommuneDto.isFocalArea) {
      const focalAreaClassificationExist = await this.focalAreaClassificationRepository.findOne({
        where: {
          id: updateAdministrativeCommuneDto.focalAreaClassification?.id,
        },
      });
      if (!focalAreaClassificationExist)
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.FOCAL_AREA_CLASSIFICATION,
          true,
        );
    }

    if (updateAdministrativeCommuneDto.communeType?.id) {
      const communeType = await this.communeTypeRepository.findOne({
        where: {
          id: updateAdministrativeCommuneDto.communeType.id,
        },
      });
      if (!communeType) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          FieldMessage.ADMINISTRATIVE_COMMUNE,
          true,
        );
      } else {
        dataA['communeType'] = updateAdministrativeCommuneDto.communeType;
      }
    }

    const communeDublicateName = await this.unitRepository.findOne({
      where: {
        name: updateAdministrativeCommuneDto.name,
        id: And(Not(1), Not(path.id)),
        parent: {
          id: updateAdministrativeCommuneDto.parent?.id,
        },
      },
    });
    if (communeDublicateName) {
      message.push(FieldMessage.NAME + '.' + ContentMessage.EXIST);
      flag++;
    }

    const communeDublicateCode = await this.unitRepository.findOne({
      where: {
        code: updateAdministrativeCommuneDto.code,
        id: And(Not(1), Not(path.id)),
        parent: {
          id: updateAdministrativeCommuneDto.parent?.id,
        },
      },
    });
    if (communeDublicateCode) {
      message.push(FieldMessage.CODE + '.' + ContentMessage.EXIST);
      flag++;
    }

    if (flag) {
      throw new CustomBadRequestExceptionCustom(message);
    }

    const result = await this.unitRepository.save({
      ...dataA,
    });

    if (updateAdministrativeCommuneDto.isFocalArea) {
      await this.focalAreaRepository.save({
        startDate: updateAdministrativeCommuneDto.startDate,
        endDate: updateAdministrativeCommuneDto.endDate,
        focalAreaClassification: updateAdministrativeCommuneDto.focalAreaClassification,
        administrativeUnit: {
          id: result.id,
        },
      });
    }

    return this.response(result);
  }

  async remove(path: PathArrayDto) {
    const pathArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const units = await this.unitRepository.find({
      where: {
        id: And(In(pathArr), Not(1)),
      },
    });

    if (units.length === 0) {
      throw new CustomBadRequestException(
        ContentMessage.FAILURE,
        FieldMessage.ADMINISTRATIVE_COMMUNE,
        true,
      );
    }

    const affected = 0; // add reduce contrant validate here

    await this.unitRepository.softRemove(units);
    // input length - number of record has relation
    return this.response({ affected: pathArr.length - affected });
  }
}
