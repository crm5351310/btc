import { IntersectionType, OmitType, PartialType } from '@nestjs/swagger';
import { AdministrativeUnit } from '../../administrative-unit/administrative-unit.entity';
import { FocalArea } from '../../../focal-area-classification-root/focal-area/focal-area.entity';

export class CreateAdministrativeCommuneDto extends IntersectionType(
  OmitType(AdministrativeUnit, ['id'] as const),
  PartialType(OmitType(FocalArea, ['id'] as const)),
) {}

export class UpdateAdministrativeCommuneDto extends PartialType(
  IntersectionType(AdministrativeUnit, OmitType(FocalArea, ['id'] as const)),
) {}
