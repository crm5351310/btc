import { Get, Post, Body, Patch, Param, Delete, Query, Controller } from '@nestjs/common';
import { CommuneService } from './commune.service';
import { CreateAdministrativeCommuneDto, UpdateAdministrativeCommuneDto } from './commune.dto';
import { PathArrayDto, PathDto } from '../../../../../../common/base/class/base.class';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.COMMUNE)
export class CommuneController {
  constructor(private readonly administrativeDistrictService: CommuneService) {}

  @Post()
  create(@Body() createAdministrativeDistrictDto: CreateAdministrativeCommuneDto) {
    return this.administrativeDistrictService.create(createAdministrativeDistrictDto);
  }

  @Get()
  findAll(@Query() param: PathDto) {
    return this.administrativeDistrictService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.administrativeDistrictService.findOne(path);
  }

  @Patch(':id')
  update(
    @Param() path: PathDto,
    @Body() updateAdministrativeDistrictDto: UpdateAdministrativeCommuneDto,
  ) {
    return this.administrativeDistrictService.update(path, updateAdministrativeDistrictDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.administrativeDistrictService.remove(path);
  }
}
