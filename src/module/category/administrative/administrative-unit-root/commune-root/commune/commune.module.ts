import { Module } from '@nestjs/common';
import { CommuneService } from './commune.service';
import { CommuneController } from './commune.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../../administrative-unit/administrative-unit.entity';
import { AdministrativeTitle } from '../../../administrative-title';
import { CommuneType } from '../commune-type/commune-type.entity';
import { FocalAreaClassification } from '../../../focal-area-classification-root/focal-area-classification/focal-area-classification.entity';
import { FocalArea } from '../../../focal-area-classification-root/focal-area/focal-area.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { AdministrativeUnitModule } from '../../administrative-unit/administrative-unit.module';
import { CommuneRootModule } from '../commune-root.module';
import { GeoLocation } from '../../../geo-location';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AdministrativeUnit,
      AdministrativeTitle,
      CommuneType,
      FocalAreaClassification,
      FocalArea,
      GeoLocation,
    ]),
  ],
  controllers: [CommuneController],
  providers: [CommuneService],
  exports: [CommuneService],
})
export class CommuneModule {}
export const communeRouter: RouteTree = {
  path: 'commune',
  module: CommuneModule,
  children: [],
};
