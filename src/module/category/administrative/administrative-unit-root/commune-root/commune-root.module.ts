import { Module } from '@nestjs/common';
import { CommuneModule, communeRouter } from './commune/commune.module';
import { CommuneTypeModule, communeTypeRouter } from './commune-type/commune-type.module';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [CommuneTypeModule, CommuneModule],
})
export class CommuneRootModule {}
export const communeRootRouter: RouteTree = {
  path: 'commune-root',
  module: CommuneRootModule,
  children: [communeRouter, communeTypeRouter],
};
