import { Module } from '@nestjs/common';
import {
  AdministrativeUnitModule,
  administrativeUnitRouter,
} from './administrative-unit/administrative-unit.module';
import { DistrictModule, districtRouter } from './district/district.module';
import { CommuneRootModule, communeRootRouter } from './commune-root/commune-root.module';
import { ProvinceModule, provinceRouter } from './province/province.module';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [AdministrativeUnitModule, DistrictModule, CommuneRootModule, ProvinceModule],
})
export class AdministrativeUnitRootModule {}
export const administrativeUnitRootRouter: RouteTree = {
  path: 'administrative-unit-root',
  module: AdministrativeUnitRootModule,
  children: [administrativeUnitRouter, communeRootRouter, districtRouter, provinceRouter],
};
