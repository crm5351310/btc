import { Controller, Get } from '@nestjs/common';
import { AdministrativeUnitService } from './administrative-unit.service';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { ApiTags } from '@nestjs/swagger';

@Controller()
@ApiTags(TagEnum.ADMINISTRATIVE_UNIT)
export class AdministrativeUnitController {
  constructor(private readonly administrativeUnitService: AdministrativeUnitService) {}

  @Get('/tree')
  findAllTree() {
    return this.administrativeUnitService.findAll();
  }

  @Get('/list')
  findAllList() {
    return this.administrativeUnitService.findAllList();
  }
}
