import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  ValidateNested,
} from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Department } from '../../../../system/department-root/department/department.entity';
import { AdministrativeTitle } from '../../administrative-title/administrative-title.entity';
import { FocalArea } from '../../focal-area-classification-root/focal-area/focal-area.entity';
import { GeoLocation } from '../../geo-location/geo-location.entity';
import { PopulationHistory } from '../../population-history/population-history.entity';
import { CommuneType } from '../commune-root/commune-type/commune-type.entity';
import { Organization } from '../../../organization-root/organization/organization.entity';
import { MilitiaUnit } from '../../../../category/militia-defense-unit/militia-unit/entities';
import { MilitaryUnit } from '../../../../../module/category/military-unit-root/military-unit/entities';
import { School } from '../../../school-root/school/entities/school.entity';
import { MilitaryRecruitTarget } from '../../../../../module/militaty-recruitment/military-recruit-target-root/military-recruit-target/entities/military-recruit-target.entity';
import { MilitaryRecruitSubTarget } from '../../../../../module/militaty-recruitment/military-recruit-target-root/military-recruit-sub-target/entities/military-recruit-sub-target.entity';
import { Citizen } from '../../../../citizen-management/citizen/entities';

@Entity()
@Tree('nested-set')
export class AdministrativeUnit extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    type: [],
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  // entity
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mã',
    default: 'Mã 1',
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  // entity
  @Column('varchar', {
    length: 50,
    nullable: false,
  })
  code: string;

  // swagger
  @ApiProperty({
    description: 'Tên cấp hành chính',
    type: RelationTypeBase,
  })
  // validate
  @IsOptional()
  @ValidateNested()
  @Type(() => PickType(AdministrativeTitle, ['id']))
  @ManyToOne(() => AdministrativeTitle, (item) => item.administrativeUnit)
  @JoinColumn()
  administrativeTitle: AdministrativeTitle;

  // swagger
  @ApiProperty({
    description: 'Vị trí địa lý',
    type: RelationTypeBase,
  })
  // validate
  @IsOptional()
  @ValidateNested()
  @Type(() => PickType(GeoLocation, ['id']))
  @ManyToOne(() => GeoLocation, (item) => item.administrativeUnit)
  @JoinColumn()
  geolocation: GeoLocation;

  // swagger
  @ApiProperty({
    description: 'Loại xã',
    type: RelationTypeBase,
  })
  // validate
  @IsOptional()
  @ValidateNested()
  @Type(() => PickType(CommuneType, ['id']))
  @ManyToOne(() => CommuneType, (item) => item.administrativeUnit)
  @JoinColumn()
  communeType: CommuneType;

  // swagger
  @ApiProperty({
    description: 'isFocalArea',
    default: false,
  })
  // validate
  @IsNotEmpty()
  @IsBoolean()
  @Column('boolean')
  isSecurity: boolean;

  // swagger
  @ApiProperty({
    description: 'isFocalArea',
    default: false,
  })
  // validate
  @IsNotEmpty()
  @IsBoolean()
  @Column('boolean')
  isFocalArea: boolean;

  // swagger
  @ApiProperty({
    description: 'parent',
    type: RelationTypeBase,
  })
  // validate
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  // entity
  @TreeParent()
  parent: AdministrativeUnit;

  @TreeChildren()
  children: AdministrativeUnit[];

  @OneToMany(() => Department, (item) => item.administrativeUnit)
  department: Department[];

  @OneToMany(() => PopulationHistory, (item) => item.administrativeUnit)
  populationHistory: PopulationHistory[];

  @OneToMany(() => FocalArea, (item) => item.administrativeUnit)
  focalAreas: FocalArea[];

  @OneToMany(() => Organization, (item) => item.administrativeUnit)
  organizations: Organization[];

  @OneToMany(() => MilitiaUnit, (militiaUnit) => militiaUnit.administrativeUnit)
  militiaUnits: MilitiaUnit[];

  @OneToMany(() => MilitaryUnit, (militaryUnit) => militaryUnit.administrativeUnit)
  militaryUnits: MilitaryUnit[];

  @OneToMany(() => School, (school) => school.administrativeUnit)
  schools: School[];

  @OneToMany(
    () => MilitaryRecruitTarget,
    (militaryRecruitTarget) => militaryRecruitTarget.administrativeUnit,
  )
  militaryRecruitTarget: MilitaryRecruitTarget;

  @OneToMany(
    () => MilitaryRecruitSubTarget,
    (militaryRecruitSubTarget) => militaryRecruitSubTarget.administrativeUnit,
  )
  militaryRecruitSubTarget: MilitaryRecruitSubTarget;

  @OneToMany(() => Citizen, citizen => citizen.administrativeUnit)
  citizens: Citizen[];
}
