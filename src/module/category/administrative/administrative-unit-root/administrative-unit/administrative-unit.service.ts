import { Injectable } from '@nestjs/common';
import { BaseService, PathDto } from '../../../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, TreeRepository } from 'typeorm';
import { SubjectMessage } from '../../../../../common/message/subject.message';
import { AdministrativeUnit } from './administrative-unit.entity';
import { BaseMessage } from '../../../../../common/message/base.message';
import { CustomBadRequestException } from '../../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../common/message/field.message';

@Injectable()
export class AdministrativeUnitService extends BaseService {
  constructor(
    @InjectRepository(AdministrativeUnit)
    private readonly administrativeUnitRepository: Repository<AdministrativeUnit>,
    @InjectRepository(AdministrativeUnit)
    private readonly administrativeUnitTreepository: TreeRepository<AdministrativeUnit>,
  ) {
    super();
    this.name = AdministrativeUnitService.name;
    this.subject = SubjectMessage.ADMINISTRATIVE_UNIT;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const result = await this.administrativeUnitTreepository.findTrees();
    return this.response(result);
  }

  async findAllList() {
    this.action = BaseMessage.READ;
    const result = await this.administrativeUnitRepository.find({
      relations: {
        parent: true,
        administrativeTitle: {
          administrativeLevel: true,
        },
      },
      order: {
        code: 'ASC',
      },
    });
    return this.response(result);
  }
  async findOne(path: PathDto) {
    const result = await this.administrativeUnitRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_UNIT,
        true,
      );
    return this.response(result);
  }
}
