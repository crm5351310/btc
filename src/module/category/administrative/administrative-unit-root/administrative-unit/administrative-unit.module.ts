import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnitController } from './administrative-unit.controller';
import { CommuneTypeModule } from '../commune-root/commune-type/commune-type.module';
import { PopulationHistoryModule } from '../../population-history/population-history.module';
import { GeoLocationModule } from '../../geo-location/geo-location.module';
import { AdministrativeUnitService } from './administrative-unit.service';
import { AdministrativeUnit } from './administrative-unit.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [
    TypeOrmModule.forFeature([AdministrativeUnit]),
    GeoLocationModule,
    CommuneTypeModule,
    PopulationHistoryModule,
  ],
  controllers: [AdministrativeUnitController],
  providers: [AdministrativeUnitService],
  exports: [AdministrativeUnitService],
})
export class AdministrativeUnitModule {}
export const administrativeUnitRouter: RouteTree = {
  path: 'administrative-unit',
  module: AdministrativeUnitModule,
  children: [],
};
