import { PartialType } from '@nestjs/swagger';

export class CreateAdministrativeUnitDto {}

export class UpdateAdministrativeUnitDto extends PartialType(CreateAdministrativeUnitDto) {}
