import { Module } from '@nestjs/common';
import { ProvinceService } from './province.service';
import { ProvinceController } from './province.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../administrative-unit/administrative-unit.entity';
import { GeoLocation } from '../../geo-location';
import { AdministrativeTitle } from '../../administrative-title';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { DistrictModule } from '../district/district.module';

@Module({
  imports: [TypeOrmModule.forFeature([AdministrativeUnit, GeoLocation, AdministrativeTitle])],
  controllers: [ProvinceController],
  providers: [ProvinceService],
})
export class ProvinceModule {}
export const provinceRouter: RouteTree = {
  path: 'province',
  module: ProvinceModule,
  children: [],
};
