import { Controller, Body, Patch, Get } from '@nestjs/common';
import { ProvinceService } from './province.service';
import { UpdateAdministrativeProvinceDto } from './province.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.PROVINCE)
export class ProvinceController {
  constructor(private readonly provinceService: ProvinceService) {}
  @Patch()
  update(@Body() updateAdministrativeProvinceDto: UpdateAdministrativeProvinceDto) {
    return this.provinceService.update(updateAdministrativeProvinceDto);
  }

  @Get()
  findOne() {
    return this.provinceService.findOne();
  }
}
