import { Injectable } from '@nestjs/common';
import { UpdateAdministrativeProvinceDto } from './province.dto';
import { BaseService, PathDto } from '../../../../../common/base/class/base.class';
import { BaseMessage } from '../../../../../common/message/base.message';
import { StringHelper } from '../../../../../common/utils/string.util';
import { CustomBadRequestException } from '../../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../../common/message/content.message';
import { FieldMessage } from '../../../../../common/message/field.message';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../administrative-unit/administrative-unit.entity';
import { AdministrativeTitle } from '../../administrative-title';
import { SubjectMessage } from '../../../../../common/message/subject.message';
import { GeoLocation } from '../../geo-location';

@Injectable()
export class ProvinceService extends BaseService {
  constructor(
    @InjectRepository(AdministrativeUnit)
    private readonly unitRepository: Repository<AdministrativeUnit>,

    @InjectRepository(AdministrativeTitle)
    private readonly titleRepository: Repository<AdministrativeTitle>,

    @InjectRepository(GeoLocation)
    private readonly geoLocationRepository: Repository<GeoLocation>,
  ) {
    super();
    this.name = ProvinceService.name;
    this.subject = SubjectMessage.ADMINISTRATIVE_PROVINCE;
  }
  async update(updateAdministrativeProvinceDto: UpdateAdministrativeProvinceDto) {
    this.action = BaseMessage.UPDATE;
    updateAdministrativeProvinceDto = StringHelper.spaceObject(updateAdministrativeProvinceDto);
    const titleExist = await this.titleRepository.findOne({
      where: {
        id: updateAdministrativeProvinceDto.administrativeTitle?.id,
        administrativeLevel: {
          id: 1,
        },
      },
    });
    if (!titleExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.ADMINISTRATIVE_TITLE,
        true,
      );

    const geoLocationExist = await this.geoLocationRepository.findOne({
      where: {
        id: updateAdministrativeProvinceDto.geolocation?.id,
      },
    });
    if (!geoLocationExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.GEO_LOCATION,
        true,
      );

    const result = await this.unitRepository.save({
      id: 1,
      ...updateAdministrativeProvinceDto,
      isFocalArea: false,
      isSecurity: false,
    });
    return this.response(result);
  }

  async findOne() {
    this.action = BaseMessage.READ;
    const administrativeUnit = await this.unitRepository.findOne({
      relations: {
        administrativeTitle: true,
        geolocation: true,
      },
      where: {
        id: 1,
      },
    });

    const count = await this.unitRepository
      .createQueryBuilder('A')
      .leftJoin('A.children', 'B')
      .leftJoin('B.children', 'B1')
      .leftJoin('B1.populationHistory', 'C')
      .addSelect('sum(C.populationMale)', 'summaryMale')
      .addSelect('sum(C.populationFemale)', 'summaryFemale')
      .where('A.id = :id', { id: 1 })
      .getRawOne();

    return this.response({
      ...administrativeUnit,
      populationMale: count.summaryMale ? Number(count.summaryMale) : 0,
      populationFemale: count.summaryFemale ? Number(count.summaryFemale) : 0,
    });
  }
}
