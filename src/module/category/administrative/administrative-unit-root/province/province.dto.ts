import { PartialType, PickType } from '@nestjs/swagger';
import { AdministrativeUnit } from '../administrative-unit/administrative-unit.entity';

export class CreateAdministrativeProvinceDto extends PickType(AdministrativeUnit, [
  'id',
  'name',
  'code',
  'geolocation',
  'administrativeTitle',
] as const) {}

export class UpdateAdministrativeProvinceDto extends PartialType(
  PickType(AdministrativeUnit, ['name', 'code', 'geolocation', 'administrativeTitle'] as const),
) {}
