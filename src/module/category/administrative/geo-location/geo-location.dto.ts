import { OmitType, PartialType } from '@nestjs/swagger';
import { GeoLocation } from './geo-location.entity';

export class CreateGeoLocationDto extends OmitType(GeoLocation, ['isDefault', 'id'] as const) {}

export class UpdateGeoLocationDto extends PartialType(CreateGeoLocationDto) {}
