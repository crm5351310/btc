import { Module } from '@nestjs/common';
import { GeoLocationService } from './geo-location.service';
import { GeoLocationController } from './geo-location.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GeoLocation } from './geo-location.entity';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';

@Module({
  imports: [TypeOrmModule.forFeature([GeoLocation])],
  controllers: [GeoLocationController],
  providers: [GeoLocationService],
})
export class GeoLocationModule {}
export const geoLocationRouter: RouteTree = {
  path: 'geo-location',
  module: GeoLocationModule,
  children: [],
};
