import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { AdministrativeUnit } from '../administrative-unit-root/administrative-unit/administrative-unit.entity';

@Entity()
export class GeoLocation extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên vị trí địa lý',
    default: 'Vị trí 1',
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  // entity
  @Column('varchar', {
    length: 50,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mã vị trí địa lý',
    default: 'Mã 1',
  })
  // validate
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  // entity
  @Column('varchar', {
    length: 50,
    nullable: false,
  })
  code: string;

  @Column('boolean', {
    nullable: false,
    default: false,
  })
  isDefault: boolean;

  @OneToMany(() => AdministrativeUnit, (item) => item.geolocation)
  administrativeUnit: AdministrativeUnit[];
}
