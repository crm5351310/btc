import { GeoLocation } from './geo-location.entity';
import { Injectable } from '@nestjs/common';
import { CreateGeoLocationDto, UpdateGeoLocationDto } from './geo-location.dto';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { Repository, Not, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { StringHelper } from '../../../../common/utils/string.util';
import { FieldMessage } from '../../../../common/message/field.message';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';

@Injectable()
export class GeoLocationService extends BaseService {
  constructor(
    @InjectRepository(GeoLocation)
    private readonly geoLocationRepository: Repository<GeoLocation>,
  ) {
    super();
    this.name = GeoLocationService.name;
    this.subject = SubjectMessage.GEO_LOCATION;
  }

  async create(createGeoLocationDto: CreateGeoLocationDto) {
    this.action = BaseMessage.CREATE;
    createGeoLocationDto = StringHelper.spaceObject(createGeoLocationDto);
    const geoLocationDublicateName = await this.geoLocationRepository.findOne({
      where: { name: createGeoLocationDto.name },
    });
    if (geoLocationDublicateName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    const geoLocationDublicateCode = await this.geoLocationRepository.findOne({
      where: { code: createGeoLocationDto.code },
    });
    if (geoLocationDublicateCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);

    const geoLocations = await this.geoLocationRepository.save(createGeoLocationDto);
    return this.response(geoLocations);
  }

  async findAll() {
    this.action = BaseMessage.READ;
    const geoLocations = await this.geoLocationRepository.find();
    return this.response(geoLocations);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const geoLocationExist = await this.geoLocationRepository.findOne({
      where: {
        id: +path.id,
      },
    });
    if (!geoLocationExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.GEO_LOCATION,
        true,
      );
    return this.response(geoLocationExist);
  }

  async update(path: PathDto, updateGeoLocationDto: UpdateGeoLocationDto) {
    this.action = BaseMessage.UPDATE;
    updateGeoLocationDto = StringHelper.spaceObject(updateGeoLocationDto);
    const geoLocationExist = await this.geoLocationRepository.findOne({
      where: {
        id: path.id,
        isDefault: false,
      },
    });
    if (!geoLocationExist)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.GEO_LOCATION,
        true,
      );

    const geoLocationDublicateName = await this.geoLocationRepository.findOne({
      where: { name: updateGeoLocationDto.name, id: Not(path.id) },
    });
    if (geoLocationDublicateName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    const geoLocationDublicateCode = await this.geoLocationRepository.findOne({
      where: { code: updateGeoLocationDto.code, id: Not(path.id) },
    });
    if (geoLocationDublicateCode)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.CODE, false);

    const geoLocations = await this.geoLocationRepository.save({
      id: path.id,
      ...updateGeoLocationDto,
    });
    return this.response(geoLocations);
  }

  async remove(path: PathArrayDto) {
    const pathArr = path.id.split(',');
    this.action = BaseMessage.DELETE;
    const geoLocationExist = await this.geoLocationRepository.find({
      relations: {
        administrativeUnit: true,
      },
      where: {
        id: In(pathArr),
        isDefault: false,
      },
    });
    if (geoLocationExist.length != pathArr.length)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.GEO_LOCATION,
        true,
      );

    for (let i = 0; i < geoLocationExist.length; i++) {
      if (geoLocationExist[i].administrativeUnit.length) {
        throw new CustomBadRequestException(
          ContentMessage.IN_USED,
          FieldMessage.ADMINISTRATIVE_UNIT,
          true,
        );
      }
    }

    const geoLocations = await this.geoLocationRepository.softRemove(geoLocationExist);
    return this.response(geoLocations);
  }
}
