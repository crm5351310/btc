import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CreateGeoLocationDto, UpdateGeoLocationDto } from './geo-location.dto';
import { GeoLocationService } from './geo-location.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.GEO_LOCATION)
export class GeoLocationController {
  constructor(private readonly geoLocationService: GeoLocationService) {}

  @Post()
  create(@Body() createGeoLocationDto: CreateGeoLocationDto) {
    return this.geoLocationService.create(createGeoLocationDto);
  }

  @Get()
  findAll() {
    return this.geoLocationService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.geoLocationService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateGeoLocationDto: UpdateGeoLocationDto) {
    return this.geoLocationService.update(path, updateGeoLocationDto);
  }

  @Delete(':id')
  remove(@Param() path: PathArrayDto) {
    return this.geoLocationService.remove(path);
  }
}
