import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MaxLength } from 'class-validator';
import { BaseCategoryEntity } from '../../../../common/base/entity/base-category.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../citizen-process-management/citizen-process/entities';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';
import { Process } from '../../../citizen-process-management/category/process/entities/process.entity';

@Entity()
export class Reason extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
    required: false,
  })
  // validate
  @IsOptional()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;

  @ApiProperty({
    description: "Quá trình của công dân",
    example: { id: 1 },
  })
  @IsObjectRelation()
  @ManyToOne(() => Process, { nullable: false })
  process: Process;
}
