import { Injectable } from '@nestjs/common';
import { CreateReasonDto } from './dto/create-reason.dto';
import { UpdateReasonDto } from './dto/update-reason.dto';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import { Reason } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { FindAllReasonDto } from './dto/find-all-reason.dto';

@Injectable()
export class ReasonService extends BaseCategoryService<Reason> {
  constructor(
    @InjectRepository(Reason)
    repository: Repository<Reason>,
  ) {
    super(repository);
  }
  async create(createReasonDto: CreateReasonDto): Promise<ResponseCreated<Reason>> {
    await this.checkExist(['code', 'name'], createReasonDto);
    const reason: Reason = await this.repository.save(createReasonDto);

    return new ResponseCreated(reason);
  }

  async findAll(query: FindAllReasonDto): Promise<ResponseFindAll<Reason>> {
    const [results, total] = await this.repository.findAndCount({
      where: {
        process: {
          id: query.processId,
        }
      },
      skip: query.limit && query.offset,
      take: query.limit,
      relations: {
        process: true,
      }
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Reason>> {
    const result = await this.checkNotExist(id, Reason.name);

    return new ResponseFindOne(result);
  }

  async update(id: number, updateReasonDto: UpdateReasonDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Reason.name);
    await this.checkExist(['name', 'code'], updateReasonDto, id);
    const result = await this.repository.save({
      id,
      ...updateReasonDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Reason>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete(results, ids);
  }
}
