import { Test, TestingModule } from '@nestjs/testing';
import { ReasonService } from './reason.service';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import { Reason } from './entities';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { CreateReasonDto } from './dto/create-reason.dto';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { UpdateReasonDto } from './dto/update-reason.dto';

describe('ReasonService', () => {
  let service: ReasonService;
  let repository: Repository<Reason>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReasonService,
        {
          provide: getRepositoryToken(Reason),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<ReasonService>(ReasonService);
    repository = module.get(getRepositoryToken(Reason));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        reasons.find((value) => value.id === (options.where as FindOptionsWhere<Reason>).id) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return reasons;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [reasons, reasons.length];
    });

    jest.spyOn(repository, 'exist').mockImplementation(async (options: FindManyOptions<Reason>) => {
      return reasons.some(
        (e) =>
          e.code == (options.where as FindOptionsWhere<Reason>).code ||
          e.name == (options.where as FindOptionsWhere<Reason>).name,
      );
    });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: Reason) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: reasons.length,
        };
      }
    });
  });

  const reasons = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as Reason[];

  describe('ReasonService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateReasonDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });

    // it('Fail nếu trùng trường code hoặc name', async () => {
    //   const payload = {
    //     code: 'code',
    //     name: 'name1',
    //     description: 'Mô tả',
    //   } as CreateReasonDto;
    //   expect(service.create(payload)).rejects.toThrowError();
    // });
  });

  describe('ReasonService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({processId: 1});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(reasons.length);
    });
  });

  describe('ReasonService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('ReasonService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateReasonDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateReasonDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('ReasonService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
