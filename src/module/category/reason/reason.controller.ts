import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseArrayPipe,
} from '@nestjs/common';
import { ReasonService } from './reason.service';
import { CreateReasonDto } from './dto/create-reason.dto';
import { UpdateReasonDto } from './dto/update-reason.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { FindAllReasonDto } from './dto/find-all-reason.dto';

@Controller()
@ApiTags(TagEnum.REASON)
export class ReasonController {
  constructor(private readonly reasonService: ReasonService) { }

  @Post()
  create(@Body() createReasonDto: CreateReasonDto) {
    return this.reasonService.create(createReasonDto);
  }

  @Get()
  findAll(@Query() query: FindAllReasonDto) {
    return this.reasonService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reasonService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReasonDto: UpdateReasonDto) {
    return this.reasonService.update(+id, updateReasonDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.reasonService.remove(ids);
  }
}
