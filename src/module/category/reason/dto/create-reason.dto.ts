import { OmitType } from '@nestjs/swagger';
import { Reason } from '../entities';

export class CreateReasonDto extends OmitType(Reason, ['id']) {}
