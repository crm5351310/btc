import { IsNumber } from "class-validator";
import { BaseQueryDto } from "../../../../common/base/dto/base-query.dto";
import { ApiProperty } from "@nestjs/swagger";

export class FindAllReasonDto extends BaseQueryDto {
  @ApiProperty({
    description: "Id quá trình",
    example: 1,
  })
  @IsNumber()
  processId: number;
}