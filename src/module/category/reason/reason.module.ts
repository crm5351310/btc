import { Module } from '@nestjs/common';
import { ReasonService } from './reason.service';
import { ReasonController } from './reason.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reason } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([Reason])],
  controllers: [ReasonController],
  providers: [ReasonService],
  exports: [ReasonService],
})
export class ReasonModule {
  static readonly route: RouteTree = {
    path: 'reason',
    module: ReasonModule,
  };
}
