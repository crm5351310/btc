import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import {
  CreatePoliticalTheoryDto,
  FindManyPoliticalTheoryParamDto,
  UpdatePoliticalTheoryDto,
} from './political-theory.dto';
import { PoliticalTheoryService } from './political-theory.service';

@Controller()
@ApiTags(TagEnum.POLITICAL_THEORY)
export class PoliticalTheoryController {
  constructor(private readonly politicalTheoryService: PoliticalTheoryService) {}

  @Get('')
  async findAll(@Query() query: FindManyPoliticalTheoryParamDto) {
    const result = await this.politicalTheoryService.findAll(query);
    return result;
  }

  @Get(':id')
  async findOne(@Param('id') id: number) {
    const result = await this.politicalTheoryService.findOne(id);
    return result;
  }

  @Post('')
  async create(@Body() payload: CreatePoliticalTheoryDto) {
    return this.politicalTheoryService.create(payload);
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() payload: UpdatePoliticalTheoryDto) {
    const result = await this.politicalTheoryService.update(id, payload);
    return result;
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.politicalTheoryService.remove(ids);
  }
}
