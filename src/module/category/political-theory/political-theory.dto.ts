import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { PoliticalTheory } from './political-theory.entity';

export class CreatePoliticalTheoryDto extends OmitType(PoliticalTheory, ['id' as const]) {}

export class UpdatePoliticalTheoryDto extends PartialType(CreatePoliticalTheoryDto) {}

export class FindManyPoliticalTheoryParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset?: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit?: number;
}
