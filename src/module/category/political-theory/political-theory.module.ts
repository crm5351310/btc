import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core/router/interfaces/routes.interface';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PoliticalTheoryController } from './political-theory.controller';
import { PoliticalTheory } from './political-theory.entity';
import { PoliticalTheoryService } from './political-theory.service';

@Module({
  imports: [TypeOrmModule.forFeature([PoliticalTheory])],
  controllers: [PoliticalTheoryController],
  providers: [PoliticalTheoryService],
})
export class PoliticalTheoryModule {}
export const politicalTheoryRouter: RouteTree = {
  path: 'political-theory',
  module: PoliticalTheoryModule,
  children: [],
};
