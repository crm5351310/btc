import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { BaseEntity } from '../../../common/base/entity/base.entity';
import { Column, Entity, Index } from 'typeorm';

@Entity()
@Index(['deletedAt', 'code', 'name'], { unique: true })
export class PoliticalTheory extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'code',
    type: 'string',
    default: 'code 1',
    required: true,
  })
  // validate
  @IsNotEmpty()
  // entity
  @Column('varchar', {
    nullable: false,
    length: 25,
  })
  code: string;

  // swagger
  @ApiProperty({
    description: 'name',
    type: 'string',
    default: 'lý luận chính trị 1',
    required: true,
  })
  // validate
  @IsNotEmpty()
  // entity
  @Column('varchar', {
    nullable: false,
    length: 100,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Mô tả lý luận chính trị',
    default: 'Mô tả lý luận chính trị 1',
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { length: 1000, nullable: true })
  description?: string;
}
