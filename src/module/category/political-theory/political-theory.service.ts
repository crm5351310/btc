import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import {
  CreatePoliticalTheoryDto,
  FindManyPoliticalTheoryParamDto,
  UpdatePoliticalTheoryDto,
} from './political-theory.dto';
import { PoliticalTheory } from './political-theory.entity';

@Injectable()
export class PoliticalTheoryService extends BaseCategoryService<PoliticalTheory> {
  constructor(
    @InjectRepository(PoliticalTheory)
    repository: Repository<PoliticalTheory>,
  ) {
    super(repository);
  }

  async findAll(query: FindManyPoliticalTheoryParamDto): Promise<ResponseFindAll<PoliticalTheory>> {
    const [results, total] = await this.repository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<PoliticalTheory>> {
    const result = await this.checkNotExist(id, PoliticalTheory.name);

    return new ResponseFindOne(result);
  }

  async create(createPoliticalTheoryDto: CreatePoliticalTheoryDto): Promise<ResponseCreated<PoliticalTheory>> {
    await this.checkExist(['name', 'code'], createPoliticalTheoryDto);

    const politicalTheory: PoliticalTheory = await this.repository.save(createPoliticalTheoryDto);

    return new ResponseCreated(politicalTheory);
  }

  async update(
    id: number,
    updatePoliticalTheoryDto: UpdatePoliticalTheoryDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, PoliticalTheory.name);
    await this.checkExist(['name', 'code'], updatePoliticalTheoryDto, id);
    const result = await this.repository.save({
      id,
      ...updatePoliticalTheoryDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<PoliticalTheory>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<PoliticalTheory>(results, ids);
  }
}
