import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import {
  CreatePoliticalTheoryDto,
  FindManyPoliticalTheoryParamDto,
  UpdatePoliticalTheoryDto,
} from '../political-theory.dto';
import { PoliticalTheory } from '../political-theory.entity';
import { PoliticalTheoryModule } from '../political-theory.module';
import { PoliticalTheoryService } from '../political-theory.service';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';

//  npm run test:run src/module/category/political-theory

describe('PoliticalTheoryService', () => {
  let politicalTheoryService: PoliticalTheoryService;
  let module: TestingModule;
  let politicalTheoryRepository: Repository<PoliticalTheory>;

  const POLITICAL_THEORY_REPOSITORY_TOKEN = getRepositoryToken(PoliticalTheory);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        PoliticalTheoryModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
      ],
    }).compile();

    politicalTheoryRepository = module.get(POLITICAL_THEORY_REPOSITORY_TOKEN);
    politicalTheoryService = module.get<PoliticalTheoryService>(PoliticalTheoryService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('PoliticalTheoryService => create', () => {
    const payload = {
      code: 'code',
      name: 'tên',
      description: 'Mô tả',
    } as CreatePoliticalTheoryDto;

    it('Tạo mới lí luận chính trị thành công', async () => {
      jest
        .spyOn(politicalTheoryRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as PoliticalTheory);

      const result = await politicalTheoryService.create(payload);

      expect(politicalTheoryRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới lý luận chính trị không thành công nếu tên tồn tại', async () => {
      jest.spyOn(politicalTheoryService, 'create').mockImplementation(async () => {
        throw new CustomBadRequestException('EXIST', 'NAME', false);
      });

      await expect(politicalTheoryService.create(payload)).rejects.toThrowError();
      expect(politicalTheoryRepository.save).not.toHaveBeenCalled();
    });

    it('Tạo mới lý luận chính trị không thành công nếu code tồn tại', async () => {
      jest.spyOn(politicalTheoryService, 'create').mockImplementation(async () => {
        throw new CustomBadRequestException('EXIST', 'CODE', false);
      });

      await expect(politicalTheoryService.create(payload)).rejects.toThrowError();
      expect(politicalTheoryRepository.save).not.toHaveBeenCalled();
    });
  });

  describe('PoliticalTheoryService => findOne', () => {
    it('Tìm một lý luận chính trị thành công', async () => {
      const mockData = {
        id: 1,
        code: 'code',
        name: 'name',
      } as PoliticalTheory;
      jest.spyOn(politicalTheoryRepository, 'findOne').mockResolvedValue(mockData);

      const result = await politicalTheoryService.findOne({
        id: 1,
      } as PathDto);

      expect(politicalTheoryRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một lý luận chính trị nếu ID không tồn tại', async () => {
      jest.spyOn(politicalTheoryRepository, 'findOne').mockResolvedValue(null);
      await expect(politicalTheoryService.findOne({ id: 1 } as PathDto)).rejects.toThrowError();
    });
  });

  describe('PoliticalTheoryService => findAll', () => {
    it('Tìm danh lý luận chính trị thành công', async () => {
      const mockData = [
        { id: 1, code: 'code 1', name: 'name 1' },
        { id: 2, code: 'code 2', name: 'name 2' },
        { id: 3, code: 'code 3', name: 'name 3' },
      ] as PoliticalTheory[];
      const query: FindManyPoliticalTheoryParamDto = {
        offset: 0,
        limit: 10,
      };
      jest.spyOn(politicalTheoryRepository, 'find').mockResolvedValue(mockData);
      jest.spyOn(politicalTheoryRepository, 'count').mockResolvedValue(3);

      const result = await politicalTheoryService.findAll(query);

      expect(politicalTheoryRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('PoliticalTheoryService => update', () => {
    it('Cập nhật trường code lý luận chính trị thành công', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        code: 'newCode',
      } as UpdatePoliticalTheoryDto;

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as PoliticalTheory,
      );

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(politicalTheoryRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as PoliticalTheory);

      const result = await politicalTheoryService.update(path, updatePayload);

      expect(result.statusCode).toBe(200);
      expect(result.data).toEqual(expect.objectContaining(updatePayload));
      expect(politicalTheoryRepository.save).toHaveBeenCalledWith({
        id: path.id,
        ...updatePayload,
      });
    });
    it('Cập nhật trường name lý luận chính trị thành công', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        name: 'newName',
      } as UpdatePoliticalTheoryDto;

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as PoliticalTheory,
      );

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(politicalTheoryRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as PoliticalTheory);

      const result = await politicalTheoryService.update(path, updatePayload);

      expect(result.statusCode).toBe(200);
      expect(result.data).toEqual(expect.objectContaining(updatePayload));
      expect(politicalTheoryRepository.save).toHaveBeenCalledWith({
        id: path.id,
        ...updatePayload,
      });
    });

    it('Cập nhật lý luận chính trị không thành công vì trùng code', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        code: 'newCode',
      } as UpdatePoliticalTheoryDto;

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as PoliticalTheory,
      );

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => {
        throw new CustomBadRequestException('EXIST', 'CODE', false);
      });

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(politicalTheoryRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as PoliticalTheory);

      await expect(politicalTheoryService.update(path, updatePayload)).rejects.toThrowError();
      expect(politicalTheoryRepository.save).not.toHaveBeenCalled();
    });

    it('Cập nhật lý luận chính trị không thành công vì trùng name', async () => {
      const path: PathDto = { id: 1 };
      const updatePayload = {
        name: 'newName',
      } as UpdatePoliticalTheoryDto;

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(
        async () =>
          ({
            id: path.id,
          }) as PoliticalTheory,
      );

      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => {
        throw new CustomBadRequestException('EXIST', 'NAME', false);
      });
      jest.spyOn(politicalTheoryRepository, 'findOne').mockImplementationOnce(async () => null);

      jest.spyOn(politicalTheoryRepository, 'save').mockResolvedValue({
        id: path.id,
        ...updatePayload,
      } as PoliticalTheory);

      await expect(politicalTheoryService.update(path, updatePayload)).rejects.toThrowError();
      expect(politicalTheoryRepository.save).not.toHaveBeenCalled();
    });
  });

  describe('PoliticalTheoryService => remove', () => {
    it('Xoá một lý luận chính trị thành công', async () => {
      const mockData = { id: 1 } as PoliticalTheory;
      jest.spyOn(politicalTheoryRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(politicalTheoryRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await politicalTheoryService.remove({
        id: '1',
      } as PathArrayDto);

      expect(politicalTheoryRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một lý luận chính trị nếu ID không tồn tại', async () => {
      jest.spyOn(politicalTheoryRepository, 'find').mockResolvedValue([]);
      await expect(politicalTheoryService.remove({ id: '1' })).rejects.toThrowError();
    });
  });
});
