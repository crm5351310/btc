import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCategoryService } from '../../../common/base/service/base-category.service';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import {
  CreateMilitiaAndSelfDefenceTypeDto,
  QueryMilitiaAndSelfDefenceTypeDto,
  UpdateMilitiaAndSelfDefenceTypeDto,
} from './dto';
import { MilitiaAndSelfDefenseType } from './entities';

@Injectable()
export class MilitiaAndSelfDefenseTypeService extends BaseCategoryService<MilitiaAndSelfDefenseType> {
  constructor(
    @InjectRepository(MilitiaAndSelfDefenseType)
    private militiaAndSelfDefenceTypeRepository: Repository<MilitiaAndSelfDefenseType>,
  ) {
    super(militiaAndSelfDefenceTypeRepository);
  }

  async create(createMilitiaAndSelfDefenceTypeDto: CreateMilitiaAndSelfDefenceTypeDto) {
    await this.checkExist(['name', 'code'], createMilitiaAndSelfDefenceTypeDto);

    const createMilitiaAndSelfDefenceType: MilitiaAndSelfDefenseType =
      await this.militiaAndSelfDefenceTypeRepository.save(createMilitiaAndSelfDefenceTypeDto);

    return new ResponseCreated(createMilitiaAndSelfDefenceType);
  }

  async findAll(
    query: QueryMilitiaAndSelfDefenceTypeDto,
  ): Promise<ResponseFindAll<MilitiaAndSelfDefenseType>> {
    const [results, total] = await this.militiaAndSelfDefenceTypeRepository.findAndCount({
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<MilitiaAndSelfDefenseType>> {
    const result = await this.checkNotExist(id, MilitiaAndSelfDefenseType.name);

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateMilitiaAndSelfDefenceTypeDtoDto: UpdateMilitiaAndSelfDefenceTypeDto,
  ): Promise<ResponseUpdate> {
    await this.checkNotExist(id, MilitiaAndSelfDefenseType.name);
    await this.checkExist(['name', 'code'], updateMilitiaAndSelfDefenceTypeDtoDto, id);
    const result = await this.militiaAndSelfDefenceTypeRepository.save({
      id,
      ...updateMilitiaAndSelfDefenceTypeDtoDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<MilitiaAndSelfDefenseType>> {
    const results = await this.militiaAndSelfDefenceTypeRepository.find({
      where: {
        id: In(ids),
      },
    });
    await this.militiaAndSelfDefenceTypeRepository.softRemove(results);
    return new ResponseDelete<MilitiaAndSelfDefenseType>(results, ids);
  }
}
