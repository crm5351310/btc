import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitiaAndSelfDefenseType } from './entities';
import { MilitiaAndSelfDefenseTypeController } from './militia-and-self-defence-type.controller';
import { MilitiaAndSelfDefenseTypeService } from './militia-and-self-defence-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([MilitiaAndSelfDefenseType])],
  controllers: [MilitiaAndSelfDefenseTypeController],
  providers: [MilitiaAndSelfDefenseTypeService],
})
export class MilitiaAndSelfDefenseTypeModule {
  static readonly route: RouteTree = {
    path: 'militia-and-self-defence-type',
    module: MilitiaAndSelfDefenseTypeModule,
  };
}
