import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindManyOptions, FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { MilitiaAndSelfDefenseTypeService } from './militia-and-self-defence-type.service';
import { MilitiaAndSelfDefenseType } from './entities';
import { CreateMilitiaAndSelfDefenceTypeDto } from './dto/create-militia-and-self-defence-type.dto';
import { UpdateMilitiaAndSelfDefenceTypeDto } from './dto/update-militia-and-self-defence-type.dto';

describe('MilitiaAndSelfDefenseTypeService', () => {
  let service: MilitiaAndSelfDefenseTypeService;
  let repository: Repository<MilitiaAndSelfDefenseType>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitiaAndSelfDefenseTypeService,
        {
          provide: getRepositoryToken(MilitiaAndSelfDefenseType),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<MilitiaAndSelfDefenseTypeService>(MilitiaAndSelfDefenseTypeService);
    repository = module.get(getRepositoryToken(MilitiaAndSelfDefenseType));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militiaandselfdefensetypes.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitiaAndSelfDefenseType>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militiaandselfdefensetypes;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militiaandselfdefensetypes, militiaandselfdefensetypes.length];
    });

    jest
      .spyOn(repository, 'exist')
      .mockImplementation(async (options: FindManyOptions<MilitiaAndSelfDefenseType>) => {
        return militiaandselfdefensetypes.some(
          (e) =>
            e.code == (options.where as FindOptionsWhere<MilitiaAndSelfDefenseType>).code ||
            e.name == (options.where as FindOptionsWhere<MilitiaAndSelfDefenseType>).name,
        );
      });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitiaAndSelfDefenseType) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militiaandselfdefensetypes.length,
        };
      }
    });
  });

  const militiaandselfdefensetypes = [
    {
      id: 1,
      code: 'code1',
      name: 'name1',
      description: 'mo ta 1',
    },
    {
      id: 2,
      code: 'code2',
      name: 'name2',
      description: 'mo ta 2',
    },
  ] as MilitiaAndSelfDefenseType[];

  describe('MilitiaAndSelfDefenseTypeService_create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        code: 'code',
        name: 'tên',
        description: 'Mô tả',
      } as CreateMilitiaAndSelfDefenceTypeDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('MilitiaAndSelfDefenseTypeService_findAll', () => {
    it('Tìm tất cả bản ghi', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militiaandselfdefensetypes.length);
    });
  });

  describe('MilitiaAndSelfDefenseTypeService_findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một bản ghi không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('MilitiaAndSelfDefenseTypeService_update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        code: 'code11',
        name: 'name11',
      } as UpdateMilitiaAndSelfDefenceTypeDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });

    it('Cập nhật không thành công do trùng code hoặc name', async () => {
      const id = 1;
      const payload = {
        code: 'code2',
        name: 'name2',
      } as UpdateMilitiaAndSelfDefenceTypeDto;

      expect(service.update(id, payload)).rejects.toThrowError();
    });
  });

  describe('MilitiaAndSelfDefenseTypeService_delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1, 2, 3]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
