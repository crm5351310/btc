import { Test, TestingModule } from '@nestjs/testing';
import { MilitiaAndSelfDefenseTypeController } from './militia-and-self-defence-type.controller';
import { MilitiaAndSelfDefenseTypeService } from './militia-and-self-defence-type.service';

describe('AcademicDegreeController', () => {
  let controller: MilitiaAndSelfDefenseTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MilitiaAndSelfDefenseTypeController],
      providers: [MilitiaAndSelfDefenseTypeService],
    }).compile();

    controller = module.get<MilitiaAndSelfDefenseTypeController>(
      MilitiaAndSelfDefenseTypeController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
