import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import {
  CreateMilitiaAndSelfDefenceTypeDto,
  QueryMilitiaAndSelfDefenceTypeDto,
  UpdateMilitiaAndSelfDefenceTypeDto,
} from './dto';
import { MilitiaAndSelfDefenseTypeService } from './militia-and-self-defence-type.service';

@Controller()
@ApiTags(TagEnum.MILITIA_SELF_DEFENCE_TYPE)
export class MilitiaAndSelfDefenseTypeController {
  constructor(
    private readonly militiaAndSelfDefenseTypeService: MilitiaAndSelfDefenseTypeService,
  ) {}

  @Post()
  create(
    @Body()
    createMilitiaAndSelfDefenceTypeDto: CreateMilitiaAndSelfDefenceTypeDto,
  ) {
    return this.militiaAndSelfDefenseTypeService.create(createMilitiaAndSelfDefenceTypeDto);
  }

  @Get()
  findAll(@Query() query: QueryMilitiaAndSelfDefenceTypeDto) {
    return this.militiaAndSelfDefenseTypeService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.militiaAndSelfDefenseTypeService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body()
    updateMilitiaAndSelfDefenceTypeDto: UpdateMilitiaAndSelfDefenceTypeDto,
  ) {
    return this.militiaAndSelfDefenseTypeService.update(+id, updateMilitiaAndSelfDefenceTypeDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militiaAndSelfDefenseTypeService.remove(ids);
  }
}
