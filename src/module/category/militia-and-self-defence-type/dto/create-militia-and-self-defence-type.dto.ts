import { OmitType } from '@nestjs/swagger';
import { MilitiaAndSelfDefenseType } from '../entities';

export class CreateMilitiaAndSelfDefenceTypeDto extends OmitType(MilitiaAndSelfDefenseType, [
  'id',
] as const) {}
