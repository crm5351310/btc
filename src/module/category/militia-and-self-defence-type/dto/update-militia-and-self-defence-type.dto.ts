import { PartialType } from '@nestjs/swagger';
import { CreateMilitiaAndSelfDefenceTypeDto } from './create-militia-and-self-defence-type.dto';

export class UpdateMilitiaAndSelfDefenceTypeDto extends PartialType(
  CreateMilitiaAndSelfDefenceTypeDto,
) {}
