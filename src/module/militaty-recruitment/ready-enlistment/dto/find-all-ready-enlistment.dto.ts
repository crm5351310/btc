import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { BaseQueryDto } from 'src/common/base/dto/base-query.dto';
import { AgeEnum } from 'src/common/enum/age.enum';

export class FindAllReadyEnlistmentDto extends BaseQueryDto {
  @ApiProperty({
    description: 'Từ ngày',
    required: false,
  })
  @IsOptional()
  fromDate?: Date;

  @ApiProperty({
    description: 'Đến ngày',
    required: false,
  })
  @IsOptional()
  toDate?: Date;

  @ApiProperty({
    description: 'Danh sách id của các xã',
    required: false,
  })
  @IsOptional()
  @Transform(({value}) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  administrativeUnitIds?: number[];

  @ApiProperty({
    description: 'Id giới tính',
    required: false,
  })
  @IsOptional()
  genderId?: number;

  @ApiProperty({
    description: 'Độ tuổi',
    required: false,
    type: AgeEnum,
    enum: AgeEnum,
  })
  @IsOptional()
  age?: AgeEnum;

  @ApiProperty({
    description: 'Trình độ học vấn',
    required: false,
  })
  @IsOptional()
  @Transform(({value}) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  educationLevelIds?: number[];

  @ApiProperty({
    description: 'Trình độ văn hoá',
    required: false,
  })
  @IsOptional()
  @Transform(({value}) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  generalEducationLevelIds?: number[];

  @ApiProperty({
    description: 'Họ tên',
    required: false,
  })
  @IsOptional()
  fullName?: string;

  @ApiProperty({
    description: 'CCCD/CMND',
    required: false,
  })
  @IsOptional()
  identityNumber?: string;

  @ApiProperty({
    description: 'danh sách ID quá trình',
    required: false,
  })
  @IsOptional()
  @Transform(({ value }) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  processIds?: number[];
}
