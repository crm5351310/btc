import { ApiProperty, OmitType } from '@nestjs/swagger';
import { NVQSGSTProcess } from '../../../citizen-process-management/citizen-process/nvqs-process/nvqs-gst-process/entities/nvqs-gst-process.entity';
import { Citizen } from 'src/module/citizen-management/citizen/entities';
import { IsNotEmpty } from 'class-validator';

export class CreateApproveReadyEnlistmentDto extends OmitType(NVQSGSTProcess, ['id', 'toDate', 'citizen']) {
  @ApiProperty({
    description: 'công dân',
    example: [{ id: 1 }, { id: 2 }],
    required: true,
    type: () => [Citizen],
  })
  @IsNotEmpty()
  citizens: Citizen[];
}
