import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { ReadyEnlistmentService } from './ready-enlistment.service';
import { FindAllReadyEnlistmentDto } from './dto/find-all-ready-enlistment.dto';
import { CreateCitizenDto } from 'src/module/citizen-management/citizen/dto/create-citizen.dto';
import { CreateApproveReadyEnlistmentDto } from './dto/approve-ready-enlistment.dto';

@Controller()
@ApiTags(TagEnum.READ_ENLISTMENT)
export class ReadyEnlistmentController {
  constructor(private readonly readyEnlistmentService: ReadyEnlistmentService) {}

  @Get('')
  findAll(@Query() query: FindAllReadyEnlistmentDto) {
    return this.readyEnlistmentService.findAll(query);
  }

  @Post('')
  create(@Body() createCitizenDto: CreateCitizenDto) {
    return this.readyEnlistmentService.create(createCitizenDto);
  }

  @Post('/approve')
  approve(@Body() createApproveReadyEnlistmentDto: CreateApproveReadyEnlistmentDto) {
    return this.readyEnlistmentService.approve(createApproveReadyEnlistmentDto);
  }
}
