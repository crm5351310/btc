import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { ReadyEnlistmentService } from './ready-enlistment.service';
import { ReadyEnlistmentController } from './ready-enilistment.controller';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSGSTProcessModule } from '../../citizen-process-management/citizen-process/nvqs-process/nvqs-gst-process/nvqs-gst-process.module';
import { MilitaryRecruitmentTypeModule } from '../../../module/citizen-process-management/category/military-recruitment-type/military-recruitment-type.module';
import { MilitaryRecruitmentPeriodModule } from '../military-recruit-period/military-recruit-period.module';

@Module({
  imports: [
    CitizenModule,
    NVQSGSTProcessModule,
    MilitaryRecruitmentTypeModule,
    MilitaryRecruitmentPeriodModule,
  ],
  controllers: [ReadyEnlistmentController],
  providers: [ReadyEnlistmentService],
})
export class ReadyEnlistmentModule {
  static readonly route: RouteTree = {
    path: 'ready-enlistment',
    module: ReadyEnlistmentModule,
  };
}
