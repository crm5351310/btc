import { Injectable } from '@nestjs/common';
import { CitizenService } from 'src/module/citizen-management/citizen/services/citizen.service';
import { FindAllReadyEnlistmentDto } from './dto/find-all-ready-enlistment.dto';
import { CreateCitizenDto } from 'src/module/citizen-management/citizen/dto/create-citizen.dto';
import { CreateApproveReadyEnlistmentDto } from './dto/approve-ready-enlistment.dto';
import { NVQSGSTProcessService } from '../../citizen-process-management/citizen-process/nvqs-process/nvqs-gst-process/nvqs-gst-process.service';
import { CreateNVQSGSTProcessDto } from 'src/module/citizen-process-management/citizen-process/nvqs-process/nvqs-gst-process/dto/create-nvqs-gst-process.dto';
import { MilitaryRecruitmentTypeService } from 'src/module/citizen-process-management/category/military-recruitment-type/military-recruitment-type.service';
import { MilitaryRecruitmentPeriodService } from '../military-recruit-period/military-recruit-period.service';
import { ResponseUpdate } from 'src/common/response';
@Injectable()
export class ReadyEnlistmentService {
  constructor(
    private readonly citizenService: CitizenService,
    private readonly nvqsGSTProcessService: NVQSGSTProcessService,
    private readonly militaryRecruitmentTypeService: MilitaryRecruitmentTypeService,
    private readonly militaryRecruitmentPeriodService: MilitaryRecruitmentPeriodService,
  ) {}

  async findAll(query: FindAllReadyEnlistmentDto) {
    return await this.citizenService.findCitizenReadyEnlistment(query);
  }

  async create(createCitizenDto: CreateCitizenDto) {
    const data = await this.citizenService.createCitizenReadyEnlistment(createCitizenDto);
    return data;
  }

  async approve(createApproveReadyEnlistmentDto: CreateApproveReadyEnlistmentDto) {
    // check tồn tại loại nghĩa vụ (quân đội hay công an)
    await this.militaryRecruitmentTypeService.findOne(
      createApproveReadyEnlistmentDto.militaryRecruitmentType,
    );

    // check tồn tại đợt tuyển quân
    await this.militaryRecruitmentPeriodService.findOne(
      createApproveReadyEnlistmentDto.militaryRecruitmentPeriod.id,
    );
    
    const { citizens, ...payload } = createApproveReadyEnlistmentDto;
    for (const citizen of citizens) {
      await this.nvqsGSTProcessService.create({
        citizen: { id: citizen.id },
        ...payload,
      } as CreateNVQSGSTProcessDto);
    }
    return new ResponseUpdate(citizens);
  }
}
