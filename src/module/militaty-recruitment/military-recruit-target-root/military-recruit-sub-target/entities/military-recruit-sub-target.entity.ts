import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Column, Entity, JoinTable, ManyToOne } from 'typeorm';
import { MilitaryRecruitTarget } from '../../military-recruit-target/entities/military-recruit-target.entity';
import { AdministrativeUnit } from '../../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { IsObjectRelation } from '../../../../../common/validator/is-object-relation';

@Entity()
export class MilitaryRecruitSubTarget extends BaseEntity {
  @ApiProperty({
    description: 'Giao chỉ tiêu cấp huyện',
    type: RelationTypeBase,
  })
  @IsOptional()
  @IsObjectRelation()
  @ValidateNested()
  @Type(() => PickType(MilitaryRecruitTarget, ['id']))
  @ManyToOne(
    () => MilitaryRecruitTarget,
    (militaryRecruitTarget) => militaryRecruitTarget.militaryRecruitSubTarget,
  )
  militaryRecruitTarget?: MilitaryRecruitTarget;

  @ApiProperty({
    description: 'Id huyện',
    type: RelationTypeBase,
  })
  @IsOptional()
  @IsObjectRelation()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  @ManyToOne(
    () => AdministrativeUnit,
    (administrativeUnit) => administrativeUnit.militaryRecruitSubTarget,
  )
  @JoinTable()
  administrativeUnit: AdministrativeUnit;

  @ApiProperty({
    description: 'Chỉ tiêu tuyển quân',
    default: 1,
  })
  @IsOptional()
  @IsNumber()
  @Column('int', {
    nullable: true,
  })
  numberOfPeople?: number | null;
}
