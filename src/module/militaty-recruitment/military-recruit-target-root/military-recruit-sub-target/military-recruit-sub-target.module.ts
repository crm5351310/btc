import { Module } from '@nestjs/common';
import { MilitaryRecruitSubTargetService } from './military-recruit-sub-target.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRecruitSubTarget } from './entities/military-recruit-sub-target.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryRecruitSubTarget])],
  providers: [MilitaryRecruitSubTargetService],
  exports: [MilitaryRecruitSubTargetService],
})
export class MilitaryRecruitSubTargetModule {}
