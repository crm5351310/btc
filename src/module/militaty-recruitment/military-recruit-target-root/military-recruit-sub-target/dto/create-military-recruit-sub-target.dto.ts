import { OmitType } from '@nestjs/swagger';
import { MilitaryRecruitSubTarget } from '../entities/military-recruit-sub-target.entity';

export class CreateMilitaryRecruitSubTargetDto extends OmitType(MilitaryRecruitSubTarget, [
  'id',
] as const) {}
