import { Injectable } from '@nestjs/common';
import { CreateMilitaryRecruitSubTargetDto } from './dto/create-military-recruit-sub-target.dto';
import { UpdateMilitaryRecruitSubTargetDto } from './dto/update-military-recruit-sub-target.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MilitaryRecruitSubTarget } from './entities/military-recruit-sub-target.entity';
import { In, Repository } from 'typeorm';
import { ResponseCreated } from '../../../../common/response';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';

@Injectable()
export class MilitaryRecruitSubTargetService {
  constructor(
    @InjectRepository(MilitaryRecruitSubTarget)
    private readonly repository: Repository<MilitaryRecruitSubTarget>,
  ) {}

  async create(
    createMilitaryRecruitSubTargetDto: CreateMilitaryRecruitSubTargetDto[],
  ): Promise<ResponseCreated<MilitaryRecruitSubTarget[]>> {
    const results = await this.repository.save(createMilitaryRecruitSubTargetDto);
    return new ResponseCreated(results);
  }
  async update(updateMilitaryRecruitSubTargetDto: UpdateMilitaryRecruitSubTargetDto[]) {
    for (const i of updateMilitaryRecruitSubTargetDto) {
      const isExist = this.repository.findOne({
        relations: {
          militaryRecruitTarget: true,
        },
        where: {
          id: i.id,
        },
      });

      if (!isExist) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          MilitaryRecruitSubTargetService.name,
          true,
        );
      }
    }
    const results = await this.repository.save(updateMilitaryRecruitSubTargetDto);
    return results;
  }

  async remove(ids: number[]) {
    const results = await this.repository.find({
      relations: {
        militaryRecruitTarget: true,
      },
      where: {
        militaryRecruitTarget: {
          id: In(ids),
        },
      },
    });
    await this.repository.softRemove(results);
    return results;
  }
}
