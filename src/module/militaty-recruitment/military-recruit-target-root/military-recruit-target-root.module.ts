import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { MilitaryRecruitTargetModule } from './military-recruit-target/military-recruit-target.module';
import { MilitaryRecruitSubTargetModule } from './military-recruit-sub-target/military-recruit-sub-target.module';

@Module({
  imports: [MilitaryRecruitTargetModule, MilitaryRecruitSubTargetModule],
})
export class MilitaryRecruitTargetRootModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment-target-root',
    module: MilitaryRecruitTargetRootModule,
    children: [MilitaryRecruitTargetModule.route],
  };
}
