import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseArrayPipe,
  Query,
} from '@nestjs/common';
import { MilitaryRecruitTargetService } from './military-recruit-target.service';
import { CreateMilitaryRecruitTargetDto } from './dto/create-military-recruit-target.dto';
import { UpdateMilitaryRecruitTargetDto } from './dto/update-military-recruit-target.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { QueryMilitaryRecruitTargetDto } from './dto/query-military-recruit-target.dto';
import { MilitaryUnitService } from '../../../../module/category/military-unit-root/military-unit/military-unit.service';

@Controller()
@ApiTags(TagEnum.MILITARY_RECRUITMENT_TARGET)
export class MilitaryRecruitTargetController {
  constructor(
    private readonly militaryRecruitTargetService: MilitaryRecruitTargetService,
    private readonly militaryUnitService: MilitaryUnitService,
  ) { }

  @Post()
  @ApiBody({ type: [CreateMilitaryRecruitTargetDto] })
  create(
    @Body(
      new ParseArrayPipe({
        items: CreateMilitaryRecruitTargetDto,
        transformOptions: {
          enableImplicitConversion: true,
        },
      }),
    )
    createMilitaryRecruitTargetDto: CreateMilitaryRecruitTargetDto[],
  ) {
    return this.militaryRecruitTargetService.create(createMilitaryRecruitTargetDto);
  }

  @Get()
  findAll(@Query() queryMilitaryRecruitTargetDto: QueryMilitaryRecruitTargetDto) {
    return this.militaryUnitService.findAllMilitaryRecruitmentTarget(queryMilitaryRecruitTargetDto);
  }

  @Patch()
  @ApiBody({ type: [UpdateMilitaryRecruitTargetDto] })
  update(
    @Body(
      new ParseArrayPipe({
        items: UpdateMilitaryRecruitTargetDto,
        transformOptions: {
          enableImplicitConversion: true,
        },
      }),
    )
    updateMilitaryRecruitTargetDto: UpdateMilitaryRecruitTargetDto[],
  ) {
    return this.militaryRecruitTargetService.update(updateMilitaryRecruitTargetDto);
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.militaryRecruitTargetService.remove(ids);
  }
}
