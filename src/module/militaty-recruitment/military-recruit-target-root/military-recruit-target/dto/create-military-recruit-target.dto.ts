import { OmitType } from '@nestjs/swagger';
import { MilitaryRecruitTarget } from '../entities/military-recruit-target.entity';

export class CreateMilitaryRecruitTargetDto extends OmitType(MilitaryRecruitTarget, [
  'id' as const,
]) {}
