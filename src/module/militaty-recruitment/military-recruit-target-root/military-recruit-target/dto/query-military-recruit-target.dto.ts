import { ApiProperty } from '@nestjs/swagger';
import { BaseQueryDto } from '../../../../../common/base/dto/base-query.dto';
import { IsNumber, IsOptional } from 'class-validator';

export class QueryMilitaryRecruitTargetDto extends BaseQueryDto {
  @ApiProperty({
    description: 'Id đợt tuyển quân',
    required: true,
  })
  @IsNumber()
  militaryRecruitmentPeriodId?: number;

  @ApiProperty({
    description: 'Năm',
    required: true,
  })
  @IsNumber()
  year?: number;

  @ApiProperty({
    description: 'Id huyện',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  administrativeUnitId?: number;

  @ApiProperty({
    description: 'Tên đơn vị',
    required: false,
  })
  @IsOptional()
  militaryUnitName?: string;
}
