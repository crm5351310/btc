import { MilitaryRecruitTarget } from '../entities/military-recruit-target.entity';

export class UpdateMilitaryRecruitTargetDto extends MilitaryRecruitTarget {}
