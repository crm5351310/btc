import { Module } from '@nestjs/common';
import { MilitaryRecruitTargetService } from './military-recruit-target.service';
import { MilitaryRecruitTargetController } from './military-recruit-target.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRecruitTarget } from './entities/military-recruit-target.entity';
import { RouteTree } from '@nestjs/core';
import { DistrictModule } from '../../../../module/category/administrative/administrative-unit-root/district/district.module';
import { MilitaryRecruitmentPeriodModule } from '../../military-recruit-period/military-recruit-period.module';
import { MilitaryUnitModule } from '../../../../module/category/military-unit-root/military-unit/military-unit.module';
import { CommuneModule } from '../../../../module/category/administrative/administrative-unit-root/commune-root/commune/commune.module';
import { MilitaryRecruitSubTargetModule } from '../military-recruit-sub-target/military-recruit-sub-target.module';
import { AdministrativeUnitModule } from '../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MilitaryRecruitTarget]),
    DistrictModule,
    MilitaryRecruitmentPeriodModule,
    MilitaryUnitModule,
    CommuneModule,
    MilitaryRecruitSubTargetModule,
    AdministrativeUnitModule,
  ],
  controllers: [MilitaryRecruitTargetController],
  providers: [MilitaryRecruitTargetService],
})
export class MilitaryRecruitTargetModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment-target',
    module: MilitaryRecruitTargetModule,
  };
}
