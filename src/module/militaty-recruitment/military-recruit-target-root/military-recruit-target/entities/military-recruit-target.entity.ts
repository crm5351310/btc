import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../../common/base/class/base.class';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { MilitaryRecruitmentPeriod } from '../../../../../module/militaty-recruitment/military-recruit-period/entities/military-recruit-period.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { AdministrativeUnit } from '../../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { MilitaryUnit } from '../../../../../module/category/military-unit-root/military-unit/entities';
import { MilitaryRecruitSubTarget } from '../../military-recruit-sub-target/entities/military-recruit-sub-target.entity';
import { IsObjectRelation } from '../../../../../common/validator/is-object-relation';

@Entity()
export class MilitaryRecruitTarget extends BaseEntity {
  @ApiProperty({
    description: 'Id đợt tuyển quân',
    type: RelationTypeBase,
  })
  @IsOptional()
  @IsObjectRelation()
  @ValidateNested()
  @Type(() => PickType(MilitaryRecruitmentPeriod, ['id']))
  @ManyToOne(() => MilitaryRecruitmentPeriod)
  militaryRecruitmentPeriod: MilitaryRecruitmentPeriod;

  @ApiProperty({
    description: 'Id huyện',
    type: RelationTypeBase,
  })
  @IsOptional()
  @IsObjectRelation()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  @ManyToOne(() => AdministrativeUnit)
  @JoinColumn()
  administrativeUnit: AdministrativeUnit;

  @ApiProperty({
    description: 'Id quân đội',
    type: RelationTypeBase,
  })
  @IsOptional()
  @IsObjectRelation()
  @ValidateNested()
  @Type(() => PickType(MilitaryUnit, ['id']))
  @ManyToOne(() => MilitaryUnit, (militaryUnit) => militaryUnit.militaryRecruitTarget)
  @JoinColumn()
  militaryUnit: MilitaryUnit;

  @ApiProperty({
    description: 'Chỉ tiêu tuyển quân',
    default: 1,
  })
  @IsOptional()
  @IsNumber()
  @Column('int', {
    nullable: true,
  })
  numberOfPeople?: number | null;

  @OneToMany(
    () => MilitaryRecruitSubTarget,
    (militaryRecruitSubTarget) => militaryRecruitSubTarget.militaryRecruitTarget,
  )
  militaryRecruitSubTarget?: MilitaryRecruitSubTarget[];
}
