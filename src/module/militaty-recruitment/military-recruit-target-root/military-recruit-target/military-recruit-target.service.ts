import { Injectable } from '@nestjs/common';
import { CreateMilitaryRecruitTargetDto } from './dto/create-military-recruit-target.dto';
import { MilitaryRecruitTarget } from './entities/military-recruit-target.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOptionsWhere, In, Not, Repository } from 'typeorm';
import { ResponseCreated, ResponseUpdate, ResponseDelete } from '../../../../common/response';
import { DistrictService } from '../../../../module/category/administrative/administrative-unit-root/district/district.service';
import { MilitaryRecruitmentPeriodService } from '../../military-recruit-period/military-recruit-period.service';
import { MilitaryUnitService } from '../../../../module/category/military-unit-root/military-unit/military-unit.service';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import { MilitaryUnit } from '../../../../module/category/military-unit-root/military-unit/entities';
import { UpdateMilitaryRecruitTargetDto } from './dto/update-military-recruit-target.dto';
import { AdministrativeUnit } from '../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { MilitaryRecruitmentPeriod } from '../../military-recruit-period/entities/military-recruit-period.entity';
import { CommuneService } from '../../../../module/category/administrative/administrative-unit-root/commune-root/commune/commune.service';
import { MilitaryRecruitSubTargetService } from '../military-recruit-sub-target/military-recruit-sub-target.service';
import { AdministrativeUnitService } from '../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { UpdateMilitaryRecruitSubTargetDto } from '../military-recruit-sub-target/dto/update-military-recruit-sub-target.dto';
@Injectable()
export class MilitaryRecruitTargetService {
  constructor(
    @InjectRepository(MilitaryRecruitTarget)
    private readonly repository: Repository<MilitaryRecruitTarget>,
    private readonly districtService: DistrictService,
    private readonly militaryRecruitmentPeriodService: MilitaryRecruitmentPeriodService,
    private readonly militaryUnitService: MilitaryUnitService,
    private readonly communeService: CommuneService,
    private readonly militaryRecruitmentSubTargetService: MilitaryRecruitSubTargetService,
    private readonly administrativeUnitService: AdministrativeUnitService,
  ) { }
  async create(
    dto: CreateMilitaryRecruitTargetDto[],
  ): Promise<ResponseCreated<MilitaryRecruitTarget[]>> {
    const district = (await this.districtService.findAll()).data.map((item: AdministrativeUnit) => {
      return item.id;
    });
    let communes = [];
    for (const i of district) {
      const commune = await this.communeService.findAll(i);
      communes = communes.concat(commune.data);
    }
    for (const i of dto) {
      await this.districtService.findOne(i.administrativeUnit);
      await this.militaryRecruitmentPeriodService.findOne(i.militaryRecruitmentPeriod.id);
      await this.militaryUnitService.findOne(i.militaryUnit.id);
      await this.checkUnitExist(
        'militaryUnit',
        'militaryRecruitmentPeriod',
        'administrativeUnit',
        i,
      );
    }
    const array = new Set(dto.map((item) => item.administrativeUnit.id));
    this.symmetricDifference(district, array).forEach((item: number) => {
      const administrativeUnit = new AdministrativeUnit();
      administrativeUnit.id = item;
      const militaryUnit = new MilitaryUnit();
      militaryUnit.id = dto[0].militaryUnit.id;
      const militaryRecruitmentPeriod = new MilitaryRecruitmentPeriod();
      militaryRecruitmentPeriod.id = dto[0].militaryRecruitmentPeriod.id;
      dto.push({
        administrativeUnit: administrativeUnit,
        militaryUnit: militaryUnit,
        militaryRecruitmentPeriod: militaryRecruitmentPeriod,
        numberOfPeople: null,
      });
    });
    const results = await this.repository.save(dto);
    const communeData: any[] = [];
    const uniqueData: Set<string> = new Set();

    communes.forEach((commune: AdministrativeUnit) => {
      results.forEach((target: MilitaryRecruitTarget) => {
        if (target.administrativeUnit.id === commune.parent?.id) {
          uniqueData.add(
            JSON.stringify({
              militaryRecruitTarget: {
                id: target.id,
              },
              administrativeUnit: {
                id: commune.id,
              },
              numberOfPeople: null,
            }),
          );
        }
      });
    });
    uniqueData.forEach((item: string) => {
      communeData.push(JSON.parse(item));
    });
    await this.militaryRecruitmentSubTargetService.create(communeData);

    return new ResponseCreated(results);
  }
  async update(dto: UpdateMilitaryRecruitTargetDto[]): Promise<ResponseUpdate> {
    const administrativeUnit = (await this.administrativeUnitService.findAllList()).data;
    let listDistrict: UpdateMilitaryRecruitTargetDto[] = [],
      listCommune: UpdateMilitaryRecruitSubTargetDto[] = [];
    for (const i of dto) {
      const adm = new AdministrativeUnit();
      administrativeUnit.forEach((item: AdministrativeUnit) => {
        if (item.id === i.administrativeUnit.id) {
          if (item.administrativeTitle?.administrativeLevel?.code === 'huyen') {
            listDistrict.push(i);
          }
          if (item.administrativeTitle?.administrativeLevel?.code === 'xa') {
            adm.id = i.administrativeUnit.id;
            listCommune.push({
              ...i,
              administrativeUnit: adm,
              numberOfPeople: i.numberOfPeople,
            });
          }
        }
      });
      await this.militaryRecruitmentPeriodService.findOne(i.militaryRecruitmentPeriod.id);
      await this.militaryUnitService.findOne(i.militaryUnit.id);
    }
    for (const i of listDistrict) {
      const isExist = await this.repository.findOne({
        where: {
          id: i.id,
        },
      });
      if (!isExist) {
        throw new CustomBadRequestException(
          ContentMessage.NOT_FOUND,
          MilitaryRecruitTargetService.name,
          true,
        );
      }
    }
    const resultsCommune = await this.militaryRecruitmentSubTargetService.update(listCommune);
    const resultsDistrict = await this.repository.save(listDistrict);
    return new ResponseUpdate(resultsDistrict || resultsCommune);
  }
  async remove(ids: number[]): Promise<ResponseDelete<MilitaryRecruitTarget>> {
    const results = await this.repository.find({
      relations: {
        militaryRecruitmentPeriod: true,
        militaryUnit: {
          administrativeUnit: true,
        },
      },
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<MilitaryRecruitTarget>(results, ids);
  }
  protected async checkUnitExist(
    relation1: keyof DeepPartial<MilitaryRecruitTarget>,
    relation2: keyof DeepPartial<MilitaryRecruitTarget>,
    relation3: keyof DeepPartial<MilitaryRecruitTarget>,
    dto: DeepPartial<MilitaryRecruitTarget>,
    id?: number,
  ): Promise<void> {
    const errors: string[] = [];

    const relationId1 = (dto[relation1] as { id?: number })?.id;
    const relationId2 = (dto[relation2] as { id?: number })?.id;
    const relationId3 = (dto[relation3] as { id?: number })?.id;

    if (relationId1 && relationId2) {
      const isExist = await this.repository.exist({
        where: {
          id: id ? Not(id) : undefined,
          [relation1]: { id: relationId1 },
          [relation2]: { id: relationId2 },
          [relation3]: { id: relationId3 },
        } as FindOptionsWhere<MilitaryRecruitTarget>,
      });
      if (isExist) {
        errors.push(`${relation1.toString()}.${ContentMessage.EXIST}`);
      }
    }
    if (errors.length > 0) {
      throw new CustomBadRequestExceptionCustom(errors);
    }
  }
  symmetricDifference(arr1, arr2) {
    const combinedArray = [...arr1, ...arr2];
    const uniqueElements = combinedArray.filter((value, index, array) => {
      return array.indexOf(value) === array.lastIndexOf(value);
    });
    return uniqueElements;
  }
}
