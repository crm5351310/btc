import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { ApprovalService } from './approval.service';
import { ApprovalController } from './approval.controller';
import { ProcessModule } from '../../citizen-process-management/category/process/process.module';

@Module({
  imports: [CitizenModule, ProcessModule],
  controllers: [ApprovalController],
  providers: [ApprovalService],
})
export class ApprovalModule {
  static readonly route: RouteTree = {
    path: 'approval',
    module: ApprovalModule,
  };
}
