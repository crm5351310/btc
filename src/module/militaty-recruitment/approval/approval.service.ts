import { Injectable } from '@nestjs/common';
import { CitizenService } from 'src/module/citizen-management/citizen/services/citizen.service';
import { FindAllApprovalDto } from './dto/find-all-approval.dto';
import { ListTypeEnum } from 'src/common/enum/list-type.enum';
import { ProcessService } from 'src/module/citizen-process-management/category/process/process.service';
@Injectable()
export class ApprovalService {
  constructor(
    private readonly citizenService: CitizenService,
    private readonly processService: ProcessService,
  ) {}

  async findAll(query: FindAllApprovalDto, listType: ListTypeEnum) {
    if (!query.processIds)
      query.processIds = this.processService.findProcessIdsByListType(listType);
    return await this.citizenService.findCitizenByListType(query);
  }
}
