import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { ApprovalService } from './approval.service';
import { FindAllApprovalDto } from './dto/find-all-approval.dto';
import { ListTypeEnum } from 'src/common/enum/list-type.enum';

@Controller()
@ApiTags(TagEnum.APPROVAL)
export class ApprovalController {
  constructor(private readonly ApprovalService: ApprovalService) {}

  @Get('commune')
  communeApproval(@Query() query: FindAllApprovalDto) {
    return this.ApprovalService.findAll(query, ListTypeEnum.XET_DUYET_CAP_XA);
  }

  @Get('district')
  districtApproval(@Query() query: FindAllApprovalDto) {
    return this.ApprovalService.findAll(query, ListTypeEnum.XET_DUYET_CAP_HUYEN);
  }

  @Get('enlistment')
  enlistment(@Query() query: FindAllApprovalDto) {
    return this.ApprovalService.findAll(query, ListTypeEnum.NHAP_NGU);
  }
}
