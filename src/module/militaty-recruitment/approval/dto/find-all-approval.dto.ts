import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';
import { BaseQueryDto } from 'src/common/base/dto/base-query.dto';
import { ApprovalTypeEnum } from 'src/common/enum/approval-type.enum';

export class FindAllApprovalDto extends BaseQueryDto {
  @ApiProperty({
    description: 'Danh sách ID của các xã',
    required: false,
  })
  @IsOptional()
  @Transform(({ value }) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  administrativeUnitIds?: number[];

  @ApiProperty({
    description: 'ID đợt tuyển quân',
    required: false,
  })
  @IsOptional()
  militaryRecruitmentPeriodId?: number;

  @ApiProperty({
    description: 'danh sách ID quá trình',
    required: false,
  })
  @IsOptional()
  @Transform(({ value }) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  processIds?: number[];
}
