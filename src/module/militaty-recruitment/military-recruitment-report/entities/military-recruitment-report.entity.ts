import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength } from 'class-validator';
import { Column, Entity } from 'typeorm';
import { BaseCodeEntity } from '../../../../common/base/entity/base-code.entity';

@Entity()
export class MilitaryRecruitmentReport extends BaseCodeEntity {
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 1000,
  })
  // validate
  @IsString()
  @MaxLength(1000)
  // entity
  @Column('varchar', {
    length: 1000,
    nullable: false,
  })
  name: string;

  // swagger
  @ApiProperty({
    description: 'Đường Dẫn',
    default: 'report 1',
    maxLength: 1000,
  })
  // validate
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: false })
  url: string;
}
