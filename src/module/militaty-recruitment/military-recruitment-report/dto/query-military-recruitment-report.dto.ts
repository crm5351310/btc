import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export abstract class QueryMilitaryRecruitmentReportDto {
  @ApiProperty({
    description: 'search',
    type: 'string',
    required: false,
  })
  @IsOptional()
  @IsString()
  search?: string;
}
