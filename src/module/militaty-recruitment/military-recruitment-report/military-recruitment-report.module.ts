import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRecruitmentReport } from './entities/military-recruitment-report.entity';
import { MilitaryRecruitmentReportController } from './military-recruitment-report.controller';
import { MilitaryRecruitmentReportService } from './military-recruitment-report.service';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryRecruitmentReport])],
  controllers: [MilitaryRecruitmentReportController],
  providers: [MilitaryRecruitmentReportService],
  exports: [MilitaryRecruitmentReportService],
})
export class MilitaryRecruitmentReportModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment-report',
    module: MilitaryRecruitmentReportModule,
  };
}
