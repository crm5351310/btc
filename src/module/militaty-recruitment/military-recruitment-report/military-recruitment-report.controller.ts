import { Controller, Get, Param, Query } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { QueryMilitaryRecruitmentReportDto } from './dto/query-military-recruitment-report.dto';
import { MilitaryRecruitmentReportService } from './military-recruitment-report.service';

@Controller()
@ApiTags(TagEnum.RECRUIT_REPORT)
export class MilitaryRecruitmentReportController {
  constructor(
    private readonly militaryRecruitmentReportService: MilitaryRecruitmentReportService,
  ) {}

  @Get()
  findAll(@Query() query: QueryMilitaryRecruitmentReportDto) {
    return this.militaryRecruitmentReportService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.militaryRecruitmentReportService.findOne(+id);
  }
}
