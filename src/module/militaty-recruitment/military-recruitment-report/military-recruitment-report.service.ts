import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseSimpleCategoryService } from '../../../common/base/service/base-simple-category.service';
import { ILike, Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from '../../../common/response';
import { QueryMilitaryRecruitmentReportDto } from './dto/query-military-recruitment-report.dto';
import { MilitaryRecruitmentReport } from './entities/military-recruitment-report.entity';

@Injectable()
export class MilitaryRecruitmentReportService extends BaseSimpleCategoryService<MilitaryRecruitmentReport> {
  constructor(
    @InjectRepository(MilitaryRecruitmentReport)
    repository: Repository<MilitaryRecruitmentReport>,
  ) {
    super(repository);
  }

  async findAll(
    query: QueryMilitaryRecruitmentReportDto,
  ): Promise<ResponseFindAll<MilitaryRecruitmentReport>> {
    const [results, total] = await this.repository.findAndCount({
      where:[
        {name: ILike(`%${query.search ?? ''}%`)},
        {code: ILike(`%${query.search ?? ''}%`)}
      ]
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<MilitaryRecruitmentReport>> {
    const result = await this.repository.findOne({
      where: { id: id },
    });
    return new ResponseFindOne(result);
  }
}
