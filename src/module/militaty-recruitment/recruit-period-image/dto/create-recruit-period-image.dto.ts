import { ApiProperty, OmitType } from '@nestjs/swagger';
import { RecruitPeriodImage } from '../entities/recruit-period-image.entity';

export class CreateRecruitPeriodImageDto extends OmitType(RecruitPeriodImage, [
  'id',
  'key',
  'name',
  'militaryRecruitmentPeriod'
]) {

  @ApiProperty({
    type: 'array',
    items: { type: 'string', format: 'binary' },
    description: 'Tệp cần tải lên',
    required: true,
  })
  files: any;
}
