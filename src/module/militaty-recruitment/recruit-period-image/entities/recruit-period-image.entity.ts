import {
  MaxLength
} from 'class-validator';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { MilitaryRecruitmentPeriod } from '../../military-recruit-period/entities/military-recruit-period.entity';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';

@Entity()
export class RecruitPeriodImage extends BaseEntity {
  // validator
  @MaxLength(1000)
  @Column('varchar', { length: 1000 })
  name: string;

  //entity
  @Column('varchar')
  key: string;

  @IsObjectRelation()
  @ManyToOne(() => MilitaryRecruitmentPeriod)
  militaryRecruitmentPeriod: MilitaryRecruitmentPeriod;

}
