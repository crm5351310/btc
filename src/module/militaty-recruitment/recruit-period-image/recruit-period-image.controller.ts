import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateRecruitPeriodImageDto } from './dto/create-recruit-period-image.dto';
import { QueryRecruitPeriodImageDto } from './dto/query-recruit-period-image.dto';
import { RecruitPeriodImageService } from './recruit-period-image.service';

@Controller()
@ApiTags(TagEnum.RECRUIT_PERIOD_IMAGE)
export class RecruitPeriodImageController {
  constructor(private readonly recruitPeriodImageService: RecruitPeriodImageService) {}

  @Post()
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files'))
  async uploadRecruitPeriodImages(
    @UploadedFiles() files,
    @Body() createRecruitPeriodImageDto: CreateRecruitPeriodImageDto,
  ) {
    return this.recruitPeriodImageService.create(createRecruitPeriodImageDto, files);
  }

  @Get('/military-recruitment-period/:militaryRecruitmentPeriodId')
  findAll(
    @Param('militaryRecruitmentPeriodId') militaryRecruitmentPeriodId: number,
    @Query() queryRecruitPeriodImageDto: QueryRecruitPeriodImageDto,
  ) {
    return this.recruitPeriodImageService.findAll(
      militaryRecruitmentPeriodId,
      queryRecruitPeriodImageDto,
    );
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.recruitPeriodImageService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.recruitPeriodImageService.remove(+id);
  }
}
