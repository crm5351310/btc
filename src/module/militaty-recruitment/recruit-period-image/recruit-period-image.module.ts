import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RecruitPeriodImage } from './entities/recruit-period-image.entity';
import { RecruitPeriodImageController } from './recruit-period-image.controller';
import { RecruitPeriodImageService } from './recruit-period-image.service';
import { MinioModule } from 'src/module/shared/minio/minio.module';
import { MinioService } from 'src/module/shared/minio/minio.service';

@Module({
  imports: [TypeOrmModule.forFeature([RecruitPeriodImage]), MinioModule],
  controllers: [RecruitPeriodImageController],
  providers: [RecruitPeriodImageService, MinioService],
})
export class RecruitPeriodImageModule {
  static readonly route: RouteTree = {
    path: 'cruitment-period-image',
    module: RecruitPeriodImageModule,
  };
}
