import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as path from 'path';
import { BaseService } from 'src/common/base/class/base.class';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { MinioService } from 'src/module/shared/minio/minio.service';
import { Repository } from 'typeorm';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne
} from '../../../common/response';
import { CreateRecruitPeriodImageDto } from './dto/create-recruit-period-image.dto';
import { QueryRecruitPeriodImageDto } from './dto/query-recruit-period-image.dto';
import { RecruitPeriodImage } from './entities/recruit-period-image.entity';

@Injectable()
export class RecruitPeriodImageService extends BaseService
{
  constructor(
    @InjectRepository(RecruitPeriodImage)
    private repository: Repository<RecruitPeriodImage>,
    private minioService: MinioService,
  ) {
    super();
    this.name = RecruitPeriodImageService.name;
  }

  async create(
    createRecruitPeriodImageDto: CreateRecruitPeriodImageDto,
    files: Express.Multer.File[],
  ) {
    const savedFiles: RecruitPeriodImage[] = [];
    for (const file of files) {
      const allowedFileExtensions = [
        '.doc',
        '.pdf',
        '.docx',
        '.png',
        '.svg',
        '.jpeg',
        '.jpg',
      ];
      const fileExtension = path.extname(file.originalname).toLowerCase();

      if (!allowedFileExtensions.includes(fileExtension)) {
        throw new CustomBadRequestException(
          ContentMessage.INVALID,
          FieldMessage.RECRUIT_PERIOD_IMAGE,
          true,
        );
      } 
      const recruitPeriodImage = new RecruitPeriodImage();
      recruitPeriodImage.name =  Buffer.from(file.originalname, 'latin1').toString('utf8');
      recruitPeriodImage.key = await this.minioService.createFile(file, true,'recruitment')

      const savedImage = await this.repository.save(recruitPeriodImage);
      savedFiles.push(savedImage);

    }

    return new ResponseCreated(savedFiles);
  }

  async findAll(
    militaryRecruitmentPeriodId: number,
    query: QueryRecruitPeriodImageDto,
  ): Promise<ResponseFindAll<RecruitPeriodImage>> {
    const [results, total] = await this.repository.findAndCount({
      where: { militaryRecruitmentPeriod : {id: militaryRecruitmentPeriodId }},
      relations : ['militaryRecruitmentPeriod'],
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number) {
    const file = await this.repository.findOne({
      where: { id: id },
    });
    if (!file) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.RECRUIT_PERIOD_IMAGE,
        true,
      );
    }
    const result = await this.minioService.findUrlFile(file?.key)

    return new ResponseFindOne(result);
  }

  async remove(id: number) {
    const result = await this.repository.findOne({
      where: { id: id },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.MILITARY_RECRUITMENT_PERIOD,
        true,
      );
    await this.repository.softDelete(id);
    await this.minioService.removeFile(result.key)
    return this.response(result);
  }
}
