import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { MilitaryRecruitmentPeriodModule } from './military-recruit-period/military-recruit-period.module';
import { MilitaryRecruitTargetRootModule } from './military-recruit-target-root/military-recruit-target-root.module';
import { RecruitPeriodImageModule } from './recruit-period-image/recruit-period-image.module';
import { ReadyEnlistmentModule } from './ready-enlistment/read-enlistment.module';
import { ApprovalModule } from './approval/approval.module';
import { MilitaryRecruitmentReportModule } from './military-recruitment-report/military-recruitment-report.module';

@Module({
  imports: [
    MilitaryRecruitmentPeriodModule,
    MilitaryRecruitTargetRootModule,
    RecruitPeriodImageModule,
    ReadyEnlistmentModule,
    ApprovalModule,
    MilitaryRecruitmentReportModule,
  ],
})
export class MilitaryRecruitmentModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment',
    module: MilitaryRecruitmentModule,
    children: [
      MilitaryRecruitmentPeriodModule.route,
      MilitaryRecruitTargetRootModule.route,
      RecruitPeriodImageModule.route,
      ReadyEnlistmentModule.route,
      ApprovalModule.route,
      MilitaryRecruitmentReportModule.route,
    ],
  };
}
