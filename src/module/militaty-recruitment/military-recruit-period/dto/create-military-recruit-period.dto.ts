import { OmitType } from '@nestjs/swagger';
import { MilitaryRecruitmentPeriod } from '../entities/military-recruit-period.entity';

export class CreateMilitaryRecruitmentPeriodDto extends OmitType(MilitaryRecruitmentPeriod, [
  'id',
]) {}
