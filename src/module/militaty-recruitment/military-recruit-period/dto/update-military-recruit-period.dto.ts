import { PartialType } from '@nestjs/swagger';
import { CreateMilitaryRecruitmentPeriodDto } from './create-military-recruit-period.dto';

export class UpdateMilitaryRecruitmentPeriodDto extends PartialType(
  CreateMilitaryRecruitmentPeriodDto,
) {}
