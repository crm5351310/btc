import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { BaseQueryDto } from '../../../../common/base/dto/base-query.dto';

export class QueryMilitaryRecruitmentPeriodDto extends BaseQueryDto {
  @ApiProperty({
    description: 'năm',
    required: false,
  })
  @IsOptional()
  year?: number;
}
