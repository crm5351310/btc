import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import { FindOptionsWhere, Repository } from 'typeorm';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { CreateMilitaryRecruitmentPeriodDto } from './dto/create-military-recruit-period.dto';
import { UpdateMilitaryRecruitmentPeriodDto } from './dto/update-military-recruit-period.dto';
import { MilitaryRecruitmentPeriod } from './entities/military-recruit-period.entity';
import { MilitaryRecruitmentPeriodService } from './military-recruit-period.service';

describe('MilitaryRecruitmentPeriodService', () => {
  let service: MilitaryRecruitmentPeriodService;
  let repository: Repository<MilitaryRecruitmentPeriod>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MilitaryRecruitmentPeriodService,
        {
          provide: getRepositoryToken(MilitaryRecruitmentPeriod),
          useValue: createStubInstance(Repository),
        },
      ],
    }).compile();

    service = module.get<MilitaryRecruitmentPeriodService>(MilitaryRecruitmentPeriodService);
    repository = module.get(getRepositoryToken(MilitaryRecruitmentPeriod));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        militaryRecruitmentPeriods.find(
          (value) => value.id === (options.where as FindOptionsWhere<MilitaryRecruitmentPeriod>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return militaryRecruitmentPeriods;
    });

    jest.spyOn(repository, 'findAndCount').mockImplementation(async () => {
      return [militaryRecruitmentPeriods, militaryRecruitmentPeriods.length];
    });

    jest.spyOn(repository, 'save').mockImplementation(async (entity: MilitaryRecruitmentPeriod) => {
      if (entity.id) {
        return entity;
      } else {
        return {
          ...entity,
          id: militaryRecruitmentPeriods.length,
        };
      }
    });
  });
  const militaryRecruitmentPeriods = [
    {
      id: 1,
      year: 2024,
      name: 'Tên 1',
      fromDate: new Date(),
      toDate: new Date(),
      lockDate: new Date(),
      transferDate: new Date(),
      note: 'Ghi chú 1',
    },
    {
      id: 2,
      year: 2023,
      name: 'Tên 1',
      fromDate: new Date(),
      toDate: new Date(),
      lockDate: new Date(),
      transferDate: new Date(),
      note: 'Ghi chú 1',
    },
  ] as MilitaryRecruitmentPeriod[];

  describe('MilitaryRecruitmentPeriodService => create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        year: 2023,
        name: 'Tên 1',
        fromDate: new Date(),
        toDate: new Date(),
        lockDate: new Date(),
        transferDate: new Date(),
        note: 'Ghi chú 1',
      } as CreateMilitaryRecruitmentPeriodDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('MilitaryRecruitmentPeriodService => findAll', () => {
    it('Tìm tất cả', async () => {
      const result = await service.findAll({});
      expect(result).toBeInstanceOf(ResponseFindAll);
      expect(result.data.result.length).toBe(militaryRecruitmentPeriods.length);
    });
  });

  describe('MilitaryRecruitmentPeriodService = > findOne', () => {
    it('Tìm thành công một đợt tuyển quân', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });

    it('Tìm một đợt tuyển quân không tồn tại', async () => {
      const id = 3;
      expect(service.findOne(id)).rejects.toThrowError();
    });
  });

  describe('MilitaryRecruitmentPeriodService = > update', () => {
    it('Cập nhật đợt tuyển quân thành công', async () => {
      const id = 1;
      const payload = {
        year: 2023,
        name: 'Tên 1',
        fromDate: new Date(),
        toDate: new Date(),
        lockDate: new Date(),
        transferDate: new Date(),
        note: 'Ghi chú 1',
      } as UpdateMilitaryRecruitmentPeriodDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
  });

  describe('MilitaryRecruitmentPeriodService', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.removeOne(1);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
