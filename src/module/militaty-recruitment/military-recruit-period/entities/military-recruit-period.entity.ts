import { ApiProperty } from '@nestjs/swagger';
import {
  IsDate,
  IsNumber,
  IsOptional,
  Max,
  MaxLength,
  Min,
} from 'class-validator';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';
import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { RecruitPeriodImage } from '../../recruit-period-image/entities/recruit-period-image.entity';

@Entity()
export class MilitaryRecruitmentPeriod extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'năm',
    default: '2024',
  })
  // validator
  @IsNumber()
  @Min(1900)
  @Max(9999)
  @Column('integer')
  year: number;

  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validator
  @MaxLength(100)
  //entity
  @Column('varchar', { length: 100 })
  name: string;

  // swagger
  @ApiProperty({
    description: 'từ ngày',
    default: '2000-01-01',
    maxLength: 100,
  })
  // validator
  @IsDate()
  //entity
  @Column('date')
  fromDate: Date;

  // swagger
  @ApiProperty({
    description: 'đến này',
    default: '2024-01-01',
    maxLength: 100,
  })
  // validator
  @IsDate()
  //entity
  @Column('date')
  toDate: Date;

  // swagger
  @ApiProperty({
    description: 'ngày khóa đăng kí',
    default: '2024-01-01',
    maxLength: 100,
  })
  // validator
  @IsDate()
  //entity
  @Column('date')
  lockDate: Date;

  // swagger
  @ApiProperty({
    description: 'ngày giao quân',
    default: '2024-01-01',
    maxLength: 100,
  })
  // validator
  @IsDate()
  @IsOptional()
  //entity
  @Column('date', { nullable: true })
  transferDate?: Date;

  // swagger
  @ApiProperty({
    description: 'Ghi chú',
    default: 'Ghi chú 1',
    maxLength: 1000,
  })
  // validator
  @IsOptional()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  note?: string;


  @ApiProperty({
    description: 'Đợt tuyển quân',
    example: [{ id: 1 }],
    required: false,
  })
  @IsObjectRelation({each: true})
  @OneToMany(() => RecruitPeriodImage, (item) => item.militaryRecruitmentPeriod)
  recruitPeriodImages: RecruitPeriodImage[];
}
