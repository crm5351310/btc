import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateMilitaryRecruitmentPeriodDto } from './dto/create-military-recruit-period.dto';
import { QueryMilitaryRecruitmentPeriodDto } from './dto/query-military-recruit-period';
import { UpdateMilitaryRecruitmentPeriodDto } from './dto/update-military-recruit-period.dto';
import { MilitaryRecruitmentPeriodService } from './military-recruit-period.service';

@Controller()
@ApiTags(TagEnum.MILITARY_RECRUIT_PERIOD)
export class MilitaryRecruitmentPeriodController {
  constructor(
    private readonly militaryRecruitmentPeriodService: MilitaryRecruitmentPeriodService,
  ) {}

  @Post()
  create(
    @Body()
    createMilitaryCruitmentPeriodDto: CreateMilitaryRecruitmentPeriodDto,
  ) {
    return this.militaryRecruitmentPeriodService.create(createMilitaryCruitmentPeriodDto);
  }

  @Get()
  findAll(@Query() queryMilitaryCruitmentPeriodDto: QueryMilitaryRecruitmentPeriodDto) {
    return this.militaryRecruitmentPeriodService.findAll(queryMilitaryCruitmentPeriodDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.militaryRecruitmentPeriodService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body()
    updateMilitaryCruitmentPeriodDto: UpdateMilitaryRecruitmentPeriodDto,
  ) {
    return this.militaryRecruitmentPeriodService.update(+id, updateMilitaryCruitmentPeriodDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.militaryRecruitmentPeriodService.removeOne(+id);
  }

  @Get('/find/last-year')
  findYear() {
    return this.militaryRecruitmentPeriodService.findYear();
  }
}
