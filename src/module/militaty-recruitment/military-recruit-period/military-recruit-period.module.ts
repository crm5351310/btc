import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { MilitaryRecruitmentPeriodService } from './military-recruit-period.service';
import { MilitaryRecruitmentPeriodController } from './military-recruit-period.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRecruitmentPeriod } from './entities/military-recruit-period.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryRecruitmentPeriod])],
  controllers: [MilitaryRecruitmentPeriodController],
  providers: [MilitaryRecruitmentPeriodService],
  exports: [MilitaryRecruitmentPeriodService],
})
export class MilitaryRecruitmentPeriodModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment-period',
    module: MilitaryRecruitmentPeriodModule,
  };
}
