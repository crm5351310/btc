import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { BaseService } from '../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate
} from '../../../common/response';
import { CreateMilitaryRecruitmentPeriodDto } from './dto/create-military-recruit-period.dto';
import { QueryMilitaryRecruitmentPeriodDto } from './dto/query-military-recruit-period';
import { UpdateMilitaryRecruitmentPeriodDto } from './dto/update-military-recruit-period.dto';
import { MilitaryRecruitmentPeriod } from './entities/military-recruit-period.entity';

@Injectable()
export class MilitaryRecruitmentPeriodService extends BaseService {
  constructor(
    @InjectRepository(MilitaryRecruitmentPeriod)
    private repository: Repository<MilitaryRecruitmentPeriod>,
  ) {
    super();
    this.name = MilitaryRecruitmentPeriodService.name;
  }

  async create(createMilitaryCruitmentPeriodDto: CreateMilitaryRecruitmentPeriodDto) {
    const checkExistNameMilitaryCruitmentPeriod = await this.repository.findOne({
      where: {
        name: createMilitaryCruitmentPeriodDto.name,
      },
    });
    if (checkExistNameMilitaryCruitmentPeriod)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);
    const militaryCruitmentPeriod: MilitaryRecruitmentPeriod = await this.repository.save(
      createMilitaryCruitmentPeriodDto,
    );

    return new ResponseCreated(militaryCruitmentPeriod);
  }

  async findAll(
    query: QueryMilitaryRecruitmentPeriodDto,
  ): Promise<ResponseFindAll<MilitaryRecruitmentPeriod>> {
    const [results, total] = await this.repository.findAndCount({
      where: { year: query.year ?? undefined },
      skip: query.limit && query.offset,
      take: query.limit,
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<MilitaryRecruitmentPeriod>> {
    const result = await this.repository.findOne({
      where: { id: id },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.MILITARY_RECRUITMENT_PERIOD,
        true,
      );

    return new ResponseFindOne(result);
  }
  
  async update(
    id: number,
    updateMilitaryCruitmentPeriodDto: UpdateMilitaryRecruitmentPeriodDto,
  ): Promise<ResponseUpdate> {
    const checkExist = await this.repository.findOne({
      where: { id: id as any },
    });
    if (!checkExist) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.MILITARY_RECRUITMENT_PERIOD,
        true,
      );
    }
    const checkExistName = await this.repository.findOne({
      where: {
        id: Not(id),
        name: updateMilitaryCruitmentPeriodDto.name,
      },
    });
    if (checkExistName)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.NAME, false);

    const result = await this.repository.save({
      id,
      ...updateMilitaryCruitmentPeriodDto,
    });
    return new ResponseUpdate(result);
  }

  async removeOne(id: number) {
    const result = await this.repository.findOne({
      where: { id: id },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.MILITARY_RECRUITMENT_PERIOD,
        true,
      );
    await this.repository.softDelete(id);
    return this.response(result);
  }

  async findYear(
  ): Promise<ResponseFindAll<MilitaryRecruitmentPeriod>> {
    const maxYearRecord = await this.repository.find({
      order: { year: 'DESC' },
      take: 1, 
    });
    const maxYear: number = maxYearRecord[0].year
    console.log(maxYear);
    
    const [results, total] = await this.repository.findAndCount({
      where: { year: maxYear ?? undefined },
    });
    return new ResponseFindAll(results, total);
  }
  }

