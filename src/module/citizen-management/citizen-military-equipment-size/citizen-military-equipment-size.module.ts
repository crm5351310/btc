import { Module } from '@nestjs/common';
import { CitizenMilitaryEquipmentSizeService } from './citizen-military-equipment-size.service';
import { CitizenMilitaryEquipmentSizeController } from './citizen-military-equipment-size.controller';
import { CitizenMilitaryEquipmentSize } from './entities/citizen-military-equipment-size.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouteTree } from '@nestjs/core';
import { Citizen } from '../citizen/entities';
import { CitizenModule } from '../citizen/citizen.module';

@Module({
  imports: [TypeOrmModule.forFeature([CitizenMilitaryEquipmentSize, Citizen]), CitizenModule],
  controllers: [CitizenMilitaryEquipmentSizeController],
  providers: [CitizenMilitaryEquipmentSizeService],
})
export class CitizenMilitaryEquipmentSizeModule {
  static readonly route: RouteTree = {
    path: 'citizen-military-equipment-size',
    module: CitizenMilitaryEquipmentSizeModule,
  };
}
