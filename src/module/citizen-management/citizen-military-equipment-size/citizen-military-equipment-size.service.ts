import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { In, Repository } from 'typeorm';
import { CitizenService } from '../citizen/services/citizen.service';
import { CreateCitizenMilitaryEquipmentSizeDto } from './dto/create-citizen-military-equipment-size.dto';
import { UpdateCitizenMilitaryEquipmentSizeDto } from './dto/update-citizen-military-equipment-size.dto';
import { CitizenMilitaryEquipmentSize } from './entities/citizen-military-equipment-size.entity';

@Injectable()
export class CitizenMilitaryEquipmentSizeService {
  constructor(
    @InjectRepository(CitizenMilitaryEquipmentSize)
    private readonly respository: Repository<CitizenMilitaryEquipmentSize>,
    private readonly citizenService: CitizenService,
  ) {}

  async create(
    createCitizenMilitaryEquipmentSizeDto: CreateCitizenMilitaryEquipmentSizeDto,
  ): Promise<ResponseCreated<CitizenMilitaryEquipmentSize>> {
    await this.citizenService.findOne(createCitizenMilitaryEquipmentSizeDto.citizen.id);
    const result: CitizenMilitaryEquipmentSize = await this.respository.save(
      createCitizenMilitaryEquipmentSizeDto,
    );
    return new ResponseCreated(result);
  }

  async findOne(id: number): Promise<ResponseFindOne<CitizenMilitaryEquipmentSize>> {
    const result = await this.respository.findOne({
      relations: {
        citizen: true,
      },
      where: {
        id: id,
      },
    });
    if (!result) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.CITIZEN_MILITARY_EQUIPMENT_SIZE,
        true,
      );
    }
    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateCitizenMilitaryEquipmentSizeDto: UpdateCitizenMilitaryEquipmentSizeDto,
  ): Promise<ResponseUpdate> {
    await this.citizenService.findOne(updateCitizenMilitaryEquipmentSizeDto.citizen?.id ?? 0);

    const equipment = await this.respository.find({
      relations: {
        citizen: true,
      },
      where: {
        id: id,
        citizen: {
          id: updateCitizenMilitaryEquipmentSizeDto.citizen?.id,
        },
      },
    });
    if (!equipment) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.CITIZEN_MILITARY_EQUIPMENT_SIZE,
        true,
      );
    }
    const result = await this.respository.save({
      id,
      ...updateCitizenMilitaryEquipmentSizeDto,
    });
    return new ResponseUpdate(result);
  }

  async remove(ids: number[]) {
    const results = await this.respository.find({
      relations: {
        citizen: true,
      },
      where: {
        id: In(ids),
      },
    });
    if (results.length === 0) {
      throw new CustomBadRequestException(
        ContentMessage.FAILURE,
        CitizenMilitaryEquipmentSize.name,
        true,
      );
    }
    await this.respository.softRemove(results);
    return new ResponseDelete<CitizenMilitaryEquipmentSize>(results, ids);
  }
}
