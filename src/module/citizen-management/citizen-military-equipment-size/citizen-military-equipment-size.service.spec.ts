import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { createStubInstance } from 'sinon';
import {
  ResponseCreated,
  ResponseFindOne,
  ResponseUpdate,
  ResponseDelete,
} from 'src/common/response';
import { Repository, FindOptionsWhere } from 'typeorm';
import { CitizenService } from '../citizen/services/citizen.service';
import { CitizenMilitaryEquipmentSizeService } from './citizen-military-equipment-size.service';
import { CreateCitizenMilitaryEquipmentSizeDto } from './dto/create-citizen-military-equipment-size.dto';
import { UpdateCitizenMilitaryEquipmentSizeDto } from './dto/update-citizen-military-equipment-size.dto';
import { CitizenMilitaryEquipmentSize } from './entities/citizen-military-equipment-size.entity';

describe('CitizenMilitaryEquipmentSizeService', () => {
  let service: CitizenMilitaryEquipmentSizeService;
  let repository: Repository<CitizenMilitaryEquipmentSize>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CitizenMilitaryEquipmentSizeService,
        {
          provide: getRepositoryToken(CitizenMilitaryEquipmentSize),
          useValue: createStubInstance(Repository),
        },
        {
          provide: CitizenService,
          useValue: createStubInstance(CitizenService),
        },
      ],
    }).compile();

    service = module.get<CitizenMilitaryEquipmentSizeService>(CitizenMilitaryEquipmentSizeService);
    repository = module.get(getRepositoryToken(CitizenMilitaryEquipmentSize));

    jest.spyOn(repository, 'findOne').mockImplementation(async (options) => {
      return (
        citizenMilitaryEquipmentSize.find(
          (value) =>
            value.id === (options.where as FindOptionsWhere<CitizenMilitaryEquipmentSize>).id,
        ) ?? null
      );
    });

    jest.spyOn(repository, 'find').mockImplementation(async () => {
      return citizenMilitaryEquipmentSize;
    });

    jest
      .spyOn(repository, 'save')
      .mockImplementation(async (entity: CitizenMilitaryEquipmentSize) => {
        if (entity.id) {
          return entity;
        } else {
          return {
            ...entity,
            id: citizenMilitaryEquipmentSize.length,
          };
        }
      });
  });

  const citizenMilitaryEquipmentSize = [
    {
      id: 1,
      shirtSize: 1,
      trousersSize: 1,
      hatSize: 1,
      shoesSize: 1,
      citizen: {
        id: 1,
      },
    },
  ] as CitizenMilitaryEquipmentSize[];

  describe('create', () => {
    it('Tạo mới thành công', async () => {
      const payload = {
        shirtSize: 1,
        trousersSize: 1,
        hatSize: 1,
        shoesSize: 1,
        citizen: {
          id: 1,
        },
      } as CreateCitizenMilitaryEquipmentSizeDto;

      const result = await service.create(payload);

      expect(repository.save).toHaveBeenCalled();

      expect(result).toBeInstanceOf(ResponseCreated);
    });
  });

  describe('findOne', () => {
    it('Tìm thành công một bản ghi', async () => {
      const id = 1;
      const result = await service.findOne(id);
      expect(result).toBeInstanceOf(ResponseFindOne);
    });
  });

  describe('update', () => {
    it('Cập nhật thành công', async () => {
      const id = 1;
      const payload = {
        shirtSize: 1,
        trousersSize: 1,
        hatSize: 1,
        shoesSize: 1,
        citizen: {
          id: 1,
        },
      } as UpdateCitizenMilitaryEquipmentSizeDto;

      const result = await service.update(id, payload);
      expect(result).toBeInstanceOf(ResponseUpdate);
    });
  });

  describe('delete', () => {
    it('Xóa thành công', async () => {
      jest.spyOn(repository, 'softRemove').mockImplementation();
      const result = await service.remove([1]);
      expect(result).toBeInstanceOf(ResponseDelete);
    });
  });
});
