import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { Citizen } from '../../citizen/entities';
import { RelationTypeBase } from '../../../../common/base/class/base.class';

@Entity()
export class CitizenMilitaryEquipmentSize extends BaseEntity {
  @ApiProperty({
    description: 'Áo',
    default: 1,
  })
  // validate
  @IsOptional()
  @IsNumber()
  // entity
  @Column('int', {
    nullable: true,
  })
  shirtSize: number;
  @ApiProperty({
    description: 'Quần',
    default: 1,
  })
  // validate
  @IsOptional()
  @IsNumber()
  // entity
  @Column('int', {
    nullable: true,
  })
  trousersSize: number;
  @ApiProperty({
    description: 'Mũ',
    default: 1,
  })
  // validate
  @IsOptional()
  @IsNumber()
  // entity
  @Column('int', {
    nullable: true,
  })
  hatSize: number;
  @ApiProperty({
    description: 'Giày',
    default: 1,
  })
  // validate
  @IsOptional()
  @IsNumber()
  // entity
  @Column('int', {
    nullable: true,
  })
  shoesSize: number;

  @ApiProperty({
    description: 'Id công dân',
    required: true,
    type: RelationTypeBase,
  })
  @IsObjectRelation()
  @OneToOne(() => Citizen, (item) => item.citizenMilitaryEquipmentSize)
  @JoinColumn()
  citizen: Citizen;
}
