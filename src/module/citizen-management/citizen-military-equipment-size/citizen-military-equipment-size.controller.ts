import { Controller, Get, Post, Body, Patch, Param, Delete, ParseArrayPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from '../../../common/base/class/base.class';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CitizenMilitaryEquipmentSizeService } from './citizen-military-equipment-size.service';
import { CreateCitizenMilitaryEquipmentSizeDto } from './dto/create-citizen-military-equipment-size.dto';
import { UpdateCitizenMilitaryEquipmentSizeDto } from './dto/update-citizen-military-equipment-size.dto';

@Controller()
@ApiTags(TagEnum.CITIZEN_MILITARY_EQUIPMENT_SIZE)
export class CitizenMilitaryEquipmentSizeController {
  constructor(
    private readonly citizenMilitaryEquipmentSizeService: CitizenMilitaryEquipmentSizeService,
  ) {}

  @Post()
  create(
    @Body()
    createCitizenMilitaryEquipmentSizeDto: CreateCitizenMilitaryEquipmentSizeDto,
  ) {
    return this.citizenMilitaryEquipmentSizeService.create(createCitizenMilitaryEquipmentSizeDto);
  }

  @Get(':id')
  findOne(@Param() param: PathDto) {
    return this.citizenMilitaryEquipmentSizeService.findOne(param.id);
  }

  @Patch(':id')
  update(
    @Param() param: PathDto,
    @Body()
    updateCitizenMilitaryEquipmentSizeDto: UpdateCitizenMilitaryEquipmentSizeDto,
  ) {
    return this.citizenMilitaryEquipmentSizeService.update(
      param.id,
      updateCitizenMilitaryEquipmentSizeDto,
    );
  }

  @Delete(':id')
  remove(
    @Param('id', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.citizenMilitaryEquipmentSizeService.remove(ids);
  }
}
