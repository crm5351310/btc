import { PartialType } from '@nestjs/mapped-types';
import { CreateCitizenMilitaryEquipmentSizeDto } from './create-citizen-military-equipment-size.dto';

export class UpdateCitizenMilitaryEquipmentSizeDto extends PartialType(
  CreateCitizenMilitaryEquipmentSizeDto,
) {}
