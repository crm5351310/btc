import { OmitType } from '@nestjs/swagger';
import { CitizenMilitaryEquipmentSize } from '../entities/citizen-military-equipment-size.entity';

export class CreateCitizenMilitaryEquipmentSizeDto extends OmitType(CitizenMilitaryEquipmentSize, [
  'id' as const,
]) {}
