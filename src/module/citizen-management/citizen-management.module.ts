import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { CitizenModule } from './citizen/citizen.module';
import { CategoryModule } from './category/category.module';
import { CitizenFamilyMemberModule } from './citizen-family-member/citizen-family-member.module';
import { CitizenMilitaryEquipmentSizeModule } from './citizen-military-equipment-size/citizen-military-equipment-size.module';

@Module({
  imports: [
    CitizenModule,
    CategoryModule,
    CitizenFamilyMemberModule,
    CitizenMilitaryEquipmentSizeModule,
  ],
})
export class CitizenManagementModule {
  static readonly route: RouteTree = {
    path: 'citizen-management',
    module: CitizenManagementModule,
    children: [
      CitizenModule.route,
      CategoryModule.route,
      CitizenFamilyMemberModule.route,
      CitizenMilitaryEquipmentSizeModule.route,
    ],
  };
}
