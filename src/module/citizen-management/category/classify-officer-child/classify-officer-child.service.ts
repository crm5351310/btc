import { Injectable } from '@nestjs/common';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';
import { ClassifyOfficerChild } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';

@Injectable()
export class ClassifyOfficerChildService extends BaseSimpleCategoryService<ClassifyOfficerChild> {
  constructor(
    @InjectRepository(ClassifyOfficerChild)
    repository: Repository<ClassifyOfficerChild>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<ClassifyOfficerChild>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<ClassifyOfficerChild>> {
    const result = await this.checkNotExist(id, ClassifyOfficerChild.name);

    return new ResponseFindOne(result);
  }
}
