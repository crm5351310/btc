import { Test, TestingModule } from '@nestjs/testing';
import { ClassifyOfficerChildService } from './classify-officer-child.service';

describe('ClassifyOfficerChildService', () => {
  let service: ClassifyOfficerChildService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassifyOfficerChildService],
    }).compile();

    service = module.get<ClassifyOfficerChildService>(ClassifyOfficerChildService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
