import { Controller, Get, Param } from '@nestjs/common';
import { ClassifyOfficerChildService } from './classify-officer-child.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.CLASSIFY_OFFICER_CHILD)
export class ClassifyOfficerChildController {
  constructor(private readonly classifyOfficerChildService: ClassifyOfficerChildService) {}

  @Get()
  findAll() {
    return this.classifyOfficerChildService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.classifyOfficerChildService.findOne(+id);
  }
}
