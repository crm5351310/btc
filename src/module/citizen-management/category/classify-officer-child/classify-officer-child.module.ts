import { Module } from '@nestjs/common';
import { ClassifyOfficerChildService } from './classify-officer-child.service';
import { ClassifyOfficerChildController } from './classify-officer-child.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClassifyOfficerChild } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([ClassifyOfficerChild])],
  controllers: [ClassifyOfficerChildController],
  providers: [ClassifyOfficerChildService],
})
export class ClassifyOfficerChildModule {
  static readonly route: RouteTree = {
    path: 'classify-officer-child',
    module: ClassifyOfficerChildModule,
  };
}
