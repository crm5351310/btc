import { Injectable } from '@nestjs/common';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';
import { FamilyEconomic } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';

@Injectable()
export class FamilyEconomicService extends BaseSimpleCategoryService<FamilyEconomic> {
  constructor(
    @InjectRepository(FamilyEconomic)
    repository: Repository<FamilyEconomic>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<FamilyEconomic>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<FamilyEconomic>> {
    const result = await this.checkNotExist(id, FamilyEconomic.name);
    return new ResponseFindOne(result);
  }
}
