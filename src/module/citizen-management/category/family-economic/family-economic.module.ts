import { Module } from '@nestjs/common';
import { FamilyEconomicService } from './family-economic.service';
import { FamilyEconomicController } from './family-economic.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FamilyEconomic } from './entities';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([FamilyEconomic])],
  controllers: [FamilyEconomicController],
  providers: [FamilyEconomicService],
})
export class FamilyEconomicModule {
  static readonly route: RouteTree = {
    path: 'family-economic',
    module: FamilyEconomicModule,
  };
}
