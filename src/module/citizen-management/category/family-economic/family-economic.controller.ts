import { Controller, Get, Param } from '@nestjs/common';
import { FamilyEconomicService } from './family-economic.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.FAMILY_ECONOMIC)
export class FamilyEconomicController {
  constructor(private readonly familyEconomicService: FamilyEconomicService) {}

  @Get()
  findAll() {
    return this.familyEconomicService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.familyEconomicService.findOne(+id);
  }
}
