import { Test, TestingModule } from '@nestjs/testing';
import { FamilyEconomicService } from './family-economic.service';

describe('FamilyEconomicService', () => {
  let service: FamilyEconomicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FamilyEconomicService],
    }).compile();

    service = module.get<FamilyEconomicService>(FamilyEconomicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
