import { Test, TestingModule } from '@nestjs/testing';
import { FamilySituationService } from './family-situation.service';

describe('FamilySituationService', () => {
  let service: FamilySituationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FamilySituationService],
    }).compile();

    service = module.get<FamilySituationService>(FamilySituationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
