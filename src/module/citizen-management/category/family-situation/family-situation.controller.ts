import { Controller, Get, Param } from '@nestjs/common';
import { FamilySituationService } from './family-situation.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.FAMILY_SITUATION)
export class FamilySituationController {
  constructor(private readonly familySituationService: FamilySituationService) {}

  @Get()
  findAll() {
    return this.familySituationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.familySituationService.findOne(+id);
  }
}
