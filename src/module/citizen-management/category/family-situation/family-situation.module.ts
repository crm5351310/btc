import { Module } from '@nestjs/common';
import { FamilySituationService } from './family-situation.service';
import { FamilySituationController } from './family-situation.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FamilySituation } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([FamilySituation])],
  controllers: [FamilySituationController],
  providers: [FamilySituationService],
})
export class FamilySituationModule {
  static readonly route: RouteTree = {
    path: 'family-situation',
    module: FamilySituationModule,
  };
}
