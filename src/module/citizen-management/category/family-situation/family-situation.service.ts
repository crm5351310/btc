import { Injectable } from '@nestjs/common';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';
import { FamilySituation } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';

@Injectable()
export class FamilySituationService extends BaseSimpleCategoryService<FamilySituation> {
  constructor(
    @InjectRepository(FamilySituation)
    repository: Repository<FamilySituation>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<FamilySituation>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<FamilySituation>> {
    const result = await this.checkNotExist(id, FamilySituation.name);

    return new ResponseFindOne(result);
  }
}
