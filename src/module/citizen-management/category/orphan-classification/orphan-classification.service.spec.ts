import { Test, TestingModule } from '@nestjs/testing';
import { OrphanClassificationService } from './orphan-classification.service';

describe('OrphanClassificationService', () => {
  let service: OrphanClassificationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrphanClassificationService],
    }).compile();

    service = module.get<OrphanClassificationService>(OrphanClassificationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
