import { Module } from '@nestjs/common';
import { OrphanClassificationService } from './orphan-classification.service';
import { OrphanClassificationController } from './orphan-classification.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrphanClassification } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([OrphanClassification])],
  controllers: [OrphanClassificationController],
  providers: [OrphanClassificationService],
})
export class OrphanClassificationModule {
  static readonly route: RouteTree = {
    path: 'orphan-classification',
    module: OrphanClassificationModule,
  };
}
