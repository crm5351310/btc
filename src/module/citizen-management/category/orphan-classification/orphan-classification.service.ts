import { Injectable } from '@nestjs/common';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';
import { OrphanClassification } from './entities';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class OrphanClassificationService extends BaseSimpleCategoryService<OrphanClassification> {
  constructor(
    @InjectRepository(OrphanClassification)
    repository: Repository<OrphanClassification>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<OrphanClassification>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<OrphanClassification>> {
    const result = await this.checkNotExist(id, OrphanClassification.name);
    return new ResponseFindOne(result);
  }
}
