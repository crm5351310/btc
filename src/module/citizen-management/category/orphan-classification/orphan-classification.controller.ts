import { Controller, Get, Param } from '@nestjs/common';
import { OrphanClassificationService } from './orphan-classification.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.ORPHAN_CLASSIFICATION)
export class OrphanClassificationController {
  constructor(private readonly orphanClassificationService: OrphanClassificationService) {}

  @Get()
  findAll() {
    return this.orphanClassificationService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orphanClassificationService.findOne(+id);
  }
}
