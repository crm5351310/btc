import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';
import { Gender } from './entities';
import { Repository } from 'typeorm';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';

@Injectable()
export class GenderService extends BaseSimpleCategoryService<Gender> {
  constructor(
    @InjectRepository(Gender)
    repository: Repository<Gender>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<Gender>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<Gender>> {
    const result = await this.checkNotExist(id, Gender.name);

    return new ResponseFindOne(result);
  }
}
