import { Module } from '@nestjs/common';
import { GenderModule } from './gender/gender.module';
import { RouteTree } from '@nestjs/core';
import { OrphanClassificationModule } from './orphan-classification/orphan-classification.module';
import { FamilyEconomicModule } from './family-economic/family-economic.module';
import { PartyMemberRankingModule } from './party-member-ranking/party-member-ranking.module';
import { FamilySituationModule } from './family-situation/family-situation.module';
import { ClassifyOfficerChildModule } from './classify-officer-child/classify-officer-child.module';
import { BusinessTypeModule } from './business-type/business-type.module';

@Module({
  imports: [
    GenderModule,
    OrphanClassificationModule,
    FamilyEconomicModule,
    PartyMemberRankingModule,
    FamilySituationModule,
    ClassifyOfficerChildModule,
    BusinessTypeModule,
  ],
})
export class CategoryModule {
  static readonly route: RouteTree = {
    path: 'category',
    module: CategoryModule,
    children: [
      GenderModule.route,
      OrphanClassificationModule.route,
      FamilyEconomicModule.route,
      PartyMemberRankingModule.route,
      FamilySituationModule.route,
      ClassifyOfficerChildModule.route,
      BusinessTypeModule.route,
    ],
  };
}
