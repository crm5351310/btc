import { Controller, Get, Param } from '@nestjs/common';
import { BusinessTypeService } from './business-type.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.BUSSINESS_TYPE)
export class BusinessTypeController {
  constructor(private readonly businessTypeService: BusinessTypeService) {}

  @Get()
  findAll() {
    return this.businessTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.businessTypeService.findOne(+id);
  }
}
