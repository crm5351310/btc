import { Injectable } from '@nestjs/common';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';
import { BusinessType } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';

@Injectable()
export class BusinessTypeService extends BaseSimpleCategoryService<BusinessType> {
  constructor(
    @InjectRepository(BusinessType)
    repository: Repository<BusinessType>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<BusinessType>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<BusinessType>> {
    const result = await this.checkNotExist(id, BusinessType.name);

    return new ResponseFindOne(result);
  }
}
