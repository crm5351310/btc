import { Test, TestingModule } from '@nestjs/testing';
import { PartyMemberRankingService } from './party-member-ranking.service';

describe('PartyMemberRankingService', () => {
  let service: PartyMemberRankingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PartyMemberRankingService],
    }).compile();

    service = module.get<PartyMemberRankingService>(PartyMemberRankingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
