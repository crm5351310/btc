import { Injectable } from '@nestjs/common';
import { BaseSimpleCategoryService } from 'src/common/base/service/base-simple-category.service';
import { PartyMemberRanking } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseFindAll, ResponseFindOne } from 'src/common/response';

@Injectable()
export class PartyMemberRankingService extends BaseSimpleCategoryService<PartyMemberRanking> {
  constructor(
    @InjectRepository(PartyMemberRanking)
    repository: Repository<PartyMemberRanking>,
  ) {
    super(repository);
  }

  async findAll(): Promise<ResponseFindAll<PartyMemberRanking>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<PartyMemberRanking>> {
    const result = await this.checkNotExist(id, PartyMemberRanking.name);

    return new ResponseFindOne(result);
  }
}
