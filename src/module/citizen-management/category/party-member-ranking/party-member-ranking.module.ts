import { Module } from '@nestjs/common';
import { PartyMemberRankingService } from './party-member-ranking.service';
import { PartyMemberRankingController } from './party-member-ranking.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PartyMemberRanking } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([PartyMemberRanking])],
  controllers: [PartyMemberRankingController],
  providers: [PartyMemberRankingService],
})
export class PartyMemberRankingModule {
  static readonly route: RouteTree = {
    path: 'party-member-ranking',
    module: PartyMemberRankingModule,
  };
}
