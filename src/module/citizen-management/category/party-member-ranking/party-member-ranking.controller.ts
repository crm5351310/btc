import { Controller, Get, Param } from '@nestjs/common';
import { PartyMemberRankingService } from './party-member-ranking.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.PARTY_MEMBER_RANKING)
export class PartyMemberRankingController {
  constructor(private readonly partyMemberRankingService: PartyMemberRankingService) {}

  @Get()
  findAll() {
    return this.partyMemberRankingService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.partyMemberRankingService.findOne(+id);
  }
}
