export enum CitizenFileProcEnum{
  DROPLIST_ETHNIC = 'droplist_ethicity',
  DROPLIST_GENDER = 'droplist_gender',
  DROPLIST_RELIGION = 'droplist_religion',
  DROPLIST_PERSONAL_CLASS = 'droplist_personal_class',
  DROPLIST_FAMILY_CLASS = 'droplist_family_class',
  DROPLIST_ORPHAN_CLASSIFICATION = 'droplist_orphan_classification',
  DROPLIST_FAMILY_ECONOMIC = 'droplist_family_economic',
  DROPLIST_GENERAL_EDUCATION_LEVEL = 'droplist_general_education_level',
  DROPLIST_EDUCATION_LEVEL = 'droplist_education_level',
  DROPLIST_ACADEMIC_DEGREE = 'droplist_academic_degree',
  DROPLIST_PARTY_MEMBER_RANKING = 'droplist_party_member_ranking',
  DROPLIST_FAMILY_SITUATION = 'droplist_family_situation',
  DROPLIST_CLASSIFY_OFFICER_CHILD = 'droplist_classify_officer_child',
  DROPLIST_BUSINESS_TYPE = 'droplist_business_type',
  DROPLIST_JOB = 'droplist_job',
  DROPLIST_JOB_POSITION = 'droplist_job_position',
  CITIZEN_IMPORT_DISTRICT = 'citizen_import_district',
  CITIZEN_IMPORT_SCHOOL = 'citizen_import_school',
  DROPLIST_SCHOOL = 'droplist_school',
  DROPLIST_JOB_SPECIALZATION_GROUP = 'droplist_job_specialization_group',
  CITIZEN_IMPORT_REASON = 'citizen_import_reason',
  CITIZEN_IMPORT_JOB_SPECIALIZATION = 'citizen_import_job_specialization',
  CITIZEN_IMPORT_MAJOR = 'citizen_import_major',
  CITIZEN_IMPORT_COMMUNE = 'citizen_import_commune'
}