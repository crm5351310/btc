import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsOptional,
  MaxLength,
} from 'class-validator';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { Column, Entity, ManyToOne, OneToMany, OneToOne } from 'typeorm';
import { Gender } from '../../category/gender/entities';
import { IsIdentityNumber } from '../../../../common/validator/is-identity-number';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';
import {
  EducationLevel,
  Ethnicity,
  FamilyClass,
  GeneralEducationLevel,
  PersonalClass,
} from '../../../../module/category';
import { AdministrativeUnit } from '../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { Religion } from '../../../category/religion/religion.entity';
import { OrphanClassification } from '../../category/orphan-classification/entities';
import { FamilyEconomic } from '../../category/family-economic/entities';
import { School } from '../../../category/school-root/school/entities/school.entity';
import { Major } from '../../../category/school-root/major/entities/major.entity';
import { AcademicDegree } from '../../../category/academic-degree/entities';
import { JobSpecialization } from '../../../category/job-specialization-root/job-specialization/entities/job-specialization.entity';
import { PartyMemberRanking } from '../../category/party-member-ranking/entities';
import { FamilySituation } from '../../category/family-situation/entities';
import { ClassifyOfficerChild } from '../../category/classify-officer-child/entities';
import { BusinessType } from '../../category/business-type/entities';
import { Job } from '../../../category/job/job.entity';
import { JobPosition } from '../../../category/jop-position/entities';
import { CitizenMilitaryEquipmentSize } from '../../citizen-military-equipment-size/entities/citizen-military-equipment-size.entity';
import { CitizenProcess } from '../../../citizen-process-management/citizen-process/entities/citizen-process.entity';
import { CitizenFamilyMember } from '../../citizen-family-member/entities/citizen-family-member.entity';
@Entity()
export class Citizen extends BaseEntity {
  @ApiProperty({
    description: 'Họ tên',
    example: 'Nguyễn Văn A',
    maxLength: 100,
  })
  @IsNotEmpty()
  @MaxLength(100)
  @Column('varchar', { length: 100 })
  fullName: string;

  @ApiProperty({
    description: 'Tên gọi khác',
    required: false,
    example: 'Nguyễn Văn Tý',
    maxLength: 100,
  })
  @IsOptional()
  @MaxLength(100)
  @Column('varchar', { nullable: true, length: 100 })
  otherName?: string;

  @ApiProperty({
    description: 'Ngày sinh',
    example: '2000-01-01',
  })
  @IsDate()
  @Column('date')
  dateOfBirth: Date;

  @ApiProperty({
    description: 'Giới tính',
    example: { id: 1 },
  })
  @IsObjectRelation()
  @ManyToOne(() => Gender, { nullable: false })
  gender: Gender;

  @ApiProperty({
    description: 'CCCD/CMND',
    example: '001097123456',
    maxLength: 30,
  })
  @IsIdentityNumber()
  @MaxLength(30)
  @Column('varchar', { length: 30 })
  identityNumber: string;

  @ApiProperty({
    description: 'Nơi cấp CCCD',
    example: 'Cục cảnh sát Quản lý dân cư',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  identityPlace?: string;

  @ApiProperty({
    description: 'Ngày cấp CCCD',
    example: '2020-01-01',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  identityDate?: Date;

  @ApiProperty({
    description: 'Ngày hết hạn CCCD',
    example: '2030-01-01',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  identityExpireDate?: Date;

  @ApiProperty({
    description: 'Quê quán',
    example: 'Phường 1, Quận 1, TP.HCM',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  originPlace?: string;

  @ApiProperty({
    description: 'Dân tộc',
    example: { id: 1 },
  })
  @IsObjectRelation()
  @ManyToOne(() => Ethnicity, { nullable: false })
  ethnicity: Ethnicity;

  @ApiProperty({
    description: 'Nơi cư trú',
    example: { id: 1 },
    type: () => AdministrativeUnit,
  })
  @IsObjectRelation()
  @ManyToOne(() => AdministrativeUnit, administrativeUnit => administrativeUnit.citizens, { nullable: false })
  administrativeUnit: AdministrativeUnit;

  @ApiProperty({
    description: 'Nơi cư trú chi tiết',
    example: 'Số 69, đường 1',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  residenceDetail?: string;

  @ApiProperty({
    description: 'Nơi ở hiện nay',
    example: 'Số 69, đường 1',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  address?: string;

  @ApiProperty({
    description: 'Tôn giáo',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => Religion, { nullable: true })
  religion?: Religion;

  @ApiProperty({
    description: 'Thành phần bản thân',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => PersonalClass, { nullable: true })
  personalClass?: PersonalClass;

  @ApiProperty({
    description: 'Thành phần gia đình',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => FamilyClass, { nullable: true })
  familyClass?: FamilyClass;

  @ApiProperty({
    description: 'Phân loại trẻ mồ côi',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => OrphanClassification, { nullable: true })
  orphanClassification?: OrphanClassification;

  @ApiProperty({
    description: 'Tình trạng kinh tế hộ gia đình',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => FamilyEconomic, { nullable: true })
  familyEconomic?: FamilyEconomic;

  @ApiProperty({
    description: 'Trình độ học vấn',
    example: { id: 1 },
  })
  @IsObjectRelation()
  @ManyToOne(() => EducationLevel, { nullable: false })
  educationLevel: EducationLevel;

  @ApiProperty({
    description: 'Trình độ văn hóa',
    example: { id: 1 },
  })
  @IsObjectRelation()
  @ManyToOne(() => GeneralEducationLevel, { nullable: false })
  generalEducationLevel: GeneralEducationLevel;

  @ApiProperty({
    description: 'Trường học',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => School, { nullable: true })
  school?: School;

  @ApiProperty({
    description: 'Ngành học',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => Major, { nullable: true })
  major?: Major;

  @ApiProperty({
    description: 'Tháng năm ra trường',
    example: '2024-06-01',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  graduationMonth?: Date;

  @ApiProperty({
    description: 'Chưa ra trường',
    example: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Column('boolean', { nullable: true })
  notGraduated?: boolean;

  @ApiProperty({
    description: 'Học vị',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => AcademicDegree, { nullable: true })
  academicDegree?: AcademicDegree;

  @ApiProperty({
    description: 'Ngành nghề CMKT',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => JobSpecialization, { nullable: true })
  jobSpecialization?: JobSpecialization;

  @ApiProperty({
    description: 'Trường CMKT',
    example: 'Trường chuyên môn kỹ thuật số 1',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  specializationSchool?: string;

  @ApiProperty({
    description: 'Trình độ học vấn CMKT',
    example: { id: 1 },
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => EducationLevel, { nullable: true })
  educationLevelSpecialization?: EducationLevel;

  @ApiProperty({
    description: 'Thông tin chiến đấu',
    example: 'Chiến đấu tốt',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  militantInformation?: string;

  @ApiProperty({
    description: 'Ngày vào đoàn',
    example: '2023-01-01',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  unionDate?: Date;

  @ApiProperty({
    description: 'Ngày vào đảng',
    example: '2023-02-02',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  partyDate?: Date;

  @ApiProperty({
    description: 'Ngày đăng ký cảm tình đảng',
    example: '2023-02-02',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  partyRegistrationDate?: Date;

  @ApiProperty({
    description: 'Ngày vào đảng chính thức',
    example: '2023-02-02',
    required: false,
  })
  @IsOptional()
  @IsDate()
  @Column('date', { nullable: true })
  partyOfficialDate?: Date;

  @ApiProperty({
    description: 'Xếp loại đảng viên',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => PartyMemberRanking, { nullable: true })
  partyMemberRanking?: PartyMemberRanking;

  @ApiProperty({
    description: 'Hoàn cảnh gia đình',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => FamilySituation, { nullable: true })
  familySituation?: FamilySituation;

  @ApiProperty({
    description: 'Đủ tiêu chuẩn chính trị',
    example: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Column('boolean', { nullable: true })
  isPoliticalStandard?: boolean;

  @ApiProperty({
    description: 'Phân loại con cán bộ',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => ClassifyOfficerChild, { nullable: true })
  classifyOfficerChild?: ClassifyOfficerChild;

  @ApiProperty({
    description: 'Loại cơ quan - doanh nghiệp',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => BusinessType, { nullable: true })
  businessType?: BusinessType;

  @ApiProperty({
    description: 'Nghề nghiệp',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => Job, { nullable: true })
  job?: Job;

  @ApiProperty({
    description: 'Chức vụ',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => JobPosition, { nullable: true })
  jobPosition?: JobPosition;

  @ApiProperty({
    description: 'Nơi làm việc',
    example: 'Trần Đăng Ninh, Cầu Giấy, HN',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  workPlace?: string;

  @ApiProperty({
    description: 'Vướng chính trị',
    example: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Column('boolean', { nullable: true })
  isPoliticalProblem?: boolean;

  @ApiProperty({
    description: 'Ghi chú vướng chính trị',
    example: 'Vướng chính trị somethings',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  politicalProblemNote?: string;

  @ApiProperty({
    description: 'Đi làm ăn xa khỏi địa phương',
    example: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Column('boolean', { nullable: true })
  isWorkAway?: boolean;

  @ApiProperty({
    description: 'Ở nước ngoài 2 năm trở lên',
    example: false,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Column('boolean', { nullable: true })
  isAbroad?: boolean;

  @ApiProperty({
    description: 'Ghi chú',
    example: 'Ghi chú 1',
    maxLength: 1000,
    required: false,
  })
  @IsOptional()
  @MaxLength(1000)
  @Column('varchar', { nullable: true, length: 1000 })
  note?: string;

  @OneToOne(() => CitizenMilitaryEquipmentSize, (item) => item.citizen)
  citizenMilitaryEquipmentSize: CitizenMilitaryEquipmentSize;

  @OneToMany(() => CitizenProcess, (process) => process.citizen, { cascade: ['update'] })
  citizenProcesses: CitizenProcess[];

  @OneToMany(() => CitizenFamilyMember, (member) => member.citizen)
  citizenFamilyMembers: CitizenFamilyMember[];
}
