import { Injectable } from '@nestjs/common';
import { CreateCitizenDto } from '../dto/create-citizen.dto';
import { UpdateCitizenDto } from '../dto/update-citizen.dto';
import { Citizen } from '../entities';
import {
  ResponseCreated,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
  ResponseDelete,
} from '../../../../common/response';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityManager, In, Repository } from 'typeorm';
import { BaseService } from '../../../../common/base/service/base-service';
import { FindAllCitizenDto } from '../dto/find-all-citizen.dto';
import { CitizenProcess } from 'src/module/citizen-process-management/citizen-process/entities';
import {
  DBDV_BCH1,
  DBDV_BCH2,
  DBDV_H2,
  DQTV_DKTN,
  DQTV_XDHN,
  NVQS_DK,
  NVQS_DKTN,
  NVQS_XN,
  PROCESS_INIT,
} from '../../../../database/seeds/process.seed';
import { FilterByConditionEnum } from 'src/common/enum/filter-by-condition.enum';
import { jobPositionData } from 'src/database/seeds/job-position.seed';
import { CitizenMilitaryEquipmentSize } from '../../citizen-military-equipment-size/entities/citizen-military-equipment-size.entity';
import { CitizenFamilyMember } from '../../citizen-family-member/entities/citizen-family-member.entity';
import { businessTypeData } from 'src/database/seeds/business-type.seed.';
import { genderData } from 'src/database/seeds/gender.seed';
import { personalClassData } from 'src/database/seeds/personal-class.seed';
import { CAO_DANG, DAI_HOC, SAU_DAI_HOC } from 'src/database/seeds/education-level.seed';
import { LOP_10, LOP_11, LOP_12, LOP_7, LOP_8, LOP_9 } from 'src/database/seeds/general-education-level.seed';
import { ethnicityData } from 'src/database/seeds/ethnicity.seed';
import { FindAllReadyEnlistmentDto } from 'src/module/militaty-recruitment/ready-enlistment/dto/find-all-ready-enlistment.dto';
import { AgeEnum } from 'src/common/enum/age.enum';
import { NVQSDKProcess } from 'src/module/citizen-process-management/citizen-process/nvqs-process/nvqs-dk-process/entities/nvqs-dk-process.entity';
import { FindAllApprovalDto } from 'src/module/militaty-recruitment/approval/dto/find-all-approval.dto';
import { NVQSGSTProcess } from 'src/module/citizen-process-management/citizen-process/nvqs-process/nvqs-gst-process/entities/nvqs-gst-process.entity';
import { DbdvBch1Process } from 'src/module/citizen-process-management/citizen-process/dbdv-process/dbdv-bch1-process/entities';
import { DbdvBch2Process } from 'src/module/citizen-process-management/citizen-process/dbdv-process/dbdv-bch2-process/entities';
import { NVQSXNProcess } from 'src/module/citizen-process-management/citizen-process/nvqs-process/nvqs-xn-process/entities/nvqs-xn-process.entity';
import { jobTitleData } from 'src/database/seeds/job-title.seed';
import { FindAllCitizenFilterByConditionDto } from '../dto/find-all-citizen-filter-by-condition.dto';

@Injectable()
export class CitizenService extends BaseService<Citizen> {
  constructor(
    @InjectRepository(Citizen)
    repository: Repository<Citizen>,
    private readonly entityManager: EntityManager,
  ) {
    super(repository);
  }

  /**
   * Tạo mới công dân
   * @param createCitizenDto body tạo mới
   * @returns công dân được tạo mới
   */
  async create(createCitizenDto: CreateCitizenDto): Promise<ResponseCreated<Citizen>> {
    // check unique
    await this.checkExist(['identityNumber'], createCitizenDto);
    const result = await this.entityManager.transaction(
      async (transactionalEntityManager: EntityManager) => {
        const result = await transactionalEntityManager.save(Citizen, createCitizenDto);
        await transactionalEntityManager.save(CitizenProcess, {
          process: PROCESS_INIT,
          fromDate: createCitizenDto.dateOfBirth,
          citizen: {
            id: result.id,
          },
        });
        return result;
      },
    );

    return new ResponseCreated(result);
  }

  /**
   * @param query
   * @returns Danh sách công dân
   */
  async findAll(query: FindAllCitizenDto): Promise<ResponseFindAll<Citizen>> {
    const queryBuilder = this.repository.createQueryBuilder('A');

    queryBuilder
      .leftJoinAndSelect('A.generalEducationLevel', 'generalEducationLevel')
      .leftJoinAndSelect('A.administrativeUnit', 'administrativeUnit')
      .leftJoinAndSelect('A.gender', 'gender')
      .leftJoinAndSelect(
        'A.citizenProcesses',
        'citizenProcess',
        'citizenProcess.id = (SELECT MAX(id) FROM citizen_process AS cp WHERE cp.citizenId = A.id)',
      )
      .leftJoinAndSelect('citizenProcess.process', 'process')
      .leftJoinAndSelect('process.nextProcesses', 'nextProcess');

    if (query.processIds) {
      queryBuilder.andWhere('process.id IN (:...processIds)', {
        processIds: query.processIds,
      });
    }
    if (query.administrativeUnitIds) {
      queryBuilder.andWhere('A.administrativeUnitId IN (:...administrativeUnitIds)', {
        administrativeUnitIds: query.administrativeUnitIds,
      });
    }

    if (query.genderId) {
      queryBuilder.andWhere('gender.id = :genderId', {
        genderId: query.genderId,
      });
    }

    if (query.fullName) {
      queryBuilder.andWhere('A.fullName LIKE :fullName', {
        fullName: `%${query.fullName}%`,
      });
    }

    if (query.identityNumber) {
      queryBuilder.andWhere('A.identityNumber LIKE :identityNumber', {
        identityNumber: `%${query.identityNumber}%`,
      });
    }

    if (query.fromDate) {
      queryBuilder.andWhere(
        '(citizenProcess.toDate IS NOT NULL AND citizenProcess.toDate > :fromDate) OR citizenProcess.toDate IS NULL',
        {
          fromDate: query.fromDate,
        },
      );
    }

    if (query.toDate) {
      queryBuilder.andWhere('(citizenProcess.fromDate < :toDate)', {
        toDate: query.fromDate,
      });
    }

    if (query.limit && query.offset) {
      queryBuilder.offset(query.offset);
      queryBuilder.limit(query.limit);
    }

    const [results, total] = await queryBuilder.getManyAndCount();

    return new ResponseFindAll(results, total);
  }

  /**
   * Danh sách công dân được lọc và xét duyệt theo: DBDV hạng 1, hạng 2, DQTV, SQDB
   * @param param xét duyệt theo: DBDV hạng 1, hạng 2, DQTV, SQDB
   * @returns Danh sách công dân thoả mãn điều kiện trên
   */
  async findManyByConditionFilter(
    param: FilterByConditionEnum,
    query: FindAllCitizenFilterByConditionDto,
  ) {
    const queryBuilder = this.repository.createQueryBuilder('A');

    queryBuilder
      .leftJoinAndSelect('A.generalEducationLevel', 'generalEducationLevel')
      .leftJoinAndSelect('A.administrativeUnit', 'administrativeUnit')
      .leftJoin('A.gender', 'gender')
      .leftJoin('A.citizenProcesses', 'citizenProcesses')
      .leftJoin('citizenProcesses.process', 'P1')
      .leftJoinAndSelect(
        'A.citizenProcesses',
        'lastProcess',
        'lastProcess.id = (SELECT MAX(id) FROM citizen_process AS cp WHERE cp.citizenId = A.id)',
      )
      .leftJoinAndSelect('lastProcess.process', 'P2')
      .leftJoinAndSelect('P2.nextProcesses', 'P3');

    if (param === FilterByConditionEnum.DQTV) {
      queryBuilder
        // nam, 18-50, đk tự nguyện
        .orWhere(
          '((P2.id = :processId2 AND P1.id = :processId1 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 50 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) >= 18) AND gender.code = :genderMaleCode)',
          {
            processId1: DQTV_DKTN.id,
            processId2: NVQS_DK.id || NVQS_XN.id,
            genderMaleCode: genderData.MALE.code,
          },
        )
        // nữ, 18-45, đk tự nguyện
        .orWhere(
          '((P2.id = :processId2 AND P1.id = :processId1 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 45 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) >= 18) AND gender.code = :genderFemaleCode)',
          {
            processId1: DQTV_DKTN.id,
            genderFemaleCode: genderData.FEMALE.code,
          },
        )
        // nam, 18-45
        .orWhere(
          '((P2.id = :processId2 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) < 45 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) > 18 AND gender.code = :genderMaleCode))',
        )
        // nữ, 18-40
        .orWhere(
          '((P2.id = :processId2 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) < 40 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) > 18 AND gender.code = :genderFemaleCode))',
        );
    }

    if (param === FilterByConditionEnum.DBDVH1) {
      queryBuilder
        .leftJoin('A.jobPosition', 'jp')
        .leftJoin('A.businessType', 'bt')

        // đk lần đầu, chức vụ công an xã
        .orWhere('(P2.id = :processId1 AND jp.code = :jobPositionCode)', {
          processId1: NVQS_DK.id,
          jobPositionCode: jobPositionData.CONG_AN_XA.code,
        })
        // đk lần đầu, CQ-DN quốc phòng
        .orWhere('(P2.id = :processId1 AND bt.code = :businessTypeCode)', {
          processId1: NVQS_DK.id,
          businessTypeCode: businessTypeData.QUOC_PHONG.code,
        })
        // xuất ngũ + dk dbdv hạng 2 + xây dựng hàng năm
        .orWhere('(P2.id = :processId2)', {
          processId2: NVQS_XN.id || DBDV_H2.id || DQTV_XDHN.id,
        });
    }

    if (param === FilterByConditionEnum.DBDVH2) {
      queryBuilder.leftJoin('A.businessType', 'businessType');
      // tồn tại xuất ngũ
      queryBuilder.orWhere('(P1.id = :processId1)', {
        processId1: NVQS_XN.id,
      });
      // DK lần đầu, CQ-DN quốc phòng + DK lần đầu, ngành nghề CMKT có dữ liệu + DK lần đầu, trường CMKT có dữ liệu
      queryBuilder.orWhere(
        '(P2.id = :processId2 AND (businessType.code = :businessTypeCode OR A.jobSpecialization IS NOT NULL OR A.specializationSchool IS NOT NULL))',
        {
          processId2: NVQS_DK.id,
          businessTypeCode: businessTypeData.QUOC_PHONG.code,
        },
      );
      // ĐK lần đầu hoặc tự nguyện, nữ
      queryBuilder.orWhere('(P2.id = :processId3 AND gender.code = :genderFemaleCode)', {
        processId3: NVQS_DK.id || NVQS_DKTN.id,
        genderFemaleCode: genderData.FEMALE.code,
      });
    }

    if (param === FilterByConditionEnum.SQDB) {
      queryBuilder.leftJoin('A.educationLevel', 'educationLevel');
      queryBuilder.leftJoin('A.personalClass', 'personalClass');
      queryBuilder.leftJoin('A.ethnicity', 'ethnicity');

      // đủ tiêu chuẩn chính trị, chức danh tiểu đội trưởng/chỉ huy trưởng/chỉ huy phó/phó trung đội trưởng, xuất ngũ hoặc biên chế DBDVH1, biên chế DBDVH2
      queryBuilder
        .leftJoinAndMapOne(
          'citizenProcesses.dbdvBch1Process',
          DbdvBch1Process,
          'dbdvBch1Process',
          'citizenProcesses.id = dbdvBch1Process.id',
        )
        .leftJoinAndSelect('dbdvBch1Process.jobTitle', 'bch1JobTitle')
        .leftJoinAndMapOne(
          'citizenProcesses.dbdvBch2Process',
          DbdvBch2Process,
          'dbdvBch2Process',
          'citizenProcesses.id = dbdvBch2Process.id',
        )
        .leftJoinAndSelect('dbdvBch1Process.jobTitle', 'bch2JobTitle')
        .leftJoinAndMapOne(
          'citizenProcesses.nvqsXnProcess',
          NVQSXNProcess,
          'nvqsXnProcess',
          'citizenProcesses.id = nvqsXnProcess.id',
        )
        .leftJoinAndSelect('dbdvBch1Process.jobTitle', 'xnJobTitle')
        .orWhere(
          '(A.isPoliticalStandard = :isPoliticalStandard AND P2.id = :processId3 AND (bch1JobTitle.id = :jobTitleId OR bch2JobTitle.id = :jobTitleId OR xnJobTitle.id = :jobTitleId))',
          {
            processId3: NVQS_XN.id || DBDV_BCH1.id || DBDV_BCH2.id,
            isPoliticalStandard: true,
            jobTitleId:
              jobTitleData.TIEU_DOI_TRUONG.id ||
              jobTitleData.CHI_HUY_TRUONG.id ||
              jobTitleData.CHI_HUY_PHO.id ||
              jobTitleData.TRUNG_DOI_TRUONG.id ||
              jobTitleData.PHO_TRUNG_DOI_TRUONG.id ||
              jobTitleData.KHAU_DOI_TRUONG.id,
          },
        );

      // đủ tiêu chuẩn chính trị, trình độ học vấn đại học, sau đại học, tuổi <= 35, dk NVQS lần đầu
      queryBuilder.orWhere(
        '(A.isPoliticalStandard = :isPoliticalStandard AND P2.id = :processId1 AND personalClass.code = :personalClassCode AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 35)',
        {
          processId1: NVQS_DK.id,
          personalClassCode: personalClassData.CONG_CHUC.code || personalClassData.VIEN_CHUC.code,
          isPoliticalStandard: true,
        },
      );
      // đủ tiêu chuẩn chính trị, trình độ học vấn đại học, sau đại học, tuổi <= 35, dk NVQS lần đầu
      queryBuilder.orWhere(
        '(A.isPoliticalStandard = :isPoliticalStandard AND P2.id = :processId1 AND educationLevel.code = :educationLevelCode AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 35)',
        {
          processId1: NVQS_DK.id,
          educationLevelCode:
            DAI_HOC.code || SAU_DAI_HOC.code,
          isPoliticalStandard: true,
        },
      );
      // đủ tiêu chuẩn chính trị, xuất ngũ, trình độ văn hoá lớp 7 đến 12, dân tộc khác kinh, tuổi <= 35
      queryBuilder.orWhere(
        '(A.isPoliticalStandard = :isPoliticalStandard AND P2.id = :processId2 AND generalEducationLevel.code = :generalEducationLevelCode AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 35 AND ethnicity.code != :ethnicityCode)',
        {
          processId2: NVQS_XN.id,
          generalEducationLevelCode:
            LOP_7.code ||
            LOP_8.code ||
            LOP_9.code ||
            LOP_10.code ||
            LOP_11.code ||
            LOP_12.code,
          ethnicityCode: ethnicityData.KINH.code,
          isPoliticalStandard: true,
        },
      );

      // đủ tiêu chuẩn chính trị, xuất ngũ, trình độ văn hoá lớp 10 đến 12, dân tộc kinh, tuổi <= 35
      queryBuilder.orWhere(
        '(A.isPoliticalStandard = :isPoliticalStandard AND P2.id = :processId2 AND generalEducationLevel.code = :generalEducationLevelCode AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 35 AND ethnicity.code = :ethnicityCode)',
        {
          processId2: NVQS_XN.id,
          generalEducationLevelCode:
            LOP_10.code ||
            LOP_11.code ||
            LOP_12.code,
          isPoliticalStandard: true,
        },
      );
    }

    queryBuilder.skip(query.offset).take(query.limit);

    const [results, total] = await queryBuilder.getManyAndCount();

    return new ResponseFindAll(results, total);
  }

  /**
   * Tìm thông tin của một công dân (bao gồm thông tin cá nhân và trình độ nghề nghiệp)
   * @param id ID của công dân
   * @returns Đối tượng công dân
   */
  async findOne(id: number): Promise<ResponseFindOne<Citizen>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        academicDegree: true,
        administrativeUnit: {
          parent: true,
        },
        citizenMilitaryEquipmentSize: true,
        businessType: true,
        classifyOfficerChild: true,
        educationLevel: true,
        educationLevelSpecialization: true,
        ethnicity: true,
        personalClass: true,
        familyClass: true,
        familyEconomic: true,
        familySituation: true,
        gender: true,
        generalEducationLevel: true,
        job: true,
        jobPosition: true,
        jobSpecialization: {
          jobSpecializationGroup: true,
        },
        school: true,
        major: true,
        partyMemberRanking: true,
        orphanClassification: true,
        religion: true,
        citizenProcesses: {
          process: {
            nextProcesses: true,
          },
        },
      },
      order: {
        citizenProcesses: {
          fromDate: 'DESC',
        },
      },
    });

    return new ResponseFindOne(result);
  }

  /**
   * Cập nhật thông tin công dân
   * @param id ID của công dân
   * @param updateCitizenDto body cập nhật
   * @returns công dân đã được cập nhật
   */
  async update(id: number, updateCitizenDto: UpdateCitizenDto): Promise<ResponseUpdate> {
    await this.checkNotExist(id, Citizen.name);
    // check unique
    await this.checkExist(['identityNumber'], updateCitizenDto, id);

    const result = await this.repository.save({
      id,
      ...updateCitizenDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<Citizen>> {
    const citizens = await this.repository.find({
      where: {
        id: In(ids),
      },
      relations: {
        citizenProcesses: true,
        citizenMilitaryEquipmentSize: true,
        citizenFamilyMembers: true,
      },
    });

    const citizenProcesses: CitizenProcess[] = [];
    const citizenMilitaryEquipmentSizes: CitizenMilitaryEquipmentSize[] = [];
    const citizenFamilyMembers: CitizenFamilyMember[] = [];
    for (const citizen of citizens) {
      citizenProcesses.push(...citizen.citizenProcesses);
      citizenMilitaryEquipmentSizes.push(citizen.citizenMilitaryEquipmentSize);
      citizenFamilyMembers.push(...citizen.citizenFamilyMembers);
    }

    await this.entityManager.transaction(async (transactionalEntityManager: EntityManager) => {
      await transactionalEntityManager.softRemove(Citizen, citizens);
      await transactionalEntityManager.softRemove(CitizenProcess, citizenProcesses);
      await transactionalEntityManager.softRemove(
        CitizenMilitaryEquipmentSize,
        citizenMilitaryEquipmentSizes,
      );
      await transactionalEntityManager.softRemove(CitizenFamilyMember, citizenFamilyMembers);
    });

    return new ResponseDelete<Citizen>(citizens, ids);
  }

  /**
   * Danh sách công dân sẵn sàng nhập ngũ
   * @param query
   * @returns Danh sách công dân
   */
  async findCitizenReadyEnlistment(query: FindAllReadyEnlistmentDto) {
    const queryBuilder = this.repository.createQueryBuilder('A');
    queryBuilder
      .leftJoinAndSelect('A.generalEducationLevel', 'generalEducationLevel')
      .leftJoinAndSelect('A.administrativeUnit', 'administrativeUnit')
      .leftJoinAndSelect('A.gender', 'gender')
      .leftJoinAndSelect('A.educationLevel', 'educationLevel')
      .leftJoin('A.citizenProcesses', 'citizenProcesses')
      .leftJoin('citizenProcesses.process', 'P1')
      .leftJoinAndSelect(
        'A.citizenProcesses',
        'lastProcess',
        'lastProcess.id = (SELECT MAX(id) FROM citizen_process AS cp WHERE cp.citizenId = A.id)',
      )
      .leftJoinAndSelect('lastProcess.process', 'P2')
      .leftJoinAndSelect('P2.nextProcesses', 'P3')

      // từ 18 đến 25 tuổi + từ 18 đến 27 tuổi và có trình độ học vấn là đại học hoặc cao đẳng
      .andWhere(
        ` ((FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 25 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) >= 18) OR (FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 27 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) >= 18 AND educationLevel.code = :educationLevelCode))`,
        {
          educationLevelCode: CAO_DANG.code || DAI_HOC.code,
        },
      )
      .andWhere('P2.id = :processId1', {
        processId1: NVQS_DK.id || NVQS_DKTN.id,
      });

    if (query.processIds) {
      queryBuilder.andWhere('P2.id IN (:...processId2)', {
        processId2: query.processIds,
      });
    }

    if (query.administrativeUnitIds) {
      queryBuilder.andWhere('administrativeUnit.id IN (:...administrativeUnitIds)', {
        administrativeUnitIds: query.administrativeUnitIds,
      });
    }

    if (query.genderId) {
      queryBuilder.andWhere('gender.id = :genderId', {
        genderId: query.genderId,
      });
    }

    if (query.age) {
      if (query.age === AgeEnum.TU_18_DEN_25_TUOI)
        queryBuilder.andWhere(
          '(FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 25 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) >=18)',
        );
      if (query.age === AgeEnum.TU_18_DEN_27_TUOI)
        queryBuilder.andWhere(
          '(FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) <= 27 AND FLOOR(DATEDIFF(NOW(), A.dateOfBirth) / 365) >=18)',
        );
    }

    if (query.educationLevelIds) {
      queryBuilder.andWhere('educationLevel.id IN (:...educationLevelIds)', {
        educationLevelIds: query.educationLevelIds,
      });
    }

    if (query.generalEducationLevelIds) {
      queryBuilder.andWhere('generalEducationLevel.id IN (:...generalEducationLevelIds)', {
        generalEducationLevelIds: query.generalEducationLevelIds,
      });
    }

    if (query.fullName) {
      queryBuilder.andWhere('A.fullName LIKE :fullName', {
        fullName: `%${query.fullName}%`,
      });
    }

    if (query.identityNumber) {
      queryBuilder.andWhere('A.identityNumber LIKE :identityNumber', {
        identityNumber: `%${query.identityNumber}%`,
      });
    }

    queryBuilder.skip(query.offset).take(query.limit);

    const [data, total] = await queryBuilder.getManyAndCount();
    return new ResponseFindAll(data, total);
  }

  /**
   * Tạo mới công dân - Kèm theo quá trình init và quá trình Đăng ký NVQS lần đầu (chức năng tạo mới công dân bên danh sách sẵn sàng nhập ngũ)
   * @param createCitizenDto body giống như API tạo mới bình thường
   * @returns công dân đã được tạo mới
   */
  async createCitizenReadyEnlistment(createCitizenDto: CreateCitizenDto) {
    // check unique
    await this.checkExist(['identityNumber'], createCitizenDto);
    const result = await this.entityManager.transaction(
      async (transactionalEntityManager: EntityManager) => {
        const result = await transactionalEntityManager.save(Citizen, createCitizenDto);
        await transactionalEntityManager.save(CitizenProcess, {
          process: PROCESS_INIT,
          fromDate: createCitizenDto.dateOfBirth,
          citizen: {
            id: result.id,
          },
        });
        await transactionalEntityManager.save(NVQSDKProcess, {
          process: NVQS_DK,
          fromDate: result.createdAt,
          citizen: {
            id: result.id,
          },
        });
        return result;
      },
    );

    return new ResponseCreated(result);
  }

  /**
   * Danh sách công dân xét tuyển cấp xã - xét tuyển cấp huyện - nhập ngũ
   * @param query.processIds giá trị processIds tương tương ứng sẽ quyết định danh sách công dân là xét tuyển cấp xã, cấp huyện hay nhập ngũ
   * @returns Danh sách công dân tương ứng
   */
  async findCitizenByListType(query: FindAllApprovalDto) {
    const queryBuilder = this.repository.createQueryBuilder('A');
    queryBuilder
      .leftJoinAndSelect('A.administrativeUnit', 'administrativeUnit')
      .leftJoinAndSelect('A.generalEducationLevel', 'generalEducationLevel')
      .leftJoin('A.citizenProcesses', 'citizenProcesses')
      .leftJoin('citizenProcesses.process', 'P1')
      .leftJoinAndSelect(
        'A.citizenProcesses',
        'lastProcess',
        'lastProcess.id = (SELECT MAX(id) FROM citizen_process AS cp WHERE cp.citizenId = A.id)',
      )
      .leftJoinAndSelect('lastProcess.process', 'P2')
      .leftJoinAndSelect('P2.nextProcesses', 'P3')
      .leftJoinAndMapOne(
        'citizenProcesses.nvqsGstProcess',
        NVQSGSTProcess,
        'nvqsGstProcess',
        'citizenProcesses.id = nvqsGstProcess.id',
      )
      .leftJoin('nvqsGstProcess.militaryRecruitmentPeriod', 'militaryRecruitmentPeriod');

    if (query.administrativeUnitIds)
      queryBuilder.andWhere('administrativeUnit.id IN (:...administrativeUnitIds)', {
        administrativeUnitIds: query.administrativeUnitIds,
      });
    if (query.militaryRecruitmentPeriodId)
      queryBuilder.andWhere('militaryRecruitmentPeriod.id = :militaryRecruitmentPeriodId', {
        militaryRecruitmentPeriodId: query.militaryRecruitmentPeriodId,
      });
    if (query.processIds)
      queryBuilder.andWhere('P2.id IN (:...processIds)', {
        processIds: query.processIds,
      });

    queryBuilder.skip(query.offset).take(query.limit);

    const [data, total] = await queryBuilder.getManyAndCount();
    return new ResponseFindAll(data, total);
  }
}
