import { Injectable } from '@nestjs/common';

import async from 'async';

import ExportTemplateCitizenFileModel, { JsonParam } from '../model/export-template-citizen-file.model';
import { ImportService } from '../../../shared/import/import.service';
import { ProcessEnum } from '../../../../common/enum/process';
import { Import } from '../../../../common/base/service/base-export.service';
import { citizenFileConst } from "../const/citizen-file.const";
import { CitizenFileProcEnum } from "../enum/citizen-file.enum";
import { ImportTemplateCitizenFileModel } from "../model/import-template-citizen-file.model";
@Injectable()
export class CitizenFileService extends ImportService {
  async import(file: Express.Multer.File) {
    //import common
    const common = new Import();
    common.setFile(file);
    common.setData(3, 1, 6);

    const jsonParam = this.init(3, common.getData());

    // if (false) {
    //   return new ReponseImport(common.getData());
    // }
    return ImportTemplateCitizenFileModel.fill(jsonParam, file.buffer, 3);
  }

  async export(process: ProcessEnum) {
    //get file from object storage
    const files = await this.minioService.findOneFile(
      'Mẫu báo cáo thông tin công dân',
      'template/import',
    );

    //binding json to excel
    const jsonParam: JsonParam[] = this.init(3);

    //get init data
    const result = await async.series([
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_ETHNIC),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_GENDER),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_RELIGION),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_PERSONAL_CLASS),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_FAMILY_CLASS),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_ORPHAN_CLASSIFICATION),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_FAMILY_ECONOMIC),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_GENERAL_EDUCATION_LEVEL),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_EDUCATION_LEVEL),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_ACADEMIC_DEGREE),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_PARTY_MEMBER_RANKING),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_FAMILY_SITUATION),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_CLASSIFY_OFFICER_CHILD),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_BUSINESS_TYPE),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_JOB),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_JOB_POSITION),
      async () => await this.getDataV3(CitizenFileProcEnum.CITIZEN_IMPORT_DISTRICT),
      async () => await this.getDataV3(CitizenFileProcEnum.CITIZEN_IMPORT_SCHOOL),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_SCHOOL),
      async () => await this.getDataV3(CitizenFileProcEnum.DROPLIST_JOB_SPECIALZATION_GROUP),
      async () => await this.getDataV3(CitizenFileProcEnum.CITIZEN_IMPORT_REASON, [process]),
    ]);

    await async.forEach(result[19] as object[], async (item: object) => {
      item['children'] = await this.getData(CitizenFileProcEnum.CITIZEN_IMPORT_JOB_SPECIALIZATION, [item['id']]);
      await this.getDataV4(item);
    });

    await async.forEach(result[18] as object[], async (item: object) => {
      item['children'] = await this.getData(CitizenFileProcEnum.CITIZEN_IMPORT_MAJOR, [item['id']]);
      await this.getDataV4(item);
    });

    await async.forEach(result[16] as object[], async (item: object) => {
      item['children'] = await this.getData(CitizenFileProcEnum.CITIZEN_IMPORT_COMMUNE, [item['id']]);
      await this.getDataV4(item);
    });

    //fill sheet
    jsonParam[14].fill = this.optional;
    jsonParam[4].fill = result[17] as object[];
    jsonParam[5].fill = result[0] as object[];
    jsonParam[6].fill = result[1] as object[];
    jsonParam[7].fill = result[2] as object[];
    jsonParam[8].fill = result[3] as object[];
    jsonParam[9].fill = result[4] as object[];
    jsonParam[10].fill = result[5] as object[];
    jsonParam[11].fill = result[6] as object[];
    jsonParam[12].fill = result[7] as object[];
    jsonParam[13].fill = result[8] as object[];
    jsonParam[15].fill = result[9] as object[];
    jsonParam[16].fill = result[10] as object[];
    jsonParam[17].fill = result[11] as object[];
    jsonParam[18].fill = result[12] as object[];
    jsonParam[19].fill = result[13] as object[];
    jsonParam[20].fill = result[14] as object[];
    jsonParam[21].fill = result[15] as object[];
    jsonParam[22].fill = result[20] as object[];

    //dynamic sheet
    jsonParam[2].initWidth = (result[16] as object[]).length;
    jsonParam[2].ref = result[16] as object[];
    jsonParam[1].initWidth = (result[18] as object[]).length;
    jsonParam[1].ref = result[18] as object[];
    jsonParam[0].initWidth = (result[19] as object[]).length;
    jsonParam[0].ref = result[19] as object[];

    //create dropdown
    const importSheet = jsonParam[3];

    importSheet.refFill = [
      this.refill(12, result[16] as object[], 'a', 'L'),
      this.refill(23, result[18] as object[], 'c', 'W'),
      this.refill(28, result[19] as object[], 'jsg', 'AB'),
    ];

    importSheet.dropdown = [
      this.createDropdown(11, 'dt', result[0] as object[]),
      this.createDropdown(5, 'gd', result[1] as object[]),
      this.createDropdown(17, 'pc', result[3] as object[]),
      this.createDropdown(18, 'fl', result[4] as object[]),
      this.createDropdown(19, 'oc', result[5] as object[]),
      this.createDropdown(16, 'rl', result[2] as object[]),
      this.createDropdown(20, 'ec', result[6] as object[]),
      this.createDropdown(21, 'gel', result[7] as object[]),
      this.createDropdown(22, 'el', result[8] as object[]),
      this.createDropdown(26, 'op', this.optional),
      this.createDropdown(27, 'ad', result[9] as object[]),
      this.createDropdown(31, 'el', result[8] as object[]),
      this.createDropdown(37, 'pmr', result[10] as object[]),
      this.createDropdown(38, 'fl', result[11] as object[]),
      this.createDropdown(39, 'coc', result[12] as object[]),
      this.createDropdown(40, 'op', this.optional),
      this.createDropdown(41, 'bt', result[13] as object[]),
      this.createDropdown(42, 'j', result[14] as object[]),
      this.createDropdown(43, 'jp', result[15] as object[]),
      this.createDropdown(45, 'op', this.optional),
      this.createDropdown(47, 'op', this.optional),
      this.createDropdown(48, 'op', this.optional),
      this.createDropdown(56, 'j', result[14] as object[]),
      this.createDropdown(59, 'j', result[14] as object[]),
      this.createDropdown(62, 'j', result[14] as object[]),
      this.createDropdown(65, 'j', result[14] as object[]),
      this.createDropdown(68, 'j', result[14] as object[]),
      this.createDropdown(71, 'j', result[14] as object[]),
      this.createDropdown(74, 'j', result[14] as object[]),
      this.createDropdown(77, 'j', result[14] as object[]),
      this.createDropdown(80, 'r', result[20] as object[]),
    ];

    //return to controller
    return await ExportTemplateCitizenFileModel.draw(jsonParam, files);
  }

  init(fillIndex: number, fill: object[] = []): JsonParam[] {
    citizenFileConst[fillIndex].fill = fill as never[];
    return citizenFileConst;
  }
}
