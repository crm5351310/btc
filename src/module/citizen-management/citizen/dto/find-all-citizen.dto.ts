import { ApiProperty } from '@nestjs/swagger';
import { BaseQueryDto } from '../../../../common/base/dto/base-query.dto';
import { IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class FindAllCitizenDto extends BaseQueryDto {
  @ApiProperty({
    description: 'Từ ngày',
    required: false,
  })
  @IsOptional()
  fromDate?: Date;

  @ApiProperty({
    description: 'Đến ngày',
    required: false,
  })
  @IsOptional()
  toDate?: Date;

  @ApiProperty({
    description: 'Id qúa trình mới nhất',
    required: false,
    type: [Number],
  })
  @IsOptional()
  @Transform(({value}) => {
    if (Array.isArray(value)) return value;
    return value.toString().split(',');
  })
  processIds?: number[];

  @ApiProperty({
    description: 'Danh sách id của các xã',
    required: false,
  })
  @IsOptional()
  administrativeUnitIds?: number[];

  @ApiProperty({
    description: 'Id giới tính',
    required: false,
  })
  @IsOptional()
  genderId?: number;

  @ApiProperty({
    description: 'Họ tên',
    required: false,
  })
  @IsOptional()
  fullName?: string;

  @ApiProperty({
    description: 'CCCD/CMND',
    required: false,
  })
  @IsOptional()
  identityNumber?: string;
}
