import { OmitType } from '@nestjs/swagger';
import { Citizen } from '../entities/citizen.entity';

export class CreateCitizenDto extends OmitType(Citizen, ['id']) {}
