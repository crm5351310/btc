import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateCitizenFileDto {
  @ApiProperty({
    description: 'Đẩy lên file',
    type: 'string',
    format: 'binary',
    required: true,
  })
  @IsNotEmpty()
  file: Express.Multer.File;
}
