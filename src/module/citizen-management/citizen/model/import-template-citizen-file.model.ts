import * as ExcelJS from "exceljs";
import { border } from "../../../../common/constant/report.constant";
import { JsonParam } from "../../../../common/utils/report.util";

export class ImportTemplateCitizenFileModel {
  static fill = async (json: JsonParam[], file: Buffer, sheetNumber: number): Promise<Buffer> => {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(file);

    const item = json[sheetNumber];
    let height = item.initHeight + 1;
    const sheet = workbook.getWorksheet(item.sheet);

    if (sheet) {
      const header: string[] = [];
      for (let i = 0; i < item.initWidth; i++) {
        header.push(String(sheet.getRow(item.initHeight - 1).getCell(i + 1).value || ''));
      }

      if (item.fill != undefined) {
        item.fill.forEach((itemFill: never) => {
          header.forEach((itemHeader: string, indexHeader: number) => {
            sheet.getRow(height).getCell(indexHeader + 1).border = border;
            if (itemFill[itemHeader] || itemFill[itemHeader] === 0) {
              sheet.getRow(height).getCell(indexHeader + 1).value = itemFill[itemHeader];
            }
          });
          height++;
        });
      }
    }

    return workbook.xlsx.writeBuffer() as Promise<Buffer>;
  };
}