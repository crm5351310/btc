import * as ExcelJS from 'exceljs';
import { border } from '../../../../common/constant/report.constant';
import { Dropdown } from '../../../../common/utils/report.util';
import { Readable as ReadableStream } from 'node:stream';

export interface JsonParam {
  sheet: string;
  initWidth: number;
  initHeight: number;
  headerHeight: number;
  fill?: object[];
  ref?: any[];
  dropdown?: Dropdown[];
  refFill?: phrase[];
  main?: boolean;
}
interface phrase {
  col: number;
  range: string;
  ref?: object[];
  nameCol: string;
  sheet: string;
}
interface Ref {
  name: string;
  children: RefChild[];
}
interface RefChild {
  name: string;
}

class ExportTemplateCitizenFileModel {
  static draw = async (json: JsonParam[], file: ReadableStream): Promise<Buffer> => {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.read(file);

    json.forEach((item: JsonParam) => {
      const sheet = workbook.getWorksheet(item.sheet);
      const refield = ['a', 'c', 'jsg'];
      if (sheet) {
        if (refield.includes(item.sheet)) {
          let initHeight = 2;
          if (item.ref) {
            item.ref.forEach((itemRef: Ref, index: number) => {
              sheet.getRow(1).getCell(index + 1).value = itemRef.name;
              itemRef.children.forEach((itemRefChild: RefChild) => {
                sheet.getRow(initHeight).getCell(index + 1).value = itemRefChild.name;
                initHeight++;
              });
              initHeight = 2;
            });
          }
        } else {
          if ((!item.fill || !item.fill.length) && item.main)
            item.fill = [
              {
                stt: 0,
                fullname: 'Nguyễn Văn A',
                aliasname: 'A@',
                dateOfbirth: '15/12/1996',
                gender: 'Nam',
                cccdNumber: '123456789123',
                indentityPlace: 'Hà Nội',
                identityDate: '15/12/1996',
                identityExpireDate: '15/12/1996',
                hometown: 'Hà Nội',
                ethnic: 'Kinh',
                district:
                  item.refFill && item.refFill[0] && item.refFill[0]['ref']
                    ? item.refFill[0]['ref'][0]['name']
                    : 'A',
                commune:
                  item.refFill &&
                  item.refFill[0] &&
                  item.refFill[0]['ref'] &&
                  item.refFill[0]['ref'][0] &&
                  item.refFill[0]['ref'][0]['children'] &&
                  item.refFill[0]['ref'][0]['children'][0]
                    ? item.refFill[0]['ref'][0]['children'][0]['name']
                    : 'A',
                school:
                  item.refFill && item.refFill[1] && item.refFill[1]['ref']
                    ? item.refFill[1]['ref'][0]['name']
                    : 'A',
                schoolField:
                  item.refFill &&
                  item.refFill[1] &&
                  item.refFill[1]['ref'] &&
                  item.refFill[1]['ref'][0] &&
                  item.refFill[1]['ref'][0]['children'] &&
                  item.refFill[1]['ref'][0]['children'][0]
                    ? item.refFill[1]['ref'][0]['children'][0]['name']
                    : 'A',
                cmktGroup:
                  item.refFill && item.refFill[2] && item.refFill[2]['ref']
                    ? item.refFill[2]['ref'][0]['name']
                    : 'A',
                cmkt:
                  item.refFill &&
                  item.refFill[2] &&
                  item.refFill[2]['ref'] &&
                  item.refFill[2]['ref'][0] &&
                  item.refFill[2]['ref'][0]['children'] &&
                  item.refFill[2]['ref'][0]['children'][0]
                    ? item.refFill[2]['ref'][0]['children'][0]['name']
                    : 'A',
                permanentAddressDetail: 'Hà Nội',
                address: 'Hà Nội',
                religion: 'Thiên chúa giáo',
                profile: 'Công nhân',
                family: 'Con liệt sỹ',
                orphan: 'Cha và mẹ',
                salary: 'Nghèo',
                cultureLevel: 'Không biết chữ',
                education: 'Sơ cấp nghề',

                //school
                greaduationMonth: '07/1998',
                notGraduated: 'Có',
                academyDegree: 'Tú tài',

                //CMKT
                cmktSchool: 'A',
                cmkEducation: 'Sơ cấp nghề',
                fight: 'A',
                unionDate: '20/01/2025',
                partyDate: '20/01/2025',
                partyRegistrationDate: '20/01/2025',
                partyOfficialDate: '20/01/2025',
                partyMemberRanking: 'Hoàn thành xuất sắc',
                familySituation: 'Con liệt sỹ',
                partyMemberChild: 'Tỉnh',
                politicalStandard: 'Có',
                classifiOfficialChild: 'Cơ quan',
                job: 'Học sinh',
                jobPosition: 'Công an xã',
                workplace: 'A',
                isPoliticalProblem: 'Có',
                politicalProblemNote: 'A',
                isWorkAway: 'Có',
                isAboardOverTwoYear: 'Có',
                note: 'A',
                shirtSize: 1,
                trouserSize: 1,
                shoesSize: 1,
                hatSize: 1,
                fullnameDaddy: 'A@',
                dateOfBirthDaddy: '20/01/2025',
                jobDaddy: 'Học sinh',
                fullnameMommy: 'A@',
                dateOfBirthMommy: '20/01/2025',
                jobMommy: 'Học sinh',
                fullnameMarriage: 'A@',
                dateOfBirthMarriage: '20/01/2025',
                jobMarriage: 'Học sinh',
                fullnameChild1: 'A@',
                dateOfBirthChild1: '20/01/2025',
                jobChild1: 'Học sinh',
                fullnameChild2: 'A@',
                dateOfBirthChild2: '20/01/2025',
                jobChild2: 'Học sinh',
                fullnameChild3: 'A@',
                dateOfBirthChild3: '20/01/2025',
                jobChild3: 'Học sinh',
                fullnameChild4: 'A@',
                dateOfBirthChild4: '20/01/2025',
                jobChild4: 'Học sinh',
                fullnameChild5: 'A@',
                dateOfBirthChild5: '20/01/2025',
                jobChild5: 'Học sinh',
                startDate: '20/01/2025',
                endDate: '20/01/2025',
              },
              { stt: 1 },
              { stt: 2 },
            ];

          let height = item.initHeight;
          const header: string[] = [];
          for (let i = 0; i < item.initWidth; i++) {
            header.push(String(sheet.getRow(item.initHeight - 1).getCell(i + 1).value || ''));
          }

          if (item.dropdown?.length) {
            item.dropdown.forEach((itemDropdown) => {
              for (let i = item.initHeight; i <= item.initHeight + 1000; i++) {
                sheet.getRow(i).getCell(itemDropdown.col).dataValidation = {
                  type: 'list',
                  allowBlank: true,
                  formulae: [itemDropdown.data],
                  showInputMessage: true,
                  promptTitle: 'Danh sách',
                  prompt: 'Chọn hoặc nhập đúng dữ liệu đúng trong droplist',
                  showErrorMessage: true,
                  errorTitle: 'Dữ liệu sai',
                  error: 'Chọn hoặc nhập lại dữ liệu đúng trong droplist',
                };
              }
            });
          }

          if (item.refFill) {
            item.refFill.forEach((itemRefFill: phrase) => {
              for (let i = item.initHeight; i <= item.initHeight + 1000; i++) {
                sheet.getRow(i).getCell(itemRefFill.col).dataValidation = {
                  type: 'list',
                  allowBlank: true,
                  formulae: [itemRefFill.range],
                  showInputMessage: true,
                  promptTitle: 'Danh sách',
                  prompt: 'Chọn hoặc nhập đúng dữ liệu đúng trong droplist',
                  showErrorMessage: true,
                  errorTitle: 'Dữ liệu sai',
                  error: 'Chọn hoặc nhập lại dữ liệu đúng trong droplist',
                };
                sheet.getRow(i).getCell(itemRefFill.col + 1).dataValidation = {
                  type: 'list',
                  allowBlank: true,
                  formulae: [
                    `OFFSET('${itemRefFill.sheet}'!$A$1,1,MATCH($${itemRefFill.nameCol}${i}, ${itemRefFill.range},0)-1,COUNTA(OFFSET('${itemRefFill.sheet}'!$A$1,1,MATCH($${itemRefFill.nameCol}${i}, ${itemRefFill.range},0)-1,1000,1)),1)`,
                  ],
                  showInputMessage: true,
                  promptTitle: 'Danh sách',
                  prompt: 'Chọn hoặc nhập đúng dữ liệu đúng trong droplist',
                  showErrorMessage: true,
                  errorTitle: 'Dữ liệu sai',
                  error: 'Chọn hoặc nhập lại dữ liệu đúng trong droplist',
                };
              }
            });
          }

          if (item.main) {
            sheet.getRow(6).getCell(1).style.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: { argb: 'FFFF66' }, // Red color, change it as needed
            };
          }

          if (item.fill != undefined) {
            item.fill.forEach((itemFill: never) => {
              header.forEach((itemHeader: string, indexHeader: number) => {
                sheet.getRow(height).getCell(indexHeader + 1).border = border;
                if (height === 6 && item.main) {
                  sheet.getRow(height).getCell(indexHeader + 1).style.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FFFF66' }, // Red color, change it as needed
                  };
                }
                if (itemFill[itemHeader] || itemFill[itemHeader] === 0) {
                  sheet.getRow(height).getCell(indexHeader + 1).value = itemFill[itemHeader];
                }
              });
              height++;
            });
          }
        }
      }
    });

    return workbook.xlsx.writeBuffer() as Promise<Buffer>;
  };
}

export default ExportTemplateCitizenFileModel;