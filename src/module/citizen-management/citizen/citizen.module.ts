import { Module } from '@nestjs/common';
import { CitizenService } from './services/citizen.service';
import { CitizenController } from './controllers/citizen.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Citizen } from './entities';
import { CitizenFileController } from './controllers/citizen-file.controller';
import { CitizenFileService } from './services/citizen-file.service';
import { ImportService } from '../../shared/import/import.service';
import { MinioModule } from "../../shared/minio/minio.module";

@Module({
  imports: [TypeOrmModule.forFeature([Citizen]), MinioModule],
  controllers: [CitizenController, CitizenFileController],
  providers: [CitizenService, CitizenFileService, ImportService],
  exports: [CitizenService, CitizenFileService],
})
export class CitizenModule {
  static readonly route: RouteTree = {
    path: 'citizen',
    module: CitizenModule,
  };
}
