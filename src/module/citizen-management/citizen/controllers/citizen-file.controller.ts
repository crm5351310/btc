import { Controller, Get, Post, UseInterceptors, UploadedFile, Res, Query } from "@nestjs/common";
import { ApiBody, ApiConsumes, ApiOperation, ApiQuery, ApiResponse, ApiTags } from "@nestjs/swagger";
import { TagEnum } from 'src/common/enum/tag.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { ReportExportUtil } from '../../../../common/utils/report.util';
import { Response } from 'express';
import { CreateCitizenFileDto } from '../dto/citizen-file.dto';
import { CitizenFileService } from '../services/citizen-file.service';
import { ProcessEnum } from "../../../../common/enum/process";
import { ResponseFindAll } from "../../../../common/response";

@Controller('file')
@ApiTags(TagEnum.CITIZEN_FILE)
export class CitizenFileController {
  constructor(private readonly citizenFileService: CitizenFileService) {}

  @Post()
  @ApiBody({
    type: CreateCitizenFileDto,
  })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Import' })
  @ApiResponse({
    status: 201,
    description: 'Import test successfully',
  })
  async import(@UploadedFile() file: Express.Multer.File, @Res() res: Response): Promise<void> {
    const result: any = await this.citizenFileService.import(file);

    if (result.message) {
      await res.status(201).json(result);
    } else {
      await res
        .status(202)
        .set(ReportExportUtil.header('Mẫu báo cáo thông tin công dân'))
        .send(result);
    }
  }

  @Get()
  @ApiQuery({ name: 'process', enum: ProcessEnum })
  async export(@Res() res: Response, @Query('process') process: ProcessEnum): Promise<void> {
    const result = await this.citizenFileService.export(process);
    res.status(200).set(ReportExportUtil.header('Mẫu báo cáo thông tin công dân')).send(result);
  }

  @Get('template/process')
  async process() {
    return new ResponseFindAll([
      {
        process: 'NVQS_DK',
      },
      {
        process: 'NVQS_MDK',
      },
      {
        process: 'NVQS_THDK',
      },
      {
        process: 'NVQS_CDK',
      },
      {
        process: 'NVQS_TDK',
      },
    ], 5)
  }
}
