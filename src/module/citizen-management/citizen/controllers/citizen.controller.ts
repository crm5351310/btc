import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseArrayPipe,
  Query,
} from '@nestjs/common';
import { CitizenService } from '../services/citizen.service';
import { CreateCitizenDto } from '../dto/create-citizen.dto';
import { UpdateCitizenDto } from '../dto/update-citizen.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { FindAllCitizenDto } from '../dto/find-all-citizen.dto';
import { FilterByConditionEnum } from 'src/common/enum/filter-by-condition.enum';
import { query } from 'express';
import { FindAllCitizenFilterByConditionDto } from '../dto/find-all-citizen-filter-by-condition.dto';

@Controller('rest')
@ApiTags(TagEnum.CITIZEN)
export class CitizenController {
  constructor(private readonly citizenService: CitizenService) {}

  @Post()
  create(@Body() createCitizenDto: CreateCitizenDto) {
    return this.citizenService.create(createCitizenDto);
  }

  @Get()
  findAll(@Query() query: FindAllCitizenDto) {
    return this.citizenService.findAll(query);
  }

  @Get('filter-by-condition/dqtv')
  findManyByConditionFilterDqtv(@Query() query: FindAllCitizenFilterByConditionDto) {
    return this.citizenService.findManyByConditionFilter(FilterByConditionEnum.DQTV, query);
  }
  @Get('filter-by-condition/dbdvh1')
  findManyByConditionFilterDbdvh1(@Query() query: FindAllCitizenFilterByConditionDto) {
    return this.citizenService.findManyByConditionFilter(FilterByConditionEnum.DBDVH1, query);
  }
  @Get('filter-by-condition/dbdvh2')
  findManyByConditionFilterDbdvh2(@Query() query: FindAllCitizenFilterByConditionDto) {
    return this.citizenService.findManyByConditionFilter(FilterByConditionEnum.DBDVH2, query);
  }
  @Get('filter-by-condition/sqdb')
  findManyByConditionFilterSqdb(@Query() query: FindAllCitizenFilterByConditionDto) {
    return this.citizenService.findManyByConditionFilter(FilterByConditionEnum.SQDB, query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.citizenService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenDto: UpdateCitizenDto) {
    return this.citizenService.update(+id, updateCitizenDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.citizenService.remove(ids);
  }
}
