import { Body, Controller, Delete, Get, Param, ParseArrayPipe, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { CreateCitizenFamilyMemberDto } from './dto/create-citizen-family-member.dto';
import { UpdateCitizenFamilyMembernDto } from './dto/update-citizen-family-member.dto';
import { CitizenFamilyMemberService } from './citizen-family-member.service';
import { QueryMilitaryJobPositionDto } from './dto/query-citizen-family-member.dto';

@Controller()
@ApiTags(TagEnum.CITIZEN_FAMILY_MEMBER)
export class CitizenFamilyMemberController {
  constructor(private readonly citizenFamilyMemberService: CitizenFamilyMemberService) {}

  @Post()
  create(@Body() createCitizenFamilyMemberDto: CreateCitizenFamilyMemberDto) {
    return this.citizenFamilyMemberService.create(createCitizenFamilyMemberDto);
  }

  @Get('')
  findAll(@Param('citizenId') citizenId: QueryMilitaryJobPositionDto) {
    return this.citizenFamilyMemberService.findAll(citizenId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.citizenFamilyMemberService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCitizenFamilyMembernDto: UpdateCitizenFamilyMembernDto,
  ) {
    return this.citizenFamilyMemberService.update(+id, updateCitizenFamilyMembernDto);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.citizenFamilyMemberService.remove(ids);
  }
}
