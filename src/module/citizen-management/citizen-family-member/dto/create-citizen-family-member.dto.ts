import { OmitType } from '@nestjs/swagger';
import { CitizenFamilyMember } from '../entities/citizen-family-member.entity';

export class CreateCitizenFamilyMemberDto extends OmitType(CitizenFamilyMember, ['id']) {}
