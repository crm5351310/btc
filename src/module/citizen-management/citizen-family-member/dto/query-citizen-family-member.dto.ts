import { ApiProperty } from "@nestjs/swagger";

export class QueryMilitaryJobPositionDto {
    @ApiProperty({
      required: true,
      description: 'ID của ngành chuyên nghiệp quân sự',
    })
    citizenId: number;
  }