import { PartialType } from '@nestjs/swagger';
import { CreateCitizenFamilyMemberDto } from './create-citizen-family-member.dto';

export class UpdateCitizenFamilyMembernDto extends PartialType(CreateCitizenFamilyMemberDto) {}
