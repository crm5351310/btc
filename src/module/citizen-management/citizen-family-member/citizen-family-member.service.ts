import { Injectable } from '@nestjs/common';
import { CreateCitizenFamilyMemberDto } from './dto/create-citizen-family-member.dto';
import { UpdateCitizenFamilyMembernDto } from './dto/update-citizen-family-member.dto';
import {
  ResponseCreated,
  ResponseDelete,
  ResponseFindAll,
  ResponseFindOne,
  ResponseUpdate,
} from '../../../common/response';
import { InjectRepository } from '@nestjs/typeorm';
import { CitizenFamilyMember } from './entities/citizen-family-member.entity';
import { In, Repository } from 'typeorm';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';
import { Citizen } from '../citizen/entities/citizen.entity';
import { QueryMilitaryJobPositionDto } from './dto/query-citizen-family-member.dto';

@Injectable()
export class CitizenFamilyMemberService {
  constructor(
    @InjectRepository(CitizenFamilyMember)
    private readonly repository: Repository<CitizenFamilyMember>,
    @InjectRepository(Citizen)
    private readonly citizenRepository: Repository<Citizen>,
  ) {}

  async create(createCitizenFamilyMemberDto: CreateCitizenFamilyMemberDto) {
    const result = await this.citizenRepository.findOne({
      where: { id: createCitizenFamilyMemberDto.citizen.id as any },
    });
    if (!result) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.CITIZEN, true);
    }

    const citizenFamilyMember: CitizenFamilyMember = await this.repository.save(
      createCitizenFamilyMemberDto,
    );

    return new ResponseCreated(citizenFamilyMember);
  }

  async findAll(citizenId: QueryMilitaryJobPositionDto): Promise<ResponseFindAll<CitizenFamilyMember>> {
    const [results, total] = await this.repository.findAndCount({
      where: {citizen: {id: citizenId.citizenId}},
      relations: {
        job: true,
        familyRelation: true,
      },
    });
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<CitizenFamilyMember>> {
    const result = await this.repository.findOne({
      where: { id: id as any },
      relations: {
        job: true,
        familyRelation: true,
      },
    });

    if (!result) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.CITIZEN_FAMILY_MEMBER,
        true,
      );
    }

    return new ResponseFindOne(result);
  }

  async update(
    id: number,
    updateCitizenFamilyMembernDto: UpdateCitizenFamilyMembernDto,
  ): Promise<ResponseUpdate> {
    const checkExist = await this.repository.findOne({
      where: { id: id as any },
    });

    if (!checkExist) {
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.CITIZEN_FAMILY_MEMBER,
        true,
      );
    }

    const result = await this.repository.save({
      id,
      ...updateCitizenFamilyMembernDto,
    });

    return new ResponseUpdate(result);
  }

  async remove(ids: number[]): Promise<ResponseDelete<CitizenFamilyMember>> {
    const results = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    await this.repository.softRemove(results);
    return new ResponseDelete<CitizenFamilyMember>(results, ids);
  }
}
