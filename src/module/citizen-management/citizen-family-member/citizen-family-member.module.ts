import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FamilyRelation } from '../../../module/category/family-relation/entities';
import { Job } from '../../../module/category/job/job.entity';
import { Citizen } from '../citizen/entities/citizen.entity';
import { CitizenFamilyMember } from './entities/citizen-family-member.entity';
import { CitizenFamilyMemberController } from './citizen-family-member.controller';
import { CitizenFamilyMemberService } from './citizen-family-member.service';

@Module({
  imports: [TypeOrmModule.forFeature([CitizenFamilyMember, Job, FamilyRelation, Citizen])],
  controllers: [CitizenFamilyMemberController],
  providers: [CitizenFamilyMemberService],
})
export class CitizenFamilyMemberModule {
  static readonly route: RouteTree = {
    path: 'citizen-family-member',
    module: CitizenFamilyMemberModule,
  };
}
