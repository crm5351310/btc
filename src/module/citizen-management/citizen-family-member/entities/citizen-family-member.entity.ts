import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional, IsString, MaxLength } from 'class-validator';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';
import { FamilyRelation } from '../../../../module/category/family-relation/entities/family-relation.entity';
import { Job } from '../../../../module/category/job/job.entity';
import { Citizen } from '../../citizen/entities/citizen.entity';

@Entity()
export class CitizenFamilyMember extends BaseEntity {
  // swagger
  @ApiProperty({
    description: 'Tên',
    default: 'Tên 1',
    maxLength: 100,
  })
  // validator
  @IsString()
  @MaxLength(100)
  //entity
  @Column('varchar', { length: 100, nullable: false })
  name: string;

  // swagger
  @ApiProperty({
    description: 'DoB',
    default: '2000-01-01',
    maxLength: 100,
  })
  // validator
  @IsDate()
  @IsOptional()
  //entity
  @Column('date', { nullable: true })
  dateOfBirth?: Date;

  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validator
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  //entity
  @Column('varchar', { length: 1000, nullable: true })
  note?: string;

  @ApiProperty({
    description: 'nghề nghiệp',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => Job)
  job: Job;

  @ApiProperty({
    description: 'quan hệ gia đình',
    example: { id: 1 },
    required: true,
  })
  @IsObjectRelation()
  @ManyToOne(() => FamilyRelation)
  familyRelation: FamilyRelation;

  @ApiProperty({
    description: 'công dân',
    example: { id: 1 },
    required: true,
    type: () => Citizen,
  })
  @IsObjectRelation()
  @ManyToOne(() => Citizen, (citizen) => citizen.citizenFamilyMembers)
  citizen: Citizen;
}
