import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { MilitaryRecruitmentTypeModule } from './military-recruitment-type/military-recruitment-type.module';
import { MilitaryStatusModule } from './military-status/military-status.module';
import { ParticipantTypeModule } from './participant-type/participant-type.module';
import { ProcessGroupModule } from './process-group/process-group.module';
import { ProcessModule } from './process/process.module';
import { RegisterTypeModule } from './register-type/register-type.module';
import { HivExaminationModule } from './hiv-examination/hiv-examination.module';
import { StatusExaminationModule } from './status-examination/status-examination.module';
import { DrugExaminationModule } from './drug-examination/drug-examination.module';

@Module({
  imports: [
    ProcessGroupModule,
    ProcessModule,
    ParticipantTypeModule,
    MilitaryStatusModule,
    RegisterTypeModule,
    MilitaryRecruitmentTypeModule,
    HivExaminationModule,
    DrugExaminationModule,
    StatusExaminationModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class CitizenProcessCategoryModule {
  static readonly route: RouteTree = {
    path: 'category',
    module: CitizenProcessCategoryModule,
    children: [
      ProcessGroupModule.route,
      ProcessModule.route,
      ParticipantTypeModule.route,
      MilitaryStatusModule.route,
      RegisterTypeModule.route,
      MilitaryRecruitmentTypeModule.route,
      HivExaminationModule.route,
      DrugExaminationModule.route,
      StatusExaminationModule.route,
    ],
  };
}
