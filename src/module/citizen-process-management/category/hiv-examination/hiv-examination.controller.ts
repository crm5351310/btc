import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { HivExaminationService } from './hiv-examination.service';

@Controller()
@ApiTags(TagEnum.HIV_EXAMINATION)
export class HivExaminationController {
  constructor(private readonly service: HivExaminationService) {}

  @Get()
  findAll() {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(+id);
  }
}
