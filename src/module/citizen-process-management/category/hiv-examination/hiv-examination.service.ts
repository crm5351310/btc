import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseSimpleCategoryService } from '../../../../common/base/service/base-simple-category.service';
import { ResponseFindAll, ResponseFindOne } from '../../../../common/response';
import { Repository } from 'typeorm';
import { HivExamination } from './entities/hiv-examination.entity';

@Injectable()
export class HivExaminationService extends BaseSimpleCategoryService<HivExamination> {
  constructor(
    @InjectRepository(HivExamination)
    repository: Repository<HivExamination>,
  ) {
    super(repository);
  }
  async findAll(): Promise<ResponseFindAll<HivExamination>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<HivExamination>> {
    const result = await this.checkNotExist(id, HivExamination.name);

    return new ResponseFindOne(result);
  }
}
