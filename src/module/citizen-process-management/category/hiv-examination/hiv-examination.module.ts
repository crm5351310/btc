import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HivExaminationController } from './hiv-examination.controller';
import { HivExaminationService } from './hiv-examination.service';
import { HivExamination } from './entities/hiv-examination.entity';

@Module({
  imports: [TypeOrmModule.forFeature([HivExamination])],
  controllers: [HivExaminationController],
  providers: [HivExaminationService],
  exports: [HivExaminationService],
})
export class HivExaminationModule {
  static readonly route: RouteTree = {
    path: 'hiv-examination',
    module: HivExaminationModule,
    children: [],
  };
}
