import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DrugExaminationController } from './drug-examination.controller';
import { DrugExaminationService } from './drug-examination.service';
import { DrugExamination } from './entities/drug-examination.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DrugExamination])],
  controllers: [DrugExaminationController],
  providers: [DrugExaminationService],
  exports: [DrugExaminationService],
})
export class DrugExaminationModule {
  static readonly route: RouteTree = {
    path: 'drug-examination',
    module: DrugExaminationModule,
    children: [],
  };
}
