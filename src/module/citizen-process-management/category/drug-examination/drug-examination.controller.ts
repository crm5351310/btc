import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { DrugExaminationService } from './drug-examination.service';

@Controller()
@ApiTags(TagEnum.NARCOTICS_EXAMINATION)
export class DrugExaminationController {
  constructor(private readonly service: DrugExaminationService) {}

  @Get()
  findAll() {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(+id);
  }
}
