import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseSimpleCategoryService } from '../../../../common/base/service/base-simple-category.service';
import { ResponseFindAll, ResponseFindOne } from '../../../../common/response';
import { Repository } from 'typeorm';
import { DrugExamination } from './entities/drug-examination.entity';

@Injectable()
export class DrugExaminationService extends BaseSimpleCategoryService<DrugExamination> {
  constructor(
    @InjectRepository(DrugExamination)
    repository: Repository<DrugExamination>,
  ) {
    super(repository);
  }
  async findAll(): Promise<ResponseFindAll<DrugExamination>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<DrugExamination>> {
    const result = await this.checkNotExist(id, DrugExamination.name);

    return new ResponseFindOne(result);
  }
}
