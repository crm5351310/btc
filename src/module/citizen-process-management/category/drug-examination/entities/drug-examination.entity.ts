import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';

@Entity()
export class DrugExamination extends BaseEntity {
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @Column('varchar', { length: 100, nullable: false })
  name: string;
}
