import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';
import { BaseService, PathDto } from 'src/common/base/class/base.class';
import { SubjectMessage } from 'src/common/message/subject.message';
import { BaseMessage } from 'src/common/message/base.message';
import { Pagination } from 'src/common/utils/pagination.util';
import { ProcessGroup } from './entities/process-group.entity';

@Injectable()
export class ProcessGroupService extends BaseService {
  constructor(
    @InjectRepository(ProcessGroup)
    private readonly processGroupRepository: Repository<ProcessGroup>,
  ) {
    super();
    this.name = ProcessGroupService.name;
    this.subject = SubjectMessage.PROCESS_GROUP;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.processGroupRepository.findAndCount();
    return this.response(new Pagination<ProcessGroup>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.processGroupRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.PROCESS_GROUP,
        true,
      );
    return this.response(result);
  }
}
