import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from 'src/common/base/class/base.class';
import { TagEnum } from 'src/common/enum/tag.enum';
import { ProcessGroupService } from './process-group.service';

@Controller()
@ApiTags(TagEnum.PROCESS_GROUP)
export class ProcessGroupController {
  constructor(private readonly processGroupService: ProcessGroupService) {}

  @Get()
  findAll() {
    return this.processGroupService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.processGroupService.findOne(path);
  }
}
