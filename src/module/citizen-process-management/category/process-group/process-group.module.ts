import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProcessGroupController } from './process-group.controller';
import { ProcessGroupService } from './process-group.service';
import { ProcessGroup } from './entities/process-group.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProcessGroup])],
  controllers: [ProcessGroupController],
  providers: [ProcessGroupService],
  exports: [ProcessGroupService],
})
export class ProcessGroupModule {
  static readonly route: RouteTree = {
    path: 'process-group',
    module: ProcessGroupModule,
    children: [],
  };
}
