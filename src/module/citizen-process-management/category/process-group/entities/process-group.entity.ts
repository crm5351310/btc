import { Column, Entity, OneToMany } from 'typeorm';
import { Process } from '../../process/entities/process.entity';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';

@Entity()
export class ProcessGroup extends BaseEntity {
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @OneToMany(() => Process, (process) => process.processGroup)
  processes: Process[];
}
