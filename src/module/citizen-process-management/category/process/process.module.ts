import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProcessController } from './process.controller';
import { ProcessService } from './process.service';
import { Process } from './entities/process.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Process])],
  controllers: [ProcessController],
  providers: [ProcessService],
  exports: [ProcessService],
})
export class ProcessModule {
  static readonly route: RouteTree = {
    path: 'process',
    module: ProcessModule,
    children: [],
  };
}
