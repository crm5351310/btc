import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsNumber } from 'class-validator';
import { ListTypeEnum } from 'src/common/enum/list-type.enum';

export class FindManyProcessParamDto {
  @ApiProperty({
    name: 'processGroupId',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  processGroupId?: number;

  @ApiProperty({
    name: 'listType',
    required: false,
    enum: ListTypeEnum,
  })
  @IsOptional()
  listType?: ListTypeEnum;
}
