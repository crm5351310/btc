import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { ProcessGroup } from '../../process-group/entities/process-group.entity';
import { CitizenProcess } from '../../../../../module/citizen-process-management/citizen-process/entities/citizen-process.entity';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';

@Entity()
export class Process extends BaseEntity {
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @ManyToOne(() => ProcessGroup, (group) => group.processes)
  processGroup: ProcessGroup;

  @ManyToMany(() => Process, (process) => process.preProcesses)
  @JoinTable()
  nextProcesses: Process[];

  @ManyToMany(() => Process, (process) => process.nextProcesses)
  preProcesses: Process[];

  @Column('varchar', { length: 100, nullable: true })
  modulePath?: string;

  @Column('varchar', { length: 100, nullable: true })
  processPath?: string;
}
