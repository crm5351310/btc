import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService, PathDto } from 'src/common/base/class/base.class';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { BaseMessage } from 'src/common/message/base.message';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';
import { SubjectMessage } from 'src/common/message/subject.message';
import { Pagination } from 'src/common/utils/pagination.util';
import { In, Repository } from 'typeorm';
import { FindManyProcessParamDto } from './dto/find-many-process-param.dto';
import { Process } from './entities/process.entity';
import { ListTypeEnum } from 'src/common/enum/list-type.enum';
import {
  NVQS_CKT,
  NVQS_CNN,
  NVQS_CST,
  NVQS_DK,
  NVQS_DKT,
  NVQS_DKTN,
  NVQS_DST,
  NVQS_GNNCT,
  NVQS_GNNDP,
  NVQS_GST,
  NVQS_KDKT,
  NVQS_KDST,
  NVQS_NN,
  NVQS_TKT,
  NVQS_TNN,
  NVQS_TST,
  NVQS_VKT,
  NVQS_VNN,
  NVQS_VST,
} from '../../../../database/seeds/process.seed';

@Injectable()
export class ProcessService extends BaseService {
  constructor(
    @InjectRepository(Process)
    private readonly processRepository: Repository<Process>,
  ) {
    super();
    this.name = ProcessService.name;
    this.subject = SubjectMessage.PROCESS;
  }
  async findAll(param: FindManyProcessParamDto) {
    this.action = BaseMessage.READ;
    let processIds: number[] = [];
    if (param.listType) processIds = this.findProcessIdsByListType(param.listType);
    const [result, totals] = await this.processRepository.findAndCount({
      where: {
        processGroup: {
          id: param.processGroupId ?? undefined,
        },
        id: param.listType ? In(processIds) : undefined,
      },
      relations: {
        processGroup: true,
      },
    });
    return this.response(new Pagination<Process>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.processRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.PROCESS, true);
    return this.response(result);
  }

  /**
   * @param listType Kiểu danh sách: sẵn sàng nhập ngũ, xét duyệt cấp xã, xét duyệt cấp huyện, nhập ngũ
   * @returns mảng các ids process tương ứng với kiểu danh sách
   */
  findProcessIdsByListType(listType: ListTypeEnum) {
    const result: number[] = [];
    if (listType === ListTypeEnum.SAN_SANG_NHAP_NGU) result.push(NVQS_DK.id, NVQS_DKTN.id);
    if (listType === ListTypeEnum.XET_DUYET_CAP_XA)
      result.push(NVQS_GST.id, NVQS_KDST.id, NVQS_CST.id, NVQS_TST.id, NVQS_VST.id);
    if (listType === ListTypeEnum.XET_DUYET_CAP_HUYEN)
      result.push(NVQS_DST.id, NVQS_KDKT.id, NVQS_CKT.id, NVQS_TKT.id, NVQS_VKT.id);
    if (listType === ListTypeEnum.NHAP_NGU)
      result.push(
        NVQS_DKT.id,
        NVQS_GNNCT.id,
        NVQS_GNNDP.id,
        NVQS_NN.id,
        NVQS_VNN.id,
        NVQS_CNN.id,
        NVQS_TNN.id,
      );
    return result;
  }

}
