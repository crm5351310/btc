import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from 'src/common/base/class/base.class';
import { TagEnum } from 'src/common/enum/tag.enum';
import { FindManyProcessParamDto } from './dto/find-many-process-param.dto';
import { ProcessService } from './process.service';

@Controller()
@ApiTags(TagEnum.PROCESS)
export class ProcessController {
  constructor(private readonly processService: ProcessService) {}

  @Get()
  findAll(@Query() param: FindManyProcessParamDto) {
    return this.processService.findAll(param);
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.processService.findOne(path);
  }
}
