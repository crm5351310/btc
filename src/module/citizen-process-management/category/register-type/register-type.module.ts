import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegisterType } from './entities/register-type.entity';
import { RegisterTypeController } from './register-type.controller';
import { RegisterTypeService } from './register-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([RegisterType])],
  controllers: [RegisterTypeController],
  providers: [RegisterTypeService],
  exports: [RegisterTypeService],
})
export class RegisterTypeModule {
  static readonly route: RouteTree = {
    path: 'register-type',
    module: RegisterTypeModule,
    children: [],
  };
}
