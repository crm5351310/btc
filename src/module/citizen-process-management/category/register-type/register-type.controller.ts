import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from 'src/common/base/class/base.class';
import { TagEnum } from 'src/common/enum/tag.enum';
import { RegisterTypeService } from './register-type.service';

@Controller()
@ApiTags(TagEnum.REGISTER_TYPE)
export class RegisterTypeController {
  constructor(private readonly registerTypeService: RegisterTypeService) {}

  @Get()
  findAll() {
    return this.registerTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.registerTypeService.findOne(path);
  }
}
