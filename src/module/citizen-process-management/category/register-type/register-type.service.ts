import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';
import { BaseService, PathDto } from 'src/common/base/class/base.class';
import { SubjectMessage } from 'src/common/message/subject.message';
import { BaseMessage } from 'src/common/message/base.message';
import { Pagination } from 'src/common/utils/pagination.util';
import { RegisterType } from './entities/register-type.entity';

@Injectable()
export class RegisterTypeService extends BaseService {
  constructor(
    @InjectRepository(RegisterType)
    private readonly registerTypeRepository: Repository<RegisterType>,
  ) {
    super();
    this.name = RegisterTypeService.name;
    this.subject = SubjectMessage.REGISTER_TYPE;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.registerTypeRepository.findAndCount();
    return this.response(new Pagination<RegisterType>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.registerTypeRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.REGISTER_TYPE,
        true,
      );
    return this.response(result);
  }
}
