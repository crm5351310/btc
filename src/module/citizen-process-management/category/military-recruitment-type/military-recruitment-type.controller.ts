import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from 'src/common/base/class/base.class';
import { TagEnum } from 'src/common/enum/tag.enum';
import { MilitaryRecruitmentTypeService } from './military-recruitment-type.service';

@Controller()
@ApiTags(TagEnum.MILITARY_RECRUITMENT_TYPE)
export class MilitaryRecruitmentTypeController {
  constructor(private readonly militaryRecruitmentTypeService: MilitaryRecruitmentTypeService) {}

  @Get()
  findAll() {
    return this.militaryRecruitmentTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.militaryRecruitmentTypeService.findOne(path);
  }
}
