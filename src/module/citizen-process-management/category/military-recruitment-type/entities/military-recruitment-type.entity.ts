import { NVQSDKTNProcess } from '../../../citizen-process/nvqs-process/nvqs-dktn-process/entities/nvqs-dktn-process.entity';
import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity()
export class MilitaryRecruitmentType extends BaseEntity {
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @Column('varchar', { length: 100, nullable: false })
  name: string;

  @OneToMany(() => NVQSDKTNProcess, (process) => process.militaryRecruitmentType)
  NVQSDKTNProcesses: NVQSDKTNProcess[];
}
