import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';
import { BaseService, PathDto } from 'src/common/base/class/base.class';
import { SubjectMessage } from 'src/common/message/subject.message';
import { BaseMessage } from 'src/common/message/base.message';
import { Pagination } from 'src/common/utils/pagination.util';
import { MilitaryRecruitmentType } from './entities/military-recruitment-type.entity';

@Injectable()
export class MilitaryRecruitmentTypeService extends BaseService {
  constructor(
    @InjectRepository(MilitaryRecruitmentType)
    private readonly militaryRecruitmentTypeRepository: Repository<MilitaryRecruitmentType>,
  ) {
    super();
    this.name = MilitaryRecruitmentTypeService.name;
    this.subject = SubjectMessage.MILITARY_RECRUITMENT_TYPE;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.militaryRecruitmentTypeRepository.findAndCount();
    return this.response(new Pagination<MilitaryRecruitmentType>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.militaryRecruitmentTypeRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.MILITARY_RECRUITMENT_TYPE,
        true,
      );
    return this.response(result);
  }
}
