import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryRecruitmentType } from './entities/military-recruitment-type.entity';
import { MilitaryRecruitmentTypeController } from './military-recruitment-type.controller';
import { MilitaryRecruitmentTypeService } from './military-recruitment-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryRecruitmentType])],
  controllers: [MilitaryRecruitmentTypeController],
  providers: [MilitaryRecruitmentTypeService],
  exports: [MilitaryRecruitmentTypeService],
})
export class MilitaryRecruitmentTypeModule {
  static readonly route: RouteTree = {
    path: 'military-recruitment-type',
    module: MilitaryRecruitmentTypeModule,
    children: [],
  };
}
