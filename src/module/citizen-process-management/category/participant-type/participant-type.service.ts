import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';
import { BaseService, PathDto } from 'src/common/base/class/base.class';
import { SubjectMessage } from 'src/common/message/subject.message';
import { BaseMessage } from 'src/common/message/base.message';
import { Pagination } from 'src/common/utils/pagination.util';
import { ParticipantType } from './entities/participant-type.entity';

@Injectable()
export class ParticipantTypeService extends BaseService {
  constructor(
    @InjectRepository(ParticipantType)
    private readonly participantTypeRepository: Repository<ParticipantType>,
  ) {
    super();
    this.name = ParticipantTypeService.name;
    this.subject = SubjectMessage.PARTICIPANT_TYPE;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.participantTypeRepository.findAndCount();
    return this.response(new Pagination<ParticipantType>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.participantTypeRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.PARTICIPANT_TYPE,
        true,
      );
    return this.response(result);
  }
}
