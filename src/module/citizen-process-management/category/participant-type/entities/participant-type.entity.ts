import { BaseEntity } from '../../../../../common/base/entity/base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class ParticipantType extends BaseEntity {
  @Column('varchar', { length: 25, nullable: false })
  code: string;

  @Column('varchar', { length: 100, nullable: false })
  name: string;
}
