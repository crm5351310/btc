import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParticipantType } from './entities/participant-type.entity';
import { ParticipantTypeController } from './participant-type.controller';
import { ParticipantTypeService } from './participant-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([ParticipantType])],
  controllers: [ParticipantTypeController],
  providers: [ParticipantTypeService],
  exports: [ParticipantTypeService],
})
export class ParticipantTypeModule {
  static readonly route: RouteTree = {
    path: 'participant-type',
    module: ParticipantTypeModule,
    children: [],
  };
}
