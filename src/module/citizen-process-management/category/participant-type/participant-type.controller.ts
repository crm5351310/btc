import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from 'src/common/base/class/base.class';
import { TagEnum } from 'src/common/enum/tag.enum';
import { ParticipantTypeService } from './participant-type.service';

@Controller()
@ApiTags(TagEnum.PARTICIPANT_TYPE)
export class ParticipantTypeController {
  constructor(private readonly ParticipantTypeService: ParticipantTypeService) {}

  @Get()
  findAll() {
    return this.ParticipantTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.ParticipantTypeService.findOne(path);
  }
}
