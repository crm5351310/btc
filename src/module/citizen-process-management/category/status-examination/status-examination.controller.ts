import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from 'src/common/enum/tag.enum';
import { StatusExaminationService } from './status-examination.service';

@Controller()
@ApiTags(TagEnum.STATUS_EXAMINATION)
export class StatusExaminationController {
  constructor(private readonly service: StatusExaminationService) {}

  @Get()
  findAll() {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.service.findOne(+id);
  }
}
