import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StatusExaminationController } from './status-examination.controller';
import { StatusExaminationService } from './status-examination.service';
import { StatusExamination } from './entities/status-examination.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StatusExamination])],
  controllers: [StatusExaminationController],
  providers: [StatusExaminationService],
  exports: [StatusExaminationService],
})
export class StatusExaminationModule {
  static readonly route: RouteTree = {
    path: 'status-examination',
    module: StatusExaminationModule,
    children: [],
  };
}
