import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseSimpleCategoryService } from '../../../../common/base/service/base-simple-category.service';
import { ResponseFindAll, ResponseFindOne } from '../../../../common/response';
import { Repository } from 'typeorm';
import { StatusExamination } from './entities/status-examination.entity';

@Injectable()
export class StatusExaminationService extends BaseSimpleCategoryService<StatusExamination> {
  constructor(
    @InjectRepository(StatusExamination)
    repository: Repository<StatusExamination>,
  ) {
    super(repository);
  }
  async findAll(): Promise<ResponseFindAll<StatusExamination>> {
    const [results, total] = await this.repository.findAndCount();
    return new ResponseFindAll(results, total);
  }

  async findOne(id: number): Promise<ResponseFindOne<StatusExamination>> {
    const result = await this.checkNotExist(id, StatusExamination.name);

    return new ResponseFindOne(result);
  }
}
