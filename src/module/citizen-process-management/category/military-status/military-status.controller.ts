import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from 'src/common/base/class/base.class';
import { TagEnum } from 'src/common/enum/tag.enum';
import { MilitaryStatusService } from './military-status.service';

@Controller()
@ApiTags(TagEnum.MILITARY_STATUS)
export class MilitaryStatusController {
  constructor(private readonly militaryStatusService: MilitaryStatusService) {}

  @Get()
  findAll() {
    return this.militaryStatusService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.militaryStatusService.findOne(path);
  }
}
