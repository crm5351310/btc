import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilitaryStatus } from './entities/military-status.entity';
import { MilitaryStatusController } from './military-status.controller';
import { MilitaryStatusService } from './military-status.service';

@Module({
  imports: [TypeOrmModule.forFeature([MilitaryStatus])],
  controllers: [MilitaryStatusController],
  providers: [MilitaryStatusService],
  exports: [MilitaryStatusService],
})
export class MilitaryStatusModule {
  static readonly route: RouteTree = {
    path: 'military-status',
    module: MilitaryStatusModule,
    children: [],
  };
}
