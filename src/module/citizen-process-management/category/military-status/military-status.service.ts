import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { BaseService, PathDto } from '../../../../common/base/class/base.class';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { MilitaryStatus } from './entities/military-status.entity';
import { BaseMessage } from '../../../../common/message/base.message';
import { Pagination } from '../../../../common/utils/pagination.util';

@Injectable()
export class MilitaryStatusService extends BaseService {
  constructor(
    @InjectRepository(MilitaryStatus)
    private readonly militaryStatusRepository: Repository<MilitaryStatus>,
  ) {
    super();
    this.name = MilitaryStatusService.name;
    this.subject = SubjectMessage.MILITARY_STATUS;
  }
  async findAll() {
    this.action = BaseMessage.READ;
    const [result, totals] = await this.militaryStatusRepository.findAndCount();
    return this.response(new Pagination<MilitaryStatus>(result, totals));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.militaryStatusRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.MILITARY_STATUS,
        true,
      );
    return this.response(result);
  }
}
