import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenProcessController } from './citizen-process.controller';
import { CitizenProcessService } from './citizen-process.service';
import { CitizenProcess } from './entities/citizen-process.entity';
import { NVQSProcessModule } from './nvqs-process/nvqs-process.module';
import { DbdvProcessModule } from './dbdv-process/dbdv-process.module';
import { DqtvProcessModule } from './dqtv-process/dqtv-process.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([CitizenProcess]),
    NVQSProcessModule,
    DbdvProcessModule,
    DqtvProcessModule,
  ],
  controllers: [CitizenProcessController],
  providers: [CitizenProcessService],
  exports: [CitizenProcessService],
})
export class CitizenProcessModule {
  static readonly route: RouteTree = {
    path: 'citizen-process',
    module: CitizenProcessModule,
    children: [NVQSProcessModule.route, DbdvProcessModule.route, DqtvProcessModule.route],
  };
}
