import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class FindAllCitizenProcessDto {
  @ApiProperty({
    name: 'citizenId',
    required: true,
  })
  @IsNumber()
  citizenId: number;
}
