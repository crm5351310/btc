import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvBcsqdbProcessService } from './dbdv-bcsqdb-process.service';
import { CreateDbdvBcsqdbProcessDto } from './dto/create-dbdv-bcsqdb-process.dto';
import { UpdateDbdvBcsqdbProcessDto } from './dto/update-dbdv-bcsqdb-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_BCSQDB_PROCESS)
export class DbdvBcsqdbProcessController {
  constructor(private readonly dbdvBcsqdbProcessService: DbdvBcsqdbProcessService) { }

  @Post()
  create(@Body() createDbdvBcsqdbProcessDto: CreateDbdvBcsqdbProcessDto) {
    return this.dbdvBcsqdbProcessService.create(createDbdvBcsqdbProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvBcsqdbProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvBcsqdbProcessDto: UpdateDbdvBcsqdbProcessDto) {
    return this.dbdvBcsqdbProcessService.update(+id, updateDbdvBcsqdbProcessDto);
  }
}
