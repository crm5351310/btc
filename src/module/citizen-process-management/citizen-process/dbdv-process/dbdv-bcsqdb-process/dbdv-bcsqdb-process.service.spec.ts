import { Test, TestingModule } from '@nestjs/testing';
import { DbdvBcsqdbProcessService } from './dbdv-bcsqdb-process.service';

describe('DbdvBcsqdbProcessService', () => {
  let service: DbdvBcsqdbProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvBcsqdbProcessService],
    }).compile();

    service = module.get<DbdvBcsqdbProcessService>(DbdvBcsqdbProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
