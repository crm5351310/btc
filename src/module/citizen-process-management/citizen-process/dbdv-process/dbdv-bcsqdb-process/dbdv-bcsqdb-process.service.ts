import { Injectable } from '@nestjs/common';
import { CreateDbdvBcsqdbProcessDto } from './dto/create-dbdv-bcsqdb-process.dto';
import { UpdateDbdvBcsqdbProcessDto } from './dto/update-dbdv-bcsqdb-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvBcsqdbProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_BCSQDB } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvBcsqdbProcessService extends CitizenProcessServiceBase<DbdvBcsqdbProcess> {
  constructor(
    @InjectRepository(DbdvBcsqdbProcess)
    repository: Repository<DbdvBcsqdbProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvBcsqdbProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        militaryStatus: true,
        militaryUnit: {
          militaryUnitClassification: true,
        },
        militaryRank: {
          militaryRankType: true,
        },
        jobTitle: true,
        militaryJobPosition: {
          militaryJob: {
            militaryJobGroup: true,
          }
        }
      }
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvBcsqdbProcessDto): Promise<ResponseCreated<DbdvBcsqdbProcess>> {
    return await super.create(dto, DBDV_BCSQDB);
  }

  async update(id: number, dto: UpdateDbdvBcsqdbProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
