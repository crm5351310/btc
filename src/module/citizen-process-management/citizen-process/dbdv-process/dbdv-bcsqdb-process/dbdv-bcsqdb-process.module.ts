import { Module } from '@nestjs/common';
import { DbdvBcsqdbProcessService } from './dbdv-bcsqdb-process.service';
import { DbdvBcsqdbProcessController } from './dbdv-bcsqdb-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvBcsqdbProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_BCSQDB } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvBcsqdbProcess]), CitizenModule],
  controllers: [DbdvBcsqdbProcessController],
  providers: [DbdvBcsqdbProcessService],
})
export class DbdvBcsqdbProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_BCSQDB.processPath,
    module: DbdvBcsqdbProcessModule,
  };
}
