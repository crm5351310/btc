import { OmitType } from '@nestjs/swagger';
import { DbdvBcsqdbProcess } from '../entities';

export class CreateDbdvBcsqdbProcessDto extends OmitType(DbdvBcsqdbProcess, ['id']) {}
