import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvBcsqdbProcessDto } from './create-dbdv-bcsqdb-process.dto';

export class UpdateDbdvBcsqdbProcessDto extends PartialType(
  OmitType(CreateDbdvBcsqdbProcessDto, ['citizen']),
) {}
