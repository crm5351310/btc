import { ChildEntity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { MilitaryRank } from '../../../../../category/military-rank-root/military-rank/military-rank.entity';
import { JobTitle } from '../../../../../category/job-title/job-title.entity';
import { MilitaryJobPosition } from '../../../../../category/military-job-root/military-job-position/entities/military-job-position.entity';
import { MilitaryStatus } from '../../../../category/military-status/entities/military-status.entity';
import { MilitaryUnit } from '../../../../../category/military-unit-root/military-unit/entities';

@ChildEntity()
export class DbdvBcsqdbProcess extends CitizenProcess {
  @ApiProperty({
    description: 'Trạng thái chuyên nghiệp quân sự',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryStatus, { nullable: true })
  militaryStatus?: MilitaryStatus;

  @ApiProperty({
    description: 'Đơn vị quân đội',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryUnit, { nullable: true })
  militaryUnit?: MilitaryUnit;

  @ApiProperty({
    description: 'Quân hàm',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRank, { nullable: true })
  militaryRank?: MilitaryRank;

  @ApiProperty({
    description: 'Chức danh',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => JobTitle, { nullable: true })
  jobTitle?: JobTitle;

  @ApiProperty({
    description: 'Vị trí chuyên môn',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryJobPosition, { nullable: true })
  militaryJobPosition?: MilitaryJobPosition;
}
