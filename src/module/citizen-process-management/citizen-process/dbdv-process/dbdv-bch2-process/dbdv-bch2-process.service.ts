import { Injectable } from '@nestjs/common';
import { CreateDbdvBch2ProcessDto } from './dto/create-dbdv-bch2-process.dto';
import { UpdateDbdvBch2ProcessDto } from './dto/update-dbdv-bch2-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvBch2Process } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_BCH2 } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvBch2ProcessService extends CitizenProcessServiceBase<DbdvBch2Process> {
  constructor(
    @InjectRepository(DbdvBch2Process)
    repository: Repository<DbdvBch2Process>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvBch2Process | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        militaryStatus: true,
        militaryUnit: {
          militaryUnitClassification: true,
        },
        militaryRank: {
          militaryRankType: true,
        },
        militaryJobPosition: {
          militaryJob: {
            militaryJobGroup: true,
          }
        }
      }
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvBch2ProcessDto): Promise<ResponseCreated<DbdvBch2Process>> {
    return await super.create(dto, DBDV_BCH2);
  }

  async update(id: number, dto: UpdateDbdvBch2ProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
