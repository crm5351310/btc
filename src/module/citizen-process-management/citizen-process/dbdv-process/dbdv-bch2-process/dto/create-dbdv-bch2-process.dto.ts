import { OmitType } from '@nestjs/swagger';
import { DbdvBch2Process } from '../entities';

export class CreateDbdvBch2ProcessDto extends OmitType(DbdvBch2Process, ['id']) {}
