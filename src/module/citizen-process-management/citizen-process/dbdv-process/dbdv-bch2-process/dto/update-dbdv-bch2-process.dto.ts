import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvBch2ProcessDto } from './create-dbdv-bch2-process.dto';

export class UpdateDbdvBch2ProcessDto extends PartialType(
  OmitType(CreateDbdvBch2ProcessDto, ['citizen']),
) {}
