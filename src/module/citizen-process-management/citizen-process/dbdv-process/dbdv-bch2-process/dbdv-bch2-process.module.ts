import { Module } from '@nestjs/common';
import { DbdvBch2ProcessService } from './dbdv-bch2-process.service';
import { DbdvBch2ProcessController } from './dbdv-bch2-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvBch2Process } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_BCH2 } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvBch2Process]), CitizenModule],
  controllers: [DbdvBch2ProcessController],
  providers: [DbdvBch2ProcessService],
})
export class DbdvBch2ProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_BCH2.processPath,
    module: DbdvBch2ProcessModule,
  };
}
