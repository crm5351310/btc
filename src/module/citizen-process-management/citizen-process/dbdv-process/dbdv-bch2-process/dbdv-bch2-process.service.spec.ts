import { Test, TestingModule } from '@nestjs/testing';
import { DbdvBch2ProcessService } from './dbdv-bch2-process.service';

describe('DbdvBch2ProcessService', () => {
  let service: DbdvBch2ProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvBch2ProcessService],
    }).compile();

    service = module.get<DbdvBch2ProcessService>(DbdvBch2ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
