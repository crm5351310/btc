import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvBch2ProcessService } from './dbdv-bch2-process.service';
import { CreateDbdvBch2ProcessDto } from './dto/create-dbdv-bch2-process.dto';
import { UpdateDbdvBch2ProcessDto } from './dto/update-dbdv-bch2-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_BCH2_PROCESS)
export class DbdvBch2ProcessController {
  constructor(private readonly dbdvBch2ProcessService: DbdvBch2ProcessService) { }

  @Post()
  create(@Body() createDbdvBch2ProcessDto: CreateDbdvBch2ProcessDto) {
    return this.dbdvBch2ProcessService.create(createDbdvBch2ProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvBch2ProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvBch2ProcessDto: UpdateDbdvBch2ProcessDto) {
    return this.dbdvBch2ProcessService.update(+id, updateDbdvBch2ProcessDto);
  }
}
