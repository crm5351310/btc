import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvTnh2ProcessService } from './dbdv-tnh2-process.service';
import { CreateDbdvTnh2ProcessDto } from './dto/create-dbdv-tnh2-process.dto';
import { UpdateDbdvTnh2ProcessDto } from './dto/update-dbdv-tnh2-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_TNH2_PROCESS)
export class DbdvTnh2ProcessController {
  constructor(private readonly dbdvTnh2ProcessService: DbdvTnh2ProcessService) { }

  @Post()
  create(@Body() createDbdvTnh2ProcessDto: CreateDbdvTnh2ProcessDto) {
    return this.dbdvTnh2ProcessService.create(createDbdvTnh2ProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvTnh2ProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvTnh2ProcessDto: UpdateDbdvTnh2ProcessDto) {
    return this.dbdvTnh2ProcessService.update(+id, updateDbdvTnh2ProcessDto);
  }
}
