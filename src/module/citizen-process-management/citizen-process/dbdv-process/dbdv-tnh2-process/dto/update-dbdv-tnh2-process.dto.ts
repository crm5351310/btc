import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvTnh2ProcessDto } from './create-dbdv-tnh2-process.dto';

export class UpdateDbdvTnh2ProcessDto extends PartialType(
  OmitType(CreateDbdvTnh2ProcessDto, ['citizen']),
) {}
