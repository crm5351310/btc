import { OmitType } from '@nestjs/swagger';
import { DbdvTnh2Process } from '../entities';

export class CreateDbdvTnh2ProcessDto extends OmitType(DbdvTnh2Process, ['id']) {}
