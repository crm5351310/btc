import { Module } from '@nestjs/common';
import { DbdvTnh2ProcessService } from './dbdv-tnh2-process.service';
import { DbdvTnh2ProcessController } from './dbdv-tnh2-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvTnh2Process } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_TNH2 } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvTnh2Process]), CitizenModule],
  controllers: [DbdvTnh2ProcessController],
  providers: [DbdvTnh2ProcessService],
})
export class DbdvTnh2ProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_TNH2.processPath,
    module: DbdvTnh2ProcessModule,
  };
}
