import { Test, TestingModule } from '@nestjs/testing';
import { DbdvTnh2ProcessService } from './dbdv-tnh2-process.service';

describe('DbdvTnh2ProcessService', () => {
  let service: DbdvTnh2ProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvTnh2ProcessService],
    }).compile();

    service = module.get<DbdvTnh2ProcessService>(DbdvTnh2ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
