import { Injectable } from '@nestjs/common';
import { CreateDbdvTnh2ProcessDto } from './dto/create-dbdv-tnh2-process.dto';
import { UpdateDbdvTnh2ProcessDto } from './dto/update-dbdv-tnh2-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvTnh2Process } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_TNH2 } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvTnh2ProcessService extends CitizenProcessServiceBase<DbdvTnh2Process> {
  constructor(
    @InjectRepository(DbdvTnh2Process)
    repository: Repository<DbdvTnh2Process>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvTnh2Process | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvTnh2ProcessDto): Promise<ResponseCreated<DbdvTnh2Process>> {
    return await super.create(dto, DBDV_TNH2);
  }

  async update(id: number, dto: UpdateDbdvTnh2ProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
