import { Module } from '@nestjs/common';
import { DbdvDkh1ProcessService } from './dbdv-dkh1-process.service';
import { DbdvDkh1ProcessController } from './dbdv-dkh1-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvDkh1Process } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_H1 } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvDkh1Process]), CitizenModule],
  controllers: [DbdvDkh1ProcessController],
  providers: [DbdvDkh1ProcessService],
})
export class DbdvDkh1ProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_H1.processPath,
    module: DbdvDkh1ProcessModule,
  };
}
