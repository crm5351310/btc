import { ChildEntity, Column, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities';
import { IsOptional, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { RegisterType } from '../../../../category/register-type/entities/register-type.entity';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { MilitaryRank } from '../../../../../category/military-rank-root/military-rank/military-rank.entity';
import { MilitaryJobPosition } from '../../../../../category/military-job-root/military-job-position/entities/military-job-position.entity';

@ChildEntity()
export class DbdvDkh1Process extends CitizenProcess {
  @ApiProperty({
    description: 'Số QNDB',
    example: '123/ABC',
    maxLength: 25,
    required: false,
  })
  @IsOptional()
  @MaxLength(25)
  @Column('varchar', { nullable: true, length: 25 })
  reserveNumber?: string;

  @ApiProperty({
    description: 'Loại đăng ký',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => RegisterType, { nullable: true })
  registerType?: RegisterType;

  @ApiProperty({
    description: 'Quân hàm',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRank, { nullable: true })
  militaryRank?: MilitaryRank;

  @ApiProperty({
    description: 'Vị trí chuyên môn',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryJobPosition, { nullable: true })
  militaryJobPosition?: MilitaryJobPosition;
}
