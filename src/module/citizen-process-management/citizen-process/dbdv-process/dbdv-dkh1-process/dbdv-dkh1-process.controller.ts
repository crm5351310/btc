import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvDkh1ProcessService } from './dbdv-dkh1-process.service';
import { CreateDbdvDkh1ProcessDto } from './dto/create-dbdv-dkh1-process.dto';
import { UpdateDbdvDkh1ProcessDto } from './dto/update-dbdv-dkh1-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_DKH1_PROCESS)
export class DbdvDkh1ProcessController {
  constructor(private readonly dbdvDkh1ProcessService: DbdvDkh1ProcessService) { }

  @Post()
  create(@Body() createDbdvDkh1ProcessDto: CreateDbdvDkh1ProcessDto) {
    return this.dbdvDkh1ProcessService.create(createDbdvDkh1ProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvDkh1ProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvDkh1ProcessDto: UpdateDbdvDkh1ProcessDto) {
    return this.dbdvDkh1ProcessService.update(+id, updateDbdvDkh1ProcessDto);
  }
}
