import { Test, TestingModule } from '@nestjs/testing';
import { DbdvDkh1ProcessService } from './dbdv-dkh1-process.service';

describe('DbdvDkh1ProcessService', () => {
  let service: DbdvDkh1ProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvDkh1ProcessService],
    }).compile();

    service = module.get<DbdvDkh1ProcessService>(DbdvDkh1ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
