import { Injectable } from '@nestjs/common';
import { CreateDbdvDkh1ProcessDto } from './dto/create-dbdv-dkh1-process.dto';
import { UpdateDbdvDkh1ProcessDto } from './dto/update-dbdv-dkh1-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvDkh1Process } from './entities';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_H1 } from '../../../../../database/seeds/process.seed';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';

@Injectable()
export class DbdvDkh1ProcessService extends CitizenProcessServiceBase<DbdvDkh1Process> {
  constructor(
    @InjectRepository(DbdvDkh1Process)
    repository: Repository<DbdvDkh1Process>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvDkh1Process | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        registerType: true,
        militaryRank: {
          militaryRankType: true,
        },
        militaryJobPosition: {
          militaryJob: {
            militaryJobGroup: true,
          }
        }
      },
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvDkh1ProcessDto): Promise<ResponseCreated<DbdvDkh1Process>> {
    return await super.create(dto, DBDV_H1);
  }

  async update(id: number, dto: UpdateDbdvDkh1ProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
