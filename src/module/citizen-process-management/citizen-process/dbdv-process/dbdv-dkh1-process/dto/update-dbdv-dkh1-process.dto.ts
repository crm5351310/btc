import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvDkh1ProcessDto } from './create-dbdv-dkh1-process.dto';

export class UpdateDbdvDkh1ProcessDto extends PartialType(
  OmitType(CreateDbdvDkh1ProcessDto, ['citizen']),
) {}
