import { OmitType } from '@nestjs/swagger';
import { DbdvDkh1Process } from '../entities';

export class CreateDbdvDkh1ProcessDto extends OmitType(DbdvDkh1Process, ['id']) {}
