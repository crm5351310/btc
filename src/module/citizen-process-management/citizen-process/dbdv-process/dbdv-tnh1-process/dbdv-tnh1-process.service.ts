import { Injectable } from '@nestjs/common';
import { CreateDbdvTnh1ProcessDto } from './dto/create-dbdv-tnh1-process.dto';
import { UpdateDbdvTnh1ProcessDto } from './dto/update-dbdv-tnh1-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvTnh1Process } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_TNH1 } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvTnh1ProcessService extends CitizenProcessServiceBase<DbdvTnh1Process> {
  constructor(
    @InjectRepository(DbdvTnh1Process)
    repository: Repository<DbdvTnh1Process>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvTnh1Process | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvTnh1ProcessDto): Promise<ResponseCreated<DbdvTnh1Process>> {
    return await super.create(dto, DBDV_TNH1);
  }

  async update(id: number, dto: UpdateDbdvTnh1ProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
