import { Module } from '@nestjs/common';
import { DbdvTnh1ProcessService } from './dbdv-tnh1-process.service';
import { DbdvTnh1ProcessController } from './dbdv-tnh1-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvTnh1Process } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_TNH1 } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvTnh1Process]), CitizenModule],
  controllers: [DbdvTnh1ProcessController],
  providers: [DbdvTnh1ProcessService],
})
export class DbdvTnh1ProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_TNH1.processPath,
    module: DbdvTnh1ProcessModule,
  };
}
