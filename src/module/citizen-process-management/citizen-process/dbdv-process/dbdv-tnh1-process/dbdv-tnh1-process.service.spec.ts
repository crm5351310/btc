import { Test, TestingModule } from '@nestjs/testing';
import { DbdvTnh1ProcessService } from './dbdv-tnh1-process.service';

describe('DbdvTnh1ProcessService', () => {
  let service: DbdvTnh1ProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvTnh1ProcessService],
    }).compile();

    service = module.get<DbdvTnh1ProcessService>(DbdvTnh1ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
