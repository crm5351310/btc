import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvTnh1ProcessDto } from './create-dbdv-tnh1-process.dto';

export class UpdateDbdvTnh1ProcessDto extends PartialType(
  OmitType(CreateDbdvTnh1ProcessDto, ['citizen']),
) {}
