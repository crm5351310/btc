import { OmitType } from '@nestjs/swagger';
import { DbdvTnh1Process } from '../entities';

export class CreateDbdvTnh1ProcessDto extends OmitType(DbdvTnh1Process, ['id']) {}
