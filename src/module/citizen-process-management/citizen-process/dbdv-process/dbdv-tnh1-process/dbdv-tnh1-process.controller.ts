import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvTnh1ProcessService } from './dbdv-tnh1-process.service';
import { CreateDbdvTnh1ProcessDto } from './dto/create-dbdv-tnh1-process.dto';
import { UpdateDbdvTnh1ProcessDto } from './dto/update-dbdv-tnh1-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_TNH1_PROCESS)
export class DbdvTnh1ProcessController {
  constructor(private readonly dbdvTnh1ProcessService: DbdvTnh1ProcessService) { }

  @Post()
  create(@Body() createDbdvTnh1ProcessDto: CreateDbdvTnh1ProcessDto) {
    return this.dbdvTnh1ProcessService.create(createDbdvTnh1ProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvTnh1ProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvTnh1ProcessDto: UpdateDbdvTnh1ProcessDto) {
    return this.dbdvTnh1ProcessService.update(+id, updateDbdvTnh1ProcessDto);
  }
}
