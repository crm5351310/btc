import { Injectable } from '@nestjs/common';
import { CreateDbdvBch1ProcessDto } from './dto/create-dbdv-bch1-process.dto';
import { UpdateDbdvBch1ProcessDto } from './dto/update-dbdv-bch1-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvBch1Process } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_BCH1 } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvBch1ProcessService extends CitizenProcessServiceBase<DbdvBch1Process> {
  constructor(
    @InjectRepository(DbdvBch1Process)
    repository: Repository<DbdvBch1Process>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvBch1Process | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        militaryStatus: true,
        militaryUnit: {
          militaryUnitClassification: true,
        },
        militaryRank: {
          militaryRankType: true,
        },
        militaryJobPosition: {
          militaryJob: {
            militaryJobGroup: true,
          }
        }
      }
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvBch1ProcessDto): Promise<ResponseCreated<DbdvBch1Process>> {
    return await super.create(dto, DBDV_BCH1);
  }

  async update(id: number, dto: UpdateDbdvBch1ProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
