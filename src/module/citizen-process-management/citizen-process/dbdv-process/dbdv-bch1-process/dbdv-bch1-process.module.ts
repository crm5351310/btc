import { Module } from '@nestjs/common';
import { DbdvBch1ProcessService } from './dbdv-bch1-process.service';
import { DbdvBch1ProcessController } from './dbdv-bch1-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvBch1Process } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_BCH1 } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvBch1Process]), CitizenModule],
  controllers: [DbdvBch1ProcessController],
  providers: [DbdvBch1ProcessService],
})
export class DbdvBch1ProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_BCH1.processPath,
    module: DbdvBch1ProcessModule,
  };
}
