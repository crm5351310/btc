import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvBch1ProcessService } from './dbdv-bch1-process.service';
import { CreateDbdvBch1ProcessDto } from './dto/create-dbdv-bch1-process.dto';
import { UpdateDbdvBch1ProcessDto } from './dto/update-dbdv-bch1-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_BCH1_PROCESS)
export class DbdvBch1ProcessController {
  constructor(private readonly dbdvBch1ProcessService: DbdvBch1ProcessService) { }

  @Post()
  create(@Body() createDbdvBch1ProcessDto: CreateDbdvBch1ProcessDto) {
    return this.dbdvBch1ProcessService.create(createDbdvBch1ProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvBch1ProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvBch1ProcessDto: UpdateDbdvBch1ProcessDto) {
    return this.dbdvBch1ProcessService.update(+id, updateDbdvBch1ProcessDto);
  }
}
