import { Test, TestingModule } from '@nestjs/testing';
import { DbdvBch1ProcessService } from './dbdv-bch1-process.service';

describe('DbdvBch1ProcessService', () => {
  let service: DbdvBch1ProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvBch1ProcessService],
    }).compile();

    service = module.get<DbdvBch1ProcessService>(DbdvBch1ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
