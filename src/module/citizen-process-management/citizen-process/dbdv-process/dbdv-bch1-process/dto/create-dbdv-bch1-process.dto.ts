import { OmitType } from '@nestjs/swagger';
import { DbdvBch1Process } from '../entities';

export class CreateDbdvBch1ProcessDto extends OmitType(DbdvBch1Process, ['id']) {}
