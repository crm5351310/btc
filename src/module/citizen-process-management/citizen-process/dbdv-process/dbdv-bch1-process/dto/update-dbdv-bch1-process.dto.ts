import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvBch1ProcessDto } from './create-dbdv-bch1-process.dto';

export class UpdateDbdvBch1ProcessDto extends PartialType(
  OmitType(CreateDbdvBch1ProcessDto, ['citizen']),
) {}
