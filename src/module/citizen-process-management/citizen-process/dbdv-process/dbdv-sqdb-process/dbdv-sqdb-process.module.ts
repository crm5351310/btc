import { Module } from '@nestjs/common';
import { DbdvSqdbProcessService } from './dbdv-sqdb-process.service';
import { DbdvSqdbProcessController } from './dbdv-sqdb-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvSqdbProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_SQDB } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvSqdbProcess]), CitizenModule],
  controllers: [DbdvSqdbProcessController],
  providers: [DbdvSqdbProcessService],
})
export class DbdvSqdbProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_SQDB.processPath,
    module: DbdvSqdbProcessModule,
  };
}
