import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvSqdbProcessDto } from './create-dbdv-sqdb-process.dto';

export class UpdateDbdvSqdbProcessDto extends PartialType(
  OmitType(CreateDbdvSqdbProcessDto, ['citizen']),
) {}
