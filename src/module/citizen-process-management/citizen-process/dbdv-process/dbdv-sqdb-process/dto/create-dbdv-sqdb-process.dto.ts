import { ChildEntity } from 'typeorm';
import { DbdvSqdbProcess } from '../entities';
import { OmitType } from '@nestjs/swagger';

@ChildEntity()
export class CreateDbdvSqdbProcessDto extends OmitType(DbdvSqdbProcess, ['id']) {}
