import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvSqdbProcessService } from './dbdv-sqdb-process.service';
import { CreateDbdvSqdbProcessDto } from './dto/create-dbdv-sqdb-process.dto';
import { UpdateDbdvSqdbProcessDto } from './dto/update-dbdv-sqdb-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_SQDB_PROCESS)
export class DbdvSqdbProcessController {
  constructor(private readonly dbdvSqdbProcessService: DbdvSqdbProcessService) { }

  @Post()
  create(@Body() createDbdvSqdbProcessDto: CreateDbdvSqdbProcessDto) {
    return this.dbdvSqdbProcessService.create(createDbdvSqdbProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvSqdbProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvSqdbProcessDto: UpdateDbdvSqdbProcessDto) {
    return this.dbdvSqdbProcessService.update(+id, updateDbdvSqdbProcessDto);
  }
}
