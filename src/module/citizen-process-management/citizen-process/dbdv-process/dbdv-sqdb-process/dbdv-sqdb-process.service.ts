import { Injectable } from '@nestjs/common';
import { CreateDbdvSqdbProcessDto } from './dto/create-dbdv-sqdb-process.dto';
import { UpdateDbdvSqdbProcessDto } from './dto/update-dbdv-sqdb-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvSqdbProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_SQDB } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvSqdbProcessService extends CitizenProcessServiceBase<DbdvSqdbProcess> {
  constructor(
    @InjectRepository(DbdvSqdbProcess)
    repository: Repository<DbdvSqdbProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvSqdbProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        registerType: true,
        militaryRank: {
          militaryRankType: true,
        },
        jobTitle: true,
        militaryJobPosition: {
          militaryJob: {
            militaryJobGroup: true,
          }
        },
      },
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvSqdbProcessDto): Promise<ResponseCreated<DbdvSqdbProcess>> {
    return await super.create(dto, DBDV_SQDB);
  }

  async update(id: number, dto: UpdateDbdvSqdbProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
