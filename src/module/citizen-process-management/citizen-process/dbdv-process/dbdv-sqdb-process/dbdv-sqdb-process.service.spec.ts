import { Test, TestingModule } from '@nestjs/testing';
import { DbdvSqdbProcessService } from './dbdv-sqdb-process.service';

describe('DbdvSqdbProcessService', () => {
  let service: DbdvSqdbProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvSqdbProcessService],
    }).compile();

    service = module.get<DbdvSqdbProcessService>(DbdvSqdbProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
