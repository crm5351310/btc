import { ChildEntity, Column, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, MaxLength } from 'class-validator';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { RegisterType } from '../../../../category/register-type/entities/register-type.entity';
import { MilitaryRank } from '../../../../../category/military-rank-root/military-rank/military-rank.entity';
import { MilitaryJobPosition } from '../../../../../category/military-job-root/military-job-position/entities/military-job-position.entity';
import { JobTitle } from '../../../../../category/job-title/job-title.entity';

@ChildEntity()
export class DbdvSqdbProcess extends CitizenProcess {
  @ApiProperty({
    description: 'Số QNDB',
    example: '123/ABC',
    maxLength: 25,
    required: false,
  })
  @IsOptional()
  @MaxLength(25)
  @Column('varchar', { nullable: true, length: 25 })
  reserveNumber?: string;

  @ApiProperty({
    description: 'Loại đăng ký',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => RegisterType, { nullable: true })
  registerType?: RegisterType;

  @ApiProperty({
    description: 'Quân hàm',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRank, { nullable: true })
  militaryRank?: MilitaryRank;

  @ApiProperty({
    description: 'Chức danh',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => JobTitle, { nullable: true })
  jobTitle?: JobTitle;

  @ApiProperty({
    description: 'Vị trí chuyên môn',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryJobPosition, { nullable: true })
  militaryJobPosition?: MilitaryJobPosition;
}
