import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvDkh2ProcessService } from './dbdv-dkh2-process.service';
import { CreateDbdvDkh2ProcessDto } from './dto/create-dbdv-dkh2-process.dto';
import { UpdateDbdvDkh2ProcessDto } from './dto/update-dbdv-dkh2-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_DKH2_PROCESS)
export class DbdvDkh2ProcessController {
  constructor(private readonly dbdvDkh2ProcessService: DbdvDkh2ProcessService) { }

  @Post()
  create(@Body() createDbdvDkh2ProcessDto: CreateDbdvDkh2ProcessDto) {
    return this.dbdvDkh2ProcessService.create(createDbdvDkh2ProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvDkh2ProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvDkh2ProcessDto: UpdateDbdvDkh2ProcessDto) {
    return this.dbdvDkh2ProcessService.update(+id, updateDbdvDkh2ProcessDto);
  }
}
