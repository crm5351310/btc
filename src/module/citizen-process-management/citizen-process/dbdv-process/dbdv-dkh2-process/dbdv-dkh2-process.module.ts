import { Module } from '@nestjs/common';
import { DbdvDkh2ProcessService } from './dbdv-dkh2-process.service';
import { DbdvDkh2ProcessController } from './dbdv-dkh2-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvDkh2Process } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_H2 } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvDkh2Process]), CitizenModule],
  controllers: [DbdvDkh2ProcessController],
  providers: [DbdvDkh2ProcessService],
})
export class DbdvDkh2ProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_H2.processPath,
    module: DbdvDkh2ProcessModule,
  };
}
