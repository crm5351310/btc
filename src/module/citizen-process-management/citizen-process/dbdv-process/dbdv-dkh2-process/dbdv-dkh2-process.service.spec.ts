import { Test, TestingModule } from '@nestjs/testing';
import { DbdvDkh2ProcessService } from './dbdv-dkh2-process.service';

describe('DbdvDkh2ProcessService', () => {
  let service: DbdvDkh2ProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvDkh2ProcessService],
    }).compile();

    service = module.get<DbdvDkh2ProcessService>(DbdvDkh2ProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
