import { Injectable } from '@nestjs/common';
import { CreateDbdvDkh2ProcessDto } from './dto/create-dbdv-dkh2-process.dto';
import { UpdateDbdvDkh2ProcessDto } from './dto/update-dbdv-dkh2-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvDkh2Process } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_H2 } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvDkh2ProcessService extends CitizenProcessServiceBase<DbdvDkh2Process> {
  constructor(
    @InjectRepository(DbdvDkh2Process)
    repository: Repository<DbdvDkh2Process>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvDkh2Process | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        registerType: true,
        militaryRank: {
          militaryRankType: true,
        },
        militaryJobPosition: {
          militaryJob: {
            militaryJobGroup: true,
          }
        }
      },
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvDkh2ProcessDto): Promise<ResponseCreated<DbdvDkh2Process>> {
    return await super.create(dto, DBDV_H2);
  }

  async update(id: number, dto: UpdateDbdvDkh2ProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
