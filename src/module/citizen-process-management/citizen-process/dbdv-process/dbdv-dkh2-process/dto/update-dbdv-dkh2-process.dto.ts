import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvDkh2ProcessDto } from './create-dbdv-dkh2-process.dto';

export class UpdateDbdvDkh2ProcessDto extends PartialType(
  OmitType(CreateDbdvDkh2ProcessDto, ['citizen']),
) {}
