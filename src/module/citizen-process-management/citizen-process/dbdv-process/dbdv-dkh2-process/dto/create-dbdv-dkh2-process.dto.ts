import { OmitType } from '@nestjs/swagger';
import { DbdvDkh2Process } from '../entities';

export class CreateDbdvDkh2ProcessDto extends OmitType(DbdvDkh2Process, ['id']) {}
