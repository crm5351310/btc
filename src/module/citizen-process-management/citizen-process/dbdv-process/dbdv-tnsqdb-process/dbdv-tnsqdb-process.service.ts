import { Injectable } from '@nestjs/common';
import { CreateDbdvTnsqdbProcessDto } from './dto/create-dbdv-tnsqdb-process.dto';
import { UpdateDbdvTnsqdbProcessDto } from './dto/update-dbdv-tnsqdb-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DbdvTnsqdbProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DBDV_TNSQDB } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DbdvTnsqdbProcessService extends CitizenProcessServiceBase<DbdvTnsqdbProcess> {
  constructor(
    @InjectRepository(DbdvTnsqdbProcess)
    repository: Repository<DbdvTnsqdbProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DbdvTnsqdbProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  async create(dto: CreateDbdvTnsqdbProcessDto): Promise<ResponseCreated<DbdvTnsqdbProcess>> {
    return await super.create(dto, DBDV_TNSQDB);
  }

  async update(id: number, dto: UpdateDbdvTnsqdbProcessDto): Promise<ResponseUpdate> {
    return await super.update(id, dto);
  }
}
