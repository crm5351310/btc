import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DbdvTnsqdbProcessService } from './dbdv-tnsqdb-process.service';
import { CreateDbdvTnsqdbProcessDto } from './dto/create-dbdv-tnsqdb-process.dto';
import { UpdateDbdvTnsqdbProcessDto } from './dto/update-dbdv-tnsqdb-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DBDV_TNSQDB_PROCESS)
export class DbdvTnsqdbProcessController {
  constructor(private readonly dbdvTnsqdbProcessService: DbdvTnsqdbProcessService) { }

  @Post()
  create(@Body() createDbdvTnsqdbProcessDto: CreateDbdvTnsqdbProcessDto) {
    return this.dbdvTnsqdbProcessService.create(createDbdvTnsqdbProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dbdvTnsqdbProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDbdvTnsqdbProcessDto: UpdateDbdvTnsqdbProcessDto) {
    return this.dbdvTnsqdbProcessService.update(+id, updateDbdvTnsqdbProcessDto);
  }
}
