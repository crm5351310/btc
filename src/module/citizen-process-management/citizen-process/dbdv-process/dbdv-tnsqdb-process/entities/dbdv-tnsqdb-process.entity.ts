import { ChildEntity } from 'typeorm';
import { CitizenProcess } from '../../../entities';

@ChildEntity()
export class DbdvTnsqdbProcess extends CitizenProcess {}
