import { Test, TestingModule } from '@nestjs/testing';
import { DbdvTnsqdbProcessService } from './dbdv-tnsqdb-process.service';

describe('DbdvTnsqdbProcessService', () => {
  let service: DbdvTnsqdbProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DbdvTnsqdbProcessService],
    }).compile();

    service = module.get<DbdvTnsqdbProcessService>(DbdvTnsqdbProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
