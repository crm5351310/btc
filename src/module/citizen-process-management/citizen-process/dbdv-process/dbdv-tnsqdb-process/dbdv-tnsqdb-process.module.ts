import { Module } from '@nestjs/common';
import { DbdvTnsqdbProcessService } from './dbdv-tnsqdb-process.service';
import { DbdvTnsqdbProcessController } from './dbdv-tnsqdb-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DbdvTnsqdbProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DBDV_TNSQDB } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DbdvTnsqdbProcess]), CitizenModule],
  controllers: [DbdvTnsqdbProcessController],
  providers: [DbdvTnsqdbProcessService],
})
export class DbdvTnsqdbProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_TNSQDB.processPath,
    module: DbdvTnsqdbProcessModule,
  };
}
