import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDbdvTnsqdbProcessDto } from './create-dbdv-tnsqdb-process.dto';

export class UpdateDbdvTnsqdbProcessDto extends PartialType(
  OmitType(CreateDbdvTnsqdbProcessDto, ['citizen']),
) {}
