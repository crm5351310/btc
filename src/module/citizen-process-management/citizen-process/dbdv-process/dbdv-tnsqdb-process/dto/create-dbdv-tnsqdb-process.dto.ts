import { OmitType } from '@nestjs/swagger';
import { DbdvTnsqdbProcess } from '../entities';

export class CreateDbdvTnsqdbProcessDto extends OmitType(DbdvTnsqdbProcess, ['id']) {}
