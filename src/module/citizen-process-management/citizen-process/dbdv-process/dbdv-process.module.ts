import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { DbdvDkh1ProcessModule } from './dbdv-dkh1-process/dbdv-dkh1-process.module';
import { DbdvDkh2ProcessModule } from './dbdv-dkh2-process/dbdv-dkh2-process.module';
import { DbdvBch1ProcessModule } from './dbdv-bch1-process/dbdv-bch1-process.module';
import { DbdvBch2ProcessModule } from './dbdv-bch2-process/dbdv-bch2-process.module';
import { DbdvTnh1ProcessModule } from './dbdv-tnh1-process/dbdv-tnh1-process.module';
import { DbdvTnh2ProcessModule } from './dbdv-tnh2-process/dbdv-tnh2-process.module';
import { DbdvSqdbProcessModule } from './dbdv-sqdb-process/dbdv-sqdb-process.module';
import { DbdvBcsqdbProcessModule } from './dbdv-bcsqdb-process/dbdv-bcsqdb-process.module';
import { DbdvTnsqdbProcessModule } from './dbdv-tnsqdb-process/dbdv-tnsqdb-process.module';
import { DBDV_H1 } from 'src/database/seeds/process.seed';

@Module({
  imports: [
    DbdvDkh1ProcessModule,
    DbdvDkh2ProcessModule,
    DbdvBch1ProcessModule,
    DbdvBch2ProcessModule,
    DbdvTnh1ProcessModule,
    DbdvTnh2ProcessModule,
    DbdvSqdbProcessModule,
    DbdvBcsqdbProcessModule,
    DbdvTnsqdbProcessModule,
  ],
})
export class DbdvProcessModule {
  static readonly route: RouteTree = {
    path: DBDV_H1.modulePath,
    module: DbdvProcessModule,
    children: [
      DbdvDkh1ProcessModule.route,
      DbdvDkh2ProcessModule.route,
      DbdvBch1ProcessModule.route,
      DbdvBch2ProcessModule.route,
      DbdvTnh1ProcessModule.route,
      DbdvTnh2ProcessModule.route,
      DbdvSqdbProcessModule.route,
      DbdvBcsqdbProcessModule.route,
      DbdvTnsqdbProcessModule.route,
    ],
  };
}
