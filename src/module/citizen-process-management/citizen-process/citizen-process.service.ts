import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseCRUDService } from 'src/common/base/service/base-crud.service';
import { ResponseDelete, ResponseFindAll, ResponseFindOne } from 'src/common/response';
import { In, Repository } from 'typeorm';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import { CitizenProcess } from './entities/citizen-process.entity';
import { FindAllCitizenProcessDto } from './dto/find-all-citizen-process.dto';
@Injectable()
export class CitizenProcessService
  implements Pick<BaseCRUDService<CitizenProcess>, 'findOne' | 'findAll' | 'remove'>
{
  constructor(
    @InjectRepository(CitizenProcess)
    private readonly repository: Repository<CitizenProcess>,
  ) {}

  async findAll(param: FindAllCitizenProcessDto) {
    const [result, total] = await this.repository.findAndCount({
      where: {
        citizen: {
          id: param.citizenId,
        },
      },
      order: {
        fromDate: 'DESC',
      },
      relations: {
        process: {
          nextProcesses: true,
          processGroup: true,
        },
      },
    });
    return new ResponseFindAll(result, total);
  }

  async findOne(id: number) {
    const data = await this.repository.findOne({
      where: {
        id: id,
      },
    });
    return new ResponseFindOne(data);
  }

  async remove(ids: number[]) {
    const checkExistCitizenProcess = await this.repository.find({
      where: {
        id: In(ids),
      },
    });
    if (checkExistCitizenProcess.length === 0)
      throw new CustomBadRequestException(
        ContentMessage.FAILURE,
        FieldMessage.CITIZEN_PROCESS,
        true,
      );
    await this.repository.softRemove(checkExistCitizenProcess);
    return new ResponseDelete(checkExistCitizenProcess, ids);
  }
}
