import { Controller, Delete, Get, Param, ParseArrayPipe, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CitizenProcessService } from './citizen-process.service';
import { FindAllCitizenProcessDto } from './dto/find-all-citizen-process.dto';

@Controller()
@ApiTags(TagEnum.CITIZEN_PROCESS)
export class CitizenProcessController {
  constructor(private readonly citizenProcessService: CitizenProcessService) {}

  @Get()
  findAll(@Query() param: FindAllCitizenProcessDto) {
    return this.citizenProcessService.findAll(param);
  }

  @Delete(':ids')
  remove(
    @Param('ids', new ParseArrayPipe({ items: Number, separator: ',' }))
    ids: number[],
  ) {
    return this.citizenProcessService.remove(ids);
  }
}
