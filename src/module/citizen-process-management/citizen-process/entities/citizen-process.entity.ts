import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty, IsOptional, Matches } from 'class-validator';
import { Column, Entity, ManyToOne, TableInheritance } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { IsObjectRelation } from '../../../../common/validator/is-object-relation';
import { Citizen } from '../../../citizen-management/citizen/entities/citizen.entity';
import { Process } from '../../category/process/entities/process.entity';

@Entity()
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
export class CitizenProcess extends BaseEntity {
  @ManyToOne(() => Process, { nullable: false })
  process: Process;

  @ApiProperty({
    description: 'ID của công dân',
    example: { id: 1 },
    type: () => Citizen,
  })
  @IsObjectRelation()
  @ManyToOne(() => Citizen, (citizen) => citizen.citizenProcesses, {
    nullable: false,
  })
  citizen: Citizen;

  @ApiProperty({
    description: 'Từ ngày',
    example: '2024-03-22',
  })
  @IsNotEmpty()
  @Column('datetime')
  fromDate: Date;

  @ApiProperty({
    description: 'Đến ngày',
    default: '2024-09-22',
    required: false,
    type: Date || null,
  })
  @IsOptional()
  @Column('datetime', { nullable: true })
  toDate?: Date;
}
