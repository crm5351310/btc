import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQS_KDST } from 'src/database/seeds/process.seed';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSKDSTProcess } from './entities/nvqs-kdst-process.entity';
import { NVQSKDSTProcessController } from './nvqs-kdst-process.controller';
import { NVQSKDSTProcessService } from './nvqs-kdst-process.service';
import { HealthModule } from '../../../../category/health/health.module';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSKDSTProcess]), CitizenModule, HealthModule],
  controllers: [NVQSKDSTProcessController],
  providers: [NVQSKDSTProcessService],
  exports: [NVQSKDSTProcessService],
})
export class NVQSKDSTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_KDST.processPath,
    module: NVQSKDSTProcessModule,
    children: [],
  };
}
