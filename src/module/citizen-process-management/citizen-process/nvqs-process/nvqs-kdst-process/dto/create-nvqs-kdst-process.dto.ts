import { OmitType } from '@nestjs/swagger';
import { NVQSKDSTProcess } from '../entities/nvqs-kdst-process.entity';

export class CreateNVQSKDSTProcessDto extends OmitType(NVQSKDSTProcess, ['id']) {}
