import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSKDSTProcessDto } from './create-nvqs-kdst-process.dto';

export class UpdateNVQSKDSTProcessDto extends OmitType(PartialType(CreateNVQSKDSTProcessDto), [
  'citizen',
]) {}
