import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSKDSTProcessDto } from './dto/create-nvqs-kdst-process.dto';
import { UpdateNVQSKDSTProcessDto } from './dto/update-nvqs-kdst-process.dto';
import { NVQSKDSTProcessService } from './nvqs-kdst-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_KDST_PROCESS)
export class NVQSKDSTProcessController {
  constructor(private readonly NVQSKDSTProcessService: NVQSKDSTProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSKDSTProcessDto) {
    return this.NVQSKDSTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSKDSTProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSKDSTProcessDto) {
    return this.NVQSKDSTProcessService.update(+id, updateCitizenProcessDto);
  }
}
