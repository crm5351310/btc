import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSKDSTProcessDto } from './dto/create-nvqs-kdst-process.dto';
import { UpdateNVQSKDSTProcessDto } from './dto/update-nvqs-kdst-process.dto';
import { NVQSKDSTProcess } from './entities/nvqs-kdst-process.entity';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_KDST } from '../../../../../database/seeds/process.seed';
import { HealthService } from 'src/module/category/health/health.service';

@Injectable()
export class NVQSKDSTProcessService extends CitizenProcessServiceBase<NVQSKDSTProcess> {
  constructor(
    @InjectRepository(NVQSKDSTProcess)
    repository: Repository<NVQSKDSTProcess>,
    citizenService: CitizenService,
    private readonly healthService: HealthService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSKDSTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        health: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSKDSTProcessDto,
  ): Promise<ResponseCreated<NVQSKDSTProcess>> {
    // check tồn tại phân loại sức khoẻ
    await this.healthService.findOne(createCitizenProcessDto.health.id);

    return await super.create(createCitizenProcessDto, NVQS_KDST);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSKDSTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
