import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSGSTProcessDto } from './dto/create-nvqs-gst-process.dto';
import { UpdateNVQSGSTProcessDto } from './dto/update-nvqs-gst-process.dto';
import { NVQSGSTProcess } from './entities/nvqs-gst-process.entity';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_GST } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NVQSGSTProcessService extends CitizenProcessServiceBase<NVQSGSTProcess> {
  constructor(
    @InjectRepository(NVQSGSTProcess)
    repository: Repository<NVQSGSTProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSGSTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        militaryRecruitmentPeriod: true, 
        militaryRecruitmentType: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSGSTProcessDto,
  ): Promise<ResponseCreated<NVQSGSTProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_GST);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSGSTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
