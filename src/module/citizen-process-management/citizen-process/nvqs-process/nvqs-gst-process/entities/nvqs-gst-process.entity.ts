import { ChildEntity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities/citizen-process.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { MilitaryRecruitmentType } from '../../../../../citizen-process-management/category/military-recruitment-type/entities/military-recruitment-type.entity';
import { MilitaryRecruitmentPeriod } from '../../../../../militaty-recruitment/military-recruit-period/entities/military-recruit-period.entity';

@ChildEntity()
export class NVQSGSTProcess extends CitizenProcess {
  @ApiProperty({
    description: 'loại nghĩa vụ',
    example: { id: 1 },
    type: () => MilitaryRecruitmentType,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRecruitmentType, (type) => type.NVQSDKTNProcesses)
  militaryRecruitmentType: MilitaryRecruitmentType;

  @ApiProperty({
    description: 'đợt tuyển quân',
    example: { id: 1 },
    type: () => MilitaryRecruitmentPeriod,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRecruitmentPeriod)
  militaryRecruitmentPeriod: MilitaryRecruitmentPeriod;
}
