import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSGSTProcessDto } from './create-nvqs-gst-process.dto';

export class UpdateNVQSGSTProcessDto extends OmitType(PartialType(CreateNVQSGSTProcessDto), [
  'citizen',
]) {}
