import { OmitType } from '@nestjs/swagger';
import { NVQSGSTProcess } from '../entities/nvqs-gst-process.entity';

export class CreateNVQSGSTProcessDto extends OmitType(NVQSGSTProcess, ['id']) {}
