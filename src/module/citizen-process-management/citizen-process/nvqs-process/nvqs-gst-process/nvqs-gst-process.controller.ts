import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSGSTProcessDto } from './dto/create-nvqs-gst-process.dto';
import { UpdateNVQSGSTProcessDto } from './dto/update-nvqs-gst-process.dto';
import { NVQSGSTProcessService } from './nvqs-gst-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_GST_PROCESS)
export class NVQSGSTProcessController {
  constructor(private readonly NVQSGSTProcessService: NVQSGSTProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSGSTProcessDto) {
    return this.NVQSGSTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSGSTProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSGSTProcessDto) {
    return this.NVQSGSTProcessService.update(+id, updateCitizenProcessDto);
  }
}
