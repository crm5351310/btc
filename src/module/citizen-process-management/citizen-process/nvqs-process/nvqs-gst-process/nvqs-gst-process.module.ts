import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQS_GST } from 'src/database/seeds/process.seed';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSGSTProcess } from './entities/nvqs-gst-process.entity';
import { NVQSGSTProcessController } from './nvqs-gst-process.controller';
import { NVQSGSTProcessService } from './nvqs-gst-process.service';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSGSTProcess]), CitizenModule],
  controllers: [NVQSGSTProcessController],
  providers: [NVQSGSTProcessService],
  exports: [NVQSGSTProcessService],
})
export class NVQSGSTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_GST.processPath,
    module: NVQSGSTProcessModule,
    children: [],
  };
}
