import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSVNNProcessDto } from './dto/create-nvqs-vnn-process.dto';
import { UpdateNVQSVNNProcessDto } from './dto/update-nvqs-vnn-process.dto';
import { NVQSVNNProcessService } from './nvqs-vnn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_VNN_PROCESS)
export class NVQSVNNProcessController {
  constructor(private readonly NVQSVNNProcessService: NVQSVNNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSVNNProcessDto) {
    return this.NVQSVNNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSVNNProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSVNNProcessDto) {
    return this.NVQSVNNProcessService.update(+id, updateCitizenProcessDto);
  }
}
