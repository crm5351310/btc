import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSVNNProcessDto } from './create-nvqs-vnn-process.dto';

export class UpdateNVQSVNNProcessDto extends OmitType(PartialType(CreateNVQSVNNProcessDto), [
  'citizen',
]) {}
