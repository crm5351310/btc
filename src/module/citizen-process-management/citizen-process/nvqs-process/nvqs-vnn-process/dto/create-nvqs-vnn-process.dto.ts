import { OmitType } from '@nestjs/swagger';
import { NVQSVNNProcess } from '../entities/nvqs-vnn-process.entity';

export class CreateNVQSVNNProcessDto extends OmitType(NVQSVNNProcess, ['id']) {}
