import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSVNNProcess } from './entities/nvqs-vnn-process.entity';
import { NVQSVNNProcessController } from './nvqs-vnn-process.controller';
import { NVQSVNNProcessService } from './nvqs-vnn-process.service';
import { NVQS_VNN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSVNNProcess]), CitizenModule],
  controllers: [NVQSVNNProcessController],
  providers: [NVQSVNNProcessService],
  exports: [NVQSVNNProcessService],
})
export class NVQSVNNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_VNN.processPath,
    module: NVQSVNNProcessModule,
    children: [],
  };
}
