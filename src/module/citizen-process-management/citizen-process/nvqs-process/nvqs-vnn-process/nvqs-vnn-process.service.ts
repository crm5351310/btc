import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_VNN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSVNNProcessDto } from './dto/create-nvqs-vnn-process.dto';
import { UpdateNVQSVNNProcessDto } from './dto/update-nvqs-vnn-process.dto';
import { NVQSVNNProcess } from './entities/nvqs-vnn-process.entity';

@Injectable()
export class NVQSVNNProcessService extends CitizenProcessServiceBase<NVQSVNNProcess> {
  constructor(
    @InjectRepository(NVQSVNNProcess)
    repository: Repository<NVQSVNNProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSVNNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSVNNProcessDto,
  ): Promise<ResponseCreated<NVQSVNNProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_VNN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSVNNProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
