import { OmitType } from '@nestjs/swagger';
import { NVQSTNNProcess } from '../entities/nvqs-tnn-process.entity';

export class CreateNVQSTNNProcessDto extends OmitType(NVQSTNNProcess, ['id']) {}
