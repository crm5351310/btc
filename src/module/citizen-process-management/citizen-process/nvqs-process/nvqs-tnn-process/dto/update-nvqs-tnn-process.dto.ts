import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSTNNProcessDto } from './create-nvqs-tnn-process.dto';

export class UpdateNVQSTNNProcessDto extends OmitType(PartialType(CreateNVQSTNNProcessDto), [
  'citizen',
]) {}
