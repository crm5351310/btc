import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSTNNProcess } from './entities/nvqs-tnn-process.entity';
import { NVQSTNNProcessController } from './nvqs-tnn-process.controller';
import { NVQSTNNProcessService } from './nvqs-tnn-process.service';
import { NVQS_TNN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSTNNProcess]), CitizenModule],
  controllers: [NVQSTNNProcessController],
  providers: [NVQSTNNProcessService],
  exports: [NVQSTNNProcessService],
})
export class NVQSTNNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_TNN.processPath,
    module: NVQSTNNProcessModule,
    children: [],
  };
}
