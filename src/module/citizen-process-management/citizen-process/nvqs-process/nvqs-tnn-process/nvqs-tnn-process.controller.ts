import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSTNNProcessDto } from './dto/create-nvqs-tnn-process.dto';
import { UpdateNVQSTNNProcessDto } from './dto/update-nvqs-tnn-process.dto';
import { NVQSTNNProcessService } from './nvqs-tnn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_TNN_PROCESS)
export class NVQSTNNProcessController {
  constructor(private readonly NVQSTNNProcessService: NVQSTNNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSTNNProcessDto) {
    return this.NVQSTNNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSTNNProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSTNNProcessDto) {
    return this.NVQSTNNProcessService.update(+id, updateCitizenProcessDto);
  }
}
