import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_TNN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSTNNProcessDto } from './dto/create-nvqs-tnn-process.dto';
import { UpdateNVQSTNNProcessDto } from './dto/update-nvqs-tnn-process.dto';
import { NVQSTNNProcess } from './entities/nvqs-tnn-process.entity';

@Injectable()
export class NVQSTNNProcessService extends CitizenProcessServiceBase<NVQSTNNProcess> {
  constructor(
    @InjectRepository(NVQSTNNProcess)
    repository: Repository<NVQSTNNProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSTNNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSTNNProcessDto,
  ): Promise<ResponseCreated<NVQSTNNProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_TNN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSTNNProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
