import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSGNNCTProcessDto } from './dto/create-nvqs-gnnct-process.dto';
import { UpdateNVQSGNNCTProcessDto } from './dto/update-nvqs-gnnct-process.dto';
import { NVQSGNNCTProcessService } from './nvqs-gnnct-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_GNNCT_PROCESS)
export class NVQSGNNCTProcessController {
  constructor(private readonly NVQSGNNCTProcessService: NVQSGNNCTProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSGNNCTProcessDto) {
    return this.NVQSGNNCTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSGNNCTProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSGNNCTProcessDto) {
    return this.NVQSGNNCTProcessService.update(+id, updateCitizenProcessDto);
  }
}
