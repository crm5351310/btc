import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQSGNNCTProcess } from './entities/nvqs-gnnct-process.entity';
import { NVQSGNNCTProcessController } from './nvqs-gnnct-process.controller';
import { NVQSGNNCTProcessService } from './nvqs-gnnct-process.service';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQS_GNNCT } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSGNNCTProcess]), CitizenModule],
  controllers: [NVQSGNNCTProcessController],
  providers: [NVQSGNNCTProcessService],
  exports: [NVQSGNNCTProcessService],
})
export class NVQSGNNCTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_GNNCT.processPath,
    module: NVQSGNNCTProcessModule,
    children: [],
  };
}
