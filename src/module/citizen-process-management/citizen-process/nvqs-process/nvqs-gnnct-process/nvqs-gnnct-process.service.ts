import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_GNNCT } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSGNNCTProcessDto } from './dto/create-nvqs-gnnct-process.dto';
import { UpdateNVQSGNNCTProcessDto } from './dto/update-nvqs-gnnct-process.dto';
import { NVQSGNNCTProcess } from './entities/nvqs-gnnct-process.entity';

@Injectable()
export class NVQSGNNCTProcessService extends CitizenProcessServiceBase<NVQSGNNCTProcess> {
  constructor(
    @InjectRepository(NVQSGNNCTProcess)
    repository: Repository<NVQSGNNCTProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSGNNCTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSGNNCTProcessDto,
  ): Promise<ResponseCreated<NVQSGNNCTProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_GNNCT);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSGNNCTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
