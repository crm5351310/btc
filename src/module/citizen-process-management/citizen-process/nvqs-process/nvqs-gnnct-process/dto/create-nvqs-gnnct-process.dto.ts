import { OmitType } from '@nestjs/swagger';
import { NVQSGNNCTProcess } from '../entities/nvqs-gnnct-process.entity';

export class CreateNVQSGNNCTProcessDto extends OmitType(NVQSGNNCTProcess, ['id']) {}
