import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSGNNCTProcessDto } from './create-nvqs-gnnct-process.dto';

export class UpdateNVQSGNNCTProcessDto extends OmitType(PartialType(CreateNVQSGNNCTProcessDto), [
  'citizen',
]) {}
