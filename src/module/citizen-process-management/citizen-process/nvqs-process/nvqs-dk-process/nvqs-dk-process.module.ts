import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQSDKProcess } from './entities/nvqs-dk-process.entity';
import { NVQSDKProcessController } from './nvqs-dk-process.controller';
import { NVQSDKProcessService } from './nvqs-dk-process.service';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { CitizenProcess } from '../../entities/citizen-process.entity';
import { NVQS_DK } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSDKProcess]), CitizenModule],
  controllers: [NVQSDKProcessController],
  providers: [NVQSDKProcessService],
  exports: [NVQSDKProcessService],
})
export class NVQSDKProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DK.processPath,
    module: NVQSDKProcessModule,
    children: [],
  };
}
