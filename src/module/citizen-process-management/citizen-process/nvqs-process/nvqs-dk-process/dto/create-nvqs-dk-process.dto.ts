import { OmitType } from '@nestjs/swagger';
import { NVQSDKProcess } from '../entities/nvqs-dk-process.entity';

export class CreateNVQSDKProcessDto extends OmitType(NVQSDKProcess, ['id']) {}
