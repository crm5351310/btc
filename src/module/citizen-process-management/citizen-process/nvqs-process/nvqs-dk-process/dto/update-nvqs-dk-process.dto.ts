import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSDKProcessDto } from './create-nvqs-dk-process.dto';

export class UpdateNVQSDKProcessDto extends OmitType(PartialType(CreateNVQSDKProcessDto), [
  'citizen',
]) {}
