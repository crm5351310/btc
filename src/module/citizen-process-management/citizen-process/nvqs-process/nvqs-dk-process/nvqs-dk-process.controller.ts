import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSDKProcessDto } from './dto/create-nvqs-dk-process.dto';
import { UpdateNVQSDKProcessDto } from './dto/update-nvqs-dk-process.dto';
import { NVQSDKProcessService } from './nvqs-dk-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_DK_PROCESS)
export class NVQSDKProcessController {
  constructor(private readonly NVQSDKProcessService: NVQSDKProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSDKProcessDto) {
    return this.NVQSDKProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSDKProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSDKProcessDto) {
    return this.NVQSDKProcessService.update(+id, updateCitizenProcessDto);
  }
}
