import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSDKProcessDto } from './dto/create-nvqs-dk-process.dto';
import { UpdateNVQSDKProcessDto } from './dto/update-nvqs-dk-process.dto';
import { NVQSDKProcess } from './entities/nvqs-dk-process.entity';
import { CitizenService } from '../../../../../module/citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_DK } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NVQSDKProcessService extends CitizenProcessServiceBase<NVQSDKProcess> {
  constructor(
    @InjectRepository(NVQSDKProcess)
    repository: Repository<NVQSDKProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSDKProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSDKProcessDto,
  ): Promise<ResponseCreated<NVQSDKProcess>> {
    console.log(createCitizenProcessDto)
    return await super.create(createCitizenProcessDto, NVQS_DK);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSDKProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
