import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_GNNDP } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSGNNDPProcessDto } from './dto/create-gnndp-dk-process.dto';
import { UpdateNVQSGNNDPProcessDto } from './dto/update-gnndp-dk-process.dto';
import { NVQSGNNDPProcess } from './entities/nvqs-gnndp-process.entity';

@Injectable()
export class NVQSGNNDPProcessService extends CitizenProcessServiceBase<NVQSGNNDPProcess> {
  constructor(
    @InjectRepository(NVQSGNNDPProcess)
    repository: Repository<NVQSGNNDPProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSGNNDPProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSGNNDPProcessDto,
  ): Promise<ResponseCreated<NVQSGNNDPProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_GNNDP);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSGNNDPProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
