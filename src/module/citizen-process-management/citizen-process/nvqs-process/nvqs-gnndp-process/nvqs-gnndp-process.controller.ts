import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSGNNDPProcessDto } from './dto/create-gnndp-dk-process.dto';
import { UpdateNVQSGNNDPProcessDto } from './dto/update-gnndp-dk-process.dto';
import { NVQSGNNDPProcessService } from './nvqs-gnndp-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_GNNDP_PROCESS)
export class NVQSGNNDPProcessController {
  constructor(private readonly NVQSGNNDPProcessService: NVQSGNNDPProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSGNNDPProcessDto) {
    return this.NVQSGNNDPProcessService.create(createCitizenProcessDto);
  }
  
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSGNNDPProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSGNNDPProcessDto) {
    return this.NVQSGNNDPProcessService.update(+id, updateCitizenProcessDto);
  }
}
