import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQSGNNDPProcess } from './entities/nvqs-gnndp-process.entity';
import { NVQSGNNDPProcessController } from './nvqs-gnndp-process.controller';
import { NVQSGNNDPProcessService } from './nvqs-gnndp-process.service';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQS_GNNDP } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSGNNDPProcess]), CitizenModule],
  controllers: [NVQSGNNDPProcessController],
  providers: [NVQSGNNDPProcessService],
  exports: [NVQSGNNDPProcessService],
})
export class NVQSGNNDPProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_GNNDP.processPath,
    module: NVQSGNNDPProcessModule,
    children: [],
  };
}
