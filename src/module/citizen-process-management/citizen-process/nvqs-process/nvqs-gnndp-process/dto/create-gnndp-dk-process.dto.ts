import { OmitType } from '@nestjs/swagger';
import { NVQSGNNDPProcess } from '../entities/nvqs-gnndp-process.entity';

export class CreateNVQSGNNDPProcessDto extends OmitType(NVQSGNNDPProcess, ['id']) {}
