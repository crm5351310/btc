import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSGNNDPProcessDto } from './create-gnndp-dk-process.dto';

export class UpdateNVQSGNNDPProcessDto extends OmitType(PartialType(CreateNVQSGNNDPProcessDto), [
  'citizen',
]) {}
