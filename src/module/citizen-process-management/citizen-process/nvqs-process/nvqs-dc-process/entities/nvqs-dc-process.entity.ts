import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { ChildEntity, ManyToOne } from 'typeorm';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { MilitaryUnit } from '../../../../../category/military-unit-root/military-unit/entities';
import { CitizenProcess } from '../../../entities/citizen-process.entity';

@ChildEntity()
export class NVQSDCProcess extends CitizenProcess {
  @ApiProperty({
    description: 'đơn vị quân đội',
    example: { id: 1 },
    type: () => MilitaryUnit,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryUnit)
  militaryUnit: MilitaryUnit;
}
