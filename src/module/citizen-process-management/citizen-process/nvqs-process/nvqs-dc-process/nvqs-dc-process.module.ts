import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSDCProcess } from './entities/nvqs-dc-process.entity';
import { NVQSDCProcessController } from './nvqs-dc-process.controller';
import { NVQSDCProcessService } from './nvqs-dc-process.service';
import { NVQS_DC } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSDCProcess]), CitizenModule],
  controllers: [NVQSDCProcessController],
  providers: [NVQSDCProcessService],
  exports: [NVQSDCProcessService],
})
export class NVQSDCProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DC.processPath,
    module: NVQSDCProcessModule,
    children: [],
  };
}
