import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSDCProcessDto } from './dto/create-nvqs-dc-process.dto';
import { UpdateNVQSDCProcessDto } from './dto/update-nvqs-dc-process.dto';
import { NVQSDCProcessService } from './nvqs-dc-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_DC_PROCESS)
export class NVQSDCProcessController {
  constructor(private readonly NVQSDCProcessService: NVQSDCProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSDCProcessDto) {
    return this.NVQSDCProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSDCProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSDCProcessDto) {
    return this.NVQSDCProcessService.update(+id, updateCitizenProcessDto);
  }
}
