import { OmitType } from '@nestjs/swagger';
import { NVQSDCProcess } from '../entities/nvqs-dc-process.entity';

export class CreateNVQSDCProcessDto extends OmitType(NVQSDCProcess, ['id']) {}
