import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSDCProcessDto } from './create-nvqs-dc-process.dto';

export class UpdateNVQSDCProcessDto extends OmitType(PartialType(CreateNVQSDCProcessDto), [
  'citizen',
]) {}
