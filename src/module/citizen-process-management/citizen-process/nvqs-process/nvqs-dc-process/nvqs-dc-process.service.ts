import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_DC } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSDCProcessDto } from './dto/create-nvqs-dc-process.dto';
import { UpdateNVQSDCProcessDto } from './dto/update-nvqs-dc-process.dto';
import { NVQSDCProcess } from './entities/nvqs-dc-process.entity';

@Injectable()
export class NVQSDCProcessService extends CitizenProcessServiceBase<NVQSDCProcess> {
  constructor(
    @InjectRepository(NVQSDCProcess)
    repository: Repository<NVQSDCProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSDCProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        militaryUnit: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSDCProcessDto,
  ): Promise<ResponseCreated<NVQSDCProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_DC);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSDCProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
