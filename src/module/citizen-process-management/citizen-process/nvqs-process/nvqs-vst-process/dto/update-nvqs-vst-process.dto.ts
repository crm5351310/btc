import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSVSTProcessDto } from './create-nvqs-vst-process.dto';

export class UpdateNVQSVSTProcessDto extends OmitType(PartialType(CreateNVQSVSTProcessDto), [
  'citizen',
]) {}
