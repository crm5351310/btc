import { OmitType } from '@nestjs/swagger';
import { NVQSVSTProcess } from '../entities/nvqs-vst-process.entity';

export class CreateNVQSVSTProcessDto extends OmitType(NVQSVSTProcess, ['id']) {}
