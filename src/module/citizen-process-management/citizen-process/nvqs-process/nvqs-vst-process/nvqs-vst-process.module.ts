import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQS_VST } from 'src/database/seeds/process.seed';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSVSTProcess } from './entities/nvqs-vst-process.entity';
import { NVQSVSTProcessController } from './nvqs-vst-process.controller';
import { NVQSVSTProcessService } from './nvqs-vst-process.service';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSVSTProcess]), CitizenModule],
  controllers: [NVQSVSTProcessController],
  providers: [NVQSVSTProcessService],
  exports: [NVQSVSTProcessService],
})
export class NVQSVSTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_VST.processPath,
    module: NVQSVSTProcessModule,
    children: [],
  };
}
