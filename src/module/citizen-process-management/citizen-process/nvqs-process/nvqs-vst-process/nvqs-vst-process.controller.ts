import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSVSTProcessDto } from './dto/create-nvqs-vst-process.dto';
import { UpdateNVQSVSTProcessDto } from './dto/update-nvqs-vst-process.dto';
import { NVQSVSTProcessService } from './nvqs-vst-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_VST_PROCESS)
export class NVQSVSTProcessController {
  constructor(private readonly NVQSVSTProcessService: NVQSVSTProcessService) {}

  
  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSVSTProcessDto) {
    return this.NVQSVSTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSVSTProcessService.findOne(+id);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSVSTProcessDto) {
    return this.NVQSVSTProcessService.update(+id, updateCitizenProcessDto);
  }
}
