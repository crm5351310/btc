import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSVSTProcessDto } from './dto/create-nvqs-vst-process.dto';
import { UpdateNVQSVSTProcessDto } from './dto/update-nvqs-vst-process.dto';
import { NVQSVSTProcess } from './entities/nvqs-vst-process.entity';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_VST } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NVQSVSTProcessService extends CitizenProcessServiceBase<NVQSVSTProcess> {
  constructor(
    @InjectRepository(NVQSVSTProcess)
    repository: Repository<NVQSVSTProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSVSTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSVSTProcessDto,
  ): Promise<ResponseCreated<NVQSVSTProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_VST);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSVSTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
