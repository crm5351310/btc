import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_TH } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSTHProcessDto } from './dto/create-nvqs-th-process.dto';
import { UpdateNVQSTHProcessDto } from './dto/update-nvqs-th-process.dto';
import { NVQSTHProcess } from './entities/nvqs-th-process.entity';

@Injectable()
export class NVQSTHProcessService extends CitizenProcessServiceBase<NVQSTHProcess> {
  constructor(
    @InjectRepository(NVQSTHProcess)
    repository: Repository<NVQSTHProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSTHProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSTHProcessDto,
  ): Promise<ResponseCreated<NVQSTHProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_TH);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSTHProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
