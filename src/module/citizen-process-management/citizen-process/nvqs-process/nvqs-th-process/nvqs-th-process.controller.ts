import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSTHProcessDto } from './dto/create-nvqs-th-process.dto';
import { UpdateNVQSTHProcessDto } from './dto/update-nvqs-th-process.dto';
import { NVQSTHProcessService } from './nvqs-th-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_TH_PROCESS)
export class NVQSTHProcessController {
  constructor(private readonly NVQSTHProcessService: NVQSTHProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSTHProcessDto) {
    return this.NVQSTHProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSTHProcessService.findOne(+id);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSTHProcessDto) {
    return this.NVQSTHProcessService.update(+id, updateCitizenProcessDto);
  }
}
