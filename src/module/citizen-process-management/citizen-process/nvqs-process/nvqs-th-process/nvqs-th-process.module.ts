import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSTHProcess } from './entities/nvqs-th-process.entity';
import { NVQSTHProcessController } from './nvqs-th-process.controller';
import { NVQSTHProcessService } from './nvqs-th-process.service';
import { NVQS_TH } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSTHProcess]), CitizenModule],
  controllers: [NVQSTHProcessController],
  providers: [NVQSTHProcessService],
  exports: [NVQSTHProcessService],
})
export class NVQSTHProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_TH.processPath,
    module: NVQSTHProcessModule,
    children: [],
  };
}
