import { OmitType } from '@nestjs/swagger';
import { NVQSTHProcess } from '../entities/nvqs-th-process.entity';

export class CreateNVQSTHProcessDto extends OmitType(NVQSTHProcess, ['id']) {}
