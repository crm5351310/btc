import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSTHProcessDto } from './create-nvqs-th-process.dto';

export class UpdateNVQSTHProcessDto extends OmitType(PartialType(CreateNVQSTHProcessDto), [
  'citizen',
]) {}
