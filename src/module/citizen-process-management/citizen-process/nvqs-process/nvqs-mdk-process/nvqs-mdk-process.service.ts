import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_MDK } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSMDKProcessDto } from './dto/create-nvqs-mdk-process.dto';
import { UpdateNVQSMDKProcessDto } from './dto/update-nvqs-mdk-process.dto';
import { NVQSMDKProcess } from './entities/nvqs-mdk-process.entity';

@Injectable()
export class NVQSMDKProcessService extends CitizenProcessServiceBase<NVQSMDKProcess> {
  constructor(
    @InjectRepository(NVQSMDKProcess)
    repository: Repository<NVQSMDKProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSMDKProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSMDKProcessDto,
  ): Promise<ResponseCreated<NVQSMDKProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_MDK);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSMDKProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
