import { OmitType } from '@nestjs/swagger';
import { NVQSMDKProcess } from '../entities/nvqs-mdk-process.entity';

export class CreateNVQSMDKProcessDto extends OmitType(NVQSMDKProcess, ['id']) {}
