import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSMDKProcessDto } from './create-nvqs-mdk-process.dto';

export class UpdateNVQSMDKProcessDto extends OmitType(PartialType(CreateNVQSMDKProcessDto), [
  'citizen',
]) {}
