import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSMDKProcessDto } from './dto/create-nvqs-mdk-process.dto';
import { UpdateNVQSMDKProcessDto } from './dto/update-nvqs-mdk-process.dto';
import { NVQSMDKProcessService } from './nvqs-mdk-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_MDK_PROCESS)
export class NVQSMDKProcessController {
  constructor(private readonly NVQSMDKProcessService: NVQSMDKProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSMDKProcessDto) {
    return this.NVQSMDKProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSMDKProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSMDKProcessDto) {
    return this.NVQSMDKProcessService.update(+id, updateCitizenProcessDto);
  }
}
