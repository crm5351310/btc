import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSMDKProcess } from './entities/nvqs-mdk-process.entity';
import { NVQSMDKProcessController } from './nvqs-mdk-process.controller';
import { NVQSMDKProcessService } from './nvqs-mdk-process.service';
import { NVQS_MDK } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSMDKProcess]), CitizenModule],
  controllers: [NVQSMDKProcessController],
  providers: [NVQSMDKProcessService],
  exports: [NVQSMDKProcessService],
})
export class NVQSMDKProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_MDK.processPath,
    module: NVQSMDKProcessModule,
    children: [],
  };
}
