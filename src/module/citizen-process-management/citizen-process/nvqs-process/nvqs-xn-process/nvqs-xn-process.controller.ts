import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSXNProcessDto } from './dto/create-nvqs-xn-process.dto';
import { UpdateNVQSXNProcessDto } from './dto/update-nvqs-xn-process.dto';
import { NVQSXNProcessService } from './nvqs-xn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_XN_PROCESS)
export class NVQSXNProcessController {
  constructor(private readonly NVQSXNProcessService: NVQSXNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSXNProcessDto) {
    return this.NVQSXNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSXNProcessService.findOne(+id);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSXNProcessDto) {
    return this.NVQSXNProcessService.update(+id, updateCitizenProcessDto);
  }
}
