import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_XN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSXNProcessDto } from './dto/create-nvqs-xn-process.dto';
import { UpdateNVQSXNProcessDto } from './dto/update-nvqs-xn-process.dto';
import { NVQSXNProcess } from './entities/nvqs-xn-process.entity';

@Injectable()
export class NVQSXNProcessService extends CitizenProcessServiceBase<NVQSXNProcess> {
  constructor(
    @InjectRepository(NVQSXNProcess)
    repository: Repository<NVQSXNProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSXNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        militaryJobPosition: true,
        militaryRank: true,
        militaryUnit: {
          administrativeUnit: true,
        },
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSXNProcessDto,
  ): Promise<ResponseCreated<NVQSXNProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_XN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSXNProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
