import { ChildEntity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities/citizen-process.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { MilitaryUnit } from '../../../../../../module/category/military-unit-root/military-unit/entities';
import { MilitaryRank } from '../../../../../../module/category/military-rank-root/military-rank/military-rank.entity';
import { MilitaryJobPosition } from '../../../../../../module/category/military-job-root/military-job-position/entities/military-job-position.entity';
import { JobTitle } from '../../../../../category/job-title/job-title.entity';

@ChildEntity()
export class NVQSXNProcess extends CitizenProcess {
  @ApiProperty({
    description: 'đơn vị quân đội',
    example: { id: 1 },
    type: () => MilitaryUnit,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryUnit)
  militaryUnit: MilitaryUnit;

  @ApiProperty({
    description: 'quân hàm',
    example: { id: 1 },
    type: () => MilitaryRank,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRank)
  militaryRank: MilitaryRank;

  @ApiProperty({
    description: 'vị trí chuyên môn (chuyên nghiệp quân sự)',
    example: { id: 1 },
    type: () => MilitaryJobPosition,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryJobPosition)
  militaryJobPosition: MilitaryJobPosition;

  @ApiProperty({
    description: 'Chức danh',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => JobTitle, { nullable: true })
  jobTitle?: JobTitle;
}
