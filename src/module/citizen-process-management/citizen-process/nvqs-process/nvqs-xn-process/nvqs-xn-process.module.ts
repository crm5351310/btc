import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSXNProcess } from './entities/nvqs-xn-process.entity';
import { NVQSXNProcessController } from './nvqs-xn-process.controller';
import { NVQSXNProcessService } from './nvqs-xn-process.service';
import { NVQS_XN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSXNProcess]), CitizenModule],
  controllers: [NVQSXNProcessController],
  providers: [NVQSXNProcessService],
  exports: [NVQSXNProcessService],
})
export class NVQSXNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_XN.processPath,
    module: NVQSXNProcessModule,
    children: [],
  };
}
