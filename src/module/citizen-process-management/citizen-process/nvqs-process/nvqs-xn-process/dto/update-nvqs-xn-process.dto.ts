import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSXNProcessDto } from './create-nvqs-xn-process.dto';

export class UpdateNVQSXNProcessDto extends OmitType(PartialType(CreateNVQSXNProcessDto), [
  'citizen',
]) {}
