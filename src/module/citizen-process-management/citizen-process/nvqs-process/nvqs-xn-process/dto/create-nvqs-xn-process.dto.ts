import { OmitType } from '@nestjs/swagger';
import { NVQSXNProcess } from '../entities/nvqs-xn-process.entity';

export class CreateNVQSXNProcessDto extends OmitType(NVQSXNProcess, ['id']) {}
