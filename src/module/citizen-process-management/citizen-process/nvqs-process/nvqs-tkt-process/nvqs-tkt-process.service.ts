import { Injectable } from '@nestjs/common';
import { CreateNvqsTktProcessDto } from './dto/create-nvqs-tkt-process.dto';
import { UpdateNvqsTktProcessDto } from './dto/update-nvqs-tkt-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NvqsTktProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { NVQS_TKT } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NvqsTktProcessService extends CitizenProcessServiceBase<NvqsTktProcess> {
  constructor(
    @InjectRepository(NvqsTktProcess)
    repository: Repository<NvqsTktProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NvqsTktProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }
  create(dto: CreateNvqsTktProcessDto): Promise<ResponseCreated<NvqsTktProcess>> {
    return super.create(dto, NVQS_TKT);
  }

  update(id: number, dto: UpdateNvqsTktProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
