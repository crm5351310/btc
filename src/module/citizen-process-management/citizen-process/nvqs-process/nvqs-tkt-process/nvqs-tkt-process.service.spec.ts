import { Test, TestingModule } from '@nestjs/testing';
import { NvqsTktProcessService } from './nvqs-tkt-process.service';

describe('NvqsTktProcessService', () => {
  let service: NvqsTktProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NvqsTktProcessService],
    }).compile();

    service = module.get<NvqsTktProcessService>(NvqsTktProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
