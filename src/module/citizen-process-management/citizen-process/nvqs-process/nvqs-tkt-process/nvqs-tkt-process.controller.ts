import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { NvqsTktProcessService } from './nvqs-tkt-process.service';
import { CreateNvqsTktProcessDto } from './dto/create-nvqs-tkt-process.dto';
import { UpdateNvqsTktProcessDto } from './dto/update-nvqs-tkt-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.NVQS_TKT_PROCESS)
export class NvqsTktProcessController {
  constructor(private readonly nvqsTktProcessService: NvqsTktProcessService) { }

  @Post()
  create(@Body() createNvqsTktProcessDto: CreateNvqsTktProcessDto) {
    return this.nvqsTktProcessService.create(createNvqsTktProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nvqsTktProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNvqsTktProcessDto: UpdateNvqsTktProcessDto) {
    return this.nvqsTktProcessService.update(+id, updateNvqsTktProcessDto);
  }
}
