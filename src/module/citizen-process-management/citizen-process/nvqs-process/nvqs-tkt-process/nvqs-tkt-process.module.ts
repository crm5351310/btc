import { Module } from '@nestjs/common';
import { NvqsTktProcessService } from './nvqs-tkt-process.service';
import { NvqsTktProcessController } from './nvqs-tkt-process.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NvqsTktProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { RouteTree } from '@nestjs/core';
import { NVQS_TKT } from '../../../../../database/seeds/process.seed';

@Module({
  imports: [
    TypeOrmModule.forFeature([NvqsTktProcess]),
    CitizenModule,
  ],
  controllers: [NvqsTktProcessController],
  providers: [NvqsTktProcessService],
})
export class NvqsTktProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_TKT.processPath,
    module: NvqsTktProcessModule,
  };
}
