import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNvqsTktProcessDto } from './create-nvqs-tkt-process.dto';

export class UpdateNvqsTktProcessDto extends PartialType(
  OmitType(CreateNvqsTktProcessDto, ['citizen'])
) { }
