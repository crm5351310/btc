import { OmitType } from "@nestjs/swagger";
import { NvqsTktProcess } from "../entities";

export class CreateNvqsTktProcessDto extends OmitType(NvqsTktProcess, ['id']) { }
