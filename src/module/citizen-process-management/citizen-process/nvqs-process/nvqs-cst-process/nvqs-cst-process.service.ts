import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSCSTProcessDto } from './dto/create-nvqs-cst-process.dto';
import { UpdateNVQSCSTProcessDto } from './dto/update-nvqs-cst-process.dto';
import { NVQSCSTProcess } from './entities/nvqs-cst-process.entity';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_CST } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NVQSCSTProcessService extends CitizenProcessServiceBase<NVQSCSTProcess> {
  constructor(
    @InjectRepository(NVQSCSTProcess)
    repository: Repository<NVQSCSTProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSCSTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSCSTProcessDto,
  ): Promise<ResponseCreated<NVQSCSTProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_CST);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSCSTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
