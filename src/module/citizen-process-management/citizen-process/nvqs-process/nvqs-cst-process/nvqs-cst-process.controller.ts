import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSCSTProcessDto } from './dto/create-nvqs-cst-process.dto';
import { UpdateNVQSCSTProcessDto } from './dto/update-nvqs-cst-process.dto';
import { NVQSCSTProcessService } from './nvqs-cst-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_CST_PROCESS)
export class NVQSCSTProcessController {
  constructor(private readonly NVQSCSTProcessService: NVQSCSTProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSCSTProcessDto) {
    return this.NVQSCSTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSCSTProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSCSTProcessDto) {
    return this.NVQSCSTProcessService.update(+id, updateCitizenProcessDto);
  }
}
