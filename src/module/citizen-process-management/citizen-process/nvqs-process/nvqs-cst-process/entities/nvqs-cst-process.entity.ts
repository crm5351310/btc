import { ChildEntity } from 'typeorm';
import { CitizenProcess } from '../../../entities/citizen-process.entity';

@ChildEntity()
export class NVQSCSTProcess extends CitizenProcess {}
