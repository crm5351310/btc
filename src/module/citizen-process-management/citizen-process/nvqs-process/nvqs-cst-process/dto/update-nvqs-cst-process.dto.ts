import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSCSTProcessDto } from './create-nvqs-cst-process.dto';

export class UpdateNVQSCSTProcessDto extends OmitType(PartialType(CreateNVQSCSTProcessDto), [
  'citizen',
]) {}
