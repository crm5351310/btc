import { OmitType } from '@nestjs/swagger';
import { NVQSCSTProcess } from '../entities/nvqs-cst-process.entity';

export class CreateNVQSCSTProcessDto extends OmitType(NVQSCSTProcess, ['id']) {}
