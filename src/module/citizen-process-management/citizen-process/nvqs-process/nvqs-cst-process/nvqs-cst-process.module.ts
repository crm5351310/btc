import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQS_CST } from 'src/database/seeds/process.seed';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSCSTProcess } from './entities/nvqs-cst-process.entity';
import { NVQSCSTProcessController } from './nvqs-cst-process.controller';
import { NVQSCSTProcessService } from './nvqs-cst-process.service';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSCSTProcess]), CitizenModule],
  controllers: [NVQSCSTProcessController],
  providers: [NVQSCSTProcessService],
  exports: [NVQSCSTProcessService],
})
export class NVQSCSTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_CST.processPath,
    module: NVQSCSTProcessModule,
    children: [],
  };
}
