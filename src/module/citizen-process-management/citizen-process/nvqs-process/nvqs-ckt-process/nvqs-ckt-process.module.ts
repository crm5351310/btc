import { Module } from '@nestjs/common';
import { NvqsCktProcessService } from './nvqs-ckt-process.service';
import { NvqsCktProcessController } from './nvqs-ckt-process.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NvqsCktProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { RouteTree } from '@nestjs/core';
import { NVQS_CKT } from '../../../../../database/seeds/process.seed';

@Module({
  imports: [
    TypeOrmModule.forFeature([NvqsCktProcess]),
    CitizenModule,
  ],
  controllers: [NvqsCktProcessController],
  providers: [NvqsCktProcessService],
})
export class NvqsCktProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_CKT.processPath,
    module: NvqsCktProcessModule,
  };
}
