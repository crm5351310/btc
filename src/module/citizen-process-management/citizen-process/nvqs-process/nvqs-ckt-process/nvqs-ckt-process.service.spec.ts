import { Test, TestingModule } from '@nestjs/testing';
import { NvqsCktProcessService } from './nvqs-ckt-process.service';

describe('NvqsCktProcessService', () => {
  let service: NvqsCktProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NvqsCktProcessService],
    }).compile();

    service = module.get<NvqsCktProcessService>(NvqsCktProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
