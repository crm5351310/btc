import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNvqsCktProcessDto } from './create-nvqs-ckt-process.dto';

export class UpdateNvqsCktProcessDto extends PartialType(
  OmitType(CreateNvqsCktProcessDto, ['citizen'])
) { }
