import { OmitType } from "@nestjs/swagger";
import { NvqsCktProcess } from "../entities";

export class CreateNvqsCktProcessDto extends OmitType(NvqsCktProcess, ['id']) { }
