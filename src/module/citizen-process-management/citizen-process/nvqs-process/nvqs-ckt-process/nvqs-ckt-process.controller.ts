import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { NvqsCktProcessService } from './nvqs-ckt-process.service';
import { CreateNvqsCktProcessDto } from './dto/create-nvqs-ckt-process.dto';
import { UpdateNvqsCktProcessDto } from './dto/update-nvqs-ckt-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.NVQS_CKT_PROCESS)
export class NvqsCktProcessController {
  constructor(private readonly nvqsCktProcessService: NvqsCktProcessService) {}

  @Post()
  create(@Body() createNvqsCktProcessDto: CreateNvqsCktProcessDto) {
    return this.nvqsCktProcessService.create(createNvqsCktProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nvqsCktProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNvqsCktProcessDto: UpdateNvqsCktProcessDto) {
    return this.nvqsCktProcessService.update(+id, updateNvqsCktProcessDto);
  }
}
