import { Injectable } from '@nestjs/common';
import { CreateNvqsCktProcessDto } from './dto/create-nvqs-ckt-process.dto';
import { UpdateNvqsCktProcessDto } from './dto/update-nvqs-ckt-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NvqsCktProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { NVQS_CKT } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NvqsCktProcessService extends CitizenProcessServiceBase<NvqsCktProcess> {
  constructor(
    @InjectRepository(NvqsCktProcess)
    repository: Repository<NvqsCktProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NvqsCktProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateNvqsCktProcessDto): Promise<ResponseCreated<NvqsCktProcess>> {
    return super.create(dto, NVQS_CKT);
  }

  update(id: number, dto: UpdateNvqsCktProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
