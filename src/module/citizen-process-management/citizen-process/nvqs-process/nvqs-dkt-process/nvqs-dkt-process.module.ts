import { Module } from '@nestjs/common';
import { NvqsDktProcessService } from './nvqs-dkt-process.service';
import { NvqsDktProcessController } from './nvqs-dkt-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NvqsDktProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { NVQS_DKT } from '../../../../../database/seeds/process.seed';

@Module({
  imports: [
    TypeOrmModule.forFeature([NvqsDktProcess]),
    CitizenModule,
  ],
  controllers: [NvqsDktProcessController],
  providers: [NvqsDktProcessService],
})
export class NvqsDktProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DKT.processPath,
    module: NvqsDktProcessModule,
  };
}
