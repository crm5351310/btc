import { Test, TestingModule } from '@nestjs/testing';
import { NvqsDktProcessService } from './nvqs-dkt-process.service';

describe('NvqsDktProcessService', () => {
  let service: NvqsDktProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NvqsDktProcessService],
    }).compile();

    service = module.get<NvqsDktProcessService>(NvqsDktProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
