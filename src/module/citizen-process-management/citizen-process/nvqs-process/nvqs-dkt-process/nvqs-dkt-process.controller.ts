import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { NvqsDktProcessService } from './nvqs-dkt-process.service';
import { CreateNvqsDktProcessDto } from './dto/create-nvqs-dkt-process.dto';
import { UpdateNvqsDktProcessDto } from './dto/update-nvqs-dkt-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.NVQS_DKT_PROCESS)
export class NvqsDktProcessController {
  constructor(private readonly nvqsDktProcessService: NvqsDktProcessService) {}

  @Post()
  create(@Body() createNvqsDktProcessDto: CreateNvqsDktProcessDto) {
    return this.nvqsDktProcessService.create(createNvqsDktProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nvqsDktProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNvqsDktProcessDto: UpdateNvqsDktProcessDto) {
    return this.nvqsDktProcessService.update(+id, updateNvqsDktProcessDto);
  }
}
