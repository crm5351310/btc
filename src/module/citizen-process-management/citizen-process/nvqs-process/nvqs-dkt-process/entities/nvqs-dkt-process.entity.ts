import { ChildEntity, Column, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities';
import { IsBoolean, IsOptional, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Health } from '../../../../../category';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { HivExamination } from '../../../../category/hiv-examination/entities/hiv-examination.entity';
import { DrugExamination } from '../../../../category/drug-examination/entities/drug-examination.entity';

@ChildEntity()
export class NvqsDktProcess extends CitizenProcess {
  @ApiProperty({
    description: 'Điểm khám thể lực',
    example: 2,
  })
  @Max(6)
  @Min(1)
  @Column('integer')
  physicalPowerPoint: number;

  @ApiProperty({
    description: 'Điểm khám lâm sàng và cận lâm sàng',
    example: 2,
  })
  @Max(6)
  @Min(1)
  @Column('integer')
  clinicalExaminationPoint: number;

  @ApiProperty({
    description: 'Kết quả sàng lọc HIV',
    example: { id: 1 },
  })
  @ManyToOne(() => HivExamination)
  hivExamination: HivExamination;

  @ApiProperty({
    description: 'Kết quả sàng lọc ma túy',
    example: { id: 1 },
  })
  @ManyToOne(() => DrugExamination)
  drugExamination: DrugExamination;

  @ApiProperty({
    description: 'Số mũi tiêm covid',
    example: 2,
    required: false,
  })
  @IsOptional()
  @Max(10)
  @Min(0)
  @Column('integer', { nullable: true })
  vaccineCovid?: number;

  @ApiProperty({
    description: 'Có hình xăm / chữ xăm?',
    example: false,
  })
  @IsBoolean()
  @Column('boolean')
  hasTatto: boolean;

  @ApiProperty({
    description: 'Phân loại sức khỏe',
    example: { id: 1 },
  })
  @IsObjectRelation()
  @ManyToOne(() => Health, { onDelete: 'RESTRICT' })
  health: Health;
}
