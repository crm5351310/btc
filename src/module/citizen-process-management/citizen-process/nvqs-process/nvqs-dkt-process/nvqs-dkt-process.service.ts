import { Injectable } from '@nestjs/common';
import { CreateNvqsDktProcessDto } from './dto/create-nvqs-dkt-process.dto';
import { UpdateNvqsDktProcessDto } from './dto/update-nvqs-dkt-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NvqsDktProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { NVQS_DKT } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NvqsDktProcessService extends CitizenProcessServiceBase<NvqsDktProcess> {
  constructor(
    @InjectRepository(NvqsDktProcess)
    repository: Repository<NvqsDktProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NvqsDktProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        health: true,
      },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateNvqsDktProcessDto): Promise<ResponseCreated<NvqsDktProcess>> {
    return super.create(dto, NVQS_DKT);
  }

  update(id: number, dto: UpdateNvqsDktProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
