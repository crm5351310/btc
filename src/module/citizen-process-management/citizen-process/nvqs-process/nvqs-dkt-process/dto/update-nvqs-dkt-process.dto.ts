import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNvqsDktProcessDto } from './create-nvqs-dkt-process.dto';

export class UpdateNvqsDktProcessDto extends PartialType(
  OmitType(CreateNvqsDktProcessDto, ['citizen'])
) { }
