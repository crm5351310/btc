import { OmitType } from "@nestjs/swagger";
import { NvqsDktProcess } from "../entities";

export class CreateNvqsDktProcessDto extends OmitType(NvqsDktProcess, ['id']) { }
