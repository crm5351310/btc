import { OmitType } from '@nestjs/swagger';
import { NVQSTSTProcess } from '../entities/nvqs-tst-process.entity';

export class CreateNVQSTSTProcessDto extends OmitType(NVQSTSTProcess, ['id']) {}
