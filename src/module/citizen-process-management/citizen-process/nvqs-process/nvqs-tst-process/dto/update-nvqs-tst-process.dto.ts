import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSTSTProcessDto } from './create-nvqs-tst-process.dto';

export class UpdateNVQSTSTProcessDto extends OmitType(PartialType(CreateNVQSTSTProcessDto), [
  'citizen',
]) {}
