import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSTSTProcessDto } from './dto/create-nvqs-tst-process.dto';
import { UpdateNVQSTSTProcessDto } from './dto/update-nvqs-tst-process.dto';
import { NVQSTSTProcessService } from './nvqs-tst-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_TST_PROCESS)
export class NVQSTSTProcessController {
  constructor(private readonly NVQSTSTProcessService: NVQSTSTProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSTSTProcessDto) {
    return this.NVQSTSTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSTSTProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSTSTProcessDto) {
    return this.NVQSTSTProcessService.update(+id, updateCitizenProcessDto);
  }
}
