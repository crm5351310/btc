import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQS_TST } from 'src/database/seeds/process.seed';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSTSTProcess } from './entities/nvqs-tst-process.entity';
import { NVQSTSTProcessController } from './nvqs-tst-process.controller';
import { NVQSTSTProcessService } from './nvqs-tst-process.service';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSTSTProcess]), CitizenModule],
  controllers: [NVQSTSTProcessController],
  providers: [NVQSTSTProcessService],
  exports: [NVQSTSTProcessService],
})
export class NVQSTSTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_TST.processPath,
    module: NVQSTSTProcessModule,
    children: [],
  };
}
