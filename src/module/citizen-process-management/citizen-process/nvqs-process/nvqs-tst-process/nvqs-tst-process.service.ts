import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSTSTProcessDto } from './dto/create-nvqs-tst-process.dto';
import { UpdateNVQSTSTProcessDto } from './dto/update-nvqs-tst-process.dto';
import { NVQSTSTProcess } from './entities/nvqs-tst-process.entity';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_TST } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NVQSTSTProcessService extends CitizenProcessServiceBase<NVQSTSTProcess> {
  constructor(
    @InjectRepository(NVQSTSTProcess)
    repository: Repository<NVQSTSTProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSTSTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSTSTProcessDto,
  ): Promise<ResponseCreated<NVQSTSTProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_TST);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSTSTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
