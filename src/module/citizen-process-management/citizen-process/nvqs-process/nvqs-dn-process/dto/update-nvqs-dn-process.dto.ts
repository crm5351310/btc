import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSDNProcessDto } from './create-nvqs-dn-process.dto';

export class UpdateNVQSDNProcessDto extends OmitType(PartialType(CreateNVQSDNProcessDto), [
  'citizen',
]) {}
