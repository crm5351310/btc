import { OmitType } from '@nestjs/swagger';
import { NVQSDNProcess } from '../entities/nvqs-dn-process.entity';

export class CreateNVQSDNProcessDto extends OmitType(NVQSDNProcess, ['id']) {}
