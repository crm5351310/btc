import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_DN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSDNProcessDto } from './dto/create-nvqs-dn-process.dto';
import { UpdateNVQSDNProcessDto } from './dto/update-nvqs-dn-process.dto';
import { NVQSDNProcess } from './entities/nvqs-dn-process.entity';

@Injectable()
export class NVQSDNProcessService extends CitizenProcessServiceBase<NVQSDNProcess> {
  constructor(
    @InjectRepository(NVQSDNProcess)
    repository: Repository<NVQSDNProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSDNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSDNProcessDto,
  ): Promise<ResponseCreated<NVQSDNProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_DN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSDNProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
