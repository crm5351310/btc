import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSDNProcess } from './entities/nvqs-dn-process.entity';
import { NVQSDNProcessController } from './nvqs-dn-process.controller';
import { NVQSDNProcessService } from './nvqs-dn-process.service';
import { NVQS_DN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSDNProcess]), CitizenModule],
  controllers: [NVQSDNProcessController],
  providers: [NVQSDNProcessService],
  exports: [NVQSDNProcessService],
})
export class NVQSDNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DN.processPath,
    module: NVQSDNProcessModule,
    children: [],
  };
}
