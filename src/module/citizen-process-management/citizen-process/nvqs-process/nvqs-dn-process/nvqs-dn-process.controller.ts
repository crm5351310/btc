import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSDNProcessDto } from './dto/create-nvqs-dn-process.dto';
import { UpdateNVQSDNProcessDto } from './dto/update-nvqs-dn-process.dto';
import { NVQSDNProcessService } from './nvqs-dn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_DN_PROCESS)
export class NVQSDNProcessController {
  constructor(private readonly NVQSDNProcessService: NVQSDNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSDNProcessDto) {
    return this.NVQSDNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSDNProcessService.findOne(+id);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSDNProcessDto) {
    return this.NVQSDNProcessService.update(+id, updateCitizenProcessDto);
  }
}
