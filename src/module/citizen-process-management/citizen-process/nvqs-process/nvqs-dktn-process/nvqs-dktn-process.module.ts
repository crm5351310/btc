import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQSDKTNProcess } from './entities/nvqs-dktn-process.entity';
import { NVQSDKTNProcessController } from './nvqs-dktn-process.controller';
import { NVQSDKTNProcessService } from './nvqs-dktn-process.service';
import { CitizenModule } from '../../../../../module/citizen-management/citizen/citizen.module';
import { MilitaryRecruitmentTypeModule } from '../../../../../module/citizen-process-management/category/military-recruitment-type/military-recruitment-type.module';
import { NVQS_DKTN } from '../../../../../database/seeds/process.seed';
import { MilitaryRecruitmentPeriodModule } from '../../../../../module/militaty-recruitment/military-recruit-period/military-recruit-period.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([NVQSDKTNProcess]),
    CitizenModule,
    MilitaryRecruitmentTypeModule,
    MilitaryRecruitmentPeriodModule,
  ],
  controllers: [NVQSDKTNProcessController],
  providers: [NVQSDKTNProcessService],
  exports: [NVQSDKTNProcessService],
})
export class NVQSDKTNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DKTN.processPath,
    module: NVQSDKTNProcessModule,
    children: [],
  };
}
