import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NVQS_DKTN } from 'src/database/seeds/process.seed';
import { MilitaryRecruitmentTypeService } from 'src/module/citizen-process-management/category/military-recruitment-type/military-recruitment-type.service';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSDKTNProcessDto } from './dto/create-nvqs-dktn-process.dto';
import { UpdateNVQSDKTNProcessDto } from './dto/update-nvqs-dktn-process.dto';
import { NVQSDKTNProcess } from './entities/nvqs-dktn-process.entity';
import { ResponseFindOne } from '../../../../../common/response';
import { MilitaryRecruitmentPeriodService } from 'src/module/militaty-recruitment/military-recruit-period/military-recruit-period.service';

@Injectable()
export class NVQSDKTNProcessService extends CitizenProcessServiceBase<NVQSDKTNProcess> {
  constructor(
    @InjectRepository(NVQSDKTNProcess)
    repository: Repository<NVQSDKTNProcess>,
    citizenService: CitizenService,
    private readonly militaryRecruitmentTypeService: MilitaryRecruitmentTypeService,
    private readonly militaryRecruitmentPeriodService: MilitaryRecruitmentPeriodService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSDKTNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        militaryRecruitmentType: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(createCitizenProcessDto: CreateNVQSDKTNProcessDto) {
    // check tồn tại loại nghĩa vụ (quân đội hay công an)
    if (createCitizenProcessDto.militaryRecruitmentType)
      await this.militaryRecruitmentTypeService.findOne(
        createCitizenProcessDto.militaryRecruitmentType,
      );

    // check tồn tại đợt tuyển quân
    await this.militaryRecruitmentPeriodService.findOne(
      createCitizenProcessDto.militaryRecruitmentPeriod.id,
    );
    return await super.create(createCitizenProcessDto, NVQS_DKTN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSDKTNProcessDto) {
    // check tồn tại loại nghĩa vụ (quân đội hay công an)
    if (updateCitizenProcessDto.militaryRecruitmentType)
      await this.militaryRecruitmentTypeService.findOne(
        updateCitizenProcessDto.militaryRecruitmentType,
      );

    // check tồn tại đợt tuyển quân
    if (updateCitizenProcessDto.militaryRecruitmentPeriod)
      await this.militaryRecruitmentPeriodService.findOne(
        updateCitizenProcessDto.militaryRecruitmentPeriod.id,
      );

    return await super.update(id, updateCitizenProcessDto);
  }
}
