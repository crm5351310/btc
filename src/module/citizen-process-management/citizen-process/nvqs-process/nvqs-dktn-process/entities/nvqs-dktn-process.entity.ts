import { ChildEntity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities/citizen-process.entity';
import { MilitaryRecruitmentType } from '../../../../category/military-recruitment-type/entities/military-recruitment-type.entity';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { MilitaryRecruitmentPeriod } from '../../../../../../module/militaty-recruitment/military-recruit-period/entities/military-recruit-period.entity';

@ChildEntity()
export class NVQSDKTNProcess extends CitizenProcess {
  @ApiProperty({
    description: 'loại nghĩa vụ',
    example: { id: 1 },
    type: () => MilitaryRecruitmentType,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRecruitmentType, (type) => type.NVQSDKTNProcesses)
  militaryRecruitmentType?: MilitaryRecruitmentType;

  @ApiProperty({
    description: 'đợt tuyển quân',
    example: { id: 1 },
    type: () => MilitaryRecruitmentPeriod,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRecruitmentPeriod)
  militaryRecruitmentPeriod: MilitaryRecruitmentPeriod;
}
