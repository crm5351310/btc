import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSDKTNProcessDto } from './create-nvqs-dktn-process.dto';

export class UpdateNVQSDKTNProcessDto extends OmitType(PartialType(CreateNVQSDKTNProcessDto), [
  'citizen',
]) {}
