import { OmitType } from '@nestjs/swagger';
import { NVQSDKTNProcess } from '../entities/nvqs-dktn-process.entity';

export class CreateNVQSDKTNProcessDto extends OmitType(NVQSDKTNProcess, ['id']) {}
