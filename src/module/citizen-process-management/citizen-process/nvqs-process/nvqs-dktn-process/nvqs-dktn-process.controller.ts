import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from '../../../../../common/base/class/base.class';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSDKTNProcessDto } from './dto/create-nvqs-dktn-process.dto';
import { UpdateNVQSDKTNProcessDto } from './dto/update-nvqs-dktn-process.dto';
import { NVQSDKTNProcessService } from './nvqs-dktn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_DKTN_PROCESS)
export class NVQSDKTNProcessController {
  constructor(private readonly NVQSDKTNProcessService: NVQSDKTNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSDKTNProcessDto) {
    return this.NVQSDKTNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSDKTNProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateCitizenProcessDto: UpdateNVQSDKTNProcessDto) {
    return this.NVQSDKTNProcessService.update(path.id, updateCitizenProcessDto);
  }
}
