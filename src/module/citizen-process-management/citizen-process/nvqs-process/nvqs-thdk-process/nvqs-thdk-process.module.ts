import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQSTHDKProcess } from './entities/nvqs-thdk-process.entity';
import { NVQSTHDKProcessController } from './nvqs-thdk-process.controller';
import { NVQSTHDKProcessService } from './nvqs-thdk-process.service';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQS_THDK } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSTHDKProcess]), CitizenModule],
  controllers: [NVQSTHDKProcessController],
  providers: [NVQSTHDKProcessService],
  exports: [NVQSTHDKProcessService],
})
export class NVQSTHDKProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_THDK.processPath,
    module: NVQSTHDKProcessModule,
    children: [],
  };
}
