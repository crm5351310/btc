import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NVQS_THDK } from 'src/database/seeds/process.seed';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSTHDKProcessDto } from './dto/create-nvqs-thdk-process.dto';
import { UpdateNVQSTHDKProcessDto } from './dto/update-nvqs-thdk-process.dto';
import { NVQSTHDKProcess } from './entities/nvqs-thdk-process.entity';
import { ResponseFindOne } from '../../../../../common/response';

@Injectable()
export class NVQSTHDKProcessService extends CitizenProcessServiceBase<NVQSTHDKProcess> {
  constructor(
    @InjectRepository(NVQSTHDKProcess)
    repository: Repository<NVQSTHDKProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSTHDKProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(createCitizenProcessDto: CreateNVQSTHDKProcessDto) {
    return await super.create(createCitizenProcessDto, NVQS_THDK);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSTHDKProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
