import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PathDto } from '../../../../../common/base/class/base.class';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSTHDKProcessDto } from './dto/create-nvqs-thdk-process.dto';
import { UpdateNVQSTHDKProcessDto } from './dto/update-nvqs-thdk-process.dto';
import { NVQSTHDKProcessService } from './nvqs-thdk-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_THDK_PROCESS)
export class NVQSTHDKProcessController {
  constructor(private readonly NVQSTHDKProcessService: NVQSTHDKProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSTHDKProcessDto) {
    return this.NVQSTHDKProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSTHDKProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateCitizenProcessDto: UpdateNVQSTHDKProcessDto) {
    return this.NVQSTHDKProcessService.update(path.id, updateCitizenProcessDto);
  }
}
