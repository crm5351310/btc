import { OmitType } from '@nestjs/swagger';
import { NVQSTHDKProcess } from '../entities/nvqs-thdk-process.entity';

export class CreateNVQSTHDKProcessDto extends OmitType(NVQSTHDKProcess, ['id']) {}
