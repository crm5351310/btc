import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSTHDKProcessDto } from './create-nvqs-thdk-process.dto';

export class UpdateNVQSTHDKProcessDto extends OmitType(PartialType(CreateNVQSTHDKProcessDto), [
  'citizen',
]) {}
