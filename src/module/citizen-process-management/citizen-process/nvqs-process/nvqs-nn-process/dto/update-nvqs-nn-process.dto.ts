import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSNNProcessDto } from './create-nvqs-nn-process.dto';

export class UpdateNVQSNNProcessDto extends OmitType(PartialType(CreateNVQSNNProcessDto), [
  'citizen',
]) {}
