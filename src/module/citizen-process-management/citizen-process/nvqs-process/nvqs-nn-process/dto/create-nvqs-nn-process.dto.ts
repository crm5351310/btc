import { OmitType } from '@nestjs/swagger';
import { NVQSNNProcess } from '../entities/nvqs-nn-process.entity';

export class CreateNVQSNNProcessDto extends OmitType(NVQSNNProcess, ['id']) {}
