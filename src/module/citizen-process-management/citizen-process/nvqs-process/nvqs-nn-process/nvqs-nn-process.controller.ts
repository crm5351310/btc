import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSNNProcessDto } from './dto/create-nvqs-nn-process.dto';
import { UpdateNVQSNNProcessDto } from './dto/update-nvqs-nn-process.dto';
import { NVQSNNProcessService } from './nvqs-nn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_NN_PROCESS)
export class NVQSNNProcessController {
  constructor(private readonly NVQSNNProcessService: NVQSNNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSNNProcessDto) {
    return this.NVQSNNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSNNProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSNNProcessDto) {
    return this.NVQSNNProcessService.update(+id, updateCitizenProcessDto);
  }
}
