import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_NN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSNNProcessDto } from './dto/create-nvqs-nn-process.dto';
import { UpdateNVQSNNProcessDto } from './dto/update-nvqs-nn-process.dto';
import { NVQSNNProcess } from './entities/nvqs-nn-process.entity';
import { MilitaryUnitService } from 'src/module/category/military-unit-root/military-unit/military-unit.service';

@Injectable()
export class NVQSNNProcessService extends CitizenProcessServiceBase<NVQSNNProcess> {
  constructor(
    @InjectRepository(NVQSNNProcess)
    repository: Repository<NVQSNNProcess>,
    citizenService: CitizenService,
    readonly militaryService: MilitaryUnitService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSNNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        militaryUnit: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSNNProcessDto,
  ): Promise<ResponseCreated<NVQSNNProcess>> {
    // check tồn tại đơn vị quân đội
    await this.militaryService.findOne(createCitizenProcessDto.militaryUnit.id);

    return await super.create(createCitizenProcessDto, NVQS_NN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSNNProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
