import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { MilitaryUnit } from '../../../../../category/military-unit-root/military-unit/entities';
import { ChildEntity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities/citizen-process.entity';

@ChildEntity()
export class NVQSNNProcess extends CitizenProcess {
  @ApiProperty({
    description: 'đơn vị quân đội',
    example: { id: 1 },
    type: () => MilitaryUnit,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryUnit)
  militaryUnit: MilitaryUnit;
}
