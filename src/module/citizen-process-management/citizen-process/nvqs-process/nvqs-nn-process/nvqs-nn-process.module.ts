import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSNNProcess } from './entities/nvqs-nn-process.entity';
import { NVQSNNProcessController } from './nvqs-nn-process.controller';
import { NVQSNNProcessService } from './nvqs-nn-process.service';
import { MilitaryUnitModule } from '../../../../category/military-unit-root/military-unit/military-unit.module';
import { NVQS_NN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSNNProcess]), CitizenModule, MilitaryUnitModule],
  controllers: [NVQSNNProcessController],
  providers: [NVQSNNProcessService],
  exports: [NVQSNNProcessService],
})
export class NVQSNNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_NN.processPath,
    module: NVQSNNProcessModule,
    children: [],
  };
}
