import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNvqsVktProcessDto } from './create-nvqs-vkt-process.dto';

export class UpdateNvqsVktProcessDto extends PartialType(
  OmitType(CreateNvqsVktProcessDto, ['citizen'])
) { }
