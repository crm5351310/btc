import { OmitType } from "@nestjs/swagger";
import { NvqsVktProcess } from "../entities";

export class CreateNvqsVktProcessDto extends OmitType(NvqsVktProcess, ['id']) { }
