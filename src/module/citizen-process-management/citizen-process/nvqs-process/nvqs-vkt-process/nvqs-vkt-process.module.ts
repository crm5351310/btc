import { Module } from '@nestjs/common';
import { NvqsVktProcessService } from './nvqs-vkt-process.service';
import { NvqsVktProcessController } from './nvqs-vkt-process.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NvqsVktProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { RouteTree } from '@nestjs/core';
import { NVQS_VKT } from '../../../../../database/seeds/process.seed';

@Module({
  imports: [
    TypeOrmModule.forFeature([NvqsVktProcess]),
    CitizenModule,
  ],
  controllers: [NvqsVktProcessController],
  providers: [NvqsVktProcessService],
})
export class NvqsVktProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_VKT.processPath,
    module: NvqsVktProcessModule,
  };
}
