import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { NvqsVktProcessService } from './nvqs-vkt-process.service';
import { CreateNvqsVktProcessDto } from './dto/create-nvqs-vkt-process.dto';
import { UpdateNvqsVktProcessDto } from './dto/update-nvqs-vkt-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.NVQS_VKT_PROCESS)
export class NvqsVktProcessController {
  constructor(private readonly nvqsVktProcessService: NvqsVktProcessService) { }

  @Post()
  create(@Body() createNvqsVktProcessDto: CreateNvqsVktProcessDto) {
    return this.nvqsVktProcessService.create(createNvqsVktProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nvqsVktProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNvqsVktProcessDto: UpdateNvqsVktProcessDto) {
    return this.nvqsVktProcessService.update(+id, updateNvqsVktProcessDto);
  }
}
