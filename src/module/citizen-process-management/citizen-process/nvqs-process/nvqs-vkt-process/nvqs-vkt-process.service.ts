import { Injectable } from '@nestjs/common';
import { CreateNvqsVktProcessDto } from './dto/create-nvqs-vkt-process.dto';
import { UpdateNvqsVktProcessDto } from './dto/update-nvqs-vkt-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NvqsVktProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { NVQS_VKT } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NvqsVktProcessService extends CitizenProcessServiceBase<NvqsVktProcess> {
  constructor(
    @InjectRepository(NvqsVktProcess)
    repository: Repository<NvqsVktProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NvqsVktProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateNvqsVktProcessDto): Promise<ResponseCreated<NvqsVktProcess>> {
    return super.create(dto, NVQS_VKT);
  }

  update(id: number, dto: UpdateNvqsVktProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
