import { Test, TestingModule } from '@nestjs/testing';
import { NvqsVktProcessService } from './nvqs-vkt-process.service';

describe('NvqsVktProcessService', () => {
  let service: NvqsVktProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NvqsVktProcessService],
    }).compile();

    service = module.get<NvqsVktProcessService>(NvqsVktProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
