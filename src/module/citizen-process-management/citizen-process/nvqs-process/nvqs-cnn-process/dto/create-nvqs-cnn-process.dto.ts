import { OmitType } from '@nestjs/swagger';
import { NVQSCNNProcess } from '../entities/nvqs-cnn-process.entity';

export class CreateNVQSCNNProcessDto extends OmitType(NVQSCNNProcess, ['id']) {}
