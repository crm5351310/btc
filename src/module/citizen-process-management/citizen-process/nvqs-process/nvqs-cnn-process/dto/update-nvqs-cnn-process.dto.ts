import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSCNNProcessDto } from './create-nvqs-cnn-process.dto';

export class UpdateNVQSCNNProcessDto extends OmitType(PartialType(CreateNVQSCNNProcessDto), [
  'citizen',
]) {}
