import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSCNNProcessDto } from './dto/create-nvqs-cnn-process.dto';
import { UpdateNVQSCNNProcessDto } from './dto/update-nvqs-cnn-process.dto';
import { NVQSCNNProcessService } from './nvqs-cnn-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_CNN_PROCESS)
export class NVQSCNNProcessController {
  constructor(private readonly NVQSCNNProcessService: NVQSCNNProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSCNNProcessDto) {
    return this.NVQSCNNProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSCNNProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSCNNProcessDto) {
    return this.NVQSCNNProcessService.update(+id, updateCitizenProcessDto);
  }
}
