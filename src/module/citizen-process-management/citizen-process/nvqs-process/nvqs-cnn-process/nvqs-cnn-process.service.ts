import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_CNN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSCNNProcessDto } from './dto/create-nvqs-cnn-process.dto';
import { UpdateNVQSCNNProcessDto } from './dto/update-nvqs-cnn-process.dto';
import { NVQSCNNProcess } from './entities/nvqs-cnn-process.entity';

@Injectable()
export class NVQSCNNProcessService extends CitizenProcessServiceBase<NVQSCNNProcess> {
  constructor(
    @InjectRepository(NVQSCNNProcess)
    repository: Repository<NVQSCNNProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSCNNProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSCNNProcessDto,
  ): Promise<ResponseCreated<NVQSCNNProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_CNN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSCNNProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
