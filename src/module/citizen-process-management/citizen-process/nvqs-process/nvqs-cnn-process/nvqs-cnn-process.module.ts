import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSCNNProcess } from './entities/nvqs-cnn-process.entity';
import { NVQSCNNProcessController } from './nvqs-cnn-process.controller';
import { NVQSCNNProcessService } from './nvqs-cnn-process.service';
import { NVQS_CNN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSCNNProcess]), CitizenModule],
  controllers: [NVQSCNNProcessController],
  providers: [NVQSCNNProcessService],
  exports: [NVQSCNNProcessService],
})
export class NVQSCNNProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_CNN.processPath,
    module: NVQSCNNProcessModule,
    children: [],
  };
}
