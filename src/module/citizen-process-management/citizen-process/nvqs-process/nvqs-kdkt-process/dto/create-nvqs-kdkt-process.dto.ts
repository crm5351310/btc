import { OmitType } from "@nestjs/swagger";
import { NvqsKdktProcess } from "../entities";

export class CreateNvqsKdktProcessDto extends OmitType(NvqsKdktProcess, ['id']) { }
