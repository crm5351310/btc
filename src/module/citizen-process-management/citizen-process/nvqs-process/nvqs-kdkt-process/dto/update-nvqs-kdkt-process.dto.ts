import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNvqsKdktProcessDto } from './create-nvqs-kdkt-process.dto';

export class UpdateNvqsKdktProcessDto extends PartialType(
  OmitType(CreateNvqsKdktProcessDto, ['citizen'])
) { }
