import { Test, TestingModule } from '@nestjs/testing';
import { NvqsKdktProcessService } from './nvqs-kdkt-process.service';

describe('NvqsKdktProcessService', () => {
  let service: NvqsKdktProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NvqsKdktProcessService],
    }).compile();

    service = module.get<NvqsKdktProcessService>(NvqsKdktProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
