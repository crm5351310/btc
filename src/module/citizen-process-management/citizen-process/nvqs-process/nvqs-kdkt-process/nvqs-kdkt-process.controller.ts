import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { NvqsKdktProcessService } from './nvqs-kdkt-process.service';
import { CreateNvqsKdktProcessDto } from './dto/create-nvqs-kdkt-process.dto';
import { UpdateNvqsKdktProcessDto } from './dto/update-nvqs-kdkt-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.NVQS_KDKT_PROCESS)
export class NvqsKdktProcessController {
  constructor(private readonly nvqsKdktProcessService: NvqsKdktProcessService) { }

  @Post()
  create(@Body() createNvqsKdktProcessDto: CreateNvqsKdktProcessDto) {
    return this.nvqsKdktProcessService.create(createNvqsKdktProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nvqsKdktProcessService.findOne(+id);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNvqsKdktProcessDto: UpdateNvqsKdktProcessDto) {
    return this.nvqsKdktProcessService.update(+id, updateNvqsKdktProcessDto);
  }
}
