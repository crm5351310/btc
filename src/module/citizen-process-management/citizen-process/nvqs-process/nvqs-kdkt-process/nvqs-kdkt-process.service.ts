import { Injectable } from '@nestjs/common';
import { CreateNvqsKdktProcessDto } from './dto/create-nvqs-kdkt-process.dto';
import { UpdateNvqsKdktProcessDto } from './dto/update-nvqs-kdkt-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NvqsKdktProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { NVQS_KDKT } from '../../../../../database/seeds/process.seed';

@Injectable()
export class NvqsKdktProcessService extends CitizenProcessServiceBase<NvqsKdktProcess> {
  constructor(
    @InjectRepository(NvqsKdktProcess)
    repository: Repository<NvqsKdktProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NvqsKdktProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        health: true,
      },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateNvqsKdktProcessDto): Promise<ResponseCreated<NvqsKdktProcess>> {
    return super.create(dto, NVQS_KDKT);
  }

  update(id: number, dto: UpdateNvqsKdktProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
