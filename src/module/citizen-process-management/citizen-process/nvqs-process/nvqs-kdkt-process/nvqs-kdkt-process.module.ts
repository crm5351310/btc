import { Module } from '@nestjs/common';
import { NvqsKdktProcessService } from './nvqs-kdkt-process.service';
import { NvqsKdktProcessController } from './nvqs-kdkt-process.controller';
import { RouteTree } from '@nestjs/core';
import { NVQS_KDKT } from '../../../../../database/seeds/process.seed';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NvqsKdktProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([NvqsKdktProcess]),
    CitizenModule,
  ],
  controllers: [NvqsKdktProcessController],
  providers: [NvqsKdktProcessService],
})
export class NvqsKdktProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_KDKT.processPath,
    module: NvqsKdktProcessModule,
  };
}
