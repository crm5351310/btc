import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_CDK } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSCDKProcessDto } from './dto/create-nvqs-cdk-process.dto';
import { UpdateNVQSCDKProcessDto } from './dto/update-nvqs-cdk-process.dto';
import { NVQSCDKProcess } from './entities/nvqs-cdk-process.entity';

@Injectable()
export class NVQSCDKProcessService extends CitizenProcessServiceBase<NVQSCDKProcess> {
  constructor(
    @InjectRepository(NVQSCDKProcess)
    repository: Repository<NVQSCDKProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSCDKProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSCDKProcessDto,
  ): Promise<ResponseCreated<NVQSCDKProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_CDK);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSCDKProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
