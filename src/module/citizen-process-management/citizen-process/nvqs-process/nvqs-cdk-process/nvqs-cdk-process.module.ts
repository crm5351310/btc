import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSCDKProcess } from './entities/nvqs-cdk-process.entity';
import { NVQSCDKProcessController } from './nvqs-cdk-process.controller';
import { NVQSCDKProcessService } from './nvqs-cdk-process.service';
import { NVQS_CDK } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSCDKProcess]), CitizenModule],
  controllers: [NVQSCDKProcessController],
  providers: [NVQSCDKProcessService],
  exports: [NVQSCDKProcessService],
})
export class NVQSCDKProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_CDK.processPath,
    module: NVQSCDKProcessModule,
    children: [],
  };
}
