import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSCDKProcessDto } from './create-nvqs-cdk-process.dto';

export class UpdateNVQSCDKProcessDto extends OmitType(PartialType(CreateNVQSCDKProcessDto), [
  'citizen',
]) {}
