import { OmitType } from '@nestjs/swagger';
import { NVQSCDKProcess } from '../entities/nvqs-cdk-process.entity';

export class CreateNVQSCDKProcessDto extends OmitType(NVQSCDKProcess, ['id']) {}
