import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSCDKProcessDto } from './dto/create-nvqs-cdk-process.dto';
import { UpdateNVQSCDKProcessDto } from './dto/update-nvqs-cdk-process.dto';
import { NVQSCDKProcessService } from './nvqs-cdk-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_CDK_PROCESS)
export class NVQSCDKProcessController {
  constructor(private readonly NVQSCDKProcessService: NVQSCDKProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSCDKProcessDto) {
    return this.NVQSCDKProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSCDKProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSCDKProcessDto) {
    return this.NVQSCDKProcessService.update(+id, updateCitizenProcessDto);
  }
}
