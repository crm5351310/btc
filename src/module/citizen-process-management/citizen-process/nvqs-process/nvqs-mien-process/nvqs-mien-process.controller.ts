import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSMIENProcessDto } from './dto/create-nvqs-mien-process.dto';
import { UpdateNVQSMIENProcessDto } from './dto/update-nvqs-mien-process.dto';
import { NVQSMIENProcessService } from './nvqs-mien-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_MIEN_PROCESS)
export class NVQSMIENProcessController {
  constructor(private readonly NVQSMIENProcessService: NVQSMIENProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSMIENProcessDto) {
    return this.NVQSMIENProcessService.create(createCitizenProcessDto);
  }
  
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSMIENProcessService.findOne(+id);
  }


  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSMIENProcessDto) {
    return this.NVQSMIENProcessService.update(+id, updateCitizenProcessDto);
  }
}
