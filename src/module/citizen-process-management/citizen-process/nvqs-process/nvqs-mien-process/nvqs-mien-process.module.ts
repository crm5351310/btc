import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSMIENProcess } from './entities/nvqs-mien-process.entity';
import { NVQSMIENProcessController } from './nvqs-mien-process.controller';
import { NVQSMIENProcessService } from './nvqs-mien-process.service';
import { NVQS_MIEN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSMIENProcess]), CitizenModule],
  controllers: [NVQSMIENProcessController],
  providers: [NVQSMIENProcessService],
  exports: [NVQSMIENProcessService],
})
export class NVQSMIENProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_MIEN.processPath,
    module: NVQSMIENProcessModule,
    children: [],
  };
}
