import { OmitType } from '@nestjs/swagger';
import { NVQSMIENProcess } from '../entities/nvqs-mien-process.entity';

export class CreateNVQSMIENProcessDto extends OmitType(NVQSMIENProcess, ['id']) {}
