import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSMIENProcessDto } from './create-nvqs-mien-process.dto';

export class UpdateNVQSMIENProcessDto extends OmitType(PartialType(CreateNVQSMIENProcessDto), [
  'citizen',
]) {}
