import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_MIEN } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSMIENProcessDto } from './dto/create-nvqs-mien-process.dto';
import { UpdateNVQSMIENProcessDto } from './dto/update-nvqs-mien-process.dto';
import { NVQSMIENProcess } from './entities/nvqs-mien-process.entity';

@Injectable()
export class NVQSMIENProcessService extends CitizenProcessServiceBase<NVQSMIENProcess> {
  constructor(
    @InjectRepository(NVQSMIENProcess)
    repository: Repository<NVQSMIENProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSMIENProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSMIENProcessDto,
  ): Promise<ResponseCreated<NVQSMIENProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_MIEN);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSMIENProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
