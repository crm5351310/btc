import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { NVQS_TDK } from '../../../../../database/seeds/process.seed';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { CreateNVQSTDKProcessDto } from './dto/create-nvqs-tdk-process.dto';
import { UpdateNVQSTDKProcessDto } from './dto/update-nvqs-tdk-process.dto';
import { NVQSTDKProcess } from './entities/nvqs-tdk-process.entity';

@Injectable()
export class NVQSTDKProcessService extends CitizenProcessServiceBase<NVQSTDKProcess> {
  constructor(
    @InjectRepository(NVQSTDKProcess)
    repository: Repository<NVQSTDKProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSTDKProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSTDKProcessDto,
  ): Promise<ResponseCreated<NVQSTDKProcess>> {
    return await super.create(createCitizenProcessDto, NVQS_TDK);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSTDKProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
