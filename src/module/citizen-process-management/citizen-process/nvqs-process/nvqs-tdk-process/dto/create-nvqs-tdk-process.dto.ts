import { OmitType } from '@nestjs/swagger';
import { NVQSTDKProcess } from '../entities/nvqs-tdk-process.entity';

export class CreateNVQSTDKProcessDto extends OmitType(NVQSTDKProcess, ['id']) {}
