import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSTDKProcessDto } from './create-nvqs-tdk-process.dto';

export class UpdateNVQSTDKProcessDto extends OmitType(PartialType(CreateNVQSTDKProcessDto), [
  'citizen',
]) {}
