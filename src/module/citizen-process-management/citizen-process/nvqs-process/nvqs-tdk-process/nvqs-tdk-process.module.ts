import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSTDKProcess } from './entities/nvqs-tdk-process.entity';
import { NVQSTDKProcessController } from './nvqs-tdk-process.controller';
import { NVQSTDKProcessService } from './nvqs-tdk-process.service';
import { NVQS_TDK } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSTDKProcess]), CitizenModule],
  controllers: [NVQSTDKProcessController],
  providers: [NVQSTDKProcessService],
  exports: [NVQSTDKProcessService],
})
export class NVQSTDKProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_TDK.processPath,
    module: NVQSTDKProcessModule,
    children: [],
  };
}
