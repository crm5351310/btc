import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSTDKProcessDto } from './dto/create-nvqs-tdk-process.dto';
import { UpdateNVQSTDKProcessDto } from './dto/update-nvqs-tdk-process.dto';
import { NVQSTDKProcessService } from './nvqs-tdk-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_TDK_PROCESS)
export class NVQSTDKProcessController {
  constructor(private readonly NVQSTDKProcessService: NVQSTDKProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSTDKProcessDto) {
    return this.NVQSTDKProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSTDKProcessService.findOne(+id);
  }
  
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSTDKProcessDto) {
    return this.NVQSTDKProcessService.update(+id, updateCitizenProcessDto);
  }
}
