import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQSDKProcess } from './nvqs-dk-process/entities/nvqs-dk-process.entity';
import { NVQSDKProcessModule } from './nvqs-dk-process/nvqs-dk-process.module';
import { NVQSDKTNProcessModule } from './nvqs-dktn-process/nvqs-dktn-process.module';
import { NVQSTHDKProcessModule } from './nvqs-thdk-process/nvqs-thdk-process.module';
import { NVQSCDKProcessModule } from './nvqs-cdk-process/nvqs-cdk-process.module';
import { NVQSTDKProcessModule } from './nvqs-tdk-process/nvqs-tdk-process.module';
import { NVQSMDKProcessModule } from './nvqs-mdk-process/nvqs-mdk-process.module';
import { NVQSVNNProcessModule } from './nvqs-vnn-process/nvqs-vnn-process.module';
import { NVQSTHProcessModule } from './nvqs-th-process/nvqs-th-process.module';
import { NVQSMIENProcessModule } from './nvqs-mien-process/nvqs-mien-process.module';
import { NVQSGNNCTProcessModule } from './nvqs-gnnct-process/nvqs-gnnct-process.module';
import { NVQSGNNDPProcessModule } from './nvqs-gnndp-process/nvqs-gnndp-process.module';
import { NVQSNNProcessModule } from './nvqs-nn-process/nvqs-nn-process.module';
import { NVQSTNNProcessModule } from './nvqs-tnn-process/nvqs-tnn-process.module';
import { NVQSCNNProcessModule } from './nvqs-cnn-process/nvqs-cnn-process.module';
import { NVQSDNProcessModule } from './nvqs-dn-process/nvqs-dn-process.module';
import { NVQSXNProcessModule } from './nvqs-xn-process/nvqs-xn-process.module';
import { NVQS_DK } from 'src/database/seeds/process.seed';
import { NVQSDCProcessModule } from './nvqs-dc-process/nvqs-dc-process.module';
import { NVQSGSTProcessModule } from './nvqs-gst-process/nvqs-gst-process.module';
import { NVQSDSTProcessModule } from './nvqs-dst-process/nvqs-dst-process.module';
import { NVQSKDSTProcessModule } from './nvqs-kdst-process/nvqs-kdst-process.module';
import { NVQSCSTProcessModule } from './nvqs-cst-process/nvqs-cst-process.module';
import { NVQSTSTProcessModule } from './nvqs-tst-process/nvqs-tst-process.module';
import { NVQSVSTProcessModule } from './nvqs-vst-process/nvqs-vst-process.module';
import { NvqsDktProcessModule } from './nvqs-dkt-process/nvqs-dkt-process.module';
import { NvqsKdktProcessModule } from './nvqs-kdkt-process/nvqs-kdkt-process.module';
import { NvqsCktProcessModule } from './nvqs-ckt-process/nvqs-ckt-process.module';
import { NvqsTktProcessModule } from './nvqs-tkt-process/nvqs-tkt-process.module';
import { NvqsVktProcessModule } from './nvqs-vkt-process/nvqs-vkt-process.module';

@Module({
  imports: [
    NVQSDKProcessModule,
    TypeOrmModule.forFeature([NVQSDKProcess]),
    NVQSDKTNProcessModule,
    NVQSTHDKProcessModule,
    NVQSCDKProcessModule,
    NVQSTDKProcessModule,
    NVQSGSTProcessModule,
    NVQSDSTProcessModule,
    NVQSKDSTProcessModule,
    NVQSCSTProcessModule,
    NVQSTSTProcessModule,
    NVQSVSTProcessModule,
    NVQSMDKProcessModule,
    NVQSVNNProcessModule,
    NVQSTHProcessModule,
    NVQSMIENProcessModule,
    NVQSGNNCTProcessModule,
    NVQSGNNDPProcessModule,
    NVQSNNProcessModule,
    NVQSTNNProcessModule,
    NVQSCNNProcessModule,
    NVQSDNProcessModule,
    NVQSDCProcessModule,
    NVQSXNProcessModule,
    NvqsDktProcessModule,
    NvqsKdktProcessModule,
    NvqsCktProcessModule,
    NvqsTktProcessModule,
    NvqsVktProcessModule,
  ],
})
export class NVQSProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DK.modulePath,
    module: NVQSProcessModule,
    children: [
      NVQSDKProcessModule.route,
      NVQSDKTNProcessModule.route,
      NVQSTHDKProcessModule.route,
      NVQSCDKProcessModule.route,
      NVQSTDKProcessModule.route,
      NVQSGSTProcessModule.route,
      NVQSDSTProcessModule.route,
      NVQSKDSTProcessModule.route,
      NVQSCSTProcessModule.route,
      NVQSTSTProcessModule.route,
      NVQSVSTProcessModule.route,
      NVQSMDKProcessModule.route,
      NVQSVNNProcessModule.route,
      NVQSTHProcessModule.route,
      NVQSMIENProcessModule.route,
      NVQSGNNCTProcessModule.route,
      NVQSGNNDPProcessModule.route,
      NVQSNNProcessModule.route,
      NVQSTNNProcessModule.route,
      NVQSCNNProcessModule.route,
      NVQSDNProcessModule.route,
      NVQSDCProcessModule.route,
      NVQSXNProcessModule.route,
      NvqsDktProcessModule.route,
      NvqsKdktProcessModule.route,
      NvqsCktProcessModule.route,
      NvqsTktProcessModule.route,
      NvqsVktProcessModule.route,
    ],
  };
}
