import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NVQS_DST } from 'src/database/seeds/process.seed';
import { CitizenModule } from 'src/module/citizen-management/citizen/citizen.module';
import { NVQSDSTProcess } from './entities/nvqs-dst-process.entity';
import { NVQSDSTProcessController } from './nvqs-dst-process.controller';
import { NVQSDSTProcessService } from './nvqs-dst-process.service';
import { HealthModule } from '../../../../category/health/health.module';

@Module({
  imports: [TypeOrmModule.forFeature([NVQSDSTProcess]), CitizenModule, HealthModule],
  controllers: [NVQSDSTProcessController],
  providers: [NVQSDSTProcessService],
  exports: [NVQSDSTProcessService],
})
export class NVQSDSTProcessModule {
  static readonly route: RouteTree = {
    path: NVQS_DST.processPath,
    module: NVQSDSTProcessModule,
    children: [],
  };
}
