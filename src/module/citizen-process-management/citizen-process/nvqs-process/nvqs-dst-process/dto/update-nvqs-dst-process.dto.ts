import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateNVQSDSTProcessDto } from './create-nvqs-dst-process.dto';

export class UpdateNVQSDSTProcessDto extends OmitType(PartialType(CreateNVQSDSTProcessDto), [
  'citizen',
]) {}
