import { OmitType } from '@nestjs/swagger';
import { NVQSDSTProcess } from '../entities/nvqs-dst-process.entity';

export class CreateNVQSDSTProcessDto extends OmitType(NVQSDSTProcess, ['id']) {}
