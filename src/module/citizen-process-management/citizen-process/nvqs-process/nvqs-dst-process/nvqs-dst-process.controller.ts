import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';
import { CreateNVQSDSTProcessDto } from './dto/create-nvqs-dst-process.dto';
import { UpdateNVQSDSTProcessDto } from './dto/update-nvqs-dst-process.dto';
import { NVQSDSTProcessService } from './nvqs-dst-process.service';
@Controller()
@ApiTags(TagEnum.NVQS_DST_PROCESS)
export class NVQSDSTProcessController {
  constructor(private readonly NVQSDSTProcessService: NVQSDSTProcessService) {}

  @Post()
  create(@Body() createCitizenProcessDto: CreateNVQSDSTProcessDto) {
    return this.NVQSDSTProcessService.create(createCitizenProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.NVQSDSTProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCitizenProcessDto: UpdateNVQSDSTProcessDto) {
    return this.NVQSDSTProcessService.update(+id, updateCitizenProcessDto);
  }
}
