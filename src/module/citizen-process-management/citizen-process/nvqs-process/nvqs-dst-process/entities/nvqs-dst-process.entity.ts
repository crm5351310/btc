import { Optional } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { ChildEntity, Column, ManyToOne } from 'typeorm';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { Health } from '../../../../../category';
import { StatusExamination } from '../../../../category/status-examination/entities/status-examination.entity';
import { CitizenProcess } from '../../../entities/citizen-process.entity';

@ChildEntity()
export class NVQSDSTProcess extends CitizenProcess {
  @ApiProperty({
    description: 'Chiều cao (cm)',
    example: 180,
  })
  @IsNotEmpty()
  @Column('int')
  height: number;

  @ApiProperty({
    description: 'Cân nặng (kg)',
    example: 180,
  })
  @IsNotEmpty()
  @Column('int')
  weight: number;

  @ApiProperty({
    description: 'Vòng ngực (cm)',
    example: 180,
  })
  @IsNotEmpty()
  @Column('int')
  chestCircumference: number;

  @ApiProperty({
    description: 'Huyết áp (mmHg)',
  })
  @IsNotEmpty()
  @MaxLength(25)
  @Column('varchar', { length: 100 })
  bloodPressure: string;

  @ApiProperty({
    description: 'Xung nhịp tim',
    example: 180,
  })
  @IsNotEmpty()
  @MaxLength(25)
  @Column('varchar', { length: 25 })
  pulse: string;

  @ApiProperty({
    description: 'Thị lực',
    example: 'T 5/10 P 6/10',
  })
  @IsNotEmpty()
  @MaxLength(25)
  @Column('varchar', { length: 25 })
  eyesight: string;

  @ApiProperty({
    description: 'Ghi chú',
    example: 'Ghi chú',
  })
  @Column('varchar', { length: 1000 })
  @Optional()
  note?: string;

  @ApiProperty({
    description: 'ID phân loại sức khoẻ',
    example: { id: 1 },
    type: () => StatusExamination,
  })
  @IsNotEmpty()
  @IsObjectRelation()
  @ManyToOne(() => Health)
  health: Health;
}
