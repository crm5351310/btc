import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseCreated, ResponseFindOne } from '../../../../../common/response';
import { Repository } from 'typeorm';
import { CreateNVQSDSTProcessDto } from './dto/create-nvqs-dst-process.dto';
import { UpdateNVQSDSTProcessDto } from './dto/update-nvqs-dst-process.dto';
import { NVQSDSTProcess } from './entities/nvqs-dst-process.entity';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { NVQS_DST } from '../../../../../database/seeds/process.seed';
import { HealthService } from 'src/module/category/health/health.service';

@Injectable()
export class NVQSDSTProcessService extends CitizenProcessServiceBase<NVQSDSTProcess> {
  constructor(
    @InjectRepository(NVQSDSTProcess)
    repository: Repository<NVQSDSTProcess>,
    citizenService: CitizenService,
    private readonly healthService: HealthService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<NVQSDSTProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        citizen: true,
        health: true,
      },
    });
    return new ResponseFindOne(result);
  }

  async create(
    createCitizenProcessDto: CreateNVQSDSTProcessDto,
  ): Promise<ResponseCreated<NVQSDSTProcess>> {
    // check tồn tại phân loại sức khoẻ
    await this.healthService.findOne(createCitizenProcessDto.health.id);

    return await super.create(createCitizenProcessDto, NVQS_DST);
  }

  async update(id: number, updateCitizenProcessDto: UpdateNVQSDSTProcessDto) {
    return await super.update(id, updateCitizenProcessDto);
  }
}
