import { Module } from '@nestjs/common';
import { DqtvXdhnProcessService } from './dqtv-xdhn-process.service';
import { DqtvXdhnProcessController } from './dqtv-xdhn-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvXdhnProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_XDHN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvXdhnProcess]), CitizenModule],
  controllers: [DqtvXdhnProcessController],
  providers: [DqtvXdhnProcessService],
})
export class DqtvXdhnProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_XDHN.processPath,
    module: DqtvXdhnProcessModule,
  };
}
