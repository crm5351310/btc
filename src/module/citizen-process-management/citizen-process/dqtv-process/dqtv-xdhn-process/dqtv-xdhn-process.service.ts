import { Injectable } from '@nestjs/common';
import { CreateDqtvXdhnProcessDto } from './dto/create-dqtv-xdhn-process.dto';
import { UpdateDqtvXdhnProcessDto } from './dto/update-dqtv-xdhn-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvXdhnProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_XDHN } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvXdhnProcessService extends CitizenProcessServiceBase<DqtvXdhnProcess> {
  constructor(
    @InjectRepository(DqtvXdhnProcess)
    repository: Repository<DqtvXdhnProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvXdhnProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvXdhnProcessDto): Promise<ResponseCreated<DqtvXdhnProcess>> {
    return super.create(dto, DQTV_XDHN);
  }

  update(id: number, dto: UpdateDqtvXdhnProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
