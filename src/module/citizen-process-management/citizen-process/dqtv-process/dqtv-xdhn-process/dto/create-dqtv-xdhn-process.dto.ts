import { OmitType } from '@nestjs/swagger';
import { DqtvXdhnProcess } from '../entities';

export class CreateDqtvXdhnProcessDto extends OmitType(DqtvXdhnProcess, ['id']) {}
