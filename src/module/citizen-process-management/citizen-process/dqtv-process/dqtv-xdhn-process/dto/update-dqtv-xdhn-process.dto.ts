import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvXdhnProcessDto } from './create-dqtv-xdhn-process.dto';

export class UpdateDqtvXdhnProcessDto extends PartialType(
  OmitType(CreateDqtvXdhnProcessDto, ['citizen']),
) {}
