import { Test, TestingModule } from '@nestjs/testing';
import { DqtvXdhnProcessService } from './dqtv-xdhn-process.service';

describe('DqtvXdhnProcessService', () => {
  let service: DqtvXdhnProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvXdhnProcessService],
    }).compile();

    service = module.get<DqtvXdhnProcessService>(DqtvXdhnProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
