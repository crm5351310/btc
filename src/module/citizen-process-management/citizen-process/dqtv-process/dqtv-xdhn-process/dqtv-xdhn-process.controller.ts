import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvXdhnProcessService } from './dqtv-xdhn-process.service';
import { CreateDqtvXdhnProcessDto } from './dto/create-dqtv-xdhn-process.dto';
import { UpdateDqtvXdhnProcessDto } from './dto/update-dqtv-xdhn-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_XDHN_PROCESS)
export class DqtvXdhnProcessController {
  constructor(private readonly dqtvXdhnProcessService: DqtvXdhnProcessService) { }

  @Post()
  create(@Body() createDqtvXdhnProcessDto: CreateDqtvXdhnProcessDto) {
    return this.dqtvXdhnProcessService.create(createDqtvXdhnProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvXdhnProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvXdhnProcessDto: UpdateDqtvXdhnProcessDto) {
    return this.dqtvXdhnProcessService.update(+id, updateDqtvXdhnProcessDto);
  }
}
