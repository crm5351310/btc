import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvLcProcessDto } from './create-dqtv-lc-process.dto';

export class UpdateDqtvLcProcessDto extends PartialType(
  OmitType(CreateDqtvLcProcessDto, ['citizen']),
) {}
