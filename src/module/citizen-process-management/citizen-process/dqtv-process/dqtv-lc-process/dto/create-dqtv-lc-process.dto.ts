import { OmitType } from '@nestjs/swagger';
import { DqtvLcProcess } from '../entities';

export class CreateDqtvLcProcessDto extends OmitType(DqtvLcProcess, ['id']) {}
