import { Test, TestingModule } from '@nestjs/testing';
import { DqtvLcProcessService } from './dqtv-lc-process.service';

describe('DqtvLcProcessService', () => {
  let service: DqtvLcProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvLcProcessService],
    }).compile();

    service = module.get<DqtvLcProcessService>(DqtvLcProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
