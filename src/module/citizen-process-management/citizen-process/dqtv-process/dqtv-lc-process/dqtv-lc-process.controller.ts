import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvLcProcessService } from './dqtv-lc-process.service';
import { CreateDqtvLcProcessDto } from './dto/create-dqtv-lc-process.dto';
import { UpdateDqtvLcProcessDto } from './dto/update-dqtv-lc-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_LC_PROCESS)
export class DqtvLcProcessController {
  constructor(private readonly dqtvLcProcessService: DqtvLcProcessService) { }

  @Post()
  create(@Body() createDqtvLcProcessDto: CreateDqtvLcProcessDto) {
    return this.dqtvLcProcessService.create(createDqtvLcProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvLcProcessService.findOne(+id);
  }


  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvLcProcessDto: UpdateDqtvLcProcessDto) {
    return this.dqtvLcProcessService.update(+id, updateDqtvLcProcessDto);
  }
}
