import { Injectable } from '@nestjs/common';
import { CreateDqtvLcProcessDto } from './dto/create-dqtv-lc-process.dto';
import { UpdateDqtvLcProcessDto } from './dto/update-dqtv-lc-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvLcProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_LC } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvLcProcessService extends CitizenProcessServiceBase<DqtvLcProcess> {
  constructor(
    @InjectRepository(DqtvLcProcess)
    repository: Repository<DqtvLcProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvLcProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        militiaDefenseUnit: true,
        force: {
          forceType: true,
        },
        militaryRank: {
          militaryRankType: true,
        },
        jobTitle: true,
      },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvLcProcessDto): Promise<ResponseCreated<DqtvLcProcess>> {
    return super.create(dto, DQTV_LC);
  }

  update(id: number, dto: UpdateDqtvLcProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
