import { Module } from '@nestjs/common';
import { DqtvLcProcessService } from './dqtv-lc-process.service';
import { DqtvLcProcessController } from './dqtv-lc-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvLcProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_LC } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvLcProcess]), CitizenModule],
  controllers: [DqtvLcProcessController],
  providers: [DqtvLcProcessService],
})
export class DqtvLcProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_LC.processPath,
    module: DqtvLcProcessModule,
  };
}
