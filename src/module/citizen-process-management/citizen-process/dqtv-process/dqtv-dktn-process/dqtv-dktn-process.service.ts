import { Injectable } from '@nestjs/common';
import { CreateDqtvDktnProcessDto } from './dto/create-dqtv-dktn-process.dto';
import { UpdateDqtvDktnProcessDto } from './dto/update-dqtv-dktn-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvDktnProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_DKTN } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvDktnProcessService extends CitizenProcessServiceBase<DqtvDktnProcess> {
  constructor(
    @InjectRepository(DqtvDktnProcess)
    repository: Repository<DqtvDktnProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvDktnProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvDktnProcessDto): Promise<ResponseCreated<DqtvDktnProcess>> {
    return super.create(dto, DQTV_DKTN);
  }

  update(id: number, dto: UpdateDqtvDktnProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
