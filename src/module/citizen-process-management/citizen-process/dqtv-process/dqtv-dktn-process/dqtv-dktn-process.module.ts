import { Module } from '@nestjs/common';
import { DqtvDktnProcessService } from './dqtv-dktn-process.service';
import { DqtvDktnProcessController } from './dqtv-dktn-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvDktnProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_DKTN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvDktnProcess]), CitizenModule],
  controllers: [DqtvDktnProcessController],
  providers: [DqtvDktnProcessService],
})
export class DqtvDktnProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_DKTN.processPath,
    module: DqtvDktnProcessModule,
  };
}
