import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvDktnProcessDto } from './create-dqtv-dktn-process.dto';

export class UpdateDqtvDktnProcessDto extends PartialType(
  OmitType(CreateDqtvDktnProcessDto, ['citizen']),
) {}
