import { OmitType } from '@nestjs/swagger';
import { DqtvDktnProcess } from '../entities';

export class CreateDqtvDktnProcessDto extends OmitType(DqtvDktnProcess, ['id']) {}
