import { Test, TestingModule } from '@nestjs/testing';
import { DqtvDktnProcessService } from './dqtv-dktn-process.service';

describe('DqtvDktnProcessService', () => {
  let service: DqtvDktnProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvDktnProcessService],
    }).compile();

    service = module.get<DqtvDktnProcessService>(DqtvDktnProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
