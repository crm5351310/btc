import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvDktnProcessService } from './dqtv-dktn-process.service';
import { CreateDqtvDktnProcessDto } from './dto/create-dqtv-dktn-process.dto';
import { UpdateDqtvDktnProcessDto } from './dto/update-dqtv-dktn-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_DKTN_PROCESS)
export class DqtvDktnProcessController {
  constructor(private readonly dqtvDktnProcessService: DqtvDktnProcessService) { }

  @Post()
  create(@Body() createDqtvDktnProcessDto: CreateDqtvDktnProcessDto) {
    return this.dqtvDktnProcessService.create(createDqtvDktnProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvDktnProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvDktnProcessDto: UpdateDqtvDktnProcessDto) {
    return this.dqtvDktnProcessService.update(+id, updateDqtvDktnProcessDto);
  }
}
