import { Module } from '@nestjs/common';
import { DqtvTbcProcessService } from './dqtv-tbc-process.service';
import { DqtvTbcProcessController } from './dqtv-tbc-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvTbcProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_TBC } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvTbcProcess]), CitizenModule],
  controllers: [DqtvTbcProcessController],
  providers: [DqtvTbcProcessService],
})
export class DqtvTbcProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_TBC.processPath,
    module: DqtvTbcProcessModule,
  };
}
