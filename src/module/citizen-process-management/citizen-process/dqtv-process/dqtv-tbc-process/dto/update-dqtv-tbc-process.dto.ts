import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvTbcProcessDto } from './create-dqtv-tbc-process.dto';

export class UpdateDqtvTbcProcessDto extends PartialType(
  OmitType(CreateDqtvTbcProcessDto, ['citizen']),
) {}
