import { OmitType } from '@nestjs/swagger';
import { DqtvTbcProcess } from '../entities';

export class CreateDqtvTbcProcessDto extends OmitType(DqtvTbcProcess, ['id']) {}
