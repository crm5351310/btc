import { ChildEntity } from 'typeorm';
import { CitizenProcess } from '../../../entities';

@ChildEntity()
export class DqtvTbcProcess extends CitizenProcess {}
