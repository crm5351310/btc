import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvTbcProcessService } from './dqtv-tbc-process.service';
import { CreateDqtvTbcProcessDto } from './dto/create-dqtv-tbc-process.dto';
import { UpdateDqtvTbcProcessDto } from './dto/update-dqtv-tbc-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_TBC_PROCESS)
export class DqtvTbcProcessController {
  constructor(private readonly dqtvTbcProcessService: DqtvTbcProcessService) { }

  @Post()
  create(@Body() createDqtvTbcProcessDto: CreateDqtvTbcProcessDto) {
    return this.dqtvTbcProcessService.create(createDqtvTbcProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvTbcProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvTbcProcessDto: UpdateDqtvTbcProcessDto) {
    return this.dqtvTbcProcessService.update(+id, updateDqtvTbcProcessDto);
  }
}
