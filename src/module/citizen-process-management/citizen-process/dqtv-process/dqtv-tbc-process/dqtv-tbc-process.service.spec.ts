import { Test, TestingModule } from '@nestjs/testing';
import { DqtvTbcProcessService } from './dqtv-tbc-process.service';

describe('DqtvTbcProcessService', () => {
  let service: DqtvTbcProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvTbcProcessService],
    }).compile();

    service = module.get<DqtvTbcProcessService>(DqtvTbcProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
