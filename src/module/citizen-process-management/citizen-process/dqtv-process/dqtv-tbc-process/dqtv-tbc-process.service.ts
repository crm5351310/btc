import { Injectable } from '@nestjs/common';
import { CreateDqtvTbcProcessDto } from './dto/create-dqtv-tbc-process.dto';
import { UpdateDqtvTbcProcessDto } from './dto/update-dqtv-tbc-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvTbcProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_TBC } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvTbcProcessService extends CitizenProcessServiceBase<DqtvTbcProcess> {
  constructor(
    @InjectRepository(DqtvTbcProcess)
    repository: Repository<DqtvTbcProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvTbcProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvTbcProcessDto): Promise<ResponseCreated<DqtvTbcProcess>> {
    return super.create(dto, DQTV_TBC);
  }

  update(id: number, dto: UpdateDqtvTbcProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
