import { Test, TestingModule } from '@nestjs/testing';
import { DqtvDkProcessService } from './dqtv-dk-process.service';

describe('DqtvDkProcessService', () => {
  let service: DqtvDkProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvDkProcessService],
    }).compile();

    service = module.get<DqtvDkProcessService>(DqtvDkProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
