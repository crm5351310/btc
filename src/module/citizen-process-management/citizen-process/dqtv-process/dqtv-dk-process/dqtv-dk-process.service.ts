import { Injectable } from '@nestjs/common';
import { CreateDqtvDkProcessDto } from './dto/create-dqtv-dk-process.dto';
import { UpdateDqtvDkProcessDto } from './dto/update-dqtv-dk-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvDkProcess } from './entities';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_DK } from '../../../../../database/seeds/process.seed';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';

@Injectable()
export class DqtvDkProcessService extends CitizenProcessServiceBase<DqtvDkProcess> {
  constructor(
    @InjectRepository(DqtvDkProcess)
    repository: Repository<DqtvDkProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvDkProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvDkProcessDto): Promise<ResponseCreated<DqtvDkProcess>> {
    return super.create(dto, DQTV_DK);
  }

  update(id: number, dto: UpdateDqtvDkProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
