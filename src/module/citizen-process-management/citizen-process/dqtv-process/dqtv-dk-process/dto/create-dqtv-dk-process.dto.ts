import { OmitType } from '@nestjs/swagger';
import { DqtvDkProcess } from '../entities';

export class CreateDqtvDkProcessDto extends OmitType(DqtvDkProcess, ['id']) {}
