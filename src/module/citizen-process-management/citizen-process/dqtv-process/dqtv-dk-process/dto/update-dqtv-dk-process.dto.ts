import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvDkProcessDto } from './create-dqtv-dk-process.dto';

export class UpdateDqtvDkProcessDto extends PartialType(
  OmitType(CreateDqtvDkProcessDto, ['citizen']),
) {}
