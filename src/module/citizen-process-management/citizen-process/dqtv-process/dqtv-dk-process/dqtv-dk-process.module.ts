import { Module } from '@nestjs/common';
import { DqtvDkProcessService } from './dqtv-dk-process.service';
import { DqtvDkProcessController } from './dqtv-dk-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvDkProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_DK } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvDkProcess]), CitizenModule],
  controllers: [DqtvDkProcessController],
  providers: [DqtvDkProcessService],
})
export class DqtvDkProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_DK.processPath,
    module: DqtvDkProcessModule,
  };
}
