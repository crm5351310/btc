import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvDkProcessService } from './dqtv-dk-process.service';
import { CreateDqtvDkProcessDto } from './dto/create-dqtv-dk-process.dto';
import { UpdateDqtvDkProcessDto } from './dto/update-dqtv-dk-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_DK_PROCESS)
export class DqtvDkProcessController {
  constructor(private readonly dqtvDkProcessService: DqtvDkProcessService) { }

  @Post()
  create(@Body() createDqtvDkProcessDto: CreateDqtvDkProcessDto) {
    return this.dqtvDkProcessService.create(createDqtvDkProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvDkProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvDkProcessDto: UpdateDqtvDkProcessDto) {
    return this.dqtvDkProcessService.update(+id, updateDqtvDkProcessDto);
  }
}
