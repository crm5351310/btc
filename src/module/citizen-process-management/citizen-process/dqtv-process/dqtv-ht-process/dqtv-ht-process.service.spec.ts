import { Test, TestingModule } from '@nestjs/testing';
import { DqtvHtProcessService } from './dqtv-ht-process.service';

describe('DqtvHtProcessService', () => {
  let service: DqtvHtProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvHtProcessService],
    }).compile();

    service = module.get<DqtvHtProcessService>(DqtvHtProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
