import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvHtProcessDto } from './create-dqtv-ht-process.dto';

export class UpdateDqtvHtProcessDto extends PartialType(
  OmitType(CreateDqtvHtProcessDto, ['citizen']),
) {}
