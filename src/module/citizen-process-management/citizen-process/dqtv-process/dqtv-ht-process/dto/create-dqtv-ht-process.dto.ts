import { OmitType } from '@nestjs/swagger';
import { DqtvHtProcess } from '../entities';

export class CreateDqtvHtProcessDto extends OmitType(DqtvHtProcess, ['id']) {}
