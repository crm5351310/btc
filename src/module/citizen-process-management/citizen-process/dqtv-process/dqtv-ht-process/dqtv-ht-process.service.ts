import { Injectable } from '@nestjs/common';
import { CreateDqtvHtProcessDto } from './dto/create-dqtv-ht-process.dto';
import { UpdateDqtvHtProcessDto } from './dto/update-dqtv-ht-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvHtProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_HT } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvHtProcessService extends CitizenProcessServiceBase<DqtvHtProcess> {
  constructor(
    @InjectRepository(DqtvHtProcess)
    repository: Repository<DqtvHtProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvHtProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvHtProcessDto): Promise<ResponseCreated<DqtvHtProcess>> {
    return super.create(dto, DQTV_HT);
  }

  update(id: number, dto: UpdateDqtvHtProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
