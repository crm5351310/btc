import { Module } from '@nestjs/common';
import { DqtvHtProcessService } from './dqtv-ht-process.service';
import { DqtvHtProcessController } from './dqtv-ht-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvHtProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_HT } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvHtProcess]), CitizenModule],
  controllers: [DqtvHtProcessController],
  providers: [DqtvHtProcessService],
})
export class DqtvHtProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_HT.processPath,
    module: DqtvHtProcessModule,
  };
}
