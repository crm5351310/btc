import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvHtProcessService } from './dqtv-ht-process.service';
import { CreateDqtvHtProcessDto } from './dto/create-dqtv-ht-process.dto';
import { UpdateDqtvHtProcessDto } from './dto/update-dqtv-ht-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_HT_PROCESS)
export class DqtvHtProcessController {
  constructor(private readonly dqtvHtProcessService: DqtvHtProcessService) { }

  @Post()
  create(@Body() createDqtvHtProcessDto: CreateDqtvHtProcessDto) {
    return this.dqtvHtProcessService.create(createDqtvHtProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvHtProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvHtProcessDto: UpdateDqtvHtProcessDto) {
    return this.dqtvHtProcessService.update(+id, updateDqtvHtProcessDto);
  }
}
