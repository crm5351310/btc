import { OmitType } from '@nestjs/swagger';
import { DqtvRkxdhnProcess } from '../entities';

export class CreateDqtvRkxdhnProcessDto extends OmitType(DqtvRkxdhnProcess, ['id']) {}
