import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvRkxdhnProcessDto } from './create-dqtv-rkxdhn-process.dto';

export class UpdateDqtvRkxdhnProcessDto extends PartialType(
  OmitType(CreateDqtvRkxdhnProcessDto, ['citizen']),
) {}
