import { Test, TestingModule } from '@nestjs/testing';
import { DqtvRkxdhnProcessService } from './dqtv-rkxdhn-process.service';

describe('DqtvRkxdhnProcessService', () => {
  let service: DqtvRkxdhnProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvRkxdhnProcessService],
    }).compile();

    service = module.get<DqtvRkxdhnProcessService>(DqtvRkxdhnProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
