import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvRkxdhnProcessService } from './dqtv-rkxdhn-process.service';
import { CreateDqtvRkxdhnProcessDto } from './dto/create-dqtv-rkxdhn-process.dto';
import { UpdateDqtvRkxdhnProcessDto } from './dto/update-dqtv-rkxdhn-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_RKXDHN_PROCESS)
export class DqtvRkxdhnProcessController {
  constructor(private readonly dqtvRkxdhnProcessService: DqtvRkxdhnProcessService) { }

  @Post()
  create(@Body() createDqtvRkxdhnProcessDto: CreateDqtvRkxdhnProcessDto) {
    return this.dqtvRkxdhnProcessService.create(createDqtvRkxdhnProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvRkxdhnProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvRkxdhnProcessDto: UpdateDqtvRkxdhnProcessDto) {
    return this.dqtvRkxdhnProcessService.update(+id, updateDqtvRkxdhnProcessDto);
  }
}
