import { Injectable } from '@nestjs/common';
import { CreateDqtvRkxdhnProcessDto } from './dto/create-dqtv-rkxdhn-process.dto';
import { UpdateDqtvRkxdhnProcessDto } from './dto/update-dqtv-rkxdhn-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvRkxdhnProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_RKXDHN } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvRkxdhnProcessService extends CitizenProcessServiceBase<DqtvRkxdhnProcess> {
  constructor(
    @InjectRepository(DqtvRkxdhnProcess)
    repository: Repository<DqtvRkxdhnProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvRkxdhnProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvRkxdhnProcessDto): Promise<ResponseCreated<DqtvRkxdhnProcess>> {
    return super.create(dto, DQTV_RKXDHN);
  }

  update(id: number, dto: UpdateDqtvRkxdhnProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
