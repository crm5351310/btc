import { Module } from '@nestjs/common';
import { DqtvRkxdhnProcessService } from './dqtv-rkxdhn-process.service';
import { DqtvRkxdhnProcessController } from './dqtv-rkxdhn-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvRkxdhnProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_RKXDHN } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvRkxdhnProcess]), CitizenModule],
  controllers: [DqtvRkxdhnProcessController],
  providers: [DqtvRkxdhnProcessService],
})
export class DqtvRkxdhnProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_RKXDHN.processPath,
    module: DqtvRkxdhnProcessModule,
  };
}
