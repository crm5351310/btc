import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { DqtvDkProcessModule } from './dqtv-dk-process/dqtv-dk-process.module';
import { DqtvDktnProcessModule } from './dqtv-dktn-process/dqtv-dktn-process.module';
import { DqtvXdhnProcessModule } from './dqtv-xdhn-process/dqtv-xdhn-process.module';
import { DqtvBcProcessModule } from './dqtv-bc-process/dqtv-bc-process.module';
import { DqtvLcProcessModule } from './dqtv-lc-process/dqtv-lc-process.module';
import { DqtvTbcProcessModule } from './dqtv-tbc-process/dqtv-tbc-process.module';
import { DqtvRkxdhnProcessModule } from './dqtv-rkxdhn-process/dqtv-rkxdhn-process.module';
import { DqtvHtProcessModule } from './dqtv-ht-process/dqtv-ht-process.module';
import { DQTV_DK } from 'src/database/seeds/process.seed';

@Module({
  imports: [
    DqtvDkProcessModule,
    DqtvDktnProcessModule,
    DqtvXdhnProcessModule,
    DqtvBcProcessModule,
    DqtvLcProcessModule,
    DqtvTbcProcessModule,
    DqtvRkxdhnProcessModule,
    DqtvHtProcessModule,
  ],
})
export class DqtvProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_DK.modulePath,
    module: DqtvProcessModule,
    children: [
      DqtvDkProcessModule.route,
      DqtvDktnProcessModule.route,
      DqtvXdhnProcessModule.route,
      DqtvBcProcessModule.route,
      DqtvLcProcessModule.route,
      DqtvTbcProcessModule.route,
      DqtvRkxdhnProcessModule.route,
      DqtvHtProcessModule.route,
    ],
  };
}
