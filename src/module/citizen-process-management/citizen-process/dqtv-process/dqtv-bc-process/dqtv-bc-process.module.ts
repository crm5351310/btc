import { Module } from '@nestjs/common';
import { DqtvBcProcessService } from './dqtv-bc-process.service';
import { DqtvBcProcessController } from './dqtv-bc-process.controller';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DqtvBcProcess } from './entities';
import { CitizenModule } from '../../../../citizen-management/citizen/citizen.module';
import { DQTV_BC } from 'src/database/seeds/process.seed';

@Module({
  imports: [TypeOrmModule.forFeature([DqtvBcProcess]), CitizenModule],
  controllers: [DqtvBcProcessController],
  providers: [DqtvBcProcessService],
})
export class DqtvBcProcessModule {
  static readonly route: RouteTree = {
    path: DQTV_BC.processPath,
    module: DqtvBcProcessModule,
  };
}
