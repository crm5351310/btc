import { Test, TestingModule } from '@nestjs/testing';
import { DqtvBcProcessService } from './dqtv-bc-process.service';

describe('DqtvBcProcessService', () => {
  let service: DqtvBcProcessService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DqtvBcProcessService],
    }).compile();

    service = module.get<DqtvBcProcessService>(DqtvBcProcessService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
