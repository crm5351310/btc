import { ChildEntity, ManyToOne } from 'typeorm';
import { CitizenProcess } from '../../../entities';
import { MilitiaDefenseUnit } from '../../../../../category/militia-defense-unit/militia-defense-unit.entity';
import { IsOptional } from 'class-validator';
import { IsObjectRelation } from '../../../../../../common/validator/is-object-relation';
import { ApiProperty } from '@nestjs/swagger';
import { Force } from '../../../../../category/force-root/force/entities/force.entity';
import { MilitaryRank } from '../../../../../category/military-rank-root/military-rank/military-rank.entity';
import { JobTitle } from '../../../../../category/job-title/job-title.entity';

@ChildEntity()
export class DqtvBcProcess extends CitizenProcess {
  @ApiProperty({
    description: 'Đơn vị dân quân tự vệ',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitiaDefenseUnit, { nullable: true })
  militiaDefenseUnit?: MilitiaDefenseUnit;

  @ApiProperty({
    description: 'Lực lượng',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => Force, { nullable: true })
  force?: Force;

  @ApiProperty({
    description: 'Quân hàm',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => MilitaryRank, { nullable: true })
  militaryRank?: MilitaryRank;

  @ApiProperty({
    description: 'Chức danh',
    example: { id: 1 },
    required: false,
  })
  @IsOptional()
  @IsObjectRelation()
  @ManyToOne(() => JobTitle, { nullable: true })
  jobTitle?: JobTitle;
}
