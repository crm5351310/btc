import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateDqtvBcProcessDto } from './create-dqtv-bc-process.dto';

export class UpdateDqtvBcProcessDto extends PartialType(
  OmitType(CreateDqtvBcProcessDto, ['citizen']),
) {}
