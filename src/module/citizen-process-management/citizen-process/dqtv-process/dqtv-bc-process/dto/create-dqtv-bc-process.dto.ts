import { DqtvBcProcess } from '../entities';
import { OmitType } from '@nestjs/swagger';

export class CreateDqtvBcProcessDto extends OmitType(DqtvBcProcess, ['id']) {}
