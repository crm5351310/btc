import { Injectable } from '@nestjs/common';
import { CreateDqtvBcProcessDto } from './dto/create-dqtv-bc-process.dto';
import { UpdateDqtvBcProcessDto } from './dto/update-dqtv-bc-process.dto';
import { CitizenProcessServiceBase } from '../../base/citizen-process-service.base';
import { DqtvBcProcess } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CitizenService } from '../../../../citizen-management/citizen/services/citizen.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../../common/response';
import { DQTV_BC } from '../../../../../database/seeds/process.seed';

@Injectable()
export class DqtvBcProcessService extends CitizenProcessServiceBase<DqtvBcProcess> {
  constructor(
    @InjectRepository(DqtvBcProcess)
    repository: Repository<DqtvBcProcess>,
    citizenService: CitizenService,
  ) {
    super(repository, citizenService);
  }

  async findOne(id: number): Promise<ResponseFindOne<DqtvBcProcess | null>> {
    const result = await this.repository.findOne({
      where: { id },
      relations: {
        militiaDefenseUnit: true,
        force: {
          forceType: true,
        },
        militaryRank: {
          militaryRankType: true,
        },
        jobTitle: true,
      },
    });
    return new ResponseFindOne(result);
  }

  create(dto: CreateDqtvBcProcessDto): Promise<ResponseCreated<DqtvBcProcess>> {
    return super.create(dto, DQTV_BC);
  }

  update(id: number, dto: UpdateDqtvBcProcessDto): Promise<ResponseUpdate> {
    return super.update(id, dto);
  }
}
