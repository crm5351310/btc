import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { DqtvBcProcessService } from './dqtv-bc-process.service';
import { CreateDqtvBcProcessDto } from './dto/create-dqtv-bc-process.dto';
import { UpdateDqtvBcProcessDto } from './dto/update-dqtv-bc-process.dto';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.DQTV_BC_PROCESS)
export class DqtvBcProcessController {
  constructor(private readonly dqtvBcProcessService: DqtvBcProcessService) { }

  @Post()
  create(@Body() createDqtvBcProcessDto: CreateDqtvBcProcessDto) {
    return this.dqtvBcProcessService.create(createDqtvBcProcessDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dqtvBcProcessService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDqtvBcProcessDto: UpdateDqtvBcProcessDto) {
    return this.dqtvBcProcessService.update(+id, updateDqtvBcProcessDto);
  }
}
