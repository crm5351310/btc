import { BaseCRUDService } from 'src/common/base/service/base-crud.service';

export interface BaseCitizenProcess<T> extends Pick<BaseCRUDService<T>, 'create' | 'update'> {}
