import { BaseCRUDService } from '../../../../common/base/service/base-crud.service';
import { ResponseCreated, ResponseFindOne, ResponseUpdate } from '../../../../common/response';
import { CitizenProcess } from '../entities/citizen-process.entity';
import { CitizenService } from '../../../citizen-management/citizen/services/citizen.service';
import { Process } from '../../category/process/entities/process.entity';
import { FindOptionsRelations, FindOptionsWhere, Repository } from 'typeorm';
import { InvalidException } from '../../../../common/exception/invalid.exception';
import { NotFoundException } from '../../../../common/exception/not-found.exception';

export abstract class CitizenProcessServiceBase<T extends CitizenProcess>
  implements Pick<BaseCRUDService<T>, 'findOne' | 'update'> {
  constructor(
    protected readonly repository: Repository<T>,
    private readonly citizenService: CitizenService,
  ) { }

  abstract findOne(id: number): Promise<ResponseFindOne<T | null>>;

  async create(dto: Omit<T, 'id'>, process: Pick<Process, 'id'>): Promise<ResponseCreated<T>> {
    if (dto.toDate && dto.fromDate > dto.toDate) {
      throw new InvalidException<CitizenProcess>("toDate");
    }

    const citizenFindOne = await this.citizenService.findOne(dto.citizen.id);

    if (!citizenFindOne.data) {
      throw new NotFoundException<CitizenProcess>("citizen");
    }

    const citizenProcesses = citizenFindOne.data.citizenProcesses;

    const currentCitizenProcess = citizenProcesses.length ? citizenProcesses[0] : undefined;

    if (!currentCitizenProcess) {
      throw new InvalidException<CitizenProcess>("process");
    }

    const nextProcesses: Pick<Process, 'id'>[] = currentCitizenProcess?.process.nextProcesses;

    // Check next process có chứa process muốn thêm vào không
    const isInclude = nextProcesses.map((e) => e.id).includes(process.id);
    if (!isInclude) {
      throw new InvalidException<CitizenProcess>("process");
    }

    if (currentCitizenProcess.toDate && dto.fromDate <= currentCitizenProcess.toDate || dto.fromDate <= currentCitizenProcess.fromDate) {
      throw new InvalidException<CitizenProcess>("fromDate");
    }

    if (!currentCitizenProcess.toDate) {
      currentCitizenProcess.toDate = new Date(dto.fromDate);
      currentCitizenProcess.toDate.setDate(currentCitizenProcess.toDate.getDate() - 1);
    }

    await this.citizenService.update(citizenFindOne.data.id, citizenFindOne.data);

    const result = await this.repository.save({
      ...dto,
      process,
    } as T);

    return new ResponseCreated(result);
  }

  async update(id: number, dto: Partial<Omit<T, 'id'>>): Promise<ResponseUpdate> {
    await this.validateDate(id, dto);

    const result = await this.repository.save({
      id,
      ...dto,
    } as T);

    return new ResponseUpdate(result);
  }

  /**
   * Hàm này dùng để kiểm tra fromDate có hợp lệ hay không
   * Ex: Công dân có 3 quá trình là 1, 2, 3.
   * Bây h muốn sửa fromDate của quá trình 2,
   * thì phải kiểm tra fromDate của quá trình 2 phải lớn hơn toDate của quá trình 1 và nhỏ hơn fromDate của quá trình 3
   * @param citizenProcessId id của citizenProcess
   * @param fromDate là ngày bắt đầu của process (quá trình) đó
   */
  private async validateDate(citizenProcessId: number, dto: Partial<Omit<T, 'id'>>) {
    const currentCitizenProcess = await this.repository.findOne({
      where: {
        id: citizenProcessId,
      } as FindOptionsWhere<T>,
      relations: {
        citizen: true,
      } as FindOptionsRelations<T>,
    });

    if (!currentCitizenProcess) throw new Error();

    const citizenFindOne = await this.citizenService.findOne(currentCitizenProcess.citizen.id);

    if (!citizenFindOne.data) throw new Error();

    const citizenProcesses = citizenFindOne.data.citizenProcesses;

    const currentIndex = citizenProcesses.findIndex((e) => e.id == citizenProcessId);

    const preCitizenProcess = currentIndex > 0 ? citizenProcesses[currentIndex - 1] : undefined;

    const nextCitizenProcess = currentIndex < citizenProcesses.length - 1 ? citizenProcesses[currentIndex + 1] : undefined;

    if (dto.fromDate &&
      (
        (preCitizenProcess && preCitizenProcess.toDate && dto.fromDate <= preCitizenProcess?.toDate) ||
        (nextCitizenProcess && dto.fromDate >= nextCitizenProcess?.fromDate) ||
        (dto.toDate
          ? dto.fromDate > dto.toDate
          : currentCitizenProcess.toDate && dto.fromDate > currentCitizenProcess.toDate
        )
      )
    ) {
      throw new InvalidException<CitizenProcess>("fromDate");
    }

    if (dto.toDate &&
      (
        (preCitizenProcess && preCitizenProcess.toDate && dto.toDate <= preCitizenProcess?.toDate) ||
        (nextCitizenProcess && dto.toDate >= nextCitizenProcess?.fromDate) ||
        (dto.fromDate ? dto.toDate < dto.fromDate : dto.toDate < currentCitizenProcess.fromDate)
      )
    ) {
      throw new InvalidException<CitizenProcess>("toDate");
    }
  }
}
