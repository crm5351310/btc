import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { CitizenProcessCategoryModule } from './category/category.module';
import { CitizenProcessModule } from './citizen-process/citizen-process.module';

@Module({
  imports: [CitizenProcessModule, CitizenProcessCategoryModule],
})
export class CitizenProcessManagementModule {}
export const citizenProcessRootRoute: RouteTree = {
  path: 'citizen-process-management',
  module: CitizenProcessManagementModule,
  children: [CitizenProcessModule.route, CitizenProcessCategoryModule.route],
};
