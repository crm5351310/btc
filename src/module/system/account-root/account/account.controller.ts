import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { AccountDecorator } from '../../../../common/decorator/account.decorator';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { AuthGuard } from '../../../../common/guard/auth.guard';
import {
  ChangePasswordAccountDto,
  CreateAccountDto,
  FindManyAccountParamDto,
  UpdateAccountDto,
  UpdatePasswordAccountDto,
} from './account.dto';
import { Account } from './account.entity';
import { AccountService } from './account.service';

@Controller()
@ApiTags(TagEnum.ACCOUNT)
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post()
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  create(@Body() createAccountDto: CreateAccountDto) {
    return this.accountService.create(createAccountDto);
  }

  @Get()
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  findAll(@Query() query: FindManyAccountParamDto) {
    return this.accountService.findAll(query);
  }

  @Get(':id')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  findOne(@Param() path: PathDto) {
    return this.accountService.findOne(path);
  }

  @Patch(':id/info')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  update(@Param() path: PathDto, @Body() updateAccountDto: UpdateAccountDto) {
    return this.accountService.update(path, updateAccountDto);
  }

  @Patch('update-password')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  updatePassword(
    @Body() updatePasswordAccountDto: UpdatePasswordAccountDto,
    @AccountDecorator() account: Account,
  ) {
    return this.accountService.updatePassword(updatePasswordAccountDto, account);
  }

  @Get(':id/generate-password')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  generatePassword(@Param('id') id: number) {
    return this.accountService.generatePassword(id);
  }

  @Patch(':id/confirm-password')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  changePassword(
    @Param() path: PathDto,
    @Body() changePasswordAccountDto: ChangePasswordAccountDto,
  ) {
    return this.accountService.confirmPassword(path, changePasswordAccountDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  remove(@Param() path: PathArrayDto) {
    return this.accountService.remove(path);
  }
}
