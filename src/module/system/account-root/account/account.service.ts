import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Like, Repository } from 'typeorm';
import { BaseService, PathArrayDto, PathDto } from '../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Pagination } from '../../../../common/utils/pagination.util';
import { DepartmentService } from '../../department-root/department/department.service';
import { RoleService } from '../../role/role.service';
import { AccountPasswordService } from '../account-password/account-password.service';
import {
  ChangePasswordAccountDto,
  CreateAccountDto,
  FindManyAccountParamDto,
  UpdateAccountDto,
  UpdatePasswordAccountDto,
} from './account.dto';
import { Account } from './account.entity';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

@Injectable()
export class AccountService extends BaseService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    private readonly roleService: RoleService,
    private readonly departmentService: DepartmentService,
    private readonly accountPasswordService: AccountPasswordService,
    @Inject(CACHE_MANAGER) private cacheService: Cache,
  ) {
    super();
    this.name = AccountService.name;
    this.subject = SubjectMessage.ACCOUNT;
  }
  async create(createAccountDto: CreateAccountDto) {
    this.action = BaseMessage.CREATE;

    const checkExistUsername = await this.accountRepository.findOne({
      where: {
        username: createAccountDto.username,
      },
    });
    if (checkExistUsername)
      throw new CustomBadRequestException(ContentMessage.EXIST, FieldMessage.USERNAME, false);

    const checkPassword = this.accountPasswordService.checkSpecificCharacterInString(
      createAccountDto.password,
    );
    if (!checkPassword)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.PASSWORD, false);

    // check exist department
    await this.departmentService.findOne(createAccountDto.department);

    // check exist role
    await this.roleService.findOne(createAccountDto.role);

    const { password, ...dto } = createAccountDto;
    const data = await this.accountRepository.save({
      ...dto,
    });
    await this.accountPasswordService.insertPassword(data.id, password);
    return this.response(data);
  }

  async findAll(query: FindManyAccountParamDto) {
    this.action = BaseMessage.READ;
    const result = await this.accountRepository.findAndCount({
      skip: query.offset || undefined,
      take: query.limit || undefined,
      where: {
        username: query.name ? Like(`%${query.name}%`) : undefined,
        department: {
          id: query.departmentIds ? In(query.departmentIds) : undefined,
        },
        role: {
          id: query.roleIds ? In(query.roleIds) : undefined,
        },
      },
      relations: {
        role: true,
        department: true,
      },
    });
    return this.response(new Pagination<Account>(result[0], result[1]));
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;
    const result = await this.accountRepository.findOne({
      where: {
        id: path.id,
      },
      relations: {
        role: true,
        department: true,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ACCOUNT, true);
    return this.response(result);
  }

  async update(path: PathDto, updateAccountDto: UpdateAccountDto) {
    this.action = BaseMessage.UPDATE;

    const checkExistAccount = await this.accountRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!checkExistAccount)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ACCOUNT, true);

    if (updateAccountDto.department)
      await this.departmentService.findOne(updateAccountDto.department);

    if (updateAccountDto.role) await this.roleService.findOne(updateAccountDto.role);

    const data = await this.accountRepository.save({
      id: path.id,
      ...updateAccountDto,
    });
    return this.response(data);
  }

  async updatePassword(updatePasswordAccountDto: UpdatePasswordAccountDto, account: Account) {
    this.action = BaseMessage.UPDATE;

    // kiểm tra password mới có đúng định dạng không
    const checkPassword = this.accountPasswordService.checkSpecificCharacterInString(
      updatePasswordAccountDto.newPassword,
    );
    if (!checkPassword)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.PASSWORD, false);

    // check tài khoản tồn tại không?
    const checkExistAccount = await this.accountRepository.findOne({
      where: {
        id: account.id,
      },
      relations: {
        accountPassword: true,
      },
    });

    // kiểm tra passowrd cũ có đúng không?
    const verifyOldPassword = await this.accountPasswordService.verifyPassword(
      updatePasswordAccountDto.oldPassword,
      checkExistAccount?.accountPassword?.password ?? '',
    );

    if (!verifyOldPassword)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.OLD_PASSWORD, false);

    // cập nhật mật khẩu mới cho tài khoản
    await this.accountPasswordService.updatePassword(
      account.id,
      updatePasswordAccountDto.newPassword,
    );
    return this.response(checkExistAccount);
  }

  async generatePassword(id: number) {
    // check tồn tại tài khoản sinh password
    await this.findOne({ id: id });

    const password = this.accountPasswordService.generatePassword(8);

    // set cache trong 5 phút
    await this.cacheService.set(
      `${password}`,
      {
        id: id,
      },
      {
        ttl: 5 * 60 * 1000,
      },
    );

    return this.response({
      password: password,
    });
  }

  async confirmPassword(account: PathDto, changePasswordAccountDto: ChangePasswordAccountDto) {
    this.action = BaseMessage.UPDATE;

    // check tài khoản tồn tại không?
    const checkExistAccount = await this.accountRepository.findOne({
      where: {
        id: account.id,
      },
    });
    if (!checkExistAccount)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ACCOUNT, true);

    const checkExistGeneratedPassword: any = await this.cacheService.get(
      `${changePasswordAccountDto.password}`,
    );

    if (!checkExistGeneratedPassword || checkExistGeneratedPassword?.id != account.id)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.PASSWORD, true);

    // hash password và lưu lại mật khẩu mới
    await this.accountPasswordService.updatePassword(account.id, changePasswordAccountDto.password);

    await this.cacheService.del(`${changePasswordAccountDto.password}`);

    return this.response(checkExistAccount);
  }

  async remove(path: PathArrayDto) {
    this.action = BaseMessage.DELETE;
    const pathArray = path.id.split(',');

    const checkExistAccount = await this.accountRepository.find({
      where: {
        id: In(pathArray),
      },
    });
    if (checkExistAccount.length !== pathArray.length)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ACCOUNT, true);

    await this.accountRepository.softRemove(checkExistAccount);
    return this.response(checkExistAccount);
  }
}
