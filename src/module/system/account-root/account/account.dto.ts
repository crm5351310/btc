import { ApiProperty, IntersectionType, OmitType, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional, MaxLength, MinLength } from 'class-validator';
import { Account } from './account.entity';
import { AccountPassword } from '../account-password/account-password.entity';

export class CreateAccountDto extends IntersectionType(
  OmitType(Account, ['id'] as const),
  PickType(AccountPassword, ['password'] as const),
) {}

export class UpdateAccountDto extends OmitType(CreateAccountDto, [
  'username',
  'password',
] as const) {}

export class UpdatePasswordAccountDto {
  @ApiProperty({
    description: 'Mật khẩu cũ',
    default: '12345678',
  })
  @IsNotEmpty()
  @MaxLength(20)
  @MinLength(6)
  oldPassword: string;

  @ApiProperty({
    description: 'Mật khẩu mới',
    default: '12345678',
  })
  @IsNotEmpty()
  @MaxLength(20)
  @MinLength(6)
  newPassword: string;
}

export class ChangePasswordAccountDto extends PickType(AccountPassword, ['password'] as const) {}

export class FindManyAccountParamDto {
  @ApiProperty({
    name: 'offset',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  offset: number;

  @ApiProperty({
    name: 'limit',
    required: false,
  })
  @IsOptional()
  @IsNumber()
  limit: number;

  @ApiProperty({
    name: 'name',
    required: false,
  })
  @IsOptional()
  name: string;

  @ApiProperty({
    name: 'roleIds',
    required: false,
    type: [Number],
  })
  @IsOptional()
  @Transform(({ value }) => {
    if (typeof value == 'string') {
      value = [value];
    }
    return value;
  })
  roleIds: number[];

  @ApiProperty({
    name: 'departmentIds',
    required: false,
    type: [Number],
  })
  @IsOptional()
  @Transform(({ value }) => {
    if (typeof value == 'string') {
      value = [value];
    }
    return value;
  })
  departmentIds: number[];
}
