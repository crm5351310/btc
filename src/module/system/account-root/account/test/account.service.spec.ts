import { faker } from '@faker-js/faker';
import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import { Repository } from 'typeorm';
import { PathArrayDto, PathDto } from '../../../../../common/base/class/base.class';
import { CustomBadRequestException } from '../../../../../common/exception/bad.exception';
import { dataSourceOptions } from '../../../../../config/data-source.config';
import { DepartmentService } from '../../../../../module/system/department-root';
import { RoleService } from '../../../../../module/system/role/role.service';
import { AccountPasswordService } from '../../account-password/account-password.service';
import {
  ChangePasswordAccountDto,
  CreateAccountDto,
  FindManyAccountParamDto,
  UpdateAccountDto,
  UpdatePasswordAccountDto,
} from '../account.dto';
import { Account } from '../account.entity';
import { AccountModule } from '../account.module';
import { AccountService } from '../account.service';
// npm run test:run src/module/system/account-root
describe('AccountService', () => {
  let module: TestingModule;
  let accountService: AccountService;
  let accountRepository: Repository<Account>;
  let departmentServiceMock: jest.Mocked<DepartmentService>;
  let roleServiceMock: jest.Mocked<RoleService>;
  let accountPasswordServiceMock: jest.Mocked<AccountPasswordService>;

  const ACCOUNT_REPOSITORY_TOKEN = getRepositoryToken(Account);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        AccountModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    accountRepository = module.get(ACCOUNT_REPOSITORY_TOKEN);
    accountService = module.get<AccountService>(AccountService);
    departmentServiceMock = module.get(DepartmentService);
    roleServiceMock = module.get(RoleService);
    accountPasswordServiceMock = module.get(AccountPasswordService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('AccountService => create', () => {
    const payload = {
      username: faker.internet.userName(),
      fullname: faker.person.fullName(),
      phoneNumber: '123456789',
      role: {
        id: 1,
      },
      department: {
        id: 1,
      },
      password: faker.internet.password({
        length: 8,
      }),
    } as CreateAccountDto;

    it('Tạo mới tài khoản thành công', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(departmentServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(roleServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(accountPasswordServiceMock, 'insertPassword').mockResolvedValue({} as any);

      jest.spyOn(accountRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Account);

      const result = await accountService.create(payload);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).toHaveBeenCalled();
      expect(roleServiceMock.findOne).toHaveBeenCalled();
      expect(accountRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới tài khoản không thành công nếu tên tài khoản tồn tại', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue({ id: 1 } as Account);

      await expect(accountService.create(payload)).rejects.toThrowError();

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).not.toHaveBeenCalled();
      expect(roleServiceMock.findOne).not.toHaveBeenCalled();
      expect(accountRepository.save).not.toHaveBeenCalled();
    });

    it('Tạo mới tài khoản không thành công nếu đơn vị quản lý không tồn tại', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(null);
      jest
        .spyOn(departmentServiceMock, 'findOne')
        .mockRejectedValue(new CustomBadRequestException('', '', false));

      await expect(accountService.create(payload)).rejects.toThrowError(CustomBadRequestException);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).toHaveBeenCalled();
      expect(roleServiceMock.findOne).not.toHaveBeenCalled();
      expect(accountRepository.save).not.toHaveBeenCalled();
    });

    it('Tạo mới tài khoản không thành công nếu nhóm quyền không tồn tại', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(departmentServiceMock, 'findOne').mockResolvedValue({} as any);
      jest
        .spyOn(roleServiceMock, 'findOne')
        .mockRejectedValue(new CustomBadRequestException('', '', false));

      await expect(accountService.create(payload)).rejects.toThrowError(CustomBadRequestException);
      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).toHaveBeenCalled();
      expect(roleServiceMock.findOne).toHaveBeenCalled();
      expect(accountRepository.save).not.toHaveBeenCalled();
    });
  });

  describe('AccountService => findOne', () => {
    it('Tìm một tài khoản thành công', async () => {
      const mockData = { id: 1, username: 'nam' } as Account;
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(mockData);

      const result = await accountService.findOne({ id: 1 } as PathDto);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một tài khoản nếu ID không tồn tại', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(null);
      await expect(accountService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('AccountService => findAll', () => {
    it('Tìm danh sách khoản thành công', async () => {
      const mockData = [
        { id: 1, username: 'user1' },
        { id: 2, username: 'user2' },
        { id: 3, username: 'user2' },
      ] as Account[];
      jest.spyOn(accountRepository, 'findAndCount').mockResolvedValue([mockData, mockData.length]);

      const result = await accountService.findAll({} as FindManyAccountParamDto);

      expect(accountRepository.findAndCount).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data.result).toMatchObject(mockData);
      expect(result.data.result.length).toBe(mockData.length);
    });
  });

  describe('AccountService => update', () => {
    it('Cập nhật trường họ tên tài khoản thành công', async () => {
      const payload = {
        fullname: faker.person.fullName(),
      } as UpdateAccountDto;

      jest.spyOn(accountRepository, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(accountRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Account);

      const result = await accountService.update({ id: 1 }, payload);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).not.toHaveBeenCalled();
      expect(roleServiceMock.findOne).not.toHaveBeenCalled();
      expect(accountRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật trường số điện thoại của tài khoản thành công', async () => {
      const payload = {
        phoneNumber: '123456789',
      } as UpdateAccountDto;

      jest.spyOn(accountRepository, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(accountRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Account);

      const result = await accountService.update({ id: 1 }, payload);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).not.toHaveBeenCalled();
      expect(roleServiceMock.findOne).not.toHaveBeenCalled();
      expect(accountRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật đơn vị quản lý của tài khoản thành công', async () => {
      const payload = {
        department: {
          id: 1,
        },
      } as UpdateAccountDto;

      jest.spyOn(accountRepository, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(departmentServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(accountRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Account);

      const result = await accountService.update({ id: 1 }, payload);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(departmentServiceMock.findOne).toHaveBeenCalled();
      expect(roleServiceMock.findOne).not.toHaveBeenCalled();
      expect(accountRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật nhóm quyền của tài khoản thành công', async () => {
      const payload = {
        role: {
          id: 1,
        },
      } as UpdateAccountDto;

      jest.spyOn(accountRepository, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(roleServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(accountRepository, 'save').mockResolvedValue({ id: 1, ...payload } as Account);

      const result = await accountService.update({ id: 1 }, payload);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(roleServiceMock.findOne).toHaveBeenCalled();
      expect(accountRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });
  });

  describe('AccountService => remove', () => {
    it('Xoá một tài khoản thành công', async () => {
      const mockData = { id: 1 } as Account;
      jest.spyOn(accountRepository, 'find').mockResolvedValue([mockData]);
      jest.spyOn(accountRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await accountService.remove({ id: '1' } as PathArrayDto);

      expect(accountRepository.find).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một tài khoản nếu ID không tồn tại', async () => {
      jest.spyOn(accountRepository, 'find').mockResolvedValue([]);
      await expect(accountService.remove({ id: '1' })).rejects.toThrowError();
    });
  });

  describe('AccountService => updatePassword', () => {
    const mockData = {
      id: 1,
      username: 'admin@bitecco.vn',
      accountPassword: {
        password: '12345678',
      },
    } as Account;
    const payload = {
      oldPassword: 'old pass',
      newPassword: 'new pass',
    } as UpdatePasswordAccountDto;

    it('Cập nhật mật khẩu thành công', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(mockData);
      jest.spyOn(accountPasswordServiceMock, 'verifyPassword').mockResolvedValue(true);
      jest.spyOn(accountPasswordServiceMock, 'updatePassword').mockResolvedValue({} as any);
      const result = await accountService.updatePassword(payload, {
        id: 1,
      } as Account);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(accountPasswordServiceMock.verifyPassword).toHaveBeenCalled();
      expect(accountPasswordServiceMock.updatePassword).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Cập nhật mật khẩu không thành công mật khẩu cũ không đúng', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(mockData);
      jest.spyOn(accountPasswordServiceMock, 'verifyPassword').mockResolvedValue(false);
      jest.spyOn(accountPasswordServiceMock, 'updatePassword').mockResolvedValue({} as any);

      await expect(
        accountService.updatePassword(payload, {
          id: 1,
        } as Account),
      ).rejects.toThrowError();
    });

    it('Cập nhật mật khẩu không thành công nếu tài khoản không tồn tại', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(null);
      await expect(
        accountService.updatePassword(payload, {
          id: 1,
        } as Account),
      ).rejects.toThrowError();
    });
  });

  describe('AccountService => changePassword', () => {
    const mockData = {
      id: 1,
      username: 'admin@bitecco.vn',
    } as Account;
    const payload = {
      password: '12345678',
    } as ChangePasswordAccountDto;

    it('Thay đổi mật khẩu thành công', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(mockData);
      jest.spyOn(accountPasswordServiceMock, 'updatePassword').mockResolvedValue({} as any);
      const result = await accountService.changePassword({ id: mockData.id }, payload);
      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(accountPasswordServiceMock.updatePassword).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });
  });
});
