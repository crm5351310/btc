import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional, ValidateNested } from 'class-validator';
import { RelationTypeBase } from '../../../../common/base/class/base.class';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Role } from '../../role/role.entity';
import { AccountPassword } from '../account-password/account-password.entity';
import { Department } from '../../department-root/department/department.entity';

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: 'created_at', nullable: true })
  createdAt: Date;

  @Column('int', { name: 'created_by', nullable: true })
  createdBy: string;

  @UpdateDateColumn({ name: 'updated_at', nullable: true })
  updatedAt: Date;

  @Column('int', { name: 'updated_by', nullable: true })
  updatedBy: number;

  @DeleteDateColumn({ name: 'deleted_at', nullable: true })
  deletedAt?: Date;

  @ApiProperty({
    description: 'Tên tài khoản',
    default: 'admin@bitecco.vn',
  })
  @IsNotEmpty()
  @Column('varchar', { length: 100, unique: true, nullable: false })
  username: string;

  @ApiProperty({
    description: 'Id cấp hành chính',
    default: 'admin',
  })
  @IsNotEmpty()
  @Column('varchar', { length: 100, nullable: false })
  fullname: string;

  @ApiProperty({
    description: 'Số điện thoại',
    default: '0000000000',
  })
  @IsOptional()
  @IsNumber()
  @Column('varchar', { length: 15, nullable: true })
  phoneNumber: string;

  @ApiProperty({
    description: 'Id nhóm quyền',
    type: RelationTypeBase,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(Role, ['id']))
  @ManyToOne(() => Role)
  @JoinColumn()
  role: Role;

  @OneToOne(() => AccountPassword, (item) => item.account)
  accountPassword: AccountPassword;

  @ApiProperty({
    description: 'Id của đơn vị quản lý',
    type: RelationTypeBase,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(Department, ['id']))
  @ManyToOne(() => Department, (item) => item.accounts)
  @JoinColumn()
  department: Department;

  accessTokenKey: string;

  refreshTokenKey: string;
}
