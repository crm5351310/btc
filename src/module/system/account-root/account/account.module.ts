import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Department } from '../../department-root/department/department.entity';
import { Role } from '../../role/role.entity';
import { AccountPassword } from '../account-password/account-password.entity';
import { AccountController } from './account.controller';
import { Account } from './account.entity';
import { AccountService } from './account.service';
import { RoleModule } from '../../role/role.module';
import { DepartmentModule } from '../../department-root';
import { AccountPasswordModule } from '../account-password';

@Module({
  imports: [
    TypeOrmModule.forFeature([Department, Account, Role, AccountPassword]),
    JwtModule,
    RoleModule,
    DepartmentModule,
    AccountPasswordModule,
  ],
  controllers: [AccountController],
  providers: [AccountService],
  exports: [AccountService],
})
export class AccountModule {}

export const accountRouter: RouteTree = {
  path: 'account',
  module: AccountModule,
  children: [],
};
