import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { BaseService } from '../../../../common/base/class/base.class';
import { AccountPassword } from '../account-password/account-password.entity';
import { CustomBadRequestException } from 'src/common/exception/bad.exception';
import { ContentMessage } from 'src/common/message/content.message';
import { FieldMessage } from 'src/common/message/field.message';

@Injectable()
export class AccountPasswordService extends BaseService {
  constructor(
    @InjectRepository(AccountPassword)
    private readonly accountPasswordRepository: Repository<AccountPassword>,
  ) {
    super();
    this.name = AccountPasswordService.name;
  }

  private async hasingPassword(password: string): Promise<string> {
    const salt = await bcrypt.genSalt();
    const newPassword = await bcrypt.hash(password, salt);
    return newPassword;
  }

  async verifyPassword(password: string, passwordHash: string): Promise<boolean> {
    const verifyPassword = await bcrypt.compare(password, passwordHash);
    return verifyPassword;
  }

  async insertPassword(accountId: number, password: string) {
    const hashingPassword = await this.hasingPassword(password);
    await this.accountPasswordRepository.insert({
      password: hashingPassword,
      account: {
        id: accountId,
      },
    });
  }

  checkSpecificCharacterInString(input: string): boolean {
    return (
      // check có chữ hoa
      /[A-Z]/.test(input) &&
      // check có chữ thườn
      /[a-z]/.test(input) &&
      // check có ký tự đặc biệt
      /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(input) &&
      // check có số
      /\d/.test(input) &&
      // check không được gen
      !/\s/.test(input)
    );
  }

  generatePassword(length: number): string {
    const uppercaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const lowercaseChars = 'abcdefghijklmnopqrstuvwxyz';
    const specialChars = '!@#$%^&*()_+-=[]{};:\\"|,.<>/?';
    const numberChars = '0123456789';

    let result = '';

    // Chắc chắn có ít nhất một ký tự trong mỗi loại
    result += uppercaseChars[Math.floor(Math.random() * uppercaseChars.length)];
    result += lowercaseChars[Math.floor(Math.random() * lowercaseChars.length)];
    result += specialChars[Math.floor(Math.random() * specialChars.length)];
    result += numberChars[Math.floor(Math.random() * numberChars.length)];

    const remainingLength = length - 4; // 4 là tổng số ký tự đã thêm

    // Tạo chuỗi ngẫu nhiên với ký tự còn lại
    for (let i = 0; i < remainingLength; i++) {
      const charSet = Math.floor(Math.random() * 4); // Chọn một trong bốn loại ký tự
      switch (charSet) {
        case 0:
          result += uppercaseChars[Math.floor(Math.random() * uppercaseChars.length)];
          break;
        case 1:
          result += lowercaseChars[Math.floor(Math.random() * lowercaseChars.length)];
          break;
        case 2:
          result += specialChars[Math.floor(Math.random() * specialChars.length)];
          break;
        case 3:
          result += numberChars[Math.floor(Math.random() * numberChars.length)];
          break;
      }
    }

    // Trộn lại chuỗi
    result = result
      .split('')
      .sort(() => Math.random() - 0.5)
      .join('');

    return result;
  }

  async updatePassword(accountId: number, password: string) {
    const checkExistAccountPassword = await this.accountPasswordRepository.findOne({
      where: {
        account: {
          id: accountId,
        },
      },
    });
    if (!checkExistAccountPassword)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ACCOUNT, true);
    const hashingPassword = await this.hasingPassword(password);
    checkExistAccountPassword.password = hashingPassword;
    await this.accountPasswordRepository.save(checkExistAccountPassword);
  }
}
