import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { Account } from '../account/account.entity';

@Entity()
export class AccountPassword extends BaseEntity {
  @ApiProperty({
    description: 'password',
    default: '12345678',
  })
  @IsNotEmpty()
  @MaxLength(20)
  @MinLength(8)
  @Column('varchar', { length: 60, nullable: true })
  password: string;

  @OneToOne(() => Account, (item) => item.accountPassword)
  @JoinColumn()
  account: Account;
}
