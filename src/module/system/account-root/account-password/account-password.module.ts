import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountPassword } from './account-password.entity';
import { AccountPasswordService } from './account-password.service';

@Module({
  imports: [TypeOrmModule.forFeature([AccountPassword])],
  controllers: [],
  providers: [AccountPasswordService],
  exports: [AccountPasswordService],
})
export class AccountPasswordModule {}
