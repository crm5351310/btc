import { Body, Controller, Get, Patch } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { ProfileService } from './profile.service';
import { AccountDecorator } from '../../../../common/decorator/account.decorator';
import { Account } from '../account';
import { ProfileDto } from './profile.dto';

@Controller()
@ApiTags(TagEnum.PROFILE)
export class profileController {
  constructor(private readonly profileService: ProfileService) {}
  @Get()
  findOne(@AccountDecorator() account: Account) {
    return this.profileService.findOne(account);
  }

  @Patch()
  update(@AccountDecorator() account: Account, @Body() payload: ProfileDto) {
    return this.profileService.update(account, payload);
  }
}
