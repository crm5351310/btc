import { PickType } from '@nestjs/swagger';
import { Account } from '../account';

export class ProfileDto extends PickType(Account, ['fullname', 'phoneNumber'] as const) {}
