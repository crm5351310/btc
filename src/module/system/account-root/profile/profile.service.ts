import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseService } from '../../../../common/base/class/base.class';
import { BaseMessage } from '../../../../common/message/base.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { Account } from '../account';
import { ProfileDto } from './profile.dto';

@Injectable()
export class ProfileService extends BaseService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
  ) {
    super();
    this.name = ProfileService.name;
    this.subject = SubjectMessage.ACCOUNT;
  }

  async findOne(account: Account) {
    this.action = BaseMessage.READ;
    const result = await this.accountRepository.findOne({
      select: ['username', 'fullname', 'phoneNumber'],
      where: {
        username: account.username,
      },
    });

    return this.response(result);
  }

  async update(account: Account, payload: ProfileDto) {
    this.action = BaseMessage.UPDATE;
    const profile = await this.accountRepository.findOne({
      where: {
        username: account.username,
      },
    });
    const result = await this.accountRepository.save({
      id: profile!.id,
      ...payload,
    });
    return this.response(result);
  }
}
