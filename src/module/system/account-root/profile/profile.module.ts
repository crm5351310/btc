import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from '../account';
import { ProfileService } from './profile.service';
import { profileController } from './profile.controller';
@Module({
  imports: [TypeOrmModule.forFeature([Account]), JwtModule],
  controllers: [profileController],
  providers: [ProfileService],
  exports: [ProfileService],
})
export class ProfileModule {}

export const profileRouter: RouteTree = {
  path: 'profile',
  module: ProfileModule,
  children: [],
};
