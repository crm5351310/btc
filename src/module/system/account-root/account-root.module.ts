import { Module } from '@nestjs/common';
import { AccountModule, accountRouter } from './account/account.module';
import { AccountPasswordModule } from './account-password/account-password.module';
import { RouteTree } from '@nestjs/core';
import { ProfileModule, profileRouter } from './profile/profile.module';

@Module({
  imports: [AccountModule, AccountPasswordModule, ProfileModule],
  controllers: [],
  providers: [],
})
export class AccountRootModule {}
export const accountRootRouter: RouteTree = {
  path: 'account-root',
  module: AccountModule,
  children: [accountRouter, profileRouter],
};
