import { OmitType, PartialType } from '@nestjs/swagger';
import { Department } from './department.entity';

export class CreateDepartmentDto extends OmitType(Department, ['id'] as const) {}

export class UpdateDepartmentDto extends PartialType(CreateDepartmentDto) {}
