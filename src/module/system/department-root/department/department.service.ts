import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AdministrativeUnitService } from '../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { DeepPartial, FindOptionsWhere, Not, Repository, TreeRepository } from 'typeorm';
import { BaseService, PathDto } from '../../../../common/base/class/base.class';
import { AdministrativeLevelEnum } from '../../../../common/enum/administrative-level.enum';
import { CustomBadRequestException, CustomBadRequestExceptionCustom } from '../../../../common/exception/bad.exception';
import { BaseMessage } from '../../../../common/message/base.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { StringHelper } from '../../../../common/utils/string.util';
import { RegionalClassificationService } from '../regional-classification';
import { CreateDepartmentDto, UpdateDepartmentDto } from './department.dto';
import { Department } from './department.entity';

@Injectable()
export class DepartmentService extends BaseService {
  constructor(
    @InjectRepository(Department)
    private readonly departmentTreeRepository: TreeRepository<Department>,
    @InjectRepository(Department)
    private readonly departmentRepository: Repository<Department>,
    private readonly regionalClassificationService: RegionalClassificationService,
    private readonly administrativeUnitService: AdministrativeUnitService,
  ) {
    super();
    this.name = DepartmentService.name;
    this.subject = SubjectMessage.FOCAL_AREA_CLASSIFICATION;
  }

  protected async checkExist(
    properties: (keyof DeepPartial<Department>)[],
    dto: DeepPartial<Department>,
    id?: number,
  ): Promise<void> {
    const errors: string[] = [];

    await Promise.all(
      properties.map(async (property) => {
        if (!dto[property]) return;
        const isExist = await this.departmentRepository.exist({
          where: {
            id: id ? Not(id) : undefined,
            [property]: dto[property],
          } as FindOptionsWhere<Department>,
        });
        if (isExist) {
          errors.push(`${property.toString()}.${ContentMessage.EXIST}`);
        }
      }),
    );

    if (errors.length > 0) {
      throw new CustomBadRequestExceptionCustom(errors);
    }
  }
  
  async create(createDepartmentDto: CreateDepartmentDto) {
    this.action = BaseMessage.CREATE;
    createDepartmentDto = StringHelper.spaceObject(createDepartmentDto);

    await this.checkExist(['code', 'name'], createDepartmentDto);

    // check administrative unit
    await this.administrativeUnitService.findOne(createDepartmentDto.administrativeUnit);

    // check regional classification
    await this.regionalClassificationService.findOne(createDepartmentDto.regionalClassification.id);

    const result = await this.departmentRepository.save(createDepartmentDto);
    return this.response(result);
  }

  async findAll() {
    this.action = BaseMessage.READ;

    const result = await this.departmentTreeRepository.findTrees({
      relations: ['administrativeUnit', 'regionalClassification', 'parent'],
    });
    return this.response(result);
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;

    const result = await this.departmentRepository.findOne({
      where: {
        id: path.id,
      },
      relations: {
        administrativeUnit: {
          parent: {
            parent: true,
          },
          administrativeTitle: {
            administrativeLevel: true,
          },
        },
        regionalClassification: true,
        parent: true,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.DEPARTMENT, true);
    result.administrativeLevel =
      result.administrativeUnit?.administrativeTitle?.administrativeLevel;
    switch (result.administrativeLevel?.code) {
      case AdministrativeLevelEnum.XA:
        result.commune = result.administrativeUnit;
        result.district = result.administrativeUnit?.parent;
        break;
      case AdministrativeLevelEnum.HUYEN:
        result.district = result.administrativeUnit;
        break;
      case AdministrativeLevelEnum.TINH:
        break;
    }
    return this.response(result);
  }

  async update(path: PathDto, updateDepartmentDto: UpdateDepartmentDto) {
    this.action = BaseMessage.UPDATE;

    await this.checkExist(['name', 'code'], updateDepartmentDto, path.id);

    // check administrative unit
    if (updateDepartmentDto.administrativeUnit) {
      await this.administrativeUnitService.findOne(updateDepartmentDto.administrativeUnit);
    }

    // check regional classification
    if (updateDepartmentDto.regionalClassification) {
      await this.regionalClassificationService.findOne(
        updateDepartmentDto.regionalClassification.id,
      );
    }

    const result = await this.departmentRepository.save({
      id: path.id,
      ...updateDepartmentDto,
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.DEPARTMENT, true);
    return this.response(result);
  }

  async remove(path: PathDto) {
    this.action = BaseMessage.DELETE;

    const result = await this.departmentRepository.findOne({
      where: {
        id: path.id,
      },
      relations: {
        children: true,
      },
    });
    if (!result)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.DEPARTMENT, true);
    if (result.children.length !== 0)
      throw new CustomBadRequestException(ContentMessage.IN_USED, FieldMessage.DEPARTMENT, true);
    await this.departmentRepository.softDelete(path.id);
    return this.response(result);
  }
}
