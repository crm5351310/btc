import { CacheModule } from '@nestjs/cache-manager';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import { FindOneOptions, Repository, TreeRepository } from 'typeorm';
import { dataSourceOptions } from '../../../../../config/data-source.config';
import {
  DepartmentModule,
  DepartmentService,
  RegionalClassificationService,
} from '../../../../../module/system/department-root';
import { AdministrativeUnitService } from '../../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.service';
import { CreateDepartmentDto, UpdateDepartmentDto } from '../department.dto';
import { Department } from '../department.entity';
import * as _ from 'lodash';

// npm run test:run src/module/system/department-root
describe('DepartmentService', () => {
  let module: TestingModule;
  let departmentService: DepartmentService;
  let departmentRepository: Repository<Department>;
  let departmentTreeRepository: TreeRepository<Department>;
  let regionalClassificationServiceMock: Repository<RegionalClassificationService>;
  let administrativeUnitServiceMock: jest.Mocked<AdministrativeUnitService>;

  const DEPARTMENT_REPOSITORY_TOKEN = getRepositoryToken(Department);
  const DEPARTMENT_TREE_REPOSITORY_TOKEN = getRepositoryToken(Department);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        DepartmentModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
    }).compile();

    departmentRepository = module.get(DEPARTMENT_REPOSITORY_TOKEN);
    departmentTreeRepository = module.get(DEPARTMENT_TREE_REPOSITORY_TOKEN);
    departmentService = module.get<DepartmentService>(DepartmentService);
    regionalClassificationServiceMock = module.get(RegionalClassificationService);
    administrativeUnitServiceMock = module.get(AdministrativeUnitService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('DepartmentService => create', () => {
    const payload = {
      code: 'DON_VI_1',
      name: 'đơn vị quản lý 1',
      parent: {
        id: 1,
      },
      regionalClassification: {
        id: 1,
      },
      administrativeUnit: {
        id: 1,
      },
    } as CreateDepartmentDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Department>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Department>;

    it('Tạo mới đơn vị quản lý thành công', async () => {
      jest.spyOn(departmentRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(administrativeUnitServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(regionalClassificationServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(departmentRepository, 'save').mockResolvedValue({ id: 1, ...payload });

      const result = await departmentService.create(payload);

      expect(result.statusCode).toBe(201);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Tạo mới đơn vị quản lý không thành công nếu code hoặc tên đơn vị bị trùng', async () => {
      jest.spyOn(departmentRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as Department);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as Department);
        return Promise.resolve(null);
      });

      await expect(departmentService.create(payload)).rejects.toThrowError();

      expect(administrativeUnitServiceMock.findOne).not.toHaveBeenCalled();
      expect(regionalClassificationServiceMock.findOne).not.toHaveBeenCalled();
    });
  });

  describe('DepartmentService => findOne', () => {
    it('Tìm một đơn vị quản lý thành công', async () => {
      const mockData = {
        id: 1,
        code: 'DON_VI_1',
        name: 'Đơn vị 1',
      } as Department;
      jest.spyOn(departmentRepository, 'findOne').mockResolvedValue(mockData);

      const result = await departmentService.findOne({ id: 1 });

      expect(departmentRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(mockData);
    });

    it('Không thể tìm một đơn vị quản lý nếu ID không tồn tại', async () => {
      jest.spyOn(departmentRepository, 'findOne').mockResolvedValue(null);

      await expect(departmentService.findOne({ id: 1 })).rejects.toThrowError();
    });
  });

  describe('DepartmentService => findAll', () => {
    it('Tìm danh sách đơn vị quản lý thành công', async () => {
      const mockData = [
        {
          id: 1,
          name: 'đơn vị 1',
          code: 'DON_VI_ROOT',
          children: [{ id: 2, name: 'Đơn vị 2', code: 'DON_VI_2' }],
        },
      ] as Department[];
      jest.spyOn(departmentTreeRepository, 'findTrees').mockResolvedValue(mockData);
      // const data = await
    });
  });

  describe('DepartmentService => update', () => {
    const payload = {
      code: 'DON_VI_1',
      name: 'đơn vị quản lý 1',
      regionalClassification: {
        id: 1,
      },
      administrativeUnit: {
        id: 1,
      },
      parent: {
        id: 1,
      },
    } as UpdateDepartmentDto;

    const codeQuery = {
      where: {
        code: payload.code,
      },
    } as FindOneOptions<Department>;

    const nameQuery = {
      where: {
        name: payload.name,
      },
    } as FindOneOptions<Department>;
    it('Cập nhật đơn vị quản lý thành công', async () => {
      jest.spyOn(departmentRepository, 'findOne').mockResolvedValue(null);
      jest.spyOn(administrativeUnitServiceMock, 'findOne').mockResolvedValue({} as any);
      jest.spyOn(regionalClassificationServiceMock, 'findOne').mockResolvedValue({} as any);
      jest
        .spyOn(departmentRepository, 'save')
        .mockResolvedValue({ id: 1, ...payload } as Department);

      const result = await departmentService.update({ id: 1 }, payload);

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toMatchObject(payload);
    });

    it('Cập nhật đơn vị quản lý thành công không thành công nếu code bị trùng của đơn vị khác', async () => {
      jest.spyOn(departmentRepository, 'findOne').mockImplementation((query) => {
        if (_.isEqual(query, codeQuery)) return Promise.resolve({ id: 1 } as Department);
        if (_.isEqual(query, nameQuery)) return Promise.resolve({ id: 1 } as Department);
        return Promise.resolve(null);
      });

      await expect(departmentService.update({ id: 1 }, payload)).rejects.toThrowError();
    });
  });

  describe('DepartmentService => remove', () => {
    it('Xoá một đơn vị quản lý thành công', async () => {
      const mockData = { id: 1 } as Department;
      jest.spyOn(departmentRepository, 'findOne').mockResolvedValue(mockData);
      jest.spyOn(departmentRepository, 'softRemove').mockResolvedValue(mockData);
      const result = await departmentService.remove({ id: 1 });

      expect(departmentRepository.findOne).toHaveBeenCalled();
      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
    });

    it('Không thể xoá một đơn vị quản lý nếu ID không tồn tại', async () => {
      jest.spyOn(departmentRepository, 'find').mockResolvedValue([]);
      await expect(departmentService.remove({ id: 1 })).rejects.toThrowError();
    });
  });
});
