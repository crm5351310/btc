import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { AdministrativeUnit } from '../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, ValidateNested } from 'class-validator';
import { Expose, Type } from 'class-transformer';
import { RelationTypeBase } from '../../../../common/base/class/base.class';
import { Account } from '../../account-root/account/account.entity';
import { RegionalClassification } from '../regional-classification/regional-classification.entity';

@Entity()
@Tree('nested-set')
export class Department extends BaseEntity {
  @ApiProperty({
    description: 'Mã của đơn vị quản lý',
    default: 'DON_VI',
  })
  @IsNotEmpty()
  @MaxLength(25)
  @Column('varchar', { length: 25, unique: true, nullable: false })
  code: string;

  @ApiProperty({
    description: 'Tên của đơn vị quản lý',
    default: 'Đơn vị số 1',
  })
  @IsNotEmpty()
  @MaxLength(100)
  @Column('varchar', { length: 100, unique: true, nullable: false })
  name: string;

  @TreeChildren()
  children: Department[];

  @ApiProperty({
    description: 'Đơn vị cha, nếu không có cha, truyền id = 1',
    type: RelationTypeBase,
  })
  @IsNotEmpty()
  @ValidateNested()
  @TreeParent()
  @Type(() => PickType(Department, ['id']))
  parent: Department;

  @ApiProperty({
    description: 'Phân cấp của đơn vị quản lý',
    type: RelationTypeBase,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(RegionalClassification, ['id']))
  @ManyToOne(() => RegionalClassification, (item) => item.departments)
  regionalClassification: RegionalClassification;

  @ApiProperty({
    description: 'Địa phận hành chính của đơn vị quản lý',
    type: RelationTypeBase,
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => PickType(AdministrativeUnit, ['id']))
  @ManyToOne(() => AdministrativeUnit, (item) => item.department)
  @JoinColumn()
  administrativeUnit: AdministrativeUnit;

  @OneToMany(() => Account, (item) => item.department)
  accounts: Account[];

  @Expose()
  administrativeLevel: any;

  @Expose()
  district: any;

  @Expose()
  commune: any;
}
