export * from './department.module';
export * from './department.service';
export * from './department.dto';
export * from './department.entity';
