import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { PathDto } from '../../../../common/base/class/base.class';
import { CreateDepartmentDto, UpdateDepartmentDto } from './department.dto';
import { DepartmentService } from './department.service';

@Controller()
@ApiTags(TagEnum.DEPARTMENT)
export class DepartmentController {
  constructor(private readonly departmentService: DepartmentService) {}

  @Post()
  create(@Body() createDepartmentDto: CreateDepartmentDto) {
    return this.departmentService.create(createDepartmentDto);
  }

  @Get()
  findAll() {
    return this.departmentService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.departmentService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateDepartmentDto: UpdateDepartmentDto) {
    return this.departmentService.update(path, updateDepartmentDto);
  }

  @Delete(':id')
  remove(@Param() path: PathDto) {
    return this.departmentService.remove(path);
  }
}
