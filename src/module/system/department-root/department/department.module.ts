import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrativeUnit } from '../../../category/administrative/administrative-unit-root/administrative-unit/administrative-unit.entity';
import { RegionalClassification } from '../regional-classification/regional-classification.entity';
import { DepartmentController } from './department.controller';
import { Department } from './department.entity';
import { DepartmentService } from './department.service';
import { RouteTree } from '@nestjs/core';
import { RegionalClassificationModule } from '../regional-classification';
import { AdministrativeUnitModule } from '../../../../module/category/administrative/administrative-unit-root/administrative-unit/administrative-unit.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Department]),
    RegionalClassificationModule,
    AdministrativeUnitModule,
  ],
  controllers: [DepartmentController],
  providers: [DepartmentService],
  exports: [DepartmentService],
})
export class DepartmentModule {}
export const departmentRouter: RouteTree = {
  path: 'department',
  module: DepartmentModule,
  children: [],
};
