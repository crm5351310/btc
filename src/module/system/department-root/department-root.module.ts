import { Module } from '@nestjs/common';
import { DepartmentModule, departmentRouter } from './department/department.module';
import {
  RegionalClassificationModule,
  regionalClassificationRouter,
} from './regional-classification/regional-classification.module';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [DepartmentModule, RegionalClassificationModule],
  controllers: [],
  providers: [],
})
export class DepartmentRootModule {}
export const departmentRootRouter: RouteTree = {
  path: 'department-root',
  module: DepartmentRootModule,
  children: [departmentRouter, regionalClassificationRouter],
};
