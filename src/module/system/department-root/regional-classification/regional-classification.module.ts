import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegionalClassificationController } from './regional-classification.controller';
import { RegionalClassification } from './regional-classification.entity';
import { RegionalClassificationService } from './regional-classification.service';

@Module({
  imports: [TypeOrmModule.forFeature([RegionalClassification])],
  controllers: [RegionalClassificationController],
  providers: [RegionalClassificationService],
  exports: [RegionalClassificationService],
})
export class RegionalClassificationModule {}
export const regionalClassificationRouter: RouteTree = {
  path: 'regional-classification',
  module: RegionalClassificationModule,
  children: [],
};
