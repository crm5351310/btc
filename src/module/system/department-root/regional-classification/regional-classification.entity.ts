import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../common/base/entity/base.entity';
import { Department } from '../department/department.entity';

@Entity()
export class RegionalClassification extends BaseEntity {
  @Column('varchar', { length: 25, unique: true, nullable: false })
  code: string;

  @Column('varchar', { length: 100, unique: true, nullable: false })
  name: string;

  @OneToMany(() => Department, (item) => item.regionalClassification)
  departments: Department[];
}
