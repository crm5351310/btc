import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../../common/enum/tag.enum';
import { RegionalClassificationService } from './regional-classification.service';

@Controller()
@ApiTags(TagEnum.REGIONAL_CLASSIFICATION)
export class RegionalClassificationController {
  constructor(private readonly regionalClassificationService: RegionalClassificationService) {}

  @Get()
  async findAll() {
    return await this.regionalClassificationService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: number) {
    return await this.regionalClassificationService.findOne(id);
  }
}
