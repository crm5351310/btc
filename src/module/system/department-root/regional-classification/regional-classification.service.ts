import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '../../../../common/base/class/base.class';
import { BaseMessage } from '../../../../common/message/base.message';
import { Repository } from 'typeorm';
import { RegionalClassification } from './regional-classification.entity';
import { SubjectMessage } from '../../../../common/message/subject.message';
import { ContentMessage } from '../../../../common/message/content.message';
import { FieldMessage } from '../../../../common/message/field.message';
import { CustomBadRequestException } from '../../../../common/exception/bad.exception';

@Injectable()
export class RegionalClassificationService extends BaseService {
  constructor(
    @InjectRepository(RegionalClassification)
    private readonly regionalClassificationRepository: Repository<RegionalClassification>,
  ) {
    super();
    this.name = RegionalClassificationService.name;
    this.subject = SubjectMessage.FOCAL_AREA_CLASSIFICATION;
  }

  async findAll() {
    this.action = BaseMessage.READ;
    const result = await this.regionalClassificationRepository.find();
    return this.response(result);
  }

  async findOne(id: number) {
    this.action = BaseMessage.READ;
    const result = await this.regionalClassificationRepository.findOne({
      where: {
        id: id,
      },
    });
    if (!result)
      throw new CustomBadRequestException(
        ContentMessage.NOT_FOUND,
        FieldMessage.REGIONAL_CLASSIFICATION,
        true,
      );
    return this.response(result);
  }
}
