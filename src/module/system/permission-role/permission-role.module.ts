import { Module } from '@nestjs/common';
import { PermissionRoleService } from './permission-role.service';
import { PermissionRoleController } from './permission-role.controller';

@Module({
  controllers: [],
  providers: [PermissionRoleService],
})
export class PermissionRoleModule {}
