import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../common/base/entity/base.entity';
import { Permission } from '../permission/permission.entity';
import { Role } from '../role';

@Entity()
export class PermissionRole extends BaseEntity {
  @ManyToOne(() => Role, (item) => item.permissionRoles)
  @JoinColumn()
  role: Role;

  @ManyToOne(() => Permission, (item) => item.permissionRoles)
  @JoinColumn()
  permission: Permission;

  @Column('boolean', {
    nullable: false,
    default: false,
  })
  isCheck: boolean;
}
