import { Injectable } from '@nestjs/common';
import { BaseService } from '../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Permission } from '../permission/permission.entity';
import { TreeRepository } from 'typeorm';
import { SubjectMessage } from '../../../common/message/subject.message';
import { BaseMessage } from '../../../common/message/base.message';
import { Account } from '../account-root';

@Injectable()
export class MenuService extends BaseService {
  constructor(
    @InjectRepository(Permission)
    private readonly treePermissionRepository: TreeRepository<Permission>,
  ) {
    super();
    this.name = MenuService.name;
    this.subject = SubjectMessage.MENU;
  }
  async findAll(account: Account) {
    this.action = BaseMessage.READ;
    const queries = await this.treePermissionRepository.createQueryBuilder('A');
    queries.leftJoinAndSelect('A.children', 'B', 'B.isMenu = 1');
    queries.leftJoin('B.permissionRoles', 'B1', 'B1.isCheck = 1');
    queries.leftJoin('B1.role', 'B2');
    queries.where('(B2.id IS NULL OR B2.id = :role)', {
      role: account.role.id,
    });

    queries.leftJoinAndSelect('B.children', 'C', 'C.isMenu = 1');
    queries.leftJoin('C.permissionRoles', 'C1', 'C1.isCheck = 1');
    queries.leftJoin('C1.role', 'C2');
    queries.where('(C2.id IS NULL OR C2.id = :role)', {
      role: account.role.id,
    });

    queries.leftJoinAndSelect('C.children', 'D', 'D.isMenu = 1');
    queries.leftJoin('D.permissionRoles', 'D1', 'D1.isCheck = 1');
    queries.leftJoin('D1.role', 'D2');
    queries.where('(D2.id IS NULL OR D2.id = :role)', {
      role: account.role.id,
    });

    return this.response((await queries.getOne())?.children);
  }
}
