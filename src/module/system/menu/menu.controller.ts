import { Controller, Get } from '@nestjs/common';
import { MenuService } from './menu.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { AccountDecorator } from '../../../common/decorator/account.decorator';
import { Account } from '../account-root';

@Controller()
@ApiTags(TagEnum.MENU)
export class MenuController {
  constructor(private readonly menuService: MenuService) {}

  @Get()
  findAll(@AccountDecorator() account: Account) {
    return this.menuService.findAll(account);
  }
}
