import { Module } from '@nestjs/common';
import { MenuService } from './menu.service';
import { MenuController } from './menu.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Permission } from '../permission/permission.entity';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([Permission])],
  controllers: [MenuController],
  providers: [MenuService],
})
export class MenuModule {}
export const menuRouter: RouteTree = {
  path: 'menu',
  module: MenuModule,
  children: [],
};
