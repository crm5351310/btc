import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RoleService } from './role.service';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';
import { CreateRoleDto, UpdateRoleDto } from './role.dto';
import { PathDto } from '../../../common/base/class/base.class';

@Controller()
@ApiTags(TagEnum.ROLE)
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Post()
  create(@Body() createRoleDto: CreateRoleDto) {
    return this.roleService.create(createRoleDto);
  }

  @Get()
  findAll() {
    return this.roleService.findAll();
  }

  @Get(':id')
  findOne(@Param() path: PathDto) {
    return this.roleService.findOne(path);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updateRoleDto: UpdateRoleDto) {
    return this.roleService.update(path, updateRoleDto);
  }

  @Delete(':id')
  remove(@Param() path: PathDto) {
    return this.roleService.remove(path);
  }
}
