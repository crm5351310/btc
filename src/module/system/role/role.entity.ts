import { Column, Entity, OneToMany } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, MaxLength } from 'class-validator';
import { PermissionRole } from '../permission-role/permission-role.entity';
import { Account } from '../account-root/account/account.entity';
import { BaseCategoryEntity } from "../../../common/base/entity/base-category.entity";

@Entity()
export class Role extends BaseCategoryEntity {
  // swagger
  @ApiProperty({
    description: 'Mô tả',
    default: 'Mô tả 1',
    maxLength: 1000,
  })
  // validate
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  // entity
  @Column('varchar', {
    length: 1000,
    nullable: true,
  })
  description: string;

  @OneToMany(() => PermissionRole, (item) => item.role)
  permissionRoles: PermissionRole[];

  @OneToMany(() => Account, (item) => item.role)
  accounts: Account[];
}
