import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { RouteTree } from '@nestjs/core';
import { RoleController } from './role.controller';
import { RoleService } from './role.service';
import { PermissionRole } from '../permission-role/permission-role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Role, PermissionRole])],
  controllers: [RoleController],
  providers: [RoleService],
  exports: [RoleService],
})
export class RoleModule {}
export const roleRouter: RouteTree = {
  path: 'role',
  module: RoleModule,
  children: [],
};
