import { OmitType, PartialType } from '@nestjs/swagger';
import { Role } from './role.entity';

export class CreateRoleDto extends OmitType(Role, ['id'] as const) {}

export class UpdateRoleDto extends PartialType(CreateRoleDto) {}
