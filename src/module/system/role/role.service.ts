import { Injectable } from '@nestjs/common';
import { CreateRoleDto, UpdateRoleDto } from './role.dto';
import { BaseService, PathDto } from '../../../common/base/class/base.class';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { SubjectMessage } from '../../../common/message/subject.message';
import { Role } from './role.entity';
import {
  CustomBadRequestException,
  CustomBadRequestExceptionCustom,
} from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import { PermissionRole } from '../permission-role/permission-role.entity';
import { BaseMessage } from '../../../common/message/base.message';

interface Condition {
  condition?: string;
  property?: string;
  isProp: boolean;
}

@Injectable()
export class RoleService extends BaseService {
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,

    @InjectRepository(PermissionRole)
    private readonly permissionRoleRepository: Repository<PermissionRole>,
  ) {
    super();
    this.name = RoleService.name;
    this.subject = SubjectMessage.ROLE;
  }
  async create(createRoleDto: CreateRoleDto) {
    this.action = BaseMessage.CREATE;
    const message: string[] = [];
    let flag = 0;

    const dubName = await this.roleRepository.findOne({
      where: {
        name: createRoleDto.name,
      },
    });
    if (dubName) {
      message.push(FieldMessage.NAME + '.' + ContentMessage.EXIST);
      flag++;
    }

    const dubCode = await this.roleRepository.findOne({
      where: {
        code: createRoleDto.code,
      },
    });
    if (dubCode) {
      message.push(FieldMessage.CODE + '.' + ContentMessage.EXIST);
      flag++;
    }

    if (flag) {
      throw new CustomBadRequestExceptionCustom(message);
    }

    const result = await this.roleRepository.save(createRoleDto);
    const permission: any[] = [];

    for (let i = 1; i <= 429; i++) {
      permission.push({
        role: {
          id: result.id,
        },
        permission: {
          id: i,
        },
      });
    }
    await this.permissionRoleRepository.save(permission);
    return this.response(result);
  }

  async findAll() {
    this.action = BaseMessage.READ;
    return this.response(await this.roleRepository.find());
  }

  async findOne(path: PathDto) {
    this.action = BaseMessage.READ;

    const result = await this.roleRepository.findOne({
      where: {
        id: path.id,
      },
    });
    return this.response(result);
  }

  async update(path: PathDto, updateRoleDto: UpdateRoleDto) {
    this.action = BaseMessage.UPDATE;
    const message: string[] = [];
    let flag = 0;

    const role = await this.roleRepository.findOne({
      where: path,
    });
    if (!role) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    const nameDublicate = await this.roleRepository.findOne({
      where: {
        name: updateRoleDto.name,
        id: Not(path.id),
      },
    });
    if (nameDublicate) {
      message.push(FieldMessage.NAME + '.' + ContentMessage.EXIST);
      flag++;
    }

    const codeDublicate = await this.roleRepository.findOne({
      where: {
        code: updateRoleDto.code,
        id: Not(path.id),
      },
    });
    if (codeDublicate) {
      message.push(FieldMessage.CODE + '.' + ContentMessage.EXIST);
      flag++;
    }
    if (flag) {
      throw new CustomBadRequestExceptionCustom(message);
    }
    return this.response(
      await this.roleRepository.save({
        ...updateRoleDto,
        id: path.id,
      }),
    );
  }

  async remove(path: PathDto) {
    this.action = BaseMessage.DELETE;
    const role = await this.roleRepository.findOne({
      where: {
        id: path.id,
        isFreeze: false,
      },
    });
    if (!role) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, this.subject, true);
    }
    await this.roleRepository.softDelete(path.id);
    await this.permissionRoleRepository.softDelete({
      role: {
        id: path.id,
      },
    });
    return this.response(path);
  }
}
