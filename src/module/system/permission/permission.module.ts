import { Module } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { PermissionController } from './permission.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Permission } from './permission.entity';
import { RouteTree } from '@nestjs/core';
import { Role } from '../role/role.entity';
import { PermissionRole } from '../permission-role/permission-role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Permission, Role, PermissionRole])],
  controllers: [PermissionController],
  providers: [PermissionService],
})
export class PermissionModule {}
export const permissionRouter: RouteTree = {
  path: 'permission',
  module: PermissionModule,
  children: [],
};
