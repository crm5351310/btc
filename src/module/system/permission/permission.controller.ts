import { Controller, Get, Body, Patch, Param, Query } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { UpdatePermissionDto } from './permission.dto';
import { PathDto } from '../../../common/base/class/base.class';
import { ApiTags } from '@nestjs/swagger';
import { TagEnum } from '../../../common/enum/tag.enum';

@Controller()
@ApiTags(TagEnum.PERMISSION)
export class PermissionController {
  constructor(private readonly permissionService: PermissionService) {}

  @Get()
  findAll(@Query() param: PathDto) {
    return this.permissionService.findAll(param);
  }

  @Patch(':id')
  update(@Param() path: PathDto, @Body() updatePermissionDto: UpdatePermissionDto) {
    return this.permissionService.update(path, updatePermissionDto);
  }
}
