import { Column, Entity, Tree, TreeChildren, TreeParent, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../common/base/entity/base.entity';
import { PermissionRole } from '../permission-role/permission-role.entity';
import { Expose } from 'class-transformer';
@Entity()
@Tree('nested-set')
export class Permission extends BaseEntity {
  @Column('varchar', {
    length: 100,
    nullable: false,
  })
  name: string;

  @Column('varchar', {
    length: 200,
    nullable: false,
  })
  code: string;

  @Column('varchar', {
    length: 200,
    nullable: false,
  })
  url: string;

  @Column('boolean', {
    nullable: false,
    default: false,
  })
  isMenu: boolean;

  @TreeParent()
  parent: Permission;

  @TreeChildren()
  children: Permission[];

  @OneToMany(() => PermissionRole, (item) => item.permission)
  permissionRoles: PermissionRole[];

  @Expose()
  get key(): number {
    return this.id;
  }

  @Expose()
  get label(): string {
    return this.name;
  }
}
