import { Injectable } from '@nestjs/common';
import { BaseService, PathDto, RelationTypeKeyBase } from '../../../common/base/class/base.class';
import { SubjectMessage } from '../../../common/message/subject.message';
import { UpdatePermissionDto } from './permission.dto';
import { Permission } from './permission.entity';
import { Repository, TreeRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseMessage } from '../../../common/message/base.message';
import { Role } from '../role';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import { PermissionRole } from '../permission-role/permission-role.entity';

@Injectable()
export class PermissionService extends BaseService {
  constructor(
    @InjectRepository(Permission)
    private readonly permissionRepository: Repository<Permission>,
    @InjectRepository(PermissionRole)
    private readonly permissionRoleRepository: Repository<PermissionRole>,
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
    @InjectRepository(Permission)
    private readonly treePermissionRepository: TreeRepository<Permission>,
  ) {
    super();
    this.name = PermissionService.name;
    this.subject = SubjectMessage.PERMISSION;
  }

  async findAll(param: PathDto) {
    this.action = BaseMessage.READ;
    const role = await this.roleRepository.findOne({ where: param });
    if (!role) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ROLE, true);
    }

    const query = await this.treePermissionRepository.createQueryBuilder('A');
    query.leftJoinAndSelect('A.children', 'B');
    query.leftJoin('B.permissionRoles', 'C');
    query.leftJoin('C.role', 'D');
    query.where('D.id = :role', { role: param.id });

    query.leftJoinAndSelect('B.children', 'B1');
    query.leftJoin('B1.permissionRoles', 'C1');
    query.leftJoin('C1.role', 'D1');
    query.where('D1.id IS NULL OR D1.id = :role', { role: param.id });

    query.leftJoinAndSelect('B1.children', 'B2');
    query.leftJoin('B2.permissionRoles', 'C2');
    query.leftJoin('C2.role', 'D2');
    query.where('D2.id IS NULL OR D2.id = :role', { role: param.id });

    query.leftJoinAndSelect('B2.children', 'B3');
    query.leftJoin('B3.permissionRoles', 'C3');
    query.leftJoin('C3.role', 'D3');
    query.where('D3.id IS NULL OR D3.id = :role', { role: param.id });

    const querySelection = this.treePermissionRepository.createQueryBuilder('A');
    querySelection.select('A.id', 'key');
    querySelection.leftJoin('A.permissionRoles', 'B');
    querySelection.leftJoin('B.role', 'C');
    querySelection.where('C.id = :role and B.isCheck = true', {
      role: param.id,
    });
    return this.response({
      treePermission: (await query.getOne())!.children,
      listSelectedPermission: await querySelection.getRawMany(),
    });
  }

  async update(path: PathDto, updatePermissionDto: UpdatePermissionDto) {
    this.action = BaseMessage.UPDATE;
    const data = updatePermissionDto.data;
    const role = await this.roleRepository.findOne({
      where: {
        id: path.id,
      },
    });
    if (!role) {
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.ROLE, false);
    }

    const ids = data.map((item: RelationTypeKeyBase) => item.key);
    const prePermission = await this.permissionRoleRepository.find({
      relations: {
        permission: true,
      },
      where: {
        role: {
          id: path.id,
        },
      },
    });
    const curPermission: PermissionRole[] = [];
    const permission = prePermission.map((item: PermissionRole) => {
      if (ids.includes(item.permission.id)) {
        curPermission.push({ ...item, isCheck: true });
      }
      return {
        ...item,
        isCheck: false,
      };
    });

    await this.permissionRoleRepository.save(permission);
    await this.permissionRoleRepository.save(curPermission);
    return this.response(ids);
  }
}
