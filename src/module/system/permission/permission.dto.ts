import { ApiProperty } from '@nestjs/swagger';
import { RelationTypeKeyBase } from '../../../common/base/class/base.class';
import { IsArray } from 'class-validator';

export class UpdatePermissionDto {
  @ApiProperty({
    description: 'key',
    type: [RelationTypeKeyBase],
  })
  @IsArray()
  data: RelationTypeKeyBase[];
}
