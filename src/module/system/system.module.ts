import { Module } from '@nestjs/common';
import { RouteTree } from '@nestjs/core';
import { AccountRootModule, accountRootRouter } from './account-root/account-root.module';
import { AuthModule, authRouter } from './auth/auth.module';
import {
  DepartmentRootModule,
  departmentRootRouter,
} from './department-root/department-root.module';
import { RoleModule, roleRouter } from './role/role.module';
import { PermissionModule, permissionRouter } from './permission/permission.module';
import { MenuModule, menuRouter } from './menu/menu.module';
import { PermissionRoleModule } from './permission-role/permission-role.module';

@Module({
  imports: [
    AuthModule,
    DepartmentRootModule,
    AccountRootModule,
    PermissionModule,
    RoleModule,
    MenuModule,
    PermissionRoleModule,
  ],
})
export class SystemModule {}
export const systemRouter: RouteTree = {
  path: 'system',
  module: SystemModule,
  children: [
    departmentRootRouter,
    accountRootRouter,
    authRouter,
    permissionRouter,
    menuRouter,
    roleRouter,
  ],
};
