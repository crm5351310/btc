import { CACHE_MANAGER, CacheModule } from '@nestjs/cache-manager';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { Cache } from 'cache-manager';
import * as redisStore from 'cache-manager-redis-store';
import { Repository } from 'typeorm';
import { dataSourceOptions } from '../../../../config/data-source.config';
import { Account } from '../../account-root/account/account.entity';
import { LoginPayloadDto } from '../auth.dto';
import { AuthModule } from '../auth.module';
import { AuthService } from '../auth.service';
import * as bcrypt from 'bcrypt';

// npm run test:run src/module/system/auth
describe('AuthService', () => {
  let authService: AuthService;
  let accountRepository: Repository<Account>;
  let jwtService: jest.Mocked<JwtService>;
  let cacheService: jest.Mocked<Cache>;

  let module: TestingModule;

  const ACCOUNT_REPOSITORY_TOKEN = getRepositoryToken(Account);

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        AuthModule,
        TypeOrmModule.forRootAsync({
          useFactory: () => dataSourceOptions,
        }),
        CacheModule.register({
          isGlobal: true,
          host: process.env.REDIS_HOST,
          port: process.env.REDIS_PORT,
          store: redisStore,
        }),
      ],
      providers: [
        {
          provide: CACHE_MANAGER,
          useValue: {
            set: jest.fn(),
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    accountRepository = module.get(ACCOUNT_REPOSITORY_TOKEN);
    jwtService = module.get(JwtService);
    cacheService = module.get(CACHE_MANAGER);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
    expect(accountRepository).toBeDefined();
    expect(jwtService).toBeDefined();
    expect(cacheService).toBeDefined();
  });

  describe('AuthService => login', () => {
    const payload = {
      username: 'admin@bitecco.vn',
      password: '12345678',
    } as LoginPayloadDto;

    const mockAccount = {
      id: 1,
      username: payload.username,
      fullname: 'admin',
      phoneNumber: '12345678',
      role: {
        id: 1,
      },
      department: {
        id: 1,
      },
      accountPassword: {
        password: '12345678',
      },
    } as Account;

    it('Tạo mới tài khoản thành công', async () => {
      jest.spyOn(accountRepository, 'findOne').mockResolvedValue(mockAccount);
      jest.spyOn(jwtService, 'signAsync').mockResolvedValue('accessToken');
      jest.spyOn(cacheService, 'set').mockImplementation((T: string) => Promise.resolve(T));
      jest.spyOn(cacheService, 'get').mockImplementation((T) => Promise.resolve(T));
      (bcrypt.compare as jest.Mock) = jest.fn().mockResolvedValue(true);

      const result = await authService.login(payload);

      expect(accountRepository.findOne).toHaveBeenCalled();
      expect(accountRepository.save).toHaveBeenCalled();

      expect(result.statusCode).toBe(200);
      expect(result.data).not.toBeNull();
      expect(result.data).toHaveProperty('accessToken');
      expect(result.data).toHaveProperty('refreshToken');
    });
  });
});
