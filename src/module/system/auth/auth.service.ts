import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Cache } from 'cache-manager';
import { BaseService } from '../../../common/base/class/base.class';
import { BaseMessage } from '../../../common/message/base.message';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import { SubjectMessage } from '../../../common/message/subject.message';
import { Repository } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { Account } from '../account-root/account/account.entity';
import { LoginPayloadDto, RefreshTokenPayloadDto } from './auth.dto';
import { CustomBadRequestException } from '../../../common/exception/bad.exception';

@Injectable()
export class AuthService extends BaseService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    private jwtService: JwtService,
    @Inject(CACHE_MANAGER) private cacheService: Cache,
  ) {
    super();
    this.name = AuthService.name;
    this.subject = SubjectMessage.AUTH;
  }

  async login(payload: LoginPayloadDto) {
    this.action = BaseMessage.LOGIN;

    const account = await this.accountRepository.findOne({
      where: { username: payload.username },
      relations: {
        accountPassword: true,
        role: true,
        department: true,
      },
    });
    if (!account)
      throw new CustomBadRequestException(ContentMessage.NOT_FOUND, FieldMessage.USERNAME, false);
    const isCheckPassword = await bcrypt.compare(
      payload.password,
      account.accountPassword.password,
    );
    if (!isCheckPassword)
      throw new CustomBadRequestException(ContentMessage.INVALIDED, FieldMessage.PASSWORD, false);

    // gen access token and refresh token key
    const accessTokenKey = `${account.username}${uuid()}`;
    const refreshTokenKey = `${account.username}${uuid()}`;

    // create token
    const payloadToSign = {
      username: account.username,
      fullname: account.fullname,
      role: {
        id: account.role.id,
      },
      // department: {
      //   id: account.department.id,
      //   name: account.department.name,
      // },
      accessTokenKey: accessTokenKey,
      refreshTokenKey: refreshTokenKey,
    };

    const accessToken = await this.jwtService.signAsync(payloadToSign, {
      secret: process.env.JWT_SECRET_KEY_FOR_ACCESS_TOKEN ?? 'bitecco',
      expiresIn: process.env.JWT_SECRET_KEY_FOR_ACCESS_TOKEN_EXPIRES_IN ?? 86400 + 's',
    });
    const refreshToken = await this.jwtService.signAsync(payloadToSign, {
      secret: process.env.JWT_SECRET_KEY_FOR_REFRESH_TOKEN ?? 'bitecco',
      expiresIn: process.env.JWT_SECRET_KEY_FOR_REFRESH_TOKEN_EXPIRES_IN ?? 8640000 + 's',
    });
    const valueCache = JSON.stringify({
      accountId: account.id,
      username: account.username,
      accessTokenKey: accessTokenKey,
      refreshTokenKey: refreshTokenKey,
    });
    await this.cacheService.set(`${accessTokenKey}`, valueCache, {
      ttl: Number(process.env.JWT_SECRET_KEY_FOR_ACCESS_TOKEN_EXPIRES_IN ?? 86400),
    });
    await this.cacheService.set(refreshTokenKey, valueCache, {
      ttl: Number(process.env.JWT_SECRET_KEY_FOR_REFRESH_TOKEN_EXPIRES_IN ?? 8640000),
    });
    return this.response({
      accessToken: accessToken,
      refreshToken: refreshToken,
    });
  }

  async logout(accessTokenKey: string) {
    this.action = BaseMessage.LOGOUT;

    const cacheData = JSON.parse(String(await this.cacheService.get(`${accessTokenKey}`)));
    await this.cacheService.del(`${accessTokenKey}`);
    await this.cacheService.del(`${cacheData.refreshTokenKey}`);
    return this.response({});
  }

  async refresh(payload: RefreshTokenPayloadDto) {
    this.action = BaseMessage.REFRESH;

    const data = await this.jwtService.verifyAsync(payload.refreshToken, {
      secret: process.env.JWT_SECRET_KEY_FOR_REFRESH_TOKEN,
    });
    const cacheDataRefreshToken = JSON.parse(
      String(await this.cacheService.get(`${data.refreshTokenKey}`)),
    );
    if (!cacheDataRefreshToken)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.REFRESH_TOKEN, true);
    const account = await this.accountRepository.findOne({
      where: {
        id: cacheDataRefreshToken.accountId,
      },
      relations: {
        role: true,
        department: true,
      },
    });
    if (!account)
      throw new CustomBadRequestException(ContentMessage.INVALID, FieldMessage.REFRESH_TOKEN, true);
    const accessTokenKey = `${account.username}${uuid()}`;

    const payloadToSign = {
      id: account.id,
      username: account.username,
      role: {
        id: account.role.id,
      },
      fullname: account.fullname,
      donVi: {
        id: account.department.id,
        name: account.department.name,
      },
      accessTokenKey: accessTokenKey,
      refreshTokenKey: data.refreshTokenKey,
    };
    cacheDataRefreshToken.accessTokenKey = `${cacheDataRefreshToken.username}${uuid()}`;

    const accessToken = await this.jwtService.signAsync(payloadToSign, {
      secret: process.env.JWT_SECRET_KEY_FOR_ACCESS_TOKEN ?? 'bitecco',
      expiresIn: process.env.JWT_SECRET_KEY_FOR_ACCESS_TOKEN_EXPIRES_IN ?? 86400 + 's',
    });
    cacheDataRefreshToken.refreshTokenKey = data.refreshTokenKey;
    await this.cacheService.set(`${accessTokenKey}`, `${JSON.stringify(cacheDataRefreshToken)}`, {
      ttl: Number(process.env.JWT_SECRET_KEY_FOR_ACCESS_TOKEN_EXPIRES_IN),
    });
    return this.response({ accessToken: accessToken });
  }
}
