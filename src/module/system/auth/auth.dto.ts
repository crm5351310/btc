import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { ContentMessage } from '../../../common/message/content.message';
import { FieldMessage } from '../../../common/message/field.message';
import { SubjectMessage } from '../../../common/message/subject.message';
import ResponseHelper from '../../../common/utils/reponse.util';

export class LoginPayloadDto {
  @ApiProperty({
    description: 'Tên tài khoản người dùng',
    default: 'admin@bitecco.vn',
  })
  @IsNotEmpty({
    message: ResponseHelper.response(
      SubjectMessage.LOGIN,
      ContentMessage.REQUIRED,
      FieldMessage.USERNAME,
    ),
  })
  username: string;

  @ApiProperty({
    description: 'Mật khẩu người dùng',
    default: '12345678',
  })
  @IsNotEmpty({
    message: ResponseHelper.response(
      SubjectMessage.LOGIN,
      ContentMessage.REQUIRED,
      FieldMessage.PASSWORD,
    ),
  })
  password: string;
}

export class RefreshTokenPayloadDto {
  @ApiProperty({
    description: 'Refresh token',
    default: '******',
  })
  @IsNotEmpty({
    message: ResponseHelper.response(
      SubjectMessage.REFRESH,
      ContentMessage.REQUIRED,
      FieldMessage.REFRESH_TOKEN,
    ),
  })
  refreshToken: string;
}
