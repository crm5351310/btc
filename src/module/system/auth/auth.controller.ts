import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AccountDecorator } from '../../../common/decorator/account.decorator';
import { TagEnum } from '../../../common/enum/tag.enum';
import { Account } from '../account-root/account/account.entity';
import { LoginPayloadDto, RefreshTokenPayloadDto } from './auth.dto';
import { AuthGuard } from '../../../common/guard/auth.guard';
import { AuthService } from './auth.service';

@Controller()
@ApiTags(TagEnum.AUTH)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() payload: LoginPayloadDto) {
    const result = await this.authService.login(payload);
    return result;
  }

  @Get('logout')
  @UseGuards(AuthGuard)
  @ApiBearerAuth('Token')
  async logout(@AccountDecorator() account: Account) {
    const result = await this.authService.logout(account.accessTokenKey);
    return result;
  }

  @Post('refresh-token')
  async refresh(@Body() payload: RefreshTokenPayloadDto) {
    const result = await this.authService.refresh(payload);
    return result;
  }
}
