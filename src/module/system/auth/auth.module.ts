import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from '../account-root/account/account.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { RouteTree } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([Account]), JwtModule],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
export const authRouter: RouteTree = {
  path: 'auth',
  module: AuthModule,
  children: [],
};
